/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.message;

import org.ws4d.java.communication.ProtocolVersionInfo;
import org.ws4d.java.constants.DPWSMessageConstants;
import org.ws4d.java.types.AppSequence;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.URI;
import org.ws4d.java.types.UnknownDataContainer;
import org.ws4d.java.util.StringUtil;

/**
 * This class implements an abstract MessageObject.
 */
public abstract class Message extends UnknownDataContainer implements DPWSMessageConstants {

	public final static int UKNOWN_ROUTING_SCHEME	=	0x0;
	public final static int UNICAST_ROUTING_SCHEME	=	0x1;
	public final static int MULTICAST_ROUTING_SCHEME	=	0x2;
	
	
	protected MessageHeader	header;

	/**
	 * If <code>true</code>, then this is a message received over a remote
	 * communication channel; if <code>false</code>, the message is being sent
	 * from this stack instance.
	 */
	protected boolean			inbound			= false;

	// only meaningful for outgoing request messages
	private URI					targetAddress;
	
	private int 				routingScheme=UKNOWN_ROUTING_SCHEME;

	private boolean				secureMessage	= false;

	private Object				certificate;

	private Object				privateKey;

	/**
	 * Constructor.
	 * 
	 * @param header
	 */
	public Message(MessageHeader header) {
		super();
		this.header = header;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(" ]");
		return sb.toString();
	}

	/**
	 * Sets the {@link #getRelatesTo() [relationship]}, {@link #getTo() [to]}
	 * and [parameters] properties of this message to the values of the
	 * {@link #getMessageId() [message ID]} and {@link #getReplyTo() [reply to]}
	 * properties of the passed in message.
	 * 
	 * @param request the message from which to extract the source properties
	 */
	public void setResponseTo(Message request) {
		header.setResponseTo(request.header);
		header.setVersion(request.getVersion());
	}

	/**
	 * Sets the {@link #getRelatesTo() [relationship]}, {@link #getTo() [to]}
	 * and [parameters] properties of this message to the values of the
	 * {@link SOAPHeader#getMessageId() [message ID]} and
	 * {@link SOAPHeader#getReplyTo() [reply to]} properties of the passed in
	 * SOAP header.
	 * 
	 * @param requestHeader the SOAP header from which to extract the source
	 *            properties
	 */
	public void setResponseTo(MessageHeader requestHeader) {
		header.setResponseTo(requestHeader);
	}

	// ----------------------- MESSAGE -----------------------------

	/**
	 * Type of message.
	 * 
	 * @return type.
	 */
	public abstract int getType();

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getHeader()
	 */
	public MessageHeader getHeader() {
		return header;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getAction()
	 */
	public AttributedURI getAction() {
		return header.getAction();
	}

	/**
	 * Send using WS-Security techniques.
	 */
	public boolean isSecure() {
		return secureMessage;
	}

	/**
	 * This is the certificate against which the messages signature will be
	 * validated. Only used within the security module.
	 * 
	 * @param certificate must be the java.security.cert.Certificate of the
	 *            sender device/service
	 */
	public void setCertificate(Object certificate) {
		this.certificate = certificate;
	}

	/**
	 * This is the private key with which this message will be signed. Only used
	 * within the security module.
	 * 
	 * @param privKey must be the java.security.PrivateKey of the sender device/
	 *            service
	 */
	public void setPrivateKey(Object privKey) {
		this.privateKey = privKey;
	}

	/**
	 * This is the certificate against which the messages signature will be
	 * validated. * Only used within the security module.
	 * 
	 * @return java.security.cert.Certificate
	 */
	public Object getCertificate() {
		return certificate;
	}

	/**
	 * This is the private key with which this message will be signed. Only used
	 * within the security module.
	 * 
	 * @return java.security.PrivateKey
	 */
	public Object getPrivateKey() {
		return privateKey;
	}

	/**
	 * Sets wether or not the message should be sent secure. Caution: don't set
	 * this flag when received the message.
	 */
	public void setSecure(boolean b) {
		this.secureMessage = b;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getMessageId()
	 */
	public AttributedURI getMessageId() {
		return header.getMessageId();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getRelatesTo()
	 */
	public AttributedURI getRelatesTo() {
		return header.getRelatesTo();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getTo()
	 */
	public AttributedURI getTo() {
		return header.getTo();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getReplyTo()
	 */
	public EndpointReference getReplyTo() {
		return header.getReplyTo();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getAppSequence()
	 */
	public AppSequence getAppSequence() {
		return header.getAppSequence();
	}

	/**
	 * @return the targetAddress
	 */
	public URI getTargetAddress() {
		return targetAddress;
	}

	/**
	 * @param targetAddress the targetAddress to set
	 */
	public void setTargetAddress(URI targetAddress) {
		this.targetAddress = targetAddress;
	}

	/**
	 * Returns <code>true</code> if this message was received over a remote
	 * communication channel. Returns <code>false</code> if the message is being
	 * sent from this stack instance.
	 * 
	 * @return whether this is an inbound or an outbound message
	 */
	public boolean isInbound() {
		return inbound;
	}

	/**
	 * @param inbound the inbound to set
	 */
	public void setInbound(boolean inbound) {
		this.inbound = inbound;
	}

	public void setVersion(ProtocolVersionInfo Version) {
		header.setVersion(Version);
	}

	public ProtocolVersionInfo getVersion() {
		return header.getVersion();
	}
	
	
	/**
	 * Gets the outgoing routing scheme for this message. 
	 * It can be  unknown (0x0), unicast (0x1), multicast (0x2).
	 * http://en.wikipedia.org/wiki/Routing
	 * 
	 * @return
	 */
	public int getRoutingScheme()
	{
		return routingScheme;
	}
	
	public void setRoutingScheme(int routingScheme) {
		this.routingScheme = routingScheme;
	}

	public void setHeader(MessageHeader header) 
	{
		this.header = header;
	}

	/* Methods to parse and serialize */
	// public abstract Message parse(ElementParser parser, DPWSProtocolData
	// protocolData, SOAPHeader header) throws IOException;
	// public abstract void serialize(XmlSerializer serializer, ConstantsHelper helper) throws IOException;
	
	//Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPPWS Module
	//	public abstract void serialize(XmlSerializer serializer, Object helper) throws IOException;
}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.message;

import org.ws4d.java.communication.ProtocolVersionInfo;
import org.ws4d.java.types.AppSequence;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.ReferenceParametersMData;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.StringUtil;

/**
 * 
 */
public class DefaultMessageHeader extends  MessageHeader implements EventingMessageHeader{

	private ProtocolVersionInfo			versionInfo			= null;

	private AttributedURI				action;

	private AttributedURI				messageId;

	private AttributedURI				relatesTo;

	private EndpointReference			replyTo;

	private AttributedURI				to;
	
	private AttributedURI				from;

	private AppSequence					appSequence;

	private ReferenceParametersMData	referenceParameters;

	private byte[]						sigVal;

	private boolean						signatureValidated	= true;

	private EndpointReference endpointReference;
	
	private IMessageEndpoint endpoint=null;


	/**
	 * 
	 */
	public DefaultMessageHeader() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ action=").append(action);
		sb.append(", messageId=").append(messageId);
		sb.append(", relatesTo=").append(relatesTo);
		sb.append(", replyTo=").append(replyTo);
		sb.append(", to=").append(to);
		sb.append(", from=").append(from);
		sb.append(", appSequence=").append(appSequence);
		sb.append(", referenceParameters=").append(referenceParameters);
		if (sigVal!=null)
		{
			sb.append(", Sig=").append(sigVal);
			sb.append(", Signature Valid=").append(signatureValidated);
		}

		sb.append(" ]");
		return sb.toString();
	}

	/**
	 * Sets the {@link #getRelatesTo() [relationship]}, {@link #getTo() [to]}
	 * and [parameters] properties of this SOAP header to the values of the
	 * {@link #getMessageId() [message ID]} and {@link #getReplyTo() [reply to]}
	 * properties of the passed in request SOAP header.
	 * 
	 * @param requestHeader the SOAP header to extract the source properties
	 *            from
	 */
	@Override
	public void setResponseTo(MessageHeader requestHeader) {
		this.relatesTo = requestHeader.getMessageId();
		EndpointReference replyTo = requestHeader.getReplyTo();
		/*
		 * if no [reply to] specified, we don't include
		 * WSAConstants.WSA_ANONYMOUS as [to] header property of the response
		 */
		if (replyTo != null) {
			setEndpointReference(replyTo);
		}
	}

	@Override
	public void setSignature(byte[] sig) {
		sigVal = sig;
	}

	@Override
	public byte[] getSignature() {
		return sigVal;
	}

	@Override
	public void setValidated(boolean valid) {
		this.signatureValidated = valid;
	}

	@Override
	public boolean isValidated() {
		return signatureValidated;
	}

	@Override
	public AttributedURI getAction() {
		return action;
	}

	@Override
	public AppSequence getAppSequence() {
		return appSequence;
	}

	@Override
	public AttributedURI getMessageId() {
		return messageId;
	}

	@Override
	public ProtocolVersionInfo getVersion() {
		return versionInfo;
	}

	@Override
	public AttributedURI getRelatesTo() {
		return relatesTo;
	}

	@Override
	public EndpointReference getReplyTo() {
		return replyTo;
	}

	@Override
	public AttributedURI getTo() {
		return to;
	}

	@Override
	public ReferenceParametersMData getReferenceParameters() {
		return referenceParameters;
	}


	@Override
	public EndpointReference getEndpointReference() {
		return endpointReference;
	}

	/**
	 * Sets the {@link #getTo() to header property} to the value of the
	 * {@link EndpointReference#getAddress() address property} of the specified
	 * endpoint reference and copies any contained
	 * {@link EndpointReference#getReferenceParameters() reference parameters}
	 * into this SOAP header instance (see {@link #getReferenceParameters()}).
	 * 
	 * @param ref the endpoint reference to set
	 */
	@Override
	public void setEndpointReference(EndpointReference ref) {
		this.endpointReference=ref;
		if (this.endpointReference!=null)
		{
			to = this.endpointReference.getAddress();
			referenceParameters = this.endpointReference.getReferenceParameters();
		}
	}

	/**
	 * @param action the action to set
	 */
	@Override
	public void setAction(AttributedURI action) {
		this.action = action;
	}

	/**
	 * @param messageId the messageId to set
	 */
	@Override
	public void setMessageId(AttributedURI messageId) {
		this.messageId = messageId;
	}

	/**
	 * @param relatesTo the relatesTo to set
	 */
	@Override
	public void setRelatesTo(AttributedURI relatesTo) {
		this.relatesTo = relatesTo;
	}

	/**
	 * @param replyTo the replyTo to set
	 */
	@Override
	public void setReplyTo(EndpointReference replyTo) {
		this.replyTo = replyTo;
	}

	/**
	 * @param to the to to set
	 */
	@Override
	public void setTo(AttributedURI to) {
		this.to = to;
	}

	/**
	 * @param appSequence the appSequence to set
	 */
	@Override
	public void setAppSequence(AppSequence appSequence) {
		this.appSequence = appSequence;
	}

	@Override
	public void setVersion(ProtocolVersionInfo info) {
		this.versionInfo = info;
	}

	@Override
	public void setReferenceParameters(ReferenceParametersMData data) {
		this.referenceParameters = data;
	}

	@Override
	public URI getEventSubscriptionIdentifier() {
		return referenceParameters == null ? null : referenceParameters.getWseIdentifier();
	}

	@Override
	public void setEventSubscriptionIdentifier(URI identifier) {
		if (referenceParameters == null) {
			referenceParameters = new ReferenceParametersMData();
		}
		referenceParameters.setWseIdentifier(identifier);
	}

	/**
	 * @param endpoint the endpoint to set
	 */
	@Override
	public void setMessageEndpoint(IMessageEndpoint endpoint) {
		this.endpoint = endpoint;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.message.MessageHeader#getMessageEndpoint()
	 */
	@Override
	public IMessageEndpoint getMessageEndpoint() {
		// TODO Auto-generated method stub
		return endpoint;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.message.MessageHeader#getFrom()
	 */
	@Override
	public AttributedURI getFrom() {
		return from;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.message.MessageHeader#setFrom(org.ws4d.java.types.AttributedURI)
	 */
	@Override
	public void setFrom(AttributedURI from) 
	{
		this.from=from;
	}

}

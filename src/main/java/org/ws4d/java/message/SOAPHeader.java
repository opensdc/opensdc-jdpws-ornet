/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.message;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.communication.DPWSUtil;
import org.ws4d.java.communication.ProtocolVersionInfo;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.DPWSMessageConstants;
import org.ws4d.java.constants.MEXConstants;
import org.ws4d.java.constants.WSAConstants;
import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.constants.WXFConstants;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.types.AppSequence;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.ReferenceParametersMData;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.StringUtil;

/**
 * 
 */
public class SOAPHeader extends  MessageHeader implements EventingMessageHeader{

	private ProtocolVersionInfo			versionInfo			= null;

	private AttributedURI				action;

	private AttributedURI				messageId;

	private AttributedURI				relatesTo;

	private EndpointReference			replyTo;

	private AttributedURI				to;
	
	private AttributedURI				from;

	private AppSequence					appSequence;

	private ReferenceParametersMData	referenceParameters;

	private byte[]						sigVal;

	private boolean						signatureValidated	= true;

	private EndpointReference 			endpointReference;

	private IMessageEndpoint			messageEndpoint;

	/**
	 * 
	 */
	public SOAPHeader() {
		super();
	}

	public void read(MessageHeader msgHeader)
	{
		setAction(msgHeader.getAction());
		setAppSequence(msgHeader.getAppSequence());
		setEndpointReference(msgHeader.getEndpointReference());
		
		if (msgHeader instanceof EventingMessageHeader)
			setEventSubscriptionIdentifier(((EventingMessageHeader)msgHeader).getEventSubscriptionIdentifier());
		else
			setEventSubscriptionIdentifier(null);
		
		setMessageId(msgHeader.getMessageId());
		setReferenceParameters(msgHeader.getReferenceParameters());
		setRelatesTo(msgHeader.getRelatesTo());
		setReplyTo(msgHeader.getReplyTo());
		setSignature(msgHeader.getSignature());
		setTo(msgHeader.getTo());
		setValidated(msgHeader.isValidated());
		setVersion(msgHeader.getVersion());
		setMessageEndpoint(msgHeader.getMessageEndpoint());
		setFrom(msgHeader.getFrom());
		
		if (msgHeader.getUnknownAttributes()!=null)
			setUnknownAttributes(msgHeader.getUnknownAttributes());
		else if (unknownAttributes !=null)
			unknownAttributes.clear();
		
		if (msgHeader.getUnknownElements()!=null)
			setUnknownElements(msgHeader.getUnknownElements());
		else if (unknownAttributes !=null)
			unknownElements_QN_2_List.clear();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ action=").append(action);
		sb.append(", messageId=").append(messageId);
		sb.append(", relatesTo=").append(relatesTo);
		sb.append(", replyTo=").append(replyTo);
		sb.append(", to=").append(to);
		sb.append(", appSequence=").append(appSequence);
		sb.append(", referenceParameters=").append(referenceParameters);
		SecurityManagerModule secMod=(SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);
		if (sigVal!=null && secMod!=null)
		{
			sb.append(", Sig=").append(secMod.encode(sigVal));
			sb.append(", Signature Valid=").append(signatureValidated);
		}

		sb.append(" ]");
		return sb.toString();
	}

	/**
	 * Sets the {@link #getRelatesTo() [relationship]}, {@link #getTo() [to]}
	 * and [parameters] properties of this SOAP header to the values of the
	 * {@link #getMessageId() [message ID]} and {@link #getReplyTo() [reply to]}
	 * properties of the passed in request SOAP header.
	 * 
	 * @param requestHeader the SOAP header to extract the source properties
	 *            from
	 */
	@Override
	public void setResponseTo(MessageHeader requestHeader) {
		this.relatesTo = requestHeader.getMessageId();
		EndpointReference replyTo = requestHeader.getReplyTo();
		/*
		 * if no [reply to] specified, we don't include
		 * WSAConstants.WSA_ANONYMOUS as [to] header property of the response
		 */
		if (replyTo != null) {
			setEndpointReference(replyTo);
		}
	}

	@Override
	public void setSignature(byte[] sig) {
		sigVal = sig;
	}

	@Override
	public byte[] getSignature() {
		return sigVal;
	}

	@Override
	public void setValidated(boolean valid) {
		this.signatureValidated = valid;
	}

	@Override
	public boolean isValidated() {
		return signatureValidated;
	}

	@Override
	public AttributedURI getAction() {
		return action;
	}

	@Override
	public AppSequence getAppSequence() {
		return appSequence;
	}

	@Override
	public AttributedURI getMessageId() {
		return messageId;
	}

	@Override
	public ProtocolVersionInfo getVersion() {
		return versionInfo;
	}

	@Override
	public AttributedURI getRelatesTo() {
		return relatesTo;
	}

	@Override
	public EndpointReference getReplyTo() {
		return replyTo;
	}

	@Override
	public AttributedURI getTo() {
		return to;
	}

	public URI getWseIdentifier() {
		return referenceParameters == null ? null : referenceParameters.getWseIdentifier();
	}

	public int getDPWSMessageType() {
		if (action == null) {
			return DPWSMessageConstants.UNKNOWN_MESSAGE;
		}

		ConstantsHelper helper = DPWSUtil.getHelper(versionInfo);

		String actionString = action.toString();
		if (helper.getWSDActionHello().equals(actionString)) {
			return DPWSMessageConstants.HELLO_MESSAGE;
		}
		if (helper.getWSDActionBye().equals(actionString)) {
			return DPWSMessageConstants.BYE_MESSAGE;
		}
		if (helper.getWSDActionProbe().equals(actionString)) {
			return DPWSMessageConstants.PROBE_MESSAGE;
		}
		if (helper.getWSDActionProbeMatches().equals(actionString)) {
			return DPWSMessageConstants.PROBE_MATCHES_MESSAGE;
		}
		if (helper.getWSDActionResolve().equals(actionString)) {
			return DPWSMessageConstants.RESOLVE_MESSAGE;
		}
		if (helper.getWSDActionResolveMatches().equals(actionString)) {
			return DPWSMessageConstants.RESOLVE_MATCHES_MESSAGE;
		}
		if (WSEConstants.WSE_ACTION_GETSTATUS.equals(actionString)) {
			return DPWSMessageConstants.GET_STATUS_MESSAGE;
		}
		if (WSEConstants.WSE_ACTION_GETSTATUSRESPONSE.equals(actionString)) {
			return DPWSMessageConstants.GET_STATUS_RESPONSE_MESSAGE;
		}
		if (WSEConstants.WSE_ACTION_RENEW.equals(actionString)) {
			return DPWSMessageConstants.RENEW_MESSAGE;
		}
		if (WSEConstants.WSE_ACTION_RENEWRESPONSE.equals(actionString)) {
			return DPWSMessageConstants.RENEW_RESPONSE_MESSAGE;
		}
		if (WSEConstants.WSE_ACTION_SUBSCRIBE.equals(actionString)) {
			return DPWSMessageConstants.SUBSCRIBE_MESSAGE;
		}
		if (WSEConstants.WSE_ACTION_SUBSCRIBERESPONSE.equals(actionString)) {
			return DPWSMessageConstants.SUBSCRIBE_RESPONSE_MESSAGE;
		}
		if (WSEConstants.WSE_ACTION_SUBSCRIPTIONEND.equals(actionString)) {
			return DPWSMessageConstants.SUBSCRIPTION_END_MESSAGE;
		}
		if (WSEConstants.WSE_ACTION_UNSUBSCRIBE.equals(actionString)) {
			return DPWSMessageConstants.UNSUBSCRIBE_MESSAGE;
		}
		if (WSEConstants.WSE_ACTION_UNSUBSCRIBERESPONSE.equals(actionString)) {
			return DPWSMessageConstants.UNSUBSCRIBE_RESPONSE_MESSAGE;
		}
		if (WXFConstants.WXF_ACTION_GET.equals(actionString)) {
			return DPWSMessageConstants.GET_MESSAGE;
		}
		if (WXFConstants.WXF_ACTION_GETRESPONSE.equals(actionString)) {
			return DPWSMessageConstants.GET_RESPONSE_MESSAGE;
		}
		if (MEXConstants.WSX_ACTION_GETMETADATA_REQUEST.equals(actionString)) {
			return DPWSMessageConstants.GET_METADATA_MESSAGE;
		}
		if (MEXConstants.WSX_ACTION_GETMETADATA_RESPONSE.equals(actionString)) {
			return DPWSMessageConstants.GET_METADATA_RESPONSE_MESSAGE;
		}
		if (WSAConstants.WSA_ACTION_ADDRESSING_FAULT.equals(actionString) || DPWSConstants.DPWS_ACTION_DPWS_FAULT.equals(actionString) || WSAConstants.WSA_ACTION_SOAP_FAULT.equals(actionString) || WSDConstants.WSD_ACTION_WSD_FAULT.equals(actionString)) {
			return DPWSMessageConstants.FAULT_MESSAGE;
		}
		return DPWSMessageConstants.INVOKE_MESSAGE;
	}

	@Override
	public ReferenceParametersMData getReferenceParameters() {
		return referenceParameters;
	}

	/**
	 * Sets the {@link #getTo() to header property} to the value of the
	 * {@link EndpointReference#getAddress() address property} of the specified
	 * endpoint reference and copies any contained
	 * {@link EndpointReference#getReferenceParameters() reference parameters}
	 * into this SOAP header instance (see {@link #getReferenceParameters()}).
	 * 
	 * @param ref the endpoint reference to set
	 */
	@Override
	public void setEndpointReference(EndpointReference ref) {
		this.endpointReference=ref;
		if (this.endpointReference!=null)
		{
			to = this.endpointReference.getAddress();
			referenceParameters = this.endpointReference.getReferenceParameters();
		}
	}

	/**
	 * @param action the action to set
	 */
	@Override
	public void setAction(AttributedURI action) {
		this.action = action;
	}

	/**
	 * @param messageId the messageId to set
	 */
	@Override
	public void setMessageId(AttributedURI messageId) {
		this.messageId = messageId;
	}

	/**
	 * @param relatesTo the relatesTo to set
	 */
	@Override
	public void setRelatesTo(AttributedURI relatesTo) {
		this.relatesTo = relatesTo;
	}

	/**
	 * @param replyTo the replyTo to set
	 */
	@Override
	public void setReplyTo(EndpointReference replyTo) {
		this.replyTo = replyTo;
	}

	/**
	 * @param to the to to set
	 */
	@Override
	public void setTo(AttributedURI to) {
		this.to = to;
	}

	/**
	 * @param appSequence the appSequence to set
	 */
	@Override
	public void setAppSequence(AppSequence appSequence) {
		this.appSequence = appSequence;
	}

	/**
	 * @param wseIdentifier the wseIdentifier to set
	 */
	public void setWseIdentifier(URI wseIdentifier) {
		if (referenceParameters == null) {
			referenceParameters = new ReferenceParametersMData();
		}
		referenceParameters.setWseIdentifier(wseIdentifier);
	}

	@Override
	public void setVersion(ProtocolVersionInfo info) {
		this.versionInfo = info;
	}

	@Override
	public void setReferenceParameters(ReferenceParametersMData data) {
		this.referenceParameters = data;
	}

	

	
	@Override
	public URI getEventSubscriptionIdentifier() {
		return getWseIdentifier();
	}

	
	@Override
	public void setEventSubscriptionIdentifier(URI identifier) 
	{
		setWseIdentifier(identifier);
	}

	
	@Override
	public EndpointReference getEndpointReference() {
		return this.endpointReference;
	}

	/**
	 * @param messageEndpoint the messageEndpoint to set
	 */
	@Override
	public void setMessageEndpoint(IMessageEndpoint messageEndpoint) {
		this.messageEndpoint = messageEndpoint;
	}

	/**
	 * @return the messageEndpoint
	 */
	@Override
	public IMessageEndpoint getMessageEndpoint() {
		return messageEndpoint;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.message.MessageHeader#getFrom()
	 */
	@Override
	public AttributedURI getFrom() {
		return from;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.message.MessageHeader#setFrom(org.ws4d.java.types.AttributedURI)
	 */
	@Override
	public void setFrom(AttributedURI from) {
		this.from=from;
		
	}

}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.message;

import org.ws4d.java.communication.ProtocolVersionInfo;
import org.ws4d.java.types.AppSequence;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.ReferenceParametersMData;
import org.ws4d.java.types.UnknownDataContainer;

public abstract class MessageHeader extends UnknownDataContainer{
	
	public abstract AttributedURI getAction();
	
	public abstract AppSequence getAppSequence();
	
	public abstract EndpointReference getEndpointReference();

	public abstract AttributedURI getMessageId();
	
	public abstract ReferenceParametersMData getReferenceParameters();

	public abstract AttributedURI getRelatesTo();
	
	public abstract EndpointReference getReplyTo();
	
	public abstract byte[] getSignature();

	public abstract AttributedURI getTo();
	
	public abstract AttributedURI getFrom();

	public abstract ProtocolVersionInfo getVersion();
	
	public abstract boolean isValidated();
	
	//Setter
	
	public abstract void setTo(AttributedURI attributedURI);
	public abstract void setFrom(AttributedURI attributedURI);
	
	public abstract void setAction(AttributedURI attributedURI);
	
	public abstract void setMessageId(AttributedURI attributedURI);
	public abstract void setResponseTo(MessageHeader header);

	public abstract void setVersion(ProtocolVersionInfo version);

	public abstract void setEndpointReference(EndpointReference endpointReference);

	public abstract void setAppSequence(AppSequence next);

	public abstract void setReplyTo(EndpointReference endpointReference);

	public abstract void setValidated(boolean validated);

	public abstract void setRelatesTo(AttributedURI attributedURI);

	public abstract void setReferenceParameters(ReferenceParametersMData data);
	
	public abstract void setSignature(byte[] sig);
	
	public abstract IMessageEndpoint getMessageEndpoint();
	public abstract void setMessageEndpoint(IMessageEndpoint msgEndpoint);
}

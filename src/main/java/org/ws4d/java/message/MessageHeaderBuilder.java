/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.message;

import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.util.IDGenerator;

public class MessageHeaderBuilder {
	private final static  MessageHeaderBuilder instance=new MessageHeaderBuilder();

	public static MessageHeaderBuilder getInstance() {
		return instance;
	}
	
	private MessageHeaderBuilder()
	{
		//void
	}
	
	/**
	 * Returns a new SOAP header having only set its {@link #getAction() action
	 * property} to an {@link AttributedURI} constructed from the specified
	 * String <code>action</code>. Any other fields are empty.
	 * 
	 * @param action the action to set
	 * @return the newly created SOAP header
	 */
	public MessageHeader createHeader(String action) 
	{
		MessageHeader header = new DefaultMessageHeader();
		header.setAction(new AttributedURI(action));
		return header;
	}

	/**
	 * Returns a new SOAP header having set its {@link #getAction() action
	 * property} to an {@link AttributedURI} constructed from the specified
	 * String <code>action</code>. Additionally, the header's
	 * {@link #getMessageId() message ID property} is set to a new randomly and
	 * uniquely generated UUID URN. Any other fields are empty.
	 * 
	 * @param action the action to set
	 * @return the newly created SOAP header including a message ID
	 * @see #createHeader(String)
	 */
	public MessageHeader createRequestHeader(String action) {
		MessageHeader header = createHeader(action);
		header.setMessageId(new AttributedURI(IDGenerator.getUUIDasURI()));
		return header;
	}
}

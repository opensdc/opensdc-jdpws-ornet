/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.message.metadata;

import org.ws4d.java.constants.MEXConstants;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.message.MessageHeaderBuilder;
import org.ws4d.java.types.URI;

/**
 * 
 */
public class GetMetadataMessage extends Message {

	public static final URI	ACTION	= new URI(MEXConstants.WSX_ACTION_GETMETADATA_REQUEST);

	private URI				dialect;

	private URI				identifier;

	/**
	 * Creates a new Get message containing a {@link SOAPHeader} with the
	 * appropriate {@link SOAPHeader#getAction() action property} set and a
	 * unique {@link SOAPHeader#getMessageId() message ID property}. All other
	 * header- and metadataexchange-related fields are empty and it is the
	 * caller's responsibility to fill them with suitable values.
	 */
	public GetMetadataMessage() {
		this(MessageHeaderBuilder.getInstance().createRequestHeader(MEXConstants.WSX_ACTION_GETMETADATA_REQUEST));
	}

	/**
	 * @param header
	 */
	public GetMetadataMessage(MessageHeader header) {
		super(header);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.Message#getType()
	 */
	@Override
	public int getType() {
		return GET_METADATA_MESSAGE;
	}

	/**
	 * @return the dialect
	 */
	public URI getDialect() {
		return dialect;
	}

	/**
	 * @param dialect the dialect to set
	 */
	public void setDialect(URI dialect) {
		this.dialect = dialect;
	}

	/**
	 * @return the identifier
	 */
	public URI getIdentifier() {
		return identifier;
	}

	/**
	 * @param identifier the identifier to set
	 */
	public void setIdentifier(URI identifier) {
		this.identifier = identifier;
	}

	//Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPWS Module
//	public static GetMetadataMessage parse(MessageHeader header, ElementParser parser, Object helper) throws XmlPullParserException, IOException {
//		return null;
//	}
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		
//	}
//	/**
//	 * @param header
//	 * @param parser
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public static GetMetadataMessage parse(MessageHeader header, ElementParser parser, Object helper) throws XmlPullParserException, IOException {
//		GetMetadataMessage getMetadataMessage = new GetMetadataMessage(header);
//
//		getMetadataMessage.parseUnknownAttributes(parser);
//
//		while (parser.nextTag() != XmlPullParser.END_TAG) {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (MEXConstants.WSX_NAMESPACE_NAME.equals(namespace)) {
//				if (MEXConstants.WSX_ELEM_DIALECT.equals(name)) {
//					getMetadataMessage.setDialect(new URI(parser.nextText().trim()));
//				} else if (MEXConstants.WSX_ELEM_IDENTIFIER.equals(name)) {
//					getMetadataMessage.setIdentifier(new URI(parser.nextText().trim()));
//				} else {
//					getMetadataMessage.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				getMetadataMessage.parseUnknownElement(parser, namespace, name);
//			}
//		}
//		return getMetadataMessage;
//	}
//
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//		// Start-Tag
//		serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_GETMETADATA);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// Dialect adden
//		if (dialect != null) {
//			SerializeUtil.serializeTag(serializer, MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_DIALECT, (dialect.toString() == null ? "" : dialect.toString()));
//		}
//		// Identifier adden
//		if (identifier != null) {
//			SerializeUtil.serializeTag(serializer, MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_IDENTIFIER, (identifier.toString() == null ? "" : identifier.toString()));
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		// End-Tag
//		serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_GETMETADATA);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//	}
}

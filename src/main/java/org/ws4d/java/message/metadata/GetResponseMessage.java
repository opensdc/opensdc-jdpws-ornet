/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.message.metadata;

import org.ws4d.java.constants.WXFConstants;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.message.MessageHeaderBuilder;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.types.HostMData;
import org.ws4d.java.types.RelationshipMData;
import org.ws4d.java.types.ThisDeviceMData;
import org.ws4d.java.types.ThisModelMData;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.StringUtil;

/*
 * <?xml version='1.0' encoding='UTF-8' ?> <soap:Envelope
 * xmlns:un0="http://schemas.microsoft.com/windows/pnpx/2005/10"
 * xmlns:pub="http://schemas.microsoft.com/windows/pub/2005/07"
 * xmlns:wsdp="http://schemas.xmlsoap.org/ws/2006/02/devprof"
 * xmlns:soap="http://www.w3.org/2003/05/soap-envelope"
 * xmlns:wsx="http://schemas.xmlsoap.org/ws/2004/09/mex"
 * xmlns:wsa="http://schemas.xmlsoap.org/ws/2004/08/addressing"> <soap:Header>
 * <wsa
 * :To>http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous</wsa:To>
 * <
 * wsa:Action>http://schemas.xmlsoap.org/ws/2004/09/transfer/GetResponse</wsa:Action
 * >
 * <wsa:MessageID>urn:uuid:95fdc5f6-b856-4397-9b0c-5bd77669fded</wsa:MessageID>
 * <wsa:RelatesTo>urn:uuid:37836700-c845-11dd-bfa6-0d3a112efe2a</wsa:RelatesTo>
 * </soap:Header> <soap:Body> <wsx:Metadata> <wsx:MetadataSection
 * Dialect="http://schemas.xmlsoap.org/ws/2006/02/devprof/ThisDevice">
 * <wsdp:ThisDevice> <wsdp:FriendlyName>Microsoft Publication Service Device
 * Host</wsdp:FriendlyName> <wsdp:FirmwareVersion>1.0</wsdp:FirmwareVersion>
 * <wsdp:SerialNumber>20050718</wsdp:SerialNumber> </wsdp:ThisDevice>
 * </wsx:MetadataSection> <wsx:MetadataSection
 * Dialect="http://schemas.xmlsoap.org/ws/2006/02/devprof/ThisModel">
 * <wsdp:ThisModel> <wsdp:Manufacturer>Microsoft Corporation</wsdp:Manufacturer>
 * <wsdp:ManufacturerUrl>http://www.microsoft.com</wsdp:ManufacturerUrl>
 * <wsdp:ModelName>Microsoft Publication Service</wsdp:ModelName>
 * <wsdp:ModelNumber>1</wsdp:ModelNumber>
 * <wsdp:ModelUrl>http://www.microsoft.com</wsdp:ModelUrl>
 * <wsdp:PresentationUrl>http://www.microsoft.com</wsdp:PresentationUrl>
 * <un0:DeviceCategory
 * >{2BC7C4DF-D940-46f2-BCF2-CB7DADEE2D93}</un0:DeviceCategory>
 * </wsdp:ThisModel> </wsx:MetadataSection> <wsx:MetadataSection
 * Dialect="http://schemas.xmlsoap.org/ws/2006/02/devprof/Relationship">
 * <wsdp:Relationship Type="http://schemas.xmlsoap.org/ws/2006/02/devprof/host">
 * <wsdp:Host> <wsa:EndpointReference>
 * <wsa:Address>urn:uuid:45672533-4576-4911-8cec-92ed48faa748</wsa:Address>
 * </wsa:EndpointReference> <wsdp:Types>pub:Computer</wsdp:Types>
 * <wsdp:ServiceId
 * >urn:uuid:45672533-4576-4911-8cec-92ed48faa748</wsdp:ServiceId>
 * <pub:Computer>BUI-NB232/Domain:BUI</pub:Computer> </wsdp:Host>
 * </wsdp:Relationship> </wsx:MetadataSection> </wsx:Metadata> </soap:Body>
 * </soap:Envelope>
 */

public class GetResponseMessage extends Message {

	public static final URI		ACTION	= new URI(WXFConstants.WXF_ACTION_GETRESPONSE);

	private ThisModelMData		thisModel;

	private ThisDeviceMData		thisDevice;

	private RelationshipMData	relationship;

	/**
	 * Creates a new GetMetadataResponse message containing a {@link SOAPHeader}
	 * with the appropriate {@link SOAPHeader#getAction() action property} set.
	 * All other header- and transfer-related fields are empty and it is the
	 * caller's responsibility to fill them with suitable values.
	 */
	public GetResponseMessage() {
		this(MessageHeaderBuilder.getInstance().createHeader(WXFConstants.WXF_ACTION_GETRESPONSE));
	}

	/**
	 * @param header
	 */
	public GetResponseMessage(MessageHeader header) {
		super(header);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", thisModel=").append(thisModel);
		sb.append(", thisDevice=").append(thisDevice);
		sb.append(", relationship=").append(relationship);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return GET_RESPONSE_MESSAGE;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.metadata.GetResponseMessage #getThisDevice()
	 */
	public ThisDeviceMData getThisDevice() {
		return thisDevice;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.metadata.GetResponseMessage #getThisModel()
	 */
	public ThisModelMData getThisModel() {
		return thisModel;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.metadata.GetResponseMessage #getRelationship()
	 */
	public RelationshipMData getRelationship() {
		return relationship;
	}

	public HostMData getHost() {
		return relationship == null ? null : relationship.getHost();
	}

	public DataStructure getHosted() {
		return relationship == null ? null : relationship.getHosted();
	}

	/**
	 * @param thisModel the thisModel to set
	 */
	public void setThisModel(ThisModelMData thisModel) {
		this.thisModel = thisModel;
	}

	/**
	 * @param thisDevice the thisDevice to set
	 */
	public void setThisDevice(ThisDeviceMData thisDevice) {
		this.thisDevice = thisDevice;
	}

	/**
	 * @param relationship the relationship to set
	 */
	public void addRelationship(RelationshipMData relationship) {
		if (this.relationship == null) {
			this.relationship = relationship;
		} else {
			this.relationship.mergeWith(relationship);
		}
	}

	
	//Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPWS Module
//	public static GetResponseMessage parse(MessageHeader header, ElementParser parser, Object helper) throws XmlPullParserException, IOException, VersionMismatchException {
//		return null;
//	}
//	
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		
//	}
//	/**
//	 * @param header
//	 * @param parser
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 * @throws VersionMismatchException
//	 */
//	public static GetResponseMessage parse(MessageHeader header, ElementParser parser, Object helper) throws XmlPullParserException, IOException, VersionMismatchException {
//		GetResponseMessage getResponseMessage = new GetResponseMessage(header);
//
//		getResponseMessage.parseUnknownAttributes(parser);
//
//		int event = parser.nextTag();
//		if (event == XmlPullParser.END_TAG) {
//			throw new XmlPullParserException("GetResponse is empty");
//		}
//		do {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (MEXConstants.WSX_NAMESPACE_NAME.equals(namespace)) {
//				if (MEXConstants.WSX_ELEM_METADATASECTION.equals(name)) {
//					// get attribute Dialect and decide upon it
//					String dialect = parser.getAttributeValue(null, MEXConstants.WSX_ELEM_DIALECT);
//					if (helper.getMetadataDialectThisModel().equals(dialect)) {
//						getResponseMessage.setThisModel(ThisModelMData.parse(parser, helper));
//					} else if (helper.getMetadataDialectThisDevice().equals(dialect)) {
//						getResponseMessage.setThisDevice(ThisDeviceMData.parse(parser, helper));
//					} else if (helper.getMetatdataDialectRelationship().equals(dialect)) {
//						getResponseMessage.addRelationship(RelationshipMData.parse(parser, helper));
//					} else {
//						// unknown metadata dialect
//						getResponseMessage.parseUnknownElement(parser, namespace, name);
//					}
//				} else {
//					getResponseMessage.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				getResponseMessage.parseUnknownElement(parser, namespace, name);
//			}
//			event = parser.nextTag();
//		} while (event != XmlPullParser.END_TAG);
//		return getResponseMessage;
//	}
//
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//		// Start-Tag
//		serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATA);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// ThisModelMData adden
//		ThisModelMData thisModel = getThisModel();
//		if (thisModel != null) {
//			serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
//			serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, helper.getMetadataDialectThisModel());
//			thisModel.serialize(serializer, helper);
//			serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
//		}
//		// ThisDeviceMData adden
//		ThisDeviceMData thisDevice = getThisDevice();
//		if (thisDevice != null) {
//			serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
//			serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, helper.getMetadataDialectThisDevice());
//			thisDevice.serialize(serializer, helper);
//			serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
//		}
//		// RelationshipMData adden
//		RelationshipMData relationship = getRelationship();
//		if (relationship != null) {
//			serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
//			serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, helper.getMetatdataDialectRelationship());
//			relationship.serialize(serializer, helper);
//			serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		// End-Tag
//		serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATA);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//	}

}

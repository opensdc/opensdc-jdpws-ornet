/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.message;

import org.ws4d.java.constants.WSAConstants;
import org.ws4d.java.schema.PredefinedSchemaTypes;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.StringValue;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.util.StringUtil;

/**
 *
 */
public class FaultMessage extends Message {

//	public static final String	ACTION_ADDRESSING	= WSAConstants.WSA_ACTION_ADDRESSING_FAULT;
//
//	public static final String	ACTION_SOAP			= WSAConstants.WSA_ACTION_SOAP_FAULT;
//
//	public static final String	ACTION_DPWS			= DPWSConstants.DPWS_ACTION_DPWS_FAULT;
//
//	public static final String	ACTION_WSD			= WSDConstants.WSD_ACTION_WSD_FAULT;

	private QName				code;

	private QName				subcode;

	private QName				subsubcode;

	private DataStructure		reason;

	private IParameterValue		detail;


	public static final String TYPE_ADRESSING_FAULT="jmeds://faults/Adressing";
	
	public static final String TYPE_PROTOCOL_ACTION_FAULT="jmeds://faults/protocol/action";  //DPWSConstants.DPWS_ACTION_DPWS_FAULT
	
	public static final QName CODE_SENDER_FAULT=QNameFactory.getInstance().getQName("jmeds://faults/SF/");
	
	public static final QName CODE_RECEIVER_FAULT=QNameFactory.getInstance().getQName("jmeds://faults/RF/");

	public static final QName SUBCODE_ACTION_NOT_SUPPORTED=QNameFactory.getInstance().getQName("jmeds://faults/Adressing/ANS");
	
	public static final QName SUBCODE_INVALID_ADDRESSING_HEADER=QNameFactory.getInstance().getQName("jmeds://faults/Adressing/IAH");  //WSA_FAULT_INVALID_ADDRESSING_HEADER
	
	public static final QName SUBCODE_ENDPOINT_NOT_AVAILABLE=QNameFactory.getInstance().getQName("jmeds://faults/Adressing/ENA");

	public static final QName SUBCODE_EVENTING_FILTER_ACTION_NOT_SUPPORTED=QNameFactory.getInstance().getQName("jmeds://faults/Eventing/FANS");	//DPWSConstants.DPWS_FAULT_FILTER_ACTION_NOT_SUPPORTED
	


	// Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPWS Module
	public static FaultMessage createActionNotSupportedFault(Message request) {

		String inputAction = request.getAction().toString();
		/*
		 * create a SOAP Exception with code Sender and Subcode
		 * wsa:ActionNotSupported
		 */
		FaultMessage fault = new FaultMessage(MessageHeaderBuilder.getInstance().createHeader(TYPE_ADRESSING_FAULT));
		fault.setResponseTo(request);
		fault.setCode(CODE_SENDER_FAULT);
		// fill in subcode, reason and detail
		fault.setSubcode(SUBCODE_ACTION_NOT_SUPPORTED);
		LocalizedString reason = new LocalizedString("The endpoint at the specified address " + request.getTo() + " doesn't support the requested action " + inputAction + ".", null);
		fault.addReason(reason);

		IParameterValue detail = ParameterValue.createElementValue(PredefinedSchemaTypes.WSA_PROBLEM_ACTION);
		IParameterValue action = detail.get(WSAConstants.WSA_ELEM_ACTION);
		if (detail instanceof StringValue) {
			StringValue value = (StringValue) action;
			value.set(inputAction);
		}
		fault.setDetail(detail);
		return fault;
	}
	public static FaultMessage createEndpointUnavailableFault(Message message) {
				FaultMessage fault = new FaultMessage(MessageHeaderBuilder.getInstance().createHeader(TYPE_ADRESSING_FAULT));
				fault.setResponseTo(message);
		
				// send Fault wsa:EndpointUnavailable
				fault.setCode(CODE_RECEIVER_FAULT);
				fault.setSubcode(SUBCODE_ENDPOINT_NOT_AVAILABLE);
				LocalizedString reason = new LocalizedString("The endpoint at the specified address " + message.getTo() + " is unable to process the message at this time.", null);
				fault.addReason(reason);
				return fault;
	}
	//	public static FaultMessage createActionNotSupportedFault(Message request) {
	//		ConstantsHelper helper = DPWSUtil.getHelper(request.getVersion());
	//
	//		String inputAction = request.getAction().toString();
	//		/*
	//		 * create a SOAP Exception with code Sender and Subcode
	//		 * wsa:ActionNotSupported
	//		 */
	//		FaultMessage fault = new FaultMessage(helper.getWSAActionAddressingFault());
	//		fault.setResponseTo(request);
	//		fault.setCode(SOAPConstants.SOAP_FAULT_SENDER);
	//		// fill in subcode, reason and detail
	//		fault.setSubcode(helper.getWSAFaultActionNotSupported());
	//		LocalizedString reason = new LocalizedString("The endpoint at the specified address " + request.getTo() + " doesn't support the requested action " + inputAction + ".", null);
	//		fault.addReason(reason);
	//
	//		ParameterValue detail = ParameterValue.createElementValue(PredefinedSchemaTypes.WSA_PROBLEM_ACTION);
	//		ParameterValue action = detail.get(WSAConstants.WSA_ELEM_ACTION);
	//		if (detail.getValueType() == ParameterValue.TYPE_STRING) {
	//			StringValue value = (StringValue) action;
	//			value.set(inputAction);
	//		}
	//		fault.setDetail(detail);
	//		return fault;
	//	}
	//
	//	public static FaultMessage createEndpointUnavailableFault(Message message) {
	//		ConstantsHelper helper = DPWSUtil.getHelper(message.getVersion());
	//
	//		FaultMessage fault = new FaultMessage(helper.getWSAActionAddressingFault());
	//		fault.setResponseTo(message);
	//
	//		// send Fault wsa:EndpointUnavailable
	//		fault.setCode(SOAPConstants.SOAP_FAULT_RECEIVER);
	//		fault.setSubcode(helper.getWSAfaultEndpointUnavailable());
	//		LocalizedString reason = new LocalizedString("The endpoint at the specified address " + message.getTo() + " is unable to process the message at this time.", null);
	//		fault.addReason(reason);
	//		return fault;
	//	}

	/**
	 * Crates a new fault message with the given <code>action</code>, which is
	 * expected to be a valid absolute URI.
	 * 
	 * @param action the action URI of the fault message
	 */
	public FaultMessage(String action) {
		this(MessageHeaderBuilder.getInstance().createHeader(action));
	}

	/**
	 * @param header
	 */
	public FaultMessage(MessageHeader header) {
		this(header, null, null);
	}

	/**
	 * @param header
	 * @param code
	 * @param subcode
	 */
	public FaultMessage(MessageHeader header, QName code, QName subcode) {
		super(header);
		this.code = code;
		this.subcode = subcode;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", code=").append(code);
		sb.append(", subcode=").append(subcode);
		sb.append(", subsubcode=").append(subsubcode);
		sb.append(", reason=").append(reason);
		sb.append(", detail=").append(detail);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return FAULT_MESSAGE;
	}

	/**
	 * Returns the SOAP fault code.
	 * 
	 * @return the SOAP fault code
	 */
	public QName getCode() {
		return code;
	}

	/**
	 * Returns the SOAP fault subcode.
	 * 
	 * @return the SOAP fault subcode
	 */
	public QName getSubcode() {
		return subcode;
	}

	/**
	 * Returns the SOAP fault subsubcode.
	 * 
	 * @return the SOAP fault subsubcode
	 */
	public QName getSubsubcode() {
		return subsubcode;
	}

	/**
	 * Returns the list of reasons.
	 * 
	 * @return the list of reasons
	 */
	// list of LocalizedStrings
	public DataStructure getReason() {
		return reason;
	}

	/**
	 * Returns the SOAP fault detail.
	 * 
	 * @return the SOAP fault detail
	 */
	public IParameterValue getDetail() {
		return detail;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(QName code) {
		this.code = code;
	}

	/**
	 * @param subcode the subcode to set
	 */
	public void setSubcode(QName subcode) {
		this.subcode = subcode;
	}

	/**
	 * @param subsubcode the subsubcode to set
	 */
	public void setSubsubcode(QName subsubcode) {
		this.subsubcode = subsubcode;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(DataStructure reason) {
		this.reason = reason;
	}

	/**
	 * @param detail the detail to set
	 */
	public void setDetail(IParameterValue detail) {
		this.detail = detail;
	}

	public void addReason(LocalizedString reason) {
		if (this.reason == null) {
			this.reason = new ArrayList();
		}
		this.reason.add(reason);
	}


	//Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPWS Module
	//	public static FaultMessage parse(String actionName, MessageHeader header, ElementParser parser, IMessageEndpoint op) throws XmlPullParserException, IOException {
	//		return null;
	//	}
	//	
	//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
	//		
	//	}
	/**
	 * @param header
	 * @param parser
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	//	public static FaultMessage parse(String actionName, SOAPHeader header, ElementParser parser, OperationDescription op) throws XmlPullParserException, IOException {
	//		FaultMessage faultMessage = new FaultMessage(header);
	//
	//		Iterator it = op.getFaults();
	//		Fault fault = null;
	//		while (it.hasNext()) {
	//			fault = (Fault) it.next();
	//			if (actionName != null && actionName.equals(fault.getAction())) {
	//				break;
	//			}
	//		}
	//		parser.handleUnknownAttributes(faultMessage);
	//
	//		int event = parser.nextTag();
	//		if (event == XmlPullParser.END_TAG) {
	//			throw new XmlPullParserException("Fault is empty");
	//		}
	//		do {
	//			String namespace = parser.getNamespace();
	//			String name = parser.getName();
	//			if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace)) {
	//				if (SOAPConstants.SOAP_ELEM_CODE.equals(name)) {
	//					parseCode(faultMessage, parser);
	//				} else if (SOAPConstants.SOAP_ELEM_REASON.equals(name)) {
	//					faultMessage.setReason(nextReason(parser));
	//				} else if (SOAPConstants.SOAP_ELEM_DETAIL.equals(name)) {
	//					// go to content of soap:Detail
	//					if (parser.getEventType() == XmlPullParser.START_TAG) {
	//						if (fault != null) {
	//							parser.nextTag();
	//							faultMessage.setDetail(ParameterDefinition.parse(parser, fault.getElement()));
	//						} else {
	//							parser.addUnknownElement(faultMessage, namespace, name);
	//						}
	//
	//					}
	//					// parser.nextTag();
	//				} else {
	//					parser.addUnknownElement(faultMessage, namespace, name);
	//				}
	//			} else {
	//				parser.addUnknownElement(faultMessage, namespace, name);
	//			}
	//			event = parser.nextTag();
	//		} while (event != XmlPullParser.END_TAG);
	//		return faultMessage;
	//	}
	//
	//	private static void parseCode(FaultMessage faultMessage, ElementParser parser) throws XmlPullParserException, IOException {
	//		int event = parser.nextTag();
	//		if (event == XmlPullParser.END_TAG) {
	//			throw new XmlPullParserException("Code is empty");
	//		}
	//		do {
	//			String namespace = parser.getNamespace();
	//			String name = parser.getName();
	//			if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace)) {
	//				if (SOAPConstants.SOAP_ELEM_VALUE.equals(name)) {
	//					faultMessage.setCode(parser.nextQName());
	//				} else if (SOAPConstants.SOAP_ELEM_SUBCODE.equals(name)) {
	//					int event2 = parser.nextTag();
	//					if (event2 == XmlPullParser.END_TAG) {
	//						throw new XmlPullParserException("Subcode is empty");
	//					}
	//					do {
	//						String namespace2 = parser.getNamespace();
	//						String name2 = parser.getName();
	//						if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace2)) {
	//							if (SOAPConstants.SOAP_ELEM_VALUE.equals(name2)) {
	//								faultMessage.setSubcode(parser.nextQName());
	//							} else if (SOAPConstants.SOAP_ELEM_SUBCODE.equals(name)) {
	//								int event3 = parser.nextTag();
	//								if (event3 == XmlPullParser.END_TAG) {
	//									throw new XmlPullParserException("Subcode is empty");
	//								}
	//								do {
	//									String namespace3 = parser.getNamespace();
	//									String name3 = parser.getName();
	//									if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace3)) {
	//										if (SOAPConstants.SOAP_ELEM_VALUE.equals(name3)) {
	//											faultMessage.setSubsubcode(parser.nextQName());
	//										} else if (SOAPConstants.SOAP_ELEM_SUBCODE.equals(name3)) {
	//											// void, enough recursion
	//										}
	//									}
	//									event3 = parser.nextTag();
	//								} while (event3 != XmlPullParser.END_TAG);
	//							}
	//						}
	//						event2 = parser.nextTag();
	//					} while (event2 != XmlPullParser.END_TAG);
	//				}
	//			}
	//			event = parser.nextTag();
	//		} while (event != XmlPullParser.END_TAG);
	//	}
	//
	//	private static DataStructure nextReason(ElementParser parser) throws XmlPullParserException, IOException {
	//		List reason = new ArrayList();
	//		int event = parser.nextTag();
	//		if (event == XmlPullParser.END_TAG) {
	//			throw new XmlPullParserException("Reason is empty");
	//		}
	//		do {
	//			String namespace = parser.getNamespace();
	//			String name = parser.getName();
	//			if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace)) {
	//				if (SOAPConstants.SOAP_ELEM_TEXT.equals(name)) {
	//					reason.add(parser.nextLocalizedString());
	//				}
	//			}
	//			event = parser.nextTag();
	//		} while (event != XmlPullParser.END_TAG);
	//		return reason;
	//	}
	//
	//	public void serialize(XmlSerializer serializer, ConstantsHelper helper) throws IOException {
	//		// ################## Body-StartTag ##################
	//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	//		// Fault-StartTag
	//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_FAULT);
	//		// Adds UnknownAttributes
	//		serializeUnknownAttributes(serializer);
	//		// Code
	//		QName code = getCode();
	//		if (code != null) {
	//			serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_CODE);
	//			// Valueelement
	//			serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_VALUE);
	//			String prefix = serializer.getPrefix(code.getNamespace(), true);
	//			serializer.text(prefix + ":" + code.getLocalPart());
	//			serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_VALUE);
	//			// Subcode
	//			QName subcode = getSubcode();
	//			if (subcode != null) {
	//				serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_SUBCODE);
	//				// Valueelement
	//				serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_VALUE);
	//				String prefix1 = serializer.getPrefix(subcode.getNamespace(), true);
	//				serializer.text(prefix1 + ":" + subcode.getLocalPart());
	//				serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_VALUE);
	//				// Subsubcode
	//				QName subsubcode = getSubsubcode();
	//				if (subsubcode != null) {
	//					serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_SUBCODE);
	//					// Valueelement
	//					serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_VALUE);
	//					String prefix2 = serializer.getPrefix(subsubcode.getNamespace(), true);
	//					serializer.text(prefix2 + ":" + subsubcode.getLocalPart());
	//					serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_VALUE);
	//					serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_SUBCODE);
	//				}
	//				serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_SUBCODE);
	//			}
	//			serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_CODE);
	//		}
	//		// Reason
	//		DataStructure reason = getReason();
	//		if (reason != null) {
	//			ArrayList list = (ArrayList) reason;
	//			serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_REASON);
	//			for (Iterator it = list.iterator(); it.hasNext();) {
	//				LocalizedString string = (LocalizedString) it.next();
	//				SerializeUtil.serializeTagWithAttribute(serializer, SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_TEXT, string.getValue(), XMLConstants.XML_NAMESPACE_NAME, XMLConstants.XML_ATTRIBUTE_LANGUAGE, string.getLanguage());
	//			}
	//			serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_REASON);
	//		}
	//		ParameterValue detail = getDetail();
	//		if (detail != null) {
	//			serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_DETAIL);
	//			detail.serialize(serializer);
	//			serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_DETAIL);
	//		}
	//		// Adds UnknownElements
	//		serializeUnknownElements(serializer);
	//		// End-Tag
	//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_FAULT);
	//		// ################## BODY-EndTag ##################
	//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	//	}
}

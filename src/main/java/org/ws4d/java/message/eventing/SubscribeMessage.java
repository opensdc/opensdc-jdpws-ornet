/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.message.eventing;

import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.message.MessageHeaderBuilder;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.Delivery;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.Filter;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.StringUtil;

public class SubscribeMessage extends Message {

	public static final URI		ACTION	= new URI(WSEConstants.WSE_ACTION_SUBSCRIBE);

	private EndpointReference	endTo;

	private Delivery			delivery;

	private String				expires;

	private Filter				filter;
	
	private Iterator			bindingCandidatesForNotification;

	/**
	 * Creates a new Subscribe message containing a {@link SOAPHeader} with the
	 * appropriate {@link SOAPHeader#getAction() action property} set and a
	 * unique {@link SOAPHeader#getMessageId() message ID property}. All other
	 * header- and eventing-related fields are empty and it is the caller's
	 * responsibility to fill them with suitable values.
	 */
	public SubscribeMessage() {
		this(MessageHeaderBuilder.getInstance().createRequestHeader(WSEConstants.WSE_ACTION_SUBSCRIBE));
	}

	/**
	 * @param header
	 */
	public SubscribeMessage(MessageHeader header) {
		this(header, null);
	}

	/**
	 * @param header
	 * @param delivery
	 */
	public SubscribeMessage(MessageHeader header, Delivery delivery) {
		super(header);
		this.delivery = delivery;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", endTo=").append(endTo);
		sb.append(", delivery=").append(delivery);
		sb.append(", expires=").append(expires);
		sb.append(", filter=").append(filter);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return SUBSCRIBE_MESSAGE;
	}

	public Delivery getDelivery() {
		return delivery;
	}

	public EndpointReference getEndTo() {
		return endTo;
	}

	public Filter getFilter() {
		return filter;
	}

	/**
	 * @param endTo the endTo to set
	 */
	public void setEndTo(EndpointReference endTo) {
		this.endTo = endTo;
	}

	/**
	 * @param delivery the delivery to set
	 */
	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	/**
	 * @return the expires
	 */
	public String getExpires() {
		return expires;
	}

	/**
	 * @param expires the expires to set
	 */
	public void setExpires(String expires) {
		this.expires = expires;
	}

	/**
	 * @param filter the filter to set
	 */
	public void setFilter(Filter filter) {
		this.filter = filter;
	}
	

	/**
	 * @param bindings
	 * @return
	 */
	public Iterator setBindingCandidatesForNotification(Iterator bindings) {
		return bindingCandidatesForNotification = bindings;
	}

	public Iterator getBindingCandidatesForNotification() {
		return bindingCandidatesForNotification;
	}
	
	// Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPPWS Module
//	public static SubscribeMessage parse(MessageHeader header, ElementParser parser) throws XmlPullParserException, IOException 
//	{
//		return null;
//	}
//	
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		
//	}
//	/**
//	 * @param header
//	 * @param parser
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public static SubscribeMessage parse(MessageHeader header, ElementParser parser) throws XmlPullParserException, IOException {
//		SubscribeMessage subscribeMessage = new SubscribeMessage(header);
//
//		subscribeMessage.parseUnknownAttributes(parser);
//
//		int event = parser.nextTag();
//		if (event == XmlPullParser.END_TAG) {
//			throw new XmlPullParserException("Subscribe is empty");
//		}
//		do {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
//				if (WSEConstants.WSE_ELEM_ENDTO.equals(name)) {
//					subscribeMessage.setEndTo(DPWSUtil.parseEndpointReference(((DPWSProtocolVersionInfo)header.getVersion()).getDpwsVersion(), parser));
//				} else if (WSEConstants.WSE_ELEM_DELIVERY.equals(name)) {
//					subscribeMessage.setDelivery(Delivery.parse(parser,DPWSUtil.getHelper(header.getVersion())));
//				} else if (WSEConstants.WSE_ELEM_EXPIRES.equals(name)) {
//					subscribeMessage.setExpires(parser.nextText());
//				} else if (WSEConstants.WSE_ELEM_FILTER.equals(name)) {
//					subscribeMessage.setFilter(Filter.parse(parser));
//				} else {
//					subscribeMessage.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				subscribeMessage.parseUnknownElement(parser, namespace, name);
//			}
//			event = parser.nextTag();
//		} while (event != XmlPullParser.END_TAG);
//		return subscribeMessage;
//	}
//
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//		// Start-Tag
//		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_SUBSCRIBE);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// EndTo
//
//		if (endTo != null) {
//			endTo.serialize(serializer, helper, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_ENDTO);
//		}
//		// Delivery
//		if (delivery != null) {
//			delivery.serialize(serializer, helper);
//		}
//		// Expires
//		if (expires != null && !(expires.equals(""))) {
//			SerializeUtil.serializeTag(serializer, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_EXPIRES, expires);
//		}
//		// Filter
//		if (filter != null) {
//			filter.serialize(serializer, helper);
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		// End-Tag
//		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_SUBSCRIBE);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//	}

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.message.eventing;

import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.message.MessageHeaderBuilder;
import org.ws4d.java.types.URI;

public class GetStatusResponseMessage extends EventingResponseMessage {

	public static final URI	ACTION	= new URI(WSEConstants.WSE_ACTION_GETSTATUSRESPONSE);

	/**
	 * Creates a new GetStatusResponse message containing a {@link SOAPHeader}
	 * with the appropriate {@link SOAPHeader#getAction() action property} set.
	 * All other header- and eventing-related fields are empty and it is the
	 * caller's responsibility to fill them with suitable values.
	 */
	public GetStatusResponseMessage() {
		this(MessageHeaderBuilder.getInstance().createHeader(WSEConstants.WSE_ACTION_GETSTATUSRESPONSE));
	}

	/**
	 * @param header
	 */
	public GetStatusResponseMessage(MessageHeader header) {
		super(header);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return GET_STATUS_RESPONSE_MESSAGE;
	}

	
	// Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPWS Module
//	public static GetStatusResponseMessage parse(MessageHeader header, ElementParser parser) throws XmlPullParserException, IOException {
//		return null;
//	}
//	
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		
//	}
//	
//	/**
//	 * @param header
//	 * @param parser
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public static GetStatusResponseMessage parse(MessageHeader header, ElementParser parser) throws XmlPullParserException, IOException {
//		GetStatusResponseMessage getStatusResponseMessage = new GetStatusResponseMessage(header);
//
//		while (parser.nextTag() != XmlPullParser.END_TAG) {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
//				if (WSEConstants.WSE_ELEM_EXPIRES.equals(name)) {
//					getStatusResponseMessage.setExpires(parser.nextText());
//				} else {
//					getStatusResponseMessage.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				getStatusResponseMessage.parseUnknownElement(parser, namespace, name);
//			}
//		}
//		return getStatusResponseMessage;
//	}
//
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//		// Start-Tag
//		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_GETSTATUSRESPONSE);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// Expires
//		if (expires != null && !(expires.equals(""))) {
//			SerializeUtil.serializeTag(serializer, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_EXPIRES, expires);
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		// End-Tag
//		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_GETSTATUSRESPONSE);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//	}
}

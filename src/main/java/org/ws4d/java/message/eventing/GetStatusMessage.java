/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.message.eventing;

import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.message.MessageHeaderBuilder;
import org.ws4d.java.types.URI;

public class GetStatusMessage extends Message {

	public static final URI	ACTION	= new URI(WSEConstants.WSE_ACTION_GETSTATUS);

	/**
	 * Creates a new GetStatus message containing a {@link SOAPHeader} with the
	 * appropriate {@link SOAPHeader#getAction() action property} set and a
	 * unique {@link SOAPHeader#getMessageId() message ID property}. All other
	 * header- and eventing-related fields are empty and it is the caller's
	 * responsibility to fill them with suitable values.
	 */
	public GetStatusMessage() {
		this(MessageHeaderBuilder.getInstance().createRequestHeader(WSEConstants.WSE_ACTION_GETSTATUS));
	}

	/**
	 * @param header
	 */
	public GetStatusMessage(MessageHeader header) {
		super(header);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return GET_STATUS_MESSAGE;
	}

	//Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPWS Module
//	public static GetStatusMessage parse(MessageHeader header, ElementParser parser) throws XmlPullParserException, IOException {
//		return null;
//	}	
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		
//	}
//	/**
//	 * @param header
//	 * @param parser
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public static GetStatusMessage parse(MessageHeader header, ElementParser parser) throws XmlPullParserException, IOException {
//		GetStatusMessage getStatusMessage = new GetStatusMessage(header);
//		parser.nextGenericElement(getStatusMessage);
//		return getStatusMessage;
//	}
//
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//		// Start-Tag
//		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_GETSTATUS);
//		// Do Nothing because its in the specification defined
//		// End-Tag
//		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_GETSTATUS);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//
//	}

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.message.eventing;

import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.message.MessageHeaderBuilder;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.StringUtil;

public class SubscribeResponseMessage extends EventingResponseMessage {

	public static final URI		ACTION	= new URI(WSEConstants.WSE_ACTION_SUBSCRIBERESPONSE);

	private EndpointReference	subscriptionManager;

	/**
	 * Creates a new SubscribeResponse message containing a {@link SOAPHeader}
	 * with the appropriate {@link SOAPHeader#getAction() action property} set.
	 * All other header- and eventing-related fields are empty and it is the
	 * caller's responsibility to fill them with suitable values.
	 */
	public SubscribeResponseMessage() {
		this(MessageHeaderBuilder.getInstance().createHeader(WSEConstants.WSE_ACTION_SUBSCRIBERESPONSE));
	}

	/**
	 * @param header
	 */
	public SubscribeResponseMessage(MessageHeader header) {
		this(header, null, null);
	}

	/**
	 * @param header
	 * @param subscriptionManager
	 * @param expires
	 */
	public SubscribeResponseMessage(MessageHeader header, EndpointReference subscriptionManager, String expires) {
		super(header);
		this.subscriptionManager = subscriptionManager;
		this.expires = expires;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", subscriptionManager=").append(subscriptionManager);
		sb.append(", expires=").append(expires);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return SUBSCRIBE_RESPONSE_MESSAGE;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.eventing.SubscribeResponseMessage
	 * #getSubscriptionManager()
	 */
	public EndpointReference getSubscriptionManager() {
		return subscriptionManager;
	}

	/**
	 * @param subscriptionManager the subscriptionManager to set
	 */
	public void setSubscriptionManager(EndpointReference subscriptionManager) {
		this.subscriptionManager = subscriptionManager;
	}

	// Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPWS Module
//	public static SubscribeResponseMessage parse(MessageHeader header, ElementParser parser) throws XmlPullParserException, IOException {
//		return null;
//	}
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		
//	}
//	/**
//	 * @param header
//	 * @param parser
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public static SubscribeResponseMessage parse(MessageHeader header, ElementParser parser) throws XmlPullParserException, IOException {
//		SubscribeResponseMessage subscribeResponseMessage = new SubscribeResponseMessage(header);
//
//		subscribeResponseMessage.parseUnknownAttributes(parser);
//
//		int event = parser.nextTag();
//		if (event == XmlPullParser.END_TAG) {
//			throw new XmlPullParserException("SubscribeResponse is empty");
//		}
//		do {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
//				if (WSEConstants.WSE_ELEM_SUBSCRIPTIONMANAGER.equals(name)) {
//					subscribeResponseMessage.setSubscriptionManager(DPWSUtil.parseEndpointReference(((DPWSProtocolVersionInfo)header.getVersion()).getDpwsVersion(), parser));
//				} else if (WSEConstants.WSE_ELEM_EXPIRES.equals(name)) {
//					subscribeResponseMessage.setExpires(parser.nextText());
//				} else {
//					subscribeResponseMessage.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				subscribeResponseMessage.parseUnknownElement(parser, namespace, name);
//			}
//			event = parser.nextTag();
//		} while (event != XmlPullParser.END_TAG);
//		return subscribeResponseMessage;
//	}
//
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//		// Start-Tag
//		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_SUBSCRIBERESPONSE);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// Subscripton Manager
//		if (subscriptionManager != null) {
//			subscriptionManager.serialize(serializer, helper, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_SUBSCRIPTIONMANAGER);
//		}
//		// Expires
//		if (expires != null && !(expires.equals(""))) {
//			SerializeUtil.serializeTag(serializer, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_EXPIRES, expires);
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		// End-Tag
//		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_SUBSCRIBERESPONSE);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//	}
}

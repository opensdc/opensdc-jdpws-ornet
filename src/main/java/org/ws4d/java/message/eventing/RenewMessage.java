/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.message.eventing;

import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.message.MessageHeaderBuilder;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.StringUtil;

public class RenewMessage extends Message {

	public static final URI	ACTION	= new URI(WSEConstants.WSE_ACTION_RENEW);

	private String			expires;

	/**
	 * Creates a new Renew message containing a {@link SOAPHeader} with the
	 * appropriate {@link SOAPHeader#getAction() action property} set and a
	 * unique {@link SOAPHeader#getMessageId() message ID property}. All other
	 * header- and eventing-related fields are empty and it is the caller's
	 * responsibility to fill them with suitable values.
	 */
	public RenewMessage() {
		this(MessageHeaderBuilder.getInstance().createRequestHeader(WSEConstants.WSE_ACTION_RENEW));
	}

	/**
	 * @param header
	 */
	public RenewMessage(MessageHeader header) {
		super(header);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", expires=").append(expires);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return RENEW_MESSAGE;
	}

	/**
	 * @return the expires
	 */
	public String getExpires() {
		return expires;
	}

	/**
	 * @param expires the expires to set
	 */
	public void setExpires(String expires) {
		this.expires = expires;
	}

	// Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPPWS Module
//	public static RenewMessage parse(MessageHeader header, ElementParser parser) throws XmlPullParserException, IOException {
//		return null;
//	}
//	
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		
//	}
//	
//	/**
//	 * @param header
//	 * @param parser
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public static RenewMessage parse(MessageHeader header, ElementParser parser) throws XmlPullParserException, IOException {
//		RenewMessage renewMessage = new RenewMessage(header);
//		renewMessage.parseUnknownAttributes(parser);
//
//		while (parser.nextTag() != XmlPullParser.END_TAG) {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
//				if (WSEConstants.WSE_ELEM_EXPIRES.equals(name)) {
//					renewMessage.setExpires(parser.nextText());
//				} else {
//					renewMessage.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				renewMessage.parseUnknownElement(parser, namespace, name);
//			}
//		}
//		return renewMessage;
//	}
//
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//		// Start-Tag
//		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_RENEW);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// Expires
//		if (expires != null && !(expires.equals(""))) {
//			SerializeUtil.serializeTag(serializer, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_EXPIRES, expires);
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		// End-Tag
//		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_RENEW);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//	}
}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.message.eventing;

import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.message.MessageHeaderBuilder;
import org.ws4d.java.types.URI;

public class RenewResponseMessage extends EventingResponseMessage {

	public static final URI	ACTION	= new URI(WSEConstants.WSE_ACTION_RENEWRESPONSE);

	/**
	 * Creates a new RenewResponse message containing a {@link SOAPHeader} with
	 * the appropriate {@link SOAPHeader#getAction() action property} set. All
	 * other header- and eventing-related fields are empty and it is the
	 * caller's responsibility to fill them with suitable values.
	 */
	public RenewResponseMessage() {
		this(MessageHeaderBuilder.getInstance().createHeader(WSEConstants.WSE_ACTION_RENEWRESPONSE));
	}

	/**
	 * @param header
	 */
	public RenewResponseMessage(MessageHeader header) {
		super(header);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return RENEW_RESPONSE_MESSAGE;
	}

	//Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPPWS Module
//	public static RenewResponseMessage parse(MessageHeader header, ElementParser parser) throws XmlPullParserException, IOException {
//		return null;
//	}
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		
//	}
//	/**
//	 * @param header
//	 * @param parser
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public static RenewResponseMessage parse(MessageHeader header, ElementParser parser) throws XmlPullParserException, IOException {
//		RenewResponseMessage renewResponseMessage = new RenewResponseMessage(header);
//
//		while (parser.nextTag() != XmlPullParser.END_TAG) {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
//				if (WSEConstants.WSE_ELEM_EXPIRES.equals(name)) {
//					renewResponseMessage.setExpires(parser.nextText());
//				} else {
//					renewResponseMessage.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				renewResponseMessage.parseUnknownElement(parser, namespace, name);
//			}
//		}
//		return renewResponseMessage;
//	}
//
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//		// Start-Tag
//		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_RENEWRESPONSE);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// Expires
//		if (expires != null && !(expires.equals(""))) {
//			SerializeUtil.serializeTag(serializer, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_EXPIRES, expires);
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		// End-Tag
//		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_RENEWRESPONSE);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//	}
}

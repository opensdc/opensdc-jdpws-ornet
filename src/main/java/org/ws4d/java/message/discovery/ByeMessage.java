/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.message.discovery;

import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.types.DiscoveryData;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.URI;

public class ByeMessage extends DiscoveryMessage {

	public static final URI	ACTION	= new URI(WSDConstants.WSD_ACTION_BYE);

	public static MessageHeader createByeHeader() {
		return createDiscoveryHeader(WSDConstants.WSD_ACTION_BYE);
	}

	/**
	 * Creates a new Bye message containing a {@link SOAPHeader} with the
	 * appropriate {@link SOAPHeader#getAction() action property} set, the
	 * default {@link SOAPHeader#getTo() to property} for ad-hoc mode (
	 * {@link WSDConstants#WSD_TO}) and a unique
	 * {@link SOAPHeader#getMessageId() message ID property}. All other header-
	 * and discovery-related fields are empty and it is the caller's
	 * responsibility to fill them with suitable values.
	 */
	public ByeMessage() {
		this(createByeHeader(), new DiscoveryData());
	}

	/**
	 * @param header
	 */
	public ByeMessage(MessageHeader header) {
		this(header, null);
	}
	

	public ByeMessage(DiscoveryData discoveryData) {
		super(createByeHeader(), discoveryData);
	}

	/**
	 * @param header
	 * @param endpointReference
	 * @param metadataVersion
	 */
	public ByeMessage(MessageHeader header, EndpointReference endpointReference, long metadataVersion) {
		this(header, new DiscoveryData(endpointReference, metadataVersion));
	}

	/**
	 * @param header
	 * @param discoveryData
	 */
	public ByeMessage(MessageHeader header, DiscoveryData discoveryData) {
		super(header, discoveryData);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return BYE_MESSAGE;
	}

	// Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPWS Module
//	public static ByeMessage parse(MessageHeader header, ElementParser parser, ProtocolData protocolData, Object helper) throws XmlPullParserException, IOException {
//		return null;
//	}
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		
//	}
//	/**
//	 * Parse the next ByeMessage
//	 * 
//	 * @param header
//	 * @param parser
//	 * @param protocolData
//	 * @param helper
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public static ByeMessage parse(MessageHeader header, ElementParser parser, ProtocolData protocolData, Object helper) throws XmlPullParserException, IOException {
//	
////	public static ByeMessage parse(MessageHeader header, ElementParser parser, ProtocolData protocolData, Object helper) throws XmlPullParserException, IOException {
//		DiscoveryData discoveryData = new DiscoveryData();
//		discoveryData.parse(parser, WSDConstants.WSD_ELEMENT_BYE, helper);
//		ByeMessage bye = new ByeMessage(header, discoveryData);
//
//		if (bye.getHeader().getSignature() != null) {
//			String[] potentialAlia = new String[bye.getXAddrs().size()];
//			Iterator iter = bye.getXAddrs().iterator();
//			for (int i = 0; iter.hasNext(); i++) {
//				potentialAlia[i] = iter.next().toString();
//			}
//			
//			//bye.getHeader().setValidated(DPWSFramework.getSecurityManager().validateMessage(bye.getHeader().getSignature(), protocolData, bye.getEndpointReference(), potentialAlia));
//		}
//		return bye;
//	}
//
//	/**
//	 * Serialize the ByeMessage to an SOAP-Document.
//	 */
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//
//		// Canonicalize and serialize this element
//		if (DPWSFramework.hasModule(DPWSFramework.DPWS_SECURITY_MODULE)) serializer.attribute("", "ID", "BID1");
//		
//		// Start-Tag
//		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_BYE);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// Discovery Data adden
//		getDiscoveryData().serialize(serializer, helper);
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		// END-Tag
//		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_BYE);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//
//	}

}

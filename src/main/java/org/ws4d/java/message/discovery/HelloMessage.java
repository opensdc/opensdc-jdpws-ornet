/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.message.discovery;

import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.types.DiscoveryData;
import org.ws4d.java.types.URI;

/**
 * 
 *
 */
public class HelloMessage extends DiscoveryMessage {

	public URI	ACTION	= new URI(WSDConstants.WSD_ACTION_HELLO);

	private static MessageHeader createHelloHeader() {
		return createDiscoveryHeader(WSDConstants.WSD_ACTION_HELLO);
	}

	/**
	 * Creates a new Hello message containing a {@link SOAPHeader} with the
	 * appropriate {@link SOAPHeader#getAction() action property} set, the
	 * default {@link SOAPHeader#getTo() to property} for ad-hoc mode (
	 * {@link WSDConstants#WSD_TO}) and a unique
	 * {@link SOAPHeader#getMessageId() message ID property}. All other header-
	 * and discovery-related fields are empty and it is the caller's
	 * responsibility to fill them with suitable values.
	 */
	public HelloMessage() {
		this(new DiscoveryData());
	}

	/**
	 * @param discoveryData
	 */
	public HelloMessage(DiscoveryData discoveryData) {
		this(createHelloHeader(), discoveryData);
	}

	/**
	 * @param header
	 * @param discoveryData
	 */
	public HelloMessage(MessageHeader header, DiscoveryData discoveryData) {
		super(header, discoveryData);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return HELLO_MESSAGE;
	}

	// Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPWS Module
//	/**
//	 * Parse the next HelloMessage
//	 * 
//	 * @param parser
//	 * @param newHeader
//	 * @param protocolData
//	 * @param helper
//	 * @return
//	 * @throws IOException
//	 * @throws XmlPullParserException
//	 */
//	public static HelloMessage parse(SOAPHeader header, ElementParser parser, ProtocolData protocolData, Object helper) throws IOException, XmlPullParserException {
//		DiscoveryData discoveryData = new DiscoveryData();
//		discoveryData.parse(parser, WSDConstants.WSD_ELEMENT_HELLO, helper);
//		HelloMessage hello = new HelloMessage(header, discoveryData);
//		if (hello.getHeader().getSignature() != null) {
//			String[] potentialAlia = new String[hello.getXAddrs().size()];
//			Iterator iter = hello.getXAddrs().iterator();
//			for (int i = 0; iter.hasNext(); i++) {
//				potentialAlia[i] = iter.next().toString();
//			}
//			hello.getHeader().setValidated(DPWSFramework.getSecurityManager().validateMessage(hello.getHeader().getSignature(), protocolData, hello.getEndpointReference(), potentialAlia));
//		}
//		return hello;
//	}
//
//	/**
//	 * Serialize the next HelloMessage to an SOAP-Document.
//	 */
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//
//		// Canonicalize and serialize this element
//		if (DPWSFramework.hasModule(DPWSFramework.DPWS_SECURITY_MODULE)) serializer.attribute("", "ID", "BID1");
//
//		// Start-Tag
//		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_HELLO);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// Discovery Data adden
//		getDiscoveryData().serialize(serializer, helper);
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		// End-Tag
//		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_HELLO);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//	}
}

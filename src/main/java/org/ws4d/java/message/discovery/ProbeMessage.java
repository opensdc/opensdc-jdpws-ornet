/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.message.discovery;

import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.types.ProbeScopeSet;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.StringUtil;

public class ProbeMessage extends Message {

	public static final URI	ACTION	= new URI(WSDConstants.WSD_ACTION_PROBE);

	private QNameSet		types;

	private ProbeScopeSet	scopes;

	private boolean			directed;

	public static MessageHeader createProbeHeader() {
		return DiscoveryMessage.createDiscoveryHeader(WSDConstants.WSD_ACTION_PROBE);
	}

	/**
	 * Creates a new Probe message containing a {@link SOAPHeader} with the
	 * appropriate {@link SOAPHeader#getAction() action property} set, the
	 * default {@link SOAPHeader#getTo() to property} for ad-hoc mode (
	 * {@link WSDConstants#WSD_TO}) and a unique
	 * {@link SOAPHeader#getMessageId() message ID property}. All other header-
	 * and discovery-related fields are empty and it is the caller's
	 * responsibility to fill them with suitable values.
	 */
	public ProbeMessage() {
		this(createProbeHeader());
	}

	/**
	 * @param header
	 */
	public ProbeMessage(MessageHeader header) {
		super(header);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", directed=").append(directed);
		sb.append(", types=").append(types);
		sb.append(", scopes=").append(scopes);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return PROBE_MESSAGE;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.discovery.ProbeMessage#getScopes()
	 */
	public ProbeScopeSet getScopes() {
		return scopes;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.discovery.ProbeMessage#getTypes()
	 */
	public QNameSet getTypes() {
		return types;
	}

	/**
	 * @param types the types to set
	 */
	public void setTypes(QNameSet types) {
		this.types = types;
	}

	/**
	 * @param scopes the scopes to set
	 */
	public void setScopes(ProbeScopeSet scopes) {
		this.scopes = scopes;
	}

	/**
	 * @return the directed
	 */
	public boolean isDirected() {
		return directed;
	}

	/**
	 * @param directed the directed to set
	 */
	public void setDirected(boolean directed) {
		this.directed = directed;
	}

	// Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPWS Module
//	public static ProbeMessage parse(MessageHeader header, ElementParser parser, Object helper) throws XmlPullParserException, IOException {
//		return null;
//	}
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		
//	}
//	
//	/**
//	 * Parse the next ProbeMessage
//	 * 
//	 * @param header
//	 * @param parser
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public static ProbeMessage parse(MessageHeader header, ElementParser parser, Object helper) throws XmlPullParserException, IOException {
//		ProbeMessage probeMessage = new ProbeMessage(header);
//
//		probeMessage.parseUnknownAttributes(parser);
//
//		while (parser.nextTag() != XmlPullParser.END_TAG) {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (helper.getWSDNamespace().equals(namespace)) {
//				if (WSDConstants.WSD_ELEMENT_TYPES.equals(name)) {
//					probeMessage.setTypes(QNameSet.parse(parser));
//					DefaultDPWSUtil2006.changeIncomingProbe(probeMessage);
//				} else if (WSDConstants.WSD_ELEMENT_SCOPES.equals(name)) {
//					probeMessage.setScopes(ProbeScopeSet.parseNextProbeScopeSet(parser));
//				} else {
//					probeMessage.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				probeMessage.parseUnknownElement(parser, namespace, name);
//			}
//		}
//		return probeMessage;
//	}
//
//	/**
//	 * Serialize the next ProbeMessage to an SOAP-Document.
//	 */
//	public void serialize(XmlSerializer serializer, ConstantsHelper helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//		// Start-Tag
//		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_PROBE);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// QNameSet types
//		if (types != null) {
//			types.serialize(serializer, helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_TYPES);
//		}
//		// ScopeSet scopes
//		if (scopes != null) {
//			scopes.serialize(serializer, helper.getWSDNamespace());
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		// End-Tag
//		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_PROBE);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//	}
}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.message.discovery;

import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.message.MessageHeaderBuilder;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.DiscoveryData;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.ScopeSet;
import org.ws4d.java.types.URISet;
import org.ws4d.java.util.IDGenerator;
import org.ws4d.java.util.StringUtil;


/**
 * The Class DiscoveryMessage.
 */
public abstract class DiscoveryMessage extends Message {

	private DiscoveryData	discoveryData	= null;

	/**
	 * Creates the discovery header.
	 *
	 * @param action the action
	 * @return the message header
	 */
	public static MessageHeader createDiscoveryHeader(String action) {
		//Re-Modularization 2011-01-21 Implement SOAPHeaderCreator
		//MessageHeader header = SOAPHeader.createHeader(action);
		MessageHeader header = MessageHeaderBuilder.getInstance().createHeader(action);
		header.setMessageId(new AttributedURI(IDGenerator.getUUIDasURI()));
		header.setTo(new AttributedURI(WSDConstants.WSD_TO));
		return header;
	}

	/**
	 * @param header
	 */
	DiscoveryMessage(MessageHeader header) {
		this(header, null);
	}

	/**
	 * @param header
	 * @param discoveryData
	 */
	DiscoveryMessage(MessageHeader header, DiscoveryData discoveryData) {
		super(header);
		this.discoveryData = discoveryData;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", discoveryData=").append(discoveryData);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.DiscoveryData#getEndpointReference()
	 */
	/**
	 * Gets the endpoint reference.
	 *
	 * @return the endpoint reference
	 */
	public EndpointReference getEndpointReference() {
		return discoveryData.getEndpointReference();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.DiscoveryData#getMetadataVersion()
	 */
	/**
	 * Gets the metadata version.
	 *
	 * @return the metadata version
	 */
	public long getMetadataVersion() {
		return discoveryData.getMetadataVersion();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.DiscoveryData#getTypes()
	 */
	/**
	 * Gets the types.
	 *
	 * @return the types
	 */
	public QNameSet getTypes() {
		return discoveryData.getTypes();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.DiscoveryData#getScopes()
	 */
	/**
	 * Gets the scopes.
	 *
	 * @return the scopes
	 */
	public ScopeSet getScopes() {
		return discoveryData.getScopes();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.DiscoveryData#getXAddrs()
	 */
	/**
	 * Gets the x addrs.
	 *
	 * @return the x addrs
	 */
	public URISet getXAddrs() {
		return discoveryData.getXAddrs();
	}

	// ------------------------ DISCOVERY DATA --------------------------

	/**
	 * Get discovery data.
	 * 
	 * @return Discovery data.
	 */
	public DiscoveryData getDiscoveryData() {
		return discoveryData;
	}

	/**
	 * Sets the discovery data.
	 *
	 * @param discoveryData the discoveryData to set
	 */
	public void setDiscoveryData(DiscoveryData discoveryData) {
		this.discoveryData = discoveryData;
	}

}

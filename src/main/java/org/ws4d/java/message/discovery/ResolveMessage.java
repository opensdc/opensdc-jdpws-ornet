/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.message.discovery;

import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.StringUtil;

public class ResolveMessage extends Message {

	public static final URI		ACTION	= new URI(WSDConstants.WSD_ACTION_RESOLVE);

	private EndpointReference	endpointReference;

	public static MessageHeader createResolveHeader() {
		return DiscoveryMessage.createDiscoveryHeader(WSDConstants.WSD_ACTION_RESOLVE);
	}

	/**
	 * Creates a new Resolve message containing a {@link SOAPHeader} with the
	 * appropriate {@link SOAPHeader#getAction() action property} set, the
	 * default {@link SOAPHeader#getTo() to property} for ad-hoc mode (
	 * {@link WSDConstants#WSD_TO}) and a unique
	 * {@link SOAPHeader#getMessageId() message ID property}. All other header-
	 * and discovery-related fields are empty and it is the caller's
	 * responsibility to fill them with suitable values.
	 */
	public ResolveMessage() {
		this(createResolveHeader());
	}

	public ResolveMessage(MessageHeader header) {
		this(header, null);
	}

	public ResolveMessage(MessageHeader header, EndpointReference endpointReference) {
		super(header);
		this.endpointReference = endpointReference;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", endpointReference=").append(endpointReference);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return RESOLVE_MESSAGE;
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.ws4d.java.communication.message.discovery.ResolveMessage#
	 * getEndpointReference()
	 */
	public EndpointReference getEndpointReference() {
		return endpointReference;
	}

	/**
	 * @param endpointReference the endpointReference to set
	 */
	public void setEndpointReference(EndpointReference endpointReference) {
		this.endpointReference = endpointReference;
	}

	// Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPWS Module
//	/**
//	 * Parses the next ResolveMessage.
//	 * 
//	 * @param header
//	 * @param parser
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public static ResolveMessage parse(SOAPHeader header, ElementParser parser, Object helper) throws XmlPullParserException, IOException {
//		ResolveMessage resolveMessage = new ResolveMessage(header);
//
//		resolveMessage.parseUnknownAttributes(parser);
//
//		int event = parser.nextTag();
//		if (event == XmlPullParser.END_TAG) {
//			throw new XmlPullParserException("Resolve is empty");
//		}
//		do {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (helper.getWSANamespace().equals(namespace)) {
//				if (WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE.equals(name)) {
//					resolveMessage.setEndpointReference(DPWSUtil.parseEndpointReference(helper.getDPWSVersion(), parser));
//				} else {
//					resolveMessage.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				resolveMessage.parseUnknownElement(parser, namespace, name);
//			}
//			event = parser.nextTag();
//		} while (event != XmlPullParser.END_TAG);
//		return resolveMessage;
//	}
//
//	/**
//	 * Serialize the next ResolveMessage.
//	 */
//	public void serialize(XmlSerializer serializer, ConstantsHelper helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//		// Start-Tag
//		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_RESOLVE);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// Add the EPR
//		if (endpointReference != null) {
//			endpointReference.serialize(serializer, helper, helper.getWSANamespace(), WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE);
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		// End-Tag
//		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_RESOLVE);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//	}
}

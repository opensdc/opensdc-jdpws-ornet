/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.message.discovery;

import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.StringUtil;

public class ResolveMatchesMessage extends Message {

	public static final URI	ACTION	= new URI(WSDConstants.WSD_ACTION_RESOLVEMATCHES);

	private ResolveMatch	resolveMatch;

	public static MessageHeader createResolveMatchesHeader() {
		return DiscoveryMessage.createDiscoveryHeader(WSDConstants.WSD_ACTION_RESOLVEMATCHES);
	}

	/**
	 * Creates a new ResolveMatches message containing a {@link SOAPHeader} with
	 * the appropriate {@link SOAPHeader#getAction() action property} set, the
	 * default {@link SOAPHeader#getTo() to property} for ad-hoc mode (
	 * {@link WSDConstants#WSD_TO}) and a unique
	 * {@link SOAPHeader#getMessageId() message ID property}. All other header-
	 * and discovery-related fields are empty and it is the caller's
	 * responsibility to fill them with suitable values.
	 */
	public ResolveMatchesMessage() {
		this(createResolveMatchesHeader());
	}

	/**
	 * Constructor.
	 * 
	 * @param header
	 */
	public ResolveMatchesMessage(MessageHeader header) {
		super(header);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", resolveMatch=").append(resolveMatch);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return RESOLVE_MATCHES_MESSAGE;
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.ws4d.java.communication.message.discovery.ResolveMatchesMessage#
	 * getResolveMatch()
	 */
	public ResolveMatch getResolveMatch() {
		return resolveMatch;
	}

	/**
	 * @param resolveMatch the resolveMatch to set
	 */
	public void setResolveMatch(ResolveMatch resolveMatch) {
		this.resolveMatch = resolveMatch;
	}

	
	
	//Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPWS Module
//	/**
//	 * Parses the next ResolveMatchesMessage.
//	 * 
//	 * @param header
//	 * @param parser
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public static ResolveMatchesMessage parse(MessageHeader header, ElementParser parser, Object helper, ProtocolData protocolData) throws XmlPullParserException, IOException {
//		ResolveMatchesMessage resolveMatchesMessage = new ResolveMatchesMessage(header);
//
//		resolveMatchesMessage.parseUnknownAttributes(parser);
//
//		while (parser.nextTag() != XmlPullParser.END_TAG) {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (helper.getWSDNamespace().equals(namespace)) {
//				if (WSDConstants.WSD_ELEMENT_RESOLVEMATCH.equals(name)) {
//					ResolveMatch resolveMatch = new ResolveMatch();
//					resolveMatch.parse(parser, WSDConstants.WSD_ELEMENT_RESOLVEMATCH, helper);
//					resolveMatchesMessage.setResolveMatch(resolveMatch);
//				} else {
//					resolveMatchesMessage.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				resolveMatchesMessage.parseUnknownElement(parser, namespace, name);
//			}
//		}
//
//		if (resolveMatchesMessage.getHeader().getSignature() != null) {
//			if (resolveMatchesMessage.getResolveMatch() != null) {
//				resolveMatchesMessage.getHeader().setValidated(DPWSFramework.getSecurityManager().validateMessage(resolveMatchesMessage.getHeader().getSignature(), protocolData, resolveMatchesMessage.getResolveMatch().getEndpointReference(), null));
//			}
//		}
//		return resolveMatchesMessage;
//	}
//
//	/**
//	 * Serialize the next ResolveMatchesMessage
//	 */
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//		
//		//Canonicalize and serialize this element
//		if(DPWSFramework.hasModule(DPWSFramework.DPWS_SECURITY_MODULE))serializer.attribute("", "ID", "BID1");
//		
//		// Start-Tag
//		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_RESOLVEMATCHES);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// Adds ResolveMatch Elements
//		ResolveMatch match = getResolveMatch();
//		if (match != null) {
//			match.serialize(serializer, helper);
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		// End-Tag
//		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_RESOLVEMATCHES);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//
//	}

}

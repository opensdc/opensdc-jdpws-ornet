/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.message.discovery;

import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.List;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.StringUtil;

/**
 * 
 */
public class ProbeMatchesMessage extends Message {

	public static final URI	ACTION	= new URI(WSDConstants.WSD_ACTION_PROBEMATCHES);

	private List			probeMatches;

	public static MessageHeader createProbeMatchesHeader() {
		return DiscoveryMessage.createDiscoveryHeader(WSDConstants.WSD_ACTION_PROBEMATCHES);
	}

	/**
	 * Creates a new ProbeMatches message containing a {@link SOAPHeader} with
	 * the appropriate {@link SOAPHeader#getAction() action property} set, the
	 * default {@link SOAPHeader#getTo() to property} for ad-hoc mode (
	 * {@link WSDConstants#WSD_TO}) and a unique
	 * {@link SOAPHeader#getMessageId() message ID property}. All other header-
	 * and discovery-related fields are empty and it is the caller's
	 * responsibility to fill them with suitable values.
	 */
	public ProbeMatchesMessage() {
		this(createProbeMatchesHeader());
	}

	/**
	 * @param header
	 */
	public ProbeMatchesMessage(MessageHeader header) {
		super(header);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", probeMatches=").append(probeMatches);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.DPWSMessage#getType()
	 */
	@Override
	public int getType() {
		return PROBE_MATCHES_MESSAGE;
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.ws4d.java.communication.message.discovery.ProbeMatchesMessage#
	 * getProbeMatch(int)
	 */
	public ProbeMatch getProbeMatch(int index) {
		return probeMatches == null ? null : (ProbeMatch) probeMatches.get(index);
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.ws4d.java.communication.message.discovery.ProbeMatchesMessage#
	 * getProbeMatchCount()
	 */
	public int getProbeMatchCount() {
		return probeMatches == null ? 0 : probeMatches.size();
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.ws4d.java.communication.message.discovery.ProbeMatchesMessage#
	 * getProbeMatches()
	 */
	public DataStructure getProbeMatches() {
		// XXX return a clone or an unmodifiable view?
		return probeMatches;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.discovery.ProbeMatchesMessage#isEmpty ()
	 */
	public boolean isEmpty() {
		return probeMatches == null || probeMatches.isEmpty();
	}

	public void addProbeMatch(ProbeMatch probeMatch) {
		if (probeMatch == null) {
			return;
		}
		if (probeMatches == null) {
			probeMatches = new ArrayList();
		}
		probeMatches.add(probeMatch);
	}

	
	
	// Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPWS Module
//	public static ProbeMatchesMessage parse(MessageHeader header, ElementParser parser, Object helper, ProtocolData protocolData) throws XmlPullParserException, IOException {
//		return null;
//	}
//	
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		
//	}
//	
//	/**
//	 * Parses the next ProbeMatchesMessage.
//	 * 
//	 * @param header
//	 * @param parser
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public static ProbeMatchesMessage parse(MessageHeader header, ElementParser parser, Object helper, ProtocolData protocolData) throws XmlPullParserException, IOException {
//		ProbeMatchesMessage probeMatchesMessage = new ProbeMatchesMessage(header);
//
//		probeMatchesMessage.parseUnknownAttributes(parser);
//
//		while (parser.nextTag() != XmlPullParser.END_TAG) {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (helper.getWSDNamespace().equals(namespace)) {
//				if (WSDConstants.WSD_ELEMENT_PROBEMATCH.equals(name)) {
//					ProbeMatch probeMatch = new ProbeMatch();
//					probeMatch.parse(parser, WSDConstants.WSD_ELEMENT_PROBEMATCH, helper);
//					probeMatchesMessage.addProbeMatch(probeMatch);
//				} else {
//					probeMatchesMessage.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				probeMatchesMessage.parseUnknownElement(parser, namespace, name);
//			}
//		}
//
//		// FIXME only first EPR
//		if (DPWSFramework.hasModule(DPWSFramework.DPWS_SECURITY_MODULE) && probeMatchesMessage.getHeader().getSignature() != null) {
//			if (probeMatchesMessage.getProbeMatch(0) != null) {
//				ArrayList potentialAlia = new ArrayList();
//				for (int i = 0; i < probeMatchesMessage.getProbeMatchCount(); i++) {
//					Iterator iter = probeMatchesMessage.getProbeMatch(i).getXAddrs().iterator();
//					for (int j = 0; iter.hasNext(); j++) {
//						String str = iter.next().toString();
//						if (!potentialAlia.contains(str)) potentialAlia.add(str);
//					}
//				}
//
//				Iterator iter = potentialAlia.iterator();
//				String[] pA = new String[potentialAlia.size()];
//				for (int i = 0; i < potentialAlia.size(); i++) {
//					pA[i] = iter.next().toString();
//				}
//				probeMatchesMessage.setSecure(true);
//				probeMatchesMessage.getHeader().setValidated(DPWSFramework.getSecurityManager().validateMessage(probeMatchesMessage.getHeader().getSignature(), protocolData, probeMatchesMessage.getProbeMatch(0).getEndpointReference(), pA));
//			}
//		}
//		return probeMatchesMessage;
//	}
//
//	public void serialize(XmlSerializer serializer, ConstantsHelper helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//
//		// Canonicalize and serialize this element
//		if (DPWSFramework.hasModule(DPWSFramework.DPWS_SECURITY_MODULE)) serializer.attribute("", "ID", "BID1");
//
//		// Start-Tag
//		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_PROBEMATCHES);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// Adds ProbeMatch Elements
//		for (Iterator it = getProbeMatches().iterator(); it.hasNext();) {
//			ProbeMatch probeMatch = (ProbeMatch) it.next();
//			probeMatch.serialize(serializer, helper);
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		// End-Tag
//		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_PROBEMATCHES);
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//	}
}

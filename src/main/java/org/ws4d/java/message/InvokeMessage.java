/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.message;

import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.util.StringUtil;

public class InvokeMessage extends Message {

	/** object representation of the content of the message's body */
	private IParameterValue	content	= null;

	/**
	 * Creates a new InvokeInput message containing a {@link SOAPHeader} with a
	 * {@link SOAPHeader#getAction() action property} set to the value of
	 * argument <code>inputAction</code> and a unique
	 * {@link SOAPHeader#getMessageId() message ID property}. All other header-
	 * fields are empty and it is the caller's responsibility to fill them with
	 * suitable values.
	 */
	public InvokeMessage(String action) {
		this(action, true);
	}

	/**
	 * Creates a new InvokeInput message containing a {@link SOAPHeader} with a
	 * {@link SOAPHeader#getAction() action property} set to the value of the
	 * argument <code>inputAction</code>. If argument <code>request</code> is
	 * <code>true</code> a unique {@link SOAPHeader#getMessageId() message ID
	 * property} is set too. All other header- fields are empty and it is the
	 * caller's responsibility to fill them with suitable values.
	 */
	public InvokeMessage(String action, boolean request) {
		//Re-Modularization 2011-01-21 Implement SOAPHeaderCreator
		this(request ? MessageHeaderBuilder.getInstance().createRequestHeader(action) : MessageHeaderBuilder.getInstance().createHeader(action));
	}

	public InvokeMessage(MessageHeader header) {
		super(header);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", content=").append(content);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.message.Message#getType()
	 */
	@Override
	public int getType() {
		return INVOKE_MESSAGE;
	}

	/**
	 * Gets the content of the body of this message. The {@link ParameterValue}
	 * is the object representation of the content of the body of the message.
	 * 
	 * @return the content of this message
	 */
	public IParameterValue getContent() {
		return content;
	}

	/**
	 * Sets content of the message body. The {@link ParameterValue} is the
	 * object representation of the content of the message body.
	 * 
	 * @param content the content of this message
	 */
	public void setContent(IParameterValue content) {
		this.content = content;
	}

	// Re-Modularization 2011-01-21 Extract To Parser/Serializer in DPPWS Module
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		
//	}
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		// ################## Body-StartTag ##################
//		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//		ParameterValue parameters = getContent();
//		if (parameters != null) {
//			parameters.serialize(serializer);
//		}
//		// ################## BODY-EndTag ##################
//		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
//	}
}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.constants;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.URI;

/**
 * Constants used by WS Addressing.
 */
public interface WSAConstants {

	/** The namespace name for WS Addressing. */
	public static final String	WSA_NAMESPACE_NAME								= "http://www.w3.org/2005/08/addressing".intern(); //"Web Services Addressing 1.0 - Core", see http://www.w3.org/TR/ws-addr-core/
	
	public static final String 	WSAM_NAMESPACE_NAME								= "http://www.w3.org/2007/05/addressing/metadata".intern(); //"Web Services Addressing 1.0 - Metadata", see http://www.w3.org/TR/ws-addr-metadata/
	
	public static final String 	WSAW_NAMESPACE_NAME								= "http://www.w3.org/2006/05/addressing/wsdl".intern(); //"Web Services Addressing 1.0 - WSDL Binding", see http://www.w3.org/TR/ws-addr-wsdl/

	/** "wsa" The default prefix for the WS Addressing namespace. */
	public static final String	WSA_NAMESPACE_PREFIX							= "wsa".intern();
	
	public static final String	WSAM_NAMESPACE_PREFIX							= "wsam".intern();
	
	public static final String	WSAW_NAMESPACE_PREFIX							= "wsaw".intern();

	public static final String	WSA_ATTR_RELATIONSHIP_TYPE						= "RelationshipType";

	public static final String	WSA_ATTR_IS_REFERENCE_PARAMETER					= "IsReferenceParameter";

	/** "Action". */
	public static final String	WSA_ELEM_ACTION									= "Action".intern();

	/** "Address". */
	public static final String	WSA_ELEM_ADDRESS								= "Address";

	/** "To". */
	public static final String	WSA_ELEM_TO										= "To";

	/** "EndpointReference". */
	public static final String	WSA_ELEM_ENDPOINT_REFERENCE						= "EndpointReference";

	public static final String	WSA_ELEM_FAULT_ENDPOINT							= "FaultTo";

	/** "MessageID". */
	public static final String	WSA_ELEM_MESSAGE_ID								= "MessageID";

	/** "Metadata". */
	public static final String	WSA_ELEM_METADATA								= "Metadata";

	public static final String	WSA_ELEM_REFERENCE_PARAMETERS					= "ReferenceParameters";

	/** "RelatesTo". */
	public static final String	WSA_ELEM_RELATESTO								= "RelatesTo";

	public static final String	WSA_ELEM_REPLY_TO								= "ReplyTo";

	public static final String	WSA_ELEM_SOURCE_ENDPOINT						= "From";

	/** "http://www.w3.org/2005/08/addressing/anonymous". */
	public static final URI		WSA_ANONYMOUS									= new URI(WSA_NAMESPACE_NAME + "/anonymous");

	public static final String	WSA_TYPE_RELATIONSHIP_REPLY						= "Reply";

	public static final String	WSA_ACTION_ADDRESSING_FAULT						= WSA_NAMESPACE_NAME + "/fault";

	public static final String	WSA_ACTION_SOAP_FAULT							= WSA_NAMESPACE_NAME + "/soap/fault";

	/* faults */
	public static final QName	WSA_FAULT_DESTINATION_UNREACHABLE				= QNameFactory.getInstance().getQName("DestinationUnreachable", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

	public static final QName	WSA_FAULT_INVALID_ADDRESSING_HEADER				= QNameFactory.getInstance().getQName("InvalidAddressingHeader", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

	public static final QName	WSA_FAULT_MESSAGE_ADDRESSING_HEADER_REQUIRED	= QNameFactory.getInstance().getQName("MessageAddressingHeaderRequired", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

	public static final QName	WSA_FAULT_ACTION_NOT_SUPPORTED					= QNameFactory.getInstance().getQName("ActionNotSupported", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

	public static final QName	WSA_FAULT_ENDPOINT_UNAVAILABLE					= QNameFactory.getInstance().getQName("EndpointUnavailable", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

	public static final QName	WSA_PROBLEM_HEADER_QNAME						= QNameFactory.getInstance().getQName("ProblemHeaderQName", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

	public static final QName	WSA_PROBLEM_ACTION								= QNameFactory.getInstance().getQName("ProblemAction", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

}

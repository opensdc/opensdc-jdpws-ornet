/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.constants;

/**
 * Interface holds general communication constants
 */
public interface CommunicationConstants {

	final static int			COMMUNICATION_TYPE_DPWS			= 0;

	final static int			COMMUNICATION_TYPE_BLUETOOTH	= 1;

	final static int			COMMUNICATION_TYPE_ZIGBEE		= 2;

	public static final String	COMMUNICATION_MANAGER_DPWS		= "DPWS";

	public static final String	COMMUNICATION_MANAGER_BLUETOOTH	= "BT";

	public static final String	COMMUNICATION_MANAGER_ZIGBEE	= "ZigBee";

	public static final String	URN_SCHEMA						= "urn";

	public static final String	MESSAGE_PROPERTY_COMMUNICATION	= "communication";

	public static final boolean	BUFFERED_INPUT					= true;

}

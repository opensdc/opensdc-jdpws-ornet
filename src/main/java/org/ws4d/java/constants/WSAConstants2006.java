/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.constants;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.URI;

public interface WSAConstants2006 {

	/** The namespace name for WS Addressing. */
	public static final String	WSA_NAMESPACE_NAME								= "http://schemas.xmlsoap.org/ws/2004/08/addressing".intern();

	/** "wsa" The default prefix for the WS Addressing namespace. */
	public static final String	WSA_NAMESPACE_PREFIX							= "wsa".intern();

	public static final String	WSA_ELEM_REFERENCE_PROPERTIES					= "ReferenceProperties";

	public static final String	WSA_ELEM_PORT_TYPE								= "PortType";

	public static final String	WSA_ELEM_SERVICE_NAME							= "ServiceName";

	public static final String	WSA_ELEM_PORT_NAME								= "PortName";

	public static final String	WSA_ELEM_POLICY									= "Policy";

	/** "http://schemas.xmlsoap.org/ws/2004/08/addressing/anonymous". */
	public static final URI		WSA_ANONYMOUS									= new URI(WSA_NAMESPACE_NAME + "/role/anonymous");

	public static final String	WSA_ACTION_ADDRESSING_FAULT						= WSA_NAMESPACE_NAME + "/fault";

	public static final String	WSA_ACTION_SOAP_FAULT							= WSA_NAMESPACE_NAME + "/soap/fault";

	/* faults */
	public static final QName	WSA_FAULT_DESTINATION_UNREACHABLE				= QNameFactory.getInstance().getQName("DestinationUnreachable", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

	public static final QName	WSA_FAULT_INVALID_ADDRESSING_HEADER				= QNameFactory.getInstance().getQName("InvalidAddressingHeader", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

	public static final QName	WSA_FAULT_MESSAGE_ADDRESSING_HEADER_REQUIRED	= QNameFactory.getInstance().getQName("MessageAddressingHeaderRequired", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

	public static final QName	WSA_FAULT_ACTION_NOT_SUPPORTED					= QNameFactory.getInstance().getQName("ActionNotSupported", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

	public static final QName	WSA_FAULT_ENDPOINT_UNAVAILABLE					= QNameFactory.getInstance().getQName("EndpointUnavailable", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

	public static final QName	WSA_PROBLEM_HEADER_QNAME						= QNameFactory.getInstance().getQName("ProblemHeaderQName", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

	public static final QName	WSA_PROBLEM_ACTION								= QNameFactory.getInstance().getQName("ProblemAction", WSA_NAMESPACE_NAME, WSA_NAMESPACE_PREFIX);

}

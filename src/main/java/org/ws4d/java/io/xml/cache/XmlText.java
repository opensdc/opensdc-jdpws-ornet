/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.io.xml.cache;

import java.io.IOException;

import org.ws4d.java.io.xml.XmlSerializer;
import org.ws4d.java.io.xml.canonicalization.CanonicalSerializer;

public class XmlText implements XmlStructure {

	String	text;

	public XmlText(String txt) {
		text = txt;
	}

	@Override
	public void flush(CanonicalSerializer serializer) throws IllegalArgumentException, IllegalStateException, IOException {
		serializer.texto(text);
	}

	@Override
	public void flush(XmlSerializer serializer) throws IllegalArgumentException, IllegalStateException, IOException {
		serializer.text(text);
	}

	@Override
	public int getType() {
		return XmlStructure.XML_TEXT;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public String getNamespace() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public void setNameSpace(String ns) {
		// TODO Auto-generated method stub

	}

}

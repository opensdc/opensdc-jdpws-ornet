/*******************************************************************************
 * Copyright (c) 2010 - 2015 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package org.ws4d.java.io.xml;

import org.ws4d.java.types.QName;

public class XMLSerializerHelper {

	public static String getLocalPartPrefixed(org.xmlpull.v1.XmlSerializer serializer, QName qn) {
		String localPartPrefixed = null;
		if (qn!=null && serializer!=null)
		{
			String prefix = serializer.getPrefix(qn.getNamespace(), true);
			localPartPrefixed = prefix + ":" + qn.getLocalPart();
		}
		return localPartPrefixed;
	}
	
	public static String getNamespacePrefixed(org.xmlpull.v1.XmlSerializer serializer, QName qn) {
		String nameSpacePartPrefixed = null;
		if (qn!=null && serializer!=null)
		{
			String prefix = serializer.getPrefix(qn.getNamespace(), true);
			nameSpacePartPrefixed = prefix + ":" + qn.getNamespace();
		}
		return nameSpacePartPrefixed;
	}

}

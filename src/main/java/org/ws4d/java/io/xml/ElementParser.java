/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.io.xml;

import java.io.IOException;

import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.DPWSConstants2006;
import org.ws4d.java.constants.WSAConstants;
import org.ws4d.java.constants.WSAConstants2006;
import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.EndpointReference2004;
import org.ws4d.java.types.MetadataMData;
import org.ws4d.java.types.ProbeScopeSet;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.ReferenceParametersMData;
import org.ws4d.java.types.ReferenceParametersMData.ReferenceParameter;
import org.ws4d.java.types.ScopeSet;
import org.ws4d.java.types.URI;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/**
 * 
 */
public class ElementParser extends AbstractElementParser {

	public static int nextNonWhiteSpace(String src, int offset) {
		if (src == null) {
			return -1;
		}
		int len = src.length();
		for (int i = offset + 1; i < len; i++) {
			char c = src.charAt(i);
			switch (c) {
				case (' '):
				case ('\t'):
				case ('\n'):
				case ('\r'): {
					break;
				}
				default: {
					return i;
				}
			}
		}
		return -1;
	}

	public static int nextWhiteSpace(String src, int offset) {
		if (src == null) {
			return -1;
		}
		int len = src.length();
		for (int i = offset + 1; i < len; i++) {
			char c = src.charAt(i);
			switch (c) {
				case (' '):
				case ('\t'):
				case ('\n'):
				case ('\r'): {
					return i;
				}
				default: {
					break;
				}
			}
		}
		return -1;
	}

	/**
	 * @param source
	 */
	public ElementParser(AbstractElementParser source) {
		super(source, source.getFromUri());
	}
	
	/**
	 * @param parser
	 * @param fromUri
	 */
	public ElementParser(XmlPullParser parser, URI fromUri) {
		super(parser,fromUri);
	}



//	/**
//	 * The method returns an EndpointReference for DPWS2009 if newAddressing is
//	 * "true", else if newAddressing ist "false" it returns an EnpointReference
//	 * for DPWS2006.
//	 * 
//	 * @param addressingVersion , int that gives info about the Addressing
//	 *            Version
//	 * @return EndpointReference
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public EndpointReference nextEndpointReference(int dpwsVersion) throws XmlPullParserException, IOException {
//
//		switch (dpwsVersion) {
//			case DPWSConstants.DPWS_VERSION2009:
//				return nextEndpointReference2005();
//			case DPWSConstants2006.DPWS_VERSION2006:
//				return nextEndpointReference2004();
//			default:
//				throw new IllegalArgumentException("Unsupported DPWS Version");
//		}
//	}

//	public EndpointReference nextEndpointReference2005() throws XmlPullParserException, IOException {
//		// handle attributes
//		int attributeCount = getAttributeCount();
//		HashMap unknownAttributes = null;
//		if (attributeCount > 0) {
//			unknownAttributes = new HashMap();
//			for (int i = 0; i < attributeCount; i++) {
//				String namespace = getAttributeNamespace(i);
//				if ("".equals(namespace)) {
//					// default to namespace of containing element
//					namespace = getNamespace();
//				}
//				String name = getAttributeName(i);
//				String value = getAttributeValue(i);
//				unknownAttributes.put(QNameFactory.getInstance().getQName(name, namespace), value);
//			}
//		}
//		AttributedURI address = null;
//		ReferenceParametersMData parameters = null;
//		MetadataMData metadata = null;
//		HashMap unknownElements = null;
//		while (nextTag() != END_TAG) {
//			String namespace = getNamespace();
//			String name = getName();
//			if (WSAConstants.WSA_NAMESPACE_NAME.equals(namespace)) {
//				if (WSAConstants.WSA_ELEM_ADDRESS.equals(name)) {
//					address = nextAttributedUri();
//				} else if (WSAConstants.WSA_ELEM_REFERENCE_PARAMETERS.equals(name)) {
//					parameters = parseReferenceParameters();
//				} else if (WSAConstants.WSA_ELEM_METADATA.equals(name)) {
//					metadata = new MetadataMData();
//					nextGenericElement(metadata);
//				} else {
//					QName elementName = QNameFactory.getInstance().getQName(name, namespace);
//					Object result = chainHandler(elementName);
//					if (result != null) {
//						if (unknownElements == null) {
//							unknownElements = new HashMap();
//						}
//						DataStructure elements = (DataStructure) unknownElements.get(elementName);
//						if (elements == null) {
//							elements = new ArrayList();
//							unknownElements.put(elementName, elements);
//						}
//						elements.add(result);
//					}
//				}
//			}
//		}
//		EndpointReference er = new EndpointReference(address, parameters, metadata);
//		if (unknownAttributes != null) {
//			er.setUnknownAttributes(unknownAttributes);
//		}
//		if (unknownElements != null) {
//			er.setUnknownElements(unknownElements);
//		}
//
//		return er;
//	}

//	public EndpointReference nextEndpointReference2004() throws XmlPullParserException, IOException {
//		// handle attributes
//		int attributeCount = getAttributeCount();
//		HashMap unknownAttributes = null;
//		if (attributeCount > 0) {
//			unknownAttributes = new HashMap();
//			for (int i = 0; i < attributeCount; i++) {
//				String namespace = getAttributeNamespace(i);
//				if ("".equals(namespace)) {
//					// default to namespace of containing element
//					namespace = getNamespace();
//				}
//				String name = getAttributeName(i);
//				String value = getAttributeValue(i);
//				unknownAttributes.put(QNameFactory.getInstance().getQName(name, namespace), value);
//			}
//		}
//		AttributedURI address = null;
//		ReferenceParametersMData properties = null;
//		ReferenceParametersMData parameters = null;
//		// MetadataMData metadata = null;
//		HashMap unknownElements = null;
//		QName portType = null;
//		QName serviceName = null;
//		String portName = null;
//		while (nextTag() != END_TAG) {
//			String namespace = getNamespace();
//			String name = getName();
//			if (WSAConstants2006.WSA_NAMESPACE_NAME.equals(namespace)) {
//				if (WSAConstants.WSA_ELEM_ADDRESS.equals(name)) {
//					address = nextAttributedUri();
//				} else if (WSAConstants2006.WSA_ELEM_REFERENCE_PROPERTIES.equals(name)) {
//					properties = parseReferenceParameters();
//				} else if (WSAConstants.WSA_ELEM_REFERENCE_PARAMETERS.equals(name)) {
//					parameters = parseReferenceParameters();
//				} else if (WSAConstants2006.WSA_ELEM_PORT_TYPE.equals(name)) {
//					portType = nextQName();
//				} else if (WSAConstants2006.WSA_ELEM_SERVICE_NAME.equals(name)) {
//					ArrayList list = parseServiceName();
//					portName = (String) list.get(0);
//					serviceName = (QName) list.get(1);
//				} else if (WSAConstants2006.WSA_ELEM_POLICY.equals(name)) {
//					// ergaenzen
//				} else {
//					QName elementName = QNameFactory.getInstance().getQName(name, namespace);
//					Object result = chainHandler(elementName);
//					if (result != null) {
//						if (unknownElements == null) {
//							unknownElements = new HashMap();
//						}
//						DataStructure elements = (DataStructure) unknownElements.get(elementName);
//						if (elements == null) {
//							elements = new ArrayList();
//							unknownElements.put(elementName, elements);
//						}
//						elements.add(result);
//					}
//				}
//			}
//		}
//		EndpointReference2004 er = new EndpointReference2004(address, parameters, properties, portType, serviceName, portName);
//		if (unknownAttributes != null) {
//			er.setUnknownAttributes(unknownAttributes);
//		}
//		if (unknownElements != null) {
//			er.setUnknownElements(unknownElements);
//		}
//		return er;
//	}



	public AttributedURI nextAttributedUri() throws XmlPullParserException, IOException {
		AttributedURI result;
		int attributeCount = getAttributeCount();
		if (attributeCount > 0) {
			HashMap attributes = new HashMap();
			for (int i = 0; i < attributeCount; i++) {
				String namespace = getAttributeNamespace(i);
				if ("".equals(namespace)) {
					// default to namespace of containing element
					namespace = getNamespace();
				}
				String name = getAttributeName(i);
				String value = getAttributeValue(i);
				attributes.put(QNameFactory.getInstance().getQName(name, namespace), value);
			}
			result = new AttributedURI(nextText().trim(), attributes);
		} else {
			result = new AttributedURI(nextText().trim());
		}
		return result;
	}

	

	private ArrayList parseServiceName() throws XmlPullParserException, IOException {
		ArrayList list = new ArrayList();
		ElementParser parser = new ElementParser(this);
		int attributeCount = parser.getAttributeCount();
		if (attributeCount > 0) {
			String namespace = parser.getAttributeNamespace(0);
			if ("".equals(namespace)) {
				// default to namespace of containing element
				namespace = getNamespace();
			}
			String value = parser.getAttributeValue(0);
			list.add(value);
		}
		QName serviceName = parser.nextQName();
		list.add(serviceName);
		return list;
	}

	

	public ScopeSet nextScopeSet() throws XmlPullParserException, IOException {
		ScopeSet scopeSet = new ScopeSet();
		int attributeCount = getAttributeCount();
		for (int i = 0; i < attributeCount; i++) {
			String namespace = getAttributeNamespace(i);
			if ("".equals(namespace)) {
				// default to namespace of containing element
				namespace = getNamespace();
			}
			String name = getAttributeName(i);
			String value = getAttributeValue(i);
			scopeSet.addUnknownAttribute(QNameFactory.getInstance().getQName(name, namespace), value);
		}
		nextScopeSet(scopeSet);
		return scopeSet;
	}

	public ProbeScopeSet nextProbeScopeSet() throws XmlPullParserException, IOException {
		ProbeScopeSet scopeSet = new ProbeScopeSet();
		int attributeCount = getAttributeCount();
		String matchBy = WSDConstants.WSD_MATCHING_RULE_DEFAULT;
		for (int i = 0; i < attributeCount; i++) {
			String namespace = getAttributeNamespace(i);
			if ("".equals(namespace)) {
				// default to namespace of containing element
				namespace = getNamespace();
			}
			String name = getAttributeName(i);
			String value = getAttributeValue(i);
			if (WSDConstants.WSD_NAMESPACE_NAME.equals(namespace) && WSDConstants.WSD_ATTR_MATCH_BY.equals(name)) {
				matchBy = value;
			} else {
				scopeSet.addUnknownAttribute(QNameFactory.getInstance().getQName(name, namespace), value);
			}
		}
		scopeSet.setMatchBy(matchBy);
		nextScopeSet(scopeSet);
		return scopeSet;
	}

	/**
	 * Parses scopes list and adds scope to given scope set.
	 * 
	 * @param scopes
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	public void nextScopeSet(ScopeSet scopes) throws XmlPullParserException, IOException {
		String value = nextText();
		int pos1 = -1;
		int pos2 = pos1;
		do {
			pos1 = nextNonWhiteSpace(value, pos1);
			if (pos1 == -1) {
				break;
			}
			pos2 = nextWhiteSpace(value, pos1);
			if (pos2 == -1) {
				pos2 = value.length();
			}
			String uri = value.substring(pos1, pos2);
			scopes.addScope(uri);
			pos1 = pos2;
		} while (pos1 != -1);
	}



//	private ReferenceParametersMData parseReferenceParameters() throws XmlPullParserException, IOException {
//		ReferenceParametersMData parameters = new ReferenceParametersMData();
//		handleUnknownAttributes(parameters);
//		int event = nextTag(); // go to first child
//		int depth = getDepth();
//		String namespace = getNamespace();
//		String name = getName();
//		if (event == END_TAG && (WSAConstants.WSA_NAMESPACE_NAME.equals(namespace) || WSAConstants2006.WSA_NAMESPACE_NAME.equals(namespace)) && WSAConstants.WSA_ELEM_REFERENCE_PARAMETERS.equals(name)) {
//			// empty but existing reference parameters block
//			return parameters;
//		}
//		ElementParser parser = new ElementParser(this);
//		ReferenceParameter currentParameter = null;
//		boolean onTopLevel = true;
//		StringBuffer result = new StringBuffer();
//		while (true) {
//			do {
//				switch (event) {
//					case (START_TAG): {
//						namespace = parser.getNamespace();
//						name = parser.getName();
//						if (onTopLevel) {
//							if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace) && WSEConstants.WSE_ELEM_IDENTIFIER.equals(name)) {
//								parameters.setWseIdentifier(new URI(parser.nextText().trim()));
//								continue;
//							}
//							QName elementName = QNameFactory.getInstance().getQName(name, namespace);
//							Object obj = chainHandler(elementName, false);
//							if (obj != null) {
//								parameters.addUnknownElement(elementName, obj);
//								continue;
//							}
//							// 1st chunk = '<' literal (statically known)
//							// 2nd chunk = element namespace
//							// 3rd chunk = ':' literal + element name
//							// 4th chunk = bulk char data
//							// 5th chunk = next attribute/element's namespace
//							// 6th chunk = see 4th chunk
//							// 7th chunk = see 5th chunk
//							currentParameter = new ReferenceParameter(namespace, name);
//							parameters.add(currentParameter);
//						} else {
//							result.append('<');
//							currentParameter.appendChunk(result.toString());
//							result = new StringBuffer();
//							currentParameter.appendChunk(namespace);
//							result.append(':').append(name);
//						}
//
//						int attrCount = parser.getAttributeCount();
//						for (int i = 0; i < attrCount; i++) {
//							result.append(' ');
//							String prefix = parser.getAttributePrefix(i);
//							String attribute = parser.getAttributeName(i);
//							if (prefix == null) {
//								// assume same attribute namespace as element
//								if ((WSAConstants.WSA_NAMESPACE_NAME.equals(namespace) || WSAConstants2006.WSA_NAMESPACE_NAME.equals(namespace)) && WSAConstants.WSA_ATTR_IS_REFERENCE_PARAMETER.equals(attribute)) {
//									// skip wsa:IsReferenceParameter
//									continue;
//								}
//							} else {
//								String attributeNamespace = parser.getAttributeNamespace(i);
//								if ((WSAConstants.WSA_NAMESPACE_NAME.equals(attributeNamespace) || WSAConstants2006.WSA_NAMESPACE_NAME.equals(attributeNamespace)) && WSAConstants.WSA_ATTR_IS_REFERENCE_PARAMETER.equals(attribute)) {
//									// skip wsa:IsReferenceParameter
//									continue;
//								}
//								currentParameter.appendChunk(result.toString());
//								currentParameter.appendChunk(attributeNamespace);
//								result = new StringBuffer();
//								result.append(':');
//							}
//							String value = parser.getAttributeValue(i);
//							result.append(attribute).append("=\"").append(value).append('\"');
//						}
//						result.append('>');
//						onTopLevel = false;
//						break;
//					}
//					case (TEXT): {
//						result.append(parser.getText().trim());
//						break;
//					}
//					case (END_TAG): {
//						result.append("</");
//						currentParameter.appendChunk(result.toString());
//						currentParameter.appendChunk(parser.getNamespace());
//						result = new StringBuffer();
//						result.append(':').append(parser.getName()).append('>');
//						break;
//					}
//				}
//			} while ((event = parser.next()) != END_DOCUMENT);
//			event = nextTag();
//			if (getDepth() == depth) {
//				// next reference parameter starts
//				parser = new ElementParser(this);
//				currentParameter.appendChunk(result.toString());
//				result = new StringBuffer();
//				onTopLevel = true;
//			} else {
//				// reference parameters end tag
//				break;
//			}
//		}
//		if (currentParameter != null) {
//			currentParameter.appendChunk(result.toString());
//		}
//		return parameters;
//	}

	@Override
	protected AbstractElementParser createElementParser(
			AbstractElementParser parser) {
		return new ElementParser(parser);
	}	
}

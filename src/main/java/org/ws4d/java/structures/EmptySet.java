/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.structures;

/**
 * 
 */
class EmptySet extends Set {

	private static final String	CANNOT_REMOVE	= "Cannot remove from this set.";

	public EmptySet() {
		super();
	}

	@Override
	public boolean add(Object obj) {
		throw new UnsupportedOperationException(CANNOT_REMOVE);
	}

	@Override
	public boolean addAll(DataStructure data) {
		throw new UnsupportedOperationException(CANNOT_REMOVE);
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException(CANNOT_REMOVE);
	}

	@Override
	public boolean contains(Object obj) {
		return false;
	}

	@Override
	public boolean containsAll(DataStructure data) {
		return false;
	}

	@Override
	public boolean isEmpty() {
		return true;
	}

	@Override
	public Iterator iterator() {
		return EmptyStructures.EMPTY_ITERATOR;
	}

	@Override
	public boolean remove(Object obj) {
		throw new UnsupportedOperationException(CANNOT_REMOVE);
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public Object[] toArray() {
		return EmptyStructures.EMPTY_OBJECT_ARRAY;
	}

	@Override
	public Object[] toArray(Object[] object) {
		return EmptyStructures.EMPTY_OBJECT_ARRAY;
	}

	@Override
	public String toString() {
		return "{}";
	}
}

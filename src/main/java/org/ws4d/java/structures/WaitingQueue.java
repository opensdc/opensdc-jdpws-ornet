/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.structures;

/**
 * Synchronized Queue. The enqueueing thread must wait, until someone takes the
 * object out of the queue.
 */
public class WaitingQueue extends Queue {

	/**
	 * Enqueues element into this queue.
	 * 
	 * @param o element to enqueue.
	 */
	@Override
	public void enqueue(Object o) {
		synchronized (o) {
			addElement(o, -1);
			waitOnObject(o);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.structures.Queue#get()
	 */
	@Override
	public synchronized Object get() {
		Object o = getFirstElement();
		synchronized (o) {
			o.notifyAll();
		}
		return o;
	}

	/**
	 * Enqueues element into this queue, but the element will be set on the
	 * beginning of the queue.
	 * 
	 * @param o element to enqueue.
	 */
	@Override
	public void enqueueAtBeginning(Object o) {
		synchronized (o) {
			addElement(o, 0);
			waitOnObject(o);
		}
	}

	/**
	 * Stops thread which enqueues this object, until someone takes it out of
	 * the queue.
	 * 
	 * @param o object to wait on.
	 */
	private void waitOnObject(Object o) {
		try {
			o.wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

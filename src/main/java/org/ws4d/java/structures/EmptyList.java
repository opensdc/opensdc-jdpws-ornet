/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.structures;

import java.util.NoSuchElementException;

class EmptyList extends List {

	private static final ListIterator	LIST_ITERATOR	= new ListIteratorImpl();

	private static final String			CANNOT_ADD		= "Cannot add to this list.";

	private static final String			CANNOT_REMOVE	= "Cannot remove from this list.";

	private static final String			CANNOT_MODIFY	= "Cannot modify this list.";

	EmptyList() {
		super();
	}

	@Override
	public void add(int index, Object obj) {
		throw new UnsupportedOperationException(CANNOT_ADD);
	}

	@Override
	public boolean add(Object obj) {
		throw new UnsupportedOperationException(CANNOT_REMOVE);
	}

	@Override
	public boolean addAll(DataStructure data) {
		throw new UnsupportedOperationException(CANNOT_REMOVE);
	}

	@Override
	public boolean addAll(int index, DataStructure data) {
		throw new UnsupportedOperationException(CANNOT_ADD);
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException(CANNOT_REMOVE);
	}

	@Override
	public boolean contains(Object obj) {
		return false;
	}

	@Override
	public boolean containsAll(DataStructure data) {
		return false;
	}

	@Override
	public Object get(int index) {
		return null;
	}

	@Override
	public int indexOf(Object obj) {
		return -1;
	}

	@Override
	public boolean isEmpty() {
		return true;
	}

	@Override
	public Iterator iterator() {
		return EmptyStructures.EMPTY_ITERATOR;
	}

	@Override
	public int lastIndexOf(Object obj) {
		return -1;
	}

	@Override
	public ListIterator listIterator() {
		return LIST_ITERATOR;
	}

	@Override
	public ListIterator listIterator(int index) {
		return LIST_ITERATOR;
	}

	@Override
	public Object remove(int index) {
		throw new UnsupportedOperationException(CANNOT_REMOVE);
	}

	@Override
	public boolean remove(Object obj) {
		throw new UnsupportedOperationException(CANNOT_REMOVE);
	}

	@Override
	public Object set(int index, Object obj) {
		throw new UnsupportedOperationException(CANNOT_MODIFY);
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public List subList(int fromIndex, int toIndex) {
		return this;
	}

	@Override
	public Object[] toArray() {
		return EmptyStructures.EMPTY_OBJECT_ARRAY;
	}

	@Override
	public Object[] toArray(Object[] object) {
		return EmptyStructures.EMPTY_OBJECT_ARRAY;
	}

	@Override
	public String toString() {
		return "{}";
	}

	// ============================== INNER CLASS
	// ================================

	private static class ListIteratorImpl extends EmptyIterator implements ListIterator {

		@Override
		public void add(Object obj) {
			throw new UnsupportedOperationException(CANNOT_ADD);
		}

		@Override
		public boolean hasPrevious() {
			return false;
		}

		@Override
		public int indexOfNext() {
			return 0;
		}

		@Override
		public Object previous() {
			throw new NoSuchElementException();
		}

		@Override
		public void set(Object obj) {
			throw new UnsupportedOperationException(CANNOT_MODIFY);
		}

	}
}

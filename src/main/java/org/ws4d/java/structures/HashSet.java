/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.structures;

public class HashSet extends Set {

	HashMap	map;

	/**
	 * Constructor.
	 */
	public HashSet() {
		map = new HashMap();
	}

	/**
	 * @param data
	 */
	public HashSet(DataStructure data) {
		map = new HashMap(data.size());
		addAll(data);
	}

	public HashSet(final int initialCapacity) {
		map = new HashMap(initialCapacity);
	}

	@Override
	public boolean add(Object obj) {
		return (map.put(obj, null) == null);
	}

	@Override
	public Iterator iterator() {
		return map.keySet().iterator();
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public boolean contains(Object obj) {
		return map.containsKey(obj);
	}

	@Override
	public boolean remove(Object obj) {
		boolean result = contains(obj);
		if (result) {
			map.remove(obj);
		}

		return result;
	}

}

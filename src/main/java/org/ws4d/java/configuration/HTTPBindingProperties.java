/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.configuration;

import org.ws4d.java.communication.HTTPBinding;
import org.ws4d.java.communication.HTTPSBinding;
import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.NetworkDetection;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.List;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.Math;

public class HTTPBindingProperties  implements PropertiesHandler {

	public static final String				PROP_ADDRESS						= "Address";

	public static final String				PROP_PORT							= "Port";

	public static final String				PROP_SUFFIX							= "Suffix";

	public static final String				PROP_HTTPS							= "IsHTTPS";
	
	public static final String				PROP_ALIAS							= "Alias";

	public static final String				PROP_ADDRESS_GROUP					= "AddressGroup";

	public static final String				SUBSECTION_HTTP_BINDINGS			= "HTTPBindings";

	public static final String				SUBSUBSECTION_HTTP_BINDING			= "HTTPBinding";


	public static final PropertyHeader		HEADER_SUBSECTION_HTTP_BINDINGS		= new PropertyHeader(SUBSECTION_HTTP_BINDINGS, Properties.HEADER_SECTION_BINDINGS);

	public static final PropertyHeader		HEADER_SUBSUBSECTION_HTTP_BINDING	= new PropertyHeader(SUBSUBSECTION_HTTP_BINDING, HEADER_SUBSECTION_HTTP_BINDINGS);

	// ---------------------------------------

	private static HTTPBindingProperties	handler								= null;

	// ------------ DEFAULT -------------------

	private ArrayList						defaultAddresses					= new ArrayList();

	private final int						DEFAULT_PORT						= 0;

	private int								defaultPort							= DEFAULT_PORT;

	private String							defaultSuffix						= null;

	private String							defaultAlias						= null;

	// ----------------------------------------

	private BuildUpProperties				buildUpBinding						= null;
	
	private final boolean					useUUIDPefix						=Boolean.parseBoolean(System.getProperty("JDPWS.HTTPBindingProperties.useUUIDPefix", "false"));
	
	private final String					suffixUUID							= PlatformSupport.getInstance().getToolkit().getUUID();



	HTTPBindingProperties() {
		super();
		if (handler != null) {
			throw new RuntimeException("HTTPBindingProperties: class already instantiated!");
		}
		handler = this;
	}

	static synchronized HTTPBindingProperties getInstance() {
		if (handler == null) {
			handler = new HTTPBindingProperties();
		}
		return handler;
	}

	/**
	 * * Detect a group of inet addresse strings by name. For example: value =
	 * "inet6@eth0" returns all ipv6 addresses from the interface "eth0".
	 * 
	 * @param value
	 * @return A String ArrayList with the group addresses
	 */
	private List getGroupByName(String value) 
	{
		//Moved to networkDetection resp. IPDetection
		NetworkDetection detection=PlatformSupport.getInstance().getToolkit().getNetworkDetection(NetworkDetection.IP_NETWORK);
		return detection.getGroupByName(value);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.configuration.PropertiesHandler#setProperties(org.ws4d.
	 * java.configuration.PropertyHeader, org.ws4d.java.configuration.Property)
	 */
	@Override
	public void setProperties(PropertyHeader header, Property property) {
		if (HEADER_SUBSECTION_HTTP_BINDINGS.equals(header)) {
			/*
			 * Properties of "HTTPBindings" Section, default for HTTPBindings
			 */
			if (PROP_ADDRESS.equals(property.key)) {
				defaultAddresses.add(property.value);
			} else if (PROP_ADDRESS_GROUP.equals(property.key)) {
				defaultAddresses.addAll(getGroupByName(property.value));

			} else if (PROP_PORT.equals(property.key)) {
				defaultPort = Integer.parseInt(property.value);
			} else if (PROP_SUFFIX.equals(property.key)) {
				defaultSuffix =(useUUIDPefix?suffixUUID:"")+property.value;
					
			} else if (PROP_ALIAS.equals(property.key)) {
				defaultAlias = property.value;
			}
		} else if (HEADER_SUBSUBSECTION_HTTP_BINDING.equals(header)) {
			/*
			 * Properties of "HTTPBinding" Section
			 */
			if (buildUpBinding == null) {
				buildUpBinding = new BuildUpProperties();
			}
			if (BindingProperties.PROP_BINDING_ID.equals(property.key)) {
				buildUpBinding.bindingId = Integer.valueOf(property.value);
			} else if (PROP_ADDRESS.equals(property.key)) {
				buildUpBinding.buildUpAddresses.add(property.value);
			} else if (PROP_ADDRESS_GROUP.equals(property.key)) {
				buildUpBinding.buildUpAddresses.addAll(getGroupByName(property.value));
			} else if (PROP_PORT.equals(property.key)) {
				buildUpBinding.buildUpPort = Integer.parseInt(property.value);
			} else if (PROP_SUFFIX.equals(property.key)) {
				buildUpBinding.buildUpSuffix = (useUUIDPefix?suffixUUID:"")+property.value;
			} else if (PROP_HTTPS.equals(property.key)){
				buildUpBinding.isHTTPS = Boolean.parseBoolean(property.value);
			} else if (PROP_ALIAS.equals(property.key)){
				buildUpBinding.buildUpAlias = property.value;
			}
			
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.configuration.PropertiesHandler#finishedSection(int)
	 */
	@Override
	public void finishedSection(int depth) {

		if (depth == 3 && buildUpBinding != null) {
			// initialize DeviceProperties
			if (!buildUpBinding.bindingId.equals(BindingProperties.DEFAULT_BINDING_ID)) {
				ArrayList t = buildUpBinding.createBindings();
				BindingProperties.getInstance().addCommunicationBinding(buildUpBinding.bindingId, t);
			} else {
				Log.error("HTTPBindingProperties: binding id not set: " + buildUpBinding);
			}
		}
		buildUpBinding = null;
	}

	private class BuildUpProperties {

		boolean 	isHTTPS 				= false;

		Integer		bindingId				= BindingProperties.DEFAULT_BINDING_ID;

		ArrayList	buildUpAddresses		= null;

		int			buildUpPort;

		String		buildUpSuffix;
		
		String 		buildUpAlias					;

		public BuildUpProperties() {
			buildUpAddresses = new ArrayList();
			buildUpPort = defaultPort;
			buildUpSuffix = defaultSuffix;
			buildUpAlias=defaultAlias;
		}

		public ArrayList createBindings() {
			ArrayList r = new ArrayList();
			if (buildUpAddresses.size() == 0) {
				buildUpAddresses = defaultAddresses;
			}
			for (int i = 0; i < buildUpAddresses.size(); i++) {
				NetworkDetection detection=PlatformSupport.getInstance().getToolkit().getNetworkDetection(NetworkDetection.IP_NETWORK);
				IPAddress ipAddress =null;
				Object o=buildUpAddresses.get(i);

				if (o instanceof String)
					ipAddress= detection.getIPAddress((String) o);
				else if (o instanceof IPAddress)
					ipAddress=(IPAddress)o;

				if (ipAddress != null) {
					if (isHTTPS)
					{
						HTTPSBinding binding=new HTTPSBinding(ipAddress, (buildUpPort == DEFAULT_PORT) ? Math.getRandomPortNumber() : buildUpPort, buildUpSuffix);
						if (buildUpAlias!=null)
							binding.setAlias(buildUpAlias);
						
						r.add(binding);
					}else{
						r.add(new HTTPBinding(ipAddress, (buildUpPort == DEFAULT_PORT) ? Math.getRandomPortNumber() : buildUpPort, buildUpSuffix));
					}
				}
			}
			return r;
		}

		@Override
		public String toString() {
			if (buildUpAddresses.size() == 0) {
				return "Not speziefied BuildUpProperties";
			}
			String s = "";
			int i;
			for (i = 0; i < (buildUpAddresses.size() - 1); i++)
				s = s + (String) buildUpAddresses.get(i) + " | ";
			return ("BindingId = " + bindingId + " | Addresses = [" + s + (String) buildUpAddresses.get(i) + "] " + "| Port = " + buildUpPort + " | Suffix = " + buildUpSuffix);
		}
	}
}

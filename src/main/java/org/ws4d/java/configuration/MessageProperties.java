/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.configuration;

import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.constants.WSDConstants;

public class MessageProperties implements PropertiesHandler {

	private MessageProperties()
	{
		//void
	}
	
	private static MessageProperties instance=new MessageProperties();
	
	public static MessageProperties getInstance() {
		return instance;
	}
	
	/**
	 * Size of message id buffer. The buffer is used to ignore multiplicated udp
	 * messages. <BR>
	 * Type: int <BR>
	 * Default: 50
	 */
	public static final String				PROP_MESSAGE_ID_BUFFER_SIZE			= "MessageIdBufferSize";
	
	/**
	 * Time to wait until a time exception is thrown, after a request was sent
	 * and no answer was received. <BR>
	 * Type: int <BR>
	 * Default: 10000
	 */
	public static final String				PROP_MATCH_WAIT_TIME				= "MatchWaitTime";


	/**
	 * Millis until a matching message (probe matches or resolve matches) will
	 * be handled.
	 */
	// TODO SSch ???? Move into DPWS properties
	private long							matchWaitTime						= WSDConstants.WSD_MATCH_TIMEOUT;

	@Override
	public void setProperties(PropertyHeader header, Property property) {
		if (Properties.HEADER_SECTION_MESSAGES.equals(header)) { 
			if (PROP_MATCH_WAIT_TIME.equals(property.key)) {
				matchWaitTime = Integer.parseInt(property.value);
			}
		}  else if (PROP_MESSAGE_ID_BUFFER_SIZE.equals(property.key)) {
			setMessageIdBufferSize(Integer.parseInt(property.value));
		}

	}

	@Override
	public void finishedSection(int depth) {
		//void
	}

	/**
	 * Gets time in millis for which the framework waits for a resolve matches
	 * or a probe matches message, before it throws a {@link TimeoutException}.
	 * 
	 * @return time in millis.
	 */
	public long getMatchWaitTime() {
		return matchWaitTime;
	}

	/**
	 * Sets time in millis for which the framework waits for a resolve matches
	 * or a probe matches message, before it throws a {@link TimeoutException}.
	 * 
	 * @param matchWaitTime time in millis.
	 */
	public void setMatchWaitTime(long matchWaitTime) {
		this.matchWaitTime = matchWaitTime;
	}
	
	/**
	 * Gets size of message id buffer. The buffer is used to ignore multiple
	 * received udp messages.
	 * 
	 * @return the size for message ID buffers
	 */
	public int getMessageIdBufferSize() {
		return msgIdBufferSize;
	}

	/**
	 * Sets size of message id buffer. The buffer is used to ignore multiple
	 * received udp messages.
	 * 
	 * @param msgIdBufferSize
	 */
	public void setMessageIdBufferSize(int msgIdBufferSize) {
		this.msgIdBufferSize = msgIdBufferSize;
	}
	
	/**
	 * Size of message id buffer. This buffer is used to ignore multiplicated
	 * udp messages.
	 */
	private int								msgIdBufferSize						= 50;


}

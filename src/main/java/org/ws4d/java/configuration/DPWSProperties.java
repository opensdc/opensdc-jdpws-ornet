/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.configuration;

import org.ws4d.java.communication.DPWSProtocolVersionInfo;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.DPWSConstants2006;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.StringUtil;

public class DPWSProperties implements PropertiesHandler {

	private static DPWSProperties				instance;

	/* ################# Native Router Properties ################### */

	/**
	 * Using the native udp router default: false
	 */
	public static final String					PROP_DPWS_ROUTER							= "UseCLDCUDPRouter";

	/**
	 * The routers ip default: 127.0.0.1
	 */
	public static final String					PROP_DPWS_ROUTER_ADDR						= "CLDCUDPRouterAddr";

	/**
	 * The routers port default: 1111
	 */
	public static final String					PROP_DPWS_ROUTER_PORT						= "CLDCUDPRouterPort";

	/* ###################### Connection Properties ################# */

	public static final String					PROP_DPWS_HTTP_SERVER_KEEPALIVE				= "HTTPServerKeepAlive";

	public static final String					PROP_DPWS_HTTP_CLIENT_KEEPALIVE				= "HTTPClientKeepAlive";

	public static final String					PROP_DPWS_HTTP_SERVER_AVOIDCHUNKED			= "HTTPServerAvoidChunked";

	public static final String					PROP_DPWS_HTTP_CLIENT_AVOIDCHUNKED			= "HTTPClientAvoidChunked";

	/**
	 * Time to wait for (next) request until server closes http connection.
	 */
	public static final String					PROP_DPWS_HTTP_SERVER_REQUEST_TIMEOUT		= "HTTPServerRequestTimeout";

	/**
	 * Time to wait for (next) request until client closes http connection.
	 */
	public static final String					PROP_DPWS_HTTP_CLIENT_REQUEST_TIMEOUT		= "HTTPClientRequestTimeout";

	/* ################## DPWS Version Properties ################### */

	/**
	 * Property key for supported DPWS Versions
	 */
	public static final String					PROP_DPWS_SUPPORTED_DPWS_VERSIONS			= "SupportedDPWSVersions";

	/**
	 * Property key for class name of the factory for Message2SOAPGenerator and
	 * Message2SOAPGenerator implementing classes.
	 */
	public static final String					PROP_DPWS_SOAPMSG_GENERATOR_FACTORY_CLASS	= "SOAPMessageGeneratorFactoryClass";

	// -------------------------------------------------------------------------------------------------

	public static final DPWSProtocolVersionInfo	DEFAULT_DPWS_VERSION						= DPWSProtocolVersionInfo.DPWS1_1;

	public DataStructure						supportedDPWSVersions						= new HashSet();

	/**
	 * Native Router for Communication over the CLDC Platform
	 */

	private boolean								useNativeRouter								= false;

	private String								routerIp									= "127.0.0.1";

	private int									routerPort									= 1111;

	/**
	 * Indicates whether this server should keep the connection. XXX if
	 * timeout.keepAlive == true, don#t works with CLDC (no multitasking) -
	 * Problem: MetaData timeout Problem occurs in HTTPServer on line 360
	 * (while(timeout.keepAlive() || firstRequest) {)
	 */
	private boolean								httpServerKeepAlive							= true;

	/**
	 * Indicates whether this client should keep the connection.
	 */
	private boolean								httpClientKeepAlive							= true;

	/**
	 * This field allows to avoid HTTP chunk mode for the HTTP server.
	 * <p>
	 * If set <code>true</code> no chunks will be sent. The stream will be
	 * wrapped and stored into a buffer until the body is done with. After that
	 * it will be sent as a single part with no chunks. Default is
	 * <code>false</code>.
	 * </p>
	 */
	private boolean								httpServerAvoidChunked						= false;

	/**
	 * This field allows to avoid HTTP chunk mode for the HTTP client.
	 * <p>
	 * If set <code>true</code> no chunks will be sent. The stream will be
	 * wrapped and stored into a buffer until the body is done with. After that
	 * it will be sent as a single part with no chunks. Default is
	 * <code>false</code>.
	 * </p>
	 */
	private boolean								httpClientAvoidChunked						= false;

	/**
	 * This field specifies the time until the HTTP Server closes a connection
	 * while not receiving a request.
	 */
	private long								httpServerRequestTimeout					= 20000;

	/**
	 * This field specifies the time until the HTTP Client closes a connection
	 * while not sending a request.
	 */
	private long								httpClientRequestTimeout					= 20000;

	/**
	 * Class name of the factory for soap from/to message generating classes.
	 */
	private String								soapMessageGeneratorFactoryClass			= null;

	public static DPWSProperties getInstance() {
		return (instance == null ? (instance = new DPWSProperties()) : instance);
	}

	private DPWSProperties() {
		//supportedDPWSVersions.add(DPWSProtocolVersionInfo.DPWS1_1);
		//supportedDPWSVersions.add(DPWSProtocolVersionInfo.DPWS2006);
	}

	public boolean getNativeRouter() {
		return useNativeRouter;
	}

	public String getNativeRouterIp() {
		return routerIp;
	}

	public int getNativeRouterPort() {
		return routerPort;
	}

	public boolean getHTTPServerKeepAlive() {
		return httpServerKeepAlive;
	}

	public boolean getHTTPClientKeepAlive() {
		return httpClientKeepAlive;
	}

	public boolean getHTTPServerAvoidChunked() {
		return httpServerAvoidChunked;
	}

	public boolean getHTTPClientAvoidChunked() {
		return httpClientAvoidChunked;
	}

	public long getHTTPServerRequestTimeout() {
		return httpServerRequestTimeout;
	}

	public long getHTTPClientRequestTimeout() {
		return httpClientRequestTimeout;
	}

	public String getSOAPMessageGeneratorFactoryClass() {
		return soapMessageGeneratorFactoryClass;
	}

	public void setNativeRouterPort(int port) {
		routerPort = port;
	}

	public void setNativeRouterIp(String ip) {
		routerIp = ip;
	}

	public void setNativeRouter(boolean b) {
		useNativeRouter = b;
	}

	public void setHTTPServerKeepAlive(boolean b) {
		httpServerKeepAlive = b;
	}

	public void setHTTPClientKeepAlive(boolean b) {
		httpClientKeepAlive = b;
	}

	public void setHTTPServerAvoidChunked(boolean b) {
		httpServerAvoidChunked = b;
	}

	public void setHTTPClientAvoidChunked(boolean b) {
		httpClientAvoidChunked = b;
	}

	public void setHTTPServerRequestTimeout(long timeout) {
		httpServerRequestTimeout = timeout;
	}

	public void setHTTPClientRequestTimeout(long timeout) {
		httpClientRequestTimeout = timeout;
	}

	public void setSOAPMessageGeneratorFactoryClass(String className) {
		soapMessageGeneratorFactoryClass = className;
	}

	public void addSupportedDPWSVersion(DPWSProtocolVersionInfo versionInfo) {
		supportedDPWSVersions.add(versionInfo);
	}

	public void removeSupportedDPWSVersion(DPWSProtocolVersionInfo versionInfo) {
		supportedDPWSVersions.remove(versionInfo);
	}

	public HashSet getSupportedDPWSVersions() {
		if (supportedDPWSVersions != null && supportedDPWSVersions.size()<1) {
			supportedDPWSVersions.add(DEFAULT_DPWS_VERSION);
		}
		return (HashSet) supportedDPWSVersions;
	}

	private void setSupportedDPWSVersions(String value) {
		if (value != null && !value.equals("")) {
			String[] tmp = StringUtil.split(value, ',');
			//Bugfix SSc 2011-01-13 Must be less and not less than
			for (int i = 0; i < tmp.length; i++) {
				String val = tmp[i].trim();
				if (StringUtil.equalsIgnoreCase(DPWSConstants.DPWS_2009_NAME, val)) {
					supportedDPWSVersions.add(DPWSProtocolVersionInfo.DPWS1_1);
				} else if (StringUtil.equalsIgnoreCase(DPWSConstants2006.DPWS_2006_NAME, val)) {
					supportedDPWSVersions.add(DPWSProtocolVersionInfo.DPWS2006);
				} else {
					throw new RuntimeException("Unrecognized DPWS Version in Properties defined, known values are: 'DPWS1.1', 'DPWS2006' or both (comma separated).");
				}
			}
		} else {
			throw new RuntimeException("No Supported Version in Properties defined, for example use DPWS1.1, DPWS2006 or both (comma separated).");
		}
	}

	@Override
	public void finishedSection(int depth) {
		// TODO implement me!
		Log.debug("Finished DPWS Props Section:"+ printSupportedDPWSVersions()+" "+depth);
	}

	public String printSupportedDPWSVersions() {
		HashSet set = getSupportedDPWSVersions();
		StringBuffer string = new StringBuffer();
		string.append("Supported DPWS Version(s): ");
		Iterator it = set.iterator();
		while (it.hasNext()) {
			string.append(((DPWSProtocolVersionInfo) it.next()).getDisplayName());
			if (it.hasNext()) {
				string.append(", ");
			}
		}
		return string.toString();
	}

	@Override
	public void setProperties(PropertyHeader header, Property property) {
		if (Properties.HEADER_SECTION_DPWS.equals(header)) {
			try {
				if (PROP_DPWS_ROUTER.equals(property.key)) {
					useNativeRouter = property.value.equals("true");
				} else if (PROP_DPWS_ROUTER_ADDR.equals(property.key)) {
					routerIp = property.value;
				} else if (PROP_DPWS_ROUTER_PORT.equals(property.key)) {
					routerPort = Integer.parseInt(property.value);
				} else if (PROP_DPWS_HTTP_SERVER_KEEPALIVE.equals(property.key)) {
					setHTTPServerKeepAlive(property.value.equals("true"));
				} else if (PROP_DPWS_HTTP_CLIENT_KEEPALIVE.equals(property.key)) {
					setHTTPClientKeepAlive(property.value.equals("true"));
				} else if (PROP_DPWS_HTTP_SERVER_AVOIDCHUNKED.equals(property.key)) {
					setHTTPServerAvoidChunked(property.value.equals("true"));
				} else if (PROP_DPWS_HTTP_CLIENT_AVOIDCHUNKED.equals(property.key)) {
					setHTTPClientAvoidChunked(property.value.equals("true"));
				} else if (PROP_DPWS_SUPPORTED_DPWS_VERSIONS.equals(property.key)) {
					setSupportedDPWSVersions(property.value);
				} else if (PROP_DPWS_HTTP_CLIENT_REQUEST_TIMEOUT.equals(property.key)) {
					setHTTPClientRequestTimeout(Long.parseLong(property.value));
				} else if (PROP_DPWS_HTTP_SERVER_REQUEST_TIMEOUT.equals(property.key)) {
					setHTTPServerRequestTimeout(Long.parseLong(property.value));
				} else if (PROP_DPWS_SOAPMSG_GENERATOR_FACTORY_CLASS.equals(property.key)) {
					setSOAPMessageGeneratorFactoryClass(property.value);
				}else if (PROP_APPLICATION_MAX_DELAY.equals(property.key)) {
					appMaxDelay = Integer.parseInt(property.value);
				}
			} catch (NumberFormatException e) {
				Log.info(e);
			}
		}
	}
	
	
	//From DispatchingProperties
	
	/**
	 * Amount of time in millis dispatcher will wait to send probe matches and
	 * hello messages. <BR>
	 * Type: long <BR>
	 * Default: 2500
	 */
	public static final String				PROP_APPLICATION_MAX_DELAY			= "ApplicationMaxDelay";
	


	
	/**
	 * Max millis waiting before sending a hello or probe matches.
	 */
	private int								appMaxDelay							= DPWSConstants.DPWS_APP_MAX_DELAY;
	


	/**
	 * Gets maximum time in millis, before a hello or probe matches message is
	 * sent again.
	 * 
	 * @return time in millis
	 */
	public int getApplicationMaxDelay() {
		return appMaxDelay;
	}

	/**
	 * Sets maximum time in millis, before a hello or probe matches message is
	 * sent again.
	 * 
	 * @param appMaxDelay time in millis
	 */
	public void setApplicationMaxDelay(int appMaxDelay) {
		this.appMaxDelay = appMaxDelay;
	}
	
	
}
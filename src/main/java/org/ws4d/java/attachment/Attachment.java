/**********************************************************************************
 * Copyright (c) 2008 MATERNA Information & Communications and TU Dortmund, Dpt.
 * of Computer Science, Chair 4, Distributed Systems All rights reserved. This
 * program and the accompanying materials are made available under the terms of
 * the Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 **********************************************************************************/
package org.ws4d.java.attachment;

import java.io.IOException;
import java.io.InputStream;

import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.types.InternetMediaType;

/**
 * Attachment container. Provides access to metadata (content ID, content type,
 * transfer encoding) and raw data of an attachment.
 * <p>
 * There are two different ways of obtaining raw data from an
 * <code>Attachment</code>. The first one, {@link #getInputStream()}, allows the
 * reading of a stream-based representation of the attachment's bytes, whereas
 * the second one, {@link #getBytes()}, returns them within a byte array. While
 * in some cases being faster (depending on the actual <code>Attachment</code>
 * implementation), the later approach is subject to certain limitations
 * resulting from the finite amount of memory available within the current
 * runtime.
 * </p>
 * <p>
 * Most methods of this interface are indicated to throw
 * {@link AttachmentException}s if obtaining the requested attachment data or
 * property failed for any reason. This could be the result e.g. of a network
 * failure while reading the attachment or an unexpected or erroneous attachment
 * format, or it could mean that attachment support is not available within the
 * current DPWS framework configuration.
 * </p>
 * <p>
 * The actual type of attachment can be queried by means of method
 * {@link #getType()}. Depending on the value returned (see
 * {@link #STREAM_ATTACHMENT}, {@link #FILE_ATTACHMENT} and
 * {@link #MEMORY_ATTACHMENT}), some methods like {@link #getInputStream()},
 * {@link #getBytes()} and {@link #size()} may behave differently. Additionally,
 * other methods like {@link #save(String)}, {@link #move(String)} or
 * {@link #getFilePath()} may not work at all.
 * </p>
 */
public interface Attachment {

	/**
	 * This attachment will behave like a {@link StreamAttachment}. Moreover, it
	 * will <strong>not</strong> support the {@link #save(String)},
	 * {@link #move(String)} and {@link #getFilePath()} operations.
	 * <strong>Note</strong> that checking that an attachment is of this type is
	 * <strong>not</strong> sufficient in order to safely cast it to a
	 * {@link StreamAttachment}. Use an <code>instanceof</code> expression
	 * instead.
	 */
	public static final int	STREAM_ATTACHMENT	= 1;

	/**
	 * This attachment will behave like a {@link MemoryAttachment}. Moreover, it
	 * will <strong>not</strong> support the {@link #save(String)},
	 * {@link #move(String)} and {@link #getFilePath()} operations.
	 * <strong>Note</strong> that checking that an attachment is of this type is
	 * <strong>not</strong> sufficient to safely cast it to a
	 * {@link MemoryAttachment}. Use an <code>instanceof</code> expression
	 * instead.
	 */
	public static final int	MEMORY_ATTACHMENT	= 2;

	/**
	 * This attachment will behave like a {@link FileAttachment}. Moreover, it
	 * will support the {@link #save(String)}, {@link #move(String)} and
	 * {@link #getFilePath()} operations. <strong>Note</strong> that checking
	 * that an attachment is of this type is <strong>not</strong> sufficient to
	 * safely cast it to a {@link FileAttachment}. Use an
	 * <code>instanceof</code> expression instead.
	 */
	public static final int	FILE_ATTACHMENT		= 3;

	/**
	 * Return the content ID of this attachment. The content ID is used to
	 * distinguish the attachment within a MIME package and to enable linking to
	 * it from within {@link ParameterValue} instances.
	 * 
	 * @return this attachment's content ID
	 */
	public String getContentId();

	/**
	 * The encoding for this attachment.
	 * 
	 * @return the encoding
	 * @throws AttachmentException if attachment processing is not supported
	 *             within the current runtime or obtaining the attachment failed
	 *             for any reason
	 */
	public String getTransferEncoding() throws AttachmentException;

	/**
	 * The content type (MIME) for this attachment.
	 * 
	 * @return the content type
	 * @throws AttachmentException if attachment processing is not supported
	 *             within the current runtime or obtaining the attachment failed
	 *             for any reason
	 */
	public InternetMediaType getContentType() throws AttachmentException;

	/**
	 * Returns <code>true</code> if this attachment is ready to be processed.
	 * <p>
	 * Because the DPWS framework handles attachments asynchronously, it is
	 * possible (and will most likely occur frequently) for an attachement's raw
	 * data to still be being transmitted over the network while a caller's
	 * business logic  method)
	 * gets called providing access to the attachment by means of its
	 * {@link ParameterValue#getAttachment() parameters}. Using methods other
	 * than {@link #getContentId()} or {@link #isAvailable()} on this attachment
	 * would block the caller until the entire attachment is read out. Thus,
	 * this method allows to check whether further examination of this
	 * attachment would block it or not.
	 * </p>
	 * 
	 * @return <code>true</code>, if this attachment is already available,
	 *         <code>false</code> if it is still not received (entirely)
	 */
	public boolean isAvailable();

	/**
	 * Returns this attachment's type.
	 * <p>
	 * The value returned distinguishes between different implementations and
	 * thereby different storage models for the attachment's raw data, such as
	 * in-memory, on file system or as (opaque) input stream. It further
	 * determines which ways of obtaining the raw data are suitable/possible for
	 * this instance (e.g. by means of {@link #getBytes()} or
	 * {@link #getInputStream()}). Also, usage/availability of some operations
	 * like {@link #save(String)}, {@link #move(String)} and
	 * {@link #getFilePath()} depends on the type of attachment. On some types,
	 * these operations will not be supported at all and will always throw an
	 * {@link AttachmentException}.
	 * 
	 * @return the type of this attachment instance
	 * @throws AttachmentException if attachment processing is not supported
	 *             within the current runtime or obtaining the attachment failed
	 *             for any reason
	 * @see #STREAM_ATTACHMENT
	 * @see #FILE_ATTACHMENT
	 * @see #MEMORY_ATTACHMENT
	 */
	public int getType() throws AttachmentException;

	/**
	 * Returns the raw data from this attachment as array of bytes.
	 * <p>
	 * <strong>WARNING:</strong> The result can potentially use a large amount
	 * of memory. Furthermore, some environments or some types of attachments
	 * (see e.g. {@link StreamAttachment} or {@link FileAttachment}) may not
	 * support representing the attachment's raw data as a byte array. In these
	 * cases, a call to this method will cause an {@link AttachmentException} to
	 * get thrown (see {@link #getType()}.
	 * </p>
	 * 
	 * @return the attachment data as a byte array
	 * @throws AttachmentException if attachment processing is not supported
	 *             within the current runtime or obtaining the attachment failed
	 *             for any reason or in particular, when this attachment type
	 *             doesn't support byte array access to its raw data
	 * @throws IOException if reading raw attachment data failed
	 * @see #getType()
	 * @see #getInputStream()
	 */
	public byte[] getBytes() throws AttachmentException, IOException;

	/**
	 * Returns the input stream which contains the data.
	 * <p>
	 * Depending on the actual attachment implementation, this method may either
	 * always return the same <code>java.io.InputStream</code> instance, or it
	 * could create a new one on each call. In the first case, it is important
	 * to note that reading the attachment data might be possible only once, as
	 * the returned stream is not guaranteed to support resetting (see
	 * {@link #getType()}.
	 * </p>
	 * 
	 * @return an input stream to this attachment's raw data
	 * @throws AttachmentException if attachment processing is not supported
	 *             within the current runtime or obtaining the attachment failed
	 *             for any reason
	 * @throws IOException if reading raw attachment data failed
	 * @see #getType()
	 * @see #size()
	 * @see #getBytes()
	 */
	public InputStream getInputStream() throws AttachmentException, IOException;

	/**
	 * Returns the size of the attachment in bytes.
	 * <p>
	 * Some types of attachment (e.g. {@link StreamAttachment}) may not be aware
	 * of their actual size; in such cases, this method will either return zero
	 * or a potentially inaccurate estimate (as provided by
	 * <code>java.io.InputStream.available()</code>).
	 * </p>
	 * 
	 * @return the size of this attachment's raw data
	 * @throws AttachmentException if attachment processing is not supported
	 *             within the current runtime or obtaining the attachment failed
	 *             for any reason
	 * @see #getType()
	 * @see #getInputStream()
	 */
	public long size() throws AttachmentException;

	/**
	 * Returns <code>true</code>, if this attachment was created locally. This
	 * is synonymous to the sender/originator of this attachment instance
	 * residing within the same Java virtual machine as its receiver.
	 * <p>
	 * This method is especially important for attachments of {@link #getType()
	 * type} {@link #FILE_ATTACHMENT}, as - if it returns <code>true</code> -
	 * this denotes that the {@link #getFilePath() file} the attachment points
	 * to is the original one. That is, when attempting to {@link #move(String)}
	 * a file attachment, this allows to distinguish whether the original file
	 * would be moved to a new location or whether simply the attachment file
	 * will be moved out of the local attachment store (used for caching
	 * incoming attachments) to a place outside of it.
	 * 
	 * @return <code>true</code> only if this attachment was originally created
	 *         locally (within the same JVM)
	 */
	public boolean isLocal();

	/**
	 * Returns the path to the file encapsulated by this attachment. This method
	 * is only legal for attachments of {@link #getType() type}
	 * {@link #FILE_ATTACHMENT}. For any other types, it will throw an
	 * {@link AttachmentException}.
	 * 
	 * @return the path to the file containing the attachments raw data
	 */
	public String getFilePath() throws AttachmentException;

	/**
	 * Copies the attachment's raw data to the target file path (e.g.
	 * "C:/folder/file.gif"). This method can only be used for attachments of
	 * {@link #getType() type} {@link #FILE_ATTACHMENT}. In any other cases it
	 * will throw an {@link AttachmentException}.
	 * 
	 * @param targetFilePath the new path within the local file system to store
	 *            the file to
	 * @throws AttachmentException if attachment processing is not supported
	 *             within the current runtime or obtaining the attachment failed
	 *             for any reason
	 * @throws IOException in case writing to the local file system failed for
	 *             any reason
	 */
	public void save(String targetFilePath) throws AttachmentException, IOException;

	/**
	 * This method moves the file containing the attachment's raw data to the
	 * given target file path (e.g. "C:/folder/file.gif"). It can only be used
	 * for attachments of {@link #getType() type} {@link #FILE_ATTACHMENT}. In
	 * any other cases it will throw an {@link AttachmentException}.
	 * 
	 * @param newFilePath the new path within the local file system to move the
	 *            file to
	 * @return <code>true</code>, if the file was moved/renamed successfully,
	 *         <code>false</code> otherwise
	 * @throws AttachmentException if attachment processing is not supported
	 *             within the current runtime or obtaining the attachment failed
	 *             for any reason
	 */
	public boolean move(String newFilePath) throws AttachmentException;

	/**
	 * Disposes of the attachment and - if possible - frees any resources such
	 * as volatile and/or non volatile memory it uses. After calling this
	 * method, access to this attachment's metadata and raw data will no longer
	 * be possible!
	 */
	public void dispose();

}

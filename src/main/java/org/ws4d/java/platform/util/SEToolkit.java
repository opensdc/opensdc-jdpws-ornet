/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.platform.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;

import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.connection.ip.IPDetection;
import org.ws4d.java.communication.connection.ip.NetworkDetection;
import org.ws4d.java.communication.connection.tcp.SocketFactorySE;
import org.ws4d.java.communication.connection.udp.DatagramSocketFactorySE;
import org.ws4d.java.concurrency.DefaultThreadPool;
import org.ws4d.java.concurrency.ThreadPool;
import org.ws4d.java.configuration.FrameworkProperties;
import org.ws4d.java.constants.FrameworkConstants;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.io.fs.FileSystem;
import org.ws4d.java.platform.DatagramSocketFactory;
import org.ws4d.java.platform.SocketFactory;
import org.ws4d.java.platform.Toolkit;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

/**
 * 
 */
public final class SEToolkit implements Toolkit {

	
	private volatile boolean	shutdownAdded	= false;
	private ThreadPool threadPool;
	private static volatile Object			LOCAL_FILE_SYSTEM_LOCK		= new Object();
	private FileSystem						localFileSystem				= null;
	private SocketFactory 					socketFactory 				= new SocketFactorySE();
	private DatagramSocketFactory 			datagramSocketFactory		= new DatagramSocketFactorySE();
	private Object							securityManager				= new org.ws4d.java.security.DPWSSecurityManagerSE();
	private long coldBootCounter=0;
	private final SecureRandom secureRandom= new SecureRandom();
	protected static final int exitCodeOnKill = Integer.parseInt(System.getProperty("JDPWS.SEToolkit.ExitCodeOnKill", "0"));

	/**
	 * 
	 */
	public SEToolkit() {
		super();
		addShutdownHook();
		addAndGetColdBootCounter();

	}

	private void addAndGetColdBootCounter() {
		boolean useColdBootCount=Boolean.parseBoolean(System.getProperty("MDPWS.PlatformSE.UseColdBootCount", "true"));
		String cbcFileName=System.getProperty("MDPWS.PlatformSE.ColdBootCountFileName", "_ColdBootCount_.prop");
		if (useColdBootCount)
		{
			long cbc=-1;
			try {
				FileSystem fs = getLocalFileSystem();

				try{
					InputStream is = fs.readFile(cbcFileName);
					BufferedReader in=new BufferedReader(new InputStreamReader(is));
					cbc=Long.parseLong(in.readLine());
					is.close();
				}catch(Exception e){
					//void
				}finally{
					cbc++;
					OutputStream out = fs.writeFile(cbcFileName);
					byte[] cbcBytes=Long.toString(cbc).getBytes();
					out.write(cbcBytes);
					out.flush();
					out.close();
				}
			} catch (IOException e1) {
				Log.warn(e1);
			}
			this.coldBootCounter=cbc;
		}
	}

	private synchronized void addShutdownHook() {
		if (shutdownAdded) {
			return;
		}
		Thread t = new Thread() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				if (FrameworkProperties.getInstance().getKillOnShutdownHook()) {
					FrameworkModuleRegistry.getInstance().kill();

					/*
					 * Allow the framework to do its job for one second. After
					 * that time the framework and the JavaVM is killed.
					 */
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (Log.isDebug()) {
						Log.debug("Killing DPWS Framework and JavaVM");
					}
					Runtime.getRuntime().halt(exitCodeOnKill);
				} else {
					FrameworkModuleRegistry.getInstance().stop();

				}
			}

		};
		Runtime.getRuntime().addShutdownHook(t);
		shutdownAdded = true;
	}

	@Override
	public void printStackTrace(PrintStream err, Throwable t) {
		t.printStackTrace(err);
	}

	@Override
	public ThreadPool getThreadPool() 
	{
		return this.threadPool;
	}

	@Override
	public InputStream getResourceAsStream(URI location) throws IOException {
		/*
		 * We can load any file from file system or resource before the
		 * framework is up and running
		 */
		if (location == null) throw new IOException("What?! Cannot find 'null' file. Maybe /dev/null took it.");
		if (location.getSchema()!=null && location.getSchema().startsWith(FrameworkConstants.SCHEMA_LOCAL)) {
			String file = location.toString().substring(FrameworkConstants.SCHEMA_LOCAL.length() + 1);
			InputStream in = location.getClass().getResourceAsStream(file);
			if (in == null) {
				try {
					FileSystem fs = getLocalFileSystem();
					return fs.readFile(file);
				} catch (IOException e) {
					Log.warn(e.getMessage());
					return null;
				}
			}
			return in;
		}
		if (location.getSchema()==null || location.getSchema().startsWith("file")) {
			try {
				FileSystem fs = getLocalFileSystem();
				return fs.readFile(location.getPath());
			} catch (IOException e) {
				Log.warn(e.getMessage());
				return null;
			}
		}

		if (location.toString().startsWith("jar:file"))
		{
			String locationStr = location.toString();
			int jarSepIndex = locationStr.indexOf("!/");
			String resourceLocation = locationStr.substring(jarSepIndex+2);
			return ClassLoader.getSystemResourceAsStream(resourceLocation);
		}

		/*
		 * This part should be done only if every thing is up and running
		 */
		if (FrameworkModuleRegistry.getInstance().isRunning()) {
			for (org.ws4d.java.structures.Iterator it = CommunicationManagerRegistry.getLoadedManagers(); it.hasNext();) {
				CommunicationManager manager = (CommunicationManager) it.next();
				InputStream in = manager.getResourceAsStream(location);
				if (in != null) {
					return in;
				}
			}
		} else {
			Log.warn("Framework could not load the given location before everything is up and running.");
		}
		return null;
	}


	@Override
	public FileSystem getLocalFileSystem() throws IOException {
		synchronized (LOCAL_FILE_SYSTEM_LOCK) {
			if (localFileSystem == null) {
				try {
					Class<?> clazz = Class.forName("org.ws4d.java.platform.io.fs.SEFileSystem");
					localFileSystem = (FileSystem) clazz.newInstance();
				} catch (Exception e) {
					throw new IOException("The current runtime configuration doesn't contain support for a local file system."+e.toString());
				}

			}
			return localFileSystem;
		}
	}


	@Override
	public NetworkDetection getNetworkDetection(String protocol) 
	{
		NetworkDetection detection=null;
		if (NetworkDetection.IP_NETWORK.equals(protocol)){
			detection=new IPDetection();
		}
		return detection;
	}


	@Override
	public SocketFactory getSocketFactory() {
		return this.socketFactory;
	}


	@Override
	public DatagramSocketFactory getDatagramSocketFactory() {
		return this.datagramSocketFactory;
	}


	@Override
	public synchronized void init(int threadPoolSize) 
	{
		if (threadPool!=null)
			this.threadPool.shutdown();

		this.threadPool=new DefaultThreadPool(threadPoolSize);
	}


	@Override
	public synchronized void shutdown() {
		if (threadPool!=null)
			this.threadPool.shutdown();

		threadPool = null;
	}


	@Override
	public Object getSecurityManager() {
		return this.securityManager;
	}

	@Override
	public long getColdBootCount() {
		//see http://docs.oasis-open.org/ws-dd/discovery/1.1/os/wsdd-discovery-1.1-spec-os.html#_Toc234231847
		//		return System.currentTimeMillis() / 1000;
		long coldBootCount=this.coldBootCounter<1?(System.currentTimeMillis() / 1000):this.coldBootCounter;
		this.coldBootCounter=coldBootCount;
		return coldBootCount;
	}

	@Override
	public Random getRandom() {
		return secureRandom;
	}

	@Override
	public String getUUID() {
		return UUID.randomUUID().toString();
	}

}

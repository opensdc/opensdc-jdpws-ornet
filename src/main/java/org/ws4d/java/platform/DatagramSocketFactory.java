/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.platform;

import java.io.IOException;
import java.net.NetworkInterface;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.udp.DatagramSocket;

public interface DatagramSocketFactory 
{
	public abstract DatagramSocket createDatagramSocket(IPAddress host, int port, String ifaceName) throws IOException;
	public abstract DatagramSocket createDatagramSocket(IPAddress host, int port, NetworkInterface iface) throws IOException;
	
	public abstract DatagramSocket createDatagramServerSocket(IPAddress host, int port, String ifaceName) throws IOException ;
	public abstract DatagramSocket createDatagramServerSocket(IPAddress host, int port, NetworkInterface ifaceName) throws IOException ;
	
	public abstract DatagramSocket registerMulticastGroup(IPAddress host, int port, String ifaceName) throws IOException;
	public abstract DatagramSocket registerMulticastGroup(IPAddress host, int port, NetworkInterface ifaceName) throws IOException;
}

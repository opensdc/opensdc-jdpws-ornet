/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.platform;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Random;

import org.ws4d.java.communication.connection.ip.NetworkDetection;
import org.ws4d.java.concurrency.ThreadPool;
import org.ws4d.java.io.fs.FileSystem;
import org.ws4d.java.types.URI;

/**
 * This utility class includes a collection of methods, which are specific to
 * different Java Editions.
 */
public interface Toolkit {

	/**
	 * Implementation of the stack trace logging.
	 * 
	 * @param err Stream to, if possible, print the stack trace on.
	 * @param t Throwable to print.
	 */
	public abstract void printStackTrace(PrintStream err, Throwable t);

	public abstract ThreadPool getThreadPool();
	

	public abstract InputStream getResourceAsStream(URI fromUri) throws IOException;
	
	public abstract FileSystem getLocalFileSystem() throws IOException;
	
	public abstract NetworkDetection getNetworkDetection(String protocol); //e.g. "IP"

	public abstract SocketFactory getSocketFactory();

	public abstract DatagramSocketFactory getDatagramSocketFactory();

	public abstract void init(int threadPoolSize);
	
	public abstract void shutdown();

	public abstract Object getSecurityManager();

	public abstract long getColdBootCount();

	public abstract Random getRandom();

	public abstract String getUUID();

}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.platform;


public class PlatformSupport {

	private static final PlatformSupport instance=new PlatformSupport();
	private volatile Toolkit toolkit;
	
	private PlatformSupport() {
		super();
	}

	public static PlatformSupport getInstance() {
		return instance;
	}
	
	
	public void setToolkit(Toolkit toolkit) {
		this.toolkit = toolkit;
	}

	public Toolkit getToolkit() {
		return toolkit;
	}
	
	/**
	 * checks whether the current framework instance is running on top of the
	 * CLDC library. Returns <code>true</code> if this is the case.
	 * 
	 * @return <code<true</code> in case the framework runs on top of the Java
	 *         CLDC configuration
	 */
	public static boolean onCldcLibrary() {
		try {
			Class.forName("com.sun.cldc.io.ConnectionBase");
			return true;
		} catch (Throwable t) {
			return false;
		}
	}


	
}

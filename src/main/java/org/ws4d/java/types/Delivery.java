/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.types;

import org.ws4d.java.constants.WSEConstants;

/**
 * 
 */
public class Delivery extends UnknownDataContainer {

	public static final URI		PUSH_DELIVERY	= new URI(WSEConstants.WSE_DELIVERY_MODE_PUSH);

	private URI					mode;

	private EndpointReference	notifyTo;

	/**
	 * 
	 */
	public Delivery() {
		this(null, null);
	}

	/**
	 * @param mode
	 * @param notifyTo
	 */
	public Delivery(URI mode, EndpointReference notifyTo) {
		super();
		this.mode = mode;
		this.notifyTo = notifyTo;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Delivery [ mode=").append(mode);
		sb.append(", notifyTo=").append(notifyTo);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.Delivery#getDeliveryMode()
	 */
	public URI getMode() {
		if (mode == null) return PUSH_DELIVERY;
		return mode;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.Delivery#getNotifyTo()
	 */
	public EndpointReference getNotifyTo() {
		return notifyTo;
	}

	/**
	 * @param mode the mode to set
	 */
	public void setMode(URI mode) {
		this.mode = mode;
	}

	/**
	 * @param notifyTo the notifyTo to set
	 */
	public void setNotifyTo(EndpointReference notifyTo) {
		this.notifyTo = notifyTo;
	}

//	public static Delivery parse(ElementParser parser, Object helper) throws XmlPullParserException, IOException {
//		return null;
//	}
//	public void serialize(XmlSerializer serializer, Object helper) throws IllegalArgumentException, IllegalStateException, IOException {
//		
//	}
//	public static Delivery parse(ElementParser parser, ConstantsHelper helper) throws XmlPullParserException, IOException {
//		Delivery delivery = new Delivery();
//
//		int attributeCount = parser.getAttributeCount();
//		for (int i = 0; i < attributeCount; i++) {
//			String namespace = parser.getAttributeNamespace(i);
//			if ("".equals(namespace)) {
//				// default to namespace of containing element
//				namespace = parser.getNamespace();
//			}
//			String name = parser.getAttributeName(i);
//			String value = parser.getAttributeValue(i);
//			if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace) && WSEConstants.WSE_ATTR_DELIVERY_MODE.equals(name)) {
//				delivery.setMode(new URI(value));
//			} else {
//				delivery.addUnknownAttribute(QNameFactory.getInstance().getQName(name, namespace), value);
//			}
//		}
//
//		while (parser.nextTag() != XmlPullParser.END_TAG) {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
//				if (WSEConstants.WSE_ELEM_NOTIFYTO.equals(name)) {
//					delivery.setNotifyTo(DPWSUtil.parseEndpointReference(helper.getDPWSVersion(), parser));
//				} else {
//					delivery.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				delivery.parseUnknownElement(parser, namespace, name);
//			}
//		}
//		return delivery;
//	}
//
//	public void serialize(XmlSerializer serializer, ConstantsHelper helper) throws IllegalArgumentException, IllegalStateException, IOException {
//		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_DELIVERY);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// Add the DeliveryMode
//		if (mode != null) {
//			serializer.attribute(null, WSEConstants.WSE_ATTR_DELIVERY_MODE, mode.toString());
//		}
//		// Add the EPR
//		notifyTo.serialize(serializer, helper, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_NOTIFYTO);
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_DELIVERY);
//	}
}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.types;

import org.ws4d.java.util.StringUtil;

public class HostMData extends UnknownDataContainer {

	protected EndpointReference	endpoint;

	protected QNameSet			types;

	// protected Map mapHostMetadata_QN_2_MElement;
	//
	// // lazy initialization
	// protected Map mapAttributeMap_QN_2_MEAttribute;

	public HostMData() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ endpoint=").append(endpoint);
		sb.append(", types=").append(types);
		sb.append(" ]");
		return sb.toString();
	}

	// ---------------------- GETTER ------------------------

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.Host#getEndpointReferences()
	 */
	public EndpointReference getEndpointReference() {
		return endpoint;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.Host#getTypes()
	 */
	public QNameSet getTypes() {
		return types;
	}

	// --------------------------- SETTER ----------------------

	/**
	 * Sets endpoint references.
	 * 
	 * @param endpoint
	 */
	public void setEndpointReference(EndpointReference endpoint) {
		this.endpoint = endpoint;
	}

	/**
	 * Sets port types.
	 * 
	 * @param qnTypes
	 */
	public void setTypes(QNameSet qnTypes) {
		this.types = qnTypes;
	}

//	public static HostMData parse(ElementParser parser, Object helper) throws XmlPullParserException, IOException {
//		return null;
//	}
//	public void serialize(XmlSerializer serializer, Object helper) throws IllegalArgumentException, IllegalStateException, IOException {
//		
//	}
//	public static HostMData parse(ElementParser parser, ConstantsHelper helper) throws XmlPullParserException, IOException {
//		HostMData host = new HostMData();
//
//		host.parseUnknownAttributes(parser);
//
//		int event = parser.nextTag();
//		if (event == XmlPullParser.END_TAG) {
//			throw new XmlPullParserException("Host is empty");
//		}
//		do {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (helper.getWSANamespace().equals(namespace)) {
//				if (WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE.equals(name)) {
//					host.setEndpointReference(DPWSUtil.parseEndpointReference(helper.getDPWSVersion(), parser));
//				} else {
//					host.parseUnknownElement(parser, namespace, name);
//				}
//			} else if (helper.getDPWSNamespace().equals(namespace)) {
//				if (DPWSConstants.DPWS_ELEM_TYPES.equals(name)) {
//					host.setTypes(QNameSet.parse(parser));
//				} else {
//					host.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				host.parseUnknownElement(parser, namespace, name);
//			}
//			event = parser.nextTag();
//		} while (event != XmlPullParser.END_TAG);
//		return host;
//	}
//
//	public void serialize(XmlSerializer serializer, ConstantsHelper helper) throws IllegalArgumentException, IllegalStateException, IOException {
//		serializer.startTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_RELATIONSHIP_ELEM_HOST);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// Add the EPR of Host
//		if (endpoint != null) {
//			endpoint.serialize(serializer, helper, helper.getWSANamespace(), WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE);
//		}
//		// Add the Service Types of Host
//		if (types != null) {
//			types.serialize(serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_TYPES);
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		serializer.endTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_RELATIONSHIP_ELEM_HOST);
//	}
}

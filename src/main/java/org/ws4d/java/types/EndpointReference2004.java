/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.types;


public class EndpointReference2004 extends EndpointReference {

	/** optional "wsa:EndpointReference/wsa:ReferenceProperties" element */
	private ReferenceParametersMData	referenceProperties	= null;

	/** optional "wsa:EndpointReference/wsa:PortType" element */
	private QName						portType			= null;

	/** optional "wsa:EndpointReference/wsa:ServiceName" element */
	private QName						serviceName			= null;

	/** optional "wsa:EndpointReference/wsa:ServiceName/@PortName" element */
	private String						portName			= null;

	// -------------------------------------------------------------------------

	/**
	 * Constructor.
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 */
	public EndpointReference2004(URI address) {
		this(address, null, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 */
	public EndpointReference2004(AttributedURI address) {
		this(address, null, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 * @param referenceParameters
	 *            "wsa:EndpointReference/wsa:ReferenceParameters"
	 */
	public EndpointReference2004(URI address, ReferenceParametersMData referenceParameters) {
		this(address, referenceParameters, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 * @param referenceParameters
	 *            "wsa:EndpointReference/wsa:ReferenceParameters"
	 */
	public EndpointReference2004(AttributedURI address, ReferenceParametersMData referenceParameters) {
		this(address, referenceParameters, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 * @param referenceParameters
	 *            "wsa:EndpointReference/wsa:ReferenceParameters"
	 * @param endpointMetadata "wsa:EndpointReference/wsa:Metadata"
	 */
	public EndpointReference2004(URI address, ReferenceParametersMData referenceParameters, MetadataMData endpointMetadata) {
		this(new AttributedURI(address), referenceParameters, endpointMetadata);
	}

	/**
	 * Constructor.
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 * @param referenceParameters
	 *            "wsa:EndpointReference/wsa:ReferenceParameters"
	 * @param endpointMetadata "wsa:EndpointReference/wsa:Metadata"
	 */
	public EndpointReference2004(AttributedURI address, ReferenceParametersMData referenceParameters, MetadataMData endpointMetadata) {
		super(address, referenceParameters, endpointMetadata);
	}

	/**
	 * Constructor
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 * @param portType "wsa:EndpointReference/wsa:PortType" element
	 * @param serviceName "wsa:EndpointReference/wsa:ServiceName" element
	 * @param portName "wsa:EndpointReference/wsa:ServiceName/@PortName" element
	 */
	public EndpointReference2004(URI address, QName portType, QName serviceName, String portName) {
		super(address);
		this.portType = portType;
		this.serviceName = serviceName;
		this.portName = portName;
	}

	/**
	 * Constructor
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 * @param portType "wsa:EndpointReference/wsa:PortType" element
	 * @param serviceName "wsa:EndpointReference/wsa:ServiceName" element
	 * @param portName "wsa:EndpointReference/wsa:ServiceName/@PortName" element
	 */
	public EndpointReference2004(AttributedURI address, QName portType, QName serviceName, String portName) {
		super(address);
		this.portType = portType;
		this.serviceName = serviceName;
		this.portName = portName;
	}

	/**
	 * Constructor
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 * @param portType "wsa:EndpointReference/wsa:PortType" element
	 * @param serviceName "wsa:EndpointReference/wsa:ServiceName" element
	 * @param portName "wsa:EndpointReference/wsa:ServiceName/@PortName" element
	 */
	public EndpointReference2004(URI address, ReferenceParametersMData referenceParameters, QName portType, QName serviceName, String portName) {
		super(address, referenceParameters);
		this.portType = portType;
		this.serviceName = serviceName;
		this.portName = portName;
	}

	/**
	 * Constructor
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 * @param portType "wsa:EndpointReference/wsa:PortType" element
	 * @param serviceName "wsa:EndpointReference/wsa:ServiceName" element
	 * @param portName "wsa:EndpointReference/wsa:ServiceName/@PortName" element
	 */
	public EndpointReference2004(AttributedURI address, ReferenceParametersMData referenceParameters, QName portType, QName serviceName, String portName) {
		super(address, referenceParameters);
		this.portType = portType;
		this.serviceName = serviceName;
		this.portName = portName;
	}

	/**
	 * Constructor
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 * @param referenceProperties
	 *            "wsa:EndpointReference/wsa:ReferenceProperties" element
	 * @param portType "wsa:EndpointReference/wsa:PortType" element
	 * @param serviceName "wsa:EndpointReference/wsa:ServiceName" element
	 * @param portName "wsa:EndpointReference/wsa:ServiceName/@PortName" element
	 */
	public EndpointReference2004(AttributedURI address, ReferenceParametersMData referenceParameters, ReferenceParametersMData referenceProperties, QName portType, QName serviceName, String portName) {
		super(address, referenceParameters);
		this.referenceProperties = referenceProperties;
		this.portType = portType;
		this.serviceName = serviceName;
		this.portName = portName;
	}

	// -----------------------------------------------------------------------

	public QName getPortType() {
		return portType;
	}

	public QName getServiceName() {
		return serviceName;
	}

	public String getPortName() {
		return portName;
	}

	public ReferenceParametersMData getReferenceProperties() {
		return referenceProperties;
	}

	// ------------------------------------------------------------------------

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("EndpointReference [ address=").append(getAddress());
		sb.append(", referenceProperties=").append(getReferenceProperties());
		sb.append(", referenceParameters=").append(getReferenceParameters());
		sb.append(", portType=").append(getPortType());
		sb.append(", serviceName=").append(getServiceName() + "{@portName= " + getPortName() + " }");
		sb.append(" ]");
		return sb.toString();
	}
	
//	public static EndpointReference parse(ElementParser parser) throws XmlPullParserException, IOException {
//		// handle attributes
//		int attributeCount = parser.getAttributeCount();
//		HashMap unknownAttributes = null;
//		if (attributeCount > 0) {
//			unknownAttributes = new HashMap();
//			for (int i = 0; i < attributeCount; i++) {
//				String namespace = parser.getAttributeNamespace(i);
//				if ("".equals(namespace)) {
//					// default to namespace of containing element
//					namespace = parser.getNamespace();
//				}
//				String name = parser.getAttributeName(i);
//				String value = parser.getAttributeValue(i);
//				unknownAttributes.put(QNameFactory.getInstance().getQName(name, namespace), value);
//			}
//		}
//		AttributedURI address = null;
//		ReferenceParametersMData properties = null;
//		ReferenceParametersMData parameters = null;
//		// MetadataMData metadata = null;
//		HashMap unknownElements = null;
//		QName portType = null;
//		QName serviceName = null;
//		String portName = null;
//		while (parser.nextTag() != XmlPullParser.END_TAG) {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (WSAConstants2006.WSA_NAMESPACE_NAME.equals(namespace)) {
//				if (WSAConstants.WSA_ELEM_ADDRESS.equals(name)) {
//					address = AttributedURI.parse(parser);
//				} else if (WSAConstants2006.WSA_ELEM_REFERENCE_PROPERTIES.equals(name)) {
//					properties = ReferenceParametersMData.parse(parser);
//				} else if (WSAConstants.WSA_ELEM_REFERENCE_PARAMETERS.equals(name)) {
//					parameters = ReferenceParametersMData.parse(parser);
//				} else if (WSAConstants2006.WSA_ELEM_PORT_TYPE.equals(name)) {
//					portType = parser.nextQName();
//				} else if (WSAConstants2006.WSA_ELEM_SERVICE_NAME.equals(name)) {
//					ArrayList list = parseServiceName(parser);
//					portName = (String) list.get(0);
//					serviceName = (QName) list.get(1);
//				} else if (WSAConstants2006.WSA_ELEM_POLICY.equals(name)) {
//					// ergaenzen
//				} else {
//					QName elementName = QNameFactory.getInstance().getQName(name, namespace);
//					Object result = parser.chainHandler(elementName);
//					if (result != null) {
//						if (unknownElements == null) {
//							unknownElements = new HashMap();
//						}
//						DataStructure elements = (DataStructure) unknownElements.get(elementName);
//						if (elements == null) {
//							elements = new ArrayList();
//							unknownElements.put(elementName, elements);
//						}
//						elements.add(result);
//					}
//				}
//			}
//		}
//		EndpointReference2004 er = new EndpointReference2004(address, parameters, properties, portType, serviceName, portName);
//		if (unknownAttributes != null) {
//			er.setUnknownAttributes(unknownAttributes);
//		}
//		if (unknownElements != null) {
//			er.setUnknownElements(unknownElements);
//		}
//		return er;
//	}
//	
//	private static ArrayList parseServiceName(ElementParser parentParser) throws XmlPullParserException, IOException {
//		ArrayList list = new ArrayList();
//		ElementParser parser = new ElementParser(parentParser);
//		int attributeCount = parser.getAttributeCount();
//		if (attributeCount > 0) {
//			String namespace = parser.getAttributeNamespace(0);
//			if ("".equals(namespace)) {
//				// default to namespace of containing element
//				namespace = parser.getNamespace();
//			}
//			String value = parser.getAttributeValue(0);
//			list.add(value);
//		}
//		QName serviceName = parser.nextQName();
//		list.add(serviceName);
//		return list;
//	}
	
}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.types;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashMap.Entry;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.List;
import org.ws4d.java.structures.Set;
import org.ws4d.java.util.WS4DIllegalStateException;

/**
 * 
 */
public class UnknownDataContainer {

	protected HashMap	unknownAttributes			= null;

	protected HashMap	unknownElements_QN_2_List	= null;

	/**
	 * Constructor
	 */
	public UnknownDataContainer() {
		super();
	}

	/**
	 * Copy Constructor.
	 */
	public UnknownDataContainer(UnknownDataContainer container) {
		if (container == null) {
			return;
		}

		if (container.unknownAttributes != null) {
			for (Iterator it = container.unknownAttributes.entrySet().iterator(); it.hasNext();) {
				HashMap.Entry entry = (HashMap.Entry) it.next();
				this.addUnknownAttribute((QName) entry.getKey(), (String) entry.getValue());
			}
		}
		if (container.unknownElements_QN_2_List != null) {
			for (Iterator it = container.unknownElements_QN_2_List.entrySet().iterator(); it.hasNext();) {
				HashMap.Entry entry = (HashMap.Entry) it.next();

				List elements = (List) entry.getValue();
				for (Iterator it_elem = elements.iterator(); it_elem.hasNext();) {
					this.addUnknownElement((QName) entry.getKey(), it_elem.next());
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[UnknownDataContainer ");
		sb.append("attributes={");
		if (unknownAttributes!=null){
			Set es= unknownAttributes.entrySet();
			if (es!=null)
			{
				Iterator it= es.iterator();
				while (it.hasNext())
				{
					HashMap.Entry entry=(Entry) it.next();
					sb.append(" ").append(entry.getKey()).append("=").append(entry.getValue());	
				}
				
			}
		}
		sb.append("}");
		
		sb.append(", elements={");
		if (unknownElements_QN_2_List!=null){
			Set es= unknownElements_QN_2_List.entrySet();
			if (es!=null)
			{
				Iterator it= es.iterator();
				while (it.hasNext())
				{
					HashMap.Entry entry=(Entry) it.next();
					sb.append(entry.getKey()).append("=").append(entry.getValue());	
				}
				
			}
		}
		sb.append("}]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.types.xml.XMLContainer#getUnknownAttribute(org.ws4d.java
	 * .data.QName)
	 */
	public String getUnknownAttribute(QName elementName) {
		return unknownAttributes == null ? null : (String) unknownAttributes.get(elementName);
	}

	/**
	 * Returns the <strong>first</strong> object with the given element name.
	 * 
	 * @param elementName element name
	 * @return the first object of given element name
	 */
	public Object getUnknownElement(QName elementName) {
		if (unknownElements_QN_2_List == null) {
			return null;
		}

		List elements = (List) unknownElements_QN_2_List.get(elementName);
		if (elements == null) {
			return null;
		}

		return elements.get(0);
	}

	/**
	 * Gets list of elements with given name.
	 * 
	 * @param elementName element name
	 * @return List includes each java object linked to the given element name.
	 */
	public List getUnknownElements(QName elementName) {
		if (unknownElements_QN_2_List == null) {
			return null;
		}

		return (List) unknownElements_QN_2_List.get(elementName);
	}

	/**
	 * Returns a map containing all unknown attributes of this XML container.
	 * The keys within the map are of type {@link QName} and reflect the
	 * qualified names of the attributes, whereas the values represent the
	 * corresponding attribute values as string.
	 * 
	 * @return a map of all unknown attributes
	 */
	public HashMap getUnknownAttributes() {
		// TODO return a clone or an unmodifiable view??
		return unknownAttributes;
	}

	/**
	 * Returns a map containing all unknown elements of this XML container. The
	 * keys within the map are of type {@link QName} and reflect the qualified
	 * names of the elements whereas the values are of type {@link List} and
	 * contain an entry for each child element with the same qualified name.
	 * 
	 * @return a map of all unknown elements
	 */
	public HashMap getUnknownElements() {
		// TODO return a clone or an unmodifiable view??
		return unknownElements_QN_2_List;
	}

	/**
	 * @param attributeName the name of the unknown attribute
	 * @param value the attribute's value represented as a Java string
	 */
	public void addUnknownAttribute(QName attributeName, String value) {
		if (unknownAttributes == null) {
			unknownAttributes = new HashMap();
		}
		unknownAttributes.put(attributeName, value);
	}

	/**
	 * @param elementName the name of the unknown element
	 * @param element the element represented as a Java object
	 */
	public void addUnknownElement(QName elementName, Object element) {
		if (unknownElements_QN_2_List == null) {
			unknownElements_QN_2_List = new HashMap();
		}

		DataStructure elements = (DataStructure) unknownElements_QN_2_List.get(elementName);
		if (elements == null) {
			elements = new ArrayList();
			unknownElements_QN_2_List.put(elementName, elements);
		}
		elements.add(element);
	}

	/**
	 * @param attributes
	 */
	public void setUnknownAttributes(HashMap attributes) {
		if (unknownAttributes != null) {
			throw new WS4DIllegalStateException("unknownAttributes already exist");
		}
		unknownAttributes = new HashMap(attributes);
	}

	/**
	 * @param elements
	 */
	public void setUnknownElements(HashMap elements) {
		if (unknownElements_QN_2_List != null) {
			throw new WS4DIllegalStateException("unknownElements already exist");
		}
		unknownElements_QN_2_List = new HashMap(elements);
	}

	//TODO SSch Check
//	/**
//	 * Parse all UnknownElements
//	 * 
//	 * @param container
//	 * @param namespace
//	 * @param name
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public void parseUnknownElement(AbstractElementParser parser, String namespace, String name) throws XmlPullParserException, IOException {
//		QName childName = QNameFactory.getInstance().getQName(name, namespace);
//		Object value = parser.chainHandler(childName);
//		if (value != null) {
//			addUnknownElement(childName, value);
//		}
//	}
//
//	/**
//	 * Parse all UnknownAttributes
//	 * 
//	 * @param container
//	 * @param namespace
//	 * @param name
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public void parseUnknownAttributes(AbstractElementParser parser) {
//		int count = parser.getAttributeCount();
//		for (int i = 0; i < count; i++) {
//			String namespace = parser.getAttributeNamespace(i);
//			if ("".equals(namespace)) {
//				// default to namespace of containing element
//				namespace = parser.getNamespace();
//			}
//			String name = parser.getAttributeName(i);
//			String value = parser.getAttributeValue(i);
//			addUnknownAttribute(QNameFactory.getInstance().getQName(name, namespace), value);
//		}
//	}



	// public int hashCode() {
	// //FIXME THAT'S WRONG!
	// final int prime = 31;
	// int result = 1;
	// if( unknownAttributes != null ){
	// for( Iterator it = unknownAttributes.entrySet().iterator(); it.hasNext();
	// ){
	// Map.Entry attribute = (Map.Entry) it.next();
	// result = prime * result + attribute.getKey().hashCode();
	// result = prime * result + attribute.getValue().hashCode();
	// }
	// }
	// if( unknownElements_QN_2_List != null ){
	// for( Iterator it = unknownElements_QN_2_List.entrySet().iterator();
	// it.hasNext(); ){
	// Map.Entry attribute = (Map.Entry) it.next();
	// result = prime * result + attribute.getKey().hashCode();
	// List list = (List) attribute.getValue();
	// if( o != null ){
	//
	// }
	// result = prime * result + attribute.getValue().hashCode();
	// }
	// }
	// result = prime * result + ((unknownAttributes == null) ? 0 :
	// unknownAttributes.hashCode());
	// result = prime * result + ((unknownElements_QN_2_List == null) ? 0 :
	// unknownElements_QN_2_List.hashCode());
	// return result;
	// }
	//
	// /* (non-Javadoc)
	// * @see java.lang.Object#equals(java.lang.Object)
	// */
	// public boolean equals(Object obj) {
	// if (this == obj) return true;
	// if (obj == null) return false;
	// if (getClass() != obj.getClass()) return false;
	//
	// UnknownDataContainer other = (UnknownDataContainer) obj;
	// if (unknownAttributes == null) {
	// if (other.unknownAttributes != null)
	// return false;
	// }
	// if (unknownElements_QN_2_List == null) {
	// if (other.unknownElements_QN_2_List != null)
	// return false;
	// }
	//
	// if( unknownAttributes.size() != other.unknownAttributes.size() ){
	// return false;
	// }
	// else{
	// /*
	// * Check each attribute entry.
	// */
	// for( Iterator it = unknownAttributes.entrySet().iterator(); it.hasNext();
	// ){
	// Map.Entry entry = (Map.Entry) it.next();
	// Object other_attr_value = other.unknownAttributes.get(entry.getKey());
	// if( entry.getKey() == null ){
	// if( other_attr_value != null ){
	// return false;
	// }
	// }
	// else{
	// if( ! entry.getKey().equals(other_attr_value) )
	// return false;
	// }
	// }
	// }
	//
	// if( unknownElements_QN_2_List.size() !=
	// other.unknownElements_QN_2_List.size() ){
	// return false;
	// }
	// else{
	// /*
	// * Check each element entry.
	// */
	// for( Iterator it = unknownElements_QN_2_List.entrySet().iterator();
	// it.hasNext(); ){
	// Map.Entry entry = (Map.Entry) it.next();
	// Object other_elem_value =
	// other.unknownElements_QN_2_List.get(entry.getKey());
	// if( entry.getKey() == null ){
	// if( other_elem_value != null ){
	// return false;
	// }
	// }
	// else{
	// if( ! entry.getKey().equals(other_elem_value) )
	// return false;
	// }
	// }
	// }
	//
	// return true;
	// }

}

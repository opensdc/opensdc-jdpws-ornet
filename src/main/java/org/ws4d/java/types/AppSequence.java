/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.types;


/**
 * 
 * 
 */
public class AppSequence extends UnknownDataContainer {

	private final long	instanceId;

	private final String	sequenceId;	// optional

	private final long	messageNumber;

	/**
	 * @param instanceId
	 * @param messageNumber
	 */
	public AppSequence(long instanceId, long messageNumber) {
		this(instanceId, null, messageNumber);
	}

	/**
	 * @param instanceId
	 * @param sequenceId
	 * @param messageNumber
	 */
	public AppSequence(long instanceId, String sequenceId, long messageNumber) {
		super();
		this.instanceId = instanceId;
		this.sequenceId = sequenceId;
		this.messageNumber = messageNumber;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("AppSequence [ instanceId=").append(instanceId);
		sb.append(", sequenceId=").append(sequenceId);
		sb.append(", messageNumber=").append(messageNumber);
		sb.append("{").append(super.toString()).append("}");
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.AppSequence#getInstanceId()
	 */
	public long getInstanceId() {
		return instanceId;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.AppSequence#getSequenceId()
	 */
	public String getSequenceId() {
		return sequenceId;
	}

	/**
	 * Get message number.
	 * 
	 * @return message number.
	 */
	public long getMessageNumber() {
		return messageNumber;
	}

	/**
	 * Checks if this application sequence is newer than the other specified.
	 * 
	 * @param other application sequence to compare with this.
	 * @return whether this instance is newer than the one passed-in
	 */
	public boolean isNewer(AppSequence other) {
		if (instanceId != other.instanceId) {
			if (instanceId > other.instanceId) {
				return true;
			}
			return false;
		} else if (messageNumber > other.messageNumber) {
			return true;
		}

		return false;
	}


//	public static AppSequence parse(ElementParser parser, ConstantsHelper helper) throws XmlPullParserException, IOException {
//		int attributeCount = parser.getAttributeCount();
//		if (attributeCount > 0) {
//			// InstanceID MUST be present and it MUST be an unsigned int
//			long instanceId = -1L;
//			String sequenceId = null;
//			// MessageNumber MUST be present and it MUST be an unsigned int
//			long messageNumber = -1L;
//			HashMap attributes = new HashMap();
//			for (int i = 0; i < attributeCount; i++) {
//				String namespace = parser.getAttributeNamespace(i);
//				if ("".equals(namespace)) {
//					// default to namespace of containing element
//					namespace = parser.getNamespace();
//				}
//				String name = parser.getAttributeName(i);
//				String value = parser.getAttributeValue(i);
//				if (WSDConstants.WSD_NAMESPACE_NAME.equals(namespace) || WSDConstants2006.WSD_NAMESPACE_NAME.equals(namespace)) {
//					if (WSDConstants.WSD_ATTR_INSTANCEID.equals(name)) {
//						try {
//							instanceId = Long.parseLong(value);
//						} catch (NumberFormatException e) {
//							throw new XmlPullParserException("AppSequence@InstanceId is not a number: " + value);
//						}
//					} else if (WSDConstants.WSD_ATTR_SEQUENCEID.equals(name)) {
//						sequenceId = value;
//					} else if (WSDConstants.WSD_ATTR_MESSAGENUMBER.equals(name)) {
//						try {
//							messageNumber = Long.parseLong(value);
//						} catch (NumberFormatException e) {
//							throw new XmlPullParserException("AppSequence@MessageNumber is not a number: " + value);
//						}
//					} else {
//						attributes.put(QNameFactory.getInstance().getQName(name, namespace), value);
//					}
//				} else {
//					attributes.put(QNameFactory.getInstance().getQName(name, namespace), value);
//				}
//			}
//			if (instanceId == -1L) {
//				throw new XmlPullParserException("AppSequence@InstanceId missing");
//			}
//			if (messageNumber == -1L) {
//				throw new XmlPullParserException("AppSequence@MessageNumber missing");
//			}
//			AppSequence appSequence = new AppSequence(instanceId, sequenceId, messageNumber);
//			while (parser.nextTag() == XmlPullParser.START_TAG) {
//				// fill-up child elements
//				String namespace = parser.getNamespace();
//				String name = parser.getName();
//				parser.addUnknownElement(appSequence, namespace, name);
//			}
//			return appSequence;
//		}
//		throw new XmlPullParserException("Invalid AppSequence: no attributes");
//	}
//
//	public void serialize(XmlSerializer serializer, ConstantsHelper helper) throws IllegalArgumentException, IllegalStateException, IOException {
//		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_APPSEQUENCE);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		if (instanceId >= 1) {
//			serializer.attribute(null, WSDConstants.WSD_ATTR_INSTANCEID, "" + instanceId);
//		}
//		if (messageNumber >= 1) {
//			serializer.attribute(null, WSDConstants.WSD_ATTR_MESSAGENUMBER, "" + messageNumber);
//		}
//		if (sequenceId != null) {
//			serializer.attribute(null, WSDConstants.WSD_ATTR_SEQUENCEID, "" + sequenceId);
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_APPSEQUENCE);
//	}
}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.types;

import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.util.StringUtil;

/**
 * Container for data collected during the discovery phase.
 */
public class DiscoveryData extends UnknownDataContainer {

	public static final long	UNKNOWN_METADATA_VERSION	= -1;

	private EndpointReference	endpointReference;					// always

	// mandatory

	private QNameSet			types			= QNameSet.construct(DPWSConstants.DPWS_QN_DEVICETYPE.toStringPlain()); //fix NPE if no types are provided

	private ScopeSet			scopes			= new ScopeSet(0); //Avoid NPE

	private URISet				xAddrs;							// mandatory

	// only in

	private long				metadataVersion;					// not

	// always
	// mandatory

	public DiscoveryData() {
		this((EndpointReference) null);
	}

	/**
	 * @param endpointReference
	 */
	public DiscoveryData(EndpointReference endpointReference) {
		this(endpointReference, UNKNOWN_METADATA_VERSION);
	}

	/**
	 * @param endpointReference
	 * @param metadataVersion
	 */
	public DiscoveryData(EndpointReference endpointReference, long metadataVersion) {
		this(endpointReference, metadataVersion, null);

	}

	/**
	 * @param endpointReference
	 * @param metadataVersion
	 */
	public DiscoveryData(EndpointReference endpointReference, long metadataVersion, URISet xaddresses) {
		super();
		this.endpointReference = endpointReference;
		this.metadataVersion = metadataVersion;
		this.xAddrs = xaddresses;
	}

	/**
	 * Copy Constructor. Deep Copy: Data structure within will be also be
	 * copied.
	 */
	public DiscoveryData(DiscoveryData data) {
		if (data.metadataVersion != 0) {
			metadataVersion = data.metadataVersion;
		}
		if (data.endpointReference != null) {
			endpointReference = data.endpointReference;
		}
		if (data.types != null) {
			setTypes(new QNameSet(data.types));
		}
		if (data.scopes != null) {
			setScopes(new ScopeSet(data.scopes));
		}
		if (data.xAddrs != null) {
			setXAddrs(new URISet(data.xAddrs));
		}
		metadataVersion = data.metadataVersion;
	}

	/**
	 * Update discovery data with given new discovery data. If metadata version
	 * is newer, return true. If metadata version is older, nothing will be
	 * changed.
	 * 
	 * @param newData metadata to update this metadata.
	 * @return true - if metadata version is newer and previous metadata version
	 *         is not "-1" (== unknown metadata version), else false.
	 */
	public boolean update(DiscoveryData newData) {
		if (newData == this || newData == null) {
			return false;
		}

		if (metadataVersion < newData.metadataVersion) {
			boolean ret;
			if (metadataVersion == UNKNOWN_METADATA_VERSION) {
				ret = false;
			} else {
				ret = true;
			}

			metadataVersion = newData.metadataVersion;
			types = newData.types;
			scopes = newData.scopes;
			xAddrs = newData.xAddrs;
			// XXX don't change the epr, it must be persistent

			return ret;
		} else if (metadataVersion == newData.metadataVersion) {
			/*
			 * update current discovery data
			 */
			QNameSet mergedTypes = new QNameSet(types);
			mergedTypes.addAll(newData.types);
			types = mergedTypes;

			URISet mergedXAddresses = new URISet(xAddrs);
			mergedXAddresses.addAll(newData.xAddrs);
			xAddrs = mergedXAddresses;

			ScopeSet mergedScopes = new ScopeSet(scopes);
			mergedScopes.addAll(newData.scopes);
			scopes = mergedScopes;
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ endpointReference=").append(endpointReference);
		sb.append(", types=").append(types);
		sb.append(", scopes=").append(scopes);
		sb.append(", xAddrs=").append(xAddrs);
		sb.append(", metadataVersion=").append(metadataVersion);
		sb.append(" ]");
		return sb.toString();
	}

	// ----------------------- GETTER / SETTER
	// ----------------------------------

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.DiscoveryData#getEndpointReference()
	 */
	public EndpointReference getEndpointReference() {
		return endpointReference;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.DiscoveryData#getMetadataVersion()
	 */
	public long getMetadataVersion() {
		return metadataVersion;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.DiscoveryData#getScopes()
	 */
	public ScopeSet getScopes() {
		return scopes;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.DiscoveryData#getTypes()
	 */
	public QNameSet getTypes() {
		return types;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.DiscoveryData#getXAddrs()
	 */
	public URISet getXAddrs() {
		return xAddrs;
	}

	/**
	 * @param endpointReference the endpointReference to set
	 */
	public void setEndpointReference(EndpointReference endpointReference) {
		this.endpointReference = endpointReference;
	}

	/**
	 * @param metadataVersion the metadataVersion to set
	 */
	public void setMetadataVersion(long metadataVersion) {
		this.metadataVersion = metadataVersion;
	}

	/**
	 * @param types the types to set
	 */
	public void setTypes(QNameSet types) {
		this.types = types;
	}

	/**
	 * @param scopes the scopes to set
	 */
	public void setScopes(ScopeSet scopes) {
		this.scopes = scopes;
	}

	/**
	 * @param addrs the xAddrs to set
	 */
	public void setXAddrs(URISet addrs) {
		xAddrs = addrs;
	}

	 
//	public void serialize(XmlSerializer serializer, Object helper) throws IOException {
//		
//	}
//	
//	public void parse(ElementParser parser, String displayName, Object helper) throws XmlPullParserException, IOException {
//		
//	}
	
//	/**
//	 * Parse the DiscoveryDataElement
//	 * 
//	 * @param discoveryData
//	 * @param displayName
//	 * @param parser
//	 * @return
//	 * @throws XmlPullParserException
//	 * @throws IOException
//	 */
//	public void parse(ElementParser parser, String displayName, ConstantsHelper helper) throws XmlPullParserException, IOException {
//		parseUnknownAttributes(parser);
//
//		int event = parser.nextTag();
//		if (event == XmlPullParser.END_TAG) {
//			throw new XmlPullParserException(displayName + " is empty");
//		}
//		do {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (helper.getWSANamespace().equals(namespace)) {
//				if (WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE.equals(name)) {
//					setEndpointReference(DPWSUtil.parseEndpointReference(helper.getDPWSVersion(), parser));
//				} else {
//					parseUnknownElement(parser, namespace, name);
//				}
//			} else if (helper.getWSDNamespace().equals(namespace)) {
//				if (WSDConstants.WSD_ELEMENT_TYPES.equals(name)) {
//					setTypes(QNameSet.parse(parser));
//				} else if (WSDConstants.WSD_ELEMENT_SCOPES.equals(name)) {
//					setScopes(ScopeSet.parse(parser));
//				} else if (WSDConstants.WSD_ELEMENT_XADDRS.equals(name)) {
//					setXAddrs(URISet.parse(parser));
//				} else if (WSDConstants.WSD_ELEMENT_METADATAVERSION.equals(name)) {
//					String value = parser.nextText();
//					long metadataVersion = 0L;
//					try {
//						metadataVersion = Long.parseLong(value.trim());
//					} catch (NumberFormatException e) {
//						throw new XmlPullParserException(displayName + "/MetadataVersion is not a number: " + value);
//					}
//					setMetadataVersion(metadataVersion);
//				} else {
//					parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				parseUnknownElement(parser, namespace, name);
//			}
//			event = parser.nextTag();
//		} while (event != XmlPullParser.END_TAG);
//	}
//
//	/**
//	 * Serialize all Elements from a DiscoveryData Object to the SOAP Body
//	 * 
//	 * @param discData the object in which the elements are saved
//	 * @throws IllegalArgumentException
//	 * @throws WS4DIllegalStateException
//	 * @throws IOException
//	 */
//	public void serialize(XmlSerializer serializer, ConstantsHelper helper) throws IOException {
//		// Endpointreference
//		endpointReference.serialize(serializer, helper, helper.getWSANamespace(), WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE);
//		// QNameSet Types
//		if (types != null) {
//			types.serialize(serializer, helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_TYPES);
//		}
//		// ScopeSet scopes
//		if (scopes != null) {
//			scopes.serialize(serializer, helper.getWSDNamespace());
//		}
//		// URISet xAddress
//		if (xAddrs != null) {
//			xAddrs.serialize(serializer, helper.getWSDNamespace());
//		}
//		// MetadataVersion
//		if (metadataVersion >= 1) {
//			SerializeUtil.serializeTag(serializer, helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_METADATAVERSION, ("" + metadataVersion));
//		}
//		// Adds UnknownElements to Header if exists
//		serializeUnknownElements(serializer);
//	}
}

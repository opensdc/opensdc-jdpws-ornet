/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.types;

import org.ws4d.java.constants.HTTPConstants;
import org.ws4d.java.constants.SOAPConstants;

/**
 * Implementation of "wsa:EndpointReference" element.
 * 
 * @author mspies
 */
public class EndpointReference extends UnknownDataContainer {

	/** "wsa:EndpointReference/wsa:Address" element */
	private AttributedURI				address;

	/** optional "wsa:EndpointReference/wsa:ReferenceParameters" element */
	private ReferenceParametersMData	referenceParameters	= null;

	/** optional "wsa:EndpointReference/wsa:Metadata" element */
	private MetadataMData				endpointMetadata	= null;

	// /** List<XAddress> */
	// private List xaddresses = null;

	// ------------------------ CONSTRUCTOR ---------------------------

	/**
	 * Constructor.
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 */
	public EndpointReference(URI address) {
		this(address, null, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 */
	public EndpointReference(AttributedURI address) {
		this(address, null, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 * @param referenceParameters
	 *            "wsa:EndpointReference/wsa:ReferenceParameters"
	 */
	public EndpointReference(URI address, ReferenceParametersMData referenceParameters) {
		this(address, referenceParameters, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 * @param referenceParameters
	 *            "wsa:EndpointReference/wsa:ReferenceParameters"
	 */
	public EndpointReference(AttributedURI address, ReferenceParametersMData referenceParameters) {
		this(address, referenceParameters, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 * @param referenceParameters
	 *            "wsa:EndpointReference/wsa:ReferenceParameters"
	 * @param endpointMetadata "wsa:EndpointReference/wsa:Metadata"
	 */
	public EndpointReference(URI address, ReferenceParametersMData referenceParameters, MetadataMData endpointMetadata) {
		this(new AttributedURI(address), referenceParameters, endpointMetadata);
	}

	/**
	 * Constructor.
	 * 
	 * @param address "wsa:EndpointReference/wsa:Address" element
	 * @param referenceParameters
	 *            "wsa:EndpointReference/wsa:ReferenceParameters"
	 * @param endpointMetadata "wsa:EndpointReference/wsa:Metadata"
	 */
	public EndpointReference(AttributedURI address, ReferenceParametersMData referenceParameters, MetadataMData endpointMetadata) {
		super();
		this.address = address;
		this.referenceParameters = referenceParameters;
		this.endpointMetadata = endpointMetadata;
	}

	// -----------------------------------------------------------

	/**
	 * Gets "wsa:EndpointReference/wsa:Address" element from endpoint reference
	 * 
	 * @return "wsa:EndpointReference/wsa:Address" element
	 */
	public AttributedURI getAddress() {
		return address;
	}

	// /**
	// * Get list of xaddresses.
	// *
	// * @return List of type XAddress
	// */
	// public List getXAddresses() {
	// return xaddresses;
	// }
	//
	// /**
	// * Set list of xaddresses.
	// *
	// * @param xaddresses
	// * List of type XAddress;
	// */
	// public void setXAddresses(List xaddresses) {
	// this.xaddresses = xaddresses;
	// }

	/**
	 * Gets "wsa:EndpointReference/wsa:ReferenceParameters" element from
	 * endpoint reference
	 * 
	 * @return "wsa:EndpointReference/wsa:ReferenceParameters" element
	 */
	public ReferenceParametersMData getReferenceParameters() {
		return referenceParameters;
	}

	/**
	 * Gets "wsa:EndpointReference/wsa:Metadata" element from endpoint reference
	 * 
	 * @return "wsa:EndpointReference/wsa:Metadata" element
	 */
	public MetadataMData getEndpointMetadata() {
		return endpointMetadata;
	}

	/**
	 * Returns <code>true</code> if this endpoint reference is a HTTP or
	 * SOAP-over-UDP transport address.
	 * 
	 * @return <code>true</code> if this endpoint reference is a HTTP or
	 *         SOAP-over-UDP transport address.
	 */
	public boolean isXAddress() {
		String schema = address.getSchema();
		if (schema!=null)
		{
			if (HTTPConstants.HTTP_SCHEMA.startsWith(schema)) {
				return true;
			} else if (SOAPConstants.SOAP_OVER_UDP_SCHEMA.startsWith(schema)) {
				return true;
			} else if (HTTPConstants.HTTPS_SCHEMA.startsWith(schema)) return true;
		}
		return false;
	}

	// ---------------------------- Object -----------------------------------

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("EndpointReference [ address=").append(getAddress());
		sb.append(", referenceParameters=").append(getReferenceParameters());
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		/*
		 * Only the address field is significant for endpoint reference.
		 */
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EndpointReference)) {
			return false;
		}

		EndpointReference other = (EndpointReference) obj;
		if (address == null) {
			if (other.address != null) {
				return false;
			}
		} else if (!address.equals(other.address)) {
			return false;
		}

		return true;
	}

	public void setReferenceParameters(ReferenceParametersMData ref) {
		this.referenceParameters = ref;
	}
	//
	//	/**
	//	 * Method to parse a EndpointReference (Addressing 2005 EPR)
	//	 * 
	//	 * @return
	//	 * @throws XmlPullParserException
	//	 * @throws IOException
	//	 */
	//
	//	public static EndpointReference parse(ElementParser parser) throws XmlPullParserException, IOException {
	//		// handle attributes
	//		int attributeCount = parser.getAttributeCount();
	//		HashMap unknownAttributes = null;
	//		if (attributeCount > 0) {
	//			unknownAttributes = new HashMap();
	//			for (int i = 0; i < attributeCount; i++) {
	//				String namespace = parser.getAttributeNamespace(i);
	//				if ("".equals(namespace)) {
	//					// default to namespace of containing element
	//					namespace = parser.getNamespace();
	//				}
	//				String name = parser.getAttributeName(i);
	//				String value = parser.getAttributeValue(i);
	//				unknownAttributes.put(QNameFactory.getInstance().getQName(name, namespace), value);
	//			}
	//		}
	//		AttributedURI address = null;
	//		ReferenceParametersMData parameters = null;
	//		MetadataMData metadata = null;
	//		HashMap unknownElements = null;
	//		while (parser.nextTag() != ElementParser.END_TAG) {
	//			String namespace = parser.getNamespace();
	//			String name = parser.getName();
	//			if (WSAConstants.WSA_NAMESPACE_NAME.equals(namespace)) {
	//				if (WSAConstants.WSA_ELEM_ADDRESS.equals(name)) {
	//					address = AttributedURI.parse(parser);
	//				} else if (WSAConstants.WSA_ELEM_REFERENCE_PARAMETERS.equals(name)) {
	//					parameters = ReferenceParametersMData.parse(new ElementParser(parser));
	//				} else if (WSAConstants.WSA_ELEM_METADATA.equals(name)) {
	//					metadata = new MetadataMData();
	//					parser.nextGenericElement(metadata);
	//				} else {
	//					QName elementName = QNameFactory.getInstance().getQName(name, namespace);
	//					Object result = parser.chainHandler(elementName);
	//					if (result != null) {
	//						if (unknownElements == null) {
	//							unknownElements = new HashMap();
	//						}
	//						DataStructure elements = (DataStructure) unknownElements.get(elementName);
	//						if (elements == null) {
	//							elements = new ArrayList();
	//							unknownElements.put(elementName, elements);
	//						}
	//						elements.add(result);
	//					}
	//				}
	//			}
	//		}
	//		EndpointReference epr = new EndpointReference(address, parameters, metadata);
	//		if (unknownAttributes != null) {
	//			epr.setUnknownAttributes(unknownAttributes);
	//		}
	//		if (unknownElements != null) {
	//			epr.setUnknownElements(unknownElements);
	//		}
	//
	//		return epr;
	//	}
	//
	//	/**
	//	 * Serialize the EndpointReference to the SOAP Document
	//	 * 
	//	 * @param namespace
	//	 * @param elementName
	//	 * @param ref
	//	 * @throws IllegalArgumentException
	//	 * @throws WS4DIllegalStateException
	//	 * @throws IOException
	//	 */ 
	//	public void serialize(XmlSerializer serializer, Object helper, String namespace, String elementName) throws IOException {
	//		
	//	}
	//	public void serialize(XmlSerializer serializer, ConstantsHelper helper, String namespace, String elementName) throws IOException {
	//		// Start-Tag
	//		serializer.startTag(namespace, elementName);
	//		// Adds UnknownAttributes to EPR Tag if exists
	//		serializeUnknownAttributes(serializer);
	//		// Address Element
	//		address.serialize(serializer, helper.getWSANamespace(), WSAConstants.WSA_ELEM_ADDRESS);
	//		// ReferenceParameters Element
	//		if (referenceParameters != null) {
	//			serializer.startTag(helper.getWSANamespace(), WSAConstants.WSA_ELEM_REFERENCE_PARAMETERS);
	//			referenceParameters.serializeNamespacePrefixes(serializer);
	//			serializeUnknownAttributes(serializer);
	//			// fake in order to dump reference parameter prefixes
	//			serializer.text("");
	//			referenceParameters.serialize(serializer, helper, false);
	//			serializer.endTag(helper.getWSANamespace(), WSAConstants.WSA_ELEM_REFERENCE_PARAMETERS);
	//		}
	//		// Metadata Element
	//		if (endpointMetadata != null) {
	//			serializer.startTag(helper.getWSANamespace(), WSAConstants.WSA_ELEM_METADATA);
	//			endpointMetadata.serializeUnknownAttributes(serializer);
	//			endpointMetadata.serializeUnknownElements(serializer);
	//			serializer.endTag(helper.getWSANamespace(), WSAConstants.WSA_ELEM_METADATA);
	//		}
	//		// Adds UnknownElements to EPR if exists
	//		serializeUnknownElements(serializer);
	//		serializer.endTag(namespace, elementName);
	//	}

}

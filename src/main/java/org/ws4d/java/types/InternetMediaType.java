/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.types;

import org.ws4d.java.constants.MIMEConstants;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;

/**
 * Kind of MIME-Type like described at RFC 1590.
 */
public class InternetMediaType {

	/** The main type. Case insensitive. */
	private String	type;

	/** The subtype. Case insensitive. */
	private String	subtype;

	/**
	 * String,String parameterName -> parameterValue. ParameterNames are case
	 * insensitive.
	 */
	private HashMap	parameters;

	/**
	 * Creates a new media type object from given types.
	 * 
	 * @param type media type.
	 * @param subtype media sub type.
	 */
	public InternetMediaType(String type, String subtype) {
		this.type = type;
		this.subtype = subtype;
		parameters = new HashMap();
	}

	/**
	 * Create a new media type object from the information given.
	 * 
	 * @param mediaType the string describing the media type.
	 */
	public InternetMediaType(String mediaType) {
		if (mediaType == null) {
			invalid();
			return;
		}
		int slashPos = mediaType.indexOf(MIMEConstants.SEPARATOR);
		if ((slashPos < 1) || (slashPos == (mediaType.length() - 1))) {
			invalid();
			return;
		}

		type = mediaType.substring(0, slashPos);
		int semicolonPos = mediaType.indexOf(";", slashPos + 1);
		if (semicolonPos < 0) {
			subtype = mediaType.substring(slashPos + 1).trim();
		} else {
			subtype = mediaType.substring(slashPos + 1, semicolonPos).trim();
		}
		parameters = new HashMap();

		String tmp;
		int quotePosStart;
		int quotePosEnd;

		while (semicolonPos > 0) {
			int nextSemicolonPos = mediaType.indexOf(";", semicolonPos + 1);
			if (nextSemicolonPos > 0) {

				tmp = mediaType.substring(semicolonPos + 1, nextSemicolonPos);

				quotePosStart = checkQuotation(tmp);
				if (quotePosStart > 0) {
					quotePosEnd = searchQuotationEnd(mediaType.substring(semicolonPos + 1), quotePosStart + 1) + semicolonPos + 2;
					addParameter(mediaType.substring(semicolonPos + 1, quotePosEnd));
					semicolonPos = mediaType.indexOf(";", quotePosEnd);
				} else {
					addParameter(tmp);
					semicolonPos = nextSemicolonPos;
				}

			} else {
				addParameter(mediaType.substring(semicolonPos + 1));
				semicolonPos = -1;
			}
		}
	}

	/**
	 * Adds a parameter with its value to the parameter table.
	 * 
	 * @param parameter the parameter to add. In the form attribute=value.
	 * @return <code>true</code> if the given parameter was well-formed,
	 *         <code>false</code> otherwise.
	 */
	private boolean addParameter(String parameter) {
		int equalsPos = parameter.indexOf("=");
		if ((equalsPos < 1) || (equalsPos == parameter.length() - 1)) {
			return false;
		}
		setParameter(parameter.substring(0, equalsPos), parameter.substring(equalsPos + 1));
		return true;
	}

	/**
	 * When the constructor parameter does not conform to the standard,
	 * initialize the class variables with <code>null</code> or anything else
	 * indicating failure.
	 */
	protected void invalid() {
		type = null;
		subtype = null;
		parameters = new HashMap();
	}

	/**
	 * Returns the complete media type.
	 * 
	 * @return the media type, e.g.\ "application/xml" for media type
	 *         "application/xml"
	 */
	public String getMediaType() {
		return type + MIMEConstants.SEPARATOR + subtype;
	}

	/**
	 * Returns the main type.
	 * 
	 * @return the main type, e.g.\ application for media type "application/xml"
	 */
	public String getType() {
		return type;
	}

	/**
	 * Returns the subtype.
	 * 
	 * @return the subtype, e.g.\ xml for media type "application/xml".
	 */
	public String getSubtype() {
		return subtype;
	}

	/**
	 * Checks whether this media type has the given type.
	 * 
	 * @param type the main type to check against. Case insensitive.
	 * @param subtype the subtype to check against. Case insensitive.
	 * @return <code>true</code> if the object has the given type,
	 *         <code>false</code> otherwise.
	 */
	public boolean hasType(String type, String subtype) {
		if ((this.type == null) || (this.subtype == null)) {
			return false;
		}
		return (this.type.equals(type)) && (this.subtype.equals(subtype));
	}

	/**
	 * Checks whether this media type has the given main type.
	 * 
	 * @param type the main type to check against. Case insensitive.
	 * @return <code>true</code> if the object has the given type,
	 *         <code>false</code> otherwise.
	 */
	public boolean hasMainType(String type) {
		if ((this.type == null)) {
			return false;
		}
		return (this.type.equals(type));
	}

	/**
	 * Checks whether this media type has the given sub type.
	 * 
	 * @param subtype the sub type to check against. Case insensitive.
	 * @return <code>true</code> if the object has the given type,
	 *         <code>false</code> otherwise.
	 */
	public boolean hasSubType(String subtype) {
		if ((this.subtype == null)) {
			return false;
		}
		return (this.subtype.equals(subtype));
	}

	/**
	 * Returns the value of a given attribute.
	 * 
	 * @param attributeName the name of the attribute to get the value of. Case
	 *            insensitive.
	 * @return the value of a given attribute.
	 */
	public String getParameter(String attributeName) {
		return (String) parameters.get(attributeName);
	}

	/**
	 * Checks whether this parameter exists.
	 * 
	 * @param attributeName the parameter to check for.
	 * @return <code>true</code> if the object has the given parameter,
	 *         <code>false</code> otherwise.
	 */
	public boolean hasParameter(String attributeName) {
		return parameters.containsKey(attributeName);
	}

	/**
	 * Stores the value of a given attribute. TODO keep case of attribute name
	 * 
	 * @param attributeName the attribute name.
	 * @param value the attribute value.
	 */
	public void setParameter(String attributeName, String value) {
		parameters.put(attributeName.trim(), value.trim());
	}

	/**
	 * Returns a string representation of this media type.
	 */
	@Override
	public String toString() {
		StringBuffer retval = new StringBuffer();
		retval.append(type);
		retval.append(MIMEConstants.SEPARATOR);
		retval.append(subtype);
		if (parameters != null) {
			Iterator en = parameters.keySet().iterator();
			while (en.hasNext()) {
				String attributeName = (String) en.next();
				String value = (String) parameters.get(attributeName);
				retval.append(';').append(attributeName).append('=').append(value);
			}
		}
		return retval.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((subtype == null) ? 0 : subtype.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		final InternetMediaType other = (InternetMediaType) obj;
		if (subtype == null) {
			if (other.subtype != null) return false;
		} else if (!subtype.equals(other.subtype)) return false;
		if (type == null) {
			if (other.type != null) return false;
		} else if (!type.equals(other.type)) return false;
		return true;
	}

	/**
	 * Checks if value of the parameter in the string is quoted.
	 * 
	 * @param s string to be checked.
	 * @return position of the beginning quotation mark of the parameter value.
	 *         Returns 0 if the value isn't quoted.
	 */
	private int checkQuotation(String s) {
		int equalsPos = s.indexOf("=");
		int quotePos = s.indexOf("\"");
		return (equalsPos + 1 == quotePos ? quotePos : 0);
	}

	/**
	 * Searches closing quotation mark in String.
	 * 
	 * @param s string to search in.
	 * @param index start position of search in string.
	 * @return position of the closing quotation mark of the parameter value.
	 */
	private int searchQuotationEnd(String s, int index) {
		int quotePos = s.indexOf("\"", index);

		while (s.charAt(quotePos - 1) == '\\') {
			int even = 1;
			for (int i = quotePos - 2; s.charAt(i) == '\\'; i--)
				even++;
			if (even % 2 == 0) return quotePos;

			quotePos = s.indexOf("\"", quotePos + 1);
		}

		return quotePos;
	}

	/**
	 * Returns the application/soap+xml type.
	 * 
	 * @return the application/soap+xml type.
	 */
	public final static InternetMediaType getSOAPXML() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_APPLICATION, MIMEConstants.SUBTYPE_SOAPXML);
	}

	/**
	 * Returns the application/xml type.
	 * 
	 * @return the application/xml type.
	 */
	public final static InternetMediaType getXML() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_APPLICATION, MIMEConstants.SUBTYPE_XML);
	}

	/**
	 * Returns the application/octet-stream type.
	 * 
	 * @return the application/octet-stream type.
	 */
	public final static InternetMediaType getApplicationOctetStream() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_APPLICATION, MIMEConstants.SUBTYPE_OCTETSTEAM);
	}

	/**
	 * Returns the application/xop+xml type.
	 * 
	 * @return the application/xop+xml type.
	 */
	public final static InternetMediaType getApplicationXOPXML() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_APPLICATION, MIMEConstants.SUBTYPE_XOPXML);
	}

	/**
	 * Returns the multipart/related type.
	 * 
	 * @return the multipart/related type.
	 */
	public final static InternetMediaType getMultipartRelated() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_MULTIPART, MIMEConstants.SUBTYPE_RELATED);
	}

	/**
	 * Returns the multipart/form-data type.
	 * 
	 * @return the multipart/form-data type.
	 */
	public final static InternetMediaType getMultipartFormdata() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_MULTIPART, MIMEConstants.SUBTYPE_FORMDATA);
	}

	/**
	 * Returns the application/x-www-form-urlencoded type.
	 * 
	 * @return the application/x-www-form-urlencoded type.
	 */
	public final static InternetMediaType getApplicationXWWWFormUrlEncoded() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_APPLICATION, MIMEConstants.SUBTYPE_XWWWFORMURLENCODED);
	}

	/**
	 * Returns the image/jpeg type.
	 * 
	 * @return the image/jpeg type.
	 */
	public final static InternetMediaType getImageJPEG() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_IMAGE, MIMEConstants.SUBTYPE_JPEG);
	}

	/**
	 * Returns the text/html type.
	 * 
	 * @return the text/html type.
	 */
	public final static InternetMediaType getTextHTML() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_TEXT, MIMEConstants.SUBTYPE_HTML);
	}

	/**
	 * Returns the text/plain type.
	 * 
	 * @return the text/plain type.
	 */
	public final static InternetMediaType getTextPlain() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_TEXT, MIMEConstants.SUBTYPE_PLAIN);
	}

	public static InternetMediaType getApplicationFastinfoset() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_APPLICATION, MIMEConstants.SUBTYPE_FASTINFOSET);
	}
	
	public static InternetMediaType getApplicationEXI() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_APPLICATION, MIMEConstants.SUBTYPE_EXI);
	}
	
	public static InternetMediaType getApplicationSOAPEXI() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_APPLICATION, MIMEConstants.SUBTYPE_SOAPEXI);
	}
	
	public static InternetMediaType getApplicationJSON() {
		return new InternetMediaType(MIMEConstants.MEDIATYPE_APPLICATION, MIMEConstants.SUBTYPE_JSON);
	}

}

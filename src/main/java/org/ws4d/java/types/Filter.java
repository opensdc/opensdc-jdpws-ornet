/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.types;


/**
 * 
 * 
 */
public class Filter extends UnknownDataContainer {

	private URI		dialect;

	private URISet	actions;

	/**
	 * 
	 */
	public Filter() {
		super();
	}

	/**
	 * @param dialect
	 * @param actions
	 */
	public Filter(URI dialect, URISet actions) {
		super();
		this.dialect = dialect;
		this.actions = actions;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Filter [ dialect=").append(dialect);
		sb.append(", actions=").append(actions);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.Filter#getActions()
	 */
	public URISet getActions() {
		return actions;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.types.Filter#getDialect()
	 */
	public URI getDialect() {
		return dialect;
	}

	/**
	 * @param dialect the dialect to set
	 */
	public void setDialect(URI dialect) {
		this.dialect = dialect;
	}

	/**
	 * @param actions the actions to set
	 */
	public void setActions(URISet actions) {
		this.actions = actions;
	}

//	public static Filter parse(ElementParser parser) throws XmlPullParserException, IOException {
//		return null;
//	}
//	
//	public void serialize(XmlSerializer serializer, Object helper) throws IllegalArgumentException, IllegalStateException, IOException {
//		
//	}
//	public static Filter parse(ElementParser parser) throws XmlPullParserException, IOException {
//		Filter filter = new Filter();
//
//		int attributeCount = parser.getAttributeCount();
//		for (int i = 0; i < attributeCount; i++) {
//			String namespace = parser.getAttributeNamespace(i);
//			if ("".equals(namespace)) {
//				// default to namespace of containing element
//				namespace = parser.getNamespace();
//			}
//			String name = parser.getAttributeName(i);
//			String value = parser.getAttributeValue(i);
//			if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace) && WSEConstants.WSE_ATTR_FILTER_DIALECT.equals(name)) {
//				filter.setDialect(new URI(value));
//			} else {
//				filter.addUnknownAttribute(QNameFactory.getInstance().getQName(name, namespace), value);
//			}
//		}
//		filter.setActions(URISet.parse(parser));
//
//		return filter;
//	}
//
//	public void serialize(XmlSerializer serializer, ConstantsHelper helper) throws IllegalArgumentException, IllegalStateException, IOException {
//		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_FILTER);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		if (dialect != null) {
//			serializer.attribute(null, WSEConstants.WSE_ATTR_FILTER_DIALECT, dialect.toString());
//		}
//		if (actions != null) {
//			for (Iterator it = actions.iterator(); it.hasNext();) {
//				URI uri = (URI) it.next();
//				serializer.text(uri.toString());
//				if (it.hasNext()) {
//					serializer.text(" ");
//				}
//			}
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_FILTER);
//	}

}

/**
 * 
 */
package org.ws4d.java.types;

import org.ws4d.java.structures.HashMap;

/**
 * @author Stefan Schlichting
 *
 */
public class QNameFactory 
{
	private static final QNameFactory instance=new QNameFactory();

	public static QNameFactory getInstance() {
		return instance;
	}
	
	private QNameFactory()
	{
		
	}
	
	
	private final HashMap namespace2ValueMap=new HashMap();
	private final String prefixDefault="prefix"+System.nanoTime();
	private final String nsDefault="namespace"+System.nanoTime();
	
	
	public QName getQName(String localPart, String namespace){
		return getQName(localPart, namespace,null);
	}
	
	public QName getQName(String localPart, String namespace, String prefix){
		return getQName(localPart,namespace, prefix,0);
	}
	
	public synchronized QName getQNameOld(String localPart, String namespace, String prefix, int priority)
	{
//		return new QName(localPart, namespace, prefix, priority);
		QName retVal=null;
//		System.out.println("\tFind Qname "+localPart+" "+namespace+" "+prefix+" "+priority);
		Object o=namespace2ValueMap.get(namespace!=null?namespace:nsDefault);
		if (o instanceof HashMap)
		{
			HashMap v2PMap=(HashMap)o;
			Object o2=v2PMap.get(localPart);
			if (o2 instanceof HashMap)
			{
				HashMap pf2PrioMap=(HashMap)o2;
				Object o3=pf2PrioMap.get((prefix!=null?prefix:prefixDefault));
				if (o3 instanceof HashMap)
				{
					HashMap qnMap=(HashMap)o3;
					Object qno=qnMap.get((priority>0?priority:0));
					if (qno instanceof QName)
					{
						retVal=(QName)qno;
					}else{
						retVal=createQName(localPart, namespace, (prefix!=null?prefix:prefixDefault), (priority>0?priority:0));
						qnMap.put(priority, retVal);
					}
				}else{
					retVal=createQName(localPart, namespace, (prefix!=null?prefix:prefixDefault), (priority>0?priority:0));
					HashMap qnMap=new HashMap();
					qnMap.put((priority>0?priority:0), retVal);
					pf2PrioMap.put((prefix!=null?prefix:prefixDefault), qnMap);
				}
			}else{
				retVal=createQName(localPart, namespace, (prefix!=null?prefix:prefixDefault), (priority>0?priority:0));
				HashMap qnMap=new HashMap();
				qnMap.put((priority>0?priority:0), retVal);
				HashMap pf2PrioMap=new HashMap();
				pf2PrioMap.put((prefix!=null?prefix:prefixDefault), qnMap);
				v2PMap.put(localPart, pf2PrioMap);
			}
		}else{
			retVal=createQName(localPart, (namespace!=null?namespace:nsDefault), (prefix!=null?prefix:prefixDefault), (priority>0?priority:0));
			HashMap qnMap=new HashMap();
			qnMap.put((priority>0?priority:0), retVal);
			HashMap pf2PrioMap=new HashMap();
			pf2PrioMap.put((prefix!=null?prefix:prefixDefault), qnMap);
			HashMap v2PMap=new HashMap();
			v2PMap.put(localPart, pf2PrioMap);
			namespace2ValueMap.put((namespace!=null?namespace:nsDefault), v2PMap);
		}
//		System.out.println("\tFound "+retVal.getPrefix()+" "+retVal.getPriority());
		return retVal;
	}
	
	public synchronized QName getQName(String localPart, String namespace, String prefix, int priority)
	{
		QName retVal=null;
		QNameKey key=new QNameKey(localPart, namespace!=null?namespace:nsDefault, prefix, (priority>0?priority:0));
		retVal=createQName(key.localPart,key.namespace,key.prefix,key.priority);
		
		return retVal;
//		QName retVal=null;
////		//(prefix!=null?prefix:prefixDefault)
//		QNameKey key=new QNameKey(localPart, namespace!=null?namespace:nsDefault, prefix, (priority>0?priority:0));
//		Object o=namespace2ValueMap.get(key);
//		if (o instanceof QName)
//		{
//			retVal=(QName) o;
//		}else{
//			 retVal=createQName(key.localPart,key.namespace,key.prefix,key.priority);
//			namespace2ValueMap.put(key,retVal);
//		}
////		retVal=getQNameOld(localPart,namespace,prefix,priority);
//		return retVal;
	}
	
	/**
	 * @param localPart
	 * @param namespace
	 * @param prefix
	 * @param priority
	 * @return
	 */
	private QName createQName(String localPart, String namespace,
			String prefix, int priority) {
//		System.out.println("\t\t Create "+localPart+" "+namespace+" "+prefix+" "+priority);
		return new QName((localPart!=null?localPart.intern():localPart), (namespace!=null?namespace.intern():namespace), prefix, priority);
	}

	/**
	 * Constructs a qualified name object with given namespace name and port
	 * type.
	 * 
	 * @param nsAndLocalPart namespace name and port type divided by '/'.
	 * @return Constructed QualifiedName or null.
	 */
	public QName getQName(String nsAndLocalPart){
		QName qname = null;

		if (nsAndLocalPart == null) return null;

		String namespace = null;
		String localPart = nsAndLocalPart;
		int index = localPart.lastIndexOf('/');
		if (index > 1) {
			namespace = nsAndLocalPart.substring(0, index);
			localPart = nsAndLocalPart.substring(index + 1);
		}
		qname = getQName(localPart, namespace, null);


		return qname;
	}
	
	private static class QNameKey{
		private final String localPart;
		private final String namespace;
		private final String prefix;
		private final int priority;
		
		public QNameKey(String localPart, String namespace, String prefix,
				int priority) {
			this.localPart = localPart;
			this.namespace = namespace;
			this.prefix = prefix;
			this.priority = priority;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((localPart == null) ? 0 : localPart.hashCode());
			result = prime * result
					+ ((namespace == null) ? 0 : namespace.hashCode());
			result = prime * result
					+ ((prefix == null) ? 0 : prefix.hashCode());
			result = prime * result + priority;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			QNameKey other = (QNameKey) obj;
			if (localPart == null) {
				if (other.localPart != null)
					return false;
			} else if (!localPart.equals(other.localPart))
				return false;
			if (namespace == null) {
				if (other.namespace != null)
					return false;
			} else if (!namespace.equals(other.namespace))
				return false;
			if (prefix == null) {
				if (other.prefix != null)
					return false;
			} else if (!prefix.equals(other.prefix))
				return false;
			if (priority != other.priority)
				return false;
			return true;
		}
		
		
	}
}

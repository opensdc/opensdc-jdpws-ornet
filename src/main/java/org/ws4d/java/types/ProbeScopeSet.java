/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.types;

import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.structures.Iterator;

/**
 *  
 *
 */
public class ProbeScopeSet extends ScopeSet {

	/** Matching Rule: RFC 3986 Section 6.2.1 simple string comparison (default) */
	public static final String	SCOPE_MATCHING_RULE_RFC3986	= WSDConstants.WSD_MATCHING_RULE_RFC3986;

	/** Matching Rule: case-sensitive string comparison */
	public static final String	SCOPE_MATCHING_RULE_STRCMP0	= WSDConstants.WSD_MATCHING_RULE_STRCMP0;

	/** Matching Rule: matching true if no scope in list */
	public static final String	SCOPE_MATCHING_RULE_NONE	= WSDConstants.WSD_MATCHING_RULE_NONE;

	String						matchBy;

	/**
	 * Constructor.
	 */
	public ProbeScopeSet() {
		this(null);
	}

	/**
	 * Constructor.
	 * 
	 * @param matchBy Matching Rule
	 */
	public ProbeScopeSet(String matchBy) {
		this(matchBy, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param matchBy Matching Rule
	 * @param scopes list of scopes
	 */
	public ProbeScopeSet(String matchBy, String[] scopes) {
		super(scopes);
		this.matchBy = matchBy;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("ScopeSet [ matchBy=").append(matchBy);
		sb.append(", scopes=");
		if (strScopes != null) {
			for (Iterator it = strScopes.iterator(); it.hasNext();) {
				sb.append((String) it.next());
			}
		}
		sb.append(", unknownAttributes=").append(unknownAttributes);
		sb.append(" ]");
		return sb.toString();
	}

	/**
	 * Gets matching algorithm of this scope list.
	 * 
	 * @return the matchBy algorithm
	 */
	public String getMatchBy() {
		return matchBy;
	}

	/**
	 * @param matchBy the matchBy to set
	 */
	public void setMatchBy(String matchBy) {
		this.matchBy = matchBy;
	}

//	public static ProbeScopeSet parseNextProbeScopeSet(ElementParser parser) throws XmlPullParserException, IOException {
//		ProbeScopeSet scopeSet = new ProbeScopeSet();
//		int attributeCount = parser.getAttributeCount();
//		String matchBy = WSDConstants.WSD_MATCHING_RULE_DEFAULT;
//		for (int i = 0; i < attributeCount; i++) {
//			String namespace = parser.getAttributeNamespace(i);
//			if ("".equals(namespace)) {
//				// default to namespace of containing element
//				namespace = parser.getNamespace();
//			}
//			String name = parser.getAttributeName(i);
//			String value = parser.getAttributeValue(i);
//			if (WSDConstants.WSD_NAMESPACE_NAME.equals(namespace) && WSDConstants.WSD_ATTR_MATCH_BY.equals(name)) {
//				matchBy = value;
//			} else {
//				scopeSet.addUnknownAttribute(QNameFactory.getInstance().getQName(name, namespace), value);
//			}
//		}
//		scopeSet.setMatchBy(matchBy);
//		scopeSet.addAll(ScopeSet.parse(parser));
//		return scopeSet;
//	}

}

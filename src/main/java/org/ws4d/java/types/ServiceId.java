/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.types;

import java.io.IOException;

import org.ws4d.java.constants.DPWSConstants2006;
import org.ws4d.java.io.xml.AbstractElementParser;
import org.ws4d.java.io.xml.ElementHandler;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.io.xml.XmlSerializer;
import org.ws4d.java.util.WS4DIllegalStateException;
import org.xmlpull.v1.XmlPullParserException;

public class ServiceId implements ElementHandler {

	public static final QName	QNAME	= QNameFactory.getInstance().getQName("ServiceId", DPWSConstants2006.DPWS_NAMESPACE_NAME);

	private URI					serviceId;

	public ServiceId() {}

	public ServiceId(URI serviceId) {
		this.serviceId = serviceId;
	}

	public URI getServiceId() {
		return serviceId;
	}

	public void setServiceId(URI serviceId) {
		this.serviceId = serviceId;
	}

	@Override
	public Object handleElement(QName elementName, AbstractElementParser parser, URI fromURI) throws XmlPullParserException, IOException {
		ServiceId id = new ServiceId();
		if (elementName.equals(QNAME) && parser instanceof ElementParser) {
			id.setServiceId(((ElementParser)parser).nextAttributedUri());
		}
		return id;
	}

	@Override
	public void serializeElement(XmlSerializer serializer, QName qname, Object value) throws IllegalArgumentException, WS4DIllegalStateException, IOException {

		ServiceId id = (ServiceId) value;

		serializer.startTag(qname.getNamespace(), qname.getLocalPart());
		serializer.text(id.getServiceId().toString());
		serializer.endTag(qname.getNamespace(), qname.getLocalPart());
	}

}

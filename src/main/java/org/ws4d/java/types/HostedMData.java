/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.types;

import org.ws4d.java.util.StringUtil;

/**
 * @author mspies
 */
public class HostedMData extends UnknownDataContainer {

	private URI						serviceId;

	private EndpointReferenceSet	endpointRefs	= null;

	private QNameSet				types;

	/**
	 * Constructor.
	 */
	public HostedMData() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ endpointRefs=").append(endpointRefs);
		sb.append(", types=").append(types);
		sb.append(", serviceId=").append(serviceId);
		sb.append(" ]");
		return sb.toString();
	}

	public URI getServiceId() {
		return serviceId;
	}

	/**
	 * Sets service id.
	 * 
	 * @param serviceId
	 */
	public void setServiceId(URI serviceId) {
		this.serviceId = serviceId;
	}

	public EndpointReferenceSet getEndpointReferences() {
		return endpointRefs;
	}

	/**
	 * Sets endpoint references.
	 * 
	 * @param endpoints
	 */
	public void setEndpointReferences(EndpointReferenceSet endpoints) {
		this.endpointRefs = endpoints;
	}

	/**
	 * Method to add endpoint reference. Not thread safe.
	 * 
	 * @param epr
	 */
	public void addEndpointReference(EndpointReference epr) {
		if (endpointRefs == null) {
			endpointRefs = new EndpointReferenceSet();
		}
		endpointRefs.add(epr);
	}

	public QNameSet getTypes() {
		return types;
	}

	/**
	 * Sets service types.
	 * 
	 * @param types
	 */
	public void setTypes(QNameSet types) {
		this.types = types;
	}

//	public static HostedMData parse(ElementParser parser, Object helper) throws XmlPullParserException, IOException {
//		return null;
//	}
//	public void serialize(XmlSerializer serializer, Object helper) throws IllegalArgumentException, IllegalStateException, IOException {
//		
//	}
//	public static HostedMData parse(ElementParser parser, ConstantsHelper helper) throws XmlPullParserException, IOException {
//		HostedMData hosted = new HostedMData();
//
//		hosted.parseUnknownAttributes(parser);
//
//		int event = parser.nextTag();
//		if (event == XmlPullParser.END_TAG) {
//			throw new XmlPullParserException("Hosted is empty");
//		}
//		Set references = null;
//		do {
//			String namespace = parser.getNamespace();
//			String name = parser.getName();
//			if (helper.getWSANamespace().equals(namespace)) {
//				if (WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE.equals(name)) {
//					if (references == null) {
//						references = new HashSet();
//					}
//					references.add(DPWSUtil.parseEndpointReference(helper.getDPWSVersion(), parser));
//				} else {
//					hosted.parseUnknownElement(parser, namespace, name);
//				}
//			} else if (helper.getDPWSNamespace().equals(namespace)) {
//				if (DPWSConstants.DPWS_ELEM_TYPES.equals(name)) {
//					hosted.setTypes(QNameSet.parse(parser));
//				} else if (DPWSConstants.DPWS_ELEM_SERVICEID.equals(name)) {
//					hosted.setServiceId(new URI(parser.nextText().trim()));
//				} else {
//					hosted.parseUnknownElement(parser, namespace, name);
//				}
//			} else {
//				hosted.parseUnknownElement(parser, namespace, name);
//			}
//			event = parser.nextTag();
//		} while (event != XmlPullParser.END_TAG);
//		if (references != null) {
//			hosted.setEndpointReferences(new EndpointReferenceSet(references));
//		}
//		return hosted;
//	}
//
//	public void serialize(XmlSerializer serializer, ConstantsHelper helper) throws IllegalArgumentException, IllegalStateException, IOException {
//		serializer.startTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_RELATIONSHIP_ELEM_HOSTED);
//		// Adds UnknownAttributes
//		serializeUnknownAttributes(serializer);
//		// Endpoint References
//		for (Iterator it = getEndpointReferences().iterator(); it.hasNext();) {
//			EndpointReference epr = (EndpointReference) it.next();
//			epr.serialize(serializer, helper, helper.getWSANamespace(), WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE);
//		}
//		// ServiceTypes
//		if (types != null) {
//			types.serialize(serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_TYPES);
//		}
//		// Add ServiceID
//		if (serviceId != null) {
//			SerializeUtil.serializeTag(serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_SERVICEID, (serviceId == null ? null : serviceId.toString()));
//		}
//		// Adds UnknownElements
//		serializeUnknownElements(serializer);
//		serializer.endTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_RELATIONSHIP_ELEM_HOSTED);
//	}
}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.types;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.List;

/**
 * 
 */
public class ReferenceParametersMData extends UnknownDataContainer {

	public static class ReferenceParameter {

		private final String	namespace;

		private final String	name;

		/*
		 * each odd element is a namespace URI, each even element is a
		 * concatenated XML substring between prefixes
		 */
		private List			contentChunks;

		/**
		 * @param namespace
		 * @param name
		 */
		public ReferenceParameter(String namespace, String name) {
			super();
			this.namespace = namespace;
			this.name = name;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			StringBuffer sb = new StringBuffer();
			if (contentChunks != null) {
				sb.append('<').append(namespace).append(':').append(name);
				for (Iterator it = contentChunks.iterator(); it.hasNext();) {
					sb.append(it.next());
				}
			}
			return sb.toString();
		}

		/**
		 * @return the namespace
		 */
		public String getNamespace() {
			return namespace;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param chunk the next content chunk to append
		 */
		public void appendChunk(String chunk) {
			if (contentChunks == null) {
				contentChunks = new ArrayList();
			}
			contentChunks.add(chunk);
		}

		/**
		 * Returns an array containing all the chunks of unknown XML character
		 * data stored within this reference parameter instance. In case there
		 * are no chunks, an array of length zero is returned. The returned
		 * array contains always a namespace URI at each of its
		 * <strong>odd</strong> elements and a chunk of XML characters at the
		 * <strong>even</strong> ones.
		 * 
		 * @return an array containing all the chunks
		 */
		public String[] getChunks() {
			return contentChunks == null ? EMPTY_STRING_ARRAY : (String[]) contentChunks.toArray(new String[contentChunks.size()]);
		}

	}

	private static final String[]				EMPTY_STRING_ARRAY		= new String[0];

	private static final ReferenceParameter[]	EMPTY_PARAMETER_ARRAY	= new ReferenceParameter[0];

	private URI									wseIdentifier;

	private List								parameters;

	/**
	 * 
	 */
	public ReferenceParametersMData() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		if (wseIdentifier != null) {
			sb.append("wseIdentifier=").append(wseIdentifier);
		}
		if (parameters != null) {
			if (wseIdentifier != null) {
				sb.append('\n');
			}
			for (Iterator it = parameters.iterator(); it.hasNext();) {
				sb.append(it.next());
				if (it.hasNext()) {
					sb.append('\n');
				}
			}
		}
		return sb.toString();
	}

	/**
	 * @return the wseIdentifier
	 */
	public URI getWseIdentifier() {
		return wseIdentifier;
	}

	/**
	 * @param wseIdentifier the wseIdentifier to set
	 */
	public void setWseIdentifier(URI wseIdentifier) {
		this.wseIdentifier = wseIdentifier;
	}

	/**
	 * @param parameter the next reference parameter to add
	 */
	public void add(ReferenceParameter parameter) {
		if (parameters == null) {
			parameters = new ArrayList();
		}
		parameters.add(parameter);
	}

	/**
	 * Returns an array containing all the parameters stored within this
	 * reference parameters instance. If there are no parameters, an array of
	 * length zero is returned.
	 * 
	 * @return an array containing all the parameters
	 */
	public ReferenceParameter[] getParameters() {
		return parameters == null ? EMPTY_PARAMETER_ARRAY : (ReferenceParameter[]) parameters.toArray(new ReferenceParameter[parameters.size()]);
	}


//	public static ReferenceParametersMData parse(ElementParser parentParser) throws XmlPullParserException, IOException 
//	{
//		return null;
//	}
//	
//	public void serialize(XmlSerializer serializer, Object helper, boolean withinHeader) throws IOException {
//		
//	}
	
//	public void serializeNamespacePrefixes(XmlSerializer serializer) {
//		
//	}
//	
//	public static ReferenceParametersMData parse(ElementParser parentParser) throws XmlPullParserException, IOException {
//		ReferenceParametersMData parameters = new ReferenceParametersMData();
//		parameters.parseUnknownAttributes(parentParser);
//		int event = parentParser.nextTag(); // go to first child
//		int depth = parentParser.getDepth();
//		String namespace = parentParser.getNamespace();
//		String name = parentParser.getName();
//		if (event == XmlPullParser.END_TAG && (WSAConstants.WSA_NAMESPACE_NAME.equals(namespace) || WSAConstants2006.WSA_NAMESPACE_NAME.equals(namespace)) && WSAConstants.WSA_ELEM_REFERENCE_PARAMETERS.equals(name)) {
//			// empty but existing reference parameters block
//			return parameters;
//		}
//		ElementParser parser = new ElementParser(parentParser);
//		ReferenceParameter currentParameter = null;
//		boolean onTopLevel = true;
//		StringBuffer result = new StringBuffer();
//		while (true) {
//			do {
//				switch (event) {
//					case (XmlPullParser.START_TAG): {
//						namespace = parser.getNamespace();
//						name = parser.getName();
//						if (onTopLevel) {
//							if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace) && WSEConstants.WSE_ELEM_IDENTIFIER.equals(name)) {
//								parameters.setWseIdentifier(new URI(parser.nextText().trim()));
//								continue;
//							}
//							QName elementName = QNameFactory.getInstance().getQName(name, namespace);
//							Object obj = parentParser.chainHandler(elementName, false);
//							if (obj != null) {
//								parameters.addUnknownElement(elementName, obj);
//								continue;
//							}
//							// 1st chunk = '<' literal (statically known)
//							// 2nd chunk = element namespace
//							// 3rd chunk = ':' literal + element name
//							// 4th chunk = bulk char data
//							// 5th chunk = next attribute/element's namespace
//							// 6th chunk = see 4th chunk
//							// 7th chunk = see 5th chunk
//							currentParameter = new ReferenceParameter(namespace, name);
//							parameters.add(currentParameter);
//						} else {
//							result.append('<');
//							currentParameter.appendChunk(result.toString());
//							result = new StringBuffer();
//							currentParameter.appendChunk(namespace);
//							result.append(':').append(name);
//						}
//
//						int attrCount = parser.getAttributeCount();
//						for (int i = 0; i < attrCount; i++) {
//							result.append(' ');
//							String prefix = parser.getAttributePrefix(i);
//							String attribute = parser.getAttributeName(i);
//							if (prefix == null) {
//								// assume same attribute namespace as element
//								if ((WSAConstants.WSA_NAMESPACE_NAME.equals(namespace) || WSAConstants2006.WSA_NAMESPACE_NAME.equals(namespace)) && WSAConstants.WSA_ATTR_IS_REFERENCE_PARAMETER.equals(attribute)) {
//									// skip wsa:IsReferenceParameter
//									continue;
//								}
//							} else {
//								String attributeNamespace = parser.getAttributeNamespace(i);
//								if ((WSAConstants.WSA_NAMESPACE_NAME.equals(attributeNamespace) || WSAConstants2006.WSA_NAMESPACE_NAME.equals(attributeNamespace)) && WSAConstants.WSA_ATTR_IS_REFERENCE_PARAMETER.equals(attribute)) {
//									// skip wsa:IsReferenceParameter
//									continue;
//								}
//								currentParameter.appendChunk(result.toString());
//								currentParameter.appendChunk(attributeNamespace);
//								result = new StringBuffer();
//								result.append(':');
//							}
//							String value = parser.getAttributeValue(i);
//							result.append(attribute).append("=\"").append(value).append('\"');
//						}
//						result.append('>');
//						onTopLevel = false;
//						break;
//					}
//					case (XmlPullParser.TEXT): {
//						result.append(parser.getText().trim());
//						break;
//					}
//					case (XmlPullParser.END_TAG): {
//						result.append("</");
//						currentParameter.appendChunk(result.toString());
//						currentParameter.appendChunk(parser.getNamespace());
//						result = new StringBuffer();
//						result.append(':').append(parser.getName()).append('>');
//						break;
//					}
//				}
//			} while ((event = parser.next()) != XmlPullParser.END_DOCUMENT);
//			event = parentParser.nextTag();
//			if (parentParser.getDepth() == depth) {
//				// next reference parameter starts
//				parser = new ElementParser(parentParser);
//				currentParameter.appendChunk(result.toString());
//				result = new StringBuffer();
//				onTopLevel = true;
//			} else {
//				// reference parameters end tag
//				break;
//			}
//		}
//		if (currentParameter != null) {
//			currentParameter.appendChunk(result.toString());
//		}
//		return parameters;
//	}
//
//	/**
//	 * Serializes the ReferenceParameters
//	 * 
//	 * @param params
//	 * @param withinHeader whether reference parameters are serialized whilst
//	 *            being embedded directly within a SOAP header or within a
//	 *            wsa:ReferenceParamters block of an endpoint reference
//	 * @throws IOException
//	 */
//	public void serialize(XmlSerializer serializer, ConstantsHelper helper, boolean withinHeader) throws IOException {
//		// any XML special chars should remain unescaped
//		if (wseIdentifier != null) {
//			String wseId = wseIdentifier.toString();
//			serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_IDENTIFIER);
//			if (withinHeader) {
//				serializer.attribute(helper.getWSANamespace(), WSAConstants.WSA_ATTR_IS_REFERENCE_PARAMETER, "true");
//			}
//			serializer.text(wseId == null ? "" : wseId);
//			serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_IDENTIFIER);
//		} else {
//			// we need this to close the preceding element tag
//			serializer.text("");
//		}
//		serializeUnknownElements(serializer);
//		ReferenceParameter[] allParameters = getParameters();
//		for (int i = 0; i < allParameters.length; i++) {
//			ReferenceParameter parameter = allParameters[i];
//			serializer.plainText("<");
//			String prefix = serializer.getPrefix(parameter.getNamespace(), true);
//			serializer.plainText(prefix);
//			serializer.plainText(":");
//			serializer.plainText(parameter.getName());
//			// add wsa:IsReferenceParameter if withinHeader == true
//			if (withinHeader) {
//				serializer.plainText(" ");
//				prefix = serializer.getPrefix(helper.getWSANamespace(), true);
//				serializer.plainText(prefix);
//				serializer.plainText(":");
//				serializer.plainText(WSAConstants.WSA_ATTR_IS_REFERENCE_PARAMETER);
//				serializer.plainText("=\"true\"");
//			}
//			String[] chunks = parameter.getChunks();
//			for (int j = 0; j < chunks.length; j++) {
//				if (j % 2 == 0) {
//					serializer.plainText(chunks[j]);
//				} else {
//					prefix = serializer.getPrefix(chunks[j], true);
//					serializer.plainText(prefix);
//				}
//			}
//		}
//	}
//
//	/**
//	 * Serializes the NamespacePrefixes
//	 * 
//	 * @param params
//	 * @throws IOException
//	 */
//	public void serializeNamespacePrefixes(XmlSerializer serializer) {
//		ReferenceParameter[] allParameters = this.getParameters();
//		for (int i = 0; i < allParameters.length; i++) {
//			ReferenceParameter parameter = allParameters[i];
//			String[] chunks = parameter.getChunks();
//			for (int j = 1; j < chunks.length; j += 2) {
//				serializer.getPrefix(chunks[j], true);
//			}
//		}
//	}

}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.eventing;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;

public class ClientSubscriptionManager {

	private final static ClientSubscriptionManager instance =new ClientSubscriptionManager();
	private static HashSet				subscriptions				= new HashSet();
	
	public static ClientSubscriptionManager getInstance() {
		return instance;
	}

	private ClientSubscriptionManager() {
		super();
	}

	/**
	 * Adds a event subscription to the framework. This allows the framework to
	 * unsubscribe on shutdown.
	 * 
	 * @param subscription the subscription which the framework should take care
	 *            about.
	 */
	public synchronized void addClientSubscription(ClientSubscription subscription) {
		subscriptions.add(subscription);
	}

	/**
	 * Removes a event subscription.
	 * 
	 * @param subscription the subscription which is not important any more.
	 */
	public  synchronized void removeClientSubscription(ClientSubscription subscription) {
		if (subscription!=null)
		{
			subscriptions.remove(subscription);
		}
	}

	/**
	 * Unsubscribe from all event sources.
	 */
	public void unsubscribeAll() {
		Iterator it = subscriptions.iterator();
		
		ArrayList unsub=new ArrayList();
		while(it.hasNext()){
			unsub.add(it.next());
		}
		
		Iterator it2=unsub.iterator();
		while (it2.hasNext()) {
			ClientSubscription cs = (ClientSubscription) it2.next();
			try {
				cs.unsubscribe();
			} catch (Exception e) {
				Log.warn(e);
			}
		}
	}
}

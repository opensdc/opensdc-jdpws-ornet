/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.eventing;

import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.service.OperationCommons;
import org.ws4d.java.service.Service;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.URI;
import org.ws4d.java.types.URISet;
import org.ws4d.java.util.IDGenerator;
import org.ws4d.java.wsdl.WSDLOperation;

public abstract class EventSourceCommons extends OperationCommons implements EventSource{
	
	
	public EventSourceCommons(String name, QName portType) {
		super(name, portType);
	}

	public EventSourceCommons(WSDLOperation operation) {
		super(operation);
	}

	/**
	 * Returns <code>true</code>, if the transmission type of this event source
	 * is {@link WSDLOperation#TYPE_NOTIFICATION}. Returns <code>false</code> in
	 * any other case.
	 * 
	 * @return checks whether this is a {@link WSDLOperation#TYPE_NOTIFICATION
	 *         notification} event source
	 */
	@Override
	public final boolean isNotification() {
		return getType() == WSDLOperation.TYPE_NOTIFICATION;
	}

	/**
	 * Returns <code>true</code>, if the transmission type of this event source
	 * is {@link WSDLOperation#TYPE_SOLICIT_RESPONSE}. Returns
	 * <code>false</code> in any other case.
	 * 
	 * @return checks whether this is a
	 *         {@link WSDLOperation#TYPE_SOLICIT_RESPONSE solicit-response}
	 *         event source
	 */
	@Override
	public final boolean isSolicitResponse() {
		return getType() == WSDLOperation.TYPE_SOLICIT_RESPONSE;
	}
	
	/**
	 * Returns the <code>transmission type</code> of this event source according
	 * to <a href="http://www.w3.org/TR/wsdl">WSDL 1.1 specification</a>. The
	 * value returned is one of {@link WSDLOperation#TYPE_NOTIFICATION} or
	 * {@link WSDLOperation#TYPE_SOLICIT_RESPONSE}.
	 * 
	 * @return type the transmission type of this event source
	 */
	@Override
	public final int getType() {
		if (type == WSDLOperation.TYPE_UNKNOWN) {
			if (getInput() == null) {
				type = WSDLOperation.TYPE_NOTIFICATION;
			} else {
				type = WSDLOperation.TYPE_SOLICIT_RESPONSE;
			}
		}
		return type;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.eventing.EventSource#subscribe(org.ws4d.java.eventing.
	 * EventListener, long)
	 */
	@Override
	public ClientSubscription subscribe(EventListener client, long duration) throws EventingException, TimeoutException {
		return subscribe(client, duration, null);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.eventing.EventSource#subscribe(org.ws4d.java.eventing.
	 * EventListener, long, org.ws4d.java.structures.DataStructure)
	 */
	@Override
	public ClientSubscription subscribe(EventListener client, long duration, DataStructure bindings) throws EventingException, TimeoutException {
		Service service = getService();
		EventSink sink = client.getEventSink(bindings);
		if (service.isRemote() && !sink.isOpen()) {
			sink.open();
		}
		String clientSubscriptionId = IDGenerator.getUUIDasURI().toString();
		ClientSubscription cs = service.subscribe(sink, clientSubscriptionId, new URISet(new URI(getOutputAction())), duration);
		ClientSubscriptionManager.getInstance().addClientSubscription(cs);
		return cs;
	}

}

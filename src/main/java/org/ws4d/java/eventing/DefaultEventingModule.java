/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.eventing;

import org.ws4d.java.configuration.BindingProperties;
import org.ws4d.java.configuration.EventingProperties;
import org.ws4d.java.configuration.Properties;
import org.ws4d.java.framework.module.EventingModule;

public class DefaultEventingModule implements EventingModule {

	@Override
	public void registerPropertiesHandler(Properties properties) {
		properties.register(Properties.HEADER_SECTION_BINDINGS, BindingProperties.getInstance());
		properties.register(Properties.HEADER_SECTION_EVENTING, EventingProperties.getInstance());
	}

}

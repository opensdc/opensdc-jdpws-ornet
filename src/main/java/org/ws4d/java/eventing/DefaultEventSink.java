/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.eventing;

import java.io.IOException;

import org.ws4d.java.communication.CommunicationBinding;
import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.DefaultIncomingMessageListener;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.configuration.EventingProperties;
import org.ws4d.java.constants.DPWSMessageConstants;
import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.io.xml.ElementHandlerRegistry;
import org.ws4d.java.message.EventingMessageHeader;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.IMessageEndpoint;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.MessageException;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.schema.PredefinedSchemaTypes;
import org.ws4d.java.service.DefaultClientSubscription;
import org.ws4d.java.service.OperationDescription;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.QNameValue;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.LockedList;
import org.ws4d.java.structures.LockedMap;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.StringUtil;
import org.ws4d.java.util.WS4DIllegalStateException;

/**
 * Class represents an endpoint to receive notifications.
 */
public class DefaultEventSink implements EventSink {

	private static final int[]				EVENT_SINK_MESSAGE_TYPES	= { DPWSMessageConstants.INVOKE_MESSAGE, DPWSMessageConstants.SUBSCRIPTION_END_MESSAGE };

	private final EventSinkMessageListener	incomingListener			= new EventSinkMessageListener();

	private final LockedList				bindings					= new LockedList();

	private final EventListener				eventListener;

	private boolean							registered					= false;

	private HashMap							map_CSubId_2_CSub			= new LockedMap(new HashMap(5));


	static {
		/*
		 * Register element handler for eventListener side eventing.
		 */
		ElementHandlerRegistry.getRegistry().registerElementHandler(WSEConstants.WSE_QN_IDENTIFIER, new ClientSubscriptionElementHandler());
	}

	/**
	 * Constructor.
	 * 
	 * @param eventListener Client with which this event sink should be
	 *            associated. Received events will be transmitted to the
	 *            eventListener.
	 */
	private DefaultEventSink(EventListener eventListener) {
		this.eventListener = eventListener;
	}

	/**
	 * Constructor.
	 * 
	 * @param eventListener Client with which this event sink should be
	 *            associated. Received events will be transmitted to the
	 *            eventListener.
	 * @param bindings a data structure of {@link CommunicationBinding}
	 *            instances over which to expose this event sink
	 */
	public DefaultEventSink(EventListener eventListener, DataStructure bindings) {
		this(eventListener);
		if (bindings != null) {
			// needed only for a remote event sink
			this.bindings.addAll(bindings);
		}
	}

	/**
	 * @param eventListener
	 * @param configurationId
	 */
	public DefaultEventSink(EventListener eventListener, int configurationId) {
		this(eventListener);
		this.bindings.addAll(EventingProperties.getInstance().getBindings(new Integer(configurationId)));
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.Bindable#hasBindings()
	 */
	@Override
	public boolean hasBindings() {
		return (bindings.size() > 0);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.Bindable#getBindings()
	 */
	@Override
	public Iterator getBindings() {
		return new ReadOnlyIterator(bindings);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.Bindable#supportsBindingChanges()
	 */
	@Override
	public boolean supportsBindingChanges() {
		return !registered;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.Bindable#addBinding(org.ws4d.java.communication
	 * .CommunicationBinding)
	 */
	@Override
	public void addBinding(CommunicationBinding binding) throws WS4DIllegalStateException {
		if (registered) {
			throw new WS4DIllegalStateException("Event Sink is already running, unable to add binding");
		}
		bindings.add(binding);
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.ws4d.java.communication.Bindable#removeBinding(org.ws4d.java.
	 * communication.CommunicationBinding)
	 */
	@Override
	public boolean removeBinding(CommunicationBinding binding) throws WS4DIllegalStateException {
		if (registered) {
			throw new WS4DIllegalStateException("Event Sink is already running, unable to remove binding");
		}
		return bindings.remove(binding);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.Bindable#clearBindings()
	 */
	@Override
	public void clearBindings() throws WS4DIllegalStateException {
		if (registered) {
			throw new WS4DIllegalStateException("Event Sink is already running, unable to clear bindings");
		}
		bindings.clear();
	}

	/**
	 * Opens event receiving for this event endpoint.
	 */
	@Override
	public void open() throws EventingException {
		if (registered == true) {
			Log.warn("EventSink already opened.");
			return;
		}
		if (!hasBindings()) {
			String descriptor = StringUtil.simpleClassName(getClass());
			if (Log.isDebug()) {
				Log.info("No bindings found, utobinding event sink " + descriptor);
			}
			DataStructure autoBindings = new HashSet();
			for (Iterator it = CommunicationManagerRegistry.getLoadedManagers(); it.hasNext();) {
				CommunicationManager manager = (CommunicationManager) it.next();
				try {
					manager.getAutobindings(descriptor, autoBindings);
				} catch (IOException e) {
					Log.error("Unable to obtain autobindings from communication manager " + manager.getCommunicationManagerId());
					Log.error(e);
				}
			}
			for (Iterator it = autoBindings.iterator(); it.hasNext();) {
				CommunicationBinding binding = (CommunicationBinding) it.next();
				addBinding(binding);
			}
		}

		if (bindings!=null)
		{
			//Bugfix 20120529 SSch: Avoid ConcurrentChangeException during sink opening.
			try{
				bindings.sharedLock();
				for (Iterator it = bindings.iterator(); it.hasNext();) {
					CommunicationBinding binding = (CommunicationBinding) it.next();
					try {
						CommunicationManager manager = CommunicationManagerRegistry.getManager(binding.getCommunicationManagerId());
						manager.registerService(EVENT_SINK_MESSAGE_TYPES, binding, incomingListener);
					} catch (IOException e) {
						// FIXME no need to signal per invocation exception
						EventingException ex = new EventingException("Unable to bind Event Sink to " + binding.getTransportAddress() + ": " + e);
						Log.info(ex);
						throw ex;
					}
				}
			}finally{
				bindings.releaseSharedLock();
			}
		}
		registered = true;
	}

	/**
	 * Closes event receiving for this event endpoint.
	 */
	@Override
	public void close() {
		// unbind all communication bindings
		for (Iterator it = bindings.iterator(); it.hasNext();) {
			CommunicationBinding binding = (CommunicationBinding) it.next();
			try {
				CommunicationManager manager = CommunicationManagerRegistry.getManager(binding.getCommunicationManagerId());
				manager.unregisterService(EVENT_SINK_MESSAGE_TYPES, binding, incomingListener);
			} catch (IOException e) {
				Log.error("unable to unbind from " + binding.getTransportAddress());
				e.printStackTrace();
			}
		}
		synchronized(map_CSubId_2_CSub)
		{
			map_CSubId_2_CSub.clear();
		}
		registered = false;
	}

	@Override
	public EventListener getEventListener() {
		return eventListener;
	}

	@Override
	public boolean isOpen() {
		return registered;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.eventing.EventSink#getSubscription(java.lang.String)
	 */
	@Override
	public ClientSubscription getSubscription(String clientSubId) {
		synchronized(map_CSubId_2_CSub)
		{
			return (ClientSubscription) map_CSubId_2_CSub.get(clientSubId);
		}
	}

	/**
	 * @param clientSubId
	 * @return the removed client subscription
	 */
	@Override
	public ClientSubscription removeSubscription(String clientSubId) {
		synchronized(map_CSubId_2_CSub)
		{
			return (ClientSubscription) map_CSubId_2_CSub.remove(clientSubId);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.eventing.EventSink#addSubscription(java.lang.String,
	 * org.ws4d.java.eventing.ClientSubscription)
	 */
	@Override
	public void addSubscription(String clientSubId, ClientSubscription subscription) {
		synchronized(map_CSubId_2_CSub)
		{
			map_CSubId_2_CSub.put(clientSubId, subscription);
		}
	}

	/**
	 * @param clientSubscriptionId
	 * @param actionUri
	 * @param outputParameter
	 * @return a possible result to a solicit-response event
	 */
	@Override
	public IParameterValue receiveLocalEvent(String clientSubscriptionId, URI actionUri, IParameterValue outputParameter) {
		ClientSubscription subscription;
		synchronized(map_CSubId_2_CSub)
		{
			subscription = (ClientSubscription) map_CSubId_2_CSub.get(clientSubscriptionId);
		}
		return eventListener.eventReceived(subscription, actionUri, outputParameter);
	}

	private final class EventSinkMessageListener extends DefaultIncomingMessageListener {

		private EventSinkMessageListener() {
			super();
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultIncomingMessageListener#handle
		 * (org.ws4d.java.message.invocation.InvokeMessage,
		 * org.ws4d.java.communication.ProtocolData)
		 */
		@Override
		public InvokeMessage handle(final InvokeMessage msg, ProtocolData protocolData) throws MessageException {
			if (!isOpen()) {
				// send Fault wsa:EndpointUnavailable
				if (Log.isInfo())
				{
					Log.info("EventSink has been closed.");
					if (Log.isDebug())
					{
						System.out.println(map_CSubId_2_CSub);
					}
				}
				
				throw new MessageException(FaultMessage.createEndpointUnavailableFault(msg));
			}
			String clientSubscriptionId = null;
			if (!(msg.getHeader() instanceof EventingMessageHeader)) 
			{
				Log.error("A header representing the eventListener supbscription ID (as part of the [reference parameters]) is missing.");
			} else {
				clientSubscriptionId = ((EventingMessageHeader)msg.getHeader()).getEventSubscriptionIdentifier().toString();
			}
			if (clientSubscriptionId == null) {
				// throw wsa:InvalidAddresingHeader exception
				FaultMessage fault = new FaultMessage(FaultMessage.TYPE_ADRESSING_FAULT);
				fault.setResponseTo(msg);
				fault.setCode(FaultMessage.CODE_SENDER_FAULT);
				// fill in subcode, reason and detail
				fault.setSubcode(FaultMessage.SUBCODE_INVALID_ADDRESSING_HEADER);
				LocalizedString reason = new LocalizedString("A header representing the eventListener supbscription ID (as part of the [reference parameters]) is missing", null);
				fault.addReason(reason);

				IParameterValue detail = ParameterValue.createElementValue(PredefinedSchemaTypes.WSA_PROBLEM_HEADER_QNAME);
				if (detail instanceof QNameValue) {
					QNameValue value = (QNameValue) detail;
					value.set(QNameFactory.getInstance().getQName(WSEConstants.WSE_ELEM_IDENTIFIER, WSEConstants.WSE_NAMESPACE_NAME));
				}
				fault.setDetail(detail);
				throw new MessageException(fault);
			}
			final ClientSubscription subscription;
			synchronized(map_CSubId_2_CSub)
			{
				subscription = (ClientSubscription) map_CSubId_2_CSub.get(clientSubscriptionId);
			}
			if (subscription == null) {
				// throw wsa:InvalidAddresingHeader exception
				FaultMessage fault = new FaultMessage(FaultMessage.TYPE_ADRESSING_FAULT);
				fault.setResponseTo(msg);
				fault.setCode(FaultMessage.CODE_SENDER_FAULT);
				// fill in subcode, reason and detail
				fault.setSubcode(FaultMessage.SUBCODE_INVALID_ADDRESSING_HEADER);
				LocalizedString reason = new LocalizedString("Unknown eventListener supbscription ID found: " + clientSubscriptionId, null);
				fault.addReason(reason);

				IParameterValue detail = ParameterValue.createElementValue(PredefinedSchemaTypes.WSA_PROBLEM_HEADER_QNAME);
				if (detail instanceof QNameValue) {
					QNameValue value = (QNameValue) detail;
					value.set(QNameFactory.getInstance().getQName(WSEConstants.WSE_ELEM_IDENTIFIER, WSEConstants.WSE_NAMESPACE_NAME));
				}
				fault.setDetail(detail);
				throw new MessageException(fault);
			}

			IParameterValue paramValue = eventListener.eventReceived(subscription, msg.getAction(), msg.getContent());

			if (paramValue != null) {
				/*
				 * Send solicit response message type response.
				 */
				String outputActionName = msg.getAction().toString();
				Service service;
				try {
					service = subscription.getServiceReference().getService();
					EventSource event = service.getEventSource(outputActionName);
					String inputActionName = event.getInputAction();
					InvokeMessage rspMsg = new InvokeMessage(inputActionName, false);
					rspMsg.getHeader().setMessageEndpoint(null);
					rspMsg.setResponseTo(msg);

					// set DPWSVersion from the Request to the Response
					rspMsg.setVersion(msg.getVersion());

					rspMsg.setContent(paramValue);

					return rspMsg;
				} catch (TimeoutException e) {
					Log.error("EventSink.handleMessage(Invoke): can't get service (timeout).");
					Log.error(e);

					throw new MessageException(FaultMessage.createEndpointUnavailableFault(msg));
				}
			} else {
				// send HTTP response (202)
				return null;
			}
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultIncomingMessageListener#handle
		 * (org.ws4d.java.message.eventing.SubscriptionEndMessage,
		 * org.ws4d.java.communication.ProtocolData)
		 */
		@Override
		public void handle(SubscriptionEndMessage msg, ProtocolData protocolData) {
			if (!isOpen()) {
				return;
			}
			URI msgSubId = msg.getSubscriptionManager().getReferenceParameters().getWseIdentifier();
			if (msgSubId == null) {
				Log.error("DefaultEventSink.handleMessage(SubscriptionEnd): received subscription end message without service subscription id");
				return;
			}

			DefaultClientSubscription clientSub = null;
			
			ArrayList subscriptionsForClient=null;
			synchronized(map_CSubId_2_CSub)
			{
				DataStructure clientSubs = map_CSubId_2_CSub.values();
				
				if (clientSubs!=null)
				{
					subscriptionsForClient=new ArrayList(clientSubs);
				}
			}
			
			for (Iterator it = subscriptionsForClient.iterator(); it.hasNext();) {
				/*
				 * We have to check each eventListener subscription in map
				 */
				clientSub = (DefaultClientSubscription) it.next();
				if (clientSub != null) {
					URI serviceSubId = clientSub.getServiceSubscriptionId();
					if (msgSubId.equals(serviceSubId)) {
						URI reason = msg.getStatus();
						eventListener.subscriptionEndReceived(clientSub, reason);
					}
				}
			}
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultIncomingMessageListener#getOperation
		 * (java.lang.String)
		 */
		@Override
		public IMessageEndpoint getOperation(String action) {
			OperationDescription operation = null;
			DefaultClientSubscription clientSub = null;
			synchronized(map_CSubId_2_CSub)
			{
				DataStructure clientSubs = map_CSubId_2_CSub.values();
				for (Iterator it = clientSubs.iterator(); it.hasNext();) {
					/*
					 * We have to check each eventListener subscription in map
					 */
					clientSub = (DefaultClientSubscription) it.next();
					if (clientSub != null) {
						ServiceReference servRef = clientSub.getServiceReference();
						Service service;
						try {
							service = servRef.getService();
							operation = service.getEventSource(action);
							if (operation != null) {
								break;
							}
						} catch (TimeoutException e) {
							Log.info(e);
						}
					}
				}
			}
			return operation;
		}

	}

}

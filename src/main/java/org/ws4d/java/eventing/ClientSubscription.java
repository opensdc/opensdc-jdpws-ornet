/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.eventing;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.URI;

/**
 * Subscription interface, allows client-side management of a subscription, such
 * as {@link #renew(long) renewing}, {@link #unsubscribe() unsubscribing},
 * obtaining the current {@link #getStatus() status}, etc.
 */
public interface ClientSubscription {

	/**
	 * Get system time in millis from the epoch when subscription will
	 * terminate. If "0", subscription will never terminate.
	 * 
	 * @return the time in terms of millis from the epoch when this subscription
	 *         will expire
	 */
	public long getTimeoutTime();

	/**
	 * Returns the endpoint reference of the server-side subscription manager
	 * for this subscription.
	 * 
	 * @return the endpoint reference of this subscription's subscription
	 *         manager
	 */
	public EndpointReference getServiceSubscriptionReference();

	/**
	 * Returns the server-side subscription identifier for this subscription (in
	 * terms of <a
	 * href="http://www.w3.org/Submission/WS-Eventing/">WS-Eventing</a>, this is
	 * the <em>wse:Identifier</em> URI).
	 * 
	 * @return the server-side subscription ID
	 */
	public URI getServiceSubscriptionId();

	/**
	 * Returns the event sink, which receives subscribed messages.
	 * 
	 * @return the event sink behind this subscription
	 */
	public EventSink getEventSink();

	/**
	 * Returns a reference to the subscribed service.
	 * 
	 * @return a reference to the service this subscription refers to
	 */
	public ServiceReference getServiceReference();

	// ----------------------- SUBCRIPTION HANDLING -----------------

	/**
	 * Renews subscription.
	 * 
	 * @param duration new duration of subscription
	 * @throws EventingException in case this subscription has already expired
	 *             or an invalid <code>duration </code> was specified
	 * @throws TimeoutException if this subscription refers to a remote service
	 *             and contacting it timed out
	 */
	public void renew(long duration) throws EventingException, TimeoutException;

	/**
	 * Terminates this subscription. If terminated renew or status requests
	 * can't be fulfilled.
	 * 
	 * @throws EventingException if the subscription has already expired
	 * @throws TimeoutException if this subscription refers to a remote service
	 *             and contacting it timed out
	 * @throws CommunicationException 
	 */
	public void unsubscribe() throws EventingException, CommunicationException;

	/**
	 * Get the expiration time of this subscription.
	 * 
	 * @return the expiration time in milliseconds from the epoch
	 * @throws TimeoutException if this subscription refers to a remote service
	 *             and contacting it timed out
	 * @throws EventingException if the subscription already expired
	 */
	public long getStatus() throws EventingException, TimeoutException;
	
	public abstract String getClientSubscriptionId();

}

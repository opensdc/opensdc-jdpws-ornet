/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.dispatch;

import org.ws4d.java.framework.module.FrameworkModule;
import org.ws4d.java.structures.HashMap;

public class FrameworkModuleRegistry 
{
	private static final FrameworkModuleRegistry instance=new FrameworkModuleRegistry();
	
	private FrameworkModuleRegistry()
	{


	}

	public static FrameworkModuleRegistry getInstance() {
		return instance;
	}
	
	private final HashMap modules=new HashMap();
	private FrameworkShutdownCallback callback=null;
	
	private volatile boolean running = false;
	
	public FrameworkModule getModule(Class<?> clz)
	{
		return (FrameworkModule) modules.get(clz);
	}

	public boolean hasModule(Class<?> clz) 
	{
		return modules.containsKey(clz);
	}

	public  boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running=running;
	}

	public void registerModule(Class<?> clz, Object o) 
	{
		modules.put(clz, o);	
	}

	public void kill() 
	{
		if (callback!=null)
			callback.frameworkToBeKilled();
	}

	public void stop() {
		if (callback!=null)
			callback.frameworkToBeStopped();		
	}

	public void setShutdownCallback(FrameworkShutdownCallback shutdownCallback) 
	{
		this.callback=shutdownCallback;
	}
}

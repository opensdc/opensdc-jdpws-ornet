/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.dispatch;

import org.ws4d.java.communication.DefaultResponseCallback;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.service.ProxyService;
import org.ws4d.java.service.ProxyServiceFactory;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.Reference;
import org.ws4d.java.service.reference.ServiceReferenceInternal;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.HostedMData;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.util.Log;

public class DefaultServiceReferenceCallback extends DefaultResponseCallback {

	protected final ServiceReferenceInternal	servRefHandler;

	/**
	 * @param servRefHandler
	 */
	public DefaultServiceReferenceCallback(ServiceReferenceInternal servRefHandler) {
		super();
		this.servRefHandler = servRefHandler;
	}

	@Override
	public void handle(Message request, GetMetadataResponseMessage response, ProtocolData protocolData) {
		if (servRefHandler.getLocation() == Reference.LOCATION_LOCAL) {
			Log.error("ServiceReferenceHandler.CallbackHandler.handleMessage: received get metadata rsp message for a local reference");
			return;
		}
		servRefHandler.setLocation(Reference.LOCATION_REMOTE);
		if (servRefHandler.getParentDeviceRef()!=null && response!=null)
		{
			ProtocolVersionInfoRegistry.getInstance().add(servRefHandler.getParentDeviceRef().getEndpointReference(), response.getVersion());
		}else{
			Log.error("Could not handle GetMetadataResponseMessage! ParentDeviceRef was null.");
		}

		HostedMData newHosted = response.getHosted(request.getTo());

		if (newHosted != null) {
			DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().updateStaticServiceReference(newHosted, servRefHandler);

			// Do this before creating / updating proxy service
			servRefHandler.setHosted(newHosted);
			// ServiceReferenceHandler.this.hosted = newHosted;
		}

		/*
		 * set parent device ref
		 */
		if (response.getHost() != null) {
			EndpointReference devEpr = response.getHost().getEndpointReference();
			ProtocolVersionInfoRegistry.getInstance().add(devEpr, response.getVersion());
			if (devEpr != null) {
				servRefHandler.setParentDeviceReference(DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().getStaticDeviceReference(devEpr));
				// ServiceReferenceHandler.this.parentDevRef =
				// DeviceServiceRegistry.getStaticDeviceReference(devEpr);
			}
		}

		/*
		 * update metadataReferences
		 */
		servRefHandler.setMetadataReferences(response.getMetadataReferences());

		/*
		 * update metadata locations
		 */
		servRefHandler.setMetaDataLocations(response.getMetadataLocations());

		/*
		 * update WSDLs
		 */
		servRefHandler.setWSDLs(response.getWSDLs());

		/*
		 * update / create proxy service, inform service listener
		 */
		if (!servRefHandler.isBuildUp()) {
			// service gets filled from WSDL(s) referenced within msg
			try {
				this.servRefHandler.setService(ProxyServiceFactory.getInstance().newProxyService(this.servRefHandler, servRefHandler.getParentDeviceRef()));
				ServiceReferenceEventRegistry.getInstance().announceServiceCreated(this.servRefHandler);
			} catch (RuntimeException e) {
				/*
				 * this might occur if loading some WSDL file failed; in that
				 * case service remains null, so we ignore this exception here
				 */
				Log.info(e);
			}

		} else if (newHosted != null) {
			/*
			 * update existing service.
			 */
			QNameSet newPortTypes = newHosted.getTypes();
			if (newPortTypes != null) {
				try {
					ProxyService service;
					service = (ProxyService) servRefHandler.getService(false);
					if (service != null) {
						service.appendPortTypes(newHosted.getTypes());
						ServiceReferenceEventRegistry.getInstance().announceServiceChanged(this.servRefHandler);
					}
				} catch (RuntimeException e) {
					/*
					 * This should not occur, as we have requested new metadata.
					 * The metadata should reference wsdl with new port types.
					 */
					Log.info(e);
				} catch (TimeoutException e) {
					// will not happen;
					Log.info(e);
				}
			}
		}

		beforeMessageRelease(request, response, protocolData);
		servRefHandler.releaseMessagePending();
	}

	protected void beforeMessageRelease(Message request, GetMetadataResponseMessage msg, ProtocolData protocolData) {
		//void
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.DefaultResponseCallback#handle(org.ws4d
	 * .java.communication.message.Message, org.ws4d.java.message.FaultMessage,
	 * org.ws4d.java.communication.ProtocolData)
	 */
	@Override
	public void handle(Message request, FaultMessage fault, ProtocolData protocolData) {
		Log.error("Get metadata request leads to fault message: " + fault);
		DeviceReference devRef = servRefHandler.getParentDeviceRef();
		if (devRef != null) {
			ProtocolVersionInfoRegistry.getInstance().add(devRef.getEndpointReference(), fault.getVersion());
		}
		servRefHandler.releaseMessagePending();
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.ws4d.java.communication.DefaultResponseCallback#
	 * handleMalformedResponseException (org.ws4d.java.message.Message,
	 * java.lang.Exception)
	 */
	@Override
	public void handleMalformedResponseException(Message request, Exception exception) {
		Log.error("Get metadata request leads to malformed response: " + exception);
		servRefHandler.releaseMessagePending();
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.ws4d.java.communication.DefaultResponseCallback#
	 * handleTransmissionException (org.ws4d.java.message.Message,
	 * java.lang.Exception)
	 */
	@Override
	public void handleTransmissionException(Message request, Exception exception) {
		Log.error("Unable to send get metadata message: " + exception+" "+servRefHandler);
		servRefHandler.releaseMessagePending();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.DefaultResponseCallback#handleTimeout
	 * (org.ws4d.java.message.Message)
	 */
	@Override
	public void handleTimeout(Message request) {
		// get metadata timed out
		Log.error("Get metadata message timed out");
		servRefHandler.releaseMessagePending();
	}

}

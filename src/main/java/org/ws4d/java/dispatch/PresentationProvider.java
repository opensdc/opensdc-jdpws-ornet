/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.dispatch;

import org.ws4d.java.presentation.Presentation;

public class PresentationProvider 
{
	private static final PresentationProvider instance=new PresentationProvider();

	public static PresentationProvider getInstance() {
		return instance;
	}



	private PresentationProvider()
	{
		//void
	}
	
	private Presentation presentation;

	public Presentation getPresentation()
	{
		return presentation;
	}

	public void setPresentation(Presentation presentation) {
		this.presentation=presentation;
	}
}

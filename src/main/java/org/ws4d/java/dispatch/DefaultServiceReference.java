/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.dispatch;

import org.ws4d.java.communication.CommunicationManagerID;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.ResponseCallback;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.service.LocalService;
import org.ws4d.java.service.ProxyService;
import org.ws4d.java.service.ProxyServiceFactory;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.Timeouts;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.Reference;
import org.ws4d.java.service.reference.ServiceReferenceInternal;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.EmptyStructures;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.EndpointReferenceSet;
import org.ws4d.java.types.HostedMData;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.types.URISet;
import org.ws4d.java.util.IDGenerator;
import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.WSDL;

/**
 * Class holds service reference.
 */
public class DefaultServiceReference implements ServiceReferenceInternal {

	private final ResponseCallback			handler							= ServiceReferenceFactory.getInstance().newResponseCallbackForServiceReference(this);

	private Service							service							= null;

	private HostedMData						hosted							= null;

	private DeviceReference					parentDevRef					= null;

	// a set of EndpointReferences pointing at the metadata locations
	private DataStructure					metadataReferences				= null;

	// a set of URIs pointing at the metadata locations
	private DataStructure					metadataLocations				= null;

	// a set of WSDLs belonging to the service
	private DataStructure					wsdls							= null;

	private URI								preferredXAddress;

	private CommunicationManagerID					preferredXAddressCommunication	= CommunicationManagerRegistry.getDefault();

	private int								location						= LOCATION_UNKNOWN;

	private final Object					synchronizer					= new Object();

	/** is the handler waiting for get metadata response message */
	private boolean							getMDataPending					= false;

	private boolean							secure							= false;

	private ServiceReferenceEventRegistry	eventAnnouncer					= ServiceReferenceEventRegistry.getInstance();

	private int NETWORK_COM_TIMEOUT=Timeouts.NETWORK_COM_TIMEOUT;

	/**
	 * Constructor, used for proxy services.
	 * 
	 * @param service
	 */
	protected DefaultServiceReference(HostedMData hosted) {
		// Re-Modularization 2011-01-21 Implement SecMod
		//TODO SSch QoSFramework
		SecurityManagerModule secMod=(SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);
		if (secMod!=null)
		{
			Iterator iter = hosted.getEndpointReferences().iterator();
			while (iter.hasNext()) 
			{
				EndpointReference epr=((EndpointReference) iter.next());
				if (this.preferredXAddress==null)
					this.preferredXAddress=epr.getAddress();

				if (secMod.isHTTPS(epr.getAddress())) {
					this.setSecureService(true);
					this.preferredXAddress=epr.getAddress();
					break;
				}
			}
		}

		this.hosted = hosted;
	}

	/**
	 * Constructor. Unknown location type of service.
	 * 
	 * @param epr
	 */
	protected DefaultServiceReference(EndpointReference epr) {
		this.hosted = new HostedMData();
		EndpointReferenceSet eprSet = new EndpointReferenceSet();
		eprSet.add(epr);
		hosted.setEndpointReferences(eprSet);
		preferredXAddress = epr.getAddress();
		//Re-Modularization 2011-01-21 Implement SecMod
		//		if (DPWSFramework.hasModule(DPWSFramework.DPWS_SECURITY_MODULE)) {
		//			this.secure = DPWSFramework.getSecurityManager().isHTTPS(epr.getAddress());
		//		}
		//TODO SSch QoS Framework
		SecurityManagerModule secMod=(SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);
		if (secMod!=null){
			this.secure = secMod.isHTTPS(epr.getAddress());
		}
		preferredXAddressCommunication = CommunicationManagerRegistry.getDefault();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("ServiceReferenceHandler [ hosted=");
		sb.append(hosted);
		String loc = (location == LOCATION_UNKNOWN ? "unknown" : (location == LOCATION_REMOTE ? "remote" : "local"));
		sb.append(", location=").append(loc);
		if (location != LOCATION_LOCAL) {
			sb.append(", preferredXAddress=").append(preferredXAddress);
		}
		sb.append(", service=").append(service);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.reference.ServiceReference#getService()
	 */
	@Override
	public Service getService() throws TimeoutException {
		return getService(true);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.reference.ServiceReference#getService(boolean)
	 */
	@Override
	public Service getService(boolean doBuildUp) throws TimeoutException {
		DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().updateServiceReferenceCache(this);

		if (!doBuildUp) {
			return service;
		}

		synchronized (synchronizer) {
			if (service == null) {

				EndpointReference firstEpr = (EndpointReference) hosted.getEndpointReferences().iterator().next();
				if (preferredXAddress == null) {
					/*
					 * FIXME is there something we could do here??? perhaps do a
					 * complete discovery cycle beginning with an empty probe???
					 */
					// take the first (or any other) one
					preferredXAddress = firstEpr.getAddress();
					preferredXAddressCommunication = CommunicationManagerRegistry.getDefault();
				}

				QNameSet types = hosted.getTypes();
				if (types != null && !types.isEmpty()) {
					// hosted block is not empty, we can try a local WSDL load
					try {
						service = ProxyServiceFactory.getInstance().newProxyService(this, parentDevRef);
						// nice! :-) now let's see whether we have a service ID
						// ...
						URI serviceId = getServiceId();
						if (serviceId == null) {
							// set to a "faked" one
							serviceId = IDGenerator.getUUIDasURI();
							hosted.setServiceId(serviceId);
						}
						eventAnnouncer.announceServiceCreated(this);

						return service;
					} catch (RuntimeException e) {
						// some port types not found within local repo :(
						// try resolving service metadata
					}
				}

				if (!getMDataPending) {
					getMDataPending = true;
					final GetMetadataMessage getMetadata = new GetMetadataMessage();
					getMetadata.getHeader().setEndpointReference(firstEpr);
					getMetadata.setTargetAddress(preferredXAddress);
					if (parentDevRef != null) {
						getMetadata.setVersion(ProtocolVersionInfoRegistry.getInstance().get(parentDevRef.getEndpointReference()));
					} else {
						getMetadata.setVersion(null);
					}

					// TODO Do timer/timeout in Communication Manager
					// if (getMDataTimer == null) {
					// getMDataTimer = new TimedEntry() {
					//
					// protected void timedOut() {
					// handler.handleTimeout(getMetadata);
					// }
					// };
					// }
					// WatchDog.getInstance().register(getMDataTimer,
					// DispatchingProperties.getInstance().getResponseWaitTime());

					OutDispatcher.getInstance().send(getMetadata, preferredXAddressCommunication, handler);
				}

				try {
					synchronizer.wait(NETWORK_COM_TIMEOUT);
				} catch (InterruptedException e) {
					Log.info(e);
				}
			}

			if (service == null) {
				Log.error("Service cannot be build up");
				throw new TimeoutException("Service can not be created. "+preferredXAddress);
			}
		}

		return service;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.dispatch.ServiceReferenceInternal#setService(org.ws4d.java
	 * .service.LocalService, org.ws4d.java.types.HostedMData)
	 */
	@Override
	public Service setService(LocalService service, HostedMData hosted) {
		Service oldService = this.service;
		this.service = service;
		if (service != null) {
			this.hosted = hosted;
			if (location == LOCATION_UNKNOWN) {
				location = LOCATION_LOCAL;
				// we know now, that the service is not remote
				DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().removeServiceReferenceFromCache(this);
			}
		} else {
			eventAnnouncer.announceServiceDisposed(this);
		}

		return oldService;
	}

	public Service rebuildService() throws TimeoutException {
		if (location == LOCATION_LOCAL) {
			Log.error("ServiceReferenceHandler.rebuildService: service is local, cannot be rebuild!");
			return service;
		}

		this.service = null;
		eventAnnouncer.announceServiceDisposed(this);

		return getService();
	}

	/**
	 * Update service references with hosted metadata. If new metadata lacks of
	 * previous transmitted port types, the associated service is removed. If
	 * new metadata includes new port types, service is updated.
	 * 
	 * @param endpoint Endpoint references to set.
	 */
	@Override
	public void update(HostedMData newHostedBlock, ProtocolData protocolData) {
		if (location == LOCATION_LOCAL) {
			Log.error("ServiceReferenceHandler.update: location is local");
			return;
		}
		location = LOCATION_REMOTE;

		/*
		 * set preferred x address
		 */
		setPreferredXAddress(newHostedBlock.getEndpointReferences(), protocolData);

		if (!newHostedBlock.getServiceId().equals(hosted.getServiceId())) {
			Log.error("ServiceReferenceHandler.update: Updating a service reference with a different service id: " + newHostedBlock.getServiceId());
			// throw new
			// FrameworkException("Updating a service reference with a different service id "
			// + newHostedBlock.getServiceId());
		}

		// check port types
		QNameSet otherTypes = newHostedBlock.getTypes();
		if (hosted.getTypes() == null || hosted.getTypes().size() == 0) {
			// no types received until now => use the new
			hosted.setTypes(otherTypes);
		} else if (hosted.getTypes().containsAll(otherTypes)) {
			// no new types
			if (otherTypes.size() != hosted.getTypes().size()) {
				/*
				 * CASE: some types are no more supported => discard service
				 */
				this.service = null;
				eventAnnouncer.announceServiceDisposed(this);
			}

			hosted = newHostedBlock;
			return;
		}

		/*
		 * new port types
		 */

		if (service == null) {
			// no service is build up => only change the service reference
			hosted = newHostedBlock;
			return;
		}

		if (this.service != null) {
			/*
			 * Build up service
			 */

			try {
				((ProxyService) service).appendPortTypes(otherTypes);
				hosted = newHostedBlock;
				eventAnnouncer.announceServiceChanged(this);
			} catch (RuntimeException e) {
				/*
				 * CASE: We don't have all port types in repository =>
				 * information required from service.
				 */
				EndpointReference firstEpr = (EndpointReference) newHostedBlock.getEndpointReferences().iterator().next();
				if (preferredXAddress == null) {
					// take the first (or any other) one
					preferredXAddress = firstEpr.getAddress();
					preferredXAddressCommunication = protocolData.getCommunicationManagerId();
				}

				// send get metadata

				synchronized (synchronizer) {
					if (!getMDataPending) {
						getMDataPending = true;
						GetMetadataMessage getMetadata = new GetMetadataMessage();
						getMetadata.getHeader().setEndpointReference(firstEpr);
						getMetadata.setTargetAddress(preferredXAddress);
						getMetadata.setVersion(ProtocolVersionInfoRegistry.getInstance().get(parentDevRef.getEndpointReference()));
						OutDispatcher.getInstance().send(getMetadata, preferredXAddressCommunication, handler);
					}

					try {
						synchronizer.wait(NETWORK_COM_TIMEOUT);
					} catch (InterruptedException e1) {
						Log.info(e1);
					}
				}
			}
		}

		return;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.reference.ServiceReference#getPortTypes()
	 */
	@Override
	public Iterator getPortTypes() {
		QNameSet names = hosted.getTypes();
		return names == null ? EmptyStructures.EMPTY_ITERATOR : new ReadOnlyIterator(names.iterator());
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.reference.Reference#getLocation()
	 */
	@Override
	public int getLocation() {
		return location;
	}

	/**
	 * Location of service, which this reference is linked to. Allowed values:
	 * <nl>
	 * <li> {@link Reference#LOCATION_LOCAL},
	 * <li> {@link Reference#LOCATION_REMOTE} or
	 * <li> {@link Reference#LOCATION_UNKNOWN}
	 * </nl>
	 * 
	 * @param location {@link Reference#LOCATION_LOCAL},
	 *            {@link Reference#LOCATION_REMOTE} or
	 *            {@link Reference#LOCATION_UNKNOWN}.
	 */
	@Override
	public void setLocation(int location) {
		this.location = location;
	}

	/**
	 * Get endpoint references.
	 * 
	 * @return endpoint references.
	 */
	@Override
	public Iterator getEndpointReferences() {
		EndpointReferenceSet eprs = hosted.getEndpointReferences();
		return eprs == null ? EmptyStructures.EMPTY_ITERATOR : new ReadOnlyIterator(eprs.iterator());
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.reference.ServiceReference#getServiceId()
	 */
	@Override
	public URI getServiceId() {
		return hosted.getServiceId();
	}

	/**
	 * Returns an iterator over the set of {@link EndpointReference} instances
	 * pointing at the locations of the target service's metadata descriptions
	 * (i.e. usually its WSDL files).
	 * 
	 * @return an iterator over {@link EndpointReference}s to the service's
	 *         metadata
	 */
	@Override
	public Iterator getMetadataReferences() {
		return metadataReferences == null ? EmptyStructures.EMPTY_ITERATOR : new ReadOnlyIterator(metadataReferences);
	}

	/**
	 * Returns an iterator over the set of {@link URI} instances pointing at the
	 * addresses of the target service's metadata description locations (i.e.
	 * usually its WSDL files).
	 * 
	 * @return an iterator over {@link URI}s to the service's metadata
	 */
	@Override
	public Iterator getMetadataLocations() {
		return metadataLocations == null ? EmptyStructures.EMPTY_ITERATOR : new ReadOnlyIterator(metadataLocations);
	}

	/**
	 * Returns an iterator over the set of {@link WSDL} instances describing the
	 * target service.
	 * 
	 * @return an iterator over {@link WSDL}s containing the service's metadata
	 */
	@Override
	public Iterator getWSDLs() {
		return wsdls == null ? EmptyStructures.EMPTY_ITERATOR : new ReadOnlyIterator(wsdls);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.reference.Reference#getPreferredXAddress()
	 */
	@Override
	public URI getPreferredXAddress() {
		return preferredXAddress;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.Reference#getPreferredXAddressProtocol()
	 */
	@Override
	public CommunicationManagerID getPreferredCommunicationManagerID() {
		return preferredXAddressCommunication;
	}

	/**
	 * Stores preferredXAddress for target service.
	 * 
	 * @param message
	 * @param eprs
	 */
	private void setPreferredXAddress(EndpointReferenceSet eprs, ProtocolData protocolData) {
		URI preferredXAddress = null;
		URISet xAddresses = eprs.getAddresses();
		if (xAddresses.size() == 1) {
			preferredXAddress = (URI) xAddresses.iterator().next();
		} else {
			for (Iterator it2 = xAddresses.iterator(); it2.hasNext();) {
				URI xAddr = (URI) it2.next();
				if (protocolData.sourceMatches(xAddr)) {
					preferredXAddress = xAddr;
					break;
				}
			}
			if (preferredXAddress == null) {
				if (this.preferredXAddress != null) {
					return;
				} else {
					// take the first (or any other) one
					preferredXAddress = (URI) xAddresses.iterator().next();
				}
			}
		}
		this.preferredXAddress = preferredXAddress;
		this.preferredXAddressCommunication = protocolData.getCommunicationManagerId();
	}

	/**
	 * @param devRef
	 */
	@Override
	public void setParentDeviceReference(DeviceReference devRef) {
		this.parentDevRef = devRef;
	}

	@Override
	public DeviceReference getParentDeviceRef() {
		return parentDevRef;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.reference.ServiceReference#isBuildUp()
	 */
	@Override
	public boolean isBuildUp() {
		return service != null;
	}

	@Override
	public boolean isSecureService() {
		return secure;
	}

	/**
	 * If this service uses security techniques this should be used to set the
	 * state
	 * 
	 * @param sec
	 */
	void setSecureService(boolean sec) {
		this.secure = sec;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.dispatch.ServiceReferenceInternal#setHosted(org.ws4d.java
	 * .types.HostedMData)
	 */
	@Override
	public void setHosted(HostedMData newHosted) {
		hosted = newHosted;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.dispatch.ServiceReferenceInternal#setMetaDataLocations(org
	 * .ws4d.java.types.URISet)
	 */
	@Override
	public void setMetaDataLocations(URISet metaLocs) {
		synchronized (synchronizer) {
			if (metadataLocations == null) {
				metadataLocations = new HashSet();
			} else {
				metadataLocations.clear();
			}
			if (metaLocs != null) {
				for (Iterator it = metaLocs.iterator(); it.hasNext();) {
					URI location = (URI) it.next();
					metadataLocations.add(location);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.dispatch.ServiceReferenceInternal#setMetadataReferences
	 * (org.ws4d.java.types.EndpointReferenceSet)
	 */
	@Override
	public void setMetadataReferences(EndpointReferenceSet metaRefs) {
		synchronized (synchronizer) {
			if (metadataReferences == null) {
				metadataReferences = new HashSet();
			} else {
				metadataReferences.clear();
			}
			if (metaRefs != null) {
				for (Iterator it = metaRefs.iterator(); it.hasNext();) {
					EndpointReference epr = (EndpointReference) it.next();
					metadataReferences.add(epr);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.dispatch.ServiceReferenceInternal#setWSDLs(org.ws4d.java
	 * .structures.DataStructure)
	 */
	@Override
	public void setWSDLs(DataStructure wsdls) {
		synchronized (synchronizer) {
			if (this.wsdls == null) {
				this.wsdls = new HashSet();
			} else {
				this.wsdls.clear();
			}
			if (wsdls != null) {
				for (Iterator it = wsdls.iterator(); it.hasNext();) {
					WSDL wsdl = (WSDL) it.next();
					this.wsdls.add(wsdl);
				}
			}
		}
	}

	@Override
	public void addWSDLs(WSDL wsdl) {
		synchronized (synchronizer) {
			if (this.wsdls == null) {
				this.wsdls = new HashSet();
			}
			if (wsdls != null) {
				this.wsdls.add(wsdl);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.dispatch.ServiceReferenceInternal#setService(org.ws4d.java
	 * .service.ProxyService)
	 */
	@Override
	public void setService(Service proxyService) {
		this.service = proxyService;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.dispatch.ServiceReferenceInternal#releaseMessagePending()
	 */
	@Override
	public void releaseMessagePending() {
		synchronized (synchronizer) {
			getMDataPending = false;
			synchronizer.notifyAll();
		}
	}



}

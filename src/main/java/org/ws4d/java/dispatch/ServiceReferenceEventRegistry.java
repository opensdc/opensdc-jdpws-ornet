/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.dispatch;

import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.service.reference.ServiceListener;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.LockedList;

/**
 * Registry to manage all listeners to service changes, who implement the
 * {@link ServiceListener} interface.
 */
public class ServiceReferenceEventRegistry {

	private static ServiceReferenceEventRegistry	instance	= new ServiceReferenceEventRegistry();

	public static ServiceReferenceEventRegistry getInstance() {
		return instance;
	}

	private LockedList	listeners	= null;

	private ServiceReferenceEventRegistry() {

	}

	/**
	 * Register listener (callback) for service reference changes. Listeners get
	 * get information about service changes.
	 * 
	 * @param listener listener (callback) to register.
	 */
	public synchronized void registerServiceListening(ServiceListener listener) {
		if (listeners == null) {
			listeners = new LockedList();
		}
		listeners.add(listener);
	}

	/**
	 * Unregisters listener for service reference listening. This method should
	 * be called, if holder of reference is no more interested in this
	 * reference.
	 * 
	 * @param listener listener to remove.
	 */
	public synchronized void unregisterServiceListening(ServiceListener listener) {
		listeners.remove(listener);
	}

	protected void announceServiceChanged(final ServiceReference serviceRef) {
		if (listeners == null) {
			return;
		}
		listeners.sharedLock();
		try {
			for (Iterator it = listeners.iterator(); it.hasNext();) {
				final ServiceListener listener = ((ServiceListener) it.next());
				PlatformSupport.getInstance().getToolkit().getThreadPool().execute(new Runnable() {

					@Override
					public void run() {
						listener.serviceChanged(serviceRef);
					}
				});
			}
		} finally {
			listeners.releaseSharedLock();
		}
	}

	protected void announceServiceCreated(final ServiceReference serviceRef) {
		if (listeners == null) {
			return;
		}
		listeners.sharedLock();
		try {
			for (Iterator it = listeners.iterator(); it.hasNext();) {
				final ServiceListener listener = ((ServiceListener) it.next());
				PlatformSupport.getInstance().getToolkit().getThreadPool().execute(new Runnable() {

					@Override
					public void run() {
						listener.serviceCreated(serviceRef);
					}
				});
			}
		} finally {
			listeners.releaseSharedLock();
		}
	}

	protected void announceServiceDisposed(final ServiceReference serviceRef) {
		if (listeners == null) {
			return;
		}
		listeners.sharedLock();
		try {
			for (Iterator it = listeners.iterator(); it.hasNext();) {
				final ServiceListener listener = ((ServiceListener) it.next());
				PlatformSupport.getInstance().getToolkit().getThreadPool().execute(new Runnable() {

					@Override
					public void run() {
						listener.serviceDisposed(serviceRef);
					}
				});
			}
		} finally {
			listeners.releaseSharedLock();
		}
	}
}

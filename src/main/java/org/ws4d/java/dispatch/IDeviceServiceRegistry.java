/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.dispatch;

import org.ws4d.java.communication.CommunicationBinding;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.message.Message;
import org.ws4d.java.service.LocalDevice;
import org.ws4d.java.service.LocalService;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.service.reference.ServiceReferenceInternal;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.types.DiscoveryData;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.HostedMData;
import org.ws4d.java.types.ProbeScopeSet;
import org.ws4d.java.types.QNameSet;

public interface IDeviceServiceRegistry {
	public abstract void init();
	public abstract void tearDown();
	public abstract DeviceReference getStaticDeviceReference(HelloData helloData);
	public abstract DeviceReference getStaticDeviceReference(EndpointReference epr, boolean doCreate);
	public abstract DeviceReference getStaticDeviceReference(EndpointReference endpointReference);
	public abstract DeviceReference getStaticDeviceReference(LocalDevice device);
	public abstract ServiceReference getStaticServiceReference(HostedMData hosted);
	public abstract ServiceReference getStaticServiceReference(EndpointReference endpointReference);
	public abstract ServiceReference getStaticServiceReference(EndpointReference endpointReference, boolean doCreate);
	public abstract DataStructure getLocalDeviceReferences(QNameSet deviceTypes, ProbeScopeSet scopes);
	public abstract DataStructure getLocalServiceReferences(QNameSet serviceTypes, QNameSet deviceTypes, ProbeScopeSet scopes);
	public abstract DeviceReference getUpdatedDeviceReference(DiscoveryData newData, Message msg, ProtocolData protocolData);
	public abstract void register(LocalDevice device);
	public abstract void unregister(LocalDevice device);
	public abstract void register(LocalService service);
	public abstract void unregister(LocalService service);
	public abstract void register(CommunicationBinding binding);
	public abstract void unregister(CommunicationBinding binding);
	public abstract void unregisterServiceReference(ServiceReference servRef);
	public abstract void unregisterDeviceReference(DeviceReference adeviceReference);
	public abstract void addServiceReferenceToCache(ServiceReference servRef);
	public abstract void registerDeviceReference(DeviceReference devRef);
	public abstract ServiceReferenceInternal updateStaticServiceReference(HostedMData newHosted);
	public abstract ServiceReferenceInternal updateStaticServiceReference(HostedMData newHosted, ServiceReferenceInternal servRef);
	public abstract void updateServiceReferenceCache(ServiceReference servRef);
	public abstract void removeServiceReferenceFromCache(ServiceReference servRef);
	public abstract DataStructure getAllRegisteredDevices();

	
}

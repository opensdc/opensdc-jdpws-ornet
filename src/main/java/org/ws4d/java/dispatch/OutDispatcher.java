/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.dispatch;

import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.CommunicationManagerID;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.Discovery;
import org.ws4d.java.communication.ProtocolDomain;
import org.ws4d.java.communication.ResponseCallback;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.discovery.ByeMessage;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.message.discovery.ResolveMessage;
import org.ws4d.java.message.eventing.GetStatusMessage;
import org.ws4d.java.message.eventing.RenewMessage;
import org.ws4d.java.message.eventing.SubscribeMessage;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.message.eventing.UnsubscribeMessage;
import org.ws4d.java.message.metadata.GetMessage;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;

/**
 * @author mspies
 */
public class OutDispatcher {

	private static final OutDispatcher	INSTANCE	= new OutDispatcher();

	/**
	 * Returns the output dispatcher.
	 * 
	 * @return the output dispatcher.
	 */
	public static OutDispatcher getInstance() {
		return INSTANCE;
	}

	private static DataStructure getAllAvailableDomains() {
		DataStructure domains = new ArrayList();
		for (Iterator it = CommunicationManagerRegistry.getLoadedManagers(); it.hasNext();) {
			CommunicationManager manager = (CommunicationManager) it.next();
			DataStructure availDomains= manager.getAvailableDomains();
			
			//Bugfix SSch 2011-11-22 Do not include duplicate domains
			for(Iterator domainIt=availDomains.iterator();domainIt.hasNext();)
			{
				Object o=domainIt.next();
				if (!domains.contains(o))
					domains.add(o);
			}
		}
		// FIXME debug
		if (Log.isDebug()) {
			Log.debug("-------------------------------------------");
			for (Iterator it = domains.iterator(); it.hasNext();) {
				Log.debug("Domain: " + it.next());
			}
			Log.debug("-------------------------------------------");
		}
		return domains;
	}

	private OutDispatcher() {
		super();
	}

	public void send(HelloMessage hello, DataStructure protocolDomains) {
		sendMulticast(hello, protocolDomains, null);
	}

	public void send(ByeMessage bye, DataStructure protocolDomains) {
		sendMulticast(bye, protocolDomains, null);
	}

	public void send(ProbeMessage probe, DataStructure protocolDomains, org.ws4d.java.communication.ResponseCallback callback) {
		sendMulticast(probe, protocolDomains, callback);
	}

	public void send(ResolveMessage resolve, DataStructure protocolDomains, org.ws4d.java.communication.ResponseCallback callback) {
		sendMulticast(resolve, protocolDomains, callback);
	}

	// this is for directed probes only!
	public void send(ProbeMessage probe, CommunicationManagerID communicationId, org.ws4d.java.communication.ResponseCallback callback) {
		sendUnicast(probe, communicationId, callback);
	}

	public void send(GetMessage get, CommunicationManagerID communicationId, org.ws4d.java.communication.ResponseCallback callback) {
		sendUnicast(get, communicationId, callback);
	}

	public void send(GetMetadataMessage getMetadata, CommunicationManagerID communicationId, org.ws4d.java.communication.ResponseCallback callback) {
		sendUnicast(getMetadata, communicationId, callback);
	}

	public void send(SubscribeMessage subscribe, CommunicationManagerID communicationId, org.ws4d.java.communication.ResponseCallback callback) {
		sendUnicast(subscribe, communicationId, callback);
	}

	public void send(GetStatusMessage getStatus, CommunicationManagerID communicationId, org.ws4d.java.communication.ResponseCallback callback) {
		sendUnicast(getStatus, communicationId, callback);
	}

	public void send(RenewMessage renew, CommunicationManagerID communicationId, org.ws4d.java.communication.ResponseCallback callback) {
		sendUnicast(renew, communicationId, callback);
	}

	public void send(UnsubscribeMessage unsubscribe, CommunicationManagerID communicationId, org.ws4d.java.communication.ResponseCallback callback) {
		sendUnicast(unsubscribe, communicationId, callback);
	}

	public void send(SubscriptionEndMessage subscriptionEnd, CommunicationManagerID communicationId) {
		sendUnicast(subscriptionEnd, communicationId, null);
	}

	public void send(InvokeMessage invoke, CommunicationManagerID communicationId, org.ws4d.java.communication.ResponseCallback callback) {
		sendUnicast(invoke, communicationId, callback);
	}

	private void sendMulticast(Message message, DataStructure protocolDomains, org.ws4d.java.communication.ResponseCallback callback) {
		if (message == null) {
			return;
		}
		if (message.getRoutingScheme()==Message.UKNOWN_ROUTING_SCHEME)
			message.setRoutingScheme(Message.MULTICAST_ROUTING_SCHEME);
		if (protocolDomains == null) {
			protocolDomains = Discovery.getDefaultOutputDomains();
		}
		if (protocolDomains.isEmpty()) {
			protocolDomains = getAllAvailableDomains();
		}
		preSend(message);
		for (Iterator it = protocolDomains.iterator(); it.hasNext();) {
			ProtocolDomain domain = (ProtocolDomain) it.next();
			CommunicationManager comman = CommunicationManagerRegistry.getManager(domain.getCommunicationManagerId());
			if (comman!=null)
				comman.send(message, domain, callback);
		}
	}

	private void sendUnicast(Message message, CommunicationManagerID communicationId, org.ws4d.java.communication.ResponseCallback callback) {
		if (message == null) {
			return;
		}
		if (message.getRoutingScheme()==Message.UKNOWN_ROUTING_SCHEME)
			message.setRoutingScheme(Message.UNICAST_ROUTING_SCHEME);
		if (communicationId == null) {
			// Log.warn("No protocol ID to send unicast message over: " +
			// message);
			// return;
			// instead of rejecting this, we use a system default one
			communicationId = CommunicationManagerRegistry.getDefault();
		}
		preSend(message);
		CommunicationManager comman = CommunicationManagerRegistry.getManager(communicationId);
		if (comman!=null)
			comman.send(message, null, callback);
		else
			Log.warn("Could not send message. No CommunicationManager for "+communicationId);
	}

	private void preSend(Message message) {
		message.setInbound(false);
		if (Log.isDebug()) {
			Log.debug("<O> " + message);
		}
	}

	public void sendGenericMessage(Message msg,
			CommunicationManagerID communicationId, ResponseCallback callback) {
		if (msg!=null && msg.getRoutingScheme()==Message.UNICAST_ROUTING_SCHEME)
		{
				sendUnicast(msg, communicationId, callback);
				return;
		}
		else
		{
			CommunicationManager comman = CommunicationManagerRegistry.getManager(communicationId);
			if (comman!=null)
				comman.send(msg, null, callback);
		}
	}

	public void sendGenericMessageToProtocolDomain(Message msg, DataStructure protocolDomains,
			ResponseCallback callback) {
		if (msg!=null && msg.getRoutingScheme()==Message.MULTICAST_ROUTING_SCHEME)
		{
			sendMulticast(msg, protocolDomains, callback);
		}else{
			if (Log.isInfo())
				Log.info("Could not send message as multicast. "+msg);
		}
	}

}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.dispatch;

import org.ws4d.java.util.Log;

public class DeviceServiceRegistryProvider {
	private static DeviceServiceRegistryProvider instance=new DeviceServiceRegistryProvider();

	
	public static DeviceServiceRegistryProvider getInstance() {
		return instance;
	}

	private IDeviceServiceRegistry registry;

	private DeviceServiceRegistryProvider() {
		super();
		//TODO SSch make this configurable
		IDeviceServiceRegistry registry = null;
//		String factoryClassName = FrameworkProperties.getInstance().getWsdlSupportFactoryClass();
		String factoryClassName = "org.ws4d.java.dispatch.DefaultDeviceServiceRegistry";
//		if (factoryClassName == null) {
//			// use default one silently
//			factoryClassName = "org.ws4d.java.dispatch.DefaultDeviceServiceRegistry";
//		} else 
		{
			try {
				Class<?> factoryClass = Class.forName(factoryClassName);
				registry = (IDeviceServiceRegistry) factoryClass.newInstance();
				if (Log.isDebug()) {
					Log.debug("Using IDeviceServiceRegistry [" + factoryClassName + "]");
				}
			} catch (ClassNotFoundException e) {
				Log.error("Configured IDeviceServiceRegistry class [" + factoryClassName + "] not found, falling back to default implementation");
				Log.error(e);
			} catch (Exception ex) {
				Log.error("Unable to create instance of configured WSDLSupportFactory class [" + factoryClassName + "], falling back to default implementation");
				Log.error(ex);
			}
		}
		this.registry=registry;
	}
	
	public IDeviceServiceRegistry getDeviceServiceRegistry()
	{
		return registry;
	}
}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.dispatch;

import org.ws4d.java.communication.CommunicationManagerID;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.DefaultResponseCallback;
import org.ws4d.java.communication.Discovery;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.ProtocolVersionInfo;
import org.ws4d.java.communication.ResponseCallback;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.configuration.DispatchingProperties;
import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.discovery.ProbeMatch;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMessage;
import org.ws4d.java.message.metadata.GetMessage;
import org.ws4d.java.message.metadata.GetResponseMessage;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.LocalDevice;
import org.ws4d.java.service.ProxyDevice;
import org.ws4d.java.service.ProxyService;
import org.ws4d.java.service.reference.DeviceListener;
import org.ws4d.java.service.reference.LocalDeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.service.reference.ServiceReferenceInternal;
import org.ws4d.java.structures.AppSequenceTracker;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.EmptyStructures;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.LockedSet;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.types.AppSequence;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.DiscoveryData;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.HostMData;
import org.ws4d.java.types.HostedMData;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.ScopeSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.types.URISet;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.TimedEntry;
import org.ws4d.java.util.WatchDog;

/**
 * Class holds listeners of device reference. Also manages creating and
 * disposing of devices.
 */
public class DefaultDeviceReference extends TimedEntry implements LocalDeviceReference {

	/**
	 * This sequence number should only be used to compare the first incoming
	 * message to proxy devices
	 */
	public static final AppSequence	APP_SEQUENCE_ZERO				= new AppSequence(-1, 0);

	public static final int			EVENT_DEVICE_SEEN				= 0;

	public static final int			EVENT_DEVICE_BYE				= 1;

	public static final int			EVENT_DEVICE_GET_RSP			= 2;

	public static final int			EVENT_DEVICE_CHANGED			= 3;

	public static final int			EVENT_DEVICE_FAULT_RESET		= 4;

	private boolean					isSecure						= false;

	// ------------------------------------------------------------------------------------

	private final CallbackHandler	handler							= new CallbackHandler();

	private Device					device							= null;

	private LockedSet				listeners						= new LockedSet();

	/** Changes to local device discovery data must not occur */
	private DiscoveryData			discoveryData					= null;

	private URI						preferredXAddress;

	private CommunicationManagerID			preferredXAddressCommunication	= CommunicationManagerRegistry.getDefault();

	private MessageState			resolveState					= new MessageState();

	private MessageState			getState						= new MessageState();

	private MessageState			directedProbeState				= new MessageState();

	private boolean					completeDiscovered				= false;

	private int						location						= LOCATION_UNKNOWN;

	private AppSequenceTracker		appSequenceTracker				= null;

	private StateManager			proxyReferenceState				= new StateManager();

	// private final LockSupport lock = new LockSupport();

	// -------------------------- CONSTRUCTOR ---------------------------

	/**
	 * Constructor, device is not initialized. This constructor is used for a
	 * proxy device.
	 * 
	 * @param data discovery data.
	 * @param protocolData
	 */
	DefaultDeviceReference(AppSequence appSeq, DiscoveryData data, ProtocolData protocolData) {
		super();
		this.discoveryData = data;
		this.location = LOCATION_REMOTE;
		appSequenceTracker = new AppSequenceTracker(appSeq);
		setPreferredXAddress(data, protocolData);
		setPreferredDPWSVersion();

		// Condition: we must not use Bye-Messages with metadata version to init
		if (data.getMetadataVersion() > DiscoveryData.UNKNOWN_METADATA_VERSION) {
			proxyReferenceState.setState(STATE_RUNNING);
		}

		WatchDog.getInstance().register(this, DispatchingProperties.getInstance().getReferenceCachingTime());
	}

	/**
	 * Constructor. Location of device is unknown.
	 * 
	 * @param epr
	 */
	DefaultDeviceReference(EndpointReference epr) {
		super();
		this.discoveryData = new DiscoveryData(epr, DiscoveryData.UNKNOWN_METADATA_VERSION);
		appSequenceTracker = new AppSequenceTracker();
		setPreferredDPWSVersion();
		WatchDog.getInstance().register(this, DispatchingProperties.getInstance().getReferenceCachingTime());
	}

	/**
	 * Constructor. Only to be used by local devices.
	 * 
	 * @param device Local device.
	 */
	DefaultDeviceReference(LocalDevice device) {
		super();
		setPreferredDPWSVersion();
		this.discoveryData = device.getDiscoveryData();
		setLocalDevice(device);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.management.TimedEntry#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("DeviceReference [ discoveryData=");
		sb.append(discoveryData);
		String loc = (location == LOCATION_UNKNOWN ? "unknown" : (location == LOCATION_REMOTE ? "remote" : "local"));
		sb.append(", location=").append(loc);
		if (location != LOCATION_LOCAL) {
			sb.append(", preferredXAddress=").append(preferredXAddress);
		}
		sb.append(", device=").append(device);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.reference.DeviceReference#getState()
	 */
	@Override
	public int getState() {
		if (location == LOCATION_LOCAL) {
			LocalDevice device = (LocalDevice) this.device;
			if (device != null) {
				return device.isRunning() ? STATE_BUILD_UP : STATE_STOPPED;
			} else {
				Log.error("DefaultDeviceReference.getState: Location is local, but no device specified");
				return STATE_UNKNOWN;
			}
		}
		synchronized (this) {
			return proxyReferenceState.getState();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.reference.DeviceReference#getDevice()
	 */
	@Override
	public synchronized Device getDevice() throws TimeoutException {
		return getDevice(true);
	}

	/**
	 * Returns device. If doBuildUp is <code>false</code>, no proxy device will
	 * be created, i.e. no resolve and get messages will be sent.
	 * 
	 * @param doBuildUp Specififies that a proxy device should be buildt up, if
	 *            no device already exists.
	 * @return May be <code>null</code>, if build up was not requested.
	 * @throws TimeoutException
	 */
	protected synchronized Device getDevice(boolean doBuildUp) throws TimeoutException {
		if (!doBuildUp) {
			return device;
		}

		if (location != LOCATION_LOCAL && proxyReferenceState.getState() == STATE_STOPPED) {
			if (preferredXAddress == null) {
				resolveRemoteDevice();
			} else {
				sendDirectedProbe();
			}

			if (proxyReferenceState.getState() == STATE_STOPPED || proxyReferenceState.getState() == STATE_UNKNOWN) {
				throw new TimeoutException("Device does not respond to directed probe, probably offline.");
			}
		}


		if (device != null && device.isValid()) {
			return device;
		}

		resolveRemoteDevice();

		while (directedProbeState.pendling) {
			try {
				wait();
			} catch (InterruptedException e) {
				//void
			}
		}

		// This method waits until device is built up, or error occurs.
		return getProxyDevice();
	}

	/**
	 * Sets device, replaces present device. Only to be used for local devices.
	 * 
	 * @param device Replacement device (local).
	 * @return replaced device.
	 */
	@Override
	public Device setLocalDevice(LocalDevice device) {
		if (this.device == device) {
			return device;
		}

		if (device == null) {
			/*
			 * CASE: somebody want to move local device to remote location or
			 * remove device from device ref.
			 */
			location = LOCATION_UNKNOWN;
			Device oldDevice = this.device;
			this.device = null;
			discoveryData = new DiscoveryData(discoveryData.getEndpointReference(), discoveryData.getMetadataVersion());
			completeDiscovered = false;
			return oldDevice;
		}

		if (location == LOCATION_REMOTE) {
			// XXX think about moving remote device to local location
			Log.error("DefaultDeviceReference.setDevice: Setting local device to remote reference: Two devices using the same endpoint reference!");
			throw new RuntimeException("Setting local device to a remote reference!");
		}

		location = LOCATION_LOCAL;
		completeDiscovered = true;

		LocalDevice oldDevice = (LocalDevice) this.device;
		this.device = device;

		WatchDog.getInstance().unregister(this);
		// copy device metadata from device
		discoveryData = device.getDiscoveryData();

		if (oldDevice == null || !device.equals(oldDevice)) {
			if (device.isRunning()) {
				announceDeviceChangedAndBuildUp();
			}
		}

		return oldDevice;
	}

	/**
	 * Rebuilds device. Removes all service references from registry. Should
	 * only be used for remote devices.
	 * 
	 * @return Rebuild device.
	 * @throws TimeoutException
	 */
	public synchronized Device rebuildDevice() throws TimeoutException {
		if (location == LOCATION_LOCAL) {
			Log.error("DefaultDeviceReference.rebuildDevice: device is local, cannot be rebuild!");
			return device;
		}

		Device oldDevice = device;
		if (oldDevice != null) {
			for (Iterator it = oldDevice.getServiceReferences(); it.hasNext();) {
				ServiceReference servRef = (ServiceReference) it.next();
				DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().unregisterServiceReference(servRef);
			}
			changeProxyReferenceState(EVENT_DEVICE_CHANGED);
		}

		return getDevice();
	}

	/**
	 * Resets all state information of the device reference except the endpoint
	 * reference. Removes the association between the device and services.
	 */
	public synchronized void reset() {
		if (location == LOCATION_LOCAL) {
			Log.warn("DefaultDeviceReference.reset: Not allowed to reset references to local devices! ");
			return;
		}

		if (Log.isInfo()) {
			Log.info("DefaultDeviceReference.reset: Resetting device reference with endpoint reference " + discoveryData.getEndpointReference());
		}

		disposeServiceReferences();
		device = null;
		this.discoveryData = new DiscoveryData(this.discoveryData.getEndpointReference(), DiscoveryData.UNKNOWN_METADATA_VERSION);
		changeProxyReferenceState(EVENT_DEVICE_FAULT_RESET);
		location = LOCATION_UNKNOWN;

		completeDiscovered = false;
		appSequenceTracker = new AppSequenceTracker();

		this.preferredXAddress = null;
		this.preferredXAddressCommunication = null;
	}

	/**
	 * Uses directed Probe to refresh discovery data. A previously built up
	 * device will be disposed of.
	 * 
	 * @return
	 * @throws TimeoutException
	 */
	@Override
	public synchronized void fetchCompleteDiscoveryData() throws TimeoutException {
		if (completeDiscovered) {
			return;
		}

		resolveRemoteDevice();
		sendDirectedProbe();
	}

	/**
	 * Sends directed probe to device
	 */
	private void sendDirectedProbe() throws TimeoutException {
		directedProbeState.counter++;

		try {
			while (!completeDiscovered) {
				if (directedProbeState.pendling) {
					try {
						this.wait();
					} catch (InterruptedException e) {
						Log.info(e);
					}
				} else {
					switch (handler.communicationState) {
						case CallbackHandler.COM_TIMEOUT:
							throw new TimeoutException("Device state unknown, probably offline");
						case CallbackHandler.COM_ERROR:
							throw new TimeoutException("Device send fault, probably doesn't support directed probing");
						case CallbackHandler.COM_ERROR_TRANSMISSION:
							Log.info("DefaultDeviceReference.getProxyDevice: Transmission error with xaddress=" + preferredXAddress);

							URISet xaddresses = discoveryData.getXAddrs();
							xaddresses.remove(preferredXAddress);
							if (discoveryData.getXAddrs().size() == 0) {
								preferredXAddress = null;
								throw new TimeoutException("No xaddress to communicate.");
							}

							preferredXAddress = (URI) xaddresses.iterator().next();
							handler.communicationState = CallbackHandler.COM_UNKNOWN;
							break;
					}
					directedProbeState.pendling = true;

					probeDeviceDirectly(handler);
				}

			}
		} finally {
			directedProbeState.counter--;
			if (directedProbeState.counter == 0) {
				directedProbeState.pendling = false;
				if (handler.communicationState != CallbackHandler.COM_OK) {
					handler.communicationState = CallbackHandler.COM_UNKNOWN;
				}
			}
		}
	}

	@Override
	public void probeDeviceDirectly(ResponseCallback callback) {
		sendDirectedProbeMessage(preferredXAddress, preferredXAddressCommunication, callback);
	}
	
	public void sendDirectedProbeMessage(URI xAddress, CommunicationManagerID comMgr, ResponseCallback callback) {
		final ProbeMessage probe = new ProbeMessage();
		probe.setVersion(ProtocolVersionInfoRegistry.getInstance().get(getEndpointReference()));
		probe.getHeader().setTo(new AttributedURI(WSDConstants.WSD_TO));
		probe.setTargetAddress(xAddress);
		OutDispatcher.getInstance().send(probe, comMgr, callback);
	}

	/**
	 * Sends Resolve message to remote device. ResolveMatches message will be
	 * handled by callback handler, which updates discovery data. Only one
	 * Resolve message will be sent each time.
	 * 
	 * @throws TimeoutException
	 */
	public synchronized void resolveRemoteDevice() throws TimeoutException {
		if (resolveState.pendling) {
			try {
				this.wait();
			} catch (InterruptedException e1) {
				Log.info(e1);
			}
		} else if (preferredXAddress == null) {
			EndpointReference epr = discoveryData.getEndpointReference();

			/*
			 * We resolve device always if metadata of device is unknown
			 */
			if (epr.isXAddress() && discoveryData.getMetadataVersion() != DiscoveryData.UNKNOWN_METADATA_VERSION) {
				preferredXAddress = epr.getAddress();
				// XXX assume DPWS when using http and/or soap.udp URI schemas
				preferredXAddressCommunication = CommunicationManagerRegistry.getDefault();
			} else {
				resolveState.pendling = true;

				// Send resolve to discover xAddress(es)
				ResolveMessage resolve = new ResolveMessage();
				resolve.setVersion(ProtocolVersionInfoRegistry.getInstance().get(getEndpointReference()));
				resolve.setEndpointReference(epr);
				OutDispatcher.getInstance().send(resolve, Discovery.getDefaultOutputDomains(), handler);

				try {
					this.wait();
				} catch (InterruptedException e) {
					Log.info(e);
				}

			}
		}

		// now the device should be resolved
		if (preferredXAddress == null) {
			throw new TimeoutException("Device cannot be resolved, probably offline");
		}
	}

	/**
	 * Sends Get message to remote device. GetResponse message will be handled
	 * by callback and this will build a proxy device. Only one Get message will
	 * be sent each time. All users of this method must be synchronized with the
	 * instance of the device reference handler.
	 * 
	 * @return proxy device
	 * @throws TimeoutException
	 */
	private Device getProxyDevice() throws TimeoutException {
		getState.counter++;

		try {
			while (device == null || !device.isValid()) {
				if (getState.pendling) {
					try {
						this.wait();
					} catch (InterruptedException e1) {
						if (Log.isDebug()) 
							Log.debug(e1);
					}
				} else {
					if (preferredXAddress == null) {
						throw new TimeoutException("No xaddress to communicate.");
					}

					switch (handler.communicationState) {
						case CallbackHandler.COM_TIMEOUT:
							throw new TimeoutException("Device state unknown, probably offline");
						case CallbackHandler.COM_ERROR:
							throw new TimeoutException("Device send fault, probably WSDAPI Device");
						case CallbackHandler.COM_ERROR_TRANSMISSION:
							Log.info("DefaultDeviceReference.getProxyDevice: Transmission error with xaddress=" + preferredXAddress);

							URISet xaddresses = discoveryData.getXAddrs();
							xaddresses.remove(preferredXAddress);
							if (discoveryData.getXAddrs().size() == 0) {
								preferredXAddress = null;
								throw new TimeoutException("No xaddress to communicate.");
							}

							preferredXAddress = (URI) xaddresses.iterator().next();
							handler.communicationState = CallbackHandler.COM_UNKNOWN;
							break;
					}

					getState.pendling = true;

					final GetMessage get = new GetMessage();
					get.setVersion(ProtocolVersionInfoRegistry.getInstance().get(getEndpointReference()));
					/*
					 * we should set the wsa:to property to the EPR address
					 * (usually a URN) of this device instead of to an xAddress
					 * of that device
					 */
					get.getHeader().setEndpointReference(discoveryData.getEndpointReference());
					get.setTargetAddress(preferredXAddress);

					OutDispatcher.getInstance().send(get, preferredXAddressCommunication, handler);

					try {
						this.wait();
					} catch (InterruptedException e) {
						if (Log.isDebug()) 
							Log.debug(e);
					}
				}
			}
		} finally {
			getState.counter--;
			if (getState.counter == 0) {
				getState.pendling = false;
				if (handler.communicationState != CallbackHandler.COM_OK) {
					// now we must reset the communication state
					handler.communicationState = CallbackHandler.COM_UNKNOWN;
				}
			}
		}

		return device;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.reference.Reference#getLocation()
	 */
	@Override
	public int getLocation() {
		return location;
	}

	// --------------------- DISCOVERY DATA -----------------------

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceReference#getDevicePortTypes(boolean
	 * )
	 */
	@Override
	public synchronized Iterator getDevicePortTypes(boolean doDiscovery) throws TimeoutException {
		if (discoveryData == null) {
			Log.warn("DefaultDeviceReference.getDevicePortTypes: No discovery data");
			return EmptyStructures.EMPTY_ITERATOR;
		}

		if (doDiscovery) {
			if ((discoveryData.getTypes() == null || discoveryData.getTypes().size() == 0) && preferredXAddress == null) {
				resolveRemoteDevice();
			}
		}

		QNameSet types = discoveryData.getTypes();
		return types == null ? EmptyStructures.EMPTY_ITERATOR : new ReadOnlyIterator(types.iterator());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceReference#getDevicePortTypesAsArray
	 * (boolean)
	 */
	@Override
	public synchronized QName[] getDevicePortTypesAsArray(boolean doDiscovery) throws TimeoutException {
		if (discoveryData == null) {
			Log.warn("DefaultDeviceReference.getDevicePortTypes: No discovery data");
			return (QName[]) EmptyStructures.EMPTY_OBJECT_ARRAY;
		}

		if (doDiscovery) {
			if ((discoveryData.getTypes() == null || discoveryData.getTypes().size() == 0) && preferredXAddress == null) {
				resolveRemoteDevice();
			}
		}

		QNameSet types = discoveryData.getTypes();
		return types == null ? (QName[]) EmptyStructures.EMPTY_OBJECT_ARRAY : types.toArray();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.reference.DeviceReference#getScopes(boolean)
	 */
	@Override
	public synchronized Iterator getScopes(boolean doDiscovery) throws TimeoutException {
		if (discoveryData == null) {
			Log.warn("DefaultDeviceReference.getScopes: No discovery data");
			return EmptyStructures.EMPTY_ITERATOR;
		}

		if (doDiscovery) {
			if ((discoveryData.getScopes() == null || discoveryData.getScopes().size() == 0) && preferredXAddress == null) {
				resolveRemoteDevice();
			}
		}

		ScopeSet scopes = discoveryData.getScopes();
		URISet uriScopes = scopes == null ? null : scopes.getScopesAsUris();
		return uriScopes == null ? EmptyStructures.EMPTY_ITERATOR : new ReadOnlyIterator(uriScopes.iterator());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceReference#getScopesAsArray(boolean)
	 */
	@Override
	public synchronized URI[] getScopesAsArray(boolean doDiscovery) throws TimeoutException {
		if (discoveryData == null) {
			Log.warn("DefaultDeviceReference.getScopes: No discovery data");
			return (URI[]) EmptyStructures.EMPTY_OBJECT_ARRAY;
		}

		if (doDiscovery) {
			if ((discoveryData.getScopes() == null || discoveryData.getScopes().size() == 0) && preferredXAddress == null) {
				resolveRemoteDevice();
			}
		}

		ScopeSet scopes = discoveryData.getScopes();
		URISet uriScopes = (scopes == null ? null : scopes.getScopesAsUris());
		//Bugfix SSch: Cast to URI[] results in a class cast exception
		return (uriScopes!=null)?uriScopes.toArray():null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceReference#getMetadataVersion(boolean
	 * )
	 */
	@Override
	public synchronized long getMetadataVersion(boolean doDiscovery) throws TimeoutException {
		if (discoveryData == null) {
			Log.warn("DefaultDeviceReference.getMetadataVersion: No discovery data");
			return DiscoveryData.UNKNOWN_METADATA_VERSION;
		}

		if (doDiscovery) {
			if (discoveryData.getMetadataVersion() == DiscoveryData.UNKNOWN_METADATA_VERSION && preferredXAddress == null) {
				resolveRemoteDevice();
			}
		}

		return discoveryData.getMetadataVersion();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceReference#getXAddresses(boolean)
	 */
	@Override
	public synchronized Iterator getXAddresses(boolean doDiscovery) throws TimeoutException {
		if (discoveryData == null) {
			Log.warn("DefaultDeviceReference.getXAddresses: No discovery data");
			return EmptyStructures.EMPTY_ITERATOR;
		}

		if (doDiscovery) {
			if ((discoveryData.getXAddrs() == null || discoveryData.getXAddrs().size() == 0) && preferredXAddress == null) {
				resolveRemoteDevice();
			}
		}

		URISet xAddrs = discoveryData.getXAddrs();
		return xAddrs == null ? EmptyStructures.EMPTY_ITERATOR : new ReadOnlyIterator(xAddrs.iterator());
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.reference.Reference#getPreferredXAddress()
	 */
	@Override
	public URI getPreferredXAddress() {
		return preferredXAddress;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.Reference#getPreferredCommunicationManagerID()
	 */
	@Override
	public CommunicationManagerID getPreferredCommunicationManagerID() {
		return preferredXAddressCommunication;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceReference#getEndpointReference()
	 */
	@Override
	public EndpointReference getEndpointReference() {
		if (discoveryData == null) {
			return null;
		}

		return discoveryData.getEndpointReference();
	}

	// -----------------------------------------------------------------

	/**
	 * Checks if the specified application sequence is newer than the latest. If
	 * the specified sequence is newer, the latest sequence replaced by the
	 * newer and <code>true</code> will be returned. If new sequence is
	 * <code>null</code>, method returns <code>true</code>;
	 * 
	 * @param newSequence
	 * @return <code>true</code>, if the specified sequence is newer than the
	 *         latest sequence or if new sequence is null.
	 */
	protected synchronized boolean checkAppSequence(AppSequence appSeq) {
		if (location == LOCATION_LOCAL) {
			Log.error("DefaultDeviceReference.checkAppSequence is not available for local devices.");
			throw new RuntimeException("checkAppSequence is not available for local devices!");
		}
		
		return appSequenceTracker.checkAndUpdate(appSeq);
	}

	protected synchronized int changeProxyReferenceState(int event) {
		return proxyReferenceState.transit(event);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.management.TimedEntry#timedOut()
	 */
	@Override
	protected void timedOut() {
		// Reference has no listeners, WatchDog timed out, delete this.
		listeners.sharedLock();
		try {
			if (listeners.size() == 0 && location != LOCATION_LOCAL) {
				// nobody needs this reference => remove it from registry
				if (discoveryData != null && discoveryData.getEndpointReference() != null) {
					DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().unregisterDeviceReference(this);
				}
			}
			// else {
			// Log.error("Temporary DeviceReference timed out! " +
			// "Listener counter should be 0, was " + listeners.size());
			// }
		} finally {
			listeners.releaseSharedLock();
		}
	}

	// ------------------ LISTENERS--------------------------

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceReference#addListener(org.ws4d.
	 * java.service.reference.DeviceListener)
	 */
	@Override
	public void addListener(DeviceListener listener) {
		if (listener == null) {
			return;
		}
		listeners.exclusiveLock();
		try {
			if (listeners.size() == 0 && location != LOCATION_LOCAL) {
				// only remote devices may have been registered
				WatchDog.getInstance().unregister(this);
			}
			listeners.add(listener);
		} finally {
			listeners.releaseExclusiveLock();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceReference#removeListener(org.ws4d
	 * .java.service.reference.DeviceListener)
	 */
	@Override
	public void removeListener(DeviceListener listener) {
		if (listener == null) {
			return;
		}
		listeners.exclusiveLock();
		try {
			listeners.remove(listener);
			if (listeners.size() == 0 && location != LOCATION_LOCAL) {
				// only remote device will be removed
				WatchDog.getInstance().register(this, DispatchingProperties.getInstance().getReferenceCachingTime());
			}
		} finally {
			listeners.releaseExclusiveLock();
		}
	}

	/**
	 * Returns amount of listeners for this reference.
	 * 
	 * @return Amount of listeners for this reference.
	 */
	protected int sizeOfListeners() {
		return listeners.size();
	}

	/**
	 * Get copy of listeners.
	 * 
	 * @return Data structure of listeners.
	 */
	private DataStructure getListeners() {
		if (listeners.size() == 0) {
			return EmptyStructures.EMPTY_STRUCTURE;
		}

		listeners.sharedLock();
		try {
			ArrayList clonedListeners = new ArrayList(listeners);
			return clonedListeners;
		} finally {
			listeners.releaseSharedLock();
		}
	}

	/**
	 * Set the preferredProtocolVersion to the DPWSVersion of the DPWSProperties
	 * if just one is defined, else it will set to "null" for both.
	 */
	private void setPreferredDPWSVersion() {
		int size=ProtocolVersionInfoRegistry.getInstance().getDefaultSupportedProtocolVersionInfoSize();
		if (size==1)
		{
			Iterator it = ProtocolVersionInfoRegistry.getInstance().getDefaultSupportedProtocolVersionInfoIterator();
			ProtocolVersionInfoRegistry.getInstance().add(getEndpointReference(), (ProtocolVersionInfo) it.next());
		}
		//Changed SSch to avoid dependency to DPWSProperties
//		if (supportedDPWSVersions.size() <= 1) {
//			Iterator it = supportedDPWSVersions.iterator();
//			ProtocolVersionInfoRegistry.add(getEndpointReference(), (DPWSProtocolVersionInfo) it.next());
//		}
	}

	/**
	 * @param preferredXAddress the preferredXAddress to set
	 * @param preferredXAddressCommunication the protocol ID the new preferred
	 *            XAddress belongs to
	 */
	// void setPreferredXAddress(URI preferredXAddress, CommunicationID
	// preferredXAddressCommunication) {
	void setPreferredXAddress(DiscoveryData data, ProtocolData protocolData) {
		/*
		 * store preferredXAddress for target device within the device reference
		 * (handler)
		 */
		URI preferredXAddress = null;
		URISet xAddresses = data.getXAddrs();

		if (xAddresses == null || xAddresses.size() == 0) {
			// nothing to do here
		} else if (xAddresses.size() == 1) {
			preferredXAddress = (URI) xAddresses.iterator().next();
		} else {
			for (Iterator it = xAddresses.iterator(); it.hasNext();) {
				URI xAddr = (URI) it.next();
				if (protocolData.sourceMatches(xAddr)) {
					preferredXAddress = xAddr;
					break;
				}
			}
			if (preferredXAddress == null) {
				// take the first (or any other) one
				preferredXAddress = (URI) xAddresses.iterator().next();
			}
		}

		this.preferredXAddress = preferredXAddress;
		this.preferredXAddressCommunication = protocolData.getCommunicationManagerId();
	}

	@Override
	public boolean isDeviceObjectExisting() {
		return device != null;
	}

	@Override
	public boolean isDiscovered() {
		if (location == LOCATION_LOCAL) {
			return true;
		}

		return discoveryData.getMetadataVersion() == DiscoveryData.UNKNOWN_METADATA_VERSION ? false : true;
	}

	@Override
	public boolean isCompleteDiscovered() {
		return completeDiscovered;
	}

	private void removeDeviceServiceAssociation() {
		if (location != LOCATION_LOCAL && device != null) {
			for (Iterator it = device.getServiceReferences(); it.hasNext();) {
				ServiceReferenceInternal servRef = (ServiceReferenceInternal) it.next();
				servRef.setParentDeviceReference(null);
				if (servRef.getLocation() != LOCATION_LOCAL) {
					DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().addServiceReferenceToCache(servRef);
					if (servRef.isBuildUp()) {
						try {
							((ProxyService) servRef.getService()).setParentDeviceReference(null);
						} catch (TimeoutException e) {
							Log.info(e);
						}
					}
				}
			}
		}
	}

	private void disposeServiceReferences() {
		if (location != LOCATION_LOCAL && device != null) {
			for (Iterator it = device.getServiceReferences(); it.hasNext();) {
				ServiceReferenceInternal servRef = (ServiceReferenceInternal) it.next();
				servRef.setParentDeviceReference(null);
				if (servRef.getLocation() != LOCATION_LOCAL) {
					if (servRef.isBuildUp()) {
						try {
							((ProxyService) servRef.getService()).setParentDeviceReference(null);
						} catch (TimeoutException e) {
							Log.info(e);
						}
					}
					DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().unregisterServiceReference(servRef);
				}
			}
		}
	}

	/**
	 * Updates discovery data of device reference (hence the discovery data of
	 * the proxy device).
	 * 
	 * @param data new discovery data to check old data against.
	 * @return true if endpoint reference was set.
	 */
	private boolean updateDiscoveryData(HostMData host) {
		if (location == LOCATION_LOCAL) {
			throw new RuntimeException("Updating Discovery Data for a local device is prohibited outside of the device");
		}

		if (host == null) {
			return false;
		}

		if (discoveryData == null) {
			discoveryData = new DiscoveryData();
			discoveryData.setEndpointReference(host.getEndpointReference());
			discoveryData.setTypes(host.getTypes());
			return true;
		} else {
			discoveryData.getTypes().addAll(host.getTypes());
			if (discoveryData.getEndpointReference() == null) {
				discoveryData.setEndpointReference(host.getEndpointReference());
				return true;
			}
		}

		return false;
	}

	/**
	 * Updates discovery data. Only used for references to remote devices.
	 * Returns <code>true</code>, if metadata version of the new discovery data
	 * is newer. Preferred xaddress will be set if unset or metadata version is
	 * newer. Changes proxy reference state.
	 * 
	 * @param newData
	 * @param protocolData
	 * @return <code>true</code>, if metadata version of the new discovery data
	 *         is newer.
	 */
	boolean updateDiscoveryData(DiscoveryData newData, ProtocolData protocolData) {
		boolean updated = false;
		handler.communicationState = CallbackHandler.COM_OK;

		if (discoveryData == null) {
			discoveryData = newData;
			updated = true;
		} else {
			updated = discoveryData.update(newData);
		}

		if (updated || preferredXAddress == null) {
			setPreferredXAddress(newData, protocolData);
		}

		if (updated) {
			/*
			 * We do not remove the service from framework, we only remove
			 * association to the device.
			 */
			changeProxyReferenceState(EVENT_DEVICE_CHANGED);
		} else {
			changeProxyReferenceState(EVENT_DEVICE_SEEN);
		}

		return updated;
	}

	/**
	 * Clones device listeners and informs everyone in separate thread about
	 * change.
	 */
	@Override
	public void announceDeviceRunning() {
		for (Iterator it = getListeners().iterator(); it.hasNext();) {
			final DeviceListener listener = (DeviceListener) it.next();
			PlatformSupport.getInstance().getToolkit().getThreadPool().execute(new Runnable() {

				/*
				 * (non-Javadoc)
				 * @see java.lang.Runnable#run()
				 */
				@Override
				public void run() {
					listener.deviceRunning(DefaultDeviceReference.this);
				}

			});
		}
	}

	/**
	 * Clones device listeners and informs everyone in separate thread about
	 * change.
	 */
	@Override
	public void announceDeviceBuildUp() {
		for (Iterator it = getListeners().iterator(); it.hasNext();) {
			final DeviceListener listener = (DeviceListener) it.next();
			PlatformSupport.getInstance().getToolkit().getThreadPool().execute(new Runnable() {

				/*
				 * (non-Javadoc)
				 * @see java.lang.Runnable#run()
				 */
				@Override
				public void run() {
					listener.deviceBuildUp(DefaultDeviceReference.this);
				}

			});
		}
	}

	/**
	 * Clones device listeners and informs everyone in separate thread about
	 * stop.
	 */
	@Override
	public void announceDeviceBye() {
		for (Iterator it = getListeners().iterator(); it.hasNext();) {
			final DeviceListener listener = (DeviceListener) it.next();
			PlatformSupport.getInstance().getToolkit().getThreadPool().execute(new Runnable() {

				@Override
				public void run() {
					listener.deviceBye(DefaultDeviceReference.this);
					// if (remote && disconnected) {
					// listener.onDeviceDisconnected(DefaultDeviceReference.this);
					// }
				}
			});
		}
	}

	/**
	 * Clones device listeners and informs everyone in separate thread about
	 * change.
	 */
	@Override
	public void announceDeviceChanged() 
	{
		if (location == LOCATION_LOCAL && device != null) {
			discoveryData = new DiscoveryData(((LocalDevice) device).getDiscoveryData());
		}

		for (Iterator it = getListeners().iterator(); it.hasNext();) {
			final DeviceListener listener = (DeviceListener) it.next();
			PlatformSupport.getInstance().getToolkit().getThreadPool().execute(new Runnable() {

				@Override
				public void run() {
					listener.deviceChanged(DefaultDeviceReference.this);
				}

			});
		}
	}

	/**
	 * Clones device listeners and informs everyone in separate thread about
	 * change.
	 */
	@Override
	public void announceDeviceCommunicationErrorOrReset() {
		for (Iterator it = listeners.iterator(); it.hasNext();) {
			final DeviceListener listener = (DeviceListener) it.next();
			PlatformSupport.getInstance().getToolkit().getThreadPool().execute(new Runnable() {

				@Override
				public void run() {
					listener.deviceCommunicationErrorOrReset(DefaultDeviceReference.this);
				}
			});
		}
	}

	/**
	 * Clones device listeners and informs everyone in separate thread about
	 * change.
	 */
	@Override
	public void announceDeviceChangedAndBuildUp() {
		for (Iterator it = getListeners().iterator(); it.hasNext();) {
			final DeviceListener listener = (DeviceListener) it.next();
			PlatformSupport.getInstance().getToolkit().getThreadPool().execute(new Runnable() {

				/*
				 * (non-Javadoc)
				 * @see java.lang.Runnable#run()
				 */
				@Override
				public void run() {
					listener.deviceChanged(DefaultDeviceReference.this);
					listener.deviceBuildUp(DefaultDeviceReference.this);
				}

			});
		}
	}

	/**
	 * Clones device listeners and informs everyone in separate thread about
	 * change.
	 */
	@Override
	public void announceDeviceRunningAndBuildUp() {
		for (Iterator it = getListeners().iterator(); it.hasNext();) {
			final DeviceListener listener = (DeviceListener) it.next();
			PlatformSupport.getInstance().getToolkit().getThreadPool().execute(new Runnable() {

				/*
				 * (non-Javadoc)
				 * @see java.lang.Runnable#run()
				 */
				@Override
				public void run() {
					listener.deviceRunning(DefaultDeviceReference.this);
					listener.deviceBuildUp(DefaultDeviceReference.this);
				}

			});
		}
	}

	private class CallbackHandler extends DefaultResponseCallback {

		/*
		 * Communication states
		 */

		private static final int	COM_OK					= 0;

		private static final int	COM_UNKNOWN				= 1;

		private static final int	COM_TIMEOUT				= 2;

		private static final int	COM_ERROR_TRANSMISSION	= 3;

		private static final int	COM_ERROR				= 4;

		private int					communicationState		= COM_OK;

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultResponseCallback#handle(org.ws4d
		 * .java.communication.message.Message,
		 * org.ws4d.java.message.discovery.ProbeMatchesMessage,
		 * org.ws4d.java.communication.ProtocolData)
		 */
		@Override
		public void handle(Message request, ProbeMatchesMessage response, ProtocolData protocolData) {
			/*
			 * handles directed Probe
			 */
			location = LOCATION_REMOTE;
			// set the preferred
			ProtocolVersionInfoRegistry.getInstance().add(getEndpointReference(), response.getVersion());

			if (checkAppSequence(response.getAppSequence())) {
				setSecureDevice(response.getHeader().getSignature() != null);

				DataStructure data = response.getProbeMatches();
				for (Iterator it = data.iterator(); it.hasNext();) {
					ProbeMatch match = (ProbeMatch) it.next();

					if (discoveryData.getEndpointReference().equals(match.getEndpointReference())) {
						updateDiscoveryData(match, protocolData);
						completeDiscovered = true;
						break;
					}
				}
			}
			else if (Log.isDebug()) {
				Log.debug("DefaultDeviceReference.handle: old AppSequence in ProbeMatches message (msgId = " + response.getMessageId() +")");
			}

			// Device will be generated by GetResponse message, notify waiting
			// threads.
			synchronized (DefaultDeviceReference.this) {
				communicationState = COM_OK;

				directedProbeState.pendling = false;
				DefaultDeviceReference.this.notifyAll();
			}
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultResponseCallback#handle(org.ws4d
		 * .java.communication.message.Message,
		 * org.ws4d.java.message.discovery.ResolveMatchesMessage,
		 * org.ws4d.java.communication.ProtocolData)
		 */
		@Override
		public void handle(Message request, ResolveMatchesMessage response, ProtocolData protocolData) {

			// XXX Discovery Proxy handling not implemented
			location = LOCATION_REMOTE;
			ProtocolVersionInfoRegistry.getInstance().add(getEndpointReference(), response.getVersion());
			
			if (checkAppSequence(response.getAppSequence())) {
				setSecureDevice(response.getHeader().getSignature() != null);
				
				DiscoveryData newDiscoData = response.getResolveMatch();
				updateDiscoveryData(newDiscoData, protocolData);
			}
			else if (Log.isDebug()) {
				Log.debug("DefaultDeviceReference.handle: old AppSequence in ResolveMatches message (msgId = " + response.getMessageId() +")");
			}

			// Device will be generated by GetResponse message, notify waiting
			// threads.
			synchronized (DefaultDeviceReference.this) {
				communicationState = COM_OK;

				resolveState.pendling = false;
				DefaultDeviceReference.this.notifyAll();
			}
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultResponseCallback#handle(org.ws4d
		 * .java.communication.message.Message, org.ws4d.java.message
		 * .metadataexchange.GetResponseMessage,
		 * org.ws4d.java.communication.ProtocolData)
		 */
		@Override
		public void handle(Message request, GetResponseMessage response, ProtocolData protocolData) {
			// Only message from remote devices are handled
			if (location == LOCATION_LOCAL) {
				// may occur if location of dev ref was unknown before
				return;
			}
			location = LOCATION_REMOTE;
			ProtocolVersionInfoRegistry.getInstance().add(getEndpointReference(), response.getVersion());

			boolean updated = updateDiscoveryData(response.getHost());
			if (updated) {
				// Register device reference if not known before.
				ProtocolVersionInfoRegistry.getInstance().add(response.getHost().getEndpointReference(), response.getVersion());
				DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().registerDeviceReference(DefaultDeviceReference.this);
			}

			boolean doChangeState = false;
			if (device == null) {
				device = new ProxyDevice(response, DefaultDeviceReference.this);
				doChangeState = true;
			} else if (!device.isValid()) {
				// device has been changed
				removeDeviceServiceAssociation();
				// update device (add new service references)
				((ProxyDevice) device).update(response, DefaultDeviceReference.this);
				// device = new ProxyDevice(response,
				// DefaultDeviceReference.this);
				doChangeState = true;
			}
			// parent device may have been removed while updating the device
			for (Iterator it = device.getServiceReferences(); it.hasNext();) {
				ServiceReferenceInternal servRef = (ServiceReferenceInternal) it.next();
				servRef.setParentDeviceReference(DefaultDeviceReference.this);

				if (servRef.isBuildUp()) {
					try {
						((ProxyService) servRef.getService()).setParentDeviceReference(DefaultDeviceReference.this);
					} catch (TimeoutException e) {
						e.printStackTrace();
					}
				}
			}

			if (response.getHosted() != null) {
				for (Iterator it = response.getHosted().iterator(); it.hasNext();) {
					HostedMData hosted = (HostedMData) it.next();
					ServiceReferenceInternal servRef = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().updateStaticServiceReference(hosted);
					servRef.update(hosted, protocolData);

					// Put each EPR from each Hosted Block into the Registry
					for (Iterator it_epr = hosted.getEndpointReferences().iterator(); it_epr.hasNext();) {
						EndpointReference epr = (EndpointReference) it_epr.next();
						ProtocolVersionInfoRegistry.getInstance().add(epr, response.getVersion());
					}
					// servRef.setPreferredXAddress( msg,
					// hosted.getEndpointReferences() );
					// try {
					// servRef.update(hosted);
					// } catch (FrameworkException e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// }
				}
			}

			synchronized (DefaultDeviceReference.this) {
				communicationState = COM_OK;

				getState.pendling = false;
				DefaultDeviceReference.this.notifyAll();
			}

			if (doChangeState) {
				changeProxyReferenceState(EVENT_DEVICE_GET_RSP);
			}
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultResponseCallback#handle(org.ws4d
		 * .java.communication.message.Message,
		 * org.ws4d.java.message.FaultMessage,
		 * org.ws4d.java.communication.ProtocolData)
		 */
		@Override
		public void handle(Message request, FaultMessage fault, ProtocolData protocolData) {
			// timeout of messages send
			ProtocolVersionInfoRegistry.getInstance().add(getEndpointReference(), fault.getVersion());
			synchronized (DefaultDeviceReference.this) {
				communicationState = COM_ERROR;
				changeProxyReferenceState(EVENT_DEVICE_FAULT_RESET);

				resolveState.pendling = false;
				directedProbeState.pendling = false;
				getState.pendling = false;
				DefaultDeviceReference.this.notifyAll();
			}

			if (Log.isDebug()) {
				URI msgId = fault.getRelatesTo();
				Log.debug("DefaultDeviceReference.CallbackHandler.receivedFault: get, msgId = " + msgId);
			}
		}

		/*
		 * (non-Javadoc)
		 * @seeorg.ws4d.java.communication.DefaultResponseCallback#
		 * handleMalformedResponseException (org.ws4d.java.message.Message,
		 * java.lang.Exception)
		 */
		@Override
		public void handleMalformedResponseException(Message request, Exception exception) {
			// same as for timeouts
			handleTimeout(request);
		}

		/*
		 * (non-Javadoc)
		 * @seeorg.ws4d.java.communication.DefaultResponseCallback#
		 * handleTransmissionException (org.ws4d.java.message.Message,
		 * java.lang.Exception)
		 */
		@Override
		public void handleTransmissionException(Message request, Exception exception) {

			synchronized (DefaultDeviceReference.this) {
				communicationState = COM_ERROR_TRANSMISSION;

				resolveState.pendling = false;
				directedProbeState.pendling = false;
				getState.pendling = false;
				DefaultDeviceReference.this.notifyAll();
			}
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.dispatch.ResponseCallback#receivedTimeout(org.ws4d.java
		 * .data.uri.URI)
		 */
		@Override
		public void handleTimeout(Message request) {
			// timeout of messages send

			synchronized (DefaultDeviceReference.this) {
				communicationState = COM_TIMEOUT;
				changeProxyReferenceState(EVENT_DEVICE_FAULT_RESET);

				resolveState.pendling = false;
				directedProbeState.pendling = false;
				getState.pendling = false;
				DefaultDeviceReference.this.notifyAll();
			}

		}
	}

	private class StateManager {

		private int	state	= STATE_UNKNOWN;

		private void setState(int state) {
			this.state = state;
		}

		private int getState() {
			return state;
		}

		private int transit(int event) {
			if (location == LOCATION_LOCAL) {
				throw new RuntimeException("Use of StateManager is dedicated to proxy devices!");
			}

			/*
			 * change state and announce state change
			 */
			switch (getState()) {
				case STATE_UNKNOWN:
					changeUnknownState(event);
					break;

				case STATE_RUNNING:
					changeRunningState(event);
					break;

				case STATE_BUILD_UP:
					changeBuildUpState(event);
					break;

				case STATE_STOPPED:
					changeStoppedState(event);
					break;
			}

			switch (event) {
				case EVENT_DEVICE_CHANGED:
					completeDiscovered = false;
					Device tempDevice=device;
					if (tempDevice!=null)
						tempDevice.invalidate();
//					removeDeviceServiceAssociation();
//					device = null;
					break;
			}

			return getState();
		}

		private void changeUnknownState(int event) {
			switch (event) {
				case EVENT_DEVICE_BYE:
					setState(STATE_STOPPED);
					announceDeviceBye();
					break;

				case EVENT_DEVICE_CHANGED:
					setState(STATE_RUNNING);
					announceDeviceRunning();
					break;

				case EVENT_DEVICE_GET_RSP:
					setState(STATE_BUILD_UP);
					announceDeviceBuildUp();
					break;

				case EVENT_DEVICE_SEEN:
					setState(STATE_RUNNING);
					announceDeviceRunning();
					break;
			}
		}

		private void changeRunningState(int event) {
			switch (event) {
				case EVENT_DEVICE_BYE:
					setState(STATE_STOPPED);
					announceDeviceBye();
					break;

				case EVENT_DEVICE_CHANGED:
					// state: running => running
					announceDeviceChanged();
					break;

				case EVENT_DEVICE_GET_RSP:
					setState(STATE_BUILD_UP);
					announceDeviceBuildUp();
					break;

				case EVENT_DEVICE_FAULT_RESET:
					setState(STATE_UNKNOWN);
					announceDeviceCommunicationErrorOrReset();
					break;
			}
		}

		private void changeBuildUpState(int event) {
			switch (event) {
				case EVENT_DEVICE_BYE:
					setState(STATE_STOPPED);
					announceDeviceBye();
					break;

				case EVENT_DEVICE_CHANGED:
					setState(STATE_RUNNING);
					announceDeviceChanged();
					break;

				case EVENT_DEVICE_FAULT_RESET:
					setState(STATE_UNKNOWN);
					announceDeviceCommunicationErrorOrReset();
					break;
			}
		}

		private void changeStoppedState(int event) {
			switch (event) {
				case EVENT_DEVICE_CHANGED:
					setState(STATE_RUNNING);
					announceDeviceChanged();
					break;

				case EVENT_DEVICE_GET_RSP:
					setState(STATE_BUILD_UP);
					announceDeviceBuildUp();
					break;

				case EVENT_DEVICE_SEEN:
					/*
					 * case: device has send bye, framework didn't receive a new
					 * hello and somebody sends get.
					 */
					if (device != null) {
						setState(STATE_BUILD_UP);
						announceDeviceBuildUp();
					} else {
						setState(STATE_RUNNING);
						announceDeviceRunning();
					}
					break;
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.reference.DeviceReference#isSecureDevice()
	 */
	@Override
	public boolean isSecureDevice() {
		return isSecure;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceReference#setSecureDevice(boolean)
	 */
	@Override
	public void setSecureDevice(boolean sec) {
		this.isSecure = sec;
	}

	private class MessageState {

		private boolean	pendling	= false;

		private int		counter		= 0;

	}

}

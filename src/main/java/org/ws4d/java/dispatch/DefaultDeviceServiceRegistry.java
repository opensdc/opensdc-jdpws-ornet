/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.dispatch;

import java.io.IOException;

import org.ws4d.java.communication.CommunicationBinding;
import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.DefaultIncomingMessageListener;
import org.ws4d.java.communication.IncomingMessageListener;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.concurrency.LockSupport;
import org.ws4d.java.configuration.DispatchingProperties;
import org.ws4d.java.constants.DPWSMessageConstants;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.discovery.ByeMessage;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.LocalDevice;
import org.ws4d.java.service.LocalService;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.Reference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.service.reference.ServiceReferenceInternal;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.LinkedSet;
import org.ws4d.java.structures.List;
import org.ws4d.java.types.DiscoveryData;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.EndpointReferenceSet;
import org.ws4d.java.types.HostedMData;
import org.ws4d.java.types.ProbeScopeSet;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.util.Log;


/**
 * Registry class which manages deviceInvokers, serviceInvokers and their
 * references. Many methods of this class register service and service reference
 * listeners.
 * 
 * @author mspies
 */
public class DefaultDeviceServiceRegistry implements IDeviceServiceRegistry{

	private static final int[]						DEVICE_LIFECYCLE_MESSAGE_TYPES	= { DPWSMessageConstants.HELLO_MESSAGE, DPWSMessageConstants.BYE_MESSAGE };
	private static final int						MAX_CACHE_SIZE					= DispatchingProperties.getInstance().getServiceReferenceCacheSize();


	private  final IncomingMessageListener	DEVICE_LIFECYCLE_LISTENER		= new IncomingHelloByeListener();
	/*
	 * this map is used also for synchronizing any access to device data
	 * (deviceInvokers, deviceRefs, devRefs etc.)
	 */
	// key = EndpointIdentifier, value = DeviceServiceRegistry instance
	final LockSupport						GLOBAL_LOCK						= new LockSupport();

	// epr -> devRef
	final HashMap							STATIC_DEVICE_REFS				= new HashMap();

	// epr -> servRef
	final HashMap							STATIC_SERVICE_REFS				= new HashMap();

	// default device instances
	private final DataStructure				DEVICES							= new ArrayList();

	// default service instances
	private final DataStructure				SERVICES						= new ArrayList();

	/**
	 * Cache of service refs ordered by access. Used to determine the eldest
	 * service ref. Holds only references, which are not local and not assigned
	 * to a device.
	 */
	private final LinkedSet					SERVICE_REFS_CACHE				= new LinkedSet(MAX_CACHE_SIZE, true);

	// ------------------- CONSTRUCTOR -------------------------


	DefaultDeviceServiceRegistry() {
		super();
	}

	@Override
	public void init() {
		GLOBAL_LOCK.exclusiveLock();
		try {
			for (Iterator it = CommunicationManagerRegistry.getLoadedManagers();
			it.hasNext();) {
				CommunicationManager manager = (CommunicationManager) it.next();
				try {
					manager.registerDeviceReference(DEVICE_LIFECYCLE_MESSAGE_TYPES, null,
							DEVICE_LIFECYCLE_LISTENER);
				} catch (IOException e) {
					Log.info(e);
				}
			}
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	@Override
	public void tearDown() {
		GLOBAL_LOCK.exclusiveLock();
		try {
			// unregister device lifecycle listener
			for (Iterator it = CommunicationManagerRegistry.getLoadedManagers(); it.hasNext();) {
				CommunicationManager manager = (CommunicationManager) it.next();
				try {
					manager.unregisterDeviceReference(DEVICE_LIFECYCLE_MESSAGE_TYPES, null, DEVICE_LIFECYCLE_LISTENER);
				} catch (IOException e) {
					Log.info(e);
				}
			}
			// copy deviceInvokers in order to avoid concurrent modifications
			List copiedDevices = new ArrayList(DEVICES);
			for (Iterator it = copiedDevices.iterator(); it.hasNext();) {
				LocalDevice device = (LocalDevice) it.next();
				try {
					// this stops also all serviceInvokers of this device
					device.stop();
				} catch (IOException e) {
					Log.info(e);
				}
			}
			// now stop serviceInvokers, which are NOT on top of deviceInvokers

			// copy serviceInvokers in order to avoid concurrent modifications
			List copiedServices = new ArrayList(SERVICES);
			for (Iterator it = copiedServices.iterator(); it.hasNext();) {
				LocalService service = (LocalService) it.next();
				try {
					service.stop();
				} catch (IOException e) {
					Log.info(e);
				}
			}
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	/**
	 * Get device reference of a remote device. If no device reference
	 * registered for this device, a new one will be created.
	 * 
	 * @param helloData
	 * @return
	 */
	@Override
	public DeviceReference getStaticDeviceReference(HelloData helloData) {
		if (helloData == null || helloData.getDiscoveryData() == null) {
			return null;
		}
		GLOBAL_LOCK.sharedLock();
		try {
			Object o = STATIC_DEVICE_REFS.get(helloData.getEndpointReference());
			if (o != null) {
				return (DefaultDeviceReference) o;
			}

			GLOBAL_LOCK.exclusiveLock();
			try {
				o = STATIC_DEVICE_REFS.get(helloData.getEndpointReference());
				if (o != null) {
					return (DefaultDeviceReference) o;
				}

				// no device reference available, create one
				DefaultDeviceReference devRef = new DefaultDeviceReference(helloData.getAppSequence(), helloData.getDiscoveryData(), helloData.getProtocolData());
				ProtocolVersionInfoRegistry.getInstance().add(helloData.getEndpointReference(), helloData.getVersion());
				STATIC_DEVICE_REFS.put(helloData.getEndpointReference(), devRef);
				return devRef;
			} finally {
				GLOBAL_LOCK.releaseExclusiveLock();
			}

		} finally {
			GLOBAL_LOCK.releaseSharedLock();
		}
	}

	/**
	 * Returns the device reference to the specified endpoint reference.
	 * 
	 * @param endpointReference Endpoint reference of the device being looked
	 *            for.
	 * @param doCreate If <code>true</code>, reference will be created if not
	 *            already existing.
	 * @return Device reference being looked for.
	 */
	@Override
	public DeviceReference getStaticDeviceReference(EndpointReference epr, boolean doCreate) {
		if (epr == null) {
			return null;
		}
		GLOBAL_LOCK.sharedLock();
		try {
			Object o = STATIC_DEVICE_REFS.get(epr);
			if (o != null || !doCreate) {
				return (DefaultDeviceReference) o;
			}

			GLOBAL_LOCK.exclusiveLock();
			try {
				o = STATIC_DEVICE_REFS.get(epr);
				if (o != null) {
					return (DefaultDeviceReference) o;
				}

				// no device reference available, create one
				DefaultDeviceReference devRef = new DefaultDeviceReference(epr);
				STATIC_DEVICE_REFS.put(epr, devRef);
				return devRef;
			} finally {
				GLOBAL_LOCK.releaseExclusiveLock();
			}

		} finally {
			GLOBAL_LOCK.releaseSharedLock();
		}
	}

	/**
	 * Get device reference of a device location is unknown. If no device
	 * reference registered for this device, a new will be created.
	 * 
	 * @param endpointReference
	 * @return
	 */
	@Override
	public DeviceReference getStaticDeviceReference(EndpointReference endpointReference) {
		return getStaticDeviceReference(endpointReference, true);
	}

	/**
	 * Get device reference of a local device. If no device reference registered
	 * for this device, a new will be created.
	 * 
	 * @param device
	 * @return
	 */
	@Override
	public DeviceReference getStaticDeviceReference(LocalDevice device) {
		EndpointReference epr = device.getEndpointReference();
		if (epr == null) {
			return null;
		}

		GLOBAL_LOCK.exclusiveLock();
		try {
			Object o = STATIC_DEVICE_REFS.get(epr);
			if (o != null) {
				DefaultDeviceReference devRef = (DefaultDeviceReference) o;
				/*
				 * if somebody has created a dev ref to an unknown device, and
				 * this local device is now registering.
				 */
				devRef.setLocalDevice(device);
				return devRef;
			}

			// no device reference available, create one
			DefaultDeviceReference devRef = new DefaultDeviceReference(device);
			STATIC_DEVICE_REFS.put(epr, devRef);
			return devRef;
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	@Override
	public ServiceReference getStaticServiceReference(HostedMData hosted) {
		GLOBAL_LOCK.sharedLock();
		try {
			for (Iterator it = hosted.getEndpointReferences().iterator(); it.hasNext();) {
				EndpointReference serviceEpr = (EndpointReference) it.next();
				Object o = STATIC_SERVICE_REFS.get(serviceEpr);
				if (o != null) {
					return (ServiceReference) o;
				}
			}

			GLOBAL_LOCK.exclusiveLock();
			try {
				// must check again
				for (Iterator it = hosted.getEndpointReferences().iterator(); it.hasNext();) {
					EndpointReference serviceEpr = (EndpointReference) it.next();
					Object o = STATIC_SERVICE_REFS.get(serviceEpr);
					if (o != null) {
						return (ServiceReference) o;
					}
				}

				ServiceReference serviceRef = ServiceReferenceFactory.getInstance().newServiceReference(hosted);
				addServiceReferenceToCache(serviceRef);
				for (Iterator it = hosted.getEndpointReferences().iterator(); it.hasNext();) {
					EndpointReference serviceEpr = (EndpointReference) it.next();
					STATIC_SERVICE_REFS.put(serviceEpr, serviceRef);
				}
				return serviceRef;
			} finally {
				GLOBAL_LOCK.releaseExclusiveLock();
			}
		} finally {
			GLOBAL_LOCK.releaseSharedLock();
		}
	}

	@Override
	public ServiceReference getStaticServiceReference(EndpointReference endpointReference) {
		return getStaticServiceReference(endpointReference, true);
	}

	/**
	 * Returns the service reference to the specified endpoint reference.
	 * 
	 * @param endpointReference Endpoint reference of the service being looked
	 *            for.
	 * @param doCreate If <code>true</code>, reference will be created if not
	 *            already existing.
	 * @return Service reference being looked for.
	 */
	/**
	 * @param endpointReference
	 * @return
	 */
	@Override
	public ServiceReference getStaticServiceReference(EndpointReference endpointReference, boolean doCreate) {
		if (endpointReference == null) {
			return null;
		}

		GLOBAL_LOCK.sharedLock();
		try {
			Object o = STATIC_SERVICE_REFS.get(endpointReference);
			if (o != null || !doCreate) {
				return (ServiceReference) STATIC_SERVICE_REFS.get(endpointReference);
			}

			GLOBAL_LOCK.exclusiveLock();
			try {
				o = STATIC_SERVICE_REFS.get(endpointReference);
				if (o != null) {
					return (ServiceReference) o;
				}
				ServiceReference serviceRef = ServiceReferenceFactory.getInstance().newServiceReference(endpointReference);
				addServiceReferenceToCache(serviceRef);
				STATIC_SERVICE_REFS.put(endpointReference, serviceRef);
				return serviceRef;
			} finally {
				GLOBAL_LOCK.releaseExclusiveLock();
			}
		} finally {
			GLOBAL_LOCK.releaseSharedLock();
		}
	}

	/**
	 * Update the registered endpoint addresses of the service reference.
	 * 
	 * @param newHosted
	 * @param servRef
	 * @return
	 */
	@Override
	public ServiceReferenceInternal updateStaticServiceReference(HostedMData newHosted, ServiceReferenceInternal servRef) {
		// ServiceReferenceHandler servRef = null;
		EndpointReferenceSet newEprs = newHosted.getEndpointReferences();

		GLOBAL_LOCK.exclusiveLock();
		try {
			/*
			 * remove all eprs from registry, which are not transmitted
			 */
			for (Iterator it = servRef.getEndpointReferences(); it.hasNext();) {
				EndpointReference epr = (EndpointReference) it.next();
				if (!newEprs.contains(epr)) {
					STATIC_SERVICE_REFS.remove(epr);
				}
			}

			/*
			 * add all transmitted eprs
			 */
			for (Iterator it = newEprs.iterator(); it.hasNext();) {
				EndpointReference serviceEpr = (EndpointReference) it.next();
				STATIC_SERVICE_REFS.put(serviceEpr, servRef);
			}
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}

		return servRef;
	}

	/**
	 * Update the registered endpoint addresses of the service reference or
	 * creates new service reference.
	 * 
	 * @param newHosted
	 * @return
	 */
	@Override
	public ServiceReferenceInternal updateStaticServiceReference(HostedMData newHosted) {
		ServiceReferenceInternal servRef = null;
		EndpointReferenceSet eprs = newHosted.getEndpointReferences();
		DataStructure unknownEprs = new ArrayList(eprs.size());

		GLOBAL_LOCK.exclusiveLock();
		try {
			for (Iterator it = newHosted.getEndpointReferences().iterator(); it.hasNext();) {
				EndpointReference serviceEpr = (EndpointReference) it.next();
				Object o = STATIC_SERVICE_REFS.get(serviceEpr);
				if (o != null) {
					if (servRef == null) {
						servRef = (ServiceReferenceInternal) o;
					}
				} else {
					unknownEprs.add(o);
				}
			}

			if (servRef == null) {
				servRef = ServiceReferenceFactory.getInstance().newServiceReference(newHosted);
				addServiceReferenceToCache(servRef);
				for (Iterator it = newHosted.getEndpointReferences().iterator(); it.hasNext();) {
					EndpointReference serviceEpr = (EndpointReference) it.next();
					STATIC_SERVICE_REFS.put(serviceEpr, servRef);
				}
			} else {
				for (Iterator it = servRef.getEndpointReferences(); it.hasNext();) {
					// remove all eprs from registry, which are not transmitted
					EndpointReference epr = (EndpointReference) it.next();
					if (!eprs.contains(epr)) {
						STATIC_SERVICE_REFS.remove(epr);
					}
				}

				for (Iterator it = unknownEprs.iterator(); it.hasNext();) {
					// add new eprs
					EndpointReference serviceEpr = (EndpointReference) it.next();
					STATIC_SERVICE_REFS.put(serviceEpr, servRef);
				}
			}
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}

		return servRef;
	}

	@Override
	public void removeServiceReferenceFromCache(ServiceReference servRef) {
		synchronized (SERVICE_REFS_CACHE) {
			SERVICE_REFS_CACHE.remove(servRef);
		}
	}

	@Override
	public void updateServiceReferenceCache(ServiceReference servRef) {
		if (servRef != null && SERVICE_REFS_CACHE.contains(servRef)) {
			synchronized (SERVICE_REFS_CACHE) {
				SERVICE_REFS_CACHE.touch(servRef);
			}
		}
	}

	@Override
	public void addServiceReferenceToCache(ServiceReference servRef) {
		if (servRef != null) {
			ServiceReference eldest = null;
			synchronized (SERVICE_REFS_CACHE) {
				if (SERVICE_REFS_CACHE.size() >= MAX_CACHE_SIZE) {
					eldest = (ServiceReference) SERVICE_REFS_CACHE.removeFirst();
				}
				SERVICE_REFS_CACHE.add(servRef);
			}
			if (eldest != null) {
				unregisterServiceReference0(servRef);
			}
		}
	}

	/**
	 * @param deviceTypes
	 * @param scopes
	 * @return
	 */
	@Override
	public DataStructure getLocalDeviceReferences(QNameSet deviceTypes, ProbeScopeSet scopes) {
		GLOBAL_LOCK.sharedLock();
		try {
			return getLocalDeviceReferences0(deviceTypes, scopes);
		} finally {
			GLOBAL_LOCK.releaseSharedLock();
		}
	}

	/**
	 * @param serviceTypes
	 * @param deviceTypes
	 * @param scopes
	 * @return
	 */
	@Override
	public DataStructure getLocalServiceReferences(QNameSet serviceTypes, QNameSet deviceTypes, ProbeScopeSet scopes) {
		GLOBAL_LOCK.sharedLock();
		try {
			DataStructure matchingServiceRefs = new ArrayList();
			if ((deviceTypes != null && deviceTypes.size() > 0) || scopes != null && scopes.size() > 0) {
				DataStructure matchingDeviceRefs = getLocalDeviceReferences0(deviceTypes, scopes);
				for (Iterator it = matchingDeviceRefs.iterator(); it.hasNext();) {
					DeviceReference devRef = (DeviceReference) it.next();
					Device device;
					try {
						device = devRef.getDevice();
					} catch (TimeoutException e) {
						// shouldn't ever get thrown here
						device = null;
					}

					for (Iterator it2 = device.getServiceReferences(); it2.hasNext();) {
						ServiceReference servRef = (ServiceReference) it2.next();
						if (serviceTypes.isContainedBy(servRef.getPortTypes())) {
							matchingServiceRefs.add(servRef);
						}
					}
				}
			} else {
				for (Iterator it = SERVICES.iterator(); it.hasNext();) {
					Service service = (Service) it.next();
					ServiceReference servRef = service.getServiceReference();
					if (serviceTypes.isContainedBy(servRef.getPortTypes())) {
						matchingServiceRefs.add(servRef);
					}
				}
			}
			return matchingServiceRefs;
		} finally {
			GLOBAL_LOCK.releaseSharedLock();
		}
	}

	/**
	 * Registers device reference. Used by device reference handler.
	 * 
	 * @param devRef
	 */
	@Override
	public void registerDeviceReference(DeviceReference devRef) {
		EndpointReference epr = devRef.getEndpointReference();
		GLOBAL_LOCK.exclusiveLock();
		try {
			if (!STATIC_DEVICE_REFS.containsKey(epr)) {
				STATIC_DEVICE_REFS.put(epr, devRef);
			}
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	/**
	 * Removes proxy device reference.
	 * 
	 * @param deviceReference proxy device reference to remove.
	 */
	@Override
	public void unregisterDeviceReference(DeviceReference aDeviceReference) {

		//Bugfix SSch: Has to be Not(instanceof). error during forking of jmeds; orig code was: if (deviceReference == null)
		if (!(aDeviceReference instanceof DefaultDeviceReference)) {
			return;
		}
		DefaultDeviceReference deviceReference=(DefaultDeviceReference) aDeviceReference;
		EndpointReference epr = deviceReference.getEndpointReference();

		GLOBAL_LOCK.exclusiveLock();
		try {
			if (STATIC_DEVICE_REFS.remove(epr) != null) {
				if (!deviceReference.isDeviceObjectExisting()) {
					return;
				}
				// unregister service references
				try {
					Device device = deviceReference.getDevice(false);
					// double-check, as we are not synchronizing on devRef
					if (device == null) {
						return;
					}
					Iterator servRefs = device.getServiceReferences();
					while (servRefs.hasNext()) {
						ServiceReferenceInternal servRef = (ServiceReferenceInternal) servRefs.next();
						servRef.setParentDeviceReference(null);
						//TODO SSch warum dieser Code???
						//						if (servRef.getLocation() != Reference.LOCATION_LOCAL) {
						//							addServiceReferenceToCache(servRef);
						//						}
						unregisterServiceReference(servRef);
					}
				} catch (TimeoutException e) {
					// shouldn't ever happen
					e.printStackTrace();
				}
			}
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	/**
	 * Register service proxy.
	 * 
	 * @param proxy
	 */
	void registerServiceReference(ServiceReference servRef) {
		if (servRef == null) {
			return;
		}
		GLOBAL_LOCK.exclusiveLock();
		try {
			for (Iterator it = servRef.getEndpointReferences(); it.hasNext();) {
				EndpointReference epr = (EndpointReference) it.next();
				if (!STATIC_SERVICE_REFS.containsKey(epr)) {
					STATIC_SERVICE_REFS.put(epr, servRef);
				}
			}
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	/**
	 * Unregister service proxy.
	 * 
	 * @param proxy
	 */
	@Override
	public void unregisterServiceReference(ServiceReference servRef) {
		if (servRef == null) {
			return;
		}

		GLOBAL_LOCK.exclusiveLock();
		try {
			unregisterServiceReference0(servRef);
			removeServiceReferenceFromCache(servRef);
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	private void unregisterServiceReference0(ServiceReference servRef) {
		Iterator eprs = servRef.getEndpointReferences();
		if (!eprs.hasNext()) {
			Log.error("ERROR: DeviceServiceRegistry.unregisterServiceReference0: no epr in service");
			return;
		}

		GLOBAL_LOCK.exclusiveLock();
		try {
			while (eprs.hasNext()) {
				EndpointReference epr = (EndpointReference) eprs.next();
				STATIC_SERVICE_REFS.remove(epr);
			}
			// invalidate service of servRef
			if (servRef.getLocation() != Reference.LOCATION_LOCAL) {
				ServiceReferenceInternal servRefHandler = (ServiceReferenceInternal) servRef;
				// invalidate the service
				servRefHandler.setService(null, null);
			}
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	// ----------------------------------------------------------

	@Override
	public DeviceReference getUpdatedDeviceReference(DiscoveryData newData, Message msg, ProtocolData protocolData) {
		EndpointReference epr = newData.getEndpointReference();

		GLOBAL_LOCK.exclusiveLock();
		try {
			DefaultDeviceReference devRef = (DefaultDeviceReference) STATIC_DEVICE_REFS.get(epr);
			if (devRef != null) {
				if (devRef.getLocation() == Reference.LOCATION_LOCAL || !devRef.checkAppSequence(msg.getAppSequence())) {
					/*
					 * It's our own device or message out of date => nothing to
					 * handle
					 */
					return devRef;
				}

				devRef.updateDiscoveryData(newData, protocolData);
			} else {
				// devRef == null
				devRef = new DefaultDeviceReference(msg.getAppSequence(), newData, protocolData);
				ProtocolVersionInfoRegistry.getInstance().add(devRef.getEndpointReference(), msg.getVersion());
				STATIC_DEVICE_REFS.put(epr, devRef);
			}

			devRef.setSecureDevice(msg.isSecure());

			return devRef;
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}

	}

	/**
	 * @param deviceTypes
	 * @param scopes
	 * @return
	 */
	private DataStructure getLocalDeviceReferences0(QNameSet deviceTypes, ProbeScopeSet scopes) {
		DataStructure matchingDeviceRefs = new HashSet();

		for (Iterator it = DEVICES.iterator(); it.hasNext();) {
			LocalDevice device = (LocalDevice) it.next();
			if (device.deviceMatches(deviceTypes, scopes)) {
				matchingDeviceRefs.add(device.getDeviceReference());
			}
		}

		return matchingDeviceRefs;
	}

	@Override
	public void register(LocalDevice device) {
		GLOBAL_LOCK.exclusiveLock();
		try {
			if (DEVICES.contains(device)) {
				return;
			}
			DEVICES.add(device);
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	@Override
	public void unregister(LocalDevice device) {
		GLOBAL_LOCK.exclusiveLock();
		try {
			DEVICES.remove(device);
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	@Override
	public void register(LocalService service) {
		GLOBAL_LOCK.exclusiveLock();
		try {
			if (SERVICES.contains(service)) {
				return;
			}
			SERVICES.add(service);
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	@Override
	public void unregister(LocalService service) {
		GLOBAL_LOCK.exclusiveLock();
		try {
			SERVICES.remove(service);
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	@Override
	public void register(CommunicationBinding binding) {
		GLOBAL_LOCK.exclusiveLock();
		try {
			for (Iterator it = CommunicationManagerRegistry.getLoadedManagers(); it.hasNext();) {
				CommunicationManager manager = (CommunicationManager) it.next();
				try {
					manager.registerDeviceReference(DEVICE_LIFECYCLE_MESSAGE_TYPES, binding, DEVICE_LIFECYCLE_LISTENER);
				} catch (IOException e) {
					Log.info(e);
				}
			}
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	@Override
	public void unregister(CommunicationBinding binding) {
		GLOBAL_LOCK.exclusiveLock();
		try {
			// unregister device lifecycle listener
			for (Iterator it = CommunicationManagerRegistry.getLoadedManagers(); it.hasNext();) {
				CommunicationManager manager = (CommunicationManager) it.next();
				try {
					manager.unregisterDeviceReference(DEVICE_LIFECYCLE_MESSAGE_TYPES, binding, DEVICE_LIFECYCLE_LISTENER);
				} catch (IOException e) {
					Log.info(e);
				}
			}
		} finally {
			GLOBAL_LOCK.releaseExclusiveLock();
		}
	}

	@Override
	public DataStructure getAllRegisteredDevices()
	{
		GLOBAL_LOCK.sharedLock();
		try{
			DataStructure entries= STATIC_DEVICE_REFS.values();
			ArrayList retList=new ArrayList();
			retList.addAll(entries);
			return retList;
		} finally {
			GLOBAL_LOCK.releaseSharedLock();
		}
	}

	private class IncomingHelloByeListener extends DefaultIncomingMessageListener {

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultIncomingMessageListener#handle
		 * (org.ws4d.java.message.discovery.HelloMessage,
		 * org.ws4d.java.communication.CommunicationID)
		 */
		@Override
		public void handle(HelloMessage hello, ProtocolData protocolData) {
			DiscoveryData newData = hello.getDiscoveryData();
			EndpointReference epr;
			if (newData == null || (epr = newData.getEndpointReference()) == null) {
				return;
			}

			GLOBAL_LOCK.exclusiveLock();
			try {
				DefaultDeviceReference devRef = (DefaultDeviceReference) STATIC_DEVICE_REFS.get(epr);
				if (devRef != null) {
					if (devRef.getLocation() == Reference.LOCATION_LOCAL) {
						/*
						 * It's our own device => nothing to handle
						 */
						return;
					}

					if (devRef.checkAppSequence(hello.getAppSequence())) {
						if (Log.isInfo()) {
							Log.info("Set DPWS Version for " + devRef.getEndpointReference().toString() + " to : " + ProtocolVersionInfoRegistry.getInstance().get(devRef.getEndpointReference()).getDisplayName());
						}
						devRef.setSecureDevice(hello.getHeader().getSignature() != null);
						devRef.updateDiscoveryData(newData, protocolData);						
					}
					else if (Log.isDebug()) {
						Log.debug("DeviceServiceRegistry.handle: old AppSequence in HelloMessage message (msgId = " + hello.getMessageId() +")");
					}

				} else {
					// devRef == null
					if (DispatchingProperties.getInstance().isDeviceReferenceAutoBuild()) {
						/*
						 * Build device reference
						 */
						devRef = new DefaultDeviceReference(hello.getAppSequence(), newData, protocolData);
						ProtocolVersionInfoRegistry.getInstance().add(epr, hello.getVersion());
						if (Log.isInfo()) {
							Log.info("Set DPWS Version for " + devRef.getEndpointReference().toString() + " to : " + ProtocolVersionInfoRegistry.getInstance().get(devRef.getEndpointReference()).getDisplayName());
						}

						devRef.setSecureDevice(hello.getHeader().getSignature() != null);

						STATIC_DEVICE_REFS.put(epr, devRef);
					} else {
						/*
						 * We don't add automatically device references.
						 */
						return;
					}
				}
			} finally {
				GLOBAL_LOCK.releaseExclusiveLock();
			}
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultIncomingMessageListener#handle
		 * (org.ws4d.java.message.discovery.ByeMessage,
		 * org.ws4d.java.communication.CommunicationID)
		 */
		@Override
		public void handle(ByeMessage bye, ProtocolData protocolData) {
			EndpointReference epr;
			if (bye == null || (epr = bye.getEndpointReference()) == null) {
				return;
			}

			GLOBAL_LOCK.sharedLock();
			try {
				Object o = STATIC_DEVICE_REFS.get(epr);
				if (o != null) {
					DefaultDeviceReference devRef = (DefaultDeviceReference) o;

					if (devRef.getLocation() == Reference.LOCATION_LOCAL) {
						/*
						 * local device stops are transmitted by local device
						 */
						return;
					}

					if (devRef.checkAppSequence(bye.getAppSequence())) {
						devRef.changeProxyReferenceState(DefaultDeviceReference.EVENT_DEVICE_BYE);
					}
					else if (Log.isDebug()) {
						Log.debug("DeviceServiceRegistry.handle: old AppSequence in ByeMessage message (msgId = " + bye.getMessageId() +")");
					}
				}

			} finally {
				GLOBAL_LOCK.releaseSharedLock();
			}
		}

	}

}

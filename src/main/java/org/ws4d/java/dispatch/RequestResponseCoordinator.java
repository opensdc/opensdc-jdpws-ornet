/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.dispatch;

import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.ResponseCallback;
import org.ws4d.java.constants.DPWSMessageConstants;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;
import org.ws4d.java.message.eventing.GetStatusResponseMessage;
import org.ws4d.java.message.eventing.RenewResponseMessage;
import org.ws4d.java.message.eventing.SubscribeResponseMessage;
import org.ws4d.java.message.eventing.UnsubscribeResponseMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.message.metadata.GetResponseMessage;
import org.ws4d.java.structures.LockedMap;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.TimedEntry;
import org.ws4d.java.util.WatchDog;

/**
 * Class memorizes outgoing requests and returns callbacks to incoming
 * responses.
 * 
 * @author mspies
 */
public class RequestResponseCoordinator {

	/** message id<URI> -> <TimedEntry> */
	protected final LockedMap					map_msgId_2_entry	= new LockedMap();

	/** singleton of this */
	protected static RequestResponseCoordinator	rrc					= new RequestResponseCoordinator();

	/**
	 * Private Constructor.
	 */
	private RequestResponseCoordinator() {}

	/**
	 * Get singleton instance of request response coordinator.
	 * 
	 * @return request response coordinator.
	 */
	public static RequestResponseCoordinator getInstance() {
		return rrc;
	}

	// ---------------------------------------------------------------------

	/**
	 * Registers callback for an outgoing request message. Timeouts will be
	 * generated by underlying communication layer.
	 * 
	 * @param msg - the request message.
	 * @param callback - callback for responses, faults or timeouts.
	 * @param timeUntilTimeout
	 */
	public void registerResponseCallback(Message msg, ResponseCallback callback, long timeUntilTimeout) {
		TimedResponseCallback timedCallback = new TimedResponseCallback(msg, callback);
		
		AttributedURI key=msg.getMessageId();
		timedCallback.setRelationshipKey(key);
		map_msgId_2_entry.put(key, timedCallback);
		WatchDog.getInstance().register(timedCallback, timeUntilTimeout);
	}

	/**
	 * Unregister callback by message id.
	 * 
	 * @param msgId
	 */
	public void unregisterResponseCallback(URI msgId) {
		// XXX Any problems with different locks (one for both better)?
		TimedResponseCallback timedCallback = (TimedResponseCallback) map_msgId_2_entry.remove(msgId);
		if (timedCallback!=null)
		{
			WatchDog.getInstance().unregister(timedCallback);
		}
	}

	/**
	 * Handle response message. Message will be sent to local callbacks.
	 * 
	 * @param msg
	 * @param protocolData
	 * @return <code>true</code> - if message is exclusively handled by
	 *         RequestResponseCoordinator
	 */
	public boolean handleResponse(Message msg, ProtocolData protocolData) {
		boolean ret = false;

		URI requestMsgId = msg.getRelatesTo();
		if (requestMsgId != null) {
			ret = map_msgId_2_entry.containsKey(requestMsgId);
		}

		if (ret) {
			switch (msg.getType()) {
				// DISCOVERY
				case DPWSMessageConstants.PROBE_MATCHES_MESSAGE: {
					// More than one responses possible.
					ProbeMatchesMessage probeMatches = (ProbeMatchesMessage) msg;

					TimedResponseCallback entry = getCallback(requestMsgId);
					if (entry != null) {
						entry.callback.handle(entry.request, probeMatches, protocolData);
					}
					break;
				}
				case DPWSMessageConstants.RESOLVE_MATCHES_MESSAGE: {
					ResolveMatchesMessage resolveMatches = (ResolveMatchesMessage) msg;

					TimedResponseCallback entry = getAndRemoveCallback(requestMsgId);
					if (entry != null) {
						entry.callback.handle(entry.request, resolveMatches, protocolData);
					}
					break;
				}
					// MEX
				case DPWSMessageConstants.GET_RESPONSE_MESSAGE: {
					GetResponseMessage getRsp = (GetResponseMessage) msg;

					TimedResponseCallback entry = getAndRemoveCallback(requestMsgId);
					if (entry != null) {
						entry.callback.handle(entry.request, getRsp, null);
					}
					break;
				}
				case DPWSMessageConstants.GET_METADATA_RESPONSE_MESSAGE: {
					GetMetadataResponseMessage getMetadataRsp = (GetMetadataResponseMessage) msg;

					TimedResponseCallback entry = getAndRemoveCallback(requestMsgId);
					if (entry != null) {
						entry.callback.handle(entry.request, getMetadataRsp, null);
					}
					break;
				}
					// EVENTING
				case DPWSMessageConstants.GET_STATUS_RESPONSE_MESSAGE: {
					GetStatusResponseMessage getStatusRsp = (GetStatusResponseMessage) msg;

					TimedResponseCallback entry = getAndRemoveCallback(requestMsgId);
					if (entry != null) {
						entry.callback.handle(entry.request, getStatusRsp, null);
					}
					break;
				}
				case DPWSMessageConstants.RENEW_RESPONSE_MESSAGE: {
					RenewResponseMessage renewRsp = (RenewResponseMessage) msg;

					TimedResponseCallback entry = getAndRemoveCallback(requestMsgId);
					if (entry != null) {
						entry.callback.handle(entry.request, renewRsp, null);
					}
					break;
				}
				case DPWSMessageConstants.SUBSCRIBE_RESPONSE_MESSAGE: {
					SubscribeResponseMessage subscribeRsp = (SubscribeResponseMessage) msg;

					TimedResponseCallback entry = getAndRemoveCallback(requestMsgId);
					if (entry != null) {
						entry.callback.handle(entry.request, subscribeRsp, null);
					}
					break;
				}
				case DPWSMessageConstants.UNSUBSCRIBE_RESPONSE_MESSAGE: {
					UnsubscribeResponseMessage unsubscribeRsp = (UnsubscribeResponseMessage) msg;

					TimedResponseCallback entry = getAndRemoveCallback(requestMsgId);
					if (entry != null) {
						entry.callback.handle(entry.request, unsubscribeRsp, null);
					}
					break;
				}
					// INVOCATION OR NOTIFICATION
				case DPWSMessageConstants.INVOKE_MESSAGE: {
					InvokeMessage invocationMsg = (InvokeMessage) msg;

					TimedResponseCallback entry = getAndRemoveCallback(requestMsgId);
					if (entry != null) {
						entry.callback.handle(entry.request, invocationMsg, null);
					}
					break;
				}
					// FAULT
				case DPWSMessageConstants.FAULT_MESSAGE: {
					FaultMessage faultMsg = (FaultMessage) msg;

					TimedResponseCallback entry = getAndRemoveCallback(requestMsgId);
					if (entry != null) {
						entry.callback.handle(entry.request, faultMsg, null);
					}
					break;
				}
			}
		}

		return ret;
	}

	// /**
	// * Init timeout for registered message.
	// *
	// * @param msgId message id of message to time out.
	// */
	// public void initTimeout( URI msgId ){
	// TimedResponseCallback msgEntry = (TimedResponseCallback)
	// map_msgId_2_entry.get(msgId);
	// if( msgEntry != null ){
	// msgEntry.timedOut();
	// }
	// }

	// ------------------------------- PRIVATE ---------------------------

	/**
	 * Returns <code>true</code> if msgId is managed by the request response
	 * coordinator, else <code>false</false>.
	 * 
	 * @param msgId The msg id to look for.
	 * @return Returns <code>true</code> if msgId is managed by the request
	 *         response coordinator, else <code>false</false>.
	 */
	public boolean containsMsgId(URI msgId) {
		return map_msgId_2_entry.containsKey(msgId);
	}

	/**
	 * Get callback by msg id.
	 * 
	 * @param msgId
	 * @return callback
	 */
	private TimedResponseCallback getCallback(URI msgId) {
		return (TimedResponseCallback) map_msgId_2_entry.get(msgId);
	}

	/**
	 * Get callback by msg id and remove it from list.
	 * 
	 * @param msgId message id
	 * @return callback
	 */
	private TimedResponseCallback getAndRemoveCallback(URI msgId) {
		TimedResponseCallback entry = (TimedResponseCallback) map_msgId_2_entry.remove(msgId);
		if (entry != null) {
			WatchDog.getInstance().unregister(entry);
		}

		return entry;
	}

	private class TimedResponseCallback extends TimedEntry {

		/** request message */
		Message				request;

		/** callback to be called if response is received */
		ResponseCallback	callback;

		// /** response was received? */
		// boolean responseReceived = false;

		TimedResponseCallback(Message request, ResponseCallback callback) {
			super();
			this.request = request;
			this.callback = callback;
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.concurrency.TimedEntry#timedOut()
		 */
		@Override
		public void timedOut() {
			// if( responseReceived == false ){
			// callback.receivedTimeout(msgId);
			// }
			callback.handleTimeout(request);
		}

		@Override
		public String toString() {
			return "TimedResponseCallback [request=" + request + ", callback="
					+ callback + ", relationshipKey=" + relationshipKey
					+ ", timeToRemove=" + timeToRemove + "]";
		}
	}
}

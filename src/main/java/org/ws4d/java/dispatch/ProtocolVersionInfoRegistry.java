/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.dispatch;

import org.ws4d.java.communication.ProtocolVersionInfo;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.EndpointReference;

public class ProtocolVersionInfoRegistry {

	private final static ProtocolVersionInfoRegistry instance=new ProtocolVersionInfoRegistry();


	public static ProtocolVersionInfoRegistry getInstance() {
		return instance;
	}
	private ProtocolVersionInfoRegistry()
	{
		//void
	}

	private final DataStructure defaultProtocolVersionInfos=new ArrayList();
	private final HashMap		protocolVersionInfoMapping	= new HashMap();

	public HashMap getProtocolVersionInfoMapping() {
		return protocolVersionInfoMapping;
	}

	public  ProtocolVersionInfo get(EndpointReference epr) {
		return (ProtocolVersionInfo) protocolVersionInfoMapping.get(epr);
	}

	public  ProtocolVersionInfo add(EndpointReference epr, ProtocolVersionInfo versionInfo) {

		//TODO SSch Check if correct
		if (getProtocolVersionInfoMapping().containsKey(epr)) {
			return get(epr);
		} else {
			getProtocolVersionInfoMapping().put(epr, versionInfo);
			return versionInfo;
		}

		// Re-Modularization 2011-01-21 Implement ProtocolVersionInfoMapping.containsKey
		//		if (ProtocolVersionInfoMapping.containsKey(epr)) {
		//			if (versionInfo == DPWSProperties.DEFAULT_DPWS_VERSION) {
		//				ProtocolVersionInfoMapping.put(epr, versionInfo);
		//				return versionInfo;
		//			} else {
		//				return get(epr);
		//			}
		//		} else {
		//			ProtocolVersionInfoMapping.put(epr, versionInfo);
		//			return versionInfo;
		//		}
	}
	
	public  ProtocolVersionInfo remove(EndpointReference epr) {
		return  (ProtocolVersionInfo) getProtocolVersionInfoMapping().remove(epr);
	}

	
	
	//Default
	protected DataStructure getDefaultProtocolVersionInfos() {
		return defaultProtocolVersionInfos;
	}

	//Fill in the DefaultValues from the DPWSProperties. 
	//This should be done in CommunicationManager start, e.g. DPWSCommunicationManager.start
	public boolean addDefaultSupportedProtocolVersionInfo(ProtocolVersionInfo pvi)
	{
		return getDefaultProtocolVersionInfos().add(pvi);
	}
	public boolean removeDefaultSupportedProtocolVersionInfo(ProtocolVersionInfo pvi) {
		return getDefaultProtocolVersionInfos().remove(pvi);
	}
	public Iterator getDefaultSupportedProtocolVersionInfoIterator() {
		return getDefaultProtocolVersionInfos().iterator();
	}

	public int getDefaultSupportedProtocolVersionInfoSize() {
		return getDefaultProtocolVersionInfos().size();
	}

	public ProtocolVersionInfo getAnyDefaultSupportedProtocolVersion() {

		for(Iterator it=getDefaultSupportedProtocolVersionInfoIterator();it.hasNext();)
		{
			return (ProtocolVersionInfo) it.next();
		}
		return null;
	}


}

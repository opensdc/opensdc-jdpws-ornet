/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.concurrency;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.List;
import org.ws4d.java.util.WS4DIllegalStateException;

/**
 * Implementation of a multiple-readers/single-writer lock support.
 */
public class LockSupport implements Lockable {

	/**
	 * Constant which indicates that the subject is currently unlocked.
	 */
	private static final int	UNLOCKED				= 0;

	/**
	 * Constant which indicates that the subject has only shared locks on it.
	 */
	private static final int	SHARED_LOCKED			= 1;

	/**
	 * Constant which indicates that the subject is locked exclusively.
	 */
	private static final int	EXCLUSIVE_LOCKED		= 2;

	/** List(Lock) List containing lock requests. */
	private final List			waitingForLock;

	/** Map(Thread, Lock) The locks which are currently held by the threads. */
	private final HashMap		allocatedLocks;

	private int					allocatedSharedLocks	= 0;

	private int					allocatedExclusiveLocks	= 0;

	/**
	 * Constructs a new LockSupport.
	 */
	public LockSupport() {
		super();
		this.allocatedLocks = new HashMap();
		this.waitingForLock = new ArrayList();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public synchronized String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("LockSupport (").append(hashCode()).append("): [");
		sb.append(" ASL=").append(allocatedSharedLocks);
		sb.append(", AEL=").append(allocatedExclusiveLocks);
		sb.append(", allocatedLocks=").append(allocatedLocks);
		sb.append(", waiting=").append(waitingForLock);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.concurrency.locks.Lockable#sharedLock()
	 */
	@Override
	public void sharedLock() {
		Lock lock;
		synchronized (this) {
			lock = getLockForCurrentThread();
			if (lock.tryAllocateShared()) {
				return;
			}
			lock.delaySharedAllocation();
		}
		lock.awaitDelayedAllocation();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.concurrency.locks.Lockable#exclusiveLock()
	 */
	@Override
	public void exclusiveLock() {
		Lock lock;
		synchronized (this) {
			lock = getLockForCurrentThread();
			if (lock.tryAllocateExclusive()) {
				return;
			}
			lock.delayExclusiveAllocation();
		}
		lock.awaitDelayedAllocation();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.concurrency.locks.Lockable#trySharedLock()
	 */
	@Override
	public synchronized boolean trySharedLock() {
		Lock lock = getLockForCurrentThread();
		return lock.tryAllocateShared();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.concurrency.locks.Lockable#tryExclusiveLock()
	 */
	@Override
	public synchronized boolean tryExclusiveLock() {
		Lock lock = getLockForCurrentThread();
		return lock.tryAllocateExclusive();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.concurrency.locks.Lockable#releaseSharedLock()
	 */
	@Override
	public synchronized void releaseSharedLock() {
		Lock lock = (Lock) allocatedLocks.get(Thread.currentThread());
		if ((lock == null) || (lock.sharedLocks == 0)) {
			throw new WS4DIllegalStateException("Current thread has no allocated shared lock!");
		}
		lock.releaseShared();
		checkWaitingLockRequests();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.concurrency.locks.Lockable#releaseExclusiveLock()
	 */
	@Override
	public synchronized boolean releaseExclusiveLock() {
		Lock lock = (Lock) allocatedLocks.get(Thread.currentThread());
		if ((lock == null) || (lock.exclusiveLocks == 0)) {
			throw new WS4DIllegalStateException("Current thread has no allocated exclusive lock!");
		}
		lock.releaseExclusive();
		if (lock.exclusiveLocks == 0) {
			checkWaitingLockRequests();
			return true;
		}

		return false;
	}

	private int getState() {
		if ((allocatedSharedLocks > 0) && (allocatedExclusiveLocks == 0)) {
			return SHARED_LOCKED;
		}
		if (allocatedExclusiveLocks > 0) {
			return EXCLUSIVE_LOCKED;
		}
		return UNLOCKED;
	}

	private Lock getLockForCurrentThread() {
		Lock lock = (Lock) allocatedLocks.get(Thread.currentThread());
		if (lock == null) {
			lock = new Lock();
		}
		return lock;
	}

	/**
	 * Determines which queued lock requests can be performed.
	 */
	private void checkWaitingLockRequests() {
		for (Iterator it = waitingForLock.iterator(); it.hasNext();) {
			Lock lock = (Lock) it.next();
			if (!lock.tryAllocateAfterDelay()) {
				return;
			}
			it.remove();
		}
	}

	// private void printWaiting() {
	// Snippet.log("printWaiting: START");
	// for (Iterator it = waitingForLock.iterator(); it.hasNext();) {
	// Lock lock = (Lock) it.next();
	// Snippet.log("\t" + lock.thread.getName() + " --> " + lock);
	// }
	// Snippet.log("printWaiting: END");
	// }
	//
	// private void printAllocated() {
	// Snippet.log("printAllocated: START");
	// for (Iterator it = allocatedLocks.values().iterator(); it.hasNext();) {
	// Lock lock = (Lock) it.next();
	// Snippet.log("\t" + lock);
	// }
	// Snippet.log("printAllocated: END");
	// }

	/**
	 * 
	 */
	private class Lock {

		private final Thread		thread;

		private volatile boolean	allocated		= false;

		private int					sharedLocks		= 0;

		private int					exclusiveLocks	= 0;

		public Lock() {
			super();
			thread = Thread.currentThread();
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			StringBuffer sb = new StringBuffer();
			sb.append("Lock [ ");
			sb.append("thread=").append(thread);
			sb.append(", a=").append(allocated);
			sb.append(", SL=").append(sharedLocks);
			sb.append(", EL=").append(exclusiveLocks);
			sb.append(" ]");
			return sb.toString();
		}

		void delaySharedAllocation() {
			sharedLocks++;
			delayAllocation();
		}

		void delayExclusiveAllocation() {
			exclusiveLocks++;
			if (allocatedLocks.containsKey(thread)) {
				allocatedLocks.remove(thread);
				allocatedSharedLocks -= sharedLocks;
			}
			delayAllocation();
		}

		boolean tryAllocateShared() {
			switch (getState()) {
				case UNLOCKED:
					allocateShared();
					return true;
				case SHARED_LOCKED:
					allocateShared();
					return true;
				case EXCLUSIVE_LOCKED:
					if (exclusiveLocks == allocatedExclusiveLocks) {
						allocateShared();
						return true;
					}
					return false;
				default:
					return false;
			}
		}

		boolean tryAllocateExclusive() {
			switch (getState()) {
				case UNLOCKED:
					allocateExclusive();
					return true;
				case SHARED_LOCKED:
					if (sharedLocks == allocatedSharedLocks) {
						allocateExclusive();
						return true;
					}
					return false;
				case EXCLUSIVE_LOCKED:
					if (exclusiveLocks == allocatedExclusiveLocks) {
						allocateExclusive();
						return true;
					}
					return false;
				default:
					return false;
			}
		}

		/**
		 * Checks if the lock can be allocated.
		 * 
		 * @return <code>true</code> if the lock can be allocated,
		 *         <code>false</code> otherwise.
		 */
		boolean tryAllocateAfterDelay() {
			switch (getState()) {
				case UNLOCKED:
					allocateAfterDelay();
					return true;
				case SHARED_LOCKED:
					if ((exclusiveLocks == 0) || (allocated && (sharedLocks == allocatedSharedLocks))) {
						allocateAfterDelay();
						return true;
					}
					return false;
				case EXCLUSIVE_LOCKED:
					if ((allocated) && (exclusiveLocks == allocatedExclusiveLocks)) {
						allocateAfterDelay();
						return true;
					}
					return false;
				default:
					return false;
			}
		}

		void releaseShared() {
			sharedLocks--;
			allocatedSharedLocks--;
			if (sharedLocks == 0 && exclusiveLocks == 0) {
				allocatedLocks.remove(thread);
			}
		}

		void releaseExclusive() {
			exclusiveLocks--;
			allocatedExclusiveLocks--;
			if (sharedLocks == 0 && exclusiveLocks == 0) {
				allocatedLocks.remove(thread);
				allocated = false;
			}
		}

		synchronized void awaitDelayedAllocation() {
			while (!allocated) {
				try {
					wait();
				} catch (InterruptedException e) {
					// time to check whether we can resume (allocated == true)
				}
			}
		}

		private void allocateShared() {
			sharedLocks++;
			allocatedSharedLocks++;
			allocate();
		}

		private void allocateExclusive() {
			exclusiveLocks++;
			allocatedExclusiveLocks++;
			allocate();
		}

		/**
		 * Current thread is blocked until lock allocation is possible.
		 */
		private void delayAllocation() {
			waitingForLock.add(this);
			allocated = false;
		}

		private void allocateAfterDelay() {
			allocatedExclusiveLocks += exclusiveLocks;
			allocatedSharedLocks += sharedLocks;
			allocate();
			// trigger resume within delayAllocation() of target thread
			synchronized (this) {
				notify();
			}
		}

		private void allocate() {
			allocated = true;
			allocatedLocks.put(thread, this);
		}

	}

}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.concurrency;

public interface ThreadPool {

	public abstract boolean execute(Runnable task);

	/**
	 * Assigns tasks to the thread pool for execution. Use this method for
	 * common non-critical tasks. The tasks will be started as soon as possible.
	 * 
	 * @param task runnable which is assigned to the thread pool
	 */
	public abstract boolean execute(Runnable task, int prio);

	public abstract boolean executeOrAbort(Runnable task);

	/**
	 * Assigns tasks to the thread pool for execution. Use this method for
	 * essential tasks, which have to be immediately started or else aborted.
	 * 
	 * @param task runnable which is assigned to the thread pool
	 * @return boolean returns true only if the runnable was started
	 *         immediately, false if the runnable could not be started
	 *         immediately and was aborted
	 */
	public abstract boolean executeOrAbort(Runnable task, int prio);

	/**
	 * Disposes of the thread pool. New tasks will be ignored. The currently
	 * running threads will be executed until the end.
	 */
	public abstract void shutdown();

	public abstract ThreadGroup getStackThreadGroup();
	
	public abstract boolean setSize(int size);
	
	public abstract int getAvailableFreeThreads();

}

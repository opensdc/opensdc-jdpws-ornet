/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.concurrency;

import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.LinkedList;
import org.ws4d.java.structures.List;
import org.ws4d.java.util.Log;

/**
 * Implements a simple thread pool which allows dynamic creation of worker
 * threads and automatic disposal of unused worker threads after the given
 * period of time.
 * <p>
 * Usage example:
 * </p>
 * <code>
 * <p>ThreadPool myThreadPool = new ThreadPool (3, 5000);<br />
 * myThreadPool.executeOrAbort(runnable1); // essential tasks, which must be started immediately or aborted<br />
 * myThreadPool.execute(runnable2); ...    // common tasks, which will be started as soon as possible <br />
 * myThreadPool.execute(runnableN);<br />
 * myThreadPool.shutdown();</p>
 * </code>
 */

public class DefaultThreadPool implements ThreadPool {

	/**
	 * default size of the thread pool
	 */
	private static int		DEFAULT_SIZE	= 10;

	/**
	 * default time to live for idle available thread pool workers
	 */
	private static long		DEFAULT_TIMEOUT	= 10000;

	private static long		JOIN_TIMEOUT	= Long.parseLong(System.getProperty("DefaultThreadPool.ShutDownTimeOutPerThread", "1000"));

	/**
	 * list of idle worker threads
	 */
	private final List		idleThreads		= new LinkedList();

	/**
	 * list of active worker threads
	 */
	private final List		activeThreads	= new LinkedList();

	/**
	 * queue with tasks waiting for any available worker thread
	 */
	private final List		waitingTasks	= new LinkedList();

	/**
	 * maximal number of threads in the pool
	 */
	private volatile int	size;

	/**
	 * life duration of idle available thread pool workers before which the idle
	 * threads will be disposed of
	 */
	private final long		timeout;

	/**
	 * Internal lock object
	 */
	private final Object	lock			= new Object();


	private final ThreadGroup	stackThreadGroup=new ThreadGroup("JMEDS Thread Group");

	private volatile boolean shutDownActivated=false;

	/**
	 * The constructor of the ThreadPool class, creating a thread pool with
	 * default size and default timeout.
	 */
	public DefaultThreadPool() {
		this(DEFAULT_SIZE, DEFAULT_TIMEOUT);
	}

	/**
	 * The constructor of the ThreadPool class.
	 * 
	 * @param size maximal number of threads in the pool
	 * @param timeout life duration of idle thread pool worker
	 */
	public DefaultThreadPool(int size, long timeout) {
		this.size = size;
		this.timeout = timeout;
	}

	/**
	 * The constructor of the ThreadPool class with the default life duration of
	 * idle threads
	 * 
	 * @param size maximal number of threads in the pool
	 */
	public DefaultThreadPool(int size) {
		this(size, DEFAULT_TIMEOUT);
	}

	/**
	 * Signalizes to the thread pool that a worker thread has finished his task
	 * and is ready for further tasks. If there is no tasks waiting in the queue
	 * for execution, the worker thread is placed on the list of available
	 * thread workers. Else the thread worker gets the first waiting task
	 * assigned for the execution.
	 * 
	 * @param t worker thread which has finished its task
	 */
	private void signalAvailability(WorkerThread w) {
		synchronized (lock) {
			idleThreads.add(w);
			activeThreads.remove(w);
			if (waitingTasks.size() > 0) {
				w.setTask((Runnable) waitingTasks.remove(0));
			} else {
				w.setTask(null);
			}
		}
	}

	/**
	 * Signalizes to the thread pool that a thread worker is going to terminate
	 * itself.
	 * 
	 * @param t worker thread signaling the termination
	 */
	private void signalTermination(WorkerThread w) {
		idleThreads.remove(w);
	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.concurrency.IThreadPool#execute(java.lang.Runnable)
	 */
	@Override
	public boolean execute(Runnable task) {
		return execute(task, Thread.NORM_PRIORITY);
	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.concurrency.IThreadPool#execute(java.lang.Runnable, int)
	 */
	@Override
	public boolean execute(Runnable task, int prio) {
		boolean retVal=false;
		synchronized (lock) {
			if (!tryAllocation(task, prio)) {
				waitingTasks.add(task);
			}else{
				retVal=true;
			}
		}
		return retVal;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.concurrency.IThreadPool#executeOrAbort(java.lang.Runnable)
	 */
	@Override
	public boolean executeOrAbort(Runnable task) {
		return executeOrAbort(task, Thread.NORM_PRIORITY);
	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.concurrency.IThreadPool#executeOrAbort(java.lang.Runnable, int)
	 */
	@Override
	public boolean executeOrAbort(Runnable task, int prio) {
		synchronized (lock) {

			if (!tryAllocation(task, prio)) {
				return false;
			}
			return true;
		}
	}

	private boolean tryAllocation(Runnable task, int prio) {
		if (!shutDownActivated)
		{
			if (Log.isDebug()) 
				Log.debug("Trying to allocate a task on thread pool:"+task+" Active Threads"+ activeThreads.size()+" Idle:"+idleThreads.size()+" Limit:"+size);

			if (idleThreads.size() == 0) {
				if (activeThreads.size() < size) {
					WorkerThread w = new WorkerThread(getStackThreadGroup());
					allocate(w, task, prio);
					w.start();

					return true;
				}
			} else {
				WorkerThread w = (WorkerThread) idleThreads.remove(0);
				allocate(w, task, prio);

				return true;
			}
		}
		return false;
	}

	private void allocate(WorkerThread w, Runnable task, int prio) {
		activeThreads.add(w);
		w.setTask(task);
		w.setPriority(prio);
	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.concurrency.IThreadPool#shutdown()
	 */
	@Override
	public void shutdown() {
		this.shutDownActivated=true;
		Thread thisThread = Thread.currentThread();

		int activeThreadsCnt=-1;
		int idleThreadsCnt=-1;

		List availableThreads = new LinkedList();
		synchronized (lock) {
			activeThreadsCnt=activeThreads.size();
			for (Iterator it = activeThreads.iterator(); it.hasNext();) {
				WorkerThread w = (WorkerThread) it.next();
				availableThreads.add(w);
				w.shutdown();
			}
			idleThreadsCnt=idleThreads.size();
			for (Iterator it = idleThreads.iterator(); it.hasNext();) {
				WorkerThread w = (WorkerThread) it.next();
				availableThreads.add(w);
				w.shutdown();
				it.remove();
			}
		}

		int threadGroupCount=stackThreadGroup.activeCount();
		if (Log.isInfo())
			Log.info( "ThreadPool shutdown: Worker:"+availableThreads.size()+" Active:"+activeThreadsCnt+" Idle:"+idleThreadsCnt+ ", ActiveThreadGroupCount:"+threadGroupCount);

		for (Iterator it = availableThreads.iterator(); it.hasNext();) {
			WorkerThread w = (WorkerThread) it.next();
			if (w != thisThread) {
				try {
					if (Log.isInfo())
						Log.info("ThreadPool shutdown: Waiting for "+w.getName()+" for max. "+JOIN_TIMEOUT+" ms.");

					w.join(JOIN_TIMEOUT);
				} catch (InterruptedException e) {
					Log.info(e);
				}
			}
		}
		if (Log.isInfo())
			Log.info("ThreadPool shutdown: Done.");
	}

	/**
	 * Implements a work delegation thread - worker thread class.
	 */
	class WorkerThread extends Thread {
		private static final String namePostfix="WorkerThread";
		private boolean		shutdown	= false;

		/**
		 * task that is to be executed by the worker thread
		 */
		private Runnable	task;



		public WorkerThread(ThreadGroup threadGroup) {
			super(threadGroup, namePostfix);
			setDaemon(true);
		}

		@Override
		public void run() {
			while (true) {
				synchronized (this) {
					if (!shutdown && (task == null)) {
						try {
							wait(timeout);
						} catch (InterruptedException e) {
							// swallow
						}
						if (shutdown) {
							return;
						}
					}
				}
				synchronized (lock) {
					if (task == null) {
						shutdown = true;
						signalTermination(this);

						return;
					}
				}
				try {
					task.run();
				} catch (Exception e) {
					Log.error("Exception occurred while running thread. " + e.getMessage());
					Log.error(e);
				} finally {
					signalAvailability(this);
				}
			}
		}

		/**
		 * Setter method for the task variable
		 * 
		 * @param task task to be assigned to the worker thread
		 */
		public synchronized void setTask(Runnable task) {
			this.task = task;

			//Added by SSch 
			if (task!=null)
				setName(task.getClass().getSimpleName()+"-"+namePostfix+"["+task.toString()+"]");
			notify();
		}

		public synchronized void shutdown() {
			shutdown = true;
			notify();
		}

	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.concurrency.IThreadPool#getStackThreadGroup()
	 */
	@Override
	public ThreadGroup getStackThreadGroup() {
		return stackThreadGroup;
	}

	@Override
	public boolean setSize(int newSize) {
		boolean retVal=false;
		if (newSize>0)
		{
			synchronized (lock) {
				if (newSize> activeThreads.size())
				{
					this.size=newSize;
					retVal=true;
				}
			}
		}
		return retVal;
	}

	@Override
	public int getAvailableFreeThreads() {
		synchronized (lock) {
			int retVal=size-activeThreads.size();
			return retVal;
		}
	}
}

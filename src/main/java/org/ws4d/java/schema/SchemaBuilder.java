/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.schema;

import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.service.Fault;
import org.ws4d.java.service.OperationDescription;
import org.ws4d.java.service.Service;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.StringUtil;

public class SchemaBuilder {
	
	public static void addToSchemaMap(Iterator iterator, HashMap map) {
		while (iterator.hasNext()) {
			OperationDescription op = (OperationDescription) iterator.next();
			Element input = op.getInput();
			addElementMap(input, map);
			Element output = op.getOutput();
			addElementMap(output, map);
			for (Iterator it2 = op.getFaults(); it2.hasNext();) {
				Fault fault = (Fault) it2.next();
				Element faultElement = fault.getElement();
				addElementMap(faultElement, map);
			}
		}
	}

	public static HashMap createSchema(Service service, String targetNamespace) {
		HashMap map = new HashMap();
		addToSchemaMap(service.getOperations(), map);
		addToSchemaMap(service.getEventSources(), map);
		return map;
	}

	public static Schema createSchema(Service service) {
		Schema schema = new Schema();
		String namespace = null;
		try {
			if (service.getParentDeviceReference() != null) {
				namespace = service.getParentDeviceReference().getDevice().getDefaultNamespace();
			}
		} catch (TimeoutException e1) {
			Log.debug(e1);
		}
		addToSchema(service.getOperations(), schema, namespace);
		addToSchema(service.getEventSources(), schema, namespace);

		try {
			schema.resolveSchema();
		} catch (SchemaException e) {
			Log.info(e);
		}
		return schema;
	}
	
	public static void updateSchema(Schema schema) throws SchemaException
	{
		if (schema!=null)
			schema.resolveSchema();
	}
	private static void addToSchema(Iterator operationDescs, Schema schema, String defaultNamespace) {
		while (operationDescs.hasNext()) {
			OperationDescription op = (OperationDescription) operationDescs.next();
			Element input = op.getInput();
			if (input != null) {
				input.globalScope = true;
				if (input.name == null) {
					// input.name = QNameFactory.getInstance().getQName(input.getClass().getSimpleName(),
					// defaultNamespace);
					input.name = QNameFactory.getInstance().getQName(StringUtil.simpleClassName(input.getClass()), defaultNamespace);
				}
				schema.addElement(input);
			}
			Element output = op.getOutput();
			if (output != null) {
				output.globalScope = true;
				schema.addElement(output);
			}
			for (Iterator it2 = op.getFaults(); it2.hasNext();) {
				Fault fault = (Fault) it2.next();
				Element faultElement = fault.getElement();
				if (faultElement != null) {
					faultElement.globalScope = true;
					schema.addElement(faultElement);
				}
			}
		}
	}
	
	private static void addElementMap(Element elem, HashMap map) {
		if (elem != null) {
			Schema schema = null;
			String namespace = elem.getName().getNamespace();
			if (map.containsKey(namespace)) {
				schema = (Schema) map.get(namespace);
			} else if (elem.getBelongingSchema()!=null) {
				schema=elem.getBelongingSchema();
				map.put(elem.getName().getNamespace(), elem.getBelongingSchema());
			} else {
				schema = new Schema(namespace);
				map.put(elem.getName().getNamespace(), schema);
			}
			
			//BUGFIX: for https://sourceforge.net/p/opensdc/tickets/6/
			if (schema.getElement(elem.getName())==null)
			{
				schema.addElement(elem);	
			}
			
		}
	}

}

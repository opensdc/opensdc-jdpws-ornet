/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.schema;

import java.io.IOException;

import org.ws4d.java.constants.SchemaConstants;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.util.StringUtil;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class AnyAttribute extends Reference {
	static final String	ATTRIBUTE_NAMESPACE		= ANY_NAMESPACE_ATTR;
	
	static final String	ATTRIBUTE_PROCESSCONTENTS	= ANY_PROCESSCONTENTS_ATTR;
	
	protected String namespaceAttr = null;
	protected String processContents = null;

	static final AnyAttribute createAnyAttribute(ElementParser parser) throws XmlPullParserException, IOException {
		AnyAttribute a = new AnyAttribute();

		int c = parser.getAttributeCount();
		for (int i = 0; i < c; i++) {
			String attributeName = parser.getAttributeName(i);
			String attributeNamespace = parser.getAttributeNamespace(i);
			String attributeValue = parser.getAttributeValue(i);
			if (attributeNamespace == null || "".equals(attributeNamespace)) {
				if (AnyAttribute.ATTRIBUTE_NAMESPACE.equals(attributeName)){
					a.setNamespaceAttr(attributeValue);
				}else if (AnyAttribute.ATTRIBUTE_PROCESSCONTENTS.equals(attributeName)){
					a.setProcessContents(attributeValue);
				}
			}
		}
		
		
		int d = parser.getDepth();
		while (parser.nextTag() != XmlPullParser.END_TAG && parser.getDepth() == d + 1) {
			/*
			 * check for inline definitions
			 */
			String namespace = parser.getNamespace();
			String name = parser.getName();
			if (XMLSCHEMA_NAMESPACE.equals(namespace)) {
				if (StringUtil.equalsIgnoreCase(Annotation.TAG_ANNOTATION, name)) {
					Annotation.handleAnnotation(parser, a);
				}
			}
		}
		return a;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.schema.Any#getSchemaIdentifier()
	 */
	@Override
	public int getSchemaIdentifier() {
		return SchemaConstants.XSD_ANYATTRIBUTE;
	}

	/**
	 * Serializes the schema attribute.
	 * 
	 * @param serializer the serializer which should be used.
	 * @param schema the schema.
	 * @throws IOException
	 */
	void serialize(XmlSerializer serializer, Schema schema) throws IOException {
		serializer.startTag(XMLSCHEMA_NAMESPACE, TAG_ANYATTRIBUTE);
		if (namespaceAttr!=null && namespaceAttr.length()>0)
		{
			serializer.attribute(null, AnyAttribute.ATTRIBUTE_NAMESPACE, namespaceAttr);
		}
		if (processContents!=null && processContents.length()>0)
		{
			serializer.attribute(null, AnyAttribute.ATTRIBUTE_PROCESSCONTENTS, processContents);
		}
		serializer.endTag(XMLSCHEMA_NAMESPACE, TAG_ANYATTRIBUTE);

	}

	protected String getNamespaceAttr() {
		return namespaceAttr;
	}

	protected void setNamespaceAttr(String namespaceAttr) {
		this.namespaceAttr = namespaceAttr!=null?namespaceAttr.trim():null;
	}

	protected String getProcessContents() {
		return processContents;
	}

	protected void setProcessContents(String processContents) {
		this.processContents = processContents!=null?processContents.trim():null;
	}

}

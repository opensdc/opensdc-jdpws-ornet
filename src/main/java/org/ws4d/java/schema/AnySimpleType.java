/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.schema;

import org.ws4d.java.types.QName;

public class AnySimpleType extends SimpleType {

	public AnySimpleType() {
		super();
	}

	public AnySimpleType(QName name) {
		super(name);
	}

	@Override
	public int getSchemaIdentifier() {
		//TODO SSch XSD_SIMPLETYPE???
		return XSD_SIMPLETYPE;
	}

}

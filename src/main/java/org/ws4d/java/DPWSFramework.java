/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java;

import java.io.IOException;
import java.io.InputStream;

import org.ws4d.java.attachment.AttachmentException;
import org.ws4d.java.attachment.AttachmentStore;
import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.DPWSCommunicationManagerID;
import org.ws4d.java.communication.monitor.MonitorStreamFactory;
import org.ws4d.java.concurrency.DefaultThreadPool;
import org.ws4d.java.configuration.AttachmentProperties;
import org.ws4d.java.configuration.BindingProperties;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.configuration.DevicesPropertiesHandler;
import org.ws4d.java.configuration.DispatchingProperties;
import org.ws4d.java.configuration.EventingProperties;
import org.ws4d.java.configuration.FrameworkProperties;
import org.ws4d.java.configuration.GlobalPropertiesHandler;
import org.ws4d.java.configuration.MessageProperties;
import org.ws4d.java.configuration.Properties;
import org.ws4d.java.configuration.ServicesPropertiesHandler;
import org.ws4d.java.dispatch.DeviceServiceRegistryProvider;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.dispatch.FrameworkShutdownCallback;
import org.ws4d.java.dispatch.MessageInformer;
import org.ws4d.java.dispatch.PresentationProvider;
import org.ws4d.java.eventing.ClientSubscriptionManager;
import org.ws4d.java.eventing.EventSource;
import org.ws4d.java.framework.module.AttachmentModule;
import org.ws4d.java.framework.module.ClientModule;
import org.ws4d.java.framework.module.EventingModule;
import org.ws4d.java.framework.module.PresentationModule;
import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.framework.module.ServiceModule;
import org.ws4d.java.io.fs.FileSystem;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.platform.Toolkit;
import org.ws4d.java.presentation.Presentation;
import org.ws4d.java.service.Service;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.WatchDog;

/**
 * <p>
 * This is the main framework class for the Java Multiedition DPWS Stack (JMEDS
 * 2.0).
 * </p>
 * <p>
 * It offers <i>static</i> methods to start and stop the framework.
 * </p>
 * <p>
 * This class verifies the existence of the following modules:
 * <ul>
 * <li>Client support</li>
 * <li>Device and Service support</li>
 * <li>Event support</li>
 * <li>Special platform dependent implementation of the communication and file
 * system</li>
 * <li>Attachment support</li>
 * <li>Security support</li>
 * </ul>
 * </p>
 * <p>
 * Furthermore this class allows access to some special and optional framework
 * components like:
 * <ul>
 * <li>{@link DefaultThreadPool}</li>
 * <li>{@link CommunicationManager} <i>(at least one is necessary)</i></li>
 * <li>{@link MonitorStreamFactory} <i>(optional)</i></li>
 * <li>{@link FileSystem} <i>(optional)</i></li>
 * </ul>
 * </p>
 * <p>
 * <strong>Important:</strong> It is necessary to {@link #start(String[]) start}
 * the framework before anything else can be used!
 * </p>
 * <p>
 * Your code could look something like this:
 * </p>
 * 
 * <pre>
 * DPWSFramework.start(args);
 * 
 * // Your code here
 * 
 * DPWSFramework.stop();
 * </pre>
 */
public final class DPWSFramework{

	/**
	 * Identifier for the client support (Client module).
	 * <p>
	 * This identifier can be used to verify whether the <i>Client module</i>
	 * has been loaded or not. To check this module, use the
	 * {@link #hasModule(int)} method.
	 * </p>
	 * <p>
	 * The <i>Client module</i> includes the classes to create a DPWS client and
	 * the classes which are necessary if the client wants to use the device and
	 * service discovery.
	 * </p>
	 */
	public static final int				DPWS_CLIENT_MODULE			= 0x01;

	/**
	 * Identifier for the service and device support. (Service module).
	 * <p>
	 * This identifier can be used to verify whether the <i>Service module</i>
	 * has been loaded or not. To check this module, use the
	 * {@link #hasModule(int)} method.
	 * </p>
	 * <p>
	 * The <i>Service module</i> includes the classes to create a DPWS device
	 * and service.
	 * </p>
	 */
	public static final int				DPWS_SERVICE_MODULE			= 0x02;

	/**
	 * Identifier for the event support. (Eventing module)
	 * <p>
	 * This identifier can be used to verify whether the <i>Eventing module</i>
	 * has been loaded or not. To check this module, use the
	 * {@link #hasModule(int)} method.
	 * </p>
	 * <p>
	 * The <i>Eventing module</i> includes the classes to handle incoming DPWS
	 * events.
	 * </p>
	 */
	public static final int				DPWS_EVENTING_MODULE		= 0x04;

	/**
	 * Identifier for the SE platform support. (SE module)
	 * <p>
	 * This identifier can be used to verify whether the <i>SE module</i> has
	 * been loaded or not. To check this module, use the {@link #hasModule(int)}
	 * method.
	 * </p>
	 * <p>
	 * The <i>SE module</i> includes the classes which allow networking and file
	 * access for Java SE platforms.
	 * </p>
	 */
	public static final int				DPWS_SE_PLATFORM_MODULE		= 0x08;

	/**
	 * Identifier for the CLDC platform support. (CLDC module)
	 * <p>
	 * This identifier can be used to verify whether the <i>CLDC module</i> has
	 * been loaded or not. To check this module, use the {@link #hasModule(int)}
	 * method.
	 * </p>
	 * <p>
	 * The <i>CLDC module</i> includes the classes which allow networking for
	 * Java CLDC platforms.
	 * </p>
	 */
	public static final int				DPWS_CLDC_PLATFORM_MODULE	= 0x10;

	/**
	 * Identifier for the attachment support. (Attachment module)
	 * <p>
	 * This identifier can be used to verify whether the <i>Attachment
	 * module</i> has been loaded or not. To check this module, use the
	 * {@link #hasModule(int)} method.
	 * </p>
	 * <p>
	 * The <i>Attachment module</i> includes the classes to send and receive
	 * attachments.
	 * </p>
	 */
	public static final int				DPWS_ATTACHMENT_MODULE		= 0x20;

	/**
	 * Identifier for the security support. (Security module)
	 * <p>
	 * This identifier can be used to verify whether the <i>Security module</i>
	 * has been loaded or not. To check this module, use the
	 * {@link #hasModule(int)} method.
	 * </p>
	 * <p>
	 * The <i>Security module</i> includes the classes to secure the DPWS
	 * communication, using WS-Security techniques.
	 * </p>
	 */
	public static final int				DPWS_SECURITY_MODULE		= 0x40;

	public static final int				DPWS_PRESENTATION_MODULE	= 0x80;

	//	private static final boolean		HAVE_DPWS_CLIENT_MODULE;
	//
	//	private static final boolean		HAVE_DPWS_SERVICE_MODULE;
	//
	//	private static final boolean		HAVE_DPWS_EVENTING_MODULE;
	//
	//	private static final boolean		HAVE_DPWS_SE_PLATFORM_MODULE;
	//
	//	private static final boolean		HAVE_DPWS_CLDC_PLATFORM_MODULE;
	//
	//	private static final boolean		HAVE_DPWS_ATTACHMENT_MODULE;
	//
	//	private static final boolean		HAVE_DPWS_PRESENTATION_MODULE;
	//
	//	private static final boolean		HAVE_DPWS_SECURITY_MODULE;




	//	static {
	//		boolean result = false;
	//		try {
	//			Class.forName("org.ws4d.java.client.DefaultClient");
	//			result = true;
	//		} catch (ClassNotFoundException e) {
	//			// void
	//		}
	//		HAVE_DPWS_CLIENT_MODULE = result;
	//
	//		result = false;
	//		try {
	//			Class.forName("org.ws4d.java.service.DefaultService");
	//			result = true;
	//		} catch (ClassNotFoundException e) {
	//			// void
	//		}
	//		HAVE_DPWS_SERVICE_MODULE = result;
	//
	//		result = false;
	//		try {
	//			Class.forName("org.ws4d.java.service.DefaultEventSource");
	//			result = true;
	//		} catch (ClassNotFoundException e) {
	//			// void
	//		}
	//		HAVE_DPWS_EVENTING_MODULE = result;
	//
	//		result = false;
	//		try {
	//			Class.forName("org.ws4d.java.communication.connection.tcp.SESocket");
	//			result = true;
	//		} catch (ClassNotFoundException e) {
	//			// void
	//		}
	//		HAVE_DPWS_SE_PLATFORM_MODULE = result;
	//
	//		result = false;
	//		try {
	//			Class.forName("org.ws4d.java.communication.connection.tcp.CLDCSocket");
	//			result = true;
	//		} catch (ClassNotFoundException e) {
	//			// void
	//		}
	//		HAVE_DPWS_CLDC_PLATFORM_MODULE = result;
	//
	//		result = false;
	//		try {
	//			Class.forName("org.ws4d.java.attachment.AbstractAttachment");
	//			result = true;
	//		} catch (ClassNotFoundException e) {
	//			// void
	//		}
	//		HAVE_DPWS_ATTACHMENT_MODULE = result;
	//
	//		result = false;
	//		try {
	//			Class.forName("org.ws4d.java.presentation.DeviceServicePresentation");
	//			result = true;
	//		} catch (ClassNotFoundException e) {
	//			// void
	//		}
	//		HAVE_DPWS_PRESENTATION_MODULE = result;
	//
	//		result = false;
	//		try {
	//			Class.forName("org.ws4d.java.security.DPWSSecurityManagerSE");
	//			result = true;
	//		} catch (ClassNotFoundException e) {
	//			// void
	//		}
	//		if (!result) {
	//			try {
	//				Class.forName("org.ws4d.java.security.DPWSSecurityManagerCLDC");
	//				result = true;
	//			} catch (ClassNotFoundException e) {
	//				// void
	//			}
	//		}
	//
	//		HAVE_DPWS_SECURITY_MODULE = result;
	//
	//	}

	//	private static final Object			LOCAL_FILE_SYSTEM_LOCK		= new Object();

	/**
	 * Indicator for framework run state.
	 */
	//	private static volatile boolean		running						= false;

	/**
	 * The instance thread pool.
	 */
	//	private static ThreadPool			threadpool					= null;

	private static String				propertiesPath				= null;

	private static final Properties		properties					= Properties.getInstance();

	//	private static MonitorStreamFactory	monitorFactory				= null;

	//	private static org.ws4d.java.platform.Toolkit				toolkit;

	//	private static FileSystem			localFileSystem;

	//	private static DPWSSecurityManager	securityManager				= null;

	//	private static Presentation			presentation				= null;



	//	private static HashSet				subscriptions				= new HashSet();

	//	static {
	//		boolean succ = false;
	//		try {
	//			Class clazz = Class.forName("org.ws4d.java.security.DPWSSecurityManagerSE");
	//			securityManager = ((DPWSSecurityManager) clazz.newInstance());
	//			succ = true;
	//		} catch (ClassNotFoundException e) {} catch (IllegalAccessException e) {
	//			e.printStackTrace();
	//		} catch (InstantiationException e) {
	//			e.printStackTrace();
	//		}
	//		if (!succ) {
	//			try {
	//				Class clazz = Class.forName("org.ws4d.java.security.cldc.DPWSSecurityManagerCLDC");
	//				securityManager = (DPWSSecurityManager) clazz.newInstance();
	//				succ = true;
	//			} catch (ClassNotFoundException e) {} catch (InstantiationException e) {} catch (IllegalAccessException e) {
	//				// void
	//			}
	//		}
	//
	//	}

	/**
	 * Starts the framework.
	 * <p>
	 * This method initializes the necessary framework components.
	 * </p>
	 * <p>
	 * <strong>Important:</strong> It is necessary to {@link #start(String[])
	 * start} the framework before anything else can be used!
	 * </p>
	 * <p>
	 * This method starts the watchdog, loads the properties and initializes the
	 * communications modules.
	 * </p>
	 * 
	 * @param args Here you can pass-through the command-line arguments. the
	 *            first element is interpreted as the location of the properties
	 *            file.
	 */
	public static synchronized void start(String[] args) {
		if (FrameworkModuleRegistry.getInstance().isRunning()) return;
		try {

			initializeDefaultPropertiesHandler();
			initializeModuleRegistry();
			// here we go!
			createToolkit();


			initializeProperties(args);
			
			//Add WatchDog
			boolean watchdog = PlatformSupport.getInstance().getToolkit().getThreadPool().executeOrAbort(WatchDog.getInstance());
			if (watchdog == false) {
				throw new RuntimeException("Cannot start the watchdog.");
			}

			createPresentation();

			// start message informer
			MessageInformer.getInstance().start();

			// load communication managers
			CommunicationManagerRegistry.DEFAULT_COMMUNICATION_MANAGERS.add(DPWSCommunicationManagerID.INSTANCE);
			CommunicationManagerRegistry.loadAll();

			//DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().init();



			/*
			 * Mark the framework as up and running.
			 */
			FrameworkModuleRegistry.getInstance().setRunning(true);
			Log.info("DPWS Framework ready.");
		} catch (Exception e) {
			Log.info("DPWS Framework not started.");
			Log.info(e);
		}
	}

	private static void initializeDefaultPropertiesHandler() {
		properties.register(Properties.HEADER_SECTION_DEVICES,  DevicesPropertiesHandler.getInstance());
		properties.register(Properties.HEADER_SECTION_SERVICES, ServicesPropertiesHandler.getInstance());
		properties.register(Properties.HEADER_SECTION_BINDINGS, BindingProperties.getInstance());
		properties.register(Properties.HEADER_SECTION_EVENTING, EventingProperties.getInstance());
		properties.register(Properties.HEADER_SECTION_MESSAGES, MessageProperties.getInstance());
	}

	private static void initializeProperties(String[] args) {


		if (FrameworkModuleRegistry.getInstance().hasModule(AttachmentModule.class)) {
			properties.register(AttachmentProperties.HEADER_SUBSECTION_ATTACHMENT, AttachmentProperties.getInstance());
		}

		properties.register(Properties.HEADER_SECTION_GLOBAL, GlobalPropertiesHandler.getInstance());
		properties.register(Properties.HEADER_SUBSECTION_DISPATCHING, DispatchingProperties.getInstance());
		properties.register(Properties.HEADER_SUBSECTION_FRAMEWORK, FrameworkProperties.getInstance());
		properties.register(Properties.HEADER_SECTION_DPWS, DPWSProperties.getInstance());


		if (args != null && args.length >= 1) {
			propertiesPath = args[0];
		}

		if (propertiesPath != null) {
			try {
				init(properties,propertiesPath);
			} catch (Exception e) {
				Log.info(e);
			}
		} else {
			init(properties);
		}
	}

	private static void initializeModuleRegistry() throws InstantiationException, IllegalAccessException 
	{
		FrameworkModuleRegistry.getInstance().setShutdownCallback(new ShutdownCallback());
		Class<?> result=null;

		if ((result=checkClassExistence("org.ws4d.java.client.DefaultClientModule"))!=null)
		{
			ClientModule module=(ClientModule) result.newInstance();
			module.registerPropertiesHandler(properties);
			FrameworkModuleRegistry.getInstance().registerModule(ClientModule.class,module);
		}

		if ((result=checkClassExistence("org.ws4d.java.service.DefaultServiceModule"))!=null)
		{
			ServiceModule module=(ServiceModule) result.newInstance();
			module.registerPropertiesHandler(properties);
			FrameworkModuleRegistry.getInstance().registerModule(ServiceModule.class,module);
		}
		if ((result=checkClassExistence("org.ws4d.java.eventing.DefaultEventingModule"))!=null)
		{

			FrameworkModuleRegistry.getInstance().registerModule(EventingModule.class,result.newInstance());
		}


		if ((result=checkClassExistence("org.ws4d.java.attachment.AbstractAttachment"))!=null)
		{
			FrameworkModuleRegistry.getInstance().registerModule(AttachmentModule.class,new Object()); //TODO Check
		}

		if ((result=checkClassExistence("org.ws4d.java.presentation.DeviceServicePresentation"))!=null)
		{
			FrameworkModuleRegistry.getInstance().registerModule(PresentationModule.class,result.newInstance()); 
		}

		if (Boolean.parseBoolean(System.getProperty("MDPWS.RegisterSecurityModule", "true")))
		{
			if ((result=checkClassExistence("org.ws4d.java.security.DPWSSecurityManagerSE"))!=null)
			{
				FrameworkModuleRegistry.getInstance().registerModule(SecurityManagerModule.class,result.newInstance());
			}else if ((result=checkClassExistence("org.ws4d.java.security.DPWSSecurityManagerCLDC"))!=null)
			{
				FrameworkModuleRegistry.getInstance().registerModule(SecurityManagerModule.class,result.newInstance());
			}
		}

	}

	public static Class<?> checkClassExistence(String clzName){
		Class<?> result = null;
		try {
			result=Class.forName(clzName);
		} catch (ClassNotFoundException e) {
			// void
		}
		return result;
	}

	private static void init(Properties props) 
	{
		props.read((InputStream)null);
	}

	private static void init(Properties props, String propsPath) throws IOException 
	{
		//Re-Modularization 2011-01-21 Check

		InputStream is= PlatformSupport.getInstance().getToolkit().getResourceAsStream(new URI(propsPath));
		props.read(is);
	}
	//	
	//	/**
	//	 * Initialize Properties. Default property entries will be used.
	//	 */
	//	public void init() {
	//		init((InputStream)null);
	//	}
	//
	//	/**
	//	 * Initialize Properties. Property file will be used.
	//	 * 
	//	 * @param filename The path within the filename must be relative to the
	//	 *            working directory of the stack.
	//	 */
	//	public void init(String filename) {
	// //		
	//		try {
	//			if (DPWSFramework.hasModule(DPWSFramework.DPWS_CLDC_PLATFORM_MODULE)) {
	//				Log.info("Reading via getResourceAsStream() ...");
	//				init(getClass().getResourceAsStream(filename));
	//			} else {
	//				init(DPWSFramework.getLocalFileSystem().readFile(filename));
	//			}
	//		} catch (IOException e) {
	//			Log.warn("Cannot load framework properties from " + filename + ". " + e.getMessage());
	//		}
	//	}

	/**
	 * Stops the framework as soon as possible.
	 * <p>
	 * This method is the counter piece to {@link #start(String[])}. It stops
	 * the framework and the running components. This method will wait until the
	 * opened connection are ready to be closed.
	 * </p>
	 * <p>
	 * If it is necessary to stop the framework immediately the {@link #kill()}
	 * method should be used.
	 * </p>
	 * 
	 * @see #start(String[])
	 * @see #frameworkToBeKilled()
	 */
	public static synchronized void stop() {
		stopInternal(false);
	}

	/**
	 * Stops the framework immediately!!!!
	 * <p>
	 * This method is the counter piece to {@link #start(String[])}. It stops
	 * the framework and the running components. This method will
	 * <strong>not</strong> wait until the opened connection are ready to be
	 * closed, any existing connection will be closed instant.
	 * </p>
	 * 
	 * @see #start(String[])
	 * @see #frameworkToBeStopped()
	 */
	public static synchronized void kill() {
		stopInternal(true);
	}

	private static void stopInternal(boolean kill) {
		if (!FrameworkModuleRegistry.getInstance().isRunning()) return;
		if (kill) {
			Log.info("Killing DPWS Framework...");
		} else {
			Log.info("Stopping DPWS Framework...");
		}

		if (Log.isDebug()) {
			Log.debug("Unsubscribing from all event sources.");
		}

		unsubscribeAll();



		if (Log.isDebug()) {
			Log.debug("Stopping communication managers.");
		}
		// stop communication managers
		if (kill) {
			CommunicationManagerRegistry.killAll();
		} else {
			CommunicationManagerRegistry.stopAll();
		}

		if (Log.isDebug()) {
			Log.debug("Stopping message informer.");
		}
		MessageInformer.getInstance().stop();
		if (Log.isDebug()) {
			Log.debug("Stopping watch dog.");
		}

		//TODO SSch Check
		if (FrameworkModuleRegistry.getInstance().hasModule(Service.class) | FrameworkModuleRegistry.getInstance().hasModule(EventSource.class)) 
		{
			DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().tearDown();
		}

		WatchDog.getInstance().stop();
		if (AttachmentStore.exists()) {
			try {
				AttachmentStore.getInstance().cleanup();
			} catch (AttachmentException e) {
				// void
			}
		}
		if (Log.isDebug()) {
			Log.debug("Shutting down the threadpool.");
		}
		PlatformSupport.getInstance().getToolkit().shutdown();
		FrameworkModuleRegistry.getInstance().setRunning(false);
		Log.info("DPWS Framework stopped.");
	}

	//	/**
	//	 * Indicates whether the framework was started or not.
	//	 * <p>
	//	 * This method returns <code>true</code> if the framework is running,
	//	 * <code>false</code> otherwise.
	//	 * </p>
	//	 * 
	//	 * @return <code>true</code> if the framework is running, <code>false</code>
	//	 *         otherwise.
	//	 */
	//	public static boolean isRunning() {
	//		return running;
	//	}

	//	/**
	//	 * Adds a event subscription to the framework. This allows the framework to
	//	 * unsubscribe on shutdown.
	//	 * 
	//	 * @param subscription the subscription which the framework should take care
	//	 *            about.
	//	 */
	//	public static synchronized void addClientSubscrption(ClientSubscription subscription) {
	//		subscriptions.add(subscription);
	//	}
	//
	//	/**
	//	 * Removes a event subscription.
	//	 * 
	//	 * @param subscription the subscription which is not important any more.
	//	 */
	//	public static synchronized void removeClientSubscrption(ClientSubscription subscription) {
	//		subscriptions.remove(subscription);
	//	}
	//
	/**
	 * Unsubscribe from all event sources.
	 */
	private static void unsubscribeAll() {
		ClientSubscriptionManager.getInstance().unsubscribeAll();
	}

	//	/**
	//	 * Allows to verify whether a module has been loaded and can be used or not.
	//	 * <p>
	//	 * You can check the modules listed below.
	//	 * </p>
	//	 * 
	//	 * @param module the module identifier.
	//	 * @return returns <code>true</code> if the module has been loaded,
	//	 *         <code>false</code> otherwise.
	//	 * @see #DPWS_CLIENT_MODULE
	//	 * @see #DPWS_SERVICE_MODULE
	//	 * @see #DPWS_EVENTING_MODULE
	//	 * @see #DPWS_ATTACHMENT_MODULE
	//	 * @see #DPWS_SE_PLATFORM_MODULE
	//	 * @see #DPWS_CLDC_PLATFORM_MODULE
	//	 * @see #DPWS_SECURITY_MODULE
	//	 * @see #DPWS_ATTACHMENT_MODULE
	//	 * @see #DPWS_PRESENTATION_MODULE
	//	 */
	//	public static boolean hasModule(int module) {
	//		switch (module) {
	//			case (DPWS_CLIENT_MODULE): {
	//				return HAVE_DPWS_CLIENT_MODULE;
	//			}
	//			case (DPWS_SERVICE_MODULE): {
	//				return HAVE_DPWS_SERVICE_MODULE;
	//			}
	//			case (DPWS_EVENTING_MODULE): {
	//				return HAVE_DPWS_EVENTING_MODULE;
	//			}
	//			case (DPWS_SE_PLATFORM_MODULE): {
	//				return HAVE_DPWS_SE_PLATFORM_MODULE;
	//			}
	//			case (DPWS_CLDC_PLATFORM_MODULE): {
	//				return HAVE_DPWS_CLDC_PLATFORM_MODULE;
	//			}
	//			case (DPWS_ATTACHMENT_MODULE): {
	//				return HAVE_DPWS_ATTACHMENT_MODULE;
	//			}
	//			case (DPWS_SECURITY_MODULE): {
	//				return HAVE_DPWS_SECURITY_MODULE;
	//			}
	//			case (DPWS_PRESENTATION_MODULE): {
	//				return HAVE_DPWS_PRESENTATION_MODULE;
	//			}
	//		}
	//		return false;
	//	}

	//	/**
	//	 * Allows to verify whether some modules are loaded or not.
	//	 * <p>
	//	 * This method allows to check several modules with one method. If you want
	//	 * to check only one module, see the {@link #hasModule(int)} method.
	//	 * <p>
	//	 * You can check the modules listed below.
	//	 * </p>
	//	 * 
	//	 * @param config the modules to check.
	//	 *            <p>
	//	 *            To check more than one module, sum up their values.<br />
	//	 *            e.g. DPWS_CLIENT_MODULE+DPWS_SERVICE_MODULE
	//	 *            </p>
	//	 * @return returns <code>true</code> if all given modules have been loaded,
	//	 *         <code>false</code> otherwise.
	//	 * @see #DPWS_CLIENT_MODULE
	//	 * @see #DPWS_SERVICE_MODULE
	//	 * @see #DPWS_EVENTING_MODULE
	//	 * @see #DPWS_ATTACHMENT_MODULE
	//	 * @see #DPWS_SE_PLATFORM_MODULE
	//	 * @see #DPWS_CLDC_PLATFORM_MODULE
	//	 */
	//	public static boolean supportsConfiguration(int config) {
	//		if ((config & DPWS_CLIENT_MODULE) != 0 && !HAVE_DPWS_CLIENT_MODULE) {
	//			return false;
	//		}
	//		if ((config & DPWS_SERVICE_MODULE) != 0 && !HAVE_DPWS_SERVICE_MODULE) {
	//			return false;
	//		}
	//		if ((config & DPWS_EVENTING_MODULE) != 0 && !HAVE_DPWS_EVENTING_MODULE) {
	//			return false;
	//		}
	//		if ((config & DPWS_SE_PLATFORM_MODULE) != 0 && !HAVE_DPWS_SE_PLATFORM_MODULE) {
	//			return false;
	//		}
	//		if ((config & DPWS_CLDC_PLATFORM_MODULE) != 0 && !HAVE_DPWS_CLDC_PLATFORM_MODULE) {
	//			return false;
	//		}
	//		return true;
	//	}

	/**
	 * Returns the thread pool used by the framework.
	 * <p>
	 * This thread pool is necessary for thread handling, because CLDC does not
	 * have an own thread pool. All threads created by the framework are created
	 * with this thread pool.
	 * </p>
	 * 
	 * @return the thread pool.
	 */
	//	public static ThreadPool getThreadPool() {
	//		return threadpool;
	//	}

	/**
	 * Returns an instance of a communication manager based on the given
	 * identifier.
	 * <p>
	 * The communication manager is a special layer for communication. It allows
	 * the handling of incoming messages for different technologies. e.g. DPWS.
	 * </p>
	 * 
	 * @param identifier the identifier to receive a
	 *            {@link CommunicationManager}.
	 * @return {@link CommunicationManager}
	 */
	//	public static CommunicationManager getCommunicationManager(String identifier) {
	//		if (!running) {
	//			throw new WS4DIllegalStateException("Framework not started correctly or not running.");
	//		}
	//		return CommunicationManagerRegistry.getManager(identifier);
	//	}

	/**
	 * Returns an instance of a communication manager based on the given
	 * identifier.
	 * <p>
	 * The communication manager is a special layer for communication. It allows
	 * the handling of incoming messages for different technologies. e.g. DPWS.
	 * </p>
	 * 
	 * @param identifier the identifier to receive a
	 *            {@link CommunicationManager}.
	 * @return {@link CommunicationManager}
	 */
	//	public static CommunicationManager getCommunicationManager(CommunicationManagerID identifier) {
	//		if (!running) {
	//			throw new WS4DIllegalStateException("Framework not started correctly or not running.");
	//		}
	//		return CommunicationManagerRegistry.getManager(identifier);
	//	}

	/**
	 * Returns an input stream which allows to read a resource from the given
	 * location.
	 * <p>
	 * The location is a URL. The loaded communication managers can be
	 * registered for different URL schemas. This allows the loading of
	 * resources from different locations.
	 * </p>
	 * 
	 * @param location the location of the resource (e.g.
	 *            http://example.org/test.wsdl).
	 * @return an input stream for the given resource. Returns <code>null</code>
	 *         if no communication manager could find a resource at the given
	 *         location.
	 * @throws IOException throws an exception when the resource could not be
	 *             loaded properly.
	 */
	//	public static InputStream getResourceAsStream(URI location) throws IOException {
	//		/*
	//		 * We can load any file from file system or resource before the
	//		 * framework is up and running
	//		 */
	//		if (location == null) throw new IOException("What?! Cannot find 'null' file. Maybe /dev/null took it.");
	//		if (location.getSchema().startsWith(FrameworkConstants.SCHEMA_LOCAL)) {
	//			String file = location.toString().substring(FrameworkConstants.SCHEMA_LOCAL.length() + 1);
	//			InputStream in = location.getClass().getResourceAsStream(file);
	//			if (in == null) {
	//				try {
	//					FileSystem fs = getLocalFileSystem();
	//					return fs.readFile(file);
	//				} catch (IOException e) {
	//					return null;
	//				}
	//			}
	//			return in;
	//		}
	//		if (location.getSchema().startsWith("file")) {
	//			try {
	//				FileSystem fs = getLocalFileSystem();
	//				return fs.readFile(location.getPath());
	//			} catch (IOException e) {
	//				return null;
	//			}
	//		}
	//		/*
	//		 * This part should be done only if every thing is up and running
	//		 */
	//		if (running) {
	//			for (org.ws4d.java.structures.Iterator it = CommunicationManagerRegistry.getLoadedManagers(); it.hasNext();) {
	//				CommunicationManager manager = (CommunicationManager) it.next();
	//				InputStream in = manager.getResourceAsStream(location);
	//				if (in != null) {
	//					return in;
	//				}
	//			}
	//		} else {
	//			Log.warn("Framework could not load the given location before everything is up and running.");
	//		}
	//		// no communication manager capable of serving this request, sorry :'(
	//		return null;
	//	}

	/**
	 * Get the Toolkit of this framework.
	 * <p>
	 * The retrieved toolkit includes Java Edition specific utility methods.
	 * Framework must have already been started.
	 * </p>
	 * 
	 * @return The specific toolkit used by the framework.
	 */
	//	public static Toolkit getToolkit() {
	//		return toolkit;
	//	}

	//	/**
	//	 * Returns the class which allows to create a presentation URL.
	//	 * 
	//	 * @return the presentation creator.
	//	 */
	//	public static Presentation getPresentation() {
	//		if (hasModule(DPWS_PRESENTATION_MODULE)) {
	//			try {
	//				Class clazz = Class.forName("org.ws4d.java.presentation.DeviceServicePresentation");
	//				presentation = (Presentation) clazz.newInstance();
	//			} catch (Exception e) {
	//				Log.error(e.getMessage());
	//				Log.printStackTrace(e);
	//			}
	//		}
	//		return presentation;
	//	}

	//	/**
	//	 * Set the factory for stream monitoring.
	//	 * <p>
	//	 * This enables the monitoring of streams for debug purposes. A
	//	 * <code>MonitorStreamFactory</code> wraps streams to redistribute data. A
	//	 * communication manager can use the factory to redistribute data to the
	//	 * streams created by the factory.
	//	 * </p>
	//	 * 
	//	 * @param factory the factory which wraps streams and redistribute data.
	//	 */
	//	public static void setMonitorStreamFactory(MonitorStreamFactory factory) {
	//		monitorFactory = factory;
	//	}
	//
	//	/**
	//	 * Returns the <code>MonitorStreamFactory</code> which allows to wrap
	//	 * streams and redistribute data.
	//	 * 
	//	 * @return the factory to wrap streams and redistribute data.
	//	 * @see #setMonitorStreamFactory(MonitorStreamFactory)
	//	 */
	//	public static MonitorStreamFactory getMonitorStreamFactory() {
	//		return monitorFactory;
	//	}

	/**
	 * Returns an implementation of the file system supported by the given
	 * platform.
	 * <p>
	 * It is necessary to load the corresponding module for platform support.
	 * </p>
	 * 
	 * @return an implementation of the file system.
	 * @throws IOException will throw an exception when the module could not be
	 *             loaded correctly or the runtime configuration does not
	 *             support a local file system.
	 * @see #DPWS_SE_PLATFORM_MODULE
	 * @see #DPWS_CLDC_PLATFORM_MODULE
	 */
	//	public static FileSystem getLocalFileSystem() throws IOException {
	//		synchronized (LOCAL_FILE_SYSTEM_LOCK) {
	//			if (localFileSystem == null) {
	//				if (hasModule(DPWS_SE_PLATFORM_MODULE)) {
	//					try {
	//						Class clazz = Class.forName("org.ws4d.java.platform.io.fs.SEFileSystem");
	//						localFileSystem = (FileSystem) clazz.newInstance();
	//					} catch (Exception e) {
	//						throw new IOException(e.toString());
	//					}
	//				} else {
	//					throw new IOException("The current runtime configuration doesn't contain support for a local file system.");
	//				}
	//			}
	//			return localFileSystem;
	//		}
	//	}
	//
	//	public static DPWSSecurityManager getSecurityManager() {
	//		return securityManager;
	//	}

	public static void setPropertiesPath(String path) {
		propertiesPath = path;
	}


	private static void createPresentation()
	{
		if (FrameworkModuleRegistry.getInstance().hasModule(PresentationModule.class)) {
			try {
				Class<?> clazz = Class.forName("org.ws4d.java.presentation.DeviceServicePresentation");
				Presentation presentation = (Presentation) clazz.newInstance();
				PresentationProvider.getInstance().setPresentation(presentation);
			} catch (Exception e) {
				Log.error(e);
			}
		}
	}


	private static void createToolkit() {

		try{
			Toolkit toolkit=null;
			Class<?> result;

			Log.info("Trying to find toolkit for platform");

			String toolKitClz=System.getProperty("MDPWS.ToolkitClass");
			if (toolKitClz==null)
			{

				if ((result=checkClassExistence("org.ws4d.java.platform.util.SEToolkit"))!=null)
					toolkit=(Toolkit) result.newInstance();
				else if ((result=checkClassExistence("org.ws4d.java.platform.util.CLDCToolkit"))!=null)
					toolkit=(Toolkit) result.newInstance();
			}else{
				if ((result=checkClassExistence(toolKitClz))!=null)
					toolkit=(Toolkit) result.newInstance();
			}

			if (toolkit!=null)
			{
				PlatformSupport.getInstance().setToolkit(toolkit);
				PlatformSupport.getInstance().getToolkit().init(FrameworkProperties.getInstance().getThreadPoolSize());
			}else{
				throw new IllegalStateException("No Toolkit registered");
			}
		}catch(Exception e){
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Hidden default constructor.
	 */
	private DPWSFramework() {
		super();
	}

	protected static class ShutdownCallback implements FrameworkShutdownCallback
	{

		@Override
		public void frameworkToBeKilled() {
			kill();
		}

		@Override
		public void frameworkToBeStopped() {
			stop();
		}
	}

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.client;

import org.ws4d.java.message.discovery.DiscoveryMessage;
import org.ws4d.java.types.ProbeScopeSet;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.ScopeSet;
import org.ws4d.java.util.StringUtil;

/**
 * A collection of search criteria used when searching for devices or services.
 * 
 * @see SearchManager
 */
public class SearchParameter {

	private SearchMap		searchMap;

	/** */
	private QNameSet		deviceTypes		= null;

	/** */
	private ProbeScopeSet	scopes			= null;

	/** */
	private QNameSet		serviceTypes	= null;

	/** */
	private Object			referenceObject	= null;

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(StringUtil.formatClassName(getClass()));
		sb.append(" [ deviceTypes=").append(deviceTypes);
		sb.append(", scopes=").append(scopes);
		sb.append(", serviceTypes=").append(serviceTypes);
		sb.append(", searchMap=").append(searchMap);
		sb.append(" ]");
		return sb.toString();
	}

	/**
	 * Returns the search map to use when discovering devices and services.
	 * 
	 * @return the search map for the search process
	 */
	public SearchMap getSearchMap() {
		return searchMap;
	}

	/**
	 * Sets the search map for the discovery process.
	 * 
	 * @param searchMap the search map to use, if <code>null</code>, a default
	 *            search map will be used
	 */
	public void setSearchMap(SearchMap searchMap) {
		this.searchMap = searchMap;
	}

	/**
	 * Gets device port types of device to discover.
	 * 
	 * @return device port type.
	 */
	public QNameSet getDeviceTypes() {
		return deviceTypes;
	}

	/**
	 * Sets device port types of device to discover.
	 * 
	 * @param deviceTypes device port types.
	 */
	public void setDeviceTypes(QNameSet deviceTypes) {
		this.deviceTypes = deviceTypes;
	}

	/**
	 * Gets list of scopes of device to discover.
	 * 
	 * @return list of scopes.
	 */
	public ProbeScopeSet getScopes() {
		return scopes;
	}

	/**
	 * Sets list of scopes of device to discover.
	 * 
	 * @param scopes list of scopes.
	 */
	public void setScopes(ProbeScopeSet scopes) {
		this.scopes = scopes;
	}

	/**
	 * Gets service port types of service to discover.
	 * 
	 * @return service port types.
	 */
	public QNameSet getServiceTypes() {
		return serviceTypes;
	}

	/**
	 * Sets service port types of service to discover. If no device filters are
	 * set, all devices are discovered. Later on the discovered services all
	 * filtered.
	 * 
	 * @param serviceTypes service port types.
	 */
	public void setServiceTypes(QNameSet serviceTypes) {
		this.serviceTypes = serviceTypes;
	}

	/**
	 * Gets reference object.
	 * 
	 * @return reference object.
	 */
	public Object getReferenceObject() {
		return referenceObject;
	}

	/**
	 * Sets reference object. The reference object can include data, which is
	 * important for the further handling of the discovered devices or services.
	 * 
	 * @param referenceObject
	 */
	public void setReferenceObject(Object referenceObject) {
		this.referenceObject = referenceObject;
	}

	/**
	 * Checks if the device sending the discovery message matches the searched
	 * device port types and scopes, which are part of the searchParameter. To
	 * match the device both the port types and the scopes must be part of the
	 * device.
	 * 
	 * @param searchParameter SearchParameter containing port types and scopes.
	 * @param message Discovery message of device.
	 * @return <code>true</code> - if both the given device port types and
	 *         scopes are part of the device.
	 */
	protected boolean matchesSearch(DiscoveryMessage message) {
		QNameSet msgDeviceTypes = message.getTypes();
		if (deviceTypes == null || deviceTypes.isEmpty() || msgDeviceTypes.containsAll(deviceTypes)) {
			// check scopes
			if (scopes != null && !scopes.isEmpty()) {
				ScopeSet msgScopes = message.getScopes();
				if (msgScopes == null || msgScopes.isEmpty() || !msgScopes.containsAll(scopes)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.client;

import java.io.IOException;

import org.ws4d.java.DPWSFramework;
import org.ws4d.java.communication.CommunicationBinding;
import org.ws4d.java.communication.CommunicationManager;
import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.communication.DefaultIncomingMessageListener;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.concurrency.LockSupport;
import org.ws4d.java.concurrency.Lockable;
import org.ws4d.java.constants.DPWSMessageConstants;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.dispatch.HelloData;
import org.ws4d.java.dispatch.ServiceReferenceEventRegistry;
import org.ws4d.java.eventing.ClientSubscription;
import org.ws4d.java.eventing.DefaultEventSink;
import org.ws4d.java.eventing.EventListener;
import org.ws4d.java.eventing.EventSink;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.reference.DeviceListener;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceListener;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.WS4DIllegalStateException;

/**
 * Default DPWS Client implementation. This class provides easy access to
 * several points of interaction within the DPWS framework, such as searching
 * for devices/services, tracking a device's or service's state changes and
 * receiving events from subscribed services.
 * <p>
 * The basic idea behind this class is: it extends several callback interfaces
 * and provides empty implementations for all of their methods, so that an
 * implementing client can easily overwrite those in which it is really
 * interested in.
 * </p>
 * <p>
 * A simple use case of this class could be a client searching for a particular
 * device. This can be accomplished by a call to
 * {@link #searchDevice(SearchParameter)} providing the desired search criteria
 * within the expected <code>SearchParameter</code> argument. The framework will
 * then start looking asynchronously for devices matching those criteria and
 * will invoke {@link #deviceFound(DeviceReference, SearchParameter)} each time
 * a corresponding device is discovered.<br />
 * Searching for services can be done in a similar manner, this time using the
 * method {@link #searchService(SearchParameter)} to initiate the search and
 * receiving results by means of
 * {@link #serviceFound(ServiceReference, SearchParameter)}.
 * </p>
 * <p>
 * When a client starts a {@link #searchDevice(SearchParameter) device search},
 * it is automatically registered as {@link DeviceListener} to any device
 * matching the search criteria. This is especially useful for getting
 * notifications about state changes of the device, such as a
 * {@link #deviceBye(DeviceReference) device shut-down}, an
 * {@link #deviceChanged(DeviceReference) update of a device's metadata}, etc.
 * </p>
 * <p>
 * Listening to service state changes differs from the aforementioned approach.
 * In order to start receiving service update notifications, a client must
 * {@link #registerServiceListening() register} itself for that purpose. It will
 * then be notified about any state change regarding <em>every</em> service the
 * DPWS framework knows about. This also includes any services not explicitly
 * {@link #searchService(SearchParameter) searched for} by this client.
 * </p>
 * <p>
 * A simple client implementation interested in devices providing the
 * <code>ex:Printer</code> port type (where <code>ex</code> is a XML namespace
 * prefix referring to the <code>http://www.example.org/printing</code>
 * namespace) could look like:
 * 
 * <pre>
 * // create a new client
 * Client client = new DefaultClient() {
 * 
 *     // overwrite deviceFound method in order to receive callbacks
 *     public void deviceFound(DeviceReference devRef, SearchParameter search) {
 *         // start interacting with matching device
 *         ...
 *     }
 * 
 * };
 * // describe device port type to look for
 * QName printerType = QNameFactory.getInstance().getQName(&quot;Printer&quot;, &quot;http://www.example.org/printlng&quot;);
 * QNameSet types = new QNameSet(printerType);
 * 
 * // create a search parameter object and store desired type(s) into it
 * SearchParameter criteria = new SearchParameter();
 * criteria.setDeviceTypes(types);
 * 
 * // start the asynchronous search
 * client.searchDevice(criteria);
 * </pre>
 * 
 * </p>
 */
public class DefaultClient implements DeviceListener, ServiceListener, SearchCallback, EventListener, HelloListener {

	private final static int[]	INCOMING_MESSAGE_TYPES	= { DPWSMessageConstants.HELLO_MESSAGE };

	HelloReceiver				helloReceiver			= null;

	/** Map: {@link SearchParameter} -> {@link HelloReceiver} */
	HashMap						helloReceivers			= null;

	private final Lockable		lockSupport				= new LockSupport();

	/**
	 * Default constructor. Ensures the DPWS framework is running (see
	 * {@link DPWSFramework#isRunning()}. Throws a
	 * <code>java.lang.RuntimeException</code> if this is not the case.
	 * 
	 * @throws RuntimeException if the DPWS framework is not running; i.e. it
	 *             was either not started by means of
	 *             {@link DPWSFramework#start(String[])} or has already been
	 *             stopped via {@link DPWSFramework#frameworkToBeStopped()} before calling this
	 *             constructor
	 */
	public DefaultClient() {
		super();
		if (!FrameworkModuleRegistry.getInstance().isRunning()) {
			throw new RuntimeException("Client Constructor: DPWSFramework isn't running!");
		}
	}

	public DataStructure getAllDiscoveryBindings() {
		lockSupport.exclusiveLock();
		DataStructure bindings = new ArrayList();
		try {
			for (Iterator it = CommunicationManagerRegistry.getLoadedManagers(); it.hasNext();) {
				CommunicationManager manager = (CommunicationManager) it.next();
				try {
					bindings.addAll(manager.getDiscoveryBindings());
				} catch (IOException e) {
					Log.info(e);
				}
			}
		} finally {
			lockSupport.releaseExclusiveLock();
		}
		return bindings;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.eventing.EventListener#getEventSink(org.ws4d.java.structures
	 * .DataStructure)
	 */
	@Override
	public EventSink getEventSink(DataStructure bindings) {
		return new DefaultEventSink(this, bindings);
	}

	/**
	 * Generates an event sink which can be used when registering for event
	 * notifications from a service. The supplied configuration id is supposed
	 * to refer to an EventSink property which contains at least one binding
	 * property to create a {@link CommunicationBinding} instance. This
	 * {@link CommunicationBinding} instance defines a local transport address,
	 * to which incoming notifications will be delivered.
	 * 
	 * @param configurationId Configuration id of the properties of the event
	 *            sink to generate
	 * @return a new event sink
	 */
	public EventSink generateEventSink(int configurationId) {
		return new DefaultEventSink(this, configurationId);
	}

	// --------------------- DEVICE STATE ----------------------

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceListener#deviceBye(org.ws4d.java
	 * .service.reference.DeviceReference)
	 */
	@Override
	public void deviceBye(DeviceReference deviceRef) {
		Log.info("Client: Overwrite deviceBye() to receive device status changes");
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceListener#deviceChanged(org.ws4d
	 * .java.service.reference.DeviceReference)
	 */
	@Override
	public void deviceChanged(DeviceReference deviceRef) {
		Log.info("Client: Overwrite deviceChanged() to receive device status changes");
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceListener#deviceRunning(org.ws4d
	 * .java.service.reference.DeviceReference)
	 */
	@Override
	public void deviceRunning(DeviceReference deviceRef) {
		Log.info("Client: Overwrite deviceRunning() to receive device status changes");
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceListener#deviceObjAvailable(org
	 * .ws4d.java.service.reference.DeviceReference)
	 */
	@Override
	public void deviceBuildUp(DeviceReference deviceRef) {
		if (Log.isInfo())
			Log.info("Client: Overwrite deviceBuildUp() to receive device status changes");
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.DeviceListener#deviceNotResponding(org
	 * .ws4d.java.service.reference.DeviceReference)
	 */
	@Override
	public void deviceCommunicationErrorOrReset(DeviceReference deviceRef) {
		if (Log.isInfo())
			Log.info("Client: Overwrite deviceCommunicationErrorOrReset() to receive device status changes");
	}

	// --------------------- SERVICE CHANGE LISTENING ------------------------

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.ServiceListener#onServiceChanged(org.
	 * ws4d.java.service.reference.ServiceReference)
	 */
	@Override
	public void serviceChanged(ServiceReference serviceRef) {
		if (Log.isInfo())
			Log.info("Client: Overwrite serviceChanged() to receive service status changes");
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.ServiceListener#onServiceCreated(org.
	 * ws4d.java.service.reference.ServiceReference)
	 */
	@Override
	public void serviceCreated(ServiceReference serviceRef) {
		if (Log.isInfo())
			Log.info("Client: Overwrite serviceCreated() to receive service status changes");
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.reference.ServiceListener#onServiceDisposed(org
	 * .ws4d.java.service.reference.ServiceReference)
	 */
	@Override
	public void serviceDisposed(ServiceReference serviceRef) {
		if (Log.isInfo())
			Log.info("Client: Overwrite serviceDisposed() to receive service status changes");
	}

	/**
	 * Registers client for service reference changes. Client gets information
	 * about service changes.
	 * 
	 * @see ServiceListener
	 */
	public void registerServiceListening() {
		ServiceReferenceEventRegistry.getInstance().registerServiceListening(this);
	}

	/**
	 * Unregisters service listening for this service client. This method should
	 * be called, if holder of reference is no longer interested in this
	 * reference.
	 * 
	 * @see ServiceListener
	 */
	public void unregisterServiceListening() {
		ServiceReferenceEventRegistry.getInstance().unregisterServiceListening(this);
	}

	// ---------------------- DISCOVERY ------------------------

	/**
	 * Gets device reference of device with specified endpoint reference. This
	 * <code>Client</code> instance will be used as callback for device changes
	 * of the corresponding device.
	 * 
	 * @param deviceEpr endpoint reference of device to get device reference for
	 * @return device reference
	 * @see SearchManager#getDeviceReference(EndpointReference, DeviceListener)
	 */
	public DeviceReference getDeviceReference(EndpointReference deviceEpr, CommunicationBinding binding) {
		/*
		 * we won't send resolve messages, let it be done by user with
		 * devRef.getDevice()
		 */
		return SearchManager.getDeviceReference(deviceEpr, this, binding);
	}

	public DeviceReference getDeviceReference(EndpointReference deviceEpr) {
		/*
		 * we won't send resolve messages, let it be done by user with
		 * devRef.getDevice()
		 */
		return SearchManager.getDeviceReference(deviceEpr, this, null);
	}

	/**
	 * Gets device reference of device which sent the specified hello data. This
	 * <code>Client</code> instance will be used as callback for device changes
	 * of the corresponding device.
	 * 
	 * @param helloData Hello data received from
	 *            {@link #helloReceived(HelloData)} callback method.
	 * @return device reference
	 * @see SearchManager#getDeviceReference(HelloData, DeviceListener)
	 */
	public DeviceReference getDeviceReference(HelloData helloData) {
		/*
		 * we won't send resolve messages, let it be done by user with
		 * devRef.getDevice()
		 */
		return SearchManager.getDeviceReference(helloData, this);
	}

	/**
	 * Gets service reference of service with specified endpoint reference.
	 * 
	 * @param serviceEpr endpoint reference of service to get service reference
	 *            for
	 * @return service reference
	 * @see SearchManager#getServiceReference(EndpointReference)
	 */
	public ServiceReference getServiceReference(EndpointReference serviceEpr) {
		/*
		 * we won't send GetMetadata messages, let it be done by user with
		 * servRef.getService()
		 */
		return SearchManager.getServiceReference(serviceEpr);
	}

	/**
	 * Shorthand method for searching devices. Expect search results
	 * asynchronously within this client instance's
	 * {@link #deviceFound(DeviceReference, SearchParameter)} method.
	 * 
	 * @param search search criteria
	 * @see SearchManager#searchDevice(SearchParameter, SearchCallback,
	 *      DeviceListener)
	 */
	public void searchDevice(SearchParameter search) {
		SearchManager.searchDevice(search, this, this);
	}

	/**
	 * Searches for services. Uses search parameter to specify the search.
	 * Obtained results will be delivered asynchronously to this client
	 * instance's {@link #serviceFound(ServiceReference, SearchParameter)}
	 * method.
	 * 
	 * @param search search parameter to specify the search for device and
	 *            service
	 * @see SearchManager#searchService(SearchParameter, SearchCallback)
	 */
	public void searchService(SearchParameter search) {
		SearchManager.searchService(search, this);
	}

	// TODO: Oldenburg, registerAllDiscovery

	/**
	 * Registers for incoming HelloMessages for all possible domains.
	 * <p>
	 * This method will check every {@link CommunicationManager} registered
	 * inside the framework and registers all discovery domains found with
	 * {@link #registerHelloListening(CommunicationBinding)}.
	 * </p>
	 */
	public synchronized void registerHelloListening() {
		try {
			Iterator mans = CommunicationManagerRegistry.getLoadedManagers();
			while (mans.hasNext()) {
				CommunicationManager manager = (CommunicationManager) mans.next();
				DataStructure bindings = manager.getDiscoveryBindings();
				if (bindings != null) {
					Iterator binds = bindings.iterator();
					while (binds.hasNext()) {
						CommunicationBinding binding = (CommunicationBinding) binds.next();
						registerHelloListening(binding);
					}
				}
			}
		} catch (IOException e) {
			Log.error("Cannot register for incoming wsd:Hello messages. " + e.getMessage());
		}

	}

	/**
	 * Registers for incoming HelloMessages.
	 */
	public synchronized void registerHelloListening(CommunicationBinding binding) {
		if (helloReceiver == null) {
			helloReceiver = new HelloReceiver(this);
		}

		try {
			CommunicationManager manager = CommunicationManagerRegistry.getManager(binding.getCommunicationManagerId());
			if (manager!=null)
				manager.registerDiscovery(INCOMING_MESSAGE_TYPES, binding, helloReceiver);

		} catch (WS4DIllegalStateException e) {
			throw new RuntimeException(e.getMessage());
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * Registers for incoming HelloMessages, which matches to the specified
	 * {@link SearchParameter}. {@link #helloReceived(HelloData)} is called to
	 * deliver the hello data.
	 * 
	 * @param search SearchParameter which has to match incoming hello messages.
	 *            This parameter is also used for unregistering the hello
	 *            receiving.
	 * @param binding
	 */
	public void registerHelloListening(final SearchParameter search, CommunicationBinding binding) {
		registerHelloListening(search, this, binding);
	}

	/**
	 * Registers for incoming HelloMessages, which matches to the specified
	 * SearchParameter. {@link #helloReceived(HelloData)} is called to deliver
	 * the hello data.
	 * 
	 * @param search SearchParameter which has to match incoming hello messages.
	 *            This parameter is used for unregistering the hello receiving,
	 *            too.
	 * @param helloListener Hello listener to receive the hello data from
	 *            matching hellos.
	 */
	public synchronized void registerHelloListening(final SearchParameter search, HelloListener helloListener, CommunicationBinding binding) {
		if (helloReceivers == null) {
			helloReceivers = new HashMap(3);
		} else if (helloReceivers.containsKey(search)) {
			return;
		}

		HelloReceiver helloReceiver = new HelloReceiver(helloListener == null ? this : helloListener);
		helloReceivers.put(search, helloReceiver);

		try {
			CommunicationManager manager = CommunicationManagerRegistry.getManager(binding.getCommunicationManagerId());
			manager.registerDiscovery(INCOMING_MESSAGE_TYPES, binding, helloReceiver);
		} catch (WS4DIllegalStateException e) {
			throw new RuntimeException(e.getMessage());
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * Unregisters for incoming HelloMessages
	 */
	public synchronized void unregisterHelloListening(CommunicationBinding binding) {
		if (helloReceiver != null) {
			try {
				if (binding!=null)
				{
					CommunicationManager manager = CommunicationManagerRegistry.getManager(binding.getCommunicationManagerId());
					manager.unregisterDiscovery(INCOMING_MESSAGE_TYPES, binding, helloReceiver);
				}

			} catch (WS4DIllegalStateException e) {
				throw new RuntimeException(e.getMessage());
			} catch (IOException e) {
				throw new RuntimeException(e.getMessage());
			}
		}
	}

	public synchronized void unregisterHelloListening() {
		try {
			Iterator mans = CommunicationManagerRegistry.getLoadedManagers();
			while (mans.hasNext()) {
				CommunicationManager manager = (CommunicationManager) mans.next();
				DataStructure bindings = manager.getDiscoveryBindings();
				if (bindings != null) {
					Iterator binds = bindings.iterator();
					while (binds.hasNext()) {
						CommunicationBinding binding = (CommunicationBinding) binds.next();
						unregisterHelloListening(binding);
					}
				}
			}
		} catch (IOException e) {
			Log.error("Cannot register for incoming wsd:Hello messages. " + e.getMessage());
		}

	}

	/**
	 * Unregisters for incoming hello messages, which should have matched the
	 * specified {@link SearchParameter}.
	 * 
	 * @param search The search parameter used to register for hello messages.
	 */
	public synchronized void unregisterHelloListening(SearchParameter search, CommunicationBinding binding) {
		if (helloReceivers != null) {
			HelloReceiver helloReceiver = (HelloReceiver) helloReceivers.remove(search);

			if (helloReceiver == null) return;

			try {
				CommunicationManager manager = CommunicationManagerRegistry.getManager(binding.getCommunicationManagerId());

				manager.unregisterDiscovery(INCOMING_MESSAGE_TYPES, binding, helloReceiver);

			} catch (WS4DIllegalStateException e) {
				throw new RuntimeException(e.getMessage());
			} catch (IOException e) {
				throw new RuntimeException(e.getMessage());
			}
		}
	}

	/**
	 * @param helloData
	 */
	@Override
	public void helloReceived(HelloData helloData) {
		if (Log.isInfo())
			Log.info("Client: Overwrite helloReceived() to receive and handle the UUIDs of new HelloMessages");
	}

	// --------------------- SEARCH CALLBACKS ------------------

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.client.SearchCallback#onDeviceFound(org.ws4d.java.service
	 * .reference.DeviceReference, org.ws4d.java.client.SearchParameter)
	 */
	@Override
	public void deviceFound(DeviceReference devRef, SearchParameter search) {
		if (Log.isInfo())
			Log.info("Client: Overwrite deviceFound() to receive device discovery responses");
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.client.SearchCallback#onServiceFound(org.ws4d.java.service
	 * .reference.ServiceReference, org.ws4d.java.client.SearchParameter)
	 */
	@Override
	public void serviceFound(ServiceReference servRef, SearchParameter search) {
		if (Log.isInfo())
			Log.info("Client: Overwrite serviceFound() to receive service discovery responses");
	}

	// --------------------- EVENT CALLBACKS ---------------------

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.client.EventListener#receiveEvent(org.ws4d.java.eventing
	 * .ClientSubscription, org.ws4d.java.types.uri.URI,
	 * org.ws4d.java.service.ParameterValue)
	 */
	@Override
	public IParameterValue eventReceived(ClientSubscription subscription, URI actionURI, IParameterValue parameterValue) {
		if (Log.isInfo())
			Log.info("Client: Overwrite eventReceived() to receive and handle events");
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.client.EventListener#receiveSubscriptionEnd(org.ws4d.java
	 * .eventing.ClientSubscription, org.ws4d.java.types.uri.URI)
	 */
	@Override
	public void subscriptionEndReceived(ClientSubscription subscription, URI reason) {
		if (Log.isInfo())
			Log.info("Client: Overwrite subscriptionEndReceived() to receive and handle end of subscriptions");
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.client.EventListener#receiveSubscriptionTimeout(org.ws4d
	 * .java.eventing.ClientSubscription)
	 */
	@Override
	public void subscriptionTimeoutReceived(ClientSubscription subscription) {
		if (Log.isInfo())
			Log.info("Client: Overwrite subscriptionTimeoutReceived() to receive and handle subscription timeouts");
	}

	private final class HelloReceiver extends DefaultIncomingMessageListener {

		//Re-Modularization 2011-01-21 Move to DefaultIncomingMessageListener/IncomingUDPReceiver
		//private final MessageIdBuffer	messageIdBuffer	= new MessageIdBuffer();

		private final AppSequenceBuffer appSequenceBuffer = new AppSequenceBuffer();

		private final HelloListener		helloListener;

		public HelloReceiver(HelloListener helloListener) {
			this.helloListener = helloListener;
		}

		@Override
		public void handle(HelloMessage hello, ProtocolData protocolData) {
			if (getMessageIdBuffer().containsOrEnqueue(hello.getMessageId())) {
				if (Log.isDebug())
					Log.debug("Discarding hello message message! Already saw this one!");
				return;
			}

			if (appSequenceBuffer.checkAndUpdate(hello.getEndpointReference(), hello.getAppSequence())) {
				helloListener.helloReceived(new HelloData(hello, protocolData));
			}
			else if (Log.isDebug()) {
				Log.debug("Discarding hello message message! Old AppSequence!");
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.client.SearchCallback#noDevicesFound(org.ws4d.java.client.SearchParameter)
	 */
	@Override
	public void noDevicesFound(SearchParameter search) {
		//	void
	}

}

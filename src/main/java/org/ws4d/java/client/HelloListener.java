/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.client;

import org.ws4d.java.dispatch.HelloData;

/**
 * Implementation of this interface is used to receive {@link HelloData} from
 * hello messages. Registration for receiving such hello message is done by the
 * {@link DefaultClient#registerHelloListening(SearchParameter, HelloListener)}.
 */
public interface HelloListener {

	/**
	 * This method is called, if matching hello was received.
	 * 
	 * @param helloData Hello data object.
	 */
	public void helloReceived(HelloData helloData);

}

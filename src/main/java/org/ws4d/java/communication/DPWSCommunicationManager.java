/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import org.ws4d.java.communication.DPWS2006.DefaultDPWSUtil2006;
import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.NetworkDetection;
import org.ws4d.java.communication.connection.ip.NetworkInterface;
import org.ws4d.java.communication.protocol.http.HTTPClient;
import org.ws4d.java.communication.protocol.http.HTTPClientDestination;
import org.ws4d.java.communication.protocol.http.HTTPRequest;
import org.ws4d.java.communication.protocol.http.HTTPRequestUtil;
import org.ws4d.java.communication.protocol.http.server.DefaultHTTPResourceHandler;
import org.ws4d.java.communication.protocol.http.server.HTTPServer;
import org.ws4d.java.communication.protocol.mime.DefaultMIMEHandler;
import org.ws4d.java.communication.protocol.soap.SOAPRequest;
import org.ws4d.java.communication.protocol.soap.SOAPoverUDPClient;
import org.ws4d.java.communication.protocol.soap.SOAPoverUDPClient.SOAPoverUDPHandler;
import org.ws4d.java.communication.protocol.soap.generator.MessageReceiver;
import org.ws4d.java.communication.protocol.soap.generator.SOAPMessageGeneratorFactory;
import org.ws4d.java.communication.protocol.soap.generator.WSDLBindingBuilderRegistry;
import org.ws4d.java.communication.protocol.soap.server.SOAPServer;
import org.ws4d.java.communication.protocol.soap.server.SOAPServer.SOAPHandler;
import org.ws4d.java.communication.protocol.soap.server.SOAPoverUDPServer;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.configuration.MessageProperties;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.DPWSMessageConstants;
import org.ws4d.java.constants.HTTPConstants;
import org.ws4d.java.constants.WSDLConstants;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.dispatch.MessageInformer;
import org.ws4d.java.dispatch.ProtocolVersionInfoRegistry;
import org.ws4d.java.dispatch.RequestResponseCoordinator;
import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.LinkedSet;
import org.ws4d.java.structures.List;
import org.ws4d.java.structures.MessageIdBuffer;
import org.ws4d.java.structures.Set;
import org.ws4d.java.types.InternetMediaType;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.WS4DIllegalStateException;
import org.ws4d.java.wsdl.WSDL;
import org.ws4d.java.wsdl.WSDLBinding;
import org.ws4d.java.wsdl.WSDLPortType;
import org.ws4d.java.wsdl.soap12.SOAP12DocumentLiteralHTTPBinding;
import org.ws4d.java.wsdl.soap12.SOAP12DocumentLiteralHTTPBindingBuilder;


/**
 *
 */
public class DPWSCommunicationManager implements CommunicationManager {

	private static final int					MAX_UDP_PORT_RETRIES			= 3;

	private static final Random					RND								= new Random();

	private static final MessageReceiver		GENERIC_RECEIVER				= new GenericReceiver();

	private static final MessageInformer		MESSAGE_INFORMER				= MessageInformer.getInstance();

	private volatile boolean					stopped							= true;

	// key = DPWSDomain, value = SOAPoverUDPClient
	private final HashMap						udpClientsPerDomain				= new HashMap();

	// contains either the IPv4 or the IPv6 based multicast UDP server, or both
	private final HashMap						udpServers						= new HashMap();

	// key = host ':' port, value = SOAPServer
	private final HashMap						soapServers						= new HashMap();

	// key = host ':' port, value = HTTPServer
	private final HashMap						httpServers						= new HashMap();

	private final RequestResponseCoordinator	rrc								= RequestResponseCoordinator.getInstance();

	protected final MessageIdBuffer				sentMulticastIds				= new MessageIdBuffer();

	private final SOAPoverUDPHandler			udpResponseHandler				= new SOAPoverUDPHandler(new UDPResponseReceiver(rrc), sentMulticastIds);

	private static Set							registerForGetMetadata			= new HashSet();

	private static Set							registerForGet					= new HashSet();

	private static Set							MessageIDsForGetMetadataMapping	= new HashSet();

	private final Object						udpTransmissionsLock			= new Object();

	private volatile int						pendingUDPTransmissions			= 0;

	private final DefaultDPWSUtil2006			dpws2006Util					= DefaultDPWSUtil2006.getInstance();

	private DataStructure						domains							= getAvailableDomains();

	/**
	 * Methods for DPWS 2006 translating
	 */
	public static Set getRegisterForGet() {
		return registerForGet;
	}

	public static Set getRegisterForGetMetadata() {
		return registerForGetMetadata;
	}

	public static Set getMessageIDsForGetMetadataMapping() {
		return MessageIDsForGetMetadataMapping;
	}

	static long getRandomApplicationDelay() {
		int max = DPWSProperties.getInstance().getApplicationMaxDelay(); //Moved ApplicationMaxDelay from DisptachingProperties to DPWSProperties
		if (max <= 0) {
			return 0;
		}
		int delay = RND.nextInt();
		if (delay < 0) {
			delay = -delay;
		}
		return delay % (max + 1);
	}

	/**
	 * Public default constructor, needed for reflective instance creation (
	 * <code>Class.forName(...)</code>).
	 */
	public DPWSCommunicationManager() {
		super();
	}

	/**
	 * @return
	 */
	@Override
	public CommunicationManagerID getCommunicationManagerId() {
		return DPWSCommunicationManagerID.INSTANCE;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.CommunicationManager#getAvailableDomains()
	 */
	@Override
	public DataStructure getAvailableDomains() {


		if (domains == null) {
			domains = new ArrayList();
			for (Iterator it = PlatformSupport.getInstance().getToolkit().getNetworkDetection(NetworkDetection.IP_NETWORK).getNetworkInterfaces(); it.hasNext();) {
				NetworkInterface ni = (NetworkInterface) it.next();

				if (!ni.isUp()) {
					if (Log.isDebug()) {
						Log.debug("The interface " + ni.getName() + " is not up and running...");
					}
					continue;
				}

				if (!ni.supportsMulticast()) {
					if (Log.isWarn()) {
						Log.warn("The interface " + ni.getName() + " does not support multicast. No listener will be bound.");
					}
					continue;
				}

				for (Iterator it2 = ni.getAddresses(); it2.hasNext();) {
					IPAddress address = (IPAddress) it2.next();
					domains.add(createDomain(ni, address));
				}
			}
		}
		return domains;
	}

	/**
	 * @param address 
	 * @param ni 
	 * @return
	 */
	protected ProtocolDomain createDomain(NetworkInterface ni, IPAddress address) {
		return new DPWSDomain(ni, address, true, true);
	}

	public DataStructure refreshAvailableDomains() {
		this.domains = null;
		return getAvailableDomains();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.CommunicationManager#addressMatches(org.ws4d
	 * .java.data.uri.URI, boolean, org.ws4d.java.communication.ProtocolData)
	 */
	@Override
	public boolean addressMatches(URI xAddr, boolean source, ProtocolData protocolData) {
		if (!(protocolData instanceof DPWSProtocolData)) {
			return false;
		}
		DPWSProtocolData data = (DPWSProtocolData) protocolData;
		String otherAddr = getCanonicalAddress(source ? data.getSourceHost() : data.getDestinationHost());
		String hostAddr = getCanonicalAddress(xAddr.getHost());
		return otherAddr != null && otherAddr.equals(hostAddr);
	}

	@Override
	public void start() throws IOException {
		if (!stopped) {
			return;
		}
		stopped = false;

		HashSet dpwsVersions=DPWSProperties.getInstance().getSupportedDPWSVersions();
		if (dpwsVersions!=null && !dpwsVersions.isEmpty())
		{
			for (Iterator it=dpwsVersions.iterator();it.hasNext();)
			{
				ProtocolVersionInfoRegistry.getInstance().addDefaultSupportedProtocolVersionInfo((ProtocolVersionInfo) it.next());
			}
		}
		WSDLBindingBuilderRegistry.getInstance().addBindingBuilder(WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME, new SOAP12DocumentLiteralHTTPBindingBuilder());
		if (Log.isInfo()) {
			Log.info(DPWSProperties.getInstance().printSupportedDPWSVersions());
		}
	}

	@Override
	public DataStructure getDiscoveryBindings() throws IOException {
		DataStructure domains = getAvailableDomains();
		DataStructure ret = new LinkedSet();
		for (Iterator it = domains.iterator(); it.hasNext();) {
			ProtocolDomain domain = (ProtocolDomain) it.next();
			DiscoveryBinding binding=getDiscoveryBindingForDomain(domain);
			if (binding!=null)
				ret.add(binding);
		}
		return ret;
	}

	/**
	 * Unsupported
	 */
	@Override
	public DiscoveryBinding getDiscoveryBindingForDomain(ProtocolDomain domain) throws IOException {
		if (!(domain instanceof DPWSDomain)) {
			throw new IOException("Unsupported Domain: " + domain);
		}
		DPWSDomain dpwsDomain = (DPWSDomain) domain;
		int type = (dpwsDomain.getIPAddress().isIPv6()) ? 6 : 4;
		DiscoveryBindingIP ret = new DiscoveryBindingIP(type, dpwsDomain.getInterfaceName());
		if (Log.isInfo())
			Log.info("Get discovery binding: " + ret);
		
		return ret;
	}

	@Override
	public DataStructure getDiscoveryDomainForBinding(CommunicationBinding binding) throws IOException {
		if (binding == null) {
			throw new IOException("No specified binding");
		}
		if (!(binding instanceof DiscoveryBindingIP)) {
			throw new IOException("Wrong Binding: " + binding);
		}
		ArrayList test = new ArrayList();
		DiscoveryBindingIP bind = (DiscoveryBindingIP) binding;
		for (Iterator iterator = ((ArrayList) getAvailableDomains()).iterator(); iterator.hasNext();) {
			DPWSDomain domain = (DPWSDomain) iterator.next();
			int ipVersion = domain.getHostAddress().indexOf(":") > 0 ? 6 : 4;
			if (domain.getInterfaceName().equals(bind.getIface()) && ipVersion == bind.ipVersion) {
				test.add(domain);

			}
		}
		if (test.size() == 0) {
			throw new IOException("No Domain found for Binding: " + binding);
		}
		return test;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.CommunicationManager#kill()
	 */
	@Override
	public void kill() {
		stopInternal(true);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.CommunicationManager#shutdown()
	 */
	@Override
	public void stop() {
		stopInternal(false);
	}

	private void stopInternal(boolean kill) {
		if (stopped) {
			return;
		}
		synchronized (udpTransmissionsLock) {
			while (pendingUDPTransmissions > 0) {
				try {
					udpTransmissionsLock.wait();
				} catch (InterruptedException e) {
					// ignore
				}
			}

			// close servers first
			synchronized (udpServers) {
				for (Iterator it = udpServers.values().iterator(); it.hasNext();) {
					SOAPoverUDPServer server = (SOAPoverUDPServer) it.next();
					try {
						server.stop();
					} catch (IOException e) {
						Log.error("Unable to close SOAPoverUDPServer: " + e);
						Log.error(e);
					}
				}
				udpServers.clear();
			}
			synchronized (soapServers) {
				for (Iterator it = soapServers.values().iterator(); it.hasNext();) {
					SOAPServer server = (SOAPServer) it.next();
					try {
						server.stop();
					} catch (IOException e) {
						Log.error("Unable to close SOAPServer: " + e);
						Log.error(e);
					}
				}
				soapServers.clear();
			}
			SOAPMessageGeneratorFactory.clear();
			synchronized (httpServers) {
				for (Iterator it = httpServers.values().iterator(); it.hasNext();) {
					HTTPServer server = (HTTPServer) it.next();
					try {
						server.stop();
					} catch (IOException e) {
						Log.error("Unable to close HTTPServer: " + e);
						Log.error(e);
					}
				}
				httpServers.clear();
			}
			// now close clients, too
			synchronized (udpClientsPerDomain) {
				for (Iterator it = udpClientsPerDomain.values().iterator(); it.hasNext();) {
					SOAPoverUDPClient client = (SOAPoverUDPClient) it.next();
					try {
						client.close();
					} catch (IOException e) {
						Log.error("Unable to close SOAPoverUDPClient: " + e);
						Log.error(e);
					}
				}
				udpClientsPerDomain.clear();
			}
			if (kill) {
				HTTPClient.killAllClients();
			} else {
				HTTPClient.closeAllClients();
			}
			sentMulticastIds.clear();
			stopped = true;
		}
	}

	/**
	 * Registers a device to receive (TCP) messages on a specified socket.
	 * 
	 * @param messageTypes Messages to receive. See for example
	 *            DPWSMessageConstants.
	 * @param binding HttpBinding.
	 * @param listener
	 * @throws IOException If the binding type does not match.
	 * @throws WS4DIllegalStateException
	 */
	@Override
	public void registerDevice(int[] messageTypes, CommunicationBinding binding, IncomingMessageListener listener) throws IOException, WS4DIllegalStateException {
		checkStopped();
		try {
			if (!(binding instanceof HTTPBinding))
				throw new WS4DIllegalStateException("Unsupported binding type: " + binding);

			HTTPBinding httpBinding = (HTTPBinding) binding;
			SOAPServer server = getSOAPServer(httpBinding);
			//			if (DPWSFramework.hasModule(DPWSFramework.DPWS_SECURITY_MODULE)) {
			//			server = getSOAPServer(httpBinding.getHostAddress(), httpBinding.getPort(), httpBinding.getType() == CommunicationBinding.HTTPS_BINDING, DPWSFramework.getSecurityManager().getAliasFromBinding(httpBinding));
			//			} else
			//				server = getSOAPServer(httpBinding.getHostAddress(), httpBinding.getPort(), httpBinding.getType() == CommunicationBinding.HTTPS_BINDING, null);

			String path = httpBinding.getPath();
			SOAPHandler handler = new IncomingSOAPReceiver(listener);
			server.register(path, handler);

			for (int i = 0; i < messageTypes.length; i++) {
				if (DPWSMessageConstants.GET_MESSAGE == messageTypes[i]) {
					URI transportAddress = httpBinding.getTransportAddress();
					registerForGet.add(transportAddress);
				}
				if (DPWSMessageConstants.GET_METADATA_MESSAGE == messageTypes[i]) {
					URI transportAddress = httpBinding.getTransportAddress();
					registerForGetMetadata.add(transportAddress);
				}

				if (DPWSMessageConstants.INVOKE_MESSAGE == messageTypes[i]) {
					DefaultMIMEHandler requestHandler = new DefaultMIMEHandler();
					requestHandler.register(InternetMediaType.getApplicationXOPXML(), new IncomingMIMEReceiver(listener));
					requestHandler.register(2, -1, AttachmentStoreHandler.getInstance());
					server.getHTTPServer().register(path, InternetMediaType.getMultipartRelated(), requestHandler);

				}
			}
		} catch (ClassCastException e) {
			throw new IOException("Unsupported binding type. Need HTTPBinding but was: " + binding);
		}
	}

	/**
	 * Registers a service to receive (TCP) messages on a specified socket.
	 * 
	 * @param messageTypes Messages to receive. See for example
	 *            DPWSMessageConstants.
	 * @param binding HttpBinding.
	 * @param listener
	 * @throws IOException If the binding type does not match.
	 * @throws WS4DIllegalStateException
	 */
	@Override
	public void registerService(int[] messageTypes, CommunicationBinding binding, IncomingMessageListener listener) throws IOException, WS4DIllegalStateException {
		checkStopped();
		try {
			if (!(binding instanceof HTTPBinding))
				throw new WS4DIllegalStateException("Unsupported binding type: " + binding);

			HTTPBinding httpBinding = (HTTPBinding) binding;
			if (Log.isDebug()) Log.debug("Trying to start server");
			SOAPServer server = getSOAPServer(httpBinding);
			String path = httpBinding.getPath();
			if (Log.isDebug()) Log.debug("Trying to register handler");
			SOAPHandler handler = new IncomingSOAPReceiver(listener);
			server.register(path, handler);

			for (int i = 0; i < messageTypes.length; i++) {
				if (DPWSMessageConstants.GET_MESSAGE == messageTypes[i]) {
					URI transportAddress = httpBinding.getTransportAddress();
					registerForGet.add(transportAddress);
				}
				if (DPWSMessageConstants.GET_METADATA_MESSAGE == messageTypes[i]) {
					URI transportAddress = httpBinding.getTransportAddress();
					registerForGetMetadata.add(transportAddress);
				}

				if (DPWSMessageConstants.INVOKE_MESSAGE == messageTypes[i]) {
					DefaultMIMEHandler requestHandler = new DefaultMIMEHandler();
					requestHandler.register(InternetMediaType.getApplicationXOPXML(), new IncomingMIMEReceiver(listener));
					requestHandler.register(2, -1, AttachmentStoreHandler.getInstance());
					server.getHTTPServer().register(path, InternetMediaType.getMultipartRelated(), requestHandler);
				}
			}
		} catch (ClassCastException e) {
			throw new IOException("Unsupported binding type. Need HTTPBinding but was: " + binding);
		}
		Log.debug("registerService finished");
	}

	/**
	 * Registers the framework to receive (UDP) discovery messages on a
	 * specified socket.
	 * 
	 * @param messageTypes Messages to receive. See for example
	 *            DPWSMessageConstants.
	 * @param binding DiscoveryBinding.
	 * @param listener
	 * @throws IOException If the binding type does not match.
	 * @throws WS4DIllegalStateException
	 */
	@Override
	public void registerDeviceReference(int[] messageTypes, CommunicationBinding binding, IncomingMessageListener listener) throws IOException, WS4DIllegalStateException {
		checkStopped();
		if (binding instanceof DiscoveryBindingIP) {
			try {
				DiscoveryBindingIP discoveryBinding = (DiscoveryBindingIP) binding;
				SOAPoverUDPServer server = getSOAPoverUDPServer(discoveryBinding.getHostAddress(), discoveryBinding.getPort(), discoveryBinding.getIface(), true);
				if (server!=null)
				{
					IncomingUDPReceiver receiver = (IncomingUDPReceiver) server.getHandler();
					receiver.register(messageTypes, listener);
					if (Log.isDebug()) Log.debug("Lifecycle discovery over: " + discoveryBinding);
				}else{
					if (Log.isError()) Log.error("Soap-over-UDP server not available: " + discoveryBinding);
				}
			} catch (ClassCastException e) {
				throw new IOException("Unsupported binding type. Need DiscoveryBinding but was: " + binding);
			}
		} else {
			/*
			 * FIXME temporary code in order to support 'old' discovery
			 * listening; for use only during transition time to new discovery
			 * listener registration
			 */
			/*
			 * this registration is from the DeviceServiceRegistry; register all
			 * available bindings
			 */
			DataStructure discoveryBindings = getDiscoveryBindings();
			for (Iterator it = discoveryBindings.iterator(); it.hasNext();) {
				DiscoveryBindingIP discoveryBinding = (DiscoveryBindingIP) it.next();
				registerDeviceReference(messageTypes, discoveryBinding, listener);
			}
		}
	}

	/**
	 * Registers the framework to receive (UDP) discovery messages on a
	 * specified socket.
	 * 
	 * @param messageTypes Messages to receive. See for example
	 *            DPWSMessageConstants.
	 * @param binding DiscoveryBinding.
	 * @param listener
	 * @throws IOException If the binding type does not mat
	 * @throws WS4DIllegalStateException
	 */
	@Override
	public void registerDiscovery(int[] messageTypes, CommunicationBinding binding, IncomingMessageListener listener) throws IOException, WS4DIllegalStateException {
		checkStopped();
		try {
			DiscoveryBindingIP discoveryBinding = (DiscoveryBindingIP) binding;
			SOAPoverUDPServer server = getSOAPoverUDPServer(discoveryBinding.getHostAddress(), discoveryBinding.getPort(), discoveryBinding.getIface(), true);
			if (server != null) {
				IncomingUDPReceiver receiver = (IncomingUDPReceiver) server.getHandler();
				receiver.register(messageTypes, listener);
				if (Log.isDebug()) {
					Log.debug("Registering Output discovery over: " + discoveryBinding);
				}
			}
		} catch (ClassCastException e) {
			throw new IOException("Unsupported binding type. Need DiscoveryBinding but was: " + binding.getClass().getName());
		}
	}

	protected SOAPServer getSOAPServer(IPBinding httpBinding) throws IOException
	{
		SOAPServer server = null;
		//TODO SSch Check
		if (FrameworkModuleRegistry.getInstance().hasModule(SecurityManagerModule.class))
		{
			SecurityManagerModule secMod=(SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);

			server = getSOAPServer(httpBinding.getHostAddress(), httpBinding.getPort(), httpBinding.getType() == IPBinding.HTTPS_BINDING, secMod.getAliasFromBinding(httpBinding));
		}else{
			server = getSOAPServer(httpBinding.getHostAddress(), httpBinding.getPort(), httpBinding.getType() == IPBinding.HTTPS_BINDING, null);
		}
		return server;
	}

	@Override
	public void unregisterDevice(int[] messageTypes, CommunicationBinding binding, IncomingMessageListener listener) throws IOException, WS4DIllegalStateException {
		checkStopped();
		try {
			HTTPBinding httpBinding = (HTTPBinding) binding;
			if (httpBinding != null) {
				SOAPServer server = getSOAPServer(httpBinding);
				//				SOAPServer server = null;
				//				if (DPWSFramework.hasModule(DPWSFramework.DPWS_SECURITY_MODULE)) {
				//					server = getSOAPServer(httpBinding.getHostAddress(), httpBinding.getPort(), (httpBinding.getType() == CommunicationBinding.HTTPS_BINDING), DPWSFramework.getSecurityManager().getAliasFromBinding(httpBinding));
				//				} else
				//					server = getSOAPServer(httpBinding.getHostAddress(), httpBinding.getPort(), (httpBinding.getType() == CommunicationBinding.HTTPS_BINDING), null);

				String path = httpBinding.getPath();
				server.unregister(path);
				server.getHTTPServer().unregister(path, InternetMediaType.getMultipartRelated());
			}
			for (int i = 0; i < messageTypes.length; i++) {
				if (DPWSMessageConstants.GET_METADATA_MESSAGE == messageTypes[i]) {
					URI transportAddress = httpBinding.getTransportAddress();
					registerForGetMetadata.remove(transportAddress);
				}
				if (DPWSMessageConstants.GET_MESSAGE == messageTypes[i]) {
					URI transportAddress = httpBinding.getTransportAddress();
					registerForGet.remove(transportAddress);
				}
			}
		} catch (ClassCastException e) {
			throw new IOException("Unsupported binding type. Need HTTPBinding but was: " + binding);
		}
	}

	@Override
	public void unregisterService(int[] messageTypes, CommunicationBinding binding, IncomingMessageListener listener) throws IOException, WS4DIllegalStateException {
		checkStopped();
		try {
			HTTPBinding httpBinding = (HTTPBinding) binding;
			if (httpBinding != null) {
				SOAPServer server = getSOAPServer(httpBinding);
				String path = httpBinding.getPath();
				server.unregister(path);
				server.getHTTPServer().unregister(path, InternetMediaType.getMultipartRelated());
			}
			for (int i = 0; i < messageTypes.length; i++) {
				if (DPWSMessageConstants.GET_METADATA_MESSAGE == messageTypes[i]) {
					URI transportAddress = httpBinding.getTransportAddress();
					registerForGetMetadata.remove(transportAddress);
				}
				if (DPWSMessageConstants.GET_MESSAGE == messageTypes[i]) {
					URI transportAddress = httpBinding.getTransportAddress();
					registerForGet.remove(transportAddress);
				}
			}
		} catch (ClassCastException e) {
			throw new IOException("Unsupported binding type. Need HTTPBinding but was: " + binding);
		}
	}

	@Override
	public void unregisterDiscovery(int[] messageTypes, CommunicationBinding binding, IncomingMessageListener listener) throws IOException, WS4DIllegalStateException {
		checkStopped();
		if (binding != null) {
			try {
				DiscoveryBindingIP discoveryBinding = (DiscoveryBindingIP) binding;
				IPAddress hostAddress = discoveryBinding.getHostAddress();
				int port = discoveryBinding.getPort();
				String ifaceName = discoveryBinding.getIface();
				SOAPoverUDPServer server = getSOAPoverUDPServer(hostAddress, port, ifaceName, false);
				if (server != null) {
					IncomingUDPReceiver receiver = (IncomingUDPReceiver) server.getHandler();
					receiver.unregister(messageTypes, listener);
					if (receiver.isEmpty()) {
						String key = hostAddress.getAddress()  + ":" + port + "%" + ifaceName;
						synchronized (udpServers) {
							try {
								server.stop();
								udpServers.remove(key);
							} catch (IOException e) {
								Log.warn("unable to remove SOAP-over-UDP server for multicast address " + key + ". " + e.getMessage());
							}
						}
					}
				}
			} catch (ClassCastException e) {
				throw new IOException("Unsupported binding type. Need DiscoveryBinding but was: " + binding);
			}
		}
	}

	@Override
	public void unregisterDeviceReference(int[] messageTypes, CommunicationBinding binding, IncomingMessageListener listener) throws IOException, WS4DIllegalStateException {
		checkStopped();
		if (binding != null) {
			try {
				DiscoveryBindingIP discoveryBinding = (DiscoveryBindingIP) binding;
				IPAddress hostAddress = discoveryBinding.getHostAddress();
				int port = discoveryBinding.getPort();
				String ifaceName = discoveryBinding.getIface();
				SOAPoverUDPServer server = getSOAPoverUDPServer(hostAddress, port, ifaceName, false);
				IncomingUDPReceiver receiver = (IncomingUDPReceiver) server.getHandler();
				receiver.unregister(messageTypes, listener);
				if (receiver.isEmpty()) {
					String key = hostAddress.getAddress()  + ":" + port + "%" + ifaceName;
					synchronized (udpServers) {
						try {
							server.stop();
							udpServers.remove(key);
						} catch (IOException e) {
							Log.warn("unable to remove SOAP-over-UDP server for multicast address " + key + ". " + e.getMessage());
						}
					}
				}
			} catch (ClassCastException e) {
				throw new IOException("Unsupported binding type. Need DiscoveryBinding but was: " + binding);
			}
		}
	}


	protected HTTPServer getHTTPServer(IPBinding httpBinding) throws IOException
	{
		HTTPServer server = null;
		//TODO SSch Check
		if (FrameworkModuleRegistry.getInstance().hasModule(SecurityManagerModule.class))
		{
			SecurityManagerModule secMod=(SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);

			server =  getHTTPServer(httpBinding.getHostAddress(), httpBinding.getPort(), httpBinding.getType() == IPBinding.HTTPS_BINDING, secMod.getAliasFromBinding(httpBinding));
		}else{
			server =  getHTTPServer(httpBinding.getHostAddress(), httpBinding.getPort(), httpBinding.getType() == IPBinding.HTTPS_BINDING, null);
		}
		return server;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.CommunicationManager#deploy(org.ws4d.java
	 * .communication.Resource,
	 * org.ws4d.java.communication.CommunicationBinding, java.lang.String)
	 */
	@Override
	public URI registerResource(Resource resource, CommunicationBinding binding, String resourcePath) throws IOException, WS4DIllegalStateException {
		checkStopped();
		if (binding != null && !(binding instanceof HTTPBinding)) {
			throw new IOException("Unsupported binding type: " + binding);
		}

		prepareRessource(resource,binding);
		HTTPBinding httpBinding = (HTTPBinding) binding;
		IPAddress host = httpBinding.getHostAddress();
		int port = httpBinding.getPort();
		HTTPServer server=getHTTPServer(httpBinding);
		//		HTTPServer server = httpBinding.getType() == CommunicationBinding.HTTPS_BINDING ? getHTTPServer(host, port, true, DPWSFramework.getSecurityManager().getAliasFromBinding(httpBinding)) : getHTTPServer(host, port);

		String basicPath = httpBinding.getPath();
		if (resourcePath == null) {
			resourcePath = "";
		} else if (!(resourcePath.startsWith("/") || basicPath.endsWith("/"))) {
			resourcePath = "/" + resourcePath;
		}
		String addressPath = basicPath + resourcePath;
		server.register(addressPath, new DefaultHTTPResourceHandler(resource));
		return new URI((httpBinding.getType() == IPBinding.HTTPS_BINDING ? "https://" : "http://") + host + ":" + port + addressPath);

	}

	protected void prepareRessource(Resource resource,
			CommunicationBinding binding) {
		if (resource instanceof WSDL)
		{
			WSDL wsdl=(WSDL) resource;
			for (Iterator it=wsdl.getPortTypes();it.hasNext();)
			{
				WSDLPortType wsdlPortType=(WSDLPortType) it.next();
				QName portTypeName= wsdlPortType.getName();
				SOAP12DocumentLiteralHTTPBindingBuilder bindingBuilder=new SOAP12DocumentLiteralHTTPBindingBuilder();
				WSDLBinding wsdlbinding=bindingBuilder.buildBinding(QNameFactory.getInstance().getQName(portTypeName.getLocalPart() + "Binding", portTypeName.getNamespace()), portTypeName);
				WSDLBinding currentBinding= wsdl.getBinding(wsdlbinding.getLocalName());
				if (!(currentBinding instanceof SOAP12DocumentLiteralHTTPBinding))
					wsdl.addBinding(wsdlbinding);	
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.CommunicationManager#undeploy(org.ws4d
	 * .java.data.uri.URI)
	 */
	@Override
	public void unregisterResource(URI deployAddress, CommunicationBinding binding) throws IOException, WS4DIllegalStateException {
		IPBinding ipBinding;
		try {
			ipBinding = (IPBinding) binding;
		} catch (ClassCastException e) {
			if (Log.isError()) Log.error("DPWSCommunicationManager.unregisterResource: unsupported CommunicationBinding class (" + binding.getClass() + ")");
			return;
		}
		checkStopped();
		int port = deployAddress.getPort();
		HTTPServer server = getHTTPServer(ipBinding.getHostAddress(), port);
		server.unregister(deployAddress.getPath());
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.CommunicationManager#send(org.ws4d.java
	 * .communication.message.Message,
	 * org.ws4d.java.communication.ProtocolDomain,
	 * org.ws4d.java.communication.ResponseCallback)
	 */
	@Override
	public void send(Message message, ProtocolDomain domain, ResponseCallback callback) throws WS4DIllegalStateException {
		checkStopped();

		if (message==null)
			return;

		URI targetAddress = message.getTargetAddress();
		switch(message.getRoutingScheme())
		{
		case Message.UNICAST_ROUTING_SCHEME:
			if (targetAddress == null) {
				Log.warn("No target address found within request message " + message);
				throw new IllegalArgumentException("No target address set for message " + message);
			}
			checkDPWSVersionAndSendTCP(message, callback, targetAddress);
			break;
		case Message.MULTICAST_ROUTING_SCHEME:
			boolean useSOAPoverUDPBackOffAlgo=
				(message.getType()==DPWSMessageConstants.HELLO_MESSAGE) ||
				(message.getType()==DPWSMessageConstants.BYE_MESSAGE)||
				(message.getType()==DPWSMessageConstants.RESOLVE_MESSAGE)||
				(message.getType()==DPWSMessageConstants.PROBE_MESSAGE);
			sendUDPMulticastAndCheckDPWSVersion(message, domain, callback, useSOAPoverUDPBackOffAlgo);
			break;
		case Message.UKNOWN_ROUTING_SCHEME:
		default:
			Log.warn("Attempt to send a message of an unexpected type: " + message);
			throw new IllegalArgumentException("Unexpected message type: " + message);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.CommunicationManager#getResourceAsStream
	 * (org.ws4d.java.types.uri.URI)
	 */
	@Override
	public InputStream getResourceAsStream(URI location) throws IOException {
		if (location.getSchema().startsWith(HTTPConstants.HTTP_SCHEMA)) {
			try {
				return HTTPRequestUtil.getResourceAsStream(location.toString());
			} catch (ProtocolException e) {
				throw new IOException("HTTP protocol exception.");
			}
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.CommunicationManager#getAutobindings(java
	 * .lang.String, org.ws4d.java.structures.DataStructure)
	 */

	@Override
	public void getAutobindings(String descriptor, DataStructure bindings) throws IOException {
		checkStopped();
		List ipv4Loopback = new ArrayList(4);
		List ipv6Loopback = new ArrayList(4);
		// 'real' external IPs
		List ipv4 = new ArrayList(4);
		List ipv6 = new ArrayList(4);
		for (Iterator it = getAvailableDomains().iterator(); it.hasNext();) {
			DPWSDomain domain = (DPWSDomain) it.next();
			IPAddress addr = domain.getIPAddress();
			boolean loopbackIface = domain.getIface().isLoopback();
			if (addr.isIPv6()) {
				if (loopbackIface || addr.isLoopback()) {
					ipv6Loopback.add(domain);
				} else {
					ipv6.add(domain);
				}

			} else {
				// must be v4
				if (loopbackIface || addr.isLoopback()) {
					ipv4Loopback.add(domain);
				} else {
					ipv4.add(domain);
				}
			}
		}
		// observe IPv4 case first
		List srcList = ipv4.isEmpty() ? ipv4Loopback : ipv4;

		HashMap ipv4filter = new HashMap();
		for (Iterator it = srcList.iterator(); it.hasNext();) {
			DPWSDomain domain = (DPWSDomain) it.next();
			String ifaceName = domain.getInterfaceName();
			if (ipv4filter.containsKey(ifaceName)) {
				continue;
			}
			ipv4filter.put(ifaceName, domain);
		}
		// TODO

		// observe IPv6 case
		srcList = ipv6.isEmpty() ? ipv6Loopback : ipv6;

		HashMap ipv6filter = new HashMap();
		for (Iterator it = srcList.iterator(); it.hasNext();) {
			DPWSDomain domain = (DPWSDomain) it.next();
			String ifaceName = domain.getInterfaceName();
			DPWSDomain otherDomain = (DPWSDomain) ipv6filter.get(ifaceName);
			if (otherDomain != null) {
				if (otherDomain.getIface().isLoopback()) {
					if (otherDomain.getIPAddress().isLoopback()) {
						continue;
					}
				} else if (otherDomain.getIPAddress().isIPv6LinkLocal()) {
					continue;
				}
			}
			ipv6filter.put(ifaceName, domain);
		}
		bindAddresses(descriptor, bindings, ipv4filter.values());
		bindAddresses(descriptor, bindings, ipv6filter.values());
	}

	protected void bindAddresses(String descriptor, DataStructure bindings, DataStructure domains) {
		for (Iterator it = domains.iterator(); it.hasNext();) {
			DPWSDomain domain = (DPWSDomain) it.next();

			int autoPort = org.ws4d.java.util.Math.getRandomPortNumber();
			HTTPBinding binding = new HTTPBinding(domain.getIPAddress(), autoPort, descriptor);
			if (Log.isDebug()) {
				Log.debug("Adding HTTP auto-binding on " + domain.getInterfaceName() + ": " + binding);
			}
			bindings.add(binding);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.CommunicationManager#getProtocolDomains(org
	 * .ws4d.java.structures.Iterator, org.ws4d.java.structures.DataStructure)
	 */
	@Override
	public void getProtocolDomains(Iterator bindings, DataStructure domains) throws IOException {
		DataStructure ourDomains = getAvailableDomains();
		while (bindings.hasNext()) {
			Object o = bindings.next();
			if (!(o instanceof HTTPBinding)) {
				continue;
			}

			IPAddress bindingIPAddr = ((HTTPBinding) o).getHostAddress();
			for (Iterator it = ourDomains.iterator(); it.hasNext();) {
				DPWSDomain d = (DPWSDomain) it.next();
				if (d.getIPAddress() == bindingIPAddr) {
					domains.add(d);
					break;
				}
			}
		}
	}

	private void checkStopped() throws WS4DIllegalStateException {
		if (stopped) {
			throw new WS4DIllegalStateException("this communication manager is stopped");
		}
	}

	/**
	 * Method checks the supported DPWSVersions from the DPWSProperties. If in
	 * the Properties no DPWS Versions are defined the user is an nerd. If there
	 * is one DPWSVersion defined, it will be set to the message, else if more
	 * than 1 DPWSVersions are defined nothing will be done.
	 * 
	 * @param message , the message which checks for Version.
	 */
	private void checkSupportedDPWSVersions(Message message) {
		HashSet supportedDPWSVersions = DPWSProperties.getInstance().getSupportedDPWSVersions();

		if (message.getVersion() == null && supportedDPWSVersions.size() == 1) {
			Iterator it = supportedDPWSVersions.iterator();
			message.setVersion((DPWSProtocolVersionInfo) it.next());
			return;
		} else if (message.getVersion() != null) {
			if (!supportedDPWSVersions.contains(message.getVersion())) {
				Iterator it = supportedDPWSVersions.iterator();
				message.setVersion((DPWSProtocolVersionInfo) it.next());
				if (Log.isDebug()) {
					Log.debug("The choosen DPWS Version are not supported, changed to " + message.getVersion().getDisplayName() + ".");
				}
			}
			return;
		}
	}

	protected SOAPoverUDPServer getSOAPoverUDPServer(IPAddress localHostAddress, int port, String ifaceName, boolean startServerAutomatically) {
		String key = localHostAddress.getAddress() + ":" + port + "%" + ifaceName;
		SOAPoverUDPServer server = null;
		synchronized (udpServers) {
			server = (SOAPoverUDPServer) udpServers.get(key);
			if (server == null || !server.isRunning()) {
				try {
					server = new SOAPoverUDPServer(localHostAddress, port, ifaceName, new IncomingUDPReceiver(sentMulticastIds),startServerAutomatically);
					udpServers.put(key, server);
				} catch (IOException e) {
					if (Log.isError())Log.error("Unable to create SOAP-over-UDP server for multicast address " + key + ". " + e.getMessage());
				}
			}
		}
		return server;
	}

	private SOAPServer getSOAPServer(IPAddress ipAddress, int port, boolean secure, String alias) throws IOException {
		String targetAddress = ipAddress.getAddress() + ":" + port;
		SOAPServer server;
		synchronized (soapServers) {
			server = (SOAPServer) soapServers.get(targetAddress);
			if (server == null || !server.getHTTPServer().isRunning()) {
				server = SOAPServer.get(ipAddress, port, secure, alias);
				soapServers.put(targetAddress, server);
			} else {
				return server;
			}
		}
		// cache the underlying HTTP server, too
		HTTPServer httpServer = server.getHTTPServer();
		synchronized (httpServers) {
			httpServers.put(targetAddress, httpServer);
		}
		return server;
	}

	private HTTPServer getHTTPServer(IPAddress ipAddress, int port) throws IOException {
		return getHTTPServer(ipAddress, port, false, null);
	}

	private HTTPServer getHTTPServer(IPAddress ipAddress, int port, boolean secure, String alias) throws IOException {
		String targetAddress = ipAddress.getAddress() + ":" + port;
		HTTPServer server;
		synchronized (httpServers) {
			server = (HTTPServer) httpServers.get(targetAddress);
			if (server == null || !server.isRunning()) {
				server = HTTPServer.get(ipAddress, port, secure, alias);
				httpServers.put(targetAddress, server);
			}
		}
		return server;
	}

	private String getCanonicalAddress(String address) {
		/*
		 * be aware of alias addresses, like localhost vs. 127.0.0.1 or DNS
		 * aliases, etc. map them all to a single canonical form, e.g. IPv4
		 * address
		 */
		return PlatformSupport.getInstance().getToolkit().getNetworkDetection(NetworkDetection.IP_NETWORK).getCanonicalAddress(address);
	}

	protected void checkDPWSVersionAndSendTCP(final Message message, final ResponseCallback callback, final URI targetAddress) {

		//TODO SSch check why this should be a new Thread
		Runnable r=new SendViaTCPTask(message, callback, targetAddress);
		PlatformSupport.getInstance().getToolkit().getThreadPool().execute(r);
	}

	protected void sendUDPMulticastAndCheckDPWSVersion(final Message message, final ProtocolDomain domain, final ResponseCallback callback, boolean useSOAPoverUDPBackOffAlgo) {
		// Checks and set the DPWS Version of the Message
		checkSupportedDPWSVersions(message);

		synchronized (udpTransmissionsLock) {
			pendingUDPTransmissions++;
			if (message.getVersion() == null) {
				pendingUDPTransmissions++;
			}
		}
		if (message.getVersion() == null) {

			Message dpws1_1 = dpws2006Util.copyOutgoingMessage(message);
			dpws1_1.setVersion(DPWSProtocolVersionInfo.DPWS1_1);
			sendUDPMulticast(dpws1_1, domain, callback, useSOAPoverUDPBackOffAlgo);

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Message dpws2006 = dpws2006Util.copyOutgoingMessage(message);
			dpws2006Util.changeOutgoingMessage(dpws2006);
			dpws2006.setVersion(DPWSProtocolVersionInfo.DPWS2006);
			sendUDPMulticast(dpws2006, domain, callback, useSOAPoverUDPBackOffAlgo);

		} else {
			if (Log.isDebug()) {
				Log.debug("Send " + message.getVersion().getDisplayName() + " Message");
			}
			if (message.getVersion() == DPWSProtocolVersionInfo.DPWS2006) {
				dpws2006Util.changeOutgoingMessage(message);
			}
			sendUDPMulticast(message, domain, callback, useSOAPoverUDPBackOffAlgo);
		}
	}


	/**
	 * @param message
	 * @param callback
	 * @param targetAddress
	 */
	protected void sendTCP(Message message, ResponseCallback callback, URI targetAddress) {
		MessageReceiver receiver = callback == null ? GENERIC_RECEIVER : new SOAPResponseReceiver(message, callback);

		//Bugfix SSch 2011-11-17 Should use path+query+fragment 
		HTTPRequest request = new SOAPRequest(targetAddress.getPathQueryFragment(), message, receiver);

		HTTPClientDestination clientDestination = createClientDestination(targetAddress);
		HTTPClient.exchange(clientDestination, request);
	}

	private HTTPClientDestination createClientDestination(URI targetAddress) {
		HTTPClientDestination clientDestination=null;

		SecurityManagerModule secMod=(SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);
		boolean isSecured=false;
		String alias=null;
		if (secMod!=null)
		{
			isSecured=secMod.isHTTPS(targetAddress);
			alias=secMod.getAliasForLocation(targetAddress);
			//System.out.println("Alias for clientDestination:"+alias);
		}
		clientDestination=new HTTPClientDestination(targetAddress, isSecured, alias);

		return clientDestination;
	}

	/**
	 * @param message
	 * @param domain
	 * @param callback
	 * @param useSOAPoverUDPBackOffAlgo 
	 */
	protected void sendUDPMulticast(final Message message, final ProtocolDomain domain, final ResponseCallback callback, final boolean useSOAPoverUDPBackOffAlgo) {
		if (!(domain instanceof DPWSDomain)) {
			return;
		}
		final int messageType = message.getType();
		if (callback != null && (messageType == DPWSMessageConstants.PROBE_MESSAGE || messageType == DPWSMessageConstants.RESOLVE_MESSAGE)) {
			rrc.registerResponseCallback(message, callback, MessageProperties.getInstance().getMatchWaitTime()); //Moved getMatchWaitTime from DispatchingProps to MessageProperties
		}
		// remember outgoing message id in order to ignore it when we receive it
		sentMulticastIds.containsOrEnqueue(message.getMessageId());

		final Waiter waiter = new Waiter();

		// send without letting the caller wait
		Runnable r = new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				synchronized (waiter) {
					waiter.pending = false;
					waiter.notify();
				}

				DPWSDomain dpwsDomain = (DPWSDomain) domain;
				IPAddress localHostAddress = dpwsDomain.getIPAddress();
				// chose target address based on specified domain
				//String targetMulticastAddress = URI.isIPv6Address(localHostAddress) ? DPWSConstants.DPWS_MCAST_IPv6 : DPWSConstants.DPWS_MCAST_IPv4;

				int targetMulticastPort=DPWSConstants.DPWS_MCAST_PORT;
				IPAddress targetMulticastAddress = localHostAddress.isIPv6() ? DPWSConstants.DPWS_MCAST_IPv6 : DPWSConstants.DPWS_MCAST_IPv4;

				URI targetAdressURI = message.getTargetAddress();
				if (targetAdressURI!=null)
				{
					targetMulticastAddress=new IPAddress(targetAdressURI.getHost());
					if (targetAdressURI.getPort()>0)
						targetMulticastPort=targetAdressURI.getPort();
				}


				if (messageType == DPWSMessageConstants.HELLO_MESSAGE || messageType == DPWSMessageConstants.PROBE_MATCHES_MESSAGE) {
					try {
						Thread.sleep(getRandomApplicationDelay());
					} catch (InterruptedException e) {
						// void
					}
				}

				int portRetries = 0;
				int max_Retries=useSOAPoverUDPBackOffAlgo?MAX_UDP_PORT_RETRIES:0;
				while (portRetries++ <= max_Retries) {
					SOAPoverUDPClient client = null;
					int port = getSOAPoverUDPPort();
					DPWSProtocolData protocolData = new DPWSProtocolData(dpwsDomain.getInterfaceName(), localHostAddress.getAddress(), port, targetMulticastAddress.getAddress(), DPWSConstants.DPWS_MCAST_PORT, false);
					try {
						// make soap-over-udp clients reusable
						synchronized (udpClientsPerDomain) {
							client = (SOAPoverUDPClient) udpClientsPerDomain.get(dpwsDomain);
							if (client == null || client.isClosed()) {
								client = SOAPoverUDPClient.get(localHostAddress, port, dpwsDomain.getInterfaceName());
								udpClientsPerDomain.put(dpwsDomain, client);
							}
						}
						client.send(targetMulticastAddress,targetMulticastPort , message, udpResponseHandler, protocolData);
						MESSAGE_INFORMER.forwardMessage(message, protocolData);
						// success!
						break;
					} catch (IOException e) {
						// cleanup unusable client
						try {
							client.close();
						} catch (IOException ex) {
							Log.warn("Unable to close unusable UDP client");
						}
						synchronized (udpClientsPerDomain) {
							udpClientsPerDomain.remove(dpwsDomain);
						}
						// retry
						if (portRetries == MAX_UDP_PORT_RETRIES) 
						{
							Log.warn("Could not multicast DPWS message to " + targetMulticastAddress + ":" + DPWSConstants.DPWS_MCAST_PORT + " over " + localHostAddress + ":" + port + " due to the following exception: " + e.getMessage());
							// Log.printStackTrace(e);
							if (callback != null) {
								callback.handleTransmissionException(message, e);
							}
						}
					}
				}

				// free-up on-stop-lock...
				synchronized (udpTransmissionsLock) {
					pendingUDPTransmissions--;
					udpTransmissionsLock.notifyAll();
				}
			}


		};

		PlatformSupport.getInstance().getToolkit().getThreadPool().execute(r);
		/*
		 * make sure we return after actually having started the send
		 * procedure...
		 */
		synchronized (waiter) {
			while (waiter.pending) {
				try {
					waiter.wait(1000);
				} catch (InterruptedException e) {
					// ignore
				}
			}
		}
	}

	//Added by SSch
	private final int fixedPort=Integer.parseInt(System.getProperty("MDPWS.FixedUDPPort",String.valueOf(org.ws4d.java.util.Math.getRandomPortNumber())));
	private boolean useFixedUDPPort=System.getProperty("MDPWS.FixedUDPPort")!=null;
	private int getSOAPoverUDPPort() {
		int port=-1;
		if (useFixedUDPPort)
		{
			port=fixedPort;
		}else{
			port=org.ws4d.java.util.Math.getRandomPortNumber();
		}
		return port;
	}

	private static class Waiter {

		boolean	pending	= true;
	}

	@Override
	public DiscoveryBinding getDiscoveryBindingForProtocolData(ProtocolData data) 
	{
		String address = data.getDestinationAddress();
		String iFace = data.getIFace();

		DiscoveryBindingIP binding = null;
		if (address.indexOf(':') == -1) {
			// ipV4:
			binding = new DiscoveryBindingIP(4, iFace);
		} else {
			// ipV6:
			binding = new DiscoveryBindingIP(6, iFace);
		}

		return binding;
	}

	class SendViaTCPTask implements Runnable
	{


		private Message message;
		private ResponseCallback callback;
		private URI targetAddress;



		public SendViaTCPTask(Message message, ResponseCallback callback,
				URI targetAddress) {
			super();
			this.message = message;
			this.callback = callback;
			this.targetAddress = targetAddress;
		}

		@Override
		public void run() {
			// Checks and set the DPWS Version of the Message
			checkSupportedDPWSVersions(message);
			if (message.getVersion() == null) {

				Message dpws1_1 = dpws2006Util.copyOutgoingMessage(message);
				dpws1_1.setVersion(DPWSProtocolVersionInfo.DPWS1_1);
				sendTCP(dpws1_1, callback, targetAddress);

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Message dpws2006 = dpws2006Util.copyOutgoingMessage(message);
				dpws2006Util.changeOutgoingMessage(dpws2006);
				dpws2006.setVersion(DPWSProtocolVersionInfo.DPWS2006);
				sendTCP(dpws2006, callback, targetAddress);

			} else {
				if (Log.isDebug()) {
					Log.debug("Send " + message.getVersion().getDisplayName() + " Message");
				}
				if (message.getVersion() == DPWSProtocolVersionInfo.DPWS2006) {
					if (message instanceof GetMetadataMessage) {
						// changing the GetMetadataMessage to GetMessage
						Message dpws2006 = DefaultDPWSUtil2006.getInstance().changeOutgoingMessage((GetMetadataMessage) message);
						MessageIDsForGetMetadataMapping.add(dpws2006.getMessageId());
						sendTCP(dpws2006, callback, targetAddress);
					} else {
						// here just attributs must be changed
						dpws2006Util.changeOutgoingMessage(message);
						sendTCP(message, callback, targetAddress);
					}

				} else {
					sendTCP(message, callback, targetAddress);
				}
			}
		}
	}


}
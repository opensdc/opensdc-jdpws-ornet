/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication;

import org.ws4d.java.communication.DPWS2006.DefaultDPWSUtil2006;
import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.protocol.soap.generator.MessageDiscarder;
import org.ws4d.java.communication.protocol.soap.server.SOAPoverUDPServer.SOAPoverUDPDatagramHandler;
import org.ws4d.java.constants.DPWSMessageConstants;
import org.ws4d.java.dispatch.MessageInformer;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.IMessageEndpoint;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageException;
import org.ws4d.java.message.SOAPHeader;
import org.ws4d.java.message.discovery.ByeMessage;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMessage;
import org.ws4d.java.message.eventing.GetStatusMessage;
import org.ws4d.java.message.eventing.GetStatusResponseMessage;
import org.ws4d.java.message.eventing.RenewMessage;
import org.ws4d.java.message.eventing.RenewResponseMessage;
import org.ws4d.java.message.eventing.SubscribeMessage;
import org.ws4d.java.message.eventing.SubscribeResponseMessage;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.message.eventing.UnsubscribeMessage;
import org.ws4d.java.message.eventing.UnsubscribeResponseMessage;
import org.ws4d.java.message.metadata.GetMessage;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.message.metadata.GetResponseMessage;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.MessageIdBuffer;
import org.ws4d.java.util.Log;

/**
 *
 */
final class IncomingUDPReceiver extends SOAPoverUDPDatagramHandler {

	private static final MessageInformer	MESSAGE_INFORMER		= MessageInformer.getInstance();

	private final MessageIdBuffer			duplicateMessageIds		= new MessageIdBuffer();

	private final MessageDiscarder			discarder				= new RelevanceMessageDiscarder();

	private final DefaultDPWSUtil2006		util					= DefaultDPWSUtil2006.getInstance();

	private final DataStructure				helloListeners			= new HashSet();

	private final DataStructure				helloByeListeners		= new HashSet();

	private final DataStructure				probeResolveListeners	= new HashSet();

	/**
	 * 
	 */
	IncomingUDPReceiver(MessageIdBuffer sentMulticastIds) {
		super(sentMulticastIds);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.HelloMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(final HelloMessage hello, final DPWSProtocolData protocolData) {
		IncomingSOAPReceiver.markIncoming(hello);

		if (!hello.getHeader().isValidated()) return;

		int i = 0;

		for (Iterator it = helloByeListeners.iterator(); it.hasNext(); i++) {
			final IncomingMessageListener listener = (IncomingMessageListener) it.next();
			final boolean first = i == 0;

			Runnable r = new Runnable() {

				/*
				 * (non-Javadoc)
				 * @see java.lang.Runnable#run()
				 */
				@Override
				public void run() {
					listener.handle(hello, protocolData);
					if (first) {
						MESSAGE_INFORMER.forwardMessage(hello, protocolData);
					}
				}

			};
			PlatformSupport.getInstance().getToolkit().getThreadPool().execute(r);
		}

		for (Iterator it = helloListeners.iterator(); it.hasNext(); i++) {
			final IncomingMessageListener listener = (IncomingMessageListener) it.next();
			final boolean first = i == 0;
			Runnable r = new Runnable() {

				/*
				 * (non-Javadoc)
				 * @see java.lang.Runnable#run()
				 */
				@Override
				public void run() {
					listener.handle(hello, protocolData);
					if (first) {
						MESSAGE_INFORMER.forwardMessage(hello, protocolData);
					}
				}

			};
			PlatformSupport.getInstance().getToolkit().getThreadPool().execute(r);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ByeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(final ByeMessage bye, final DPWSProtocolData protocolData) {
		IncomingSOAPReceiver.markIncoming(bye);

		if (!bye.getHeader().isValidated()) return;

		int i = 0;
		for (Iterator it = helloByeListeners.iterator(); it.hasNext(); i++) {
			final IncomingMessageListener listener = (IncomingMessageListener) it.next();
			final boolean first = i == 0;
			Runnable r = new Runnable() {

				/*
				 * (non-Javadoc)
				 * @see java.lang.Runnable#run()
				 */
				@Override
				public void run() {
					listener.handle(bye, protocolData);
					if (first) {
						MESSAGE_INFORMER.forwardMessage(bye, protocolData);
					}
				}

			};
			PlatformSupport.getInstance().getToolkit().getThreadPool().execute(r);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ProbeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(final ProbeMessage probe, final DPWSProtocolData protocolData) {
		final long receiveTime = System.currentTimeMillis();
		IncomingSOAPReceiver.markIncoming(probe);

		int i = 0;
		for (Iterator it = probeResolveListeners.iterator(); it.hasNext(); i++) {
			final IncomingMessageListener receiver = (IncomingMessageListener) it.next();
			final boolean first = i == 0;
			Runnable r = new Runnable() {

				/*
				 * (non-Javadoc)
				 * @see java.lang.Runnable#run()
				 */
				@Override
				public void run() {
					try {
						ProbeMatchesMessage probeMatches = receiver.handle(probe, protocolData);
						if (first) {
							MESSAGE_INFORMER.forwardMessage(probe, protocolData);
						}
						if (probeMatches != null) {
							IncomingSOAPReceiver.markOutgoing(probeMatches);
							// Check for Messageversion, if Version = 2006 the
							// Namespaces and some attributs must be changed
							if (probeMatches.getVersion() == DPWSProtocolVersionInfo.DPWS2006) {
								util.changeOutgoingMessage(probeMatches);
								if (Log.isDebug()) {
									Log.debug("<I> Namespaces changed to DPWS 2006 for ProbeMatchesMessage");
								}
							}
							// wait APP_MAX_DELAY before responding
							long sendTime = receiveTime + DPWSCommunicationManager.getRandomApplicationDelay();
							long waitTime = sendTime - System.currentTimeMillis();
							if (waitTime > 0) {
								try {
									Thread.sleep(waitTime);
								} catch (InterruptedException e) {
									// void
								}
							}
							respond(probeMatches, new IPAddress(protocolData.getSourceHost()), protocolData.getSourcePort());
							MESSAGE_INFORMER.forwardMessage(probeMatches, protocolData);
						}
					} catch (MessageException e) {
						if (first) {
							MESSAGE_INFORMER.forwardMessage(e.getFault(), protocolData);
						}
					}
				}
			};
			PlatformSupport.getInstance().getToolkit().getThreadPool().execute(r);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ProbeMatchesMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(ProbeMatchesMessage probeMatches, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(probeMatches, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ResolveMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(final ResolveMessage resolve, final DPWSProtocolData protocolData) {
		IncomingSOAPReceiver.markIncoming(resolve);

		int i = 0;
		for (Iterator it = probeResolveListeners.iterator(); it.hasNext(); i++) {
			final IncomingMessageListener receiver = (IncomingMessageListener) it.next();
			final boolean first = i == 0;
			Runnable r = new Runnable() {

				/*
				 * (non-Javadoc)
				 * @see java.lang.Runnable#run()
				 */
				@Override
				public void run() {
					ResolveMatchesMessage resolveMatches =  receiver.handle(resolve, protocolData);

					if (first) {
						MESSAGE_INFORMER.forwardMessage(resolve, protocolData);
					}
					if (resolveMatches != null) {
						IncomingSOAPReceiver.markOutgoing(resolveMatches);
						// Check for Messageversion, if Version = 2006 the
						// Namespaces and some attributs must be changed
						if (resolveMatches.getVersion() == DPWSProtocolVersionInfo.DPWS2006) {
							util.changeOutgoingMessage(resolveMatches);
							if (Log.isDebug()) {
								Log.debug("<I> Namespaces changed to DPWS 2006 for ResolveMatchesMessage");
							}
						}
						respond(resolveMatches, new IPAddress(protocolData.getSourceHost()), protocolData.getSourcePort());
						MESSAGE_INFORMER.forwardMessage(resolveMatches, protocolData);
					}
				}
			};
			PlatformSupport.getInstance().getToolkit().getThreadPool().execute(r);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ResolveMatchesMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(ResolveMatchesMessage resolveMatches, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(resolveMatches, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.metadata.GetMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetMessage get, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(get, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.metadata.GetResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetResponseMessage getResponse, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(getResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.metadata.GetMetadataMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetMetadataMessage getMetadata, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(getMetadata, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.metadata. GetMetadataResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetMetadataResponseMessage getMetadataResponse, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(getMetadataResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.SubscribeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(SubscribeMessage subscribe, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(subscribe, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.SubscribeResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(SubscribeResponseMessage subscribeResponse, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(subscribeResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.GetStatusMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetStatusMessage getStatus, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(getStatus, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.GetStatusResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetStatusResponseMessage getStatusResponse, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(getStatusResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.RenewMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(RenewMessage renew, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(renew, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.RenewResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(RenewResponseMessage renewResponse, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(renewResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.UnsubscribeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(UnsubscribeMessage unsubscribe, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(unsubscribe, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.UnsubscribeResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(UnsubscribeResponseMessage unsubscribeResponse, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(unsubscribeResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.SubscriptionEndMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(SubscriptionEndMessage subscriptionEnd, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(subscriptionEnd, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.invocation.InvokeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(InvokeMessage invoke, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(invoke, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.FaultMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(FaultMessage fault, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(fault, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#
	 * receiveFailed(java.lang.Exception,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receiveFailed(Exception e, DPWSProtocolData protocolData) {
		/*
		 * who cares?? :-P this exception gets logged from within the SOAP 2
		 * message generator
		 */
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#
	 * sendFailed(java.lang.Exception,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void sendFailed(Exception e, DPWSProtocolData protocolData) {
		/*
		 * we are on the server side, thus we don't send anything that could go
		 * wrong
		 */
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.protocol.soap.server.SOAPoverUDPServer.
	 * SOAPoverUDPDatagramHandler#getDiscarder()
	 */
	@Override
	protected MessageDiscarder getDiscarder() {
		return discarder;
	}

	void registerHelloListener(IncomingMessageListener listener) {
		if (!helloListeners.contains(listener)) {
			helloListeners.add(listener);
		}
	}

	void registerHelloByeListener(IncomingMessageListener listener) {
		if (!helloByeListeners.contains(listener)) {
			helloByeListeners.add(listener);
		}
	}

	void registerProbeResolveListener(IncomingMessageListener listener) {
		if (!probeResolveListeners.contains(listener)) {
			probeResolveListeners.add(listener);
		}
	}

	void unregisterHelloListener(IncomingMessageListener listener) {
		helloListeners.remove(listener);
	}

	void unregisterHelloByeListener(IncomingMessageListener listener) {
		helloByeListeners.remove(listener);
	}

	void unregisterProbeResolveListener(IncomingMessageListener listener) {
		probeResolveListeners.remove(listener);
	}

	void register(int[] messageTypes, IncomingMessageListener listener) {
		for (int i = 0; i < messageTypes.length; i++) {
			switch (messageTypes[i]) {
				case (DPWSMessageConstants.HELLO_MESSAGE): {
					if (!helloListeners.contains(listener)) {
						helloListeners.add(listener);
					}
					break;
				}
				case (DPWSMessageConstants.BYE_MESSAGE): {
					if (!helloByeListeners.contains(listener)) {
						helloByeListeners.add(listener);
					}
					break;
				}
				case (DPWSMessageConstants.PROBE_MESSAGE): {
					if (!probeResolveListeners.contains(listener)) {
						probeResolveListeners.add(listener);
					}
					break;
				}
				case (DPWSMessageConstants.RESOLVE_MESSAGE): {
					if (!probeResolveListeners.contains(listener)) {
						probeResolveListeners.add(listener);
					}
					break;
				}
			}
		}
	}

	void unregister(int[] messageTypes, IncomingMessageListener listener) {
		for (int i = 0; i < messageTypes.length; i++) {
			switch (messageTypes[i]) {
				case (DPWSMessageConstants.HELLO_MESSAGE): {
					helloListeners.remove(listener);
					break;
				}
				case (DPWSMessageConstants.BYE_MESSAGE): {
					helloByeListeners.remove(listener);
					break;
				}
				case (DPWSMessageConstants.PROBE_MESSAGE): {
					probeResolveListeners.remove(listener);
					break;
				}
				case (DPWSMessageConstants.RESOLVE_MESSAGE): {
					probeResolveListeners.remove(listener);
					break;
				}
			}
		}
	}

	boolean isEmpty() {
		return helloListeners.isEmpty() && helloByeListeners.isEmpty() && probeResolveListeners.isEmpty();
	}

	void clear() {
		helloListeners.clear();
		helloByeListeners.clear();
		probeResolveListeners.clear();
	}

	private void receiveUnexpectedMessage(Message message, DPWSProtocolData protocolData) {
		IncomingSOAPReceiver.markIncoming(message);
		String actionName = message.getAction().toString();
		Log.error("<I> Unexpected multicast SOAP-over-UDP message: " + actionName);
		if (Log.isDebug()) {
			Log.error(message.toString());
		}
		MESSAGE_INFORMER.forwardMessage(message, protocolData);
	}

	@Override
	public IMessageEndpoint getOperation(String action) {
		return null;
	}

	private class RelevanceMessageDiscarder implements MessageDiscarder {

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.protocol.soap.generator.MessageDiscarder
		 * #discardMessage(org.ws4d.java.message.SOAPHeader,
		 * org.ws4d.java.communication.DPWSProtocolData)
		 */
		@Override
		public int discardMessage(SOAPHeader header, DPWSProtocolData protocolData) {
			if (duplicateMessageIds.containsOrEnqueue(header.getMessageId())) {
				return DUPLICATE_MESSAGE;
			}
			int msgType = header.getDPWSMessageType();

			switch (msgType) {
				case DPWSMessageConstants.HELLO_MESSAGE:
					if (helloByeListeners.isEmpty() && helloListeners.isEmpty()) return NOT_RELEVANT_MESSAGE;
					break;
				case DPWSMessageConstants.BYE_MESSAGE:
					if (helloByeListeners.isEmpty()){
						if (Log.isDebug())
							Log.debug("No Bye handler!");
						
						return NOT_RELEVANT_MESSAGE;
					}
					break;
				case DPWSMessageConstants.PROBE_MESSAGE:
					if (probeResolveListeners.isEmpty()) return NOT_RELEVANT_MESSAGE;
					break;
				case DPWSMessageConstants.RESOLVE_MESSAGE:
					if (probeResolveListeners.isEmpty()) return NOT_RELEVANT_MESSAGE;
					break;
				default:
					return NOT_RELEVANT_MESSAGE;
			}

			return NOT_DISCARDED;
		}

	}

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication;

import org.ws4d.java.communication.protocol.http.header.HTTPHeader;
import org.ws4d.java.constants.HTTPConstants;
import org.ws4d.java.types.URI;

/**
 * 
 */
public class DPWSProtocolData extends ProtocolData {

	private static final Object		INSTANCE_ID_LOCK	= new Object();

	private static long				instanceIdInc		= 0L;

	private final long				instanceId;

	private final boolean			connectionOriented;

	private final String			iFace;

	private final String			sourceHost;

	private final int				sourcePort;

	private final String			destinationHost;

	private final int				destinationPort;

	private volatile MIMEContextID	currentMIMEContext;

	private URI						transportAddress	= null;

	private final String 			destinationAddressString;

	private final String 			sourceAddressString;
	
	private HTTPHeader				httpHeader=null;
	
	private String					contentType=null;
	
	private String[]				acceptedContentTypes=null;

	private String 					contentEncoding=null;

	private String[] 				acceptedEncodings;
	 

	public static DPWSProtocolData swap(DPWSProtocolData data) {
		DPWSProtocolData dpwsProtocolData = null;
		if (data!=null)
		{
			dpwsProtocolData= new DPWSProtocolData(data.iFace, data.destinationHost, data.destinationPort, data.sourceHost, data.sourcePort, data.connectionOriented);
			//dpwsProtocolData.transportAddress=data.transportAddress;
			//new DPWSProtocolData(data.iFace, data.destinationHost, data.destinationPort, data.sourceHost, data.sourcePort, data.connectionOriented);
			dpwsProtocolData.readHTTPHeader(data.httpHeader);
		}
		return dpwsProtocolData;
	}
	
	public static DPWSProtocolData copy(DPWSProtocolData data) {
		 DPWSProtocolData dpwsProtocolData = null;
		 
		 if (data!=null){
			 dpwsProtocolData=new DPWSProtocolData(data.iFace, data.sourceHost, data.sourcePort, data.destinationHost, data.destinationPort, data.connectionOriented);
			 dpwsProtocolData.transportAddress=data.transportAddress;
			 dpwsProtocolData.currentMIMEContext=data.currentMIMEContext;
			 dpwsProtocolData.readHTTPHeader(data.httpHeader);
		 }
		 return dpwsProtocolData;
	}

	public DPWSProtocolData(String iFace, String sourceHost, int sourcePort, String destinationHost, int destinationPort, boolean connectionOriented) {
		super();
		synchronized (INSTANCE_ID_LOCK) {
			this.instanceId = instanceIdInc++;
		}
		this.iFace = iFace;
		this.sourceHost = sourceHost;
		this.sourcePort = sourcePort;
		this.destinationHost = destinationHost;
		this.destinationPort = destinationPort;
		this.destinationAddressString=destinationHost + '@' + destinationPort;
		this.sourceAddressString= sourceHost + '@' + sourcePort;
		this.connectionOriented = connectionOriented;
	}
	
	
	public void readHTTPHeader(HTTPHeader httpHeader) {
		this.httpHeader = httpHeader;
		
		if (httpHeader!=null)
		{
			contentEncoding = httpHeader.getHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_ENCODING);
			contentType = httpHeader.getHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_TYPE);
			
			String acceptedTypesString=httpHeader.getHeaderFieldValue(HTTPConstants.HTTP_HEADER_ACCEPT);
			if (acceptedTypesString!=null)
			{
				acceptedContentTypes=acceptedTypesString.split(",");
			}
			
			String acceptedEncodingsString=httpHeader.getHeaderFieldValue(HTTPConstants.HTTP_HEADER_ACCEPT_ENCODING);
			if (acceptedEncodingsString!=null)
			{
				acceptedEncodings=acceptedEncodingsString.split(",");
			}
		}
	}
	
	public String getContentEncoding() {
		return contentEncoding;
	}

	public String[] getAcceptedEncodings() {
		return acceptedEncodings;
	}

	public String getContentType() {
		return contentType;
	}
	

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public void setContentEncoding(String contentEncoding) {
		this.contentEncoding = contentEncoding;
	}

	public String[] getAcceptedContentTypes() {
		return acceptedContentTypes;
	}

	@Override
	public boolean sourceMatches(URI uri) {
//		return CommunicationManagerRegistry.getManager(getCommunicationManagerId()).addressMatches(uri, true, this);
		boolean retVal=false;
		CommunicationManager comMgr = CommunicationManagerRegistry.getManager(getCommunicationManagerId());
		if (comMgr!=null)
		{
			retVal=comMgr.addressMatches(uri, true, this);
		}
		return retVal;
	}

	@Override
	public boolean destinationMatches(URI uri) {
		boolean retVal=false;
		CommunicationManager comMgr = CommunicationManagerRegistry.getManager(getCommunicationManagerId());
		if (comMgr!=null)
		{
			retVal=comMgr.addressMatches(uri, false, this);
		}
		return retVal;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.ProtocolData#getProtocolId()
	 */
	@Override
	public CommunicationManagerID getCommunicationManagerId() {
		return DPWSCommunicationManagerID.INSTANCE;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.communication.ProtocolData#getiFace()
	 */
	@Override
	public String getIFace() {
		return iFace;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.ProtocolData#getSourceAddress()
	 */
	@Override
	public String getSourceAddress() {
		
		return sourceAddressString;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.ProtocolData#getDestinationAddress()
	 */
	@Override
	public String getDestinationAddress() {
			
		return destinationAddressString;
	}

	/**
	 * @return the instanceId
	 */
	public long getInstanceId() {
		return instanceId;
	}

	/**
	 * @return
	 */
	public String getSourceHost() {
		return sourceHost;
	}

	/**
	 * @return
	 */
	public int getSourcePort() {
		return sourcePort;
	}

	/**
	 * @return
	 */
	public String getDestinationHost() {
		return destinationHost;
	}

	/**
	 * @return
	 */
	public int getDestinationPort() {
		return destinationPort;
	}

	/**
	 * @return
	 */
	public boolean isConnectionOriented() {
		return connectionOriented;
	}

	/**
	 * @return the currentMIMEContext
	 */
	public MIMEContextID getCurrentMIMEContext() {
		return currentMIMEContext;
	}

	/**
	 * @param currentMIMEContext the currentMIMEContext to set
	 */
	public void setCurrentMIMEContext(MIMEContextID currentMIMEContext) {
		this.currentMIMEContext = currentMIMEContext;
	}

	@Override
	public String toString() {
		return "DPWSProtocolData [ id=" + getInstanceId() + ", from=" + getSourceAddress() + ", to=" + getDestinationAddress() + " ]";
	}

	public URI getTransportAddress() {
		return transportAddress;
	}

	public void setTransportAddress(URI transportAddress) {
		this.transportAddress = transportAddress;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (connectionOriented ? 1231 : 1237);
		result = prime
				* result
				+ ((currentMIMEContext == null) ? 0 : currentMIMEContext
						.hashCode());
		result = prime * result
				+ ((destinationHost == null) ? 0 : destinationHost.hashCode());
		result = prime * result + destinationPort;
		result = prime * result + ((iFace == null) ? 0 : iFace.hashCode());
		result = prime * result + (int) (instanceId ^ (instanceId >>> 32));
		result = prime * result
				+ ((sourceHost == null) ? 0 : sourceHost.hashCode());
		result = prime * result + sourcePort;
		result = prime
				* result
				+ ((transportAddress == null) ? 0 : transportAddress.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DPWSProtocolData other = (DPWSProtocolData) obj;
		if (connectionOriented != other.connectionOriented)
			return false;
		if (currentMIMEContext == null) {
			if (other.currentMIMEContext != null)
				return false;
		} else if (!currentMIMEContext.equals(other.currentMIMEContext))
			return false;
		if (destinationHost == null) {
			if (other.destinationHost != null)
				return false;
		} else if (!destinationHost.equals(other.destinationHost))
			return false;
		if (destinationPort != other.destinationPort)
			return false;
		if (iFace == null) {
			if (other.iFace != null)
				return false;
		} else if (!iFace.equals(other.iFace))
			return false;
		if (instanceId != other.instanceId)
			return false;
		if (sourceHost == null) {
			if (other.sourceHost != null)
				return false;
		} else if (!sourceHost.equals(other.sourceHost))
			return false;
		if (sourcePort != other.sourcePort)
			return false;
		if (transportAddress == null) {
			if (other.transportAddress != null)
				return false;
		} else if (!transportAddress.equals(other.transportAddress))
			return false;
		return true;
	}


	
	
}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication;

import org.ws4d.java.communication.protocol.soap.generator.MessageReceiver;
import org.ws4d.java.dispatch.MessageInformer;
import org.ws4d.java.dispatch.RequestResponseCoordinator;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.IMessageEndpoint;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.discovery.ByeMessage;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMessage;
import org.ws4d.java.message.eventing.GetStatusMessage;
import org.ws4d.java.message.eventing.GetStatusResponseMessage;
import org.ws4d.java.message.eventing.RenewMessage;
import org.ws4d.java.message.eventing.RenewResponseMessage;
import org.ws4d.java.message.eventing.SubscribeMessage;
import org.ws4d.java.message.eventing.SubscribeResponseMessage;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.message.eventing.UnsubscribeMessage;
import org.ws4d.java.message.eventing.UnsubscribeResponseMessage;
import org.ws4d.java.message.metadata.GetMessage;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.message.metadata.GetResponseMessage;
import org.ws4d.java.util.Log;

/**
 * 
 */
final class UDPResponseReceiver implements MessageReceiver {

	private static final MessageInformer		MESSAGE_INFORMER	= MessageInformer.getInstance();

	private final RequestResponseCoordinator	rrc;

	/**
	 * @param rrc
	 */
	UDPResponseReceiver(RequestResponseCoordinator rrc) {
		super();
		this.rrc = rrc;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.HelloMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(HelloMessage hello, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(hello, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ByeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(ByeMessage bye, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(bye, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ProbeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(ProbeMessage probe, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(probe, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ProbeMatchesMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(ProbeMatchesMessage probeMatches, DPWSProtocolData protocolData) {
		IncomingSOAPReceiver.markIncoming(probeMatches);

		if (probeMatches.getHeader().isValidated()) {
			rrc.handleResponse(probeMatches, protocolData);
			MESSAGE_INFORMER.forwardMessage(probeMatches, protocolData);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ResolveMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(ResolveMessage resolve, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(resolve, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ResolveMatchesMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(ResolveMatchesMessage resolveMatches, DPWSProtocolData protocolData) {
		IncomingSOAPReceiver.markIncoming(resolveMatches);

		if (resolveMatches.getHeader().isValidated()) {
			rrc.handleResponse(resolveMatches, protocolData);
			MESSAGE_INFORMER.forwardMessage(resolveMatches, protocolData);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.metadata.GetMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetMessage get, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(get, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.metadata.GetResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetResponseMessage getResponse, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(getResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.metadata.GetMetadataMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetMetadataMessage getMetadata, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(getMetadata, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.metadata. GetMetadataResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetMetadataResponseMessage getMetadataResponse, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(getMetadataResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.SubscribeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(SubscribeMessage subscribe, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(subscribe, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.SubscribeResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(SubscribeResponseMessage subscribeResponse, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(subscribeResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.GetStatusMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetStatusMessage getStatus, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(getStatus, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.GetStatusResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetStatusResponseMessage getStatusResponse, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(getStatusResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.RenewMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(RenewMessage renew, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(renew, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.RenewResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(RenewResponseMessage renewResponse, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(renewResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.UnsubscribeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(UnsubscribeMessage unsubscribe, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(unsubscribe, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.UnsubscribeResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(UnsubscribeResponseMessage unsubscribeResponse, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(unsubscribeResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.SubscriptionEndMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(SubscriptionEndMessage subscriptionEnd, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(subscriptionEnd, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.invocation.InvokeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(InvokeMessage invoke, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(invoke, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.FaultMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(FaultMessage fault, DPWSProtocolData protocolData) {
		receiveUnexpectedMessage(fault, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#
	 * receiveFailed(java.lang.Exception,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receiveFailed(Exception e, DPWSProtocolData protocolData) {
		Log.error("Unable to receive SOAP-over-UDP response");
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#
	 * sendFailed(java.lang.Exception,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void sendFailed(Exception e, DPWSProtocolData protocolData) {
		// void
	}

	private void receiveUnexpectedMessage(Message message, DPWSProtocolData protocolData) {
		IncomingSOAPReceiver.markIncoming(message);
		String actionName = message.getAction().toString();
		Log.error("<I> Unexpected unicast SOAP-over-UDP response message: " + actionName);
		if (Log.isDebug()) {
			Log.error(message.toString());
		}
		MESSAGE_INFORMER.forwardMessage(message, protocolData);
	}

	@Override
	public IMessageEndpoint getOperation(String action) {
		return null;
	}
}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication;

import java.io.IOException;

import org.ws4d.java.constants.FrameworkConstants;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashMap.Entry;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.util.Log;

/**
 *
 */
public abstract class CommunicationManagerRegistry {

	public static final String				DEFAULT_CM_PACKAGE				= FrameworkConstants.DEFAULT_PACKAGENAME + ".communication";

	public static final String				DEFAULT_CM_SUFFIX				= "CommunicationManager";

	/**
	 * This array contains the communication IDs of all default communication
	 * managers. When a call to the method {@link #loadAll()} is made, the
	 * registry will attempt to instantiate and
	 * {@link CommunicationManager#start() start} each one listed herein. The
	 * first entry within the array has the special meaning of identifying the
	 * default communication technology to use when sending requests or one-way
	 * messages if none have been explicitly specified
	 */
	public static final ArrayList	DEFAULT_COMMUNICATION_MANAGERS	= new ArrayList(); 	//SSch For Dependency Injection it is better to use a list

	private static final HashMap	COM_MANAGERS					= new HashMap(5);

	public static CommunicationManagerID getDefault() {
		return (CommunicationManagerID) (DEFAULT_COMMUNICATION_MANAGERS.size() > 0 ? DEFAULT_COMMUNICATION_MANAGERS.get(0) : null);
	}

	public static void loadAll() {
		synchronized (COM_MANAGERS) {
			for (int i = 0; i < DEFAULT_COMMUNICATION_MANAGERS.size(); i++) {
				load((CommunicationManagerID) DEFAULT_COMMUNICATION_MANAGERS.get(i));
			}
		}
	}

	public static void load(CommunicationManagerID protocolId) {
		synchronized (COM_MANAGERS) {
			loadInternal(protocolId);
		}
	}

	public static CommunicationManager getManager(String communicationId) {
		synchronized (COM_MANAGERS) {
			for (Iterator it = COM_MANAGERS.entrySet().iterator(); it.hasNext();) {
				Entry e = (Entry) it.next();
				CommunicationManagerID key = (CommunicationManagerID) e.getKey();
				if (communicationId.equals(key.getId())) {
					return (CommunicationManager) e.getValue();
				}
			}
			return null;
		}
	}

	public static CommunicationManager getManager(CommunicationManagerID protocolId) {
		synchronized (COM_MANAGERS) {
			return (CommunicationManager) COM_MANAGERS.get(protocolId);
		}
	}

	public static Iterator getLoadedManagers() {
		synchronized (COM_MANAGERS) {
			DataStructure copy = new ArrayList(COM_MANAGERS.size());
			for(Iterator manIt=COM_MANAGERS.values().iterator();manIt.hasNext();)
			{
				Object obj=manIt.next();
				if (!copy.contains(obj))
					copy.add(obj);
			}
			
			return new ReadOnlyIterator(copy);
		}
	}
	
	public static void unloadManager(CommunicationManagerID protocolId, boolean forceUnload){
		synchronized (COM_MANAGERS) {
			CommunicationManager manager=(CommunicationManager) COM_MANAGERS.remove(protocolId);
			if (manager!=null)
			{
				if (forceUnload)
					manager.kill();
				else
					manager.stop();
			}
		}
	}
	
	public static void replaceManager(CommunicationManagerID manager2Replace, CommunicationManager newManager)
	{
		synchronized (COM_MANAGERS) {
			unloadManager(manager2Replace, false);
			COM_MANAGERS.put(manager2Replace, newManager);
		}
	}

	public static void stopAll() {
		synchronized (COM_MANAGERS) {
			for (Iterator it = COM_MANAGERS.values().iterator(); it.hasNext();) {
				CommunicationManager manager = (CommunicationManager) it.next();
				manager.stop();
			}
			COM_MANAGERS.clear();
		}
	}

	public static void killAll() {
		synchronized (COM_MANAGERS) {
			for (Iterator it = COM_MANAGERS.values().iterator(); it.hasNext();) {
				CommunicationManager manager = (CommunicationManager) it.next();
				manager.kill();
			}
			COM_MANAGERS.clear();
		}
	}

	/*
	 * This method assumes EXTERNAL synchronization on COM_MANAGERS!
	 */
	private static void loadInternal(CommunicationManagerID protocolId) {
		if (COM_MANAGERS.containsKey(protocolId)) {
			return;
		}
		String id = protocolId.getId();
		if (Log.isDebug()) {
			Log.debug("Loading Communication Manager " + id + "...");
		}
		String className =null;
		if (protocolId.getImplementationClassname()!=null)
			className=protocolId.getImplementationClassname();
		else
			className=DEFAULT_CM_PACKAGE + "." + id + DEFAULT_CM_SUFFIX;
		
		if (Log.isDebug()) {
			Log.debug("Loading communication manager with classname "+className);
		}
		
		try {
			Class<?> clazz = Class.forName(className);
			CommunicationManager manager = (CommunicationManager) clazz.newInstance();
			manager.start();
			COM_MANAGERS.put(protocolId, manager);
			if (Log.isDebug()) {
				Log.debug("Communication Manager " + id + " started");
			}
		} catch (ClassNotFoundException e) {
			Log.error("Unable to find class " + className+"."+e.getMessage());
			Log.error(e);
		} catch (IllegalAccessException e) {
			Log.error("Can not access class or default constructor of class " + className);
			Log.error(e);
		} catch (InstantiationException e) {
			Log.error("Unable to create instance of class " + className);
			Log.error(e);
		} catch (IOException e) {
			Log.error("Unable to start Communication Manager " + id + ": " + e);
			Log.error(e);
		}
	}

	/*
	 * Disallow any instances from outside this class.
	 */
	private CommunicationManagerRegistry() {
		super();
	}

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication;

/**
 * A simple unique identifier of a certain transport/communication protocol or
 * technology like <strong>DPWS</strong>, <strong>BlueTooth</strong>,
 * <strong>ZigBee</strong>, etc.
 */
public interface CommunicationManagerID {

	/**
	 * Returns a unique textual representation of this protocol or technology
	 * identifier. The returned string should be a short but meaningful word,
	 * such as <strong>DPWS</strong>, <strong>ZigBee</strong>, etc.
	 * 
	 * @return the textual representation of this protocol identifier
	 */
	public String getId();
	
	public String getImplementationClassname();

}

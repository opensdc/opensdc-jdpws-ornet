/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.schema.Element;
import org.ws4d.java.service.parameter.IParameterValue;

public interface SOAP2ParameterValueConverter {

	/**
	 * This method parses an given XML Parser object (XML instance document)
	 * into a equivalent parameter value.
	 * @throws IOException 
	 */
	public abstract IParameterValue parseParameterValue(ElementParser parser,Element base) throws IOException;

}

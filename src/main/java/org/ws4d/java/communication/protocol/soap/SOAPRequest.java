/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.ws4d.java.communication.AttachmentStoreHandler;
import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.monitor.MonitorStreamFactory;
import org.ws4d.java.communication.monitor.MonitorStreamFactoryProvider;
import org.ws4d.java.communication.monitor.MonitoredMessageReceiver;
import org.ws4d.java.communication.monitor.MonitoringContext;
import org.ws4d.java.communication.protocol.http.HTTPClient.ExceptionNotification;
import org.ws4d.java.communication.protocol.http.HTTPRequest;
import org.ws4d.java.communication.protocol.http.HTTPResponseHandler;
import org.ws4d.java.communication.protocol.http.header.HTTPRequestHeader;
import org.ws4d.java.communication.protocol.http.header.HTTPResponseHeader;
import org.ws4d.java.communication.protocol.mime.DefaultMIMEHandler;
import org.ws4d.java.communication.protocol.mime.MIMEEntityInput;
import org.ws4d.java.communication.protocol.mime.MIMEHandler;
import org.ws4d.java.communication.protocol.mime.MIMEUtil;
import org.ws4d.java.communication.protocol.soap.generator.MessageReceiver;
import org.ws4d.java.communication.protocol.soap.generator.SOAP2MessageGenerator;
import org.ws4d.java.communication.protocol.soap.generator.SOAPMessageGeneratorFactory;
import org.ws4d.java.constants.HTTPConstants;
import org.ws4d.java.constants.MIMEConstants;
import org.ws4d.java.dispatch.MessageInformer;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.structures.List;
import org.ws4d.java.structures.Queue;
import org.ws4d.java.types.InternetMediaType;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.ParameterUtil;

/**
 *
 */
public class SOAPRequest implements HTTPRequest {

	private static final MessageInformer	MESSAGE_INFORMER	= MessageInformer.getInstance();

	private static final int maxWaitCnt = 10;

	private final Message					request;

	private final MessageReceiver			receiver;

	private final HTTPRequestHeader			header;

	private String							mimeBoundary		= null;

	private List							attachments		= null;
	
	
	private static final InternetMediaType[] soapXMLMediaTypes={
			InternetMediaType.getApplicationFastinfoset(),
			InternetMediaType.getApplicationJSON(),
			InternetMediaType.getApplicationEXI(),
			InternetMediaType.getSOAPXML()};
	
	private static final InternetMediaType[] soapEXIMediaTypes={
		InternetMediaType.getApplicationEXI(),
		InternetMediaType.getSOAPXML()};
	
	private static final InternetMediaType[] soapJSONMediaTypes={
		InternetMediaType.getApplicationJSON(),
		InternetMediaType.getSOAPXML()};
	
	private static final InternetMediaType[] soapFIMediaTypes={
		InternetMediaType.getApplicationFastinfoset(),
		InternetMediaType.getSOAPXML()};
	
	private static final InternetMediaType[] soapMediaTypes={
		InternetMediaType.getSOAPXML()};
	
	private static final String REQUEST_MEDIA_TYPES_MODE=System.getProperty("JDPWS.SOAPRequest.REQUEST_MEDIA_TYPES_MODE", "SOAP_ONLY");
	
	private static final String[] ACCEPTED_MEDIA_TYPES;

	static{
		InternetMediaType[] selectedMediaTypes=soapMediaTypes;
		
		if (REQUEST_MEDIA_TYPES_MODE.equals("SOAP_JSON")){
			selectedMediaTypes=soapJSONMediaTypes;
		}else if (REQUEST_MEDIA_TYPES_MODE.equals("SOAP_EXI")){
			selectedMediaTypes=soapEXIMediaTypes;
		}else if (REQUEST_MEDIA_TYPES_MODE.equals("SOAP_FI")){
			selectedMediaTypes=soapFIMediaTypes;
		}else if (REQUEST_MEDIA_TYPES_MODE.equals("ALL")){
			selectedMediaTypes=soapXMLMediaTypes;
		}
		
		ACCEPTED_MEDIA_TYPES=new String[selectedMediaTypes.length];
		for (int i=0;i<selectedMediaTypes.length;i++) {
			ACCEPTED_MEDIA_TYPES[i]=selectedMediaTypes[i].getMediaType();
			if(Log.isDebug())
			{
				Log.debug("ACCEPTED_MEDIA_TYPE ["+i+"]:"+ACCEPTED_MEDIA_TYPES[i]);
			}
		}
	}

	/**
	 * @param targetADdress
	 * @param request
	 * @param receiver
	 */
	public SOAPRequest(String requestPath, Message request, MessageReceiver receiver) {
		super();
		this.request = request;
		this.receiver = receiver;
		header = new HTTPRequestHeader(HTTPConstants.HTTP_METHOD_POST, requestPath, HTTPConstants.HTTP_VERSION11);
		header.addHeaderFieldValue(HTTPConstants.HTTP_HEADER_TRANSFER_ENCODING, HTTPConstants.HTTP_HEADERVALUE_TRANSFERCODING_CHUNKED);
		String contentType = InternetMediaType.getSOAPXML().getMediaType();
		if (request instanceof InvokeMessage) //TODO SSch getType ersetzen durch instanceof 
		{
			InvokeMessage invoke = (InvokeMessage) request;
			IParameterValue pv = invoke.getContent();
			//Re-Modularization 2011-01-21  ParameterUtil split to MIMEUtil 
			if ( MIMEUtil.hasAttachment(pv)) {
				InternetMediaType mimeType = InternetMediaType.getMultipartRelated();
				mimeBoundary = MIMEConstants.BOUNDARY_PREFIX + System.currentTimeMillis();
				mimeType.setParameter(MIMEConstants.PARAMETER_BOUNDARY, mimeBoundary);
				contentType = mimeType.toString();

				attachments = ParameterUtil.getAttachments(pv);
			}else{
				//TODO Ssch how to handle attachments with objectIParameterValue?
			}
		}
		header.addHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_TYPE, contentType);
	}


	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.http.HTTPRequest#getRequestHeader()
	 */
	@Override
	public HTTPRequestHeader getRequestHeader() {
		return header;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.http.HTTPRequest#getResponseHandler
	 * (org.ws4d.java.communication.InternetMediaType)
	 */
	@Override
	public HTTPResponseHandler getResponseHandler(InternetMediaType mediaType) throws IOException {
		//TODO SSch extract to handler map
		if (isSOAPXMLMessage(mediaType)) {
			return new HTTPResponseHandler() {

				/*
				 * (non-Javadoc)
				 * @see
				 * org.ws4d.java.communication.protocol.http.HTTPResponseHandler
				 * #handle(org.ws4d.java.communication.protocol.http.header.
				 * HTTPResponseHeader, java.io.InputStream,
				 * org.ws4d.java.communication.protocol.http.HTTPRequest,
				 * org.ws4d.java.communication.DPWSProtocolData,
				 * org.ws4d.java.io.monitor.MonitoringContext)
				 */
				@Override
				public void handle(HTTPResponseHeader header, InputStream body, HTTPRequest request, DPWSProtocolData protocolData, MonitoringContext context) throws IOException {
					if (protocolData!=null)
						protocolData.readHTTPHeader(header);
					
					int httpStatus = header.getStatus();
					// TODO filter other potentially empty HTTP responses
					if (httpStatus == 202 || httpStatus == 204) {
						return;
					}

					final MessageReceiver r;

					MonitorStreamFactory monFac = MonitorStreamFactoryProvider.getInstance().getMonitorStreamFactory();
					if (monFac != null) {
						r = new MonitoredMessageReceiver(receiver, context);
					} else {
						r = receiver;
					}

					String strLen = header.getHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_LENGTH);
					int len = -1;
					if (strLen != null) {
						len = Integer.parseInt(strLen);
					}
					if (len == 0) {
						// may be a faulty HTTP response?
						if (httpStatus != 200) 
						{
							r.receive(FaultMessage.createEndpointUnavailableFault(SOAPRequest.this.request), protocolData);
						}
						return;
					}

					if (httpStatus!=200)
					{
						if (Log.isDebug()) Log.debug("HTTP-Status "+httpStatus+" for SOAPRequest "+SOAPRequest.this.request);
					}
					if (len==-1)
						len=body.available();

					int waitCnt=0;
					while (len<1 && waitCnt<maxWaitCnt){
						if (Log.isDebug()) Log.debug("Body length < 1. "+waitCnt);
						try {
							Thread.sleep(10);
							len=body.available();
						} catch (InterruptedException e) {
							Log.warn(e);
						}
						waitCnt++;
					}
					if (len>1)
					{
						SOAP2MessageGenerator generator = SOAPMessageGeneratorFactory.getInstance().getSOAP2MessageGeneratorForCurrentThread();
						generator.deliverMessage(body, r, protocolData, SOAPRequest.this.request);
						SOAPMessageGeneratorFactory.getInstance().releaseSOAP2MessageGenerator(generator);
					}else if (httpStatus<200  || httpStatus>299)
					{
						ExceptionNotification eNotification = new ExceptionNotification(protocolData, request, new IOException("HTTP Status "+httpStatus), false);
						eNotification.start();
					}
				}

			};
		} else if (InternetMediaType.getMultipartRelated().equals(mediaType)) {
			DefaultMIMEHandler mimeHandler = new DefaultMIMEHandler();
			mimeHandler.register(InternetMediaType.getApplicationXOPXML(), new MIMEHandler() {

				/*
				 * (non-Javadoc)
				 * @seeorg.ws4d.java.communication.protocol.mime.MIMEHandler#
				 * handleResponse
				 * (org.ws4d.java.communication.protocol.mime.MIMEEntityInput,
				 * org.ws4d.java.communication.DPWSProtocolData,
				 * org.ws4d.java.io.monitor.MonitoringContext)
				 */
				@Override
				public void handleResponse(MIMEEntityInput part, DPWSProtocolData protocolData, MonitoringContext context) throws IOException {

					final MessageReceiver r;

					MonitorStreamFactory monFac = MonitorStreamFactoryProvider.getInstance().getMonitorStreamFactory();
					if (monFac != null) {
						r = new MonitoredMessageReceiver(receiver, context);
					} else {
						r = receiver;
					}

					SOAP2MessageGenerator generator = SOAPMessageGeneratorFactory.getInstance().getSOAP2MessageGeneratorForCurrentThread();
					generator.deliverMessage(part.getBodyInputStream(), r, protocolData, SOAPRequest.this.request);
					SOAPMessageGeneratorFactory.getInstance().releaseSOAP2MessageGenerator(generator);
				}

				/*
				 * (non-Javadoc)
				 * @seeorg.ws4d.java.communication.protocol.mime.MIMEHandler#
				 * handleRequest
				 * (org.ws4d.java.communication.protocol.mime.MIMEEntityInput,
				 * org.ws4d.java.structures.Queue,
				 * org.ws4d.java.communication.DPWSProtocolData,
				 * org.ws4d.java.io.monitor.MonitoringContext)
				 */
				@Override
				public void handleRequest(MIMEEntityInput part, Queue responses, DPWSProtocolData protocolData, MonitoringContext context) throws IOException {
					// void
				}

			});
			mimeHandler.register(2, -1, AttachmentStoreHandler.getInstance());
			return mimeHandler;
		}
		if (Log.isDebug()) Log.debug("Could not find response handler for "+mediaType+". Only "+ InternetMediaType.getSOAPXML() +" & "+InternetMediaType.getMultipartRelated()+" are supported!");
		return null;
	}




	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.http.HTTPRequest#requestSendFailed
	 * (java.lang.Exception, org.ws4d.java.communication.ProtocolData)
	 */
	@Override
	public void requestSendFailed(Exception e, ProtocolData protocolData) {
		receiver.sendFailed(e, (DPWSProtocolData) protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.http.HTTPRequest#responseReceiveFailed
	 * (java.lang.Exception, org.ws4d.java.communication.ProtocolData)
	 */
	@Override
	public void responseReceiveFailed(Exception e, ProtocolData protocolData) {
		receiver.receiveFailed(e, (DPWSProtocolData) protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.http.HTTPRequest#serializeRequestBody
	 * (java.io.OutputStream, org.ws4d.java.communication.ProtocolData,
	 * org.ws4d.java.io.monitor.MonitoringContext)
	 */
	@Override
	public void serializeRequestBody(OutputStream out, ProtocolData protocolData, MonitoringContext context) throws IOException {
		MIMEUtil.serializeMessageWithAttachments(request, mimeBoundary, attachments, out, protocolData);
		MESSAGE_INFORMER.forwardMessage(request, protocolData);
		if (context != null) {
			context.setMessage(request);
		}
	}


	@Override
	public String[] getResponseAcceptedContentTypes() {
		String[] retVal=null;
		if (request instanceof InvokeMessage){
			retVal=ACCEPTED_MEDIA_TYPES;
//			System.out.println("Action:"+((InvokeMessage)request).getAction());
//			retVal[0]="application/soap+xml";
//			retVal[1]="application/json";
//			retVal[2]="application/exi";
//			retVal[3]="application/fastinfoset";
		}
		return retVal;
	}
	
	
	private boolean isSOAPXMLMessage(InternetMediaType mediaType) {
//		return InternetMediaType.getApplicationFastinfoset().equals(mediaType) 
//				|| InternetMediaType.getApplicationEXI().equals(mediaType)
//				|| InternetMediaType.getApplicationJSON().equals(mediaType)
//				|| InternetMediaType.getSOAPXML().equals(mediaType);
		boolean isSOAPXMLMessage=false;
		for (int i=0;i<soapXMLMediaTypes.length;i++) {
			isSOAPXMLMessage|=soapXMLMediaTypes[i].equals(mediaType);
		}
		return isSOAPXMLMessage;
	}

}

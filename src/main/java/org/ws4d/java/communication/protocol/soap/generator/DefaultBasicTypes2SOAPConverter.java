/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.WSAConstants;
import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.constants.XMLConstants;
import org.ws4d.java.io.xml.XmlSerializer;
import org.ws4d.java.message.discovery.ProbeMatch;
import org.ws4d.java.message.discovery.ResolveMatch;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashMap.Entry;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.List;
import org.ws4d.java.types.AppSequence;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.Delivery;
import org.ws4d.java.types.DiscoveryData;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.Filter;
import org.ws4d.java.types.HostMData;
import org.ws4d.java.types.HostedMData;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.ProbeScopeSet;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.ReferenceParametersMData;
import org.ws4d.java.types.ReferenceParametersMData.ReferenceParameter;
import org.ws4d.java.types.RelationshipMData;
import org.ws4d.java.types.ScopeSet;
import org.ws4d.java.types.ThisDeviceMData;
import org.ws4d.java.types.ThisModelMData;
import org.ws4d.java.types.URI;
import org.ws4d.java.types.URISet;
import org.ws4d.java.types.UnknownDataContainer;
import org.ws4d.java.util.Log;

public class DefaultBasicTypes2SOAPConverter implements
BasicTypes2SOAPConverter {


	//From UnknownDataContainer
	@Override
	public void serializeUnknownElements(UnknownDataContainer dataContainer, XmlSerializer serializer) throws IOException {
		if (dataContainer != null && dataContainer.getUnknownElements()!=null) {
			for (Iterator it = dataContainer.getUnknownElements().entrySet().iterator(); it.hasNext();) {
				HashMap.Entry ent = (Entry) it.next();
				QName qname = (QName) ent.getKey();
				serializer.unknownElements(qname, (List) ent.getValue());
			}
		}

	}

	//From UnknownDataContainer
	@Override
	public void serializeUnknownAttributes(UnknownDataContainer dataContainer, XmlSerializer serializer) throws IOException {
		if (dataContainer != null && dataContainer.getUnknownAttributes()!=null) {
			for (Iterator it = dataContainer.getUnknownAttributes().entrySet().iterator(); it.hasNext();) {
				HashMap.Entry ent = (Entry) it.next();
				QName qname = (QName) ent.getKey();
				String value = (String) ent.getValue();
				serializer.attribute(qname.getNamespace(), qname.getLocalPart(), value);
			}
		}
	}

	//From AttributedURI
	@Override
	public void serializeAttributedURI(AttributedURI attrURI, XmlSerializer serializer, String namespace, String elementName) throws IOException {
		serializer.startTag(namespace, elementName);
		if (attrURI.getAttributedMap_QN_2_Obj() != null) {

			for (Iterator it = attrURI.getAttributedMap_QN_2_Obj().entrySet().iterator(); it.hasNext();) {
				HashMap.Entry ent = (Entry) it.next();
				QName qname = (QName) ent.getKey();
				String value = (String) ent.getValue();
				serializer.attribute(qname.getNamespace(), qname.getLocalPart(), value);
			}
		}
		serializer.text(attrURI.toString());
		serializer.endTag(namespace, elementName);
	}

	/**
	 * Serializes the NamespacePrefixes
	 * 
	 * @param params
	 * @throws IOException
	 */
	@Override
	public void serializeNamespacePrefixes(ReferenceParametersMData rParams, XmlSerializer serializer) 
	{
		ReferenceParameter[] allParameters = rParams.getParameters();
		for (int i = 0; i < allParameters.length; i++) {
			ReferenceParameter parameter = allParameters[i];
			String[] chunks = parameter.getChunks();
			for (int j = 1; j < chunks.length; j += 2) {
				serializer.getPrefix(chunks[j], true);
			}
		}
	}

	@Override
	public void serializeEndpointReference(EndpointReference epr,
			XmlSerializer serializer, ConstantsHelper helper,
			String namespace, String elementName) throws IOException {
		if (epr==null) return;
		//Start-Tag
		serializer.startTag(namespace, elementName);
		// Adds UnknownAttributes to EPR Tag if exists
		serializeUnknownAttributes(epr,serializer);
		// Address Element
		serializeAttributedURI(epr.getAddress(), serializer, helper.getWSANamespace(), WSAConstants.WSA_ELEM_ADDRESS);
		// ReferenceParameters Element
		if (epr.getReferenceParameters()!= null) {
			serializer.startTag(helper.getWSANamespace(), WSAConstants.WSA_ELEM_REFERENCE_PARAMETERS);
			serializeNamespacePrefixes(epr.getReferenceParameters(), serializer);
			serializeUnknownAttributes(epr.getReferenceParameters(), serializer);
			// fake in order to dump reference parameter prefixes
			serializer.text("");
			serializeReferenceParameters(epr.getReferenceParameters(),serializer, helper, false);
			serializer.endTag(helper.getWSANamespace(), WSAConstants.WSA_ELEM_REFERENCE_PARAMETERS);
		}
		// Metadata Element
		if (epr.getEndpointMetadata() != null) {
			serializer.startTag(helper.getWSANamespace(), WSAConstants.WSA_ELEM_METADATA);
			serializeUnknownAttributes(epr.getEndpointMetadata(),serializer);
			serializeUnknownElements(epr.getEndpointMetadata(),serializer);
			serializer.endTag(helper.getWSANamespace(), WSAConstants.WSA_ELEM_METADATA);
		}
		// Adds UnknownElements to EPR if exists
		serializeUnknownElements(epr,serializer);
		serializer.endTag(namespace, elementName);

	}

	@Override
	public void serializeReferenceParameters(
			ReferenceParametersMData referenceParameters,
			XmlSerializer serializer, ConstantsHelper helper, boolean withinHeader) throws IOException
			{
		// any XML special chars should remain unescaped
		if (referenceParameters.getWseIdentifier()!= null) {
			String wseId = referenceParameters.getWseIdentifier().toString();
			serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_IDENTIFIER);
			if (withinHeader) {
				serializer.attribute(helper.getWSANamespace(), WSAConstants.WSA_ATTR_IS_REFERENCE_PARAMETER, "true");
			}
			serializer.text(wseId == null ? "" : wseId);
			serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_IDENTIFIER);
		} else {
			// we need this to close the preceding element tag
			serializer.text("");
		}
		serializeUnknownElements(referenceParameters, serializer);
		ReferenceParameter[] allParameters = referenceParameters.getParameters();
		for (int i = 0; i < allParameters.length; i++) {
			ReferenceParameter parameter = allParameters[i];
			serializer.plainText("<");
			String prefix = serializer.getPrefix(parameter.getNamespace(), true);
			serializer.plainText(prefix);
			serializer.plainText(":");
			serializer.plainText(parameter.getName());
			// add wsa:IsReferenceParameter if withinHeader == true
			if (withinHeader) {
				serializer.plainText(" ");
				prefix = serializer.getPrefix(helper.getWSANamespace(), true);
				serializer.plainText(prefix);
				serializer.plainText(":");
				serializer.plainText(WSAConstants.WSA_ATTR_IS_REFERENCE_PARAMETER);
				serializer.plainText("=\"true\"");
			}
			String[] chunks = parameter.getChunks();
			for (int j = 0; j < chunks.length; j++) {
				if (j % 2 == 0) {
					serializer.plainText(chunks[j]);
				} else {
					prefix = serializer.getPrefix(chunks[j], true);
					serializer.plainText(prefix);
				}
			}
		}
			}

	
	@Override
	public void serializeAppSequence(AppSequence appSequence,
			XmlSerializer serializer, ConstantsHelper helper) throws IllegalArgumentException, IllegalStateException, IOException {
		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_APPSEQUENCE);
		// Adds UnknownAttributes
		serializeUnknownAttributes(appSequence, serializer);
		if (appSequence.getInstanceId() >= 1) {
			serializer.attribute(null, WSDConstants.WSD_ATTR_INSTANCEID, String.valueOf(appSequence.getInstanceId()));
		}
		if (appSequence.getMessageNumber() >= 1) {
			serializer.attribute(null, WSDConstants.WSD_ATTR_MESSAGENUMBER, String.valueOf(appSequence.getMessageNumber()));
		}
		if (appSequence.getSequenceId() != null) {
			serializer.attribute(null, WSDConstants.WSD_ATTR_SEQUENCEID, String.valueOf(appSequence.getSequenceId()));
		}
		// Adds UnknownElements
		serializeUnknownElements(appSequence, serializer);
		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_APPSEQUENCE);

	}

	
	@Override
	public void serializeDiscoveryData(DiscoveryData dData,
			XmlSerializer serializer, ConstantsHelper helper)
	throws IOException {
		if (dData==null) return;	//TODO Check if exception is better
		// Endpointreference
		serializeEndpointReference(dData.getEndpointReference(), serializer, helper, helper.getWSANamespace(), WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE);
		// QNameSet Types
		if (dData.getTypes() != null) {
			serializeQNameSet(dData.getTypes(), serializer, helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_TYPES);
		}
		// ScopeSet scopes
		if (dData.getScopes() != null) {
			serializeScopeSet(dData.getScopes(), serializer, helper.getWSDNamespace());
		}
		// URISet xAddress
		if (dData.getXAddrs() != null) {
			serializeURISet(dData.getXAddrs(), serializer, helper.getWSDNamespace());
		}
		// MetadataVersion
		if (dData.getMetadataVersion() >= 1) {
			SerializeUtil.serializeTag(serializer, helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_METADATAVERSION, (String.valueOf(dData.getMetadataVersion())));
		}
		// Adds UnknownElements to Header if exists
		serializeUnknownElements(dData,serializer);

	}
	
	
	
	
	@Override
	public void serializeResolveMatch(ResolveMatch rm,
			XmlSerializer serializer, ConstantsHelper helper)
			throws IOException {
		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_RESOLVEMATCH);
		// Adds UnknownAttributes
		serializeUnknownAttributes(rm,serializer);
		// Discovery Data adden
		serializeDiscoveryData(rm,serializer, helper);
		// Adds UnknownElements
		serializeUnknownElements(rm,serializer);
		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_RESOLVEMATCH);
	}

	@Override
	public void serializeProbeMatch(ProbeMatch pm, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (pm==null)return;
		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_PROBEMATCH);
		// Adds UnknownAttributes
		serializeUnknownAttributes(pm,serializer);
		// Discovery Data adden
		serializeDiscoveryData(pm,serializer, helper);
		// Adds UnknownElements
		serializeUnknownElements(pm,serializer);
		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_PROBEMATCH);
	}

	
	@Override
	public void serializeURISet(URISet xAddrs, XmlSerializer serializer,
			String namespace) throws  IOException {
		if (xAddrs==null) return;
		try{
			serializer.startTag(namespace, WSDConstants.WSD_ELEMENT_XADDRS);
			serializer.text(xAddrs.toString() == null ? "" : xAddrs.toString());
			serializer.endTag(namespace, WSDConstants.WSD_ELEMENT_XADDRS);
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}

	}

	
	@Override
	public void serializeScopeSet(ScopeSet set, XmlSerializer serializer, String namespace) throws IOException{
		if (set!=null && !set.isEmpty()) 
		{
			serializer.startTag(namespace, WSDConstants.WSD_ELEMENT_SCOPES);
			//
			if (set instanceof ProbeScopeSet)
			{
				ProbeScopeSet probeScopeSet=(ProbeScopeSet)set;
				if (probeScopeSet.getMatchBy()!=null && !WSDConstants.WSD_MATCHING_RULE_DEFAULT.equals(probeScopeSet.getMatchBy()))
				{
					serializer.attribute(WSDConstants.WSD_NAMESPACE_NAME, WSDConstants.WSD_ATTR_MATCH_BY, probeScopeSet.getMatchBy());
				}
			}
			if (set.getUnknownAttributes() != null && !(set.getUnknownAttributes().isEmpty())) {
				for (Iterator it = set.getUnknownAttributes().entrySet().iterator(); it.hasNext();) {
					HashMap.Entry ent = (Entry) it.next();
					QName qname = (QName) ent.getKey();
					String value = (String) ent.getValue();
					serializer.attribute(qname.getNamespace(), qname.getLocalPart(), value);
				}
			}
			serializer.text(set.getScopesAsString());
			serializer.endTag(namespace, WSDConstants.WSD_ELEMENT_SCOPES);
		}

	}

	
	@Override
	public void serializeQNameSet(QNameSet set, XmlSerializer serializer,
			String namespace, String element) throws IOException{
		if (set != null && !set.isEmpty()) {
			Iterator it = set.iterator();
			while (it.hasNext()) {
				QName qn = (QName) it.next();
				if (qn.hasPriority()) {
					serializer.setPrefix(qn.getPrefix(), qn.getNamespace());
				}
			}
			serializer.startTag(namespace, element);
			it = set.iterator();
			while (it.hasNext()) {
				QName qn = (QName) it.next();
				if (qn.hasPriority()) {
					serializer.text(qn.getLocalPartPrefixed());
					if (it.hasNext()) {
						serializer.text(" ");
					}
				}
			}
			serializer.endTag(namespace, element);
		}
	}

	
	@Override
	public void serializeDelivery(Delivery delivery, XmlSerializer serializer, ConstantsHelper helper) throws IOException {
		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_DELIVERY);
		// Adds UnknownAttributes
		serializeUnknownAttributes(delivery, serializer);
		// Add the DeliveryMode
		if (delivery.getMode() != null) {
			serializer.attribute(null, WSEConstants.WSE_ATTR_DELIVERY_MODE, delivery.getMode().toString());
		}
		// Add the EPR
		serializeEndpointReference(delivery.getNotifyTo(), serializer, helper, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_NOTIFYTO);
		// Adds UnknownElements
		serializeUnknownElements(delivery, serializer);
		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_DELIVERY);
	}

	
	@Override
	public void serializeFilter(Filter filter, XmlSerializer serializer,
			ConstantsHelper helper) throws IOException {
		
		if (filter==null) return;
		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_FILTER);
		// Adds UnknownAttributes
		serializeUnknownAttributes(filter,serializer);
		if (filter.getDialect() != null) {
			serializer.attribute(null, WSEConstants.WSE_ATTR_FILTER_DIALECT, filter.getDialect().toString());
		}
		if (filter.getActions() != null) {
			for (Iterator it = filter.getActions().iterator(); it.hasNext();) {
				URI uri = (URI) it.next();
				serializer.text(uri.toString());
				if (it.hasNext()) {
					serializer.text(" ");
				}
			}
		}
		// Adds UnknownElements
		serializeUnknownElements(filter,serializer);
		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_FILTER);
	}

	
	@Override
	public void serializeHostMData(HostMData hmData, XmlSerializer serializer,
			ConstantsHelper helper) throws IOException {
		if (hmData==null) return;
		
		serializer.startTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_RELATIONSHIP_ELEM_HOST);
		// Adds UnknownAttributes
		serializeUnknownAttributes(hmData, serializer);
		// Add the EPR of Host
		if (hmData.getEndpointReference() != null) {
			serializeEndpointReference(hmData.getEndpointReference(),serializer, helper, helper.getWSANamespace(), WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE);
		}
		// Add the Service Types of Host
		if (hmData.getTypes() != null) {
			serializeQNameSet(hmData.getTypes(),serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_TYPES);
		}
		// Adds UnknownElements
		serializeUnknownElements(hmData,serializer);
		serializer.endTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_RELATIONSHIP_ELEM_HOST);
		
	}

	
	@Override
	public void serializeHostedMData(HostedMData hmData,
			XmlSerializer serializer, ConstantsHelper helper)
			throws IOException {
		if (hmData==null) return;
		serializer.startTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_RELATIONSHIP_ELEM_HOSTED);
		// Adds UnknownAttributes
		serializeUnknownAttributes(hmData,serializer);
		// Endpoint References
		for (Iterator it =hmData.getEndpointReferences().iterator(); it.hasNext();) {
			EndpointReference epr = (EndpointReference) it.next();
			serializeEndpointReference(epr, serializer, helper, helper.getWSANamespace(), WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE);
		}
		// ServiceTypes
		if (hmData.getTypes() != null) {
			serializeQNameSet(hmData.getTypes() ,serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_TYPES);
		}
		// Add ServiceID
		if (hmData.getServiceId() != null) 
		{
			SerializeUtil.serializeTag(serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_SERVICEID, (hmData.getServiceId() == null ? null : hmData.getServiceId().toString()));
		}
		// Adds UnknownElements
		serializeUnknownElements(hmData,serializer);
		serializer.endTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_RELATIONSHIP_ELEM_HOSTED);
	}

	
	@Override
	public void serializeThisDeviceMData(ThisDeviceMData deviceMData,
			XmlSerializer serializer, ConstantsHelper helper)
			throws IOException {
		if (deviceMData==null) return;
		LocalizedString value = null;
		// StartTag => dpws:thisModel
		serializer.startTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_THISDEVICE);
		// Adds UnknownAttributes
		serializeUnknownAttributes(deviceMData,serializer);
		// FriendlyName => 1 -> *
		DataStructure friendlyNames = deviceMData.getFriendlyNames();
		if (friendlyNames == null || friendlyNames.size() == 0) {
			Log.warn("Message2SOAPGenerator.addThisDevice: No friendly name defined within device");
		} else {
			for (Iterator it = friendlyNames.iterator(); it.hasNext();) {
				value = (LocalizedString) it.next();
				SerializeUtil.serializeTagWithAttribute(serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_FRIENDLYNAME, value.getValue(), XMLConstants.XML_NAMESPACE_NAME, XMLConstants.XML_ATTRIBUTE_LANGUAGE, value.getLanguage());
			}
		}
		// FirmwareVersion => 0 -> 1
		if (deviceMData.getFirmwareVersion() != null && !(deviceMData.getFirmwareVersion().equals(""))) {
			SerializeUtil.serializeTag(serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_FIRMWAREVERSION, deviceMData.getFirmwareVersion());
		}
		// SerialNumber => 0 -> 1
		if (deviceMData.getSerialNumber() != null && !(deviceMData.getSerialNumber().equals(""))) {
			SerializeUtil.serializeTag(serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_SERIALNUMBER, deviceMData.getSerialNumber());
		}
		// Adds UnknownElements
		serializeUnknownElements(deviceMData,serializer);
		// EndTag => dpws:thisModel
		serializer.endTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_THISDEVICE);
		
	}

	
	@Override
	public void serializeThisModelMData(ThisModelMData modelMData,
			XmlSerializer serializer, ConstantsHelper helper)
			throws IOException {
		
		if (modelMData==null) return;
		LocalizedString value = null;
		// StartTag => dpws:thisModel
		serializer.startTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_THISMODEL);
		// Adds UnknownAttributes
		serializeUnknownAttributes(modelMData,serializer);
		// Manufacturer => 1 -> *
		DataStructure manufacturer = modelMData.getManufacturerNames();
		for (Iterator it = manufacturer.iterator(); it.hasNext();) {
			value = (LocalizedString) it.next();
			SerializeUtil.serializeTagWithAttribute(serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_MANUFACTURER, value.getValue(), XMLConstants.XML_NAMESPACE_NAME, XMLConstants.XML_ATTRIBUTE_LANGUAGE, value.getLanguage());
		}
		// ManufaturerURL => 0 -> 1
		URI manufacturerURL = modelMData.getManufacturerUrl();
		if (manufacturerURL != null && !(manufacturerURL.toString().equals(""))) {
			SerializeUtil.serializeTag(serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_MANUFACTURERURL, (manufacturerURL.toString() == null ? "" : manufacturerURL.toString()));
		}
		// ModelName => 1 -> *
		value = null;
		DataStructure modelNames = modelMData.getModelNames();
		if (modelNames == null || modelNames.size() == 0) {
			Log.warn("Message2SOAPGenerator.addThisModel: No model name defined within device");
		} else {
			for (Iterator it = modelNames.iterator(); it.hasNext();) {
				value = (LocalizedString) it.next();
				SerializeUtil.serializeTagWithAttribute(serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_MODELNAME, value.getValue(), XMLConstants.XML_NAMESPACE_NAME, XMLConstants.XML_ATTRIBUTE_LANGUAGE, value.getLanguage());
			}
		}
		// ModelNumber => 0 -> 1
		String modelNumber = modelMData.getModelNumber();
		if (modelNumber != null && !(modelNumber.equals(""))) {
			SerializeUtil.serializeTag(serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_MODELNUMBER, (modelNumber == null ? "" : modelNumber));
		}
		// ModelUrl => 0 -> 1
		URI modelUrl = modelMData.getModelUrl();
		if (modelUrl != null && !(modelUrl.toString().equals(""))) {
			SerializeUtil.serializeTag(serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_MODELURL, (modelUrl.toString() == null ? "" : modelUrl.toString()));
		}
		// PresentationUrl => 0 -> 1
		URI presURL = modelMData.getPresentationUrl();
		if (presURL != null && !(presURL.toString().equals(""))) {
			SerializeUtil.serializeTag(serializer, helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_PRESENTATIONURL, (presURL.toString() == null ? "" : presURL.toString()));
		}
		// Adds UnknownElements
		serializeUnknownElements(modelMData,serializer);
		// EndTag => dpws:thisModel
		serializer.endTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_THISMODEL);
	}

	
	@Override
	public void serializeRelationshipMData(RelationshipMData rmData,
			XmlSerializer serializer, ConstantsHelper helper)
			throws IOException {
		if (rmData==null) return;
		
		// StartTag => dpws:Relationship
		serializer.startTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_RELATIONSHIP);
		serializer.attribute(null, DPWSConstants.DPWS_RELATIONSHIP_ATTR_TYPE, helper.getMetadataRelationshipHostingType());
		// Adds UnknownAttributes
		serializeUnknownAttributes(rmData,serializer);
		// Host
		HostMData host = rmData.getHost();
		if (host != null) {
			serializeHostMData(host,serializer, helper);
		}
		// Hosted
		DataStructure hosted = rmData.getHosted();
		if (hosted != null) {
			for (Iterator it = hosted.iterator(); it.hasNext();) {
				HostedMData hostedData = (HostedMData) it.next();
				serializeHostedMData(hostedData,serializer, helper);
			}
		}
		// Adds UnknownElements
		serializeUnknownElements(rmData,serializer);
		// EndTag => dpws:Relationship
		serializer.endTag(helper.getDPWSNamespace(), DPWSConstants.DPWS_ELEM_RELATIONSHIP);
		
	}

}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.InputStream;

import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.message.Message;

public interface SOAP2MessageGenerator {

	/**
	 * This method generates message objects from the given input stream.
	 * 
	 * @param in the stream to read input from
	 * @return complete message object - needs to be casted: use getType()
	 *         method
	 */
	public Message generate(InputStream in) throws Exception;

	/**
	 * Delivers a single incoming message obtained from reading <code>in</code>
	 * to <code>to</code>. Uses default {@link MessageDiscarder}.
	 * 
	 * @param in the stream from which to parse the message
	 * @param to the receiver to deliver the message to
	 * @param protocolData transport-related information attached to the message
	 *            being received; it is passed directly to one of the
	 *            <code>receive()</code> methods of the specified
	 *            {@link MessageReceiver} <code>to</code>
	 */
	public void deliver(InputStream in, MessageReceiver to, DPWSProtocolData protocolData);

	/**
	 * Delivers a single incoming message obtained from reading <code>in</code>
	 * to <code>to</code>.
	 * 
	 * @param in the stream from which to parse the message
	 * @param to the receiver to deliver the message to
	 * @param protocolData transport-related information attached to the message
	 *            being received; it is passed directly to one of the
	 *            <code>receive()</code> methods of the specified
	 *            {@link MessageReceiver} <code>to</code>
	 * @param discarder decides whether to deliver or drop the message
	 */
	public void deliver(InputStream in, MessageReceiver to, DPWSProtocolData protocolData, MessageDiscarder discarder, Message request);

	/**
	 * @param is
	 * @return
	 * @throws Exception
	 */
	public Message generateMessage(InputStream is) throws Exception;

	/**
	 * @param bodyInputStream
	 * @param r
	 * @param protocolData
	 */
	public void deliverMessage(InputStream bodyInputStream, MessageReceiver r, DPWSProtocolData protocolData, Message request);

	/**
	 * @param in
	 * @param r
	 * @param protocolData
	 * @param discarder
	 */
	public void deliverMessage(InputStream in, MessageReceiver r, DPWSProtocolData protocolData, MessageDiscarder discarder);
	
	public abstract SOAP2BasicTypesConverter getBasicTypesParser();
	public abstract SOAP2ParameterValueConverter getParameterValueParser();

}

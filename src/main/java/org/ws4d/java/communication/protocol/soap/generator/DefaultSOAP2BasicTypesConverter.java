/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.DPWSConstants2006;
import org.ws4d.java.constants.WSAConstants;
import org.ws4d.java.constants.WSAConstants2006;
import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.constants.WSDConstants2006;
import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.io.xml.AbstractElementParser;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Set;
import org.ws4d.java.types.AppSequence;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.Delivery;
import org.ws4d.java.types.DiscoveryData;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.EndpointReference2004;
import org.ws4d.java.types.EndpointReferenceSet;
import org.ws4d.java.types.Filter;
import org.ws4d.java.types.HostMData;
import org.ws4d.java.types.HostedMData;
import org.ws4d.java.types.MetadataMData;
import org.ws4d.java.types.ProbeScopeSet;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.ReferenceParametersMData;
import org.ws4d.java.types.ReferenceParametersMData.ReferenceParameter;
import org.ws4d.java.types.RelationshipMData;
import org.ws4d.java.types.ScopeSet;
import org.ws4d.java.types.ThisDeviceMData;
import org.ws4d.java.types.ThisModelMData;
import org.ws4d.java.types.URI;
import org.ws4d.java.types.URISet;
import org.ws4d.java.types.UnknownDataContainer;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class DefaultSOAP2BasicTypesConverter implements SOAP2BasicTypesConverter{

	
	@Override
	public AttributedURI parseAttributedURI(ElementParser parser, ConstantsHelper helper) throws IOException{
		
		String elementName=null;
		try{
			elementName=parser.getName();
			AttributedURI result;
			int attributeCount = parser.getAttributeCount();
			if (attributeCount > 0) {
				HashMap attributes = new HashMap();
				for (int i = 0; i < attributeCount; i++) {
					String namespace = parser.getAttributeNamespace(i);
					if ("".equals(namespace)) {
						// default to namespace of containing element
						namespace = parser.getNamespace();
					}
					String name = parser.getAttributeName(i);
					String value = parser.getAttributeValue(i);
					attributes.put(QNameFactory.getInstance().getQName(name, namespace), value);
				}
				result = new AttributedURI(parser.nextText().trim(), attributes);
			} else {
				result = new AttributedURI(parser.nextText().trim());
			}
			return result;
		}catch(Exception e){
			throw new IOException("Could not read attributed URI from element ["+elementName+"]:"+e.getMessage());
		}
	}

	
	@Override
	public EndpointReference parseEndpointReference(int dpwsVersion,
			ElementParser parser, ConstantsHelper helper) throws IOException {
		try{
			return parseEndpointReferenceInternal(dpwsVersion,parser, helper);
		}catch(Exception e)
		{
			throw new IOException(e.getMessage());
		}
	}

	protected EndpointReference2004 parseEPR2004(ElementParser parser, ConstantsHelper helper)throws XmlPullParserException, IOException
	{
		// handle attributes
		int attributeCount = parser.getAttributeCount();
		HashMap unknownAttributes = null;
		if (attributeCount > 0) {
			unknownAttributes = new HashMap();
			for (int i = 0; i < attributeCount; i++) {
				String namespace = parser.getAttributeNamespace(i);
				if ("".equals(namespace)) {
					// default to namespace of containing element
					namespace = parser.getNamespace();
				}
				String name = parser.getAttributeName(i);
				String value = parser.getAttributeValue(i);
				unknownAttributes.put(QNameFactory.getInstance().getQName(name, namespace), value);
			}
		}
		AttributedURI address = null;
		ReferenceParametersMData properties = null;
		ReferenceParametersMData parameters = null;
		// MetadataMData metadata = null;
		HashMap unknownElements = null;
		QName portType = null;
		QName serviceName = null;
		String portName = null;
		while (parser.nextTag() != XmlPullParser.END_TAG) {
			String namespace = parser.getNamespace();
			String name = parser.getName();
			if (WSAConstants2006.WSA_NAMESPACE_NAME.equals(namespace)) {
				if (WSAConstants.WSA_ELEM_ADDRESS.equals(name)) {
					address = parseAttributedURI(parser, helper);
				} else if (WSAConstants2006.WSA_ELEM_REFERENCE_PROPERTIES.equals(name)) {
					properties = parseReferenceParametersMData(parser, helper);
				} else if (WSAConstants.WSA_ELEM_REFERENCE_PARAMETERS.equals(name)) {
					parameters = parseReferenceParametersMData(parser, helper);
				} else if (WSAConstants2006.WSA_ELEM_PORT_TYPE.equals(name)) {
					portType = parser.nextQName();
				} else if (WSAConstants2006.WSA_ELEM_SERVICE_NAME.equals(name)) {
					ArrayList list = parseServiceName(parser, helper);
					portName = (String) list.get(0);
					serviceName = (QName) list.get(1);
				} else if (WSAConstants2006.WSA_ELEM_POLICY.equals(name)) {
					// ergaenzen
				} else {
					QName elementName = QNameFactory.getInstance().getQName(name, namespace);
					Object result = parser.chainHandler(elementName);
					if (result != null) {
						if (unknownElements == null) {
							unknownElements = new HashMap();
						}
						DataStructure elements = (DataStructure) unknownElements.get(elementName);
						if (elements == null) {
							elements = new ArrayList();
							unknownElements.put(elementName, elements);
						}
						elements.add(result);
					}
				}
			}
		}
		EndpointReference2004 er = new EndpointReference2004(address, parameters, properties, portType, serviceName, portName);
		if (unknownAttributes != null) {
			er.setUnknownAttributes(unknownAttributes);
		}
		if (unknownElements != null) {
			er.setUnknownElements(unknownElements);
		}
		return er;
	}


	@Override
	public ReferenceParametersMData parseReferenceParametersMData(
			ElementParser parentParser, ConstantsHelper helper) throws IOException {
		try{
			ReferenceParametersMData parameters = new ReferenceParametersMData();
			parseUnknownAttributes(parameters,parentParser);
			int event = parentParser.nextTag(); // go to first child
			int depth = parentParser.getDepth();
			String namespace = parentParser.getNamespace();
			String name = parentParser.getName();
			if (event == XmlPullParser.END_TAG && (WSAConstants.WSA_NAMESPACE_NAME.equals(namespace) || WSAConstants2006.WSA_NAMESPACE_NAME.equals(namespace)) && WSAConstants.WSA_ELEM_REFERENCE_PARAMETERS.equals(name)) {
				// empty but existing reference parameters block
				return parameters;
			}
			ElementParser parser = new ElementParser(parentParser);
			ReferenceParameter currentParameter = null;
			boolean onTopLevel = true;
			StringBuffer result = new StringBuffer();
			while (true) {
				do {
					switch (event) {
					case (XmlPullParser.START_TAG): {
						namespace = parser.getNamespace();
						name = parser.getName();
						if (onTopLevel) {
							if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace) && WSEConstants.WSE_ELEM_IDENTIFIER.equals(name)) {
								parameters.setWseIdentifier(new URI(parser.nextText().trim()));
								continue;
							}
							QName elementName = QNameFactory.getInstance().getQName(name, namespace);
							Object obj = parentParser.chainHandler(elementName, false);
							if (obj != null) {
								parameters.addUnknownElement(elementName, obj);
								continue;
							}
							// 1st chunk = '<' literal (statically known)
							// 2nd chunk = element namespace
							// 3rd chunk = ':' literal + element name
							// 4th chunk = bulk char data
							// 5th chunk = next attribute/element's namespace
							// 6th chunk = see 4th chunk
							// 7th chunk = see 5th chunk
							currentParameter = new ReferenceParameter(namespace, name);
							parameters.add(currentParameter);
						} else {
							result.append('<');
							currentParameter.appendChunk(result.toString());
							result = new StringBuffer();
							currentParameter.appendChunk(namespace);
							result.append(':').append(name);
						}

						int attrCount = parser.getAttributeCount();
						for (int i = 0; i < attrCount; i++) {
							result.append(' ');
							String prefix = parser.getAttributePrefix(i);
							String attribute = parser.getAttributeName(i);
							if (prefix == null) {
								// assume same attribute namespace as element
								if ((WSAConstants.WSA_NAMESPACE_NAME.equals(namespace) || WSAConstants2006.WSA_NAMESPACE_NAME.equals(namespace)) && WSAConstants.WSA_ATTR_IS_REFERENCE_PARAMETER.equals(attribute)) {
									// skip wsa:IsReferenceParameter
									continue;
								}
							} else {
								String attributeNamespace = parser.getAttributeNamespace(i);
								if ((WSAConstants.WSA_NAMESPACE_NAME.equals(attributeNamespace) || WSAConstants2006.WSA_NAMESPACE_NAME.equals(attributeNamespace)) && WSAConstants.WSA_ATTR_IS_REFERENCE_PARAMETER.equals(attribute)) {
									// skip wsa:IsReferenceParameter
									continue;
								}
								currentParameter.appendChunk(result.toString());
								currentParameter.appendChunk(attributeNamespace);
								result = new StringBuffer();
								result.append(':');
							}
							String value = parser.getAttributeValue(i);
							result.append(attribute).append("=\"").append(value).append('\"');
						}
						result.append('>');
						onTopLevel = false;
						break;
					}
					case (XmlPullParser.TEXT): {
						result.append(parser.getText().trim());
						break;
					}
					case (XmlPullParser.END_TAG): {
						result.append("</");
						currentParameter.appendChunk(result.toString());
						currentParameter.appendChunk(parser.getNamespace());
						result = new StringBuffer();
						result.append(':').append(parser.getName()).append('>');
						break;
					}
					}
				} while ((event = parser.next()) != XmlPullParser.END_DOCUMENT);
				event = parentParser.nextTag();
				if (parentParser.getDepth() == depth) {
					// next reference parameter starts
					parser = new ElementParser(parentParser);
					currentParameter.appendChunk(result.toString());
					result = new StringBuffer();
					onTopLevel = true;
				} else {
					// reference parameters end tag
					break;
				}
			}
			if (currentParameter != null) {
				currentParameter.appendChunk(result.toString());
			}
			return parameters;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}

	protected ArrayList parseServiceName(ElementParser parentParser, ConstantsHelper helper) throws XmlPullParserException, IOException {
		ArrayList list = new ArrayList();
		ElementParser parser = new ElementParser(parentParser);
		int attributeCount = parser.getAttributeCount();
		if (attributeCount > 0) {
			String namespace = parser.getAttributeNamespace(0);
			if ("".equals(namespace)) {
				// default to namespace of containing element
				namespace = parser.getNamespace();
			}
			String value = parser.getAttributeValue(0);
			list.add(value);
		}
		QName serviceName = parser.nextQName();
		list.add(serviceName);
		return list;
	}

	protected EndpointReference parseEPR(ElementParser parser, ConstantsHelper helper)throws XmlPullParserException, IOException
	{
		// handle attributes
		int attributeCount = parser.getAttributeCount();
		HashMap unknownAttributes = null;
		if (attributeCount > 0) {
			unknownAttributes = new HashMap();
			for (int i = 0; i < attributeCount; i++) {
				String namespace = parser.getAttributeNamespace(i);
				if ("".equals(namespace)) {
					// default to namespace of containing element
					namespace = parser.getNamespace();
				}
				String name = parser.getAttributeName(i);
				String value = parser.getAttributeValue(i);
				unknownAttributes.put(QNameFactory.getInstance().getQName(name, namespace), value);
			}
		}
		AttributedURI address = null;
		ReferenceParametersMData parameters = null;
		MetadataMData metadata = null;
		HashMap unknownElements = null;
		while (parser.nextTag() != XmlPullParser.END_TAG) {
			String namespace = parser.getNamespace();
			String name = parser.getName();
			if (WSAConstants.WSA_NAMESPACE_NAME.equals(namespace)) {
				if (WSAConstants.WSA_ELEM_ADDRESS.equals(name)) {
					address = parseAttributedURI(parser, helper);
				} else if (WSAConstants.WSA_ELEM_REFERENCE_PARAMETERS.equals(name)) {
					parameters = parseReferenceParametersMData(new ElementParser(parser), helper);
				} else if (WSAConstants.WSA_ELEM_METADATA.equals(name)) {
					metadata = new MetadataMData();
					parser.nextGenericElement(metadata);
				} else {
					QName elementName = QNameFactory.getInstance().getQName(name, namespace);
					Object result = parser.chainHandler(elementName);
					if (result != null) {
						if (unknownElements == null) {
							unknownElements = new HashMap();
						}
						DataStructure elements = (DataStructure) unknownElements.get(elementName);
						if (elements == null) {
							elements = new ArrayList();
							unknownElements.put(elementName, elements);
						}
						elements.add(result);
					}
				}
			}
		}
		EndpointReference epr = new EndpointReference(address, parameters, metadata);
		if (unknownAttributes != null) {
			epr.setUnknownAttributes(unknownAttributes);
		}
		if (unknownElements != null) {
			epr.setUnknownElements(unknownElements);
		}

		return epr;
	}

	/**
	 * The method returns an EndpointReference for DPWS2009 if newAddressing is
	 * "true", else if newAddressing ist "false" it returns an EnpointReference
	 * for DPWS2006.
	 * 
	 * @param addressingVersion , int that gives info about the Addressing
	 *            Version
	 * @return EndpointReference
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	protected EndpointReference parseEndpointReferenceInternal(int dpwsVersion, ElementParser parser, ConstantsHelper helper) throws XmlPullParserException, IOException {

		switch (dpwsVersion) {
		case DPWSConstants.DPWS_VERSION2009:
			return parseEPR(parser, helper);
		case DPWSConstants2006.DPWS_VERSION2006:
			return parseEPR2004(parser, helper);
		default:
			throw new IllegalArgumentException("Unsupported DPWS Version");
		}
	}

	
	@Override
	public AppSequence parseAppSequence(ElementParser parser,
			ConstantsHelper helper) throws IOException {
		try{
			int attributeCount = parser.getAttributeCount();
			if (attributeCount > 0) {
				// InstanceID MUST be present and it MUST be an unsigned int
				long instanceId = -1L;
				String sequenceId = null;
				// MessageNumber MUST be present and it MUST be an unsigned int
				long messageNumber = -1L;
				HashMap attributes = new HashMap();
				for (int i = 0; i < attributeCount; i++) {
					String namespace = parser.getAttributeNamespace(i);
					if ("".equals(namespace)) {
						// default to namespace of containing element
						namespace = parser.getNamespace();
					}
					String name = parser.getAttributeName(i);
					String value = parser.getAttributeValue(i);
					if (WSDConstants.WSD_NAMESPACE_NAME.equals(namespace) || WSDConstants2006.WSD_NAMESPACE_NAME.equals(namespace)) {
						if (WSDConstants.WSD_ATTR_INSTANCEID.equals(name)) {
							try {
								instanceId = Long.parseLong(value);
							} catch (NumberFormatException e) {
								throw new XmlPullParserException("AppSequence@InstanceId is not a number: " + value);
							}
						} else if (WSDConstants.WSD_ATTR_SEQUENCEID.equals(name)) {
							sequenceId = value;
						} else if (WSDConstants.WSD_ATTR_MESSAGENUMBER.equals(name)) {
							try {
								messageNumber = Long.parseLong(value);
							} catch (NumberFormatException e) {
								throw new XmlPullParserException("AppSequence@MessageNumber is not a number: " + value);
							}
						} else {
							attributes.put(QNameFactory.getInstance().getQName(name, namespace), value);
						}
					} else {
						attributes.put(QNameFactory.getInstance().getQName(name, namespace), value);
					}
				}
				if (instanceId == -1L) {
					throw new XmlPullParserException("AppSequence@InstanceId missing");
				}
				if (messageNumber == -1L) {
					throw new XmlPullParserException("AppSequence@MessageNumber missing");
				}
				AppSequence appSequence = new AppSequence(instanceId, sequenceId, messageNumber);
				appSequence.setUnknownAttributes(attributes);
				while (parser.nextTag() == XmlPullParser.START_TAG) {
					// fill-up child elements
					String namespace = parser.getNamespace();
					String name = parser.getName();
					parser.addUnknownElement(appSequence, namespace, name);
				}
				return appSequence;
			}
			throw new XmlPullParserException("Invalid AppSequence: no attributes");
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}

	
	@Override
	public DiscoveryData parseDiscoveryData(DiscoveryData dData, ElementParser parser,ConstantsHelper helper) throws IOException {
		DiscoveryData retVal;
		if (dData==null)
			retVal=new DiscoveryData();
		else
			retVal=dData;
		
		try{
			parseUnknownAttributes(retVal,parser);

			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new IOException("Discovery Data Element is empty");
			}
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (helper.getWSANamespace().equals(namespace)) {
					if (WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE.equals(name)) {
						retVal.setEndpointReference(parseEndpointReference(helper.getDPWSVersion(), parser, helper));
					} else {
						parseUnknownElement(retVal,parser, namespace, name);
					}
				} else if (helper.getWSDNamespace().equals(namespace)) {
					if (WSDConstants.WSD_ELEMENT_TYPES.equals(name)) {
						retVal.setTypes(parseQNameSet(parser));
					} else if (WSDConstants.WSD_ELEMENT_SCOPES.equals(name)) {
						retVal.setScopes(parseScopeSet(parser));
					} else if (WSDConstants.WSD_ELEMENT_XADDRS.equals(name)) {
						retVal.setXAddrs(parseURISet(parser));
					} else if (WSDConstants.WSD_ELEMENT_METADATAVERSION.equals(name)) {
						String value = parser.nextText();
						long metadataVersion = 0L;
						try {
							metadataVersion = Long.parseLong(value.trim());
						} catch (NumberFormatException e) {
							throw new XmlPullParserException("Discovery Data Element /MetadataVersion is not a number: " + value);
						}
						retVal.setMetadataVersion(metadataVersion);
					} else {
						parseUnknownElement(retVal,parser, namespace, name);
					}
				} else {
					parseUnknownElement(retVal,parser, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
		return retVal;
	}

	/**
	 * Parse all UnknownAttributes
	 * 
	 * @param container
	 * @param namespace
	 * @param name
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	@Override
	public UnknownDataContainer parseUnknownAttributes(UnknownDataContainer container, ElementParser parser) 
	{
		if (container==null) container=new UnknownDataContainer();

		int count = parser.getAttributeCount();
		for (int i = 0; i < count; i++) {
			String namespace = parser.getAttributeNamespace(i);
			if ("".equals(namespace)) {
				// default to namespace of containing element
				namespace = parser.getNamespace();
			}
			String name = parser.getAttributeName(i);
			String value = parser.getAttributeValue(i);
			container.addUnknownAttribute(QNameFactory.getInstance().getQName(name, namespace), value);
		}
		return container;
	}

	/**
	 * Parse all UnknownElements
	 * 
	 * @param container
	 * @param namespace
	 * @param name
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	@Override
	public UnknownDataContainer parseUnknownElement(UnknownDataContainer container, ElementParser parser, String namespace, String name) throws XmlPullParserException, IOException {
		if (container==null) container=new UnknownDataContainer();
		QName childName = QNameFactory.getInstance().getQName(name, namespace);
		Object value = parser.chainHandler(childName);
		if (value != null) {
			container.addUnknownElement(childName, value);
		}
		return container;
	}

	/**
	 * Parse the QNameSet-Element
	 * 
	 * @return the next {@link QNameSet}
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	@Override
	public QNameSet parseQNameSet(ElementParser parser)throws XmlPullParserException, IOException {
		QNameSet qNameSet = new QNameSet();
		String value = parser.nextText();
		int pos1 = -1;
		int pos2 = pos1;
		do {
			pos1 = AbstractElementParser.nextNonWhiteSpace(value, pos1);
			if (pos1 == -1) {
				break;
			}
			pos2 = AbstractElementParser.nextWhiteSpace(value, pos1);
			if (pos2 == -1) {
				pos2 = value.length();
			}
			String rawQName = value.substring(pos1, pos2);
			qNameSet.add(parser.createQName(rawQName));
			pos1 = pos2;
		} while (pos1 != -1);
		return qNameSet;
	}

	@Override
	public URISet parseURISet(ElementParser parser) throws XmlPullParserException, IOException {
		URISet uriSet = new URISet();
		String value = parser.nextText();
		int pos1 = -1;
		int pos2 = pos1;
		do {
			pos1 = AbstractElementParser.nextNonWhiteSpace(value, pos1);
			if (pos1 == -1) {
				break;
			}
			pos2 = AbstractElementParser.nextWhiteSpace(value, pos1);
			if (pos2 == -1) {
				pos2 = value.length();
			}
			String uri = value.substring(pos1, pos2);
			uriSet.add(new URI(uri));
			pos1 = pos2;
		} while (pos1 != -1);
		return uriSet;
	}

	@Override
	public ScopeSet parseScopeSet(ElementParser parser) throws IOException {
		try{
			ScopeSet scopes = new ScopeSet();
			String value = parser.nextText();
			int pos1 = -1;
			int pos2 = pos1;
			do {
				pos1 = ElementParser.nextNonWhiteSpace(value, pos1);
				if (pos1 == -1) {
					break;
				}
				pos2 = ElementParser.nextWhiteSpace(value, pos1);
				if (pos2 == -1) {
					pos2 = value.length();
				}
				String uri = value.substring(pos1, pos2);
				scopes.addScope(uri);
				pos1 = pos2;
			} while (pos1 != -1);

			return scopes;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}

	
	@Override
	public Delivery parseDelivery(ElementParser parser, ConstantsHelper helper) throws XmlPullParserException, IOException {
		Delivery delivery = new Delivery();

		int attributeCount = parser.getAttributeCount();
		for (int i = 0; i < attributeCount; i++) {
			String namespace = parser.getAttributeNamespace(i);
			if ("".equals(namespace)) {
				// default to namespace of containing element
				namespace = parser.getNamespace();
			}
			String name = parser.getAttributeName(i);
			String value = parser.getAttributeValue(i);
			if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace) && WSEConstants.WSE_ATTR_DELIVERY_MODE.equals(name)) {
				delivery.setMode(new URI(value));
			} else {
				delivery.addUnknownAttribute(QNameFactory.getInstance().getQName(name, namespace), value);
			}
		}

		while (parser.nextTag() != XmlPullParser.END_TAG) {
			String namespace = parser.getNamespace();
			String name = parser.getName();
			if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
				if (WSEConstants.WSE_ELEM_NOTIFYTO.equals(name)) {
					delivery.setNotifyTo(parseEndpointReference(helper.getDPWSVersion(), parser, helper));
				} else {
					parseUnknownElement(delivery,parser, namespace, name);
				}
			} else {
				parseUnknownElement(delivery,parser, namespace, name);
			}
		}
		return delivery;
	}

	
	@Override
	public Filter parseFilter(ElementParser parser, ConstantsHelper helper)
	throws IOException {
		try{
			Filter filter = new Filter();

			int attributeCount = parser.getAttributeCount();
			for (int i = 0; i < attributeCount; i++) {
				String namespace = parser.getAttributeNamespace(i);
				if ("".equals(namespace)) {
					// default to namespace of containing element
					namespace = parser.getNamespace();
				}
				String name = parser.getAttributeName(i);
				String value = parser.getAttributeValue(i);
				if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace) && WSEConstants.WSE_ATTR_FILTER_DIALECT.equals(name)) {
					filter.setDialect(new URI(value));
				} else {
					filter.addUnknownAttribute(QNameFactory.getInstance().getQName(name, namespace), value);
				}
			}
			filter.setActions(parseURISet(parser));

			return filter;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}

	
	@Override
	public ProbeScopeSet parseProbeScopeSet(ElementParser parser) throws  IOException {
		ProbeScopeSet scopeSet = new ProbeScopeSet();
		int attributeCount = parser.getAttributeCount();
		String matchBy = WSDConstants.WSD_MATCHING_RULE_DEFAULT;
		for (int i = 0; i < attributeCount; i++) 
		{
			String namespace = parser.getAttributeNamespace(i);
			if ("".equals(namespace)) {
				// default to namespace of containing element
				namespace = parser.getNamespace();
			}
			String name = parser.getAttributeName(i);
			String value = parser.getAttributeValue(i);
			if (WSDConstants.WSD_NAMESPACE_NAME.equals(namespace) && WSDConstants.WSD_ATTR_MATCH_BY.equals(name)) {
				matchBy = value;
			} else {
				scopeSet.addUnknownAttribute(QNameFactory.getInstance().getQName(name, namespace), value);
			}
		}
		scopeSet.setMatchBy(matchBy);
		scopeSet.addAll(parseScopeSet(parser));
		return scopeSet;
	}

	
	@Override
	public HostMData parseHostMData(ElementParser parser, ConstantsHelper helper)
	throws IOException {
		try{
			HostMData host = new HostMData();

			parseUnknownAttributes(host,parser);

			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new XmlPullParserException("Host is empty");
			}
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (helper.getWSANamespace().equals(namespace)) {
					if (WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE.equals(name)) {
						host.setEndpointReference(parseEndpointReference(helper.getDPWSVersion(), parser, helper));
					} else {
						parseUnknownElement(host,parser, namespace, name);
					}
				} else if (helper.getDPWSNamespace().equals(namespace)) {
					if (DPWSConstants.DPWS_ELEM_TYPES.equals(name)) {
						host.setTypes(parseQNameSet(parser));
					} else {
						parseUnknownElement(host,parser, namespace, name);
					}
				} else {
					parseUnknownElement(host, parser, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);
			return host;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}

	
	@Override
	public HostedMData parseHostedMData(ElementParser parser,
			ConstantsHelper helper) throws IOException {


		try{
			HostedMData hosted = new HostedMData();
			parseUnknownAttributes(hosted,parser);

			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new XmlPullParserException("Hosted is empty");
			}
			Set references = null;
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (helper.getWSANamespace().equals(namespace)) {
					if (WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE.equals(name)) {
						if (references == null) {
							references = new HashSet();
						}
						references.add(parseEndpointReference(helper.getDPWSVersion(), parser,null));
					} else {
						parseUnknownElement(hosted,parser, namespace, name);
					}
				} else if (helper.getDPWSNamespace().equals(namespace)) {
					if (DPWSConstants.DPWS_ELEM_TYPES.equals(name)) {
						hosted.setTypes(parseQNameSet(parser));
					} else if (DPWSConstants.DPWS_ELEM_SERVICEID.equals(name)) {
						hosted.setServiceId(new URI(parser.nextText().trim()));
					} else {
						parseUnknownElement(hosted, parser, namespace, name);
					}
				} else {
					parseUnknownElement(hosted,parser, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);
			if (references != null) {
				hosted.setEndpointReferences(new EndpointReferenceSet(references));
			}
			return hosted;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}

	
	@Override
	public ThisDeviceMData parseThisDeviceMData(ElementParser parser,
			ConstantsHelper helper) throws IOException {
		try{
			ThisDeviceMData thisDevice = new ThisDeviceMData();

			parser.nextTag(); // go to ThisDevice

			parseUnknownAttributes(thisDevice, parser);

			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new XmlPullParserException("ThisDevice is empty");
			}
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (helper.getDPWSNamespace().equals(namespace)) {
					if (DPWSConstants.DPWS_ELEM_FRIENDLYNAME.equals(name)) {
						thisDevice.addFriendlyName(parser.nextLocalizedString());
					} else if (DPWSConstants.DPWS_ELEM_FIRMWAREVERSION.equals(name)) {
						thisDevice.setFirmwareVersion(parser.nextText().trim());
					} else if (DPWSConstants.DPWS_ELEM_SERIALNUMBER.equals(name)) {
						thisDevice.setSerialNumber(parser.nextText().trim());
					} else {
						parseUnknownElement(thisDevice,parser, namespace, name);
					}
				} else {
					parseUnknownElement(thisDevice,parser, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);

			parser.nextTag(); // go to closing MetadataSection
			return thisDevice;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}

	
	@Override
	public ThisModelMData parseThisModelMData(ElementParser parser,
			ConstantsHelper helper) throws IOException {
		try{
			ThisModelMData thisModel = new ThisModelMData();

			parser.nextTag(); // go to ThisModel

			parseUnknownAttributes(thisModel,parser);

			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new XmlPullParserException("ThisModel is empty");
			}
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (helper.getDPWSNamespace().equals(namespace)) {
					if (DPWSConstants.DPWS_ELEM_MANUFACTURER.equals(name)) {
						thisModel.addManufacturerName(parser.nextLocalizedString());
					} else if (DPWSConstants.DPWS_ELEM_MANUFACTURERURL.equals(name)) {
						thisModel.setManufacturerUrl(new URI(parser.nextText().trim()));
					} else if (DPWSConstants.DPWS_ELEM_MODELNAME.equals(name)) {
						thisModel.addModelName(parser.nextLocalizedString());
					} else if (DPWSConstants.DPWS_ELEM_MODELNUMBER.equals(name)) {
						thisModel.setModelNumber(parser.nextText().trim());
					} else if (DPWSConstants.DPWS_ELEM_MODELURL.equals(name)) {
						thisModel.setModelUrl(new URI(parser.nextText().trim()));
					} else if (DPWSConstants.DPWS_ELEM_PRESENTATIONURL.equals(name)) {
						thisModel.setPresentationUrl(new URI(parser.nextText().trim()));
					} else {
						parseUnknownElement(thisModel,parser, namespace, name);
					}
				} else {
					parseUnknownElement(thisModel,parser, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);

			parser.nextTag(); // go to closing MetadataSection
			return thisModel;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}

	
	@Override
	public RelationshipMData parseRelationshipMData(ElementParser parser,
			ConstantsHelper helper) throws IOException {
		try{
		parser.nextTag(); // go to Relationship

		// get attribute Type and decide upon it
		String type = parser.getAttributeValue(null, DPWSConstants.DPWS_RELATIONSHIP_ATTR_TYPE);
		if (helper.getMetadataRelationshipHostingType().equals(type)) {
			RelationshipMData relationship = new RelationshipMData();
			relationship.setType(new URI(helper.getMetadataRelationshipHostingType()));

			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new XmlPullParserException("Relationship is empty");
			}
			DataStructure hosted = null;
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (helper.getDPWSNamespace().equals(namespace)) {
					if (DPWSConstants.DPWS_RELATIONSHIP_ELEM_HOST.equals(name)) {
						relationship.setHost(parseHostMData(parser, helper));
					} else if (DPWSConstants.DPWS_RELATIONSHIP_ELEM_HOSTED.equals(name)) {
						if (hosted == null) {
							hosted = new ArrayList();
						}
						hosted.add(parseHostedMData(parser, helper));
					} else {
						parseUnknownElement(relationship,parser, namespace, name);
					}
				} else {
					parseUnknownElement(relationship,parser, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);
			if (hosted != null) {
				relationship.setHosted(hosted);
			}

			parser.nextTag(); // go to closing MetadataSection
			return relationship;
		} else {
			// wrong type
			throw new VersionMismatchException("Wrong Type Attribute");
		}
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}



}

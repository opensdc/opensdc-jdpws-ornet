/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.message.SOAPHeader;

/**
 * Implementations of this interface are queried each time a message is to be
 * created within {@link SOAP2MessageGenerator}.
 */
public interface MessageDiscarder {

	public int	NOT_DISCARDED			= 0;

	public int	OWN_MESSAGE				= 1;

	public int	DUPLICATE_MESSAGE		= 2;

	public int	NOT_RELEVANT_MESSAGE	= 3;

	public int	VERSION_NOT_SUPPORTED	= 4;

	/**
	 * Returns <code>true</code> in cases where the message with the given SOAP
	 * <code>header</code> and with the associated transport information
	 * described by <code>protocolData</code> should not be further processed
	 * (i.e. it should be discarded immediately).
	 * 
	 * @param header the header of the message
	 * @param protocolData transport-related addressing information attached to
	 *            the message with the given <code>header</code>
	 * @return whether to discard the message or not
	 *         <p>
	 *         <ul>
	 *         <li>0 = message not discarded</li>
	 *         <li>1 = message discarded because it was an own message send and
	 *         received by the framework</li>
	 *         <li>2 = message discarded because it an earlier version of that
	 *         message was already parsed</li>
	 *         </ul>
	 *         </p>
	 */
	public int discardMessage(SOAPHeader header, DPWSProtocolData protocolData);

}

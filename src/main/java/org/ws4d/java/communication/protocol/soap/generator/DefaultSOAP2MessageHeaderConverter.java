/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.communication.DPWSProtocolVersionInfo;
import org.ws4d.java.communication.DPWS2006.DefaultConstantsHelper2006;
import org.ws4d.java.communication.DPWS2009.DefaultConstantsHelper2009;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.constants.WSAConstants;
import org.ws4d.java.constants.WSAConstants2006;
import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.constants.WSSecurityConstants;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.message.SOAPHeader;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class DefaultSOAP2MessageHeaderConverter implements
SOAP2MessageHeaderConverter {

	private SOAP2BasicTypesConverter typesConverter;

	public DefaultSOAP2MessageHeaderConverter(
			SOAP2BasicTypesConverter basicTypesconverter) {
		this.typesConverter=basicTypesconverter;
	}

	public SOAP2BasicTypesConverter getTypesConverter() {
		return typesConverter;
	}

	
	@Override
	public  SOAPHeader parse(ElementParser parser, ConstantsHelper helper) throws IOException,VersionMismatchException {

		try{
			SOAPHeader header = new SOAPHeader();
			parser.handleUnknownAttributes(header);

			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new IOException("SOAP Header is empty");
			}
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (WSAConstants.WSA_NAMESPACE_NAME.equals(namespace)) {
					if (DPWSProperties.getInstance().getSupportedDPWSVersions().contains(DPWSProtocolVersionInfo.DPWS1_1)) {
						header.setVersion(DPWSProtocolVersionInfo.DPWS1_1);
						helper = DefaultConstantsHelper2009.getInstance();
						parseAddressingHeader(parser, header, name, namespace, helper);
					} else {
						throw new VersionMismatchException("WS-Addressing: " + WSAConstants.WSA_NAMESPACE_NAME + " is not supported in this Configuration");
					}
				} else if (WSAConstants2006.WSA_NAMESPACE_NAME.equals(namespace)) {
					if (DPWSProperties.getInstance().getSupportedDPWSVersions().contains(DPWSProtocolVersionInfo.DPWS2006)) {
						header.setVersion(DPWSProtocolVersionInfo.DPWS2006);
						helper = DefaultConstantsHelper2006.getInstance();
						parseAddressingHeader(parser, header, name, namespace, helper);
					} else {
						throw new VersionMismatchException("WS-Addressing: " + WSAConstants2006.WSA_NAMESPACE_NAME + " is not supported in this Configuration");
					}
				} else if (WSSecurityConstants.XML_SOAP_DISCOVERY.equals(namespace)) { //TODO SSch Check Namespace for compact signature 
					//Re-Modularization 2011-01-21 Implement SecMod
					parseCompactSignature(name, namespace, parser, helper, header);
				} else if (WSDConstants.WSD_NAMESPACE_NAME.equals(namespace)) {
					if (WSDConstants.WSD_ELEMENT_APPSEQUENCE.equals(name)) {
						header.setAppSequence(getTypesConverter().parseAppSequence(parser, helper));
					} else {
						this.typesConverter.parseUnknownElement(header,parser, namespace, name);
					}
				} else if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
					if (WSEConstants.WSE_ELEM_IDENTIFIER.equals(name)) {
						header.setWseIdentifier(new URI(parser.nextText().trim()));
					} else {
						this.typesConverter.parseUnknownElement(header,parser, namespace, name);
					}
				} else {
					this.typesConverter.parseUnknownElement(header,parser, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);

			if (Log.isDebug()) Log.debug("<I> Incoming " + header.getVersion().getDisplayName() + " Message, Action: " + header.getAction());

			return header;
		} catch (XmlPullParserException e) {
				throw new IOException(e.getMessage());
		}
	}

	protected void parseCompactSignature(String name, String namespace, ElementParser parser, ConstantsHelper helper, SOAPHeader header) throws XmlPullParserException, IOException {
		//TODO SSch security QoS
		if (WSSecurityConstants.COMPACT_SECURITY.equals(name) && WSSecurityConstants.XML_SOAP_DISCOVERY.equals(namespace)) 
		{
			SecurityManagerModule secMod=(SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);
			if (secMod==null)
				return;

			parser.nextTag();

			for (int i = parser.getAttributeCount() - 1; i >= 0; i--) {
				if (parser.getAttributeName(i).equals(WSSecurityConstants.COMPACT_SIG)) {
					header.setSignature(secMod.decode(parser.getAttributeValue(i)));
				}
			}
			// closing security tag and header tag
			parser.nextTag();
			parser.nextTag();
		} else if (WSDConstants.WSD_ELEMENT_APPSEQUENCE.equals(name)) {
			header.setAppSequence(this.typesConverter.parseAppSequence(parser, helper));
		} else {
			typesConverter.parseUnknownElement(header,parser, namespace, name);
		}

	}



	protected  void parseAddressingHeader(ElementParser parser, SOAPHeader header, String name, String namespace, ConstantsHelper helper) throws XmlPullParserException, IOException {
		if (WSAConstants.WSA_ELEM_ACTION.equals(name)) {
			header.setAction(getTypesConverter().parseAttributedURI(parser, helper));
		} else if (WSAConstants.WSA_ELEM_MESSAGE_ID.equals(name)) {
			header.setMessageId(getTypesConverter().parseAttributedURI(parser, helper));
		} else if (WSAConstants.WSA_ELEM_RELATESTO.equals(name)) {
			header.setRelatesTo(getTypesConverter().parseAttributedURI(parser, helper));
		} else if (WSAConstants.WSA_ELEM_REPLY_TO.equals(name)) {
			header.setReplyTo(getTypesConverter().parseEndpointReference(((DPWSProtocolVersionInfo) header.getVersion()).getDpwsVersion(), parser, helper));
		} else if (WSAConstants.WSA_ELEM_TO.equals(name)) {
			header.setTo(getTypesConverter().parseAttributedURI(parser, helper));
		} else if (WSAConstants.WSA_ELEM_SOURCE_ENDPOINT.equals(name)) {
			header.setFrom(getTypesConverter().parseAttributedURI(parser, helper));
		}  else {
			this.typesConverter.parseUnknownElement(header,parser, namespace, name);
		}
	}

}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import org.ws4d.java.structures.HashMap;

public class WSDLBindingBuilderRegistry {

	public static final WSDLBindingBuilderRegistry instance =new WSDLBindingBuilderRegistry();
	
	public static WSDLBindingBuilderRegistry getInstance() {
		return instance;
	}

	private WSDLBindingBuilderRegistry()
	{
		//bugfix: SSch nach DPWSComMgr. start() 
//		SUPPORTED_BINDING_BUILDERS.put(WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME, new SOAP12DocumentLiteralHTTPBindingBuilder());
	}
	
	// key = QName of custom binding-level extension element, value =
	// WSDLBindingBuilder instance
	private  final HashMap	SUPPORTED_BINDING_BUILDERS	= new HashMap();

	
	
	
	public WSDLBindingBuilder getBuilder(String namespace) {
		return (WSDLBindingBuilder) SUPPORTED_BINDING_BUILDERS.get(namespace);
	}

	public void addBindingBuilder(String namespace, WSDLBindingBuilder value) {
		SUPPORTED_BINDING_BUILDERS.put(namespace, value);
	}

	public Object removeBindingBuilder(String namespace) {
		return SUPPORTED_BINDING_BUILDERS.remove(namespace);
	}
}

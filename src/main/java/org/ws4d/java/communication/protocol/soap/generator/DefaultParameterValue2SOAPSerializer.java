/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;
import java.io.OutputStream;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.constants.SchemaConstants;
import org.ws4d.java.constants.XMLConstants;
import org.ws4d.java.constants.XOPConstants;
import org.ws4d.java.io.xml.XmlSerializer;
import org.ws4d.java.io.xml.XmlSerializerImplementation;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.ExtendedComplexContent;
import org.ws4d.java.schema.RestrictedComplexContent;
import org.ws4d.java.schema.Type;
import org.ws4d.java.service.parameter.AttachmentValue;
import org.ws4d.java.service.parameter.IParameterAttribute;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.parameter.ITypedParameterAttribute;
import org.ws4d.java.service.parameter.ITypedParameterValue;
import org.ws4d.java.service.parameter.ParameterDefinition;
import org.ws4d.java.service.parameter.QNameValue;
import org.ws4d.java.service.parameter.StringValue;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.List;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

public class DefaultParameterValue2SOAPSerializer implements
ParameterValue2SOAPConverter {

	protected BasicTypes2SOAPConverter btc;

	public DefaultParameterValue2SOAPSerializer(
			BasicTypes2SOAPConverter basicTypesSerializer) {
		this.btc=basicTypesSerializer;
	}


	@Override
	public void serializeParameterValue(IParameterValue pv,
			XmlSerializer serializer, ConstantsHelper helper)
					throws IOException {
		serialize(pv, serializer);
	}


	@Override
	public void serializeParameterValue(IParameterValue pv, OutputStream out,
			ConstantsHelper helper) throws IOException {
		XmlSerializer serializer = new XmlSerializerImplementation();
		serializer.setOutput(out, XMLConstants.ENCODING);
		// serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output",
		// true);
		serializer.startDocument(XMLConstants.ENCODING, null);
		if (pv.isOverriden()) {
			/*
			 * Override the given parameter. This serializes the given string
			 * and not the content of the parameter.
			 */
			serializer.ignorableWhitespace(pv.getOverride());
		}else{
			serialize0(pv,serializer, pv.getNamespaceCache(serializer.getDepth()));
		}
		serializer.endDocument();
	}

	/**
	 * The main serialize method. This method serializes the parameter.
	 * 
	 * @param serializer
	 * @param nsCache
	 * @throws IOException
	 */
	protected synchronized void serialize0(IParameterValue pv, XmlSerializer serializer, HashMap nsCache) throws IOException {
		if (nsCache == null) {
			nsCache = pv.getNamespaceCache(serializer.getDepth());
		}

		if (pv instanceof ParameterDefinition){
			serialize0ParameterDefinition((ParameterDefinition)pv, serializer, nsCache);
		}else{

			if (pv.getName()!=null)
			{
				//if we do have a name we serialize the pv
				serializeStartTag(pv,serializer, nsCache);
				serializeAttributes(pv,serializer);
				serializeChildren(pv,serializer, nsCache);
				serializeEndTag(pv,serializer);
			}else{
				//otherwise we just check if there are any children...
				serializeChildren(pv,serializer, nsCache);
			}
		}
	}

	protected final void serializeStartTag(IParameterValue pv, XmlSerializer serializer, HashMap nsCache) throws IOException {
		//TODO SSch check if nsCache has been serialized already in this run
		for (Iterator namespaceCacheIterator=nsCache.values().iterator();namespaceCacheIterator.hasNext();){
			List namespaceCacheEntryList=(List)namespaceCacheIterator.next();
			if (namespaceCacheEntryList != null) {
				Iterator it = namespaceCacheEntryList.iterator();
				while (it.hasNext()) {
					QName nsFromCacheList = (QName) it.next();
					if (nsFromCacheList!=null && nsFromCacheList.getNamespace().length()>0)
					{
						String prefix=nsFromCacheList.getPrefix();
						if (prefix==null)
						{
							prefix=serializer.getPrefix(nsFromCacheList.getNamespace(), true);
						}else{
							String documentPrefix = serializer.getPrefix(nsFromCacheList.getNamespace(),false);
							if (documentPrefix==null){
								
								serializer.setPrefix(prefix, nsFromCacheList.getNamespace());
							}
							else if (!documentPrefix.equals(prefix)){ 
							//
							}
						}
					}
				}
			}

		}

		serializer.startTag(pv.getName().getNamespace(), pv.getName().getLocalPart());

		if (pv.isNil()) {
			serializer.attribute(SchemaConstants.XSI_NAMESPACE, SchemaConstants.ATTRIBUTE_XSINIL, "true");
		}

		boolean hasTypeInfo=false;
		String currentTypeName=null;
		if (pv.hasAttributes())
		{
			QName xsiTypeName=QNameFactory.getInstance().getQName(SchemaConstants.XSI_ATTRIBUTE_TYPE, SchemaConstants.XSI_NAMESPACE);
			for(Iterator attrs= pv.attributeNames();attrs.hasNext() && !hasTypeInfo;)
			{
				QName attrName=(QName) attrs.next();
				hasTypeInfo=attrName.equals(xsiTypeName);

			}

			if (hasTypeInfo)
			{
				currentTypeName=parseXSITypeToLocalName(pv.getAttributeValue(xsiTypeName.getLocalPart()));
				pv.setAttributeValue(xsiTypeName.getLocalPart(), null);
			}
		}
		//		if (!hasTypeInfo)
		addXSITypeInformation(pv,serializer, nsCache, currentTypeName);
	}

	private String parseXSITypeToLocalName(String xsiType) {
		int index = xsiType.indexOf(":");
		if (index >= 0) {
			xsiType = xsiType.substring(index + 1);
		}
		return xsiType;
	}

	/**
	 * 
	 * @author schlichs
	 * 
	 * @param pv
	 * @param serializer
	 * @param nsCache
	 * @param currentTypeName 
	 * @throws IOException 
	 * @throws IllegalStateException 
	 * @throws IllegalArgumentException 
	 */
	private void addXSITypeInformation(IParameterValue pv,
			XmlSerializer serializer, HashMap nsCache, String currentTypeName) throws IllegalArgumentException, IllegalStateException, IOException {
		if (pv instanceof ITypedParameterValue)
		{
			ITypedParameterValue typedPV=(ITypedParameterValue)pv;
			if (typedPV.getInstanceType()!=null)
			{
				String prefix = serializer.getPrefix(typedPV.getInstanceType().getNamespace(), true);
				serializer.attribute(SchemaConstants.XSI_NAMESPACE, SchemaConstants.XSI_ATTRIBUTE_TYPE,prefix+":"+typedPV.getInstanceType().getLocalPart());
			}
		}

	}

	protected final void serializeEndTag(IParameterValue pv, XmlSerializer serializer) throws IOException {
		serializer.endTag(pv.getName().getNamespace(), pv.getName().getLocalPart());
	}

	protected final void serializeAttributes(IParameterValue pv, XmlSerializer serializer) throws IOException {
		if (pv.hasAttributes()) {
			for (Iterator it = pv.attributes(); it.hasNext();) {
				IParameterAttribute attribute = (IParameterAttribute) it.next();
				String value = attribute.getValue();
				if (value != null) {

					String namespace = determineAttributeNamespaceForSerialization(pv, attribute);
					String attrValue= determineAttributeValue(attribute,serializer);

					serializer.attribute(namespace, attribute.getName().getLocalPart(),attrValue );
				}
			}
		}
	}


	/**
	 * @param attribute
	 * @param serializer
	 * @return
	 */
	private String determineAttributeValue(IParameterAttribute attribute,
			XmlSerializer serializer) {

		String retVal=attribute.getValue();


		if (attribute instanceof ITypedParameterAttribute)
		{
			ITypedParameterAttribute typedAttr=(ITypedParameterAttribute) attribute;
			if (typedAttr.getType()!=null && typedAttr.getType()!=null && QName.QNAME.equals(typedAttr.getType().getName()))
			{
				QName t=QNameFactory.getInstance().getQName(attribute.getValue());

				String prefix = serializer.getPrefix(t.getNamespace(), true);
				retVal=prefix+":"+t.getLocalPart();


			}
		}


		return retVal;
	}


	private String determineAttributeNamespaceForSerialization(IParameterValue pv, IParameterAttribute attribute) {
		String namespace = attribute.getName().getNamespace();

		if (pv instanceof ITypedParameterValue)
		{
			ITypedParameterValue typedPV=(ITypedParameterValue)pv;
			QName pvTypeName = determinePVType(typedPV.getType());

			if (pvTypeName!=null)
			{
				String pvNamespace = pvTypeName.getNamespace();
				if (pvNamespace!=null && pvNamespace.equals(namespace) && !isAttributeReference(typedPV,attribute))
				{
					namespace=null;
				}

			}

		}
		return namespace;
	}

	/**
	 * @param pv
	 * @param attribute
	 * @return
	 */
	private boolean isAttributeReference(ITypedParameterValue pv,
			IParameterAttribute attribute) {
		boolean retVal=false;

		QName attrName = attribute.getName();

		Type pvType = pv.getType();
		retVal = isAttributeReference(attrName, pvType);
		return retVal;
	}


	private boolean isAttributeReference( QName attrName, Type pvType) {
		boolean retVal=false;
		if (pvType!=null)
		{
			Attribute schemaAttrDef = pvType.getAttribute(attrName);
			if (schemaAttrDef!=null)
			{
				retVal= schemaAttrDef.isReference();
			}else{
				if (pvType instanceof ExtendedComplexContent)
				{
					ExtendedComplexContent eccType=(ExtendedComplexContent) pvType;
					retVal=isAttributeReference(attrName,eccType.getBase());
				}else if (pvType instanceof RestrictedComplexContent)
				{
					RestrictedComplexContent rccType=(RestrictedComplexContent) pvType;
					retVal=isAttributeReference(attrName,rccType.getBase());
				}
			}
		}
		return retVal;
	}


	/**
	 * 
	 */
	private QName determinePVType(Type type) {
		QName typeName=null;
		if (type!=null)
		{
			typeName = type.getName();
			if (typeName==null && type instanceof ExtendedComplexContent)
			{
				ExtendedComplexContent eccType=(ExtendedComplexContent)type;
				Type base = eccType.getBase();
				return determinePVType(base);
			} else if (typeName==null && type instanceof RestrictedComplexContent)
			{
				RestrictedComplexContent rccType=(RestrictedComplexContent)type;
				Type base = rccType.getBase();
				return determinePVType(base);
			}
		}
		return typeName;
	}


	protected final void serializeChildren(IParameterValue pv, XmlSerializer serializer, HashMap nsCache) throws IOException {
		if (pv.hasChildren()) {
			for (Iterator it = pv.children(); it.hasNext();) {
				IParameterValue child = (IParameterValue) it.next();
				serialize(child,serializer, nsCache);
			}
		}
	}

	/**
	 * Serializes the parameter value with a XML serializer.
	 * 
	 * @param serializer the XML serializer.
	 * @throws IOException throws an exception if the parameter value could not
	 *             be serialized correctly.
	 */
	protected void serialize(IParameterValue pv,XmlSerializer serializer, HashMap nsCache) throws IOException {
		if (pv.isOverriden()) {
			/*
			 * Override the given parameter. This serializes the given string
			 * and not the content of the parameter.
			 */
			serializer.ignorableWhitespace(pv.getOverride());
			return;
		} else {
			serialize0(pv,serializer, nsCache);
		}

	}

	/**
	 * Serializes the parameter value with a XML serializer.
	 * 
	 * @param serializer the XML serializer.
	 * @throws IOException throws an exception if the parameter value could not
	 *             be serialized correctly.
	 */
	protected void serialize(IParameterValue pv, XmlSerializer serializer) throws IOException {
		if (pv.isOverriden()) {
			/*
			 * Override the given parameter. This serializes the given string
			 * and not the content of the parameter.
			 */
			serializer.ignorableWhitespace(pv.getOverride());
			return;
		} else {
			serialize0(pv,serializer, null);
		}

	}


	protected synchronized void serialize0ParameterDefinition(ParameterDefinition pd, XmlSerializer serializer, HashMap nsCache) throws IOException {
		if (pd.getName()!=null)
		{
			serializeStartTag(pd,serializer, nsCache);
			serializeAttributes(pd,serializer);
			if (pd.hasChildren()) {
				serializeChildren(pd,serializer, nsCache);
			} else {
				serializeParameterDefinitionContent(pd,serializer, nsCache);
			}
			serializeEndTag(pd,serializer);
		}else{
			if (pd.hasChildren()) {
				serializeChildren(pd,serializer, nsCache);
			} else {
				serializeParameterDefinitionContent(pd,serializer, nsCache);
			}
		}
	}

	protected void serializeParameterDefinitionContent(ParameterDefinition pd, XmlSerializer serializer, HashMap nsCache)  throws IOException
	{
		if (pd instanceof StringValue)
		{
			serializeContentStringValue((StringValue) pd,serializer,nsCache);
		}else if (pd instanceof QNameValue){
			serializeContentQNameValue((QNameValue) pd,serializer,nsCache);
		}else if (pd instanceof AttachmentValue){
			serializeContentAttachmentValue((AttachmentValue) pd,serializer,nsCache);
		}
	}

	protected void serializeContentStringValue(StringValue sv,XmlSerializer serializer, HashMap nsCache)throws IOException 
	{
		if (serializer != null && sv!=null && sv.get() != null) {
			serializer.text(sv.get());
		}
	}

	protected void serializeContentQNameValue(QNameValue qv, XmlSerializer serializer, HashMap nsCache)throws IOException 
	{
		if (serializer != null && qv!=null&& qv.get() != null) {
			String prefixStr="";
			if (qv.get().getNamespace().trim().length()>0){
				String prefix=serializer.getPrefix(qv.get().getNamespace().trim(), false);
				if (prefix!=null)
					prefixStr=prefix+":";

				//				if (Log.isDebug())Log.debug(prefix+" "+qv.get());
			}
			serializer.text(prefixStr+qv.get().getLocalPart());
		}
	}

	protected void serializeContentAttachmentValue(AttachmentValue av,XmlSerializer serializer, HashMap nsCache)throws IOException 
	{
		if (av!=null&& av.getAttachment()!= null) {
			/*
			 * Serialize the XOP include element with attachment cid
			 */
			String cid = av.getAttachment().getContentId();

			serializer.startTag(XOPConstants.XOP_NAMESPACE_NAME, XOPConstants.XOP_ELEM_INCLUDE);
			serializer.attribute(null, XOPConstants.XOP_ATTRIB_HREF, XOPConstants.XOP_CID_PREFIX + cid);
			serializer.endTag(XOPConstants.XOP_NAMESPACE_NAME, XOPConstants.XOP_ELEM_INCLUDE);
		}
	}

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap;

import java.io.IOException;
import java.io.OutputStream;

import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.monitor.MonitoringContext;
import org.ws4d.java.communication.protocol.http.HTTPRequest;
import org.ws4d.java.communication.protocol.http.HTTPResponseHandler;
import org.ws4d.java.communication.protocol.http.header.HTTPRequestHeader;
import org.ws4d.java.communication.protocol.soap.generator.Message2SOAPGenerator;
import org.ws4d.java.communication.protocol.soap.generator.MessageReceiver;
import org.ws4d.java.communication.protocol.soap.generator.SOAPMessageGeneratorFactory;
import org.ws4d.java.constants.HTTPConstants;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.types.InternetMediaType;

public class SOAPoverHTTPRequest implements HTTPRequest {

	private HTTPRequestHeader	header		= null;

	private Message				message		= null;

	private MessageReceiver		receiver	= null;

	SOAPoverHTTPRequest(String request, Message message, MessageReceiver receiver) {
		header = new HTTPRequestHeader(HTTPConstants.HTTP_METHOD_POST, request, HTTPConstants.HTTP_VERSION11);
		header.addHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_TYPE, InternetMediaType.getSOAPXML().getMediaType());
		header.addHeaderFieldValue(HTTPConstants.HTTP_HEADER_TRANSFER_ENCODING, HTTPConstants.HTTP_HEADERVALUE_TRANSFERCODING_CHUNKED);
		this.message = message;
		this.receiver = receiver;
	}

	public MessageReceiver getMessageReceiver() {
		return receiver;
	}

	public Message getRequestMessage() {
		return message;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.http.HTTPRequest#getRequestHeader()
	 */
	@Override
	public HTTPRequestHeader getRequestHeader() {
		return header;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.http.HTTPRequest#serializeRequestBody
	 * (java.io.OutputStream, org.ws4d.java.communication.ProtocolData,
	 * org.ws4d.java.io.monitor.MonitoringContext)
	 */
	@Override
	public void serializeRequestBody(OutputStream out, ProtocolData protocolData, MonitoringContext context) throws IOException {
		Message2SOAPGenerator generator= SOAPMessageGeneratorFactory.getInstance().getMessage2SOAPGeneratorForCurrentThread();
		generator.generateSOAPMessage(out, message, protocolData);
		SOAPMessageGeneratorFactory.getInstance().releaseMessage2SOAPGenerator(generator);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.http.HTTPRequest#requestSendFailed
	 * (java.lang.Exception, org.ws4d.java.communication.ProtocolData)
	 */
	@Override
	public void requestSendFailed(Exception e, ProtocolData pd) {
		receiver.sendFailed(e, (DPWSProtocolData) pd);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.http.HTTPRequest#responseReceiveFailed
	 * (java.lang.Exception, org.ws4d.java.communication.ProtocolData)
	 */
	@Override
	public void responseReceiveFailed(Exception e, ProtocolData pd) {
		receiver.receiveFailed(e, (DPWSProtocolData) pd);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.http.HTTPRequest#getResponseHandler
	 * (org.ws4d.java.communication.InternetMediaType)
	 */
	@Override
	public HTTPResponseHandler getResponseHandler(InternetMediaType mediaType) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getResponseAcceptedContentTypes() {
		String[] retVal=null;
		if (message instanceof InvokeMessage){
			retVal=new String[2];
			retVal[0]="application/soap+xml";
			retVal[1]="application/fastinfoset";
		}
		return retVal;
	}

}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.IMessageEndpoint;
import org.ws4d.java.message.SOAPHeader;
import org.ws4d.java.message.discovery.ByeMessage;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMessage;
import org.ws4d.java.message.eventing.GetStatusMessage;
import org.ws4d.java.message.eventing.GetStatusResponseMessage;
import org.ws4d.java.message.eventing.RenewMessage;
import org.ws4d.java.message.eventing.RenewResponseMessage;
import org.ws4d.java.message.eventing.SubscribeMessage;
import org.ws4d.java.message.eventing.SubscribeResponseMessage;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.message.eventing.UnsubscribeMessage;
import org.ws4d.java.message.eventing.UnsubscribeResponseMessage;
import org.ws4d.java.message.metadata.GetMessage;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.message.metadata.GetResponseMessage;

public interface SOAP2MessageConverter {

	public abstract FaultMessage parseFaultMessage(String actionName, SOAPHeader header, ElementParser parser, DPWSProtocolData protocolData, ConstantsHelper helper, IMessageEndpoint msgEndpoint) throws IOException;
	
	/*Discovery*/
	
	public abstract HelloMessage parseHelloMessage(SOAPHeader header, ElementParser parser, DPWSProtocolData protocolData, ConstantsHelper helper) throws IOException;

	public abstract ByeMessage parseByeMessage(SOAPHeader header, ElementParser parser, DPWSProtocolData protocolData, ConstantsHelper helper) throws IOException;

	public abstract ProbeMessage parseProbeMessage(SOAPHeader header, ElementParser parser, DPWSProtocolData protocolData, ConstantsHelper helper) throws IOException;

	public abstract ProbeMatchesMessage parseProbeMatchesMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException;
	

	public abstract ResolveMessage parseResolveMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException;

	public abstract ResolveMatchesMessage parseResolveMatchesMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException;
	
	/*MetaData */

	public abstract GetMessage parseGetMessage(String actionName, SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException;

	public abstract GetMetadataMessage parseGetMetadataMessage(String actionName,SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException;

	public abstract GetMetadataResponseMessage parseGetMetadataResponseMessage(String actionName,
			SOAPHeader header, ElementParser parser,
			DPWSProtocolData protocolData, ConstantsHelper helper) throws IOException;

	public abstract GetResponseMessage parseGetResponseMessage(String actionName,SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException;
	
	
	/* Eventing */

	public abstract GetStatusMessage parseGetStatusMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException;

	public abstract SubscribeMessage parseSubscribeMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException;

	public abstract SubscribeResponseMessage parseSubscribeResponseMessage(
			SOAPHeader header, ElementParser parser,
			DPWSProtocolData protocolData, ConstantsHelper helper) throws IOException;

	public abstract RenewResponseMessage parseRenewResponseMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException;

	public abstract RenewMessage parseRenewMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException;

	public abstract GetStatusResponseMessage parseGetStatusResponseMessage(
			SOAPHeader header, ElementParser parser,
			DPWSProtocolData protocolData, ConstantsHelper helper) throws IOException;

	public abstract UnsubscribeMessage parseUnsubscribeMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException;

	public abstract UnsubscribeResponseMessage parseUnsubscribeResponseMessage(
			SOAPHeader header, ElementParser parser,
			DPWSProtocolData protocolData, ConstantsHelper helper) throws IOException;

	public abstract SubscriptionEndMessage parseSubscriptionEndMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException;

	

}

/**
 * 
 */
package org.ws4d.java.communication.protocol.mime;

import java.io.IOException;
import java.io.OutputStream;

import org.ws4d.java.attachment.AbstractAttachment;
import org.ws4d.java.attachment.AttachmentException;
import org.ws4d.java.util.Log;

/**
 * @author Stefan Schlichting
 *
 */
public class MIMEMemoryAttachment extends AbstractMIMEAttachment {

	/**
	 * @param anAttachment
	 */
	public MIMEMemoryAttachment(AbstractAttachment anAttachment) {
		super(anAttachment);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.mime.MIMEEntityOutput#serialize(
	 * java.io.OutputStream)
	 */
	@Override
	public void serialize(OutputStream out) throws IOException {
		byte[] bytes;
		try {
			bytes = getBytes();
		} catch (AttachmentException e) {
			Log.error(e);
			throw new IOException(e.getMessage());
		}
		
		if ( bytes!= null) {
			out.write(bytes);
			dispose();
		}
	}

}

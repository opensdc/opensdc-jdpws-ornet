/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.DPWSProtocolVersionInfo;
import org.ws4d.java.communication.DPWSUtil;
import org.ws4d.java.communication.DPWS2006.DefaultDPWSUtil2006;
import org.ws4d.java.constants.MEXConstants;
import org.ws4d.java.constants.SOAPConstants;
import org.ws4d.java.constants.WSAConstants;
import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.constants.WSDLConstants;
import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.IMessageEndpoint;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.SOAPHeader;
import org.ws4d.java.message.discovery.ByeMessage;
import org.ws4d.java.message.discovery.DiscoveryMessage;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.message.discovery.ProbeMatch;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.message.discovery.ResolveMatch;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMessage;
import org.ws4d.java.message.eventing.GetStatusMessage;
import org.ws4d.java.message.eventing.GetStatusResponseMessage;
import org.ws4d.java.message.eventing.RenewMessage;
import org.ws4d.java.message.eventing.RenewResponseMessage;
import org.ws4d.java.message.eventing.SubscribeMessage;
import org.ws4d.java.message.eventing.SubscribeResponseMessage;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.message.eventing.UnsubscribeMessage;
import org.ws4d.java.message.eventing.UnsubscribeResponseMessage;
import org.ws4d.java.message.metadata.GetMessage;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.message.metadata.GetResponseMessage;
import org.ws4d.java.schema.Element;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.List;
import org.ws4d.java.types.DiscoveryData;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.WSDL;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class DefaultSOAP2MessageConverter implements SOAP2MessageConverter {

	protected SOAP2BasicTypesConverter basicTypesConverter;
	protected SOAP2ParameterValueConverter pvConverter;

	public DefaultSOAP2MessageConverter(
			SOAP2BasicTypesConverter basicTypesConverter, SOAP2ParameterValueConverter pvConverter) 
	{
		this.basicTypesConverter=basicTypesConverter;
		this.pvConverter=pvConverter;
	}


	@Override
	public HelloMessage parseHelloMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException {
		//		basicTypesConverter.parseDiscoveryData(parser, WSDConstants.WSD_ELEMENT_HELLO, helper);
		DiscoveryData discoveryData= basicTypesConverter.parseDiscoveryData(null,parser, helper);
		HelloMessage hello = new HelloMessage(header, discoveryData);

		addQoSInformationFromParsedMessage (hello,protocolData, helper);

		return hello;	}




	@Override
	public ByeMessage parseByeMessage(SOAPHeader header, ElementParser parser,
			DPWSProtocolData protocolData, ConstantsHelper helper)
					throws IOException {
		//		discoveryData.parse(parser, WSDConstants.WSD_ELEMENT_BYE, helper);
		DiscoveryData discoveryData=basicTypesConverter.parseDiscoveryData(null,parser, helper);
		ByeMessage bye = new ByeMessage(header, discoveryData);

		addQoSInformationFromParsedMessage(bye,protocolData, helper);

		return bye;
	}


	@Override
	public ProbeMessage parseProbeMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException {
		try{
			ProbeMessage probeMessage = new ProbeMessage(header);

			basicTypesConverter.parseUnknownAttributes(probeMessage,parser);

			while (parser.nextTag() != XmlPullParser.END_TAG) {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (helper.getWSDNamespace().equals(namespace)) {
					if (WSDConstants.WSD_ELEMENT_TYPES.equals(name)) {
						probeMessage.setTypes(basicTypesConverter.parseQNameSet(parser));
						DefaultDPWSUtil2006.changeIncomingProbe(probeMessage);
					} else if (WSDConstants.WSD_ELEMENT_SCOPES.equals(name)) {
						probeMessage.setScopes(basicTypesConverter.parseProbeScopeSet(parser));
					} else {
						basicTypesConverter.parseUnknownElement(probeMessage,parser, namespace, name);
					}
				} else {
					basicTypesConverter.parseUnknownElement(probeMessage,parser, namespace, name);
				}
			}
			return probeMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public ProbeMatchesMessage parseProbeMatchesMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException {
		try{
			ProbeMatchesMessage probeMatchesMessage = new ProbeMatchesMessage(header);

			basicTypesConverter.parseUnknownAttributes(probeMatchesMessage,parser);

			while (parser.nextTag() != XmlPullParser.END_TAG) {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (helper.getWSDNamespace().equals(namespace)) {
					if (WSDConstants.WSD_ELEMENT_PROBEMATCH.equals(name)) {
						ProbeMatch probeMatch = new ProbeMatch();
						//							basicTypesConverter.parseDiscoveryData(parser, WSDConstants.WSD_ELEMENT_PROBEMATCH, helper);
						basicTypesConverter.parseDiscoveryData(probeMatch, parser, helper);
						probeMatchesMessage.addProbeMatch(probeMatch);
					} else {
						basicTypesConverter.parseUnknownElement(probeMatchesMessage,parser, namespace, name);
					}
				} else {
					basicTypesConverter.parseUnknownElement(probeMatchesMessage,parser, namespace, name);
				}
			}

			//Re-Modularization 2011-01-21 Implement SecMod
			addQoSInformationFromParsedMessage(probeMatchesMessage, protocolData, helper);
			return probeMatchesMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public ResolveMessage parseResolveMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException {
		try{
			ResolveMessage resolveMessage = new ResolveMessage(header);

			basicTypesConverter.parseUnknownAttributes(resolveMessage,parser);

			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new XmlPullParserException("Resolve is empty");
			}
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (helper.getWSANamespace().equals(namespace)) {
					if (WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE.equals(name)) {
						resolveMessage.setEndpointReference(basicTypesConverter.parseEndpointReference(helper.getDPWSVersion(), parser, helper));
					} else {
						basicTypesConverter.parseUnknownElement(resolveMessage,parser, namespace, name);
					}
				} else {
					basicTypesConverter.parseUnknownElement(resolveMessage,parser, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);
			return resolveMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public ResolveMatchesMessage parseResolveMatchesMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException {
		try{
			ResolveMatchesMessage resolveMatchesMessage = new ResolveMatchesMessage(header);

			basicTypesConverter.parseUnknownAttributes(resolveMatchesMessage,parser);

			while (parser.nextTag() != XmlPullParser.END_TAG) {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (helper.getWSDNamespace().equals(namespace)) {
					if (WSDConstants.WSD_ELEMENT_RESOLVEMATCH.equals(name)) {
						ResolveMatch resolveMatch=new ResolveMatch();
						basicTypesConverter.parseDiscoveryData(resolveMatch,parser, helper);
						resolveMatchesMessage.setResolveMatch(resolveMatch);
					} else {
						basicTypesConverter.parseUnknownElement(resolveMatchesMessage,parser, namespace, name);
					}
				} else {
					basicTypesConverter.parseUnknownElement(resolveMatchesMessage,parser, namespace, name);
				}
			}

			//Re-Modularization 2011-01-21 Implement SecMod
			addQoSInformationFromParsedMessage(resolveMatchesMessage, protocolData, helper);
			return resolveMatchesMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public GetMessage parseGetMessage(String actionName, SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) {
		return new GetMessage(header);
	}


	@Override
	public GetMetadataMessage parseGetMetadataMessage(String actionName,
			SOAPHeader header, ElementParser parser,
			DPWSProtocolData protocolData, ConstantsHelper helper) throws IOException {
		//		return new GetMetadataMessage(header);
		GetMetadataMessage getMetadataMessage = new GetMetadataMessage(header);

		parser.handleUnknownAttributes(getMetadataMessage);

		try{
			while (parser.nextTag() != XmlPullParser.END_TAG) {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (MEXConstants.WSX_NAMESPACE_NAME.equals(namespace)) {
					if (MEXConstants.WSX_ELEM_DIALECT.equals(name)) {
						getMetadataMessage.setDialect(new URI(parser.nextText().trim()));
					} else if (MEXConstants.WSX_ELEM_IDENTIFIER.equals(name)) {
						getMetadataMessage.setIdentifier(new URI(parser.nextText().trim()));
					} else {
						parser.addUnknownElement(getMetadataMessage, namespace, name);
					}
				} else {
					parser.addUnknownElement(getMetadataMessage, namespace, name);
				}
			}
		}catch(Exception e)
		{
			throw new IOException(e);
		}
		return getMetadataMessage;
	}


	@Override
	public GetMetadataResponseMessage parseGetMetadataResponseMessage(
			String actionName, SOAPHeader header, ElementParser parser,
			DPWSProtocolData protocolData, ConstantsHelper helper) throws IOException {

		try{
			GetMetadataResponseMessage getMetadataResponseMessage = new GetMetadataResponseMessage(header);

			basicTypesConverter.parseUnknownAttributes(getMetadataResponseMessage,parser);

			// go to first wsx:MetadataSection element
			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new XmlPullParserException("GetMetadataResponse is empty");
			}
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (MEXConstants.WSX_NAMESPACE_NAME.equals(namespace)) {
					if (MEXConstants.WSX_ELEM_METADATASECTION.equals(name)) {
						// get attribute Dialect and decide upon it
						String dialect = parser.getAttributeValue(null, MEXConstants.WSX_ELEM_DIALECT);
						if (MEXConstants.WSX_DIALECT_WSDL.equals(dialect) || MEXConstants.WSX_DIALECT_WSDL_DEPRECATED.equals(dialect)) {
							parser.nextTag(); // go to child element
							namespace = parser.getNamespace();
							name = parser.getName();
							if (MEXConstants.WSX_NAMESPACE_NAME.equals(namespace)) {
								if (MEXConstants.WSX_ELEM_METADATAREFERENCE.equals(name)) {
									getMetadataResponseMessage.addMetadataReference(basicTypesConverter.parseEndpointReference(helper.getDPWSVersion(), parser, helper));
								} else if (MEXConstants.WSX_ELEM_LOCATION.equals(name)) {
									getMetadataResponseMessage.addMetadataLocation(new URI(parser.nextText().trim()));
								}
							} else if (WSDLConstants.WSDL_NAMESPACE_NAME.equals(namespace)) {
								if (WSDLConstants.WSDL_ELEM_DEFINITIONS.equals(name)) {
									getMetadataResponseMessage.addWSDL(WSDL.parse(new ElementParser(parser)));
								}
							}
							// go to closing child
							parser.nextTag();
						} else if (helper.getMetatdataDialectRelationship().equals(dialect)) {
							getMetadataResponseMessage.addRelationship(basicTypesConverter.parseRelationshipMData(parser, helper));
						} else {
							// unknown metadata dialect
							/*
							 * what about XML Schema and/or WS-Policy dialects? and
							 * what about embedded metadata elements, like
							 * wsdl:definitions or xs:schema? these all could be
							 * handled here, if we want it someday...
							 */
							basicTypesConverter.parseUnknownElement(getMetadataResponseMessage,parser, namespace, name);
						}
					} else {
						basicTypesConverter.parseUnknownElement(getMetadataResponseMessage,parser, namespace, name);
					}
				} else {
					basicTypesConverter.parseUnknownElement(getMetadataResponseMessage,parser, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);
			return getMetadataResponseMessage;
		}catch(Exception e){
			Log.info(e);
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public GetResponseMessage parseGetResponseMessage(String actionName,
			SOAPHeader header, ElementParser parser,
			DPWSProtocolData protocolData, ConstantsHelper helper) throws IOException {
		try{
			GetResponseMessage getResponseMessage = new GetResponseMessage(header);

			basicTypesConverter.parseUnknownAttributes(getResponseMessage, parser);

			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new XmlPullParserException("GetResponse is empty");
			}
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (MEXConstants.WSX_NAMESPACE_NAME.equals(namespace)) {
					if (MEXConstants.WSX_ELEM_METADATASECTION.equals(name)) {
						// get attribute Dialect and decide upon it
						String dialect = parser.getAttributeValue(null, MEXConstants.WSX_ELEM_DIALECT);
						if (helper.getMetadataDialectThisModel().equals(dialect)) {
							getResponseMessage.setThisModel(basicTypesConverter.parseThisModelMData(parser, helper));
						} else if (helper.getMetadataDialectThisDevice().equals(dialect)) {
							getResponseMessage.setThisDevice(basicTypesConverter.parseThisDeviceMData(parser, helper));
						} else if (helper.getMetatdataDialectRelationship().equals(dialect)) {
							getResponseMessage.addRelationship(basicTypesConverter.parseRelationshipMData(parser, helper));
						} else {
							// unknown metadata dialect
							basicTypesConverter.parseUnknownElement(getResponseMessage,parser, namespace, name);
						}
					} else {
						basicTypesConverter.parseUnknownElement(getResponseMessage,parser, namespace, name);
					}
				} else {
					basicTypesConverter.parseUnknownElement(getResponseMessage,parser, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);
			return getResponseMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public GetStatusMessage parseGetStatusMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException {
		try{
			GetStatusMessage getStatusMessage = new GetStatusMessage(header);
			parser.nextGenericElement(getStatusMessage);
			return getStatusMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public SubscribeMessage parseSubscribeMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException {
		try{
			SubscribeMessage subscribeMessage = new SubscribeMessage(header);

			basicTypesConverter.parseUnknownAttributes(subscribeMessage,parser);

			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new XmlPullParserException("Subscribe is empty");
			}
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
					if (WSEConstants.WSE_ELEM_ENDTO.equals(name)) {
						subscribeMessage.setEndTo(basicTypesConverter.parseEndpointReference(((DPWSProtocolVersionInfo)header.getVersion()).getDpwsVersion(), parser, helper));
					} else if (WSEConstants.WSE_ELEM_DELIVERY.equals(name)) {
						subscribeMessage.setDelivery(basicTypesConverter.parseDelivery(parser,DPWSUtil.getHelper(header.getVersion())));
					} else if (WSEConstants.WSE_ELEM_EXPIRES.equals(name)) {
						subscribeMessage.setExpires(parser.nextText());
					} else if (WSEConstants.WSE_ELEM_FILTER.equals(name)) {
						subscribeMessage.setFilter(basicTypesConverter.parseFilter(parser, helper));
					} else {
						basicTypesConverter.parseUnknownElement(subscribeMessage,parser, namespace, name);
					}
				} else {
					basicTypesConverter.parseUnknownElement(subscribeMessage,parser, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);
			return subscribeMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public SubscribeResponseMessage parseSubscribeResponseMessage(
			SOAPHeader header, ElementParser parser,
			DPWSProtocolData protocolData, ConstantsHelper helper) throws IOException {
		try{
			SubscribeResponseMessage subscribeResponseMessage = new SubscribeResponseMessage(header);

			basicTypesConverter.parseUnknownAttributes(subscribeResponseMessage,parser);

			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new XmlPullParserException("SubscribeResponse is empty");
			}
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
					if (WSEConstants.WSE_ELEM_SUBSCRIPTIONMANAGER.equals(name)) {
						subscribeResponseMessage.setSubscriptionManager(
								basicTypesConverter.parseEndpointReference(((DPWSProtocolVersionInfo)header.getVersion()).getDpwsVersion(), parser, helper));
					} else if (WSEConstants.WSE_ELEM_EXPIRES.equals(name)) {
						subscribeResponseMessage.setExpires(parser.nextText());
					} else {
						basicTypesConverter.parseUnknownElement(subscribeResponseMessage,parser, namespace, name);
					}
				} else {
					basicTypesConverter.parseUnknownElement(subscribeResponseMessage,parser, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);
			return subscribeResponseMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public RenewResponseMessage parseRenewResponseMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException {
		try{
			RenewResponseMessage renewResponseMessage = new RenewResponseMessage(header);

			while (parser.nextTag() != XmlPullParser.END_TAG) {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
					if (WSEConstants.WSE_ELEM_EXPIRES.equals(name)) {
						renewResponseMessage.setExpires(parser.nextText());
					} else {
						basicTypesConverter.parseUnknownElement(renewResponseMessage,parser, namespace, name);
					}
				} else {
					basicTypesConverter.parseUnknownElement(renewResponseMessage,parser, namespace, name);
				}
			}
			return renewResponseMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public RenewMessage parseRenewMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException {
		try{
			RenewMessage renewMessage = new RenewMessage(header);
			basicTypesConverter.parseUnknownAttributes(renewMessage,parser);

			while (parser.nextTag() != XmlPullParser.END_TAG) {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
					if (WSEConstants.WSE_ELEM_EXPIRES.equals(name)) {
						renewMessage.setExpires(parser.nextText());
					} else {
						basicTypesConverter.parseUnknownElement(renewMessage,parser, namespace, name);
					}
				} else {
					basicTypesConverter.parseUnknownElement(renewMessage,parser, namespace, name);
				}
			}
			return renewMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public GetStatusResponseMessage parseGetStatusResponseMessage(
			SOAPHeader header, ElementParser parser,
			DPWSProtocolData protocolData, ConstantsHelper helper) throws IOException {
		try{
			GetStatusResponseMessage getStatusResponseMessage = new GetStatusResponseMessage(header);

			while (parser.nextTag() != XmlPullParser.END_TAG) {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
					if (WSEConstants.WSE_ELEM_EXPIRES.equals(name)) {
						getStatusResponseMessage.setExpires(parser.nextText());
					} else {
						basicTypesConverter.parseUnknownElement(getStatusResponseMessage,parser, namespace, name);
					}
				} else {
					basicTypesConverter.parseUnknownElement(getStatusResponseMessage,parser, namespace, name);
				}
			}
			return getStatusResponseMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public UnsubscribeMessage parseUnsubscribeMessage(SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper) throws IOException {
		try{
			UnsubscribeMessage unsubscribeMessage = new UnsubscribeMessage(header);
			parser.nextGenericElement(unsubscribeMessage);
			return unsubscribeMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public UnsubscribeResponseMessage parseUnsubscribeResponseMessage(
			SOAPHeader header, ElementParser parser,
			DPWSProtocolData protocolData, ConstantsHelper helper) {
		return new UnsubscribeResponseMessage(header);
	}


	@Override
	public SubscriptionEndMessage parseSubscriptionEndMessage(
			SOAPHeader header, ElementParser parser,
			DPWSProtocolData protocolData, ConstantsHelper helper) throws IOException {
		try{
			SubscriptionEndMessage subscriptionEndMessage = new SubscriptionEndMessage(header);

			basicTypesConverter.parseUnknownAttributes(subscriptionEndMessage,parser);

			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new XmlPullParserException("SubscriptionEnd is empty");
			}
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (WSEConstants.WSE_NAMESPACE_NAME.equals(namespace)) {
					if (WSEConstants.WSE_ELEM_SUBSCRIPTIONMANAGER.equals(name)) {
						subscriptionEndMessage.setSubscriptionManager(basicTypesConverter.parseEndpointReference(((DPWSProtocolVersionInfo)header.getVersion()).getDpwsVersion(), parser,helper));
					} else if (WSEConstants.WSE_ELEM_STATUS.equals(name)) {
						subscriptionEndMessage.setStatus(new URI(parser.nextText().trim()));
					} else if (WSEConstants.WSE_ELEM_REASON.equals(name)) {
						subscriptionEndMessage.setReason(parser.nextLocalizedString());
					} else {
						basicTypesConverter.parseUnknownElement(subscriptionEndMessage,parser, namespace, name);
					}
				} else {
					basicTypesConverter.parseUnknownElement(subscriptionEndMessage,parser, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);
			return subscriptionEndMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage());
		}
	}


	@Override
	public FaultMessage parseFaultMessage(String actionName, SOAPHeader header,
			ElementParser parser, DPWSProtocolData protocolData,
			ConstantsHelper helper, IMessageEndpoint msgEndpoint)
					throws IOException {
		try{
			//TODO SSch Code/SubCode/Type von Fault ersetzen ???
			FaultMessage faultMessage = new FaultMessage(header);

			Element faultElement = msgEndpoint.getFaultElement(actionName);

			parser.handleUnknownAttributes(faultMessage);

			int event = parser.nextTag();
			if (event == XmlPullParser.END_TAG) {
				throw new XmlPullParserException("Fault is empty");
			}
			do {
				String namespace = parser.getNamespace();
				String name = parser.getName();
				if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace)) {
					if (SOAPConstants.SOAP_ELEM_CODE.equals(name)) {
						parseCode(faultMessage, parser);
					} else if (SOAPConstants.SOAP_ELEM_REASON.equals(name)) {
						faultMessage.setReason(nextReason(parser));
					} else if (SOAPConstants.SOAP_ELEM_DETAIL.equals(name)) {
						// go to content of soap:Detail
						if (parser.getEventType() == XmlPullParser.START_TAG) {
							if (faultElement != null) {
								parser.nextTag();
								faultMessage.setDetail(pvConverter.parseParameterValue(parser, faultElement));
							} else {
								parser.addUnknownElement(faultMessage, namespace, name);
							}

						}
						// parser.nextTag();
					} else {
						parser.addUnknownElement(faultMessage, namespace, name);
					}
				} else {
					parser.addUnknownElement(faultMessage, namespace, name);
				}
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);
			return faultMessage;
		}catch(Exception e){
			throw new IOException(e.getMessage()); 
		}
	}

	protected DataStructure nextReason(ElementParser parser) throws XmlPullParserException, IOException {
		List reason = new ArrayList();
		int event = parser.nextTag();
		if (event == XmlPullParser.END_TAG) {
			throw new XmlPullParserException("Reason is empty");
		}
		do {
			String namespace = parser.getNamespace();
			String name = parser.getName();
			if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace)) {
				if (SOAPConstants.SOAP_ELEM_TEXT.equals(name)) {
					reason.add(parser.nextLocalizedString());
				}
			}
			event = parser.nextTag();
		} while (event != XmlPullParser.END_TAG);
		return reason;
	}

	//TODO SSch implement a recursive approach that returns a list of codes
	protected void parseCode(FaultMessage faultMessage, ElementParser parser) throws XmlPullParserException, IOException {
		int event = parser.nextTag();
		if (event == XmlPullParser.END_TAG) {
			throw new XmlPullParserException("Code is empty");
		}
		do {
			String namespace = parser.getNamespace();
			String name = parser.getName();
			if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace)) 
			{
				if (SOAPConstants.SOAP_ELEM_VALUE.equals(name)) 
				{
					faultMessage.setCode(parser.nextQName());
				}else if (SOAPConstants.SOAP_ELEM_SUBCODE.equals(name)) 
				{
					int event2 = parser.nextTag();
					if (event2 == XmlPullParser.END_TAG) 
					{
						//Removed SSch 2011-11-24 Subcode is optional only, see http://www.w3.org/TR/2007/REC-soap12-part1-20070427/#faultsubcodeelement
						//throw new XmlPullParserException("Subcode is empty");
					}else{

						do {
							String namespace2 = parser.getNamespace();
							String name2 = parser.getName();
							if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace2)) 
							{
								if (SOAPConstants.SOAP_ELEM_VALUE.equals(name2)) 
								{
									faultMessage.setSubcode(parser.nextQName());
								}else if (SOAPConstants.SOAP_ELEM_SUBCODE.equals(name)) 
								{
									int event3 = parser.nextTag();
									if (event3 == XmlPullParser.END_TAG) 
									{
										//Removed SSch 2011-11-24 Subcode is optional only, see http://www.w3.org/TR/2007/REC-soap12-part1-20070427/#faultsubcodeelement
										//throw new XmlPullParserException("Subcode is empty");
									}else{
										do {
											String namespace3 = parser.getNamespace();
											String name3 = parser.getName();
											if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace3)) {
												if (SOAPConstants.SOAP_ELEM_VALUE.equals(name3)) {
													faultMessage.setSubsubcode(parser.nextQName());
												} else if (SOAPConstants.SOAP_ELEM_SUBCODE.equals(name3)) {
													// void, enough recursion
												}
											}
											event3 = parser.nextTag();
										} while (event3 != XmlPullParser.END_TAG);
									}
								}
							}
							event2 = parser.nextTag();
						} while (event2 != XmlPullParser.END_TAG);
					}
				}
			}
			event = parser.nextTag();
		} while (event != XmlPullParser.END_TAG);

	}

	protected void addQoSInformationFromParsedMessage(Message msg, DPWSProtocolData protocolData, ConstantsHelper helper) {

		//TODO SSch security QoS ???
		if (msg!=null && (msg.getHeader() instanceof SOAPHeader))
		{
			SecurityManagerModule secMod=(SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);
			if (secMod==null)
				return;

			SOAPHeader header=(SOAPHeader) msg.getHeader();
			byte[] signature=header.getSignature();
			if (signature== null) return; 

			boolean hasValidSignature=false;

			if (msg instanceof HelloMessage || msg instanceof ByeMessage)
			{
				DiscoveryMessage discoveryMsg=(DiscoveryMessage) msg;
				if (discoveryMsg.getDiscoveryData()!=null) 
				{
					DiscoveryData discoveryData=discoveryMsg.getDiscoveryData();
					hasValidSignature=determineSignatureStatus(discoveryData,signature,secMod,protocolData);
				}
			}else if (msg instanceof ProbeMatchesMessage)
			{
				ProbeMatchesMessage probeMatchesMessage=(ProbeMatchesMessage)msg;

				for (int i = 0; i < probeMatchesMessage.getProbeMatchCount(); i++) {
					DiscoveryData discoveryData= probeMatchesMessage.getProbeMatch(i);
					if (discoveryData!=null){
						hasValidSignature=hasValidSignature || determineSignatureStatus(discoveryData,signature,secMod,protocolData); 
					}
				}

				probeMatchesMessage.setSecure(true); //TODO SSch Check if this is the only msg where the flag is set
			}else if (msg instanceof ResolveMatchesMessage){
				DiscoveryData discoveryData=((ResolveMatchesMessage)msg).getResolveMatch();
				if (discoveryData != null) 
				{
					hasValidSignature=determineSignatureStatus(discoveryData, signature, secMod, protocolData);
				}
			}
			secMod.releaseMessage(protocolData);

			header.setValidated(hasValidSignature);
		}
	}

	protected boolean determineSignatureStatus(DiscoveryData discoveryData, byte[] signature, SecurityManagerModule secMod, DPWSProtocolData protocolData)
	{

		String[] potentialAlia = new String[discoveryData.getXAddrs().size()];
		Iterator iter = discoveryData.getXAddrs().iterator();
		for (int i = 0; iter.hasNext(); i++) {
			potentialAlia[i] = iter.next().toString();
		}
		boolean validationStatus=secMod.validateMessage(signature, protocolData, discoveryData.getEndpointReference(), potentialAlia);
		return validationStatus;
	}

}

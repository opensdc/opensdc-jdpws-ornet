/**
 * 
 */
package org.ws4d.java.communication.protocol.mime;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.ws4d.java.attachment.AbstractAttachment;
import org.ws4d.java.attachment.AttachmentException;
import org.ws4d.java.attachment.DefaultAttachmentStore;
import org.ws4d.java.util.Log;

/**
 * @author Stefan Schlichting
 *
 */
public class MIMEFileAttachment extends AbstractMIMEAttachment {

	/**
	 * @param anAttachment
	 */
	public MIMEFileAttachment(AbstractAttachment anAttachment) {
		super(anAttachment);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.mime.MIMEEntityOutput#serialize(
	 * java.io.OutputStream)
	 */
	@Override
	public void serialize(OutputStream out) throws IOException {
		//SSch different from original materna impl.
		InputStream in;
		try {
			in = getInputStream();
		} catch (AttachmentException e) {
			Log.error(e);
			throw new IOException(e.getMessage());
		}
		DefaultAttachmentStore.readOut(in, out);


	}

}

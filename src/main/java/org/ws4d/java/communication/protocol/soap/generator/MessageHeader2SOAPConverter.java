/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.io.xml.XmlSerializer;
import org.ws4d.java.message.Message;

public interface MessageHeader2SOAPConverter 
{
	/**
	 * Serialize the header to the SOAP Document
	 * @param pd 
	 */
	public void serializeMessageHeader(Message msg, XmlSerializer serializer, ConstantsHelper helper, boolean secure, ProtocolData pd) throws IOException;

}

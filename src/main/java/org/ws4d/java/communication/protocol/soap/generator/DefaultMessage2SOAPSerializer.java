/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.communication.HTTPBinding;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.MEXConstants;
import org.ws4d.java.constants.SOAPConstants;
import org.ws4d.java.constants.WSAConstants;
import org.ws4d.java.constants.WSDConstants;
import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.constants.XMLConstants;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.io.xml.XmlSerializer;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.discovery.ByeMessage;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.message.discovery.ProbeMatch;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.message.discovery.ResolveMatch;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMessage;
import org.ws4d.java.message.eventing.GetStatusMessage;
import org.ws4d.java.message.eventing.GetStatusResponseMessage;
import org.ws4d.java.message.eventing.RenewMessage;
import org.ws4d.java.message.eventing.RenewResponseMessage;
import org.ws4d.java.message.eventing.SubscribeMessage;
import org.ws4d.java.message.eventing.SubscribeResponseMessage;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.message.eventing.UnsubscribeMessage;
import org.ws4d.java.message.eventing.UnsubscribeResponseMessage;
import org.ws4d.java.message.metadata.GetMessage;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.message.metadata.GetResponseMessage;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.RelationshipMData;
import org.ws4d.java.types.ThisDeviceMData;
import org.ws4d.java.types.ThisModelMData;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;


public class DefaultMessage2SOAPSerializer implements Message2SOAPConverter {

	protected static final boolean JMEDS_DPWS_EXPLORER_FALLBACK_ENABLED=Boolean.parseBoolean(System.getProperty("MDPWS.JMEDS_DPWS_EXPLORER_FALLBACK_ENABLED", "true"));

	protected BasicTypes2SOAPConverter basicTypesConverter;
	protected ParameterValue2SOAPConverter pvSerializer;

	public DefaultMessage2SOAPSerializer(
			BasicTypes2SOAPConverter basicTypesConverter,
			ParameterValue2SOAPConverter pvSerializer) {
		this.basicTypesConverter=basicTypesConverter;
		this.pvSerializer=pvSerializer;
	}




	@Override
	public BasicTypes2SOAPConverter getBasicTypesSerializer() {
		return basicTypesConverter;
	}

	@Override
	public ParameterValue2SOAPConverter getParameterValueSerializer() {
		return pvSerializer;
	}

	@Override
	public void serialize(Message msg, XmlSerializer serializer,
			ConstantsHelper helper, ProtocolData pd) throws IOException {
		if (msg==null) return;
		//TODO SSch ProtocolData an die serializeXXX methoden übergeben
		if (msg instanceof FaultMessage){
			serializeFaultMessage((FaultMessage)msg, serializer, helper);
		}else if (msg instanceof InvokeMessage){
			serializeInvokeMessage((InvokeMessage)msg, serializer, helper);
		}else if (msg instanceof ByeMessage){
			serializeByeMessage((ByeMessage)msg, serializer, helper);
		}else if (msg instanceof GetMessage){
			serializeGetMessage((GetMessage)msg, serializer, helper);
		}else if (msg instanceof GetMetadataMessage){
			serializeGetMetadataMessage((GetMetadataMessage)msg, serializer, helper);
		}else if (msg instanceof GetMetadataResponseMessage){
			serializeGetMetadataResponseMessage((GetMetadataResponseMessage)msg, serializer, helper);
		}else if (msg instanceof GetResponseMessage){
			serializeGetResponseMessage((GetResponseMessage)msg, serializer, helper);
		}else if (msg instanceof GetStatusMessage){
			serializeGetStatusMessage((GetStatusMessage)msg, serializer, helper);
		}else if (msg instanceof GetStatusResponseMessage){
			serializeGetStatusResponseMessage((GetStatusResponseMessage)msg, serializer, helper);
		}else if (msg instanceof HelloMessage){
			serializeHelloMessage((HelloMessage)msg, serializer, helper);
		}else if (msg instanceof ProbeMatchesMessage){
			serializeProbeMatchesMessage((ProbeMatchesMessage)msg, serializer, helper);
		}else if (msg instanceof ProbeMessage){
			serializeProbeMessage((ProbeMessage)msg, serializer, helper);
		}else if (msg instanceof RenewMessage){
			serializeRenewMessage((RenewMessage)msg, serializer, helper);
		}else if (msg instanceof RenewResponseMessage){
			serializeRenewResponseMessage((RenewResponseMessage)msg, serializer, helper);
		}else if (msg instanceof ResolveMatchesMessage){
			serializeResolveMatchesMessage((ResolveMatchesMessage)msg, serializer, helper);
		}else if (msg instanceof ResolveMessage){
			serializeResolveMessage((ResolveMessage)msg, serializer, helper);
		}else if (msg instanceof SubscribeMessage){
			serializeSubscribeMessage((SubscribeMessage)msg, serializer, helper, pd);
		}else if (msg instanceof SubscribeResponseMessage){
			serializeSubscribeResponseMessage((SubscribeResponseMessage)msg, serializer, helper);
		}else if (msg instanceof SubscriptionEndMessage){
			serializeSubscriptionEndMessage((SubscriptionEndMessage)msg, serializer, helper);
		}else if (msg instanceof UnsubscribeMessage){
			serializeUnsubscribeMessage((UnsubscribeMessage)msg, serializer, helper);
		}else if (msg instanceof UnsubscribeResponseMessage){
			serializeUnsubscribeResponseMessage((UnsubscribeResponseMessage)msg, serializer, helper);
		}
	}


	protected void serializeFaultMessage(FaultMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException 
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Fault-StartTag
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_FAULT);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// Code
		QName code = convertCode(msg,helper);
		if (code != null) {
			serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_CODE);
			// Valueelement
			serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_VALUE);
			String prefix = serializer.getPrefix(code.getNamespace(), true);
			serializer.text(prefix + ":" + code.getLocalPart());
			serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_VALUE);
			// Subcode
			QName subcode = convertSubcode(msg,helper);
			if (subcode != null) {
				serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_SUBCODE);
				// Valueelement
				serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_VALUE);
				String prefix1 = serializer.getPrefix(subcode.getNamespace(), true);
				serializer.text(prefix1 + ":" + subcode.getLocalPart());
				serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_VALUE);
				// Subsubcode
				QName subsubcode = msg.getSubsubcode();
				if (subsubcode != null) {
					serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_SUBCODE);
					// Valueelement
					serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_VALUE);
					String prefix2 = serializer.getPrefix(subsubcode.getNamespace(), true);
					serializer.text(prefix2 + ":" + subsubcode.getLocalPart());
					serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_VALUE);
					serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_SUBCODE);
				}
				serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_SUBCODE);
			}
			serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_CODE);
		}
		// Reason
		DataStructure reason = msg.getReason();
		if (reason != null) {
			ArrayList list = (ArrayList) reason;
			serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_REASON);
			for (Iterator it = list.iterator(); it.hasNext();) {
				LocalizedString string = (LocalizedString) it.next();
				SerializeUtil.serializeTagWithAttribute(serializer, SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_TEXT, string.getValue(), XMLConstants.XML_NAMESPACE_NAME, XMLConstants.XML_ATTRIBUTE_LANGUAGE, string.getLanguage());
			}
			serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_REASON);
		}
		IParameterValue detail = msg.getDetail();
		if (detail != null) {
			serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_DETAIL);
			//			detail.serialize(serializer);
			this.pvSerializer.serializeParameterValue(detail, serializer, helper);
			serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_DETAIL);
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_FAULT);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	private QName convertSubcode(FaultMessage msg, ConstantsHelper helper) {
		QName code=msg.getSubcode();
		if (code!=null)
		{
			if (FaultMessage.SUBCODE_ACTION_NOT_SUPPORTED.equals(code))
			{
				code=helper.getWSAFaultActionNotSupported();
			}else if (FaultMessage.SUBCODE_ENDPOINT_NOT_AVAILABLE.equals(code)){
				code=helper.getWSAfaultEndpointUnavailable();
			}else if (FaultMessage.SUBCODE_INVALID_ADDRESSING_HEADER.equals(code)){
				code=WSAConstants.WSA_FAULT_INVALID_ADDRESSING_HEADER;
			}else if (FaultMessage.SUBCODE_EVENTING_FILTER_ACTION_NOT_SUPPORTED.equals(code)){
				code=DPWSConstants.DPWS_FAULT_FILTER_ACTION_NOT_SUPPORTED;
			}
		}
		return code;
	}

	protected QName convertCode(FaultMessage msg, ConstantsHelper helper) {
		QName code=msg.getCode();
		if (code!=null)
		{
			if (FaultMessage.CODE_RECEIVER_FAULT.equals(code))
			{
				code=SOAPConstants.SOAP_FAULT_RECEIVER;
			}else if (FaultMessage.CODE_SENDER_FAULT.equals(code)){
				code=SOAPConstants.SOAP_FAULT_SENDER;
			}
		}
		return code;
	}

	protected void serializeInvokeMessage(InvokeMessage msg,
			XmlSerializer serializer, ConstantsHelper helper) throws IOException 
			{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		IParameterValue parameters = msg.getContent();
		this.pvSerializer.serializeParameterValue(parameters,serializer, helper);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
			}

	protected void serializeRenewMessage(RenewMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start-Tag
		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_RENEW);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// Expires
		if (msg.getExpires() != null && !(msg.getExpires().equals(""))) {
			SerializeUtil.serializeTag(serializer, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_EXPIRES, msg.getExpires());
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_RENEW);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeRenewResponseMessage(RenewResponseMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start-Tag
		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_RENEWRESPONSE);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// Expires
		if (msg.getExpires() != null && !(msg.getExpires().equals(""))) {
			SerializeUtil.serializeTag(serializer, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_EXPIRES, msg.getExpires());
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_RENEWRESPONSE);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeSubscribeMessage(SubscribeMessage msg, XmlSerializer serializer, ConstantsHelper helper, ProtocolData pd) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start-Tag
		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_SUBSCRIBE);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// EndTo

		if (msg.getEndTo() != null) {
			basicTypesConverter.serializeEndpointReference(msg.getEndTo(),serializer, helper, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_ENDTO);
		}
		// Delivery
		if (msg.getDelivery()!= null) {
			if (msg.getBindingCandidatesForNotification()!= null) {
				Iterator bindingCandidatesForNotification=msg.getBindingCandidatesForNotification();
				URI notifyToAddress = null;
				while (bindingCandidatesForNotification.hasNext()) {
					HTTPBinding binding = (HTTPBinding) bindingCandidatesForNotification.next();
					URI address = binding.getTransportAddress();
					if (pd.sourceMatches(address)) {
						notifyToAddress = address;
						break;
					}
					if (!bindingCandidatesForNotification.hasNext()) {
						notifyToAddress = address;
						if (Log.isInfo()) {
							Log.info("No appropriate local address found for event notification, fallbacking to: " + notifyToAddress+" ProtocolData:"+pd);
						}
					}
				}
				msg.getDelivery().setNotifyTo(new EndpointReference(notifyToAddress, msg.getDelivery().getNotifyTo().getReferenceParameters()));
			}
			basicTypesConverter.serializeDelivery(msg.getDelivery(),serializer, helper);
		}
		// Expires
		if (msg.getExpires() != null && !(msg.getExpires().equals(""))) {
			SerializeUtil.serializeTag(serializer, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_EXPIRES, msg.getExpires());
		}
		// Filter
		if (msg.getFilter() != null) {
			basicTypesConverter.serializeFilter(msg.getFilter(),serializer, helper);
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_SUBSCRIBE);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeByeMessage(ByeMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);


		//Re-Modularization 2011-01-21 Implement SecMod
		//if (DPWSFramework.hasModule(DPWSFramework.DPWS_SECURITY_MODULE)) serializer.attribute("", "ID", "BID1");
		addQoSInformationToSOAPBody(msg,serializer,helper);

		// Start-Tag
		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_BYE);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// Discovery Data adden
		basicTypesConverter.serializeDiscoveryData(msg.getDiscoveryData(),serializer, helper);
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// END-Tag
		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_BYE);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeGetMessage(GetMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);

		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeGetMetadataMessage(GetMetadataMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start-Tag
		serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_GETMETADATA);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// Dialect adden
		if (msg.getDialect() != null) {
			SerializeUtil.serializeTag(serializer, MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_DIALECT, (msg.getDialect().toString() == null ? "" : msg.getDialect().toString()));
		}
		// Identifier adden
		if (msg.getIdentifier() != null) {
			SerializeUtil.serializeTag(serializer, MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_IDENTIFIER, (msg.getIdentifier().toString() == null ? "" : msg.getIdentifier().toString()));
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_GETMETADATA);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeGetMetadataResponseMessage(GetMetadataResponseMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		
		if (msg==null) return;
		
		//System.out.println("Response: "+msg.getRequestedDialect());
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start Tag
		serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATA);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		if (msg.getMetadataReferences() != null) {
			for (Iterator it = msg.getMetadataReferences().iterator(); it.hasNext();) {
				EndpointReference ref = (EndpointReference) it.next();

				if (JMEDS_DPWS_EXPLORER_FALLBACK_ENABLED)
				{
					if (msg.getRequestedDialect()==null ||  MEXConstants.WSX_DIALECT_WSDL_DEPRECATED.equals(msg.getRequestedDialect().toString()))
					{
						// Start MetadataSection for WSDL
						serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
						// Dialect adden
						serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, MEXConstants.WSX_DIALECT_WSDL_DEPRECATED);
						// EndpointReference(s) adden
						basicTypesConverter.serializeEndpointReference(ref,serializer, helper, MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATAREFERENCE);
						// End Tag
						serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
					}
				}
				if (msg.getRequestedDialect()==null ||  MEXConstants.WSX_DIALECT_WSDL.equals(msg.getRequestedDialect().toString()))
				{
					// Start MetadataSection for WSDL
					serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
					// Dialect adden
					serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, MEXConstants.WSX_DIALECT_WSDL);
					// EndpointReference(s) adden
					basicTypesConverter.serializeEndpointReference(ref,serializer, helper, MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATAREFERENCE);
					// End Tag
					serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
				}


			}
		}
		if (msg.getMetadataLocations() != null) {
			for (Iterator it = msg.getMetadataLocations().iterator(); it.hasNext();) {
				URI location = (URI) it.next();

				if (JMEDS_DPWS_EXPLORER_FALLBACK_ENABLED)
				{
					if (msg.getRequestedDialect()==null ||  MEXConstants.WSX_DIALECT_WSDL_DEPRECATED.equals(msg.getRequestedDialect().toString()))
					{
						// Start MetadataSection for WSDL
						serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
						// Dialect adden
						serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, MEXConstants.WSX_DIALECT_WSDL_DEPRECATED);
						// URI(s) adden
						SerializeUtil.serializeTag(serializer, MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_LOCATION, (location.toString() == null ? "" : location.toString()));
						// End Tag
						serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
					}
				}
				if (msg.getRequestedDialect()==null ||  MEXConstants.WSX_DIALECT_WSDL.equals(msg.getRequestedDialect().toString()))
				{
					// Start MetadataSection for WSDL
					serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
					// Dialect adden
					serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, MEXConstants.WSX_DIALECT_WSDL);
					// URI(s) adden
					SerializeUtil.serializeTag(serializer, MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_LOCATION, (location.toString() == null ? "" : location.toString()));
					// End Tag
					serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
				}
			}
		}
		if (msg.getRequestedDialect()==null ||  helper.getMetatdataDialectRelationship().equals(msg.getRequestedDialect().toString()))
		{
			// Start MetadataSection for Relationship
			serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
			// RelationshipMData adden
			serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, helper.getMetatdataDialectRelationship());
			if (msg.getRelationship() != null) {
				basicTypesConverter.serializeRelationshipMData(msg.getRelationship(),serializer, helper);
			}
			// End Tags
			serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATA);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeGetResponseMessage(GetResponseMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start-Tag
		serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATA);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// ThisModelMData adden
		ThisModelMData thisModel = msg.getThisModel();
		if (thisModel != null) {
			serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
			serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, helper.getMetadataDialectThisModel());
			basicTypesConverter.serializeThisModelMData(thisModel, serializer, helper);
			serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
		}
		// ThisDeviceMData adden
		ThisDeviceMData thisDevice = msg.getThisDevice();
		if (thisDevice != null) {
			serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
			serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, helper.getMetadataDialectThisDevice());
			basicTypesConverter.serializeThisDeviceMData(thisDevice, serializer, helper);
			serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
		}
		// RelationshipMData adden
		RelationshipMData relationship = msg.getRelationship();
		if (relationship != null) {
			serializer.startTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
			serializer.attribute(null, MEXConstants.WSX_ELEM_DIALECT, helper.getMetatdataDialectRelationship());
			basicTypesConverter.serializeRelationshipMData(relationship,serializer, helper);
			serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATASECTION);
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(MEXConstants.WSX_NAMESPACE_NAME, MEXConstants.WSX_ELEM_METADATA);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeGetStatusMessage(GetStatusMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start-Tag
		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_GETSTATUS);
		// Do Nothing because its in the specification defined
		// End-Tag
		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_GETSTATUS);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeGetStatusResponseMessage(GetStatusResponseMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start-Tag
		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_GETSTATUSRESPONSE);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// Expires
		if (msg.getExpires() != null && !(msg.getExpires().equals(""))) {
			SerializeUtil.serializeTag(serializer, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_EXPIRES, msg.getExpires());
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_GETSTATUSRESPONSE);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeHelloMessage(HelloMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);


		//Re-Modularization 2011-01-21 Implement SecMod
		//if (DPWSFramework.hasModule(DPWSFramework.DPWS_SECURITY_MODULE)) serializer.attribute("", "ID", "BID1");
		addQoSInformationToSOAPBody(msg,serializer,helper);


		// Start-Tag
		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_HELLO);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// Discovery Data adden
		basicTypesConverter.serializeDiscoveryData(msg.getDiscoveryData(),serializer, helper);
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_HELLO);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}


	protected void serializeProbeMatchesMessage(ProbeMatchesMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);


		//Re-Modularization 2011-01-21 Implement SecMod
		//		if (DPWSFramework.hasModule(DPWSFramework.DPWS_SECURITY_MODULE)) serializer.attribute("", "ID", "BID1");
		addQoSInformationToSOAPBody(msg,serializer,helper);		

		// Start-Tag
		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_PROBEMATCHES);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// Adds ProbeMatch Elements
		for (Iterator it = msg.getProbeMatches().iterator(); it.hasNext();) {
			ProbeMatch probeMatch = (ProbeMatch) it.next();
			basicTypesConverter.serializeProbeMatch(probeMatch,serializer, helper);
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_PROBEMATCHES);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeProbeMessage(ProbeMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start-Tag
		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_PROBE);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// QNameSet types
		if (msg.getTypes() != null) {
			basicTypesConverter.serializeQNameSet(msg.getTypes(),serializer, helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_TYPES);
		}
		// ScopeSet scopes
		if (msg.getScopes() != null) {
			basicTypesConverter.serializeScopeSet(msg.getScopes(),serializer, helper.getWSDNamespace());
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_PROBE);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeResolveMatchesMessage(ResolveMatchesMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);


		//Re-Modularization 2011-01-21 Implement SecMod
		//		if(DPWSFramework.hasModule(DPWSFramework.DPWS_SECURITY_MODULE))serializer.attribute("", "ID", "BID1");
		addQoSInformationToSOAPBody(msg,serializer,helper);



		// Start-Tag
		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_RESOLVEMATCHES);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// Adds ResolveMatch Elements
		ResolveMatch match = msg.getResolveMatch();
		if (match != null) {
			basicTypesConverter.serializeResolveMatch(match,serializer, helper);
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_RESOLVEMATCHES);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void addQoSInformationToSOAPBody(Message msg,
			XmlSerializer serializer, ConstantsHelper helper) throws IllegalArgumentException, IllegalStateException, IOException 
			{
		//Canonicalize and serialize this element
		//TODO SSch security QoS
		SecurityManagerModule secMod=(SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);
		if (secMod!=null)
			serializer.attribute("", "ID", "BID1");	
			}

	protected void serializeResolveMessage(ResolveMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start-Tag
		serializer.startTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_RESOLVE);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// Add the EPR
		if (msg.getEndpointReference() != null) {
			basicTypesConverter.serializeEndpointReference(msg.getEndpointReference(),serializer, helper, helper.getWSANamespace(), WSAConstants.WSA_ELEM_ENDPOINT_REFERENCE);
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(helper.getWSDNamespace(), WSDConstants.WSD_ELEMENT_RESOLVE);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeSubscribeResponseMessage(SubscribeResponseMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start-Tag
		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_SUBSCRIBERESPONSE);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// Subscripton Manager
		if (msg.getSubscriptionManager() != null) {
			basicTypesConverter.serializeEndpointReference(msg.getSubscriptionManager(),serializer, helper, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_SUBSCRIPTIONMANAGER);
		}
		// Expires
		if (msg.getExpires() != null && !(msg.getExpires().equals(""))) {
			SerializeUtil.serializeTag(serializer, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_EXPIRES, msg.getExpires());
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_SUBSCRIBERESPONSE);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeSubscriptionEndMessage(SubscriptionEndMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start-Tag
		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_SUBSCRIPTIONEND);
		// Adds UnknownAttributes
		basicTypesConverter.serializeUnknownAttributes(msg,serializer);
		// Subscripton Manager
		if (msg.getSubscriptionManager() != null) {
			basicTypesConverter.serializeEndpointReference(msg.getSubscriptionManager(),serializer, helper, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_SUBSCRIPTIONMANAGER);
		}
		// Status
		if (msg.getStatus() != null) {
			SerializeUtil.serializeTag(serializer, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_STATUS, msg.getStatus().toString());
		}
		// Reason
		if (msg.getReason() != null) {
			SerializeUtil.serializeTagWithAttribute(serializer, WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_REASON, msg.getReason().getValue(), XMLConstants.XML_NAMESPACE_NAME, XMLConstants.XML_ATTRIBUTE_LANGUAGE, msg.getReason().getLanguage());
		}
		// Adds UnknownElements
		basicTypesConverter.serializeUnknownElements(msg,serializer);
		// End-Tag
		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_SUBSCRIPTIONEND);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeUnsubscribeMessage(UnsubscribeMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
		// Start-Tag
		serializer.startTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_UNSUBSCRIBE);
		// Do Nothing because its in the specification defined
		// End-Tag
		serializer.endTag(WSEConstants.WSE_NAMESPACE_NAME, WSEConstants.WSE_ELEM_UNSUBSCRIBE);
		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

	protected void serializeUnsubscribeResponseMessage(UnsubscribeResponseMessage msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException
	{
		if (msg==null) return;
		// ################## Body-StartTag ##################
		serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);

		// ################## BODY-EndTag ##################
		serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_BODY);
	}

}

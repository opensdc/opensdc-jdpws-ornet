/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap;

import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.DPWSProtocolVersionInfo;
import org.ws4d.java.communication.protocol.soap.generator.MessageDiscarder;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.message.SOAPHeader;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.MessageIdBuffer;
import org.ws4d.java.util.Log;

/**
 * Discards messages sent by the framework.
 */
public class OwnMessageDiscarder implements MessageDiscarder {

	private final MessageIdBuffer	sentMessageIds;

	private final MessageDiscarder	discarder;

	/**
	 * Creates a discarder which discards messages sent by the framework.
	 * 
	 * @param sentMessageIds a buffer which contains message identifiers of sent
	 *            messages.
	 * @param next a message discarder for the discarder chain. This discarder
	 *            is called after checking own message identifiers.
	 */
	public OwnMessageDiscarder(MessageIdBuffer sentMessageIds, MessageDiscarder next) {
		super();
		this.sentMessageIds = sentMessageIds;
		this.discarder = next;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageDiscarder#
	 * discardMessage(org.ws4d.java.message.SOAPHeader,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public int discardMessage(SOAPHeader header, DPWSProtocolData protocolData) {
		/*
		 * Extract version data and verify framework DPWS versions.
		 */
		DPWSProtocolVersionInfo dpwsVersion = (DPWSProtocolVersionInfo) header.getVersion();
		if (dpwsVersion==null) return VERSION_NOT_SUPPORTED;
		HashSet versions = DPWSProperties.getInstance().getSupportedDPWSVersions();
		if (versions != null) {
			Iterator itv = versions.iterator();
			boolean supVer = false;
			while (itv.hasNext()) {
				DPWSProtocolVersionInfo pvi = (DPWSProtocolVersionInfo) itv.next();
				if (dpwsVersion.getDpwsVersion() == pvi.getDpwsVersion()) {
					supVer = true;
					break;
				}
			}
			if (!supVer) {
				Log.debug("Unsupported DPWS Version : " + dpwsVersion + ", just 0 for DPWS 2009 or 1 for DPWS 2006 are defined.");
				return MessageDiscarder.VERSION_NOT_SUPPORTED;
			}
		}
		/*
		 * Check own messages sent.
		 */
		if (sentMessageIds != null && sentMessageIds.contains(header.getMessageId())) {
			return MessageDiscarder.OWN_MESSAGE;
		}
		if (discarder != null) {
			return discarder.discardMessage(header, protocolData);
		}
		return MessageDiscarder.NOT_DISCARDED;
	}

}

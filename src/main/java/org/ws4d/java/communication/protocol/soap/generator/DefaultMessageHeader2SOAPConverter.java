/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.SOAPConstants;
import org.ws4d.java.constants.WSAConstants;
import org.ws4d.java.io.xml.XmlSerializer;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.SOAPHeader;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.ReferenceParametersMData;

public class DefaultMessageHeader2SOAPConverter implements
		MessageHeader2SOAPConverter {

	private BasicTypes2SOAPConverter typesSerializer;

	public DefaultMessageHeader2SOAPConverter(BasicTypes2SOAPConverter typesSerializer) {
		super();
		this.typesSerializer=typesSerializer;
	}
	

	public BasicTypes2SOAPConverter getTypesSerializer() {
		return typesSerializer;
	}

	
	@Override
	public void serializeMessageHeader(Message msg,
			XmlSerializer serializer, ConstantsHelper helper, boolean secure, ProtocolData pd) throws IOException 
	{

			if (!(msg.getHeader() instanceof SOAPHeader))return;
			
			SOAPHeader header=(SOAPHeader) msg.getHeader();
		
			// prerequisite namespaces for copied reference parameters => optional
			ReferenceParametersMData params = header.getReferenceParameters();
			if (params != null) {
				getTypesSerializer().serializeNamespacePrefixes(params, serializer);
			}

			// ################## Header-StartTag ##################
			serializer.startTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_HEADER);
			// Adds UnknownAttributes to Header Tag if exists
			getTypesSerializer().serializeUnknownAttributes(header, serializer);
			// Action-Tag => mandatory
			serializeActionTag(msg,header, serializer, helper, secure);

			// MessageID-Tag => optional
			if (header.getMessageId() != null) {
				getTypesSerializer().serializeAttributedURI(header.getMessageId(),serializer, helper.getWSANamespace(), WSAConstants.WSA_ELEM_MESSAGE_ID);
			}
			// relatesTo-Tag => optional
			if (header.getRelatesTo() != null) {
				getTypesSerializer().serializeAttributedURI(header.getRelatesTo(),serializer, helper.getWSANamespace(), WSAConstants.WSA_ELEM_RELATESTO);
			}
			// replyTo-Tag => optional
			if (header.getReplyTo() != null && !header.getReplyTo().equals(helper.getWSAAnonymus())) {
				getTypesSerializer().serializeEndpointReference(header.getReplyTo(),serializer, helper, helper.getWSANamespace(), WSAConstants.WSA_ELEM_REPLY_TO);
			}
			// To-Tag => optional
			if (header.getTo() != null) {
				getTypesSerializer().serializeAttributedURI(header.getTo(), serializer, helper.getWSANamespace(), WSAConstants.WSA_ELEM_TO);
			}
			// from-tag ==> optional
			if (header.getFrom() != null) {
				getTypesSerializer().serializeAttributedURI(header.getFrom(), serializer, helper.getWSANamespace(), WSAConstants.WSA_ELEM_SOURCE_ENDPOINT);
			}
			// copied reference parameters => optional
			if (params!= null) {
				getTypesSerializer().serializeReferenceParameters(params,serializer, helper, true);
			}
			// AppSequence-Tag => optional
			if ( header.getAppSequence()!= null) {
				getTypesSerializer().serializeAppSequence(header.getAppSequence(), serializer, helper);
			}
			// Adds UnknownElements to Header if exists
			getTypesSerializer().serializeUnknownElements(header, serializer);
			// ################## Header-EndTag ##################
			serializer.endTag(SOAPConstants.SOAP12_NAMESPACE_NAME, SOAPConstants.SOAP_ELEM_HEADER);
	}


	private void serializeActionTag(Message msg, SOAPHeader header,
			XmlSerializer serializer, ConstantsHelper helper, boolean secure) throws IOException {
		if (msg instanceof FaultMessage)
		{
			if (header.getAction().equals(FaultMessage.TYPE_ADRESSING_FAULT))
			{
				header.setAction(new AttributedURI(helper.getWSAActionAddressingFault()));
			}else if (header.getAction().equals(FaultMessage.TYPE_PROTOCOL_ACTION_FAULT))
			{
				header.setAction(new AttributedURI(DPWSConstants.DPWS_ACTION_DPWS_FAULT));
			}
		}
		getTypesSerializer().serializeAttributedURI( header.getAction(),serializer, helper.getWSANamespace(), WSAConstants.WSA_ELEM_ACTION);
	}

}

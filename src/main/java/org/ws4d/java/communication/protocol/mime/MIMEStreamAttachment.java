/**
 * 
 */
package org.ws4d.java.communication.protocol.mime;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.ws4d.java.attachment.AbstractAttachment;
import org.ws4d.java.attachment.AttachmentException;
import org.ws4d.java.attachment.DefaultAttachmentStore;
import org.ws4d.java.configuration.AttachmentProperties;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.ObjectPool;
import org.ws4d.java.util.ObjectPool.InstanceCreator;

/**
 * @author Stefan Schlichting
 *
 */
public class MIMEStreamAttachment extends AbstractMIMEAttachment {
	
	private static final ObjectPool	STREAM_BUFFERS	= new ObjectPool(new InstanceCreator() {

		/*
		 * (non-Javadoc )
		 * @see org .ws4d .
		 * java. util.
		 * ObjectPool .
		 * InstanceCreator #
		 * createInstance ()
		 */
		@Override
		public Object createInstance() {
			return new byte[AttachmentProperties.getInstance().getStreamBufferSize()];
		}

	}, 1);

	/**
	 * @param anAttachment
	 */
	public MIMEStreamAttachment(AbstractAttachment aStreamAttachment) {
		super(aStreamAttachment);
	}


	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.mime.MIMEEntity#serialize(java.io
	 * .OutputStream)
	 */
	@Override
	public void serialize(OutputStream out) throws IOException {
		InputStream in;
		try {
			in = getInputStream();
		} catch (AttachmentException e) {
			Log.printStackTrace(e);
			Log.error(e.getMessage());
			throw new IOException(e.getMessage());
		}
		if (in == null) {
			return;
		}
		DefaultAttachmentStore.readOut(in, out, (byte[]) STREAM_BUFFERS.acquire());

		/*
		 * as this method should only be called on the sender side when
		 * transmitting the attachment's data to a remote receiver, we assume no
		 * one is going to use this attachment instance after that anymore;
		 * thus, ensure input stream is closed
		 */
		dispose();
	}

}

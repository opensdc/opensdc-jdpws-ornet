/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.mime;

import java.io.IOException;
import java.io.InputStream;

import org.ws4d.java.attachment.AbstractAttachment;
import org.ws4d.java.attachment.AttachmentException;
import org.ws4d.java.communication.protocol.http.HTTPResponse;
import org.ws4d.java.constants.MIMEConstants;
import org.ws4d.java.types.InternetMediaType;
import org.ws4d.java.util.Log;

public abstract class AbstractMIMEAttachment extends AbstractAttachment implements MIMEEntityOutput{

	private AbstractAttachment attachment=null;
	
	public AbstractMIMEAttachment(AbstractAttachment anAttachment) {
		super((InternetMediaType)null); //TODO SSch this is hack, we just use the delegates
		this.attachment=anAttachment;
	}


	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.mime.MIMEEntity#getEntityHeader()
	 */
	@Override
	public MIMEBodyHeader getEntityHeader() {
		MIMEBodyHeader header = new MIMEBodyHeader();
		header.setHeaderField(MIMEConstants.MIME_HEADER_CONTENT_ID, getContentId());
		try {
			// if contentType is null set to "" to avoid NullPointerExceptions
			String contentTypeString = "";
			InternetMediaType contentType = getContentType();
			if (contentType != null) {
				contentTypeString = contentType.toString();
			}

			header.setHeaderField(MIMEConstants.MIME_HEADER_CONTENT_TYPE, contentTypeString);
			header.setHeaderField(MIMEConstants.MIME_HEADER_CONTENT_TRANSFER_ENCODING, getTransferEncoding());
		} catch (AttachmentException e) {
			/*
			 * shouldn't ever happen, as getContentType() or
			 * getTransferEncoding() shouldn't fail locally
			 */
			Log.warn(e);
		}
		return header;
	}


	@Override
	public InternetMediaType getContentType() throws AttachmentException {
		return attachment.getContentType();
	}

	@Override
	public String getTransferEncoding() throws AttachmentException {
		return attachment.getTransferEncoding();
	}

	@Override
	public int getType() throws AttachmentException {
		return attachment.getType();
	}

	@Override
	public InputStream getInputStream() throws AttachmentException, IOException {
		return attachment.getInputStream();
	}

	@Override
	public String getFilePath() throws AttachmentException {
		return attachment.getFilePath();
	}

	@Override
	public void dispose() {
		attachment.dispose();
	}

	@Override
	public boolean equals(Object arg0) {
		return attachment.equals(arg0);
	}

	@Override
	public String getContentId() {
		return attachment.getContentId();
	}

	@Override
	public byte[] getBytes() throws AttachmentException, IOException {
		return attachment.getBytes();
	}

	@Override
	public int hashCode() {
		return attachment.hashCode();
	}

	@Override
	public boolean isAvailable() {
		return attachment.isAvailable();
	}

	@Override
	public long size() throws AttachmentException {
		return attachment.size();
	}

	@Override
	public boolean isLocal() {
		return attachment.isLocal();
	}

	@Override
	public void save(String targetFilePath) throws AttachmentException,
			IOException {
		attachment.save(targetFilePath);
	}

	@Override
	public boolean move(String newFilePath) throws AttachmentException {
		return attachment.move(newFilePath);
	}

	@Override
	public String toString() {
		return attachment.toString();
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.mime.MIMEEntity#getHTTPResponse()
	 */
	@Override
	public HTTPResponse getHTTPResponse() {
		// void
		return null;
	}
}

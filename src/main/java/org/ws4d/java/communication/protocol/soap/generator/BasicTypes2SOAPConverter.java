/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.io.xml.XmlSerializer;
import org.ws4d.java.message.discovery.ProbeMatch;
import org.ws4d.java.message.discovery.ResolveMatch;
import org.ws4d.java.types.AppSequence;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.Delivery;
import org.ws4d.java.types.DiscoveryData;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.Filter;
import org.ws4d.java.types.HostMData;
import org.ws4d.java.types.HostedMData;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.ReferenceParametersMData;
import org.ws4d.java.types.RelationshipMData;
import org.ws4d.java.types.ScopeSet;
import org.ws4d.java.types.ThisDeviceMData;
import org.ws4d.java.types.ThisModelMData;
import org.ws4d.java.types.URISet;
import org.ws4d.java.types.UnknownDataContainer;

public interface BasicTypes2SOAPConverter {

	/*UnknownData*/
	
	/**
	 * Serialize all UnknownElements to the SOAP Document.
	 * 
	 */
	public abstract void serializeUnknownElements(UnknownDataContainer dataContainer, XmlSerializer serializer) throws IOException;
	

	/**
	 * Serialize all UnknownAttributes from Map to parent Tag.
	 * 
	 */
	public abstract void serializeUnknownAttributes(UnknownDataContainer dataContainer, XmlSerializer serializer) throws IOException;


	/**
	 * Serialize the Attributed URI to the Soap Document.
	 */
	public abstract void serializeAttributedURI(AttributedURI attrURI, XmlSerializer serializer, String namespace, String elementName) throws IOException;

	public abstract void serializeEndpointReference(EndpointReference epr,XmlSerializer serializer, ConstantsHelper helper,String namespace, String elementName)throws IOException;
	
	public abstract void serializeNamespacePrefixes(ReferenceParametersMData rParams, XmlSerializer serializer);
	
	public abstract void serializeReferenceParameters(ReferenceParametersMData referenceParameters, XmlSerializer serializer, ConstantsHelper helper, boolean withinHeader) throws IOException;

	public abstract void serializeAppSequence(AppSequence appSequence, XmlSerializer serializer, ConstantsHelper helper)  throws IOException;
	
	
	/* Eventing */

	public abstract void serializeDelivery(Delivery delivery, XmlSerializer serializer, ConstantsHelper helper) throws IOException;
	
	public abstract void serializeFilter(Filter filter, XmlSerializer serializer, ConstantsHelper helper) throws IOException;
	

	/* DiscoveryData */
	public abstract void serializeProbeMatch(ProbeMatch pm, XmlSerializer serializer, ConstantsHelper helper) throws IOException;
	
	public abstract void serializeResolveMatch(ResolveMatch msg, XmlSerializer serializer, ConstantsHelper helper) throws IOException;
	
	public abstract void serializeDiscoveryData(DiscoveryData dData, XmlSerializer serializer, ConstantsHelper helper) throws IOException;
	

	/* MetaData */	
	public abstract void serializeHostMData(HostMData hmData, XmlSerializer serializer, ConstantsHelper helper) throws IOException;
	
	public abstract void serializeHostedMData(HostedMData hmData, XmlSerializer serializer, ConstantsHelper helper) throws IOException;
	
	public abstract void serializeThisDeviceMData(ThisDeviceMData deviceMData, XmlSerializer serializer, ConstantsHelper helper) throws IOException;
	
	public abstract void serializeThisModelMData(ThisModelMData modelMData, XmlSerializer serializer, ConstantsHelper helper) throws IOException;
	
	public abstract void serializeRelationshipMData(RelationshipMData rmData, XmlSerializer serializer, ConstantsHelper helper) throws IOException;
	

	/* Sets */
	public abstract void serializeURISet(URISet xAddrs, XmlSerializer serializer, String namespace) throws IOException;

	public abstract void serializeScopeSet(ScopeSet scopes, XmlSerializer serializer, String namespace) throws IOException;

	public abstract void serializeQNameSet(QNameSet types, XmlSerializer serializer,String namespace, String element) throws IOException;

}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.types.AppSequence;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.Delivery;
import org.ws4d.java.types.DiscoveryData;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.Filter;
import org.ws4d.java.types.HostMData;
import org.ws4d.java.types.HostedMData;
import org.ws4d.java.types.ProbeScopeSet;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.ReferenceParametersMData;
import org.ws4d.java.types.RelationshipMData;
import org.ws4d.java.types.ScopeSet;
import org.ws4d.java.types.ThisDeviceMData;
import org.ws4d.java.types.ThisModelMData;
import org.ws4d.java.types.URISet;
import org.ws4d.java.types.UnknownDataContainer;
import org.xmlpull.v1.XmlPullParserException;

public interface SOAP2BasicTypesConverter {
	
	/*UnknownData*/
	
	public abstract UnknownDataContainer parseUnknownAttributes(UnknownDataContainer container, ElementParser parser);
	
	public abstract UnknownDataContainer parseUnknownElement(UnknownDataContainer container, ElementParser parser, String namespace, String name) throws XmlPullParserException, IOException ;
	

	public abstract AttributedURI parseAttributedURI(ElementParser parser, ConstantsHelper helper)throws IOException;

	public abstract EndpointReference parseEndpointReference(int dpwsVersion, ElementParser parser, ConstantsHelper helper) throws IOException;
	
	public abstract ReferenceParametersMData parseReferenceParametersMData(ElementParser parentParser, ConstantsHelper helper) throws IOException;

	public abstract AppSequence parseAppSequence(ElementParser parser, ConstantsHelper helper) throws IOException;


	/* Sets */
	
	public abstract QNameSet parseQNameSet(ElementParser parser) throws XmlPullParserException, IOException;
	
	public abstract URISet parseURISet(ElementParser parser) throws XmlPullParserException, IOException;
	
	public abstract ScopeSet parseScopeSet(ElementParser parser) throws IOException;
	
	public abstract ProbeScopeSet parseProbeScopeSet(ElementParser parser) throws IOException;

	/* MetaData */
	
	public abstract DiscoveryData parseDiscoveryData(DiscoveryData discoveryData, ElementParser parser, ConstantsHelper helper) throws IOException;
		
	public abstract HostMData parseHostMData(ElementParser parser, ConstantsHelper helper) throws IOException;
	
	public abstract HostedMData parseHostedMData(ElementParser parser, ConstantsHelper helper) throws IOException;
	
	public abstract ThisDeviceMData parseThisDeviceMData(ElementParser parser, ConstantsHelper helper) throws IOException;
	
	public abstract ThisModelMData parseThisModelMData(ElementParser parser, ConstantsHelper helper) throws IOException;
	
	public abstract RelationshipMData parseRelationshipMData(ElementParser parser, ConstantsHelper helper) throws IOException;
	
	/* Eventing */
	
	public abstract Filter parseFilter(ElementParser parser, ConstantsHelper helper) throws IOException;
	
	public abstract Delivery parseDelivery(ElementParser parser, ConstantsHelper helper) throws XmlPullParserException, IOException;
	
	

}

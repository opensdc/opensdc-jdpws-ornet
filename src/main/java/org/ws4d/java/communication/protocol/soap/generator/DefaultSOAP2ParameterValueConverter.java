/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;

import org.ws4d.java.constants.SchemaConstants;
import org.ws4d.java.constants.XOPConstants;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.schema.Attribute;
import org.ws4d.java.schema.AttributeGroup;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.ExtendedComplexContent;
import org.ws4d.java.schema.RestrictedComplexContent;
import org.ws4d.java.schema.Schema;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.schema.Type;
import org.ws4d.java.service.parameter.AttachmentValue;
import org.ws4d.java.service.parameter.IParameterAttribute;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.parameter.ITypedParameterAttribute;
import org.ws4d.java.service.parameter.ITypedParameterValue;
import org.ws4d.java.service.parameter.ParameterAttribute;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.QNameValue;
import org.ws4d.java.service.parameter.StringValue;
import org.ws4d.java.service.parameter.StringValueFactory;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.HashMap.Entry;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.Set;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class DefaultSOAP2ParameterValueConverter implements
SOAP2ParameterValueConverter {
	

	protected SOAP2BasicTypesConverter btc;

	public DefaultSOAP2ParameterValueConverter(
			SOAP2BasicTypesConverter basicTypesConverter) {
		this.btc = basicTypesConverter;
	}

	@Override
	public IParameterValue parseParameterValue(ElementParser parser,
			Element base) throws IOException {
		try {

			return parse0(parser, base, null);
		} catch (Exception e) {
			Log.info(e);
			throw new IOException(e.getMessage());
		}
	}

	protected ITypedParameterValue parse0(ElementParser parser, Element base,
			IParameterValue parent) throws IOException, XmlPullParserException {
		Type t = base.getType();
		QName parsedName = QNameFactory.getInstance().getQName(
				parser.getName(), parser.getNamespace(), parser.getPrefix());

		if (!parsedName.equals(base.getName())) {
			throw new IOException(
					"Cannot create parameter. Element mismatch. Should be "
							+ base.getName() + ", but " + parsedName
							+ " was found.");
		}

		boolean nil = false;

		HashMap attrs = null;

		int attributeCount = parser.getAttributeCount();
		if (attributeCount > 0) {

			attrs = new HashMap();

			for (int i = 0; i < attributeCount; i++) {
				String attributeNamespace = parser.getAttributeNamespace(i);
				if (attributeNamespace==null || attributeNamespace.isEmpty())
				{
					attributeNamespace = t.getName().getNamespace();
				}

				QName attName = QNameFactory.getInstance().getQName(
						parser.getAttributeName(i),
						attributeNamespace);
				if (SchemaConstants.XSI_NAMESPACE
						.equals(attName.getNamespace())
						&& SchemaConstants.ATTRIBUTE_XSINIL.equals(attName
								.getLocalPart())) {
					/*
					 * XML instance <strong>nil</code> set? This parameter can
					 * have a nil value.
					 */
					nil = true;
				} else {
					IParameterAttribute attribute = new ParameterAttribute(
							attName);
					attribute.setValue(parser.getAttributeValue(i));
					attrs.put(attribute.getName().getLocalPart(), attribute);
				}
			}
		}

		ITypedParameterValue pv = null;

		/*
		 * Eat text or check for children.
		 */
		if (!t.isComplexType()) {
			pv = ParameterValue.load(t);
			parseContent(pv, parser);
		} else {

			pv = handleComplexType(parser, t, parsedName, base);
		}

		if (attrs != null) {
			pv.setAttributes(attrs);

			Set entrySet = attrs.entrySet();
			Iterator attributes = entrySet.iterator();
			while(attributes.hasNext()){
				Entry entry=(Entry) attributes.next();
				if (entry.getValue() instanceof ITypedParameterAttribute){
					ITypedParameterAttribute attribute=(ITypedParameterAttribute)entry.getValue();
					updateAttributeType(t, attribute);
					
					if (attribute.getType()!=null && attribute.getType().getName()!=null && QName.QNAME.equals(attribute.getType().getName()))
					{
						String shortQName = attribute.getValue();
						QName qNameValue = parseStringToQName(shortQName,parser);
						attribute.setValue(qNameValue.toStringPlain());
					}
				}
			}

			pv.setNil(nil);
		}

		pv.setMaxOccurs(base.getMaxOccurs());
		pv.setMinOccurs(base.getMinOccurs());

		pv.setName(parsedName);
		if (pv.getType()==null)
			pv.setType(t);

		return pv;
	}

	private void updateAttributeType(Type parameterValueType, ITypedParameterAttribute attribute) {
		Attribute attributeDef = parameterValueType.getAttribute(attribute.getName());
		if (attributeDef==null)
		{
			Iterator attributeGroups = parameterValueType.attributeGroups();
			while(attributeGroups.hasNext() && attributeDef==null)
			{
				AttributeGroup attrGroup= (AttributeGroup) attributeGroups.next();
				
				if (attrGroup.isReference())
				{
					attrGroup = (AttributeGroup) attrGroup.getReference();
					
				}
				attributeDef=attrGroup.getAttribute(attribute.getName());
				
			}

			if (attributeDef==null)
			{
				if (parameterValueType instanceof ExtendedComplexContent)
				{
					ExtendedComplexContent eccType=(ExtendedComplexContent)parameterValueType;
					updateAttributeType(eccType.getBase(), attribute);
				}else if (parameterValueType instanceof RestrictedComplexContent)
				{
					RestrictedComplexContent rccType=(RestrictedComplexContent)parameterValueType;
					updateAttributeType(rccType.getBase(), attribute);
				}
			}
		}
		
		
		if (attributeDef!=null)
		{
			attribute.setType(attributeDef.getType());
		}
		
	}

	private ITypedParameterValue handleComplexType(ElementParser parser,
			Type t, QName parsedName, Element base)
					throws XmlPullParserException, IOException {

		ITypedParameterValue pv;
		// SSch 2011-01-14 Can also by anytype => anysimpletype
		String xsiType = getXSITypeAttributeValue(parser);
		// maybe use http://www.w3.org/2001/XMLSchema-instance)

		int parentDepth = parser.getDepth();
		int tag = parser.next();

		QName xsiTypeName = getInstanceTypeQName(parser, xsiType);

		pv = new ParameterValue();
		pv.setInstanceType(xsiTypeName);
		ComplexType complex = getComplexType(t, base, xsiTypeName, parser);
		if (complex!=null)
			pv.setType(complex);

		while (tag != XmlPullParser.END_TAG && parser.getDepth() >= parentDepth) {
			if (tag == XmlPullParser.COMMENT
					|| tag == XmlPullParser.PROCESSING_INSTRUCTION
					|| tag == XmlPullParser.IGNORABLE_WHITESPACE) {
				tag = parser.next();
				continue;
			} else if (tag == XmlPullParser.TEXT) {
				if (parser.isWhitespace()) {
					tag = parser.next();
					continue;
				} else {
					pv = StringValueFactory.getInstance().getStringValue();
					parseContent(pv, parser);
				}
			} else {

				if (parser.getDepth()<=parentDepth) 
					break;

				QName nextStartName = QNameFactory.getInstance().getQName(
						parser.getName(), parser.getNamespace(),
						parser.getPrefix());
				/*
				 * TODO: This is a very simple parser implementation. It should
				 * be better if we check for occurrence and container type like
				 * ALL, SEQUENCE and CHOICE. At the moment we just check whether
				 * the element name is possible or not.
				 */
				Element nextElement = ParameterValue.searchElement(complex,
						nextStartName);

				ITypedParameterValue child = null;
				if (nextElement == null) {
					ITypedParameterValue[] pvv = parse1(parser, pv);
					if (pvv.length > 1
							&& pvv[1] != null
							&& pvv[1].getValueType() == ParameterValue.TYPE_ATTACHMENT) {
						pvv[1].setName(parsedName);
						child = pvv[1];
						pvv[1] = null;
					} else {
						child = pvv[0];
					}
					//					child = pvv[0];
				} else {
					child = parse0(parser, nextElement, pv);
				}
				pv.add(child);
			}
			tag = parser.getEventType();
			if (tag != XmlPullParser.START_TAG
					&& tag != XmlPullParser.END_DOCUMENT)
				tag = parser.nextTag();

		}
		return pv;
	}

	private QName getInstanceTypeQName(ElementParser parser, String xsiType) {
		QName xsiTypeName = null;

		if (xsiType != null)
			xsiTypeName = parseStringToQName(xsiType, parser);
		return xsiTypeName;
	}

	private String getXSITypeAttributeValue(ElementParser parser) {
		String xsiType = parser.getAttributeValue(SchemaUtil.XSI_NAMESPACE,
				"type");
		xsiType = xsiType != null ? xsiType.trim() : null;
		xsiType = xsiType != null && xsiType.length() > 0 ? xsiType : null;
		// System.out.println("XSI-Type: "+xsiType);
		return xsiType;
	}

	private ComplexType getComplexType(Type t, Element base, QName xsiTypeName,
			ElementParser parser) {
		ComplexType complex = null;
		if (t instanceof ComplexType) {
			complex = (ComplexType) t;
			if (xsiTypeName != null) {
				Schema schema = base.getBelongingSchema();
				if (schema != null) {
					Type tempType = schema.getType(xsiTypeName);
					if (tempType == null) {
						// try to find an imported/linked schema inside the
						// top-level ones ...
						for (Iterator it = schema.getLinkedSchemas(); it
								.hasNext();) {
							schema = (Schema) it.next();
							Type type = schema.getType(xsiTypeName);
							if (type != null) {
								tempType = type;
								// System.out.println("\t\t Found in Linked Schema"+tempType);
								break;
							}
						}
						// last fallback, if it is a built-in schema type
						if (tempType == null)
							tempType = SchemaUtil.getType(xsiTypeName);
					}

					if (tempType instanceof ComplexType)
						complex = (ComplexType) tempType;
				}
			}
		}
		// System.out.println("\t Loaded Type:"+complex);
		return complex;
	}

	private QName parseStringToQName(String xsiType, ElementParser parser) {
		String nsp = null;
		int index = xsiType.indexOf(":");
		if (index >= 0) {
			if (index > 0) {
				nsp = xsiType.substring(0, index);
			}
			xsiType = xsiType.substring(index + 1);
		}
		return QNameFactory.getInstance().getQName(xsiType,
				parser.getNamespace(nsp));
	}

	protected ITypedParameterValue[] parse1(ElementParser parser,
			IParameterValue parent) throws XmlPullParserException, IOException {
		QName parsedName = QNameFactory.getInstance().getQName(
				parser.getName(), parser.getNamespace(), parser.getPrefix());

		ITypedParameterValue[] pv = new ITypedParameterValue[2];

		pv[0] = new ParameterValue();

		pv[0].setName(parsedName);


		int attributeCount = parser.getAttributeCount();
		if (attributeCount > 0) {

			for (int i = 0; i < attributeCount; i++) {
				QName attName = QNameFactory.getInstance().getQName(
						parser.getAttributeName(i),
						parser.getAttributeNamespace(i),
						parser.getAttributePrefix(i));
				if (SchemaConstants.XSI_NAMESPACE
						.equals(attName.getNamespace())
						&& SchemaConstants.ATTRIBUTE_XSINIL.equals(attName
								.getLocalPart())) {
					/*
					 * XML instance <strong>nil</code> set? This parameter can
					 * have a nil value.
					 */
					pv[0].setNil(true);
				} else {
					IParameterAttribute attribute = new ParameterAttribute(
							attName);
					attribute.setValue(parser.getAttributeValue(i));
					pv[0].add(attribute);
				}
			}
		}

		int tag = parser.getEventType();
		switch (tag) {
		case XmlPullParser.START_DOCUMENT:
			tag = parser.nextTag();
			break;
		case XmlPullParser.START_TAG:
			if (XOPConstants.XOP_NAMESPACE_NAME.equals(parser.getNamespace())
					&& XOPConstants.XOP_ELEM_INCLUDE.equals(parser.getName())) {
				pv[1] = ParameterValue.load(SchemaUtil
						.getSchemaType(SchemaUtil.TYPE_BASE64_BINARY));
				parseContent(pv[1], parser);
				tag = parser.nextTag();
				return pv;
			} else {
				tag = parser.next();
				break;
			}
		default:
			break;
		}
		if (tag == XmlPullParser.TEXT) {
			pv[0] = ParameterValue.load(null);
			parseContent(pv[0], parser);
			tag = parser.nextTag();

			pv[0].setName(parsedName);
			return pv;
		}
		int d = parser.getDepth();
		while (tag != XmlPullParser.END_TAG && parser.getDepth() >= d) {
			ITypedParameterValue[] child = parse1(parser, pv[0]);
			if (child[1] != null
					&& child[1].getValueType() == ParameterValue.TYPE_ATTACHMENT) {
				child[1].setName(parsedName);
				pv[0] = child[1];
				child[1] = null;
			} else {
				pv[0].add(child[0]);
			}

			do {
				tag = parser.next();
			} while (tag >= XmlPullParser.TEXT);

			if (tag == XmlPullParser.END_TAG && parser.getDepth() == d) {
				// own end tag, go to next start tag
				tag = parser.nextTag();
			}
		}

		return pv;
	}

	protected void parseContent(IParameterValue pv, ElementParser parser)
			throws IOException, XmlPullParserException {
		if (pv instanceof AttachmentValue)
			parseAttachmentValueContent((AttachmentValue) pv, parser);
		else if (pv instanceof StringValue)
			parseStringValueContent((StringValue) pv, parser);
		else if (pv instanceof QNameValue)
			parseQNameValueContent((QNameValue) pv, parser);
		else
			throw new IOException("Unknown Type for ParameterValue");

		if (parser.getEventType()==XmlPullParser.TEXT)
			parser.nextTag();

	}

	protected void parseAttachmentValueContent(AttachmentValue av,
			ElementParser parser) throws IOException, XmlPullParserException {
		int tag = parser.getEventType();
		boolean xop = false;
		if (tag == XmlPullParser.START_TAG
				&& XOPConstants.XOP_NAMESPACE_NAME
				.equals(parser.getNamespace())
				&& XOPConstants.XOP_ELEM_INCLUDE.equals(parser.getName())) {
			xop = true;
		}
		// XOP:Include start tag
		if (!xop) {
			tag = parser.nextTag();
		}
		if (tag == XmlPullParser.START_TAG
				&& XOPConstants.XOP_NAMESPACE_NAME
				.equals(parser.getNamespace())
				&& XOPConstants.XOP_ELEM_INCLUDE.equals(parser.getName())) {
			String href = parser.getAttributeValue(null,
					XOPConstants.XOP_ATTRIB_HREF);
			/*
			 * Strip the cid prefix from this href ! :D
			 */
			if (href.startsWith(XOPConstants.XOP_CID_PREFIX)) {
				href = href.substring(XOPConstants.XOP_CID_PREFIX.length(),
						href.length());
			}
			av.setAttachment(av.new AttachmentStub(href));
		} else {
			throw new IOException(
					"Cannot create attachment. Element xop:include not found.");
		}
		// XOP:Include end tag
		if (!xop) {
			tag = parser.nextTag();
		}
	}

	protected void parseStringValueContent(StringValue sv, ElementParser parser)
			throws IOException, XmlPullParserException {
		int tag = parser.getEventType();
		if (tag == XmlPullParser.START_TAG) {
			tag = parser.next(); // move to the content
		}
		if (tag == XmlPullParser.TEXT) {
			sv.set(parser.getText());
		}
	}

	protected void parseQNameValueContent(QNameValue qv, ElementParser parser)
			throws IOException, XmlPullParserException {
		int tag = parser.getEventType();
		if (tag == XmlPullParser.START_TAG) {
			tag = parser.next(); // move to the content
		}
		if (tag == XmlPullParser.TEXT) {
			String local = parser.getText();
			String nsp = null;
			int index = local.indexOf(":");
			if (index >= 0) {
				if (index > 0) {
					nsp = local.substring(0, index);
				}
				local = local.substring(index + 1);
			}
			QName qn = QNameFactory.getInstance().getQName(local,
					parser.getNamespace(nsp));
			qv.set(qn);
		}
	}

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.protocol.soap;

import java.io.IOException;
import java.io.OutputStream;

import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.monitor.MonitoringContext;
import org.ws4d.java.communication.protocol.http.HTTPResponse;
import org.ws4d.java.communication.protocol.http.HTTPResponseUtil;
import org.ws4d.java.communication.protocol.http.header.HTTPRequestHeader;
import org.ws4d.java.communication.protocol.http.header.HTTPResponseHeader;
import org.ws4d.java.communication.protocol.mime.MIMEUtil;
import org.ws4d.java.constants.HTTPConstants;
import org.ws4d.java.constants.MIMEConstants;
import org.ws4d.java.dispatch.MessageInformer;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.structures.List;
import org.ws4d.java.types.InternetMediaType;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.ParameterUtil;

/**
 *
 */
public class SOAPResponse implements HTTPResponse {

	private static final MessageInformer	MESSAGE_INFORMER	= MessageInformer.getInstance();

	private final Message					response;

	private final HTTPResponseHeader		header;

	private String							mimeBoundary		= null;

	private List							mimeEntities		= null;

	/**
	 * @param httpStatus
	 * @param response
	 */
	public SOAPResponse(int httpStatus, Message response) {
		super();
		this.response = response;

		header = HTTPResponseUtil.getResponseHeader(httpStatus);

		header.addHeaderFieldValue(HTTPConstants.HTTP_HEADER_TRANSFER_ENCODING, HTTPConstants.HTTP_HEADERVALUE_TRANSFERCODING_CHUNKED);
		String contentType = InternetMediaType.getSOAPXML().getMediaType();
		if (response instanceof InvokeMessage) {
			InvokeMessage invoke = (InvokeMessage) response;
			contentType = inspectAttachments(contentType, invoke.getContent());
		} else if (response instanceof FaultMessage) {
			FaultMessage fault = (FaultMessage) response;
			contentType = inspectAttachments(contentType, fault.getDetail());
		}
		header.addHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_TYPE, contentType);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.http.HTTPResponse#getResponseHeader
	 * ()
	 */
	@Override
	public HTTPResponseHeader getResponseHeader() {
		return header;
	}

	@Override
	public void serializeResponseBody(URI request, HTTPRequestHeader header, OutputStream out, DPWSProtocolData protocolData, MonitoringContext context) throws IOException {
		if (response == null) {
			// omit one-ways
			return;
		}

		MIMEUtil.serializeMessageWithAttachments(response, mimeBoundary, mimeEntities, out, protocolData);
		MESSAGE_INFORMER.forwardMessage(response, protocolData);

		if (context != null) {
			context.setMessage(response);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.protocol.http.HTTPResponse#waitFor()
	 */
	@Override
	public void waitFor() {
		// TODO Auto-generated method stub
	}

	/**
	 * @param contentType
	 * @param pv
	 * @return
	 */
	private String inspectAttachments(String contentType, IParameterValue pv) {
		// Re-Modularization 2011-01-21  ParameterUtil split to MIMEUtil 
		if (MIMEUtil.hasAttachment(pv)) {
			InternetMediaType mimeType = InternetMediaType.getMultipartRelated();
			mimeBoundary = MIMEConstants.BOUNDARY_PREFIX + System.currentTimeMillis();
			mimeType.setParameter(MIMEConstants.PARAMETER_BOUNDARY, mimeBoundary);
			contentType = mimeType.toString();
			mimeEntities = ParameterUtil.getAttachments(pv);
		}
		//TODO SSch check  objectpayload & attachments
		return contentType;
	}



	@Override
	public String getContentTypeForSerializedMessage(URI request,
			HTTPRequestHeader header, 
			ProtocolData protocolData) {

		String contentType=null;
		if (header!=null)
			contentType=header.getHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_TYPE);
		
		String contentTypeFromSerializer = MIMEUtil.getContentTypeForSerializeMessageWithAttachments(response, mimeBoundary, mimeEntities, protocolData);
		if (contentTypeFromSerializer!=null)
			contentType=contentTypeFromSerializer;
		
		return contentType;
	}

}

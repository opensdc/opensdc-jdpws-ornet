/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.communication.protocol.soap.generator;

import java.io.IOException;
import java.io.InputStream;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.DPWSUtil;
import org.ws4d.java.communication.monitor.MonitorStreamFactory;
import org.ws4d.java.communication.monitor.MonitorStreamFactoryProvider;
import org.ws4d.java.communication.monitor.MonitoringContext;
import org.ws4d.java.concurrency.LockSupport;
import org.ws4d.java.constants.MEXConstants;
import org.ws4d.java.constants.SOAPConstants;
import org.ws4d.java.constants.WSAConstants;
import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.constants.WXFConstants;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.io.xml.XmlPullParserSupport;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.IMessageEndpoint;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.SOAPHeader;
import org.ws4d.java.message.discovery.ByeMessage;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMessage;
import org.ws4d.java.message.eventing.GetStatusMessage;
import org.ws4d.java.message.eventing.GetStatusResponseMessage;
import org.ws4d.java.message.eventing.RenewMessage;
import org.ws4d.java.message.eventing.RenewResponseMessage;
import org.ws4d.java.message.eventing.SubscribeMessage;
import org.ws4d.java.message.eventing.SubscribeResponseMessage;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.message.eventing.UnsubscribeMessage;
import org.ws4d.java.message.eventing.UnsubscribeResponseMessage;
import org.ws4d.java.message.metadata.GetMessage;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.message.metadata.GetResponseMessage;
import org.ws4d.java.schema.Element;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.List;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.WSDLOperation;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class DefaultSOAP2MessageGenerator implements SOAP2MessageGenerator {

	protected static final MessageDiscarder	DEFAULT_DISCARDER	= new MessageDiscarder() {

		@Override
		public int discardMessage(SOAPHeader header, DPWSProtocolData protocolData) {
			return 0;
		}
	};

	/**
	 * This map holds all (statically) known message handlers.
	 */
	protected  		ConstantsHelper		helper;

	protected 		LockSupport 		helperLock=new LockSupport();

	private static MessageDiscarder			defaultDiscarder	= DEFAULT_DISCARDER;

	//	private  XmlPullParser				parser;

	protected final SOAP2BasicTypesConverter 		basicTypesConverter;
	protected final SOAP2MessageHeaderConverter 	messageHeaderConverter;
	protected final SOAP2MessageConverter 			messageConverter;
	protected final SOAP2ParameterValueConverter 	pvConverter;

	/**
	 * Standard constructor
	 */
	public DefaultSOAP2MessageGenerator() {
		super();
		basicTypesConverter=createSOAP2BasicTypesConverter();
		messageHeaderConverter=createSOAP2MessageHeaderConverter(basicTypesConverter);
		pvConverter=createSOAP2MessageConverter(basicTypesConverter);
		messageConverter=createSOAP2MessageConverter(basicTypesConverter,pvConverter);
	}



	/**
	 * @param basicTypesConverter2
	 * @param pvConverter2
	 * @return
	 */
	protected SOAP2MessageConverter createSOAP2MessageConverter(
			SOAP2BasicTypesConverter basicTypesConverter,
			SOAP2ParameterValueConverter pvConverter) {
		return new DefaultSOAP2MessageConverter(basicTypesConverter,pvConverter);
	}

	/**
	 * 
	 */
	protected SOAP2BasicTypesConverter createSOAP2BasicTypesConverter() {
		return new DefaultSOAP2BasicTypesConverter();
	}

	protected SOAP2MessageHeaderConverter createSOAP2MessageHeaderConverter(SOAP2BasicTypesConverter 		basicTypesConverter) {
		return new DefaultSOAP2MessageHeaderConverter(basicTypesConverter);
	}

	protected SOAP2ParameterValueConverter createSOAP2MessageConverter(SOAP2BasicTypesConverter 		basicTypesConverter) {
		return new DefaultSOAP2ParameterValueConverter(basicTypesConverter);
	}

	@Override
	public SOAP2BasicTypesConverter getBasicTypesParser() {
		return basicTypesConverter;
	}



	@Override
	public SOAP2ParameterValueConverter getParameterValueParser() {
		return pvConverter;
	}



	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.SOAP2MessageGenerator
	 * #generateMessage(java.io.InputStream)
	 */
	@Override
	public Message generateMessage(InputStream in) throws Exception {
		return generate(in);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.SOAP2MessageGenerator
	 * #deliverMessage(java.io.InputStream,
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void deliverMessage(InputStream in, MessageReceiver to, DPWSProtocolData protocolData, Message request) {
		deliver(in, to, protocolData, getDefaultMessageDiscarder(), request);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.SOAP2MessageGenerator
	 * #deliverMessage(java.io.InputStream,
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver,
	 * org.ws4d.java.communication.DPWSProtocolData,
	 * org.ws4d.java.communication.protocol.soap.generator.MessageDiscarder)
	 */
	@Override
	public void deliverMessage(InputStream in, MessageReceiver to, DPWSProtocolData protocolData, MessageDiscarder discarder) {
		deliver(in, to, protocolData, discarder,null);
	}

	public static synchronized MessageDiscarder getDefaultMessageDiscarder() {
		return defaultDiscarder;
	}

	public static synchronized void setMessageDiscarder(MessageDiscarder newDiscarder) {
		defaultDiscarder = (newDiscarder == null ? DEFAULT_DISCARDER : newDiscarder);
	}

	protected void deliverBody(SOAPHeader header, ElementParser parser, MessageReceiver to, DPWSProtocolData protocolData, Message request) throws XmlPullParserException, IOException, UnexpectedMessageException, MissingElementException, UnexpectedElementException, VersionMismatchException {
		String namespace;
		String name;

		AttributedURI action;
		if (header == null) {
			if (Log.isDebug()) Log.debug("No SOAP header found, try to use request "+request);
			
			if (request!=null)
			{
				action=request.getAction();
			}else{
				throw new MissingElementException("No SOAP header found");
			}
		}else{
			try{
				helperLock.exclusiveLock();
				helper = DPWSUtil.getHelper(header.getVersion());
				action= header.getAction();
			}finally{
				helperLock.releaseExclusiveLock();
			}
		}
		if (action == null) {
			String wsa_ns=null;
			try{
				helperLock.sharedLock();
				if (helper!=null)
					wsa_ns=helper.getWSANamespace();
				else
					wsa_ns="WSA";
			}finally{
				helperLock.releaseSharedLock();
			}

			throw new MissingElementException(wsa_ns + ":" + WSAConstants.WSA_ELEM_ACTION);
		}
		String actionName= action.toString();
		/*
		 * goes for the next tag inside this message. this can be a new tag, or
		 * the closing soap:Body tag. check for text inside the body tag
		 */

		int eventType = parser.nextTag();
		////System.out.println("Name3:"+parser.getName());
		if (eventType == XmlPullParser.TEXT) {
			// eat unnecessary text
			parser.getText();
			eventType = parser.next();
		}
		if (eventType == XmlPullParser.START_TAG || eventType == XmlPullParser.END_TAG) {


			boolean messageHandled=handleMessage(parser, actionName,header, to,  protocolData );

			if (messageHandled)return;
			// this must be an invoke message
			/*
			 * there might be a soap:Fault within the body rather than the
			 * message stuff, check and propagate accordingly
			 */
			name = parser.getName();
			namespace = parser.getNamespace();
			if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace) && SOAPConstants.SOAP_ELEM_FAULT.equals(name)) {
				// The FaultMessage parses itself.
				IMessageEndpoint op = to.getOperation(actionName);
				header.setMessageEndpoint(op);
				//				to.receive(FaultMessage.parse(actionName, header, parser, op), protocolData);
				FaultMessage msg=null;
				try{
					helperLock.sharedLock();
					msg=messageConverter.parseFaultMessage(actionName, header, parser, protocolData,helper, op);

				}finally{
					helperLock.releaseSharedLock();
				}
				beforeReceive(msg, protocolData, to);
				to.receive(msg, protocolData);
				return;
			}

			InvokeMessage msg = new InvokeMessage(header);


			AttributedURI relatesTo = header.getRelatesTo();

			List l = new ArrayList();
			IMessageEndpoint operation = to.getOperation(msg.getAction().toString());
			header.setMessageEndpoint(operation);
			int tag=parser.getEventType();
			if (operation!=null)
			{
				while ((tag=parser.getEventType()) != XmlPullParser.END_TAG && tag != XmlPullParser.END_DOCUMENT) {
					////System.out.println("Depth "+depth+" "+parser.getDepth());
					/*
					 * if this is not the closing soap:Body, get the stuff inside.
					 */
					int ot = operation.getType();
					Element element = null;
					if ((relatesTo == null && ot == WSDLOperation.TYPE_SOLICIT_RESPONSE) || (relatesTo != null && ot == WSDLOperation.TYPE_REQUEST_RESPONSE) || (relatesTo != null && ot == WSDLOperation.TYPE_ONE_WAY) || (relatesTo == null && ot == WSDLOperation.TYPE_NOTIFICATION)) {
						element = operation.getOutput();
					} else if ((relatesTo != null && ot == WSDLOperation.TYPE_SOLICIT_RESPONSE) || (relatesTo == null && ot == WSDLOperation.TYPE_REQUEST_RESPONSE) || (relatesTo == null && ot == WSDLOperation.TYPE_ONE_WAY) || (relatesTo != null && ot == WSDLOperation.TYPE_NOTIFICATION)) {
						element = operation.getInput();
					}
					if (element!=null)
					{
						l.add(pvConverter.parseParameterValue(parser, element));
					}else{
						if (Log.isWarn())
							Log.warn("Could not determine parameter value due to missing element! Operation: "+operation+", input element: "+operation.getInput()+", output element:"+operation.getOutput());
					}
					
					if (parser.getEventType() != XmlPullParser.END_DOCUMENT){
						try{
						tag = parser.next();
						//////System.out.println("Start Tag"+tag);
						while (tag== XmlPullParser.COMMENT || tag==XmlPullParser.PROCESSING_INSTRUCTION || tag==XmlPullParser.IGNORABLE_WHITESPACE || tag==XmlPullParser.TEXT){
							////System.out.println("Skipping Tag"+tag+" depth"+ parser.getDepth()+" "+parser.getText());
							tag=parser.next();
						}
						////System.out.println("Tag after skipping:"+tag+" "+parser.getName()+" "+parser.getText());
						}catch(java.io.EOFException e){
							break;
						}
					}else{
						break;
					}
				}

				switch (l.size()) {
				case (0): {
					break;
				}
				case (1): {
					IParameterValue value = (IParameterValue) l.get(0);
					msg.setContent(value);
					break;
				}
				default: {
					throw new UnexpectedElementException("too much message parts: " + l.size() + "; next part=" + l.get(1).toString());
				}
				}
				sendInvokeMessageToReceiver(to, protocolData, msg); //extracted by SSch
			}else{
				if (Log.isError())
					Log.error("Could not find operation for "+msg.getAction().toString());
			}

		}
	}

	/**
	 * @param to
	 * @param protocolData
	 * @param msg
	 */
	protected void sendInvokeMessageToReceiver(MessageReceiver to,
			DPWSProtocolData protocolData, InvokeMessage msg) {
		beforeReceive(msg, protocolData, to); //added by SSch
		to.receive(msg, protocolData);
	}

	/**
	 * Added by SSch.
	 * 
	 * @param msg
	 * @param protocolData
	 * @param to
	 */
	protected void beforeReceive(Message msg,
			DPWSProtocolData protocolData, MessageReceiver to) 
	{
		//void
	}

	//Added 201-11-12 SSch:  to ease extension
	protected boolean handleMessage(ElementParser parser, String actionName, SOAPHeader header, MessageReceiver to, DPWSProtocolData protocolData) throws XmlPullParserException, IOException, VersionMismatchException {
		// The right Message parse itself.
		try{
			helperLock.sharedLock();
			if (helper==null || header==null) return false;
			if (helper.getWSDActionHello().equals(actionName)) {
				//			to.receive(HelloMessage.parse(header, parser, protocolData, helper), protocolData);
				//TODO SSch before every receive add beforeReceive(msg, protocolData, to);
				to.receive(messageConverter.parseHelloMessage(header, parser, protocolData, helper), protocolData);
				return true;
			} else if (helper.getWSDActionBye().equals(actionName)) {
				to.receive(messageConverter.parseByeMessage(header, parser, protocolData, helper), protocolData);
				return true;
			} else if (helper.getWSDActionProbe().equals(actionName)) {
				to.receive(messageConverter.parseProbeMessage(header, parser,protocolData, helper), protocolData);
				return true;
			} else if (helper.getWSDActionProbeMatches().equals(actionName)) {
				to.receive(messageConverter.parseProbeMatchesMessage(header, parser, protocolData, helper ), protocolData);
				return true;
			} else if (helper.getWSDActionResolve().equals(actionName)) {
				to.receive(messageConverter.parseResolveMessage(header, parser,protocolData, helper), protocolData);
				return true;
			} else if (helper.getWSDActionResolveMatches().equals(actionName)) {
				to.receive(messageConverter.parseResolveMatchesMessage(header, parser, protocolData, helper), protocolData);
				return true;
			} else if (WXFConstants.WXF_ACTION_GET.equals(actionName)) {
				if (protocolData.getTransportAddress() != null && DPWSCommunicationManager.getRegisterForGet().contains(protocolData.getTransportAddress())) {
					//				to.receive(new GetMessage(header), protocolData);
					to.receive(messageConverter.parseGetMessage(actionName, header, parser, protocolData, helper), protocolData);
				} else if (protocolData.getTransportAddress() != null && DPWSCommunicationManager.getRegisterForGetMetadata().contains(protocolData.getTransportAddress())) {
					//				to.receive(new GetMetadataMessage(header), protocolData);
					to.receive(messageConverter.parseGetMetadataMessage(actionName, header, parser, protocolData, helper), protocolData);
				}
				return true;
			} else if (WXFConstants.WXF_ACTION_GETRESPONSE.equals(actionName)) {

				HashSet check = (HashSet) DPWSCommunicationManager.getMessageIDsForGetMetadataMapping();
				if (check.contains(header.getRelatesTo())) {
					to.receive(messageConverter.parseGetMetadataResponseMessage(actionName,header, parser, protocolData,helper), protocolData);
					DPWSCommunicationManager.getMessageIDsForGetMetadataMapping().remove(header.getMessageId());
				} else {
					to.receive(messageConverter.parseGetResponseMessage(actionName,header, parser,protocolData, helper), protocolData);
				}
				return true;
			} else if (MEXConstants.WSX_ACTION_GETMETADATA_REQUEST.equals(actionName)) {
				to.receive(messageConverter.parseGetMetadataMessage(actionName,header, parser, protocolData, helper), protocolData);
				return true;
			} else if (MEXConstants.WSX_ACTION_GETMETADATA_RESPONSE.equals(actionName)) {
				to.receive(messageConverter.parseGetMetadataResponseMessage(actionName,header,  parser, protocolData, helper), protocolData);
				return true;
			} else if (WSEConstants.WSE_ACTION_SUBSCRIBE.equals(actionName)) {
				to.receive(messageConverter.parseSubscribeMessage(header, parser, protocolData, helper), protocolData);
				return true;
			} else if (WSEConstants.WSE_ACTION_SUBSCRIBERESPONSE.equals(actionName)) {
				to.receive(messageConverter.parseSubscribeResponseMessage(header, parser, protocolData, helper), protocolData);
				return true;
			} else if (WSEConstants.WSE_ACTION_RENEW.equals(actionName)) {
				to.receive(messageConverter.parseRenewMessage(header, parser, protocolData, helper), protocolData);
				return true;
			} else if (WSEConstants.WSE_ACTION_RENEWRESPONSE.equals(actionName)) {
				to.receive(messageConverter.parseRenewResponseMessage(header, parser, protocolData, helper), protocolData);
				return true;
			} else if (WSEConstants.WSE_ACTION_GETSTATUS.equals(actionName)) {
				to.receive(messageConverter.parseGetStatusMessage(header, parser, protocolData, helper), protocolData);
				return true;
			} else if (WSEConstants.WSE_ACTION_GETSTATUSRESPONSE.equals(actionName)) {
				to.receive(messageConverter.parseGetStatusResponseMessage(header, parser, protocolData, helper), protocolData);
				return true;
			} else if (WSEConstants.WSE_ACTION_UNSUBSCRIBE.equals(actionName)) {
				to.receive(messageConverter.parseUnsubscribeMessage(header, parser, protocolData, helper), protocolData);
				return true;
			} else if (WSEConstants.WSE_ACTION_UNSUBSCRIBERESPONSE.equals(actionName)) {
				//			to.receive(new UnsubscribeResponseMessage(header, parser, protocolData, helper), protocolData);
				to.receive(messageConverter.parseUnsubscribeResponseMessage(header, parser, protocolData, helper), protocolData);
				return true;
			} else if (WSEConstants.WSE_ACTION_SUBSCRIPTIONEND.equals(actionName)) {
				to.receive(messageConverter.parseSubscriptionEndMessage(header, parser, protocolData, helper), protocolData);
				return true;
			}
		}finally{
			helperLock.releaseSharedLock();
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.SOAP2MessageGenerator
	 * #generate(java.io.InputStream)
	 */
	@Override
	public Message generate(InputStream in) throws Exception {
		InlineMessageReceiver receiver = new InlineMessageReceiver();
		deliver(in, receiver, null, getDefaultMessageDiscarder(),null);
		if (receiver.e != null) {
			throw receiver.e;
		}
		return receiver.result;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.SOAP2MessageGenerator
	 * #deliver(java.io.InputStream,
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void deliver(InputStream in, MessageReceiver to, DPWSProtocolData protocolData) {
		deliver(in, to, protocolData, getDefaultMessageDiscarder(), null);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.SOAP2MessageGenerator
	 * #deliver(java.io.InputStream,
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver,
	 * org.ws4d.java.communication.DPWSProtocolData,
	 * org.ws4d.java.communication.protocol.soap.generator.MessageDiscarder)
	 */
	@Override
	public void deliver(InputStream in, MessageReceiver to, DPWSProtocolData protocolData, MessageDiscarder discarder, Message request) {
		XmlPullParser parser = createParser(); //Changed SSch - allow override

		//System.out.println("\t\t\t\tParser created [id="+protocolData.getInstanceId()+"]");
		//Resetting the helper
		try{
			helperLock.exclusiveLock();
			helper=null;
		}finally{
			helperLock.releaseExclusiveLock();
		}

		//TODO SSch Qos Framework ???
		SecurityManagerModule secMod=(SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);
		if (secMod!=null)
		{
			in = secMod.wrapInputStream(in, protocolData);
		}

		SOAPHeader header = null;
		try {
			parser.setInput(in, null);
			parser.nextTag(); // go to SOAP Envelope
			String namespace = parser.getNamespace();
			String name = parser.getName();

			if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace)) {
				if (SOAPConstants.SOAP_ELEM_ENVELOPE.equals(name)) {
					ElementParser elementParser = createNewElementParser(parser, request!=null?request.getTargetAddress():null); //Added by SSch in order to allow extension
					elementParser.nextTag(); // go to SOAP Header
					namespace = elementParser.getNamespace();
					name = elementParser.getName();
					if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace) 
							&& SOAPConstants.SOAP_ELEM_HEADER.equals(name)) {
						// SOAPHeader is parsing itself.
						//						header = SOAPHeader.parse(elementParser, helper);
						try{
							helperLock.sharedLock();
							header = messageHeaderConverter.parse(elementParser, helper);
						}finally{
							helperLock.releaseSharedLock();
						}
						if (Log.isDebug()) {
							Log.debug("<I> Incoming SOAP message header: [ " + header + " ]");
						}
						if (discarder == null) {
							discarder = getDefaultMessageDiscarder();
						}

						int reason = discarder.discardMessage(header, protocolData);

						if (reason > MessageDiscarder.NOT_DISCARDED) {
							MonitorStreamFactory msf = MonitorStreamFactoryProvider.getInstance().getMonitorStreamFactory();
							if (msf != null) {
								MonitoringContext context = msf.getMonitoringContextIn(protocolData);
								if (context != null) {
									msf.discard(protocolData, context, header, reason);
								} else {
									Log.warn("Cannot get correct monitoring context for message generation.");
								}
							}
							return;
						}
						name = elementParser.getName();
						////System.out.println("Name:"+name);
						elementParser.nextTag(); // go to SOAP Body
						namespace = elementParser.getNamespace();
						name = elementParser.getName();
						////System.out.println("Name2:"+name);
					}
					if (SOAPConstants.SOAP12_NAMESPACE_NAME.equals(namespace) && SOAPConstants.SOAP_ELEM_BODY.equals(name)) {
						//System.out.println("Delivering body [id="+protocolData.getInstanceId()+"]");
						deliverBody(header, elementParser, to, protocolData, request);
						//System.out.println("Delivering body done. [id="+protocolData.getInstanceId()+"]");
					} else {
						// no body present
						throw new UnexpectedElementException(namespace + ":" + name + " (SOAP12:Body expected)");
					}

				} else {
					// no envelope present
					throw new UnexpectedElementException(namespace + ":" + name + " (SOAP12:Envelope expected)");
				}
			} else if (SOAPConstants.SOAP11_OLD_NAMESPACE_NAME.equals(namespace)) {
				throw new VersionMismatchException("SOAP " + SOAPConstants.SOAP11_OLD_NAMESPACE_NAME);
			} else {
				// no envelope present
				throw new UnexpectedElementException(namespace + ":" + name + " (SOAP12:Envelope expected)");
			}
		} catch (VersionMismatchException e) {
			// only SOAP Envelope or WS-Addressing [action] can cause this
			if (Log.isDebug()) Log.debug("Version mismatch: " + e.getMessage()+" SOAPHeader:"+header+" ProtocolData:"+protocolData);
			to.receiveFailed(e, protocolData);
		} catch (UnexpectedMessageException e) {
			if (Log.isInfo()){
				Log.info("Unexpected message: " + e.getMessage()+" SOAPHeader:"+header+" ProtocolData:"+protocolData);
				Log.info(e);
			}
			to.receiveFailed(e, protocolData);
		} catch (MissingElementException e) {
			// only WS-Addressing [action] can cause this
			if (Log.isInfo()){
				Log.info("Missing required element " + e.getMessage()+" SOAPHeader:"+header+" ProtocolData:"+protocolData);
				Log.info(e);
			}
			to.receiveFailed(e, protocolData);
		} catch (UnexpectedElementException e) {
			if (Log.isInfo()){
				Log.info("Unexpected element: " + e.getMessage()+" ProtocolData:"+protocolData);
				Log.info(e);
			}
			to.receiveFailed(e, protocolData);
		} catch (XmlPullParserException e) {
			if (Log.isInfo()){
				Log.info("Parse exception during XML processing: " + e + ", caused by " + e.getDetail()+" SOAPHeader:"+header+" ProtocolData:"+protocolData);
				Log.info(e);
			}
			to.receiveFailed(e, protocolData);
		} catch (IOException e) {
			if (Log.isInfo()){
				Log.info("IO exception during XML processing: " + e +" SOAPHeader:"+header+" ProtocolData:"+protocolData);
				Log.info(e);
			}
			to.receiveFailed(e, protocolData);
		}catch(Exception e){
			if (Log.isInfo()){
				Log.info((e.getMessage()!=null?e.getMessage():e.getClass())+" SOAPHeader:"+header+" ProtocolData:"+protocolData);
				Log.info(e);
			}
			to.receiveFailed(e, protocolData);
		} finally {
			try {

				releaseParser(parser);
				//System.out.println("\t\t\t\tParser released. [id="+protocolData.getInstanceId()+"]");
				//SSch Bugfix: memory leak
				if (secMod!=null)
				{
					secMod.releaseMessage(protocolData);
				}
			} catch (XmlPullParserException e2) {
				// shouldn't ever occur
				if (Log.isError()){
					Log.error("Unable to reset XML parser: " + e2+" ProtocolData:"+protocolData);
					Log.error(e2);
				}
				
			}
		}
	}

	/**
	 * Added by SSch.
	 * 
	 * @param parser2
	 * @param in 
	 * @return
	 */
	protected ElementParser createNewElementParser(XmlPullParser parser, URI fromUri) {
		ElementParser retVal= new ElementParser(parser, fromUri);
		return retVal;
	}


	protected XmlPullParser createParser() {
		XmlPullParser parser = null;
		try {
			parser = XmlPullParserSupport.getFactory().newPullParser();
		} catch (XmlPullParserException e) {
			Log.error("Could not create XmlPullParser: " + e);
			Log.error(e);
			throw new RuntimeException("Could not create XmlPullParser: " + e);
		}
		return parser;
	}

	protected void releaseParser(XmlPullParser parser) throws XmlPullParserException {
		if (parser!=null)
			parser.setInput(null);
	}

	protected static class InlineMessageReceiver implements MessageReceiver {

		Message		result;

		Exception	e;

		@Override
		public void sendFailed(Exception e, DPWSProtocolData protocolData) {
			this.e = e;
		}

		@Override
		public void receiveFailed(Exception e, DPWSProtocolData protocolData) {
			this.e = e;
		}

		@Override
		public void receive(FaultMessage fault, DPWSProtocolData protocolData) {
			this.result = fault;
		}

		@Override
		public void receive(InvokeMessage invoke, DPWSProtocolData protocolData) {
			this.result = invoke;
		}

		@Override
		public void receive(SubscriptionEndMessage subscriptionEnd, DPWSProtocolData protocolData) {
			this.result = subscriptionEnd;
		}

		@Override
		public void receive(UnsubscribeResponseMessage unsubscribeResponse, DPWSProtocolData protocolData) {
			this.result = unsubscribeResponse;
		}

		@Override
		public void receive(UnsubscribeMessage unsubscribe, DPWSProtocolData protocolData) {
			this.result = unsubscribe;
		}

		@Override
		public void receive(RenewResponseMessage renewResponse, DPWSProtocolData protocolData) {
			this.result = renewResponse;
		}

		@Override
		public void receive(RenewMessage renew, DPWSProtocolData protocolData) {
			this.result = renew;
		}

		@Override
		public void receive(GetStatusResponseMessage getStatusResponse, DPWSProtocolData protocolData) {
			this.result = getStatusResponse;
		}

		@Override
		public void receive(GetStatusMessage getStatus, DPWSProtocolData protocolData) {
			this.result = getStatus;
		}

		@Override
		public void receive(SubscribeResponseMessage subscribeResponse, DPWSProtocolData protocolData) {
			this.result = subscribeResponse;
		}

		@Override
		public void receive(SubscribeMessage subscribe, DPWSProtocolData protocolData) {
			this.result = subscribe;
		}

		@Override
		public void receive(GetMetadataResponseMessage getMetadataResponse, DPWSProtocolData protocolData) {
			this.result = getMetadataResponse;
		}

		@Override
		public void receive(GetMetadataMessage getMetadata, DPWSProtocolData protocolData) {
			this.result = getMetadata;
		}

		@Override
		public void receive(GetResponseMessage getResponse, DPWSProtocolData protocolData) {
			this.result = getResponse;
		}

		@Override
		public void receive(GetMessage get, DPWSProtocolData protocolData) {
			this.result = get;
		}

		@Override
		public void receive(ResolveMatchesMessage resolveMatches, DPWSProtocolData protocolData) {
			this.result = resolveMatches;
		}

		@Override
		public void receive(ResolveMessage resolve, DPWSProtocolData protocolData) {
			this.result = resolve;
		}

		@Override
		public void receive(ProbeMatchesMessage probeMatches, DPWSProtocolData protocolData) {
			this.result = probeMatches;
		}

		@Override
		public void receive(ProbeMessage probe, DPWSProtocolData protocolData) {
			this.result = probe;
		}

		@Override
		public void receive(ByeMessage bye, DPWSProtocolData protocolData) {
			this.result = bye;
		}

		@Override
		public void receive(HelloMessage hello, DPWSProtocolData protocolData) {
			this.result = hello;
		}

		@Override
		public IMessageEndpoint getOperation(String action) {
			return null;
		}
	}


}
/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication;

import org.ws4d.java.constants.CommunicationConstants;

/**
 * The protocol ID of the DPWS technology.
 */
public final class DPWSCommunicationManagerID implements CommunicationManagerID {

	public static final CommunicationManagerID	INSTANCE	= new DPWSCommunicationManagerID();

	/*
	 * Disallow any instances from outside this class.
	 */
	private DPWSCommunicationManagerID() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.CommunicationID#getId()
	 */
	@Override
	public String getId() {
		return CommunicationConstants.COMMUNICATION_MANAGER_DPWS;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getId();
	}

	@Override
	public String getImplementationClassname() {
		return null;
	}

}

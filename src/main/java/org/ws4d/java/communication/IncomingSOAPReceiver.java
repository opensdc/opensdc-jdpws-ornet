/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication;

import org.ws4d.java.communication.DPWS2006.DefaultDPWSUtil2006;
import org.ws4d.java.communication.protocol.soap.server.SOAPServer.SOAPHandler;
import org.ws4d.java.constants.SOAPConstants;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.dispatch.MessageInformer;
import org.ws4d.java.framework.module.EventingModule;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.IMessageEndpoint;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageException;
import org.ws4d.java.message.discovery.ByeMessage;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMessage;
import org.ws4d.java.message.eventing.GetStatusMessage;
import org.ws4d.java.message.eventing.GetStatusResponseMessage;
import org.ws4d.java.message.eventing.RenewMessage;
import org.ws4d.java.message.eventing.RenewResponseMessage;
import org.ws4d.java.message.eventing.SubscribeMessage;
import org.ws4d.java.message.eventing.SubscribeResponseMessage;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.message.eventing.UnsubscribeMessage;
import org.ws4d.java.message.eventing.UnsubscribeResponseMessage;
import org.ws4d.java.message.metadata.GetMessage;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.message.metadata.GetResponseMessage;
import org.ws4d.java.util.Log;

/**
 * 
 */
final class IncomingSOAPReceiver extends SOAPHandler {

	private static final MessageInformer	MESSAGE_INFORMER	= MessageInformer.getInstance();

	private final IncomingMessageListener	listener;

	private final DefaultDPWSUtil2006		util				= DefaultDPWSUtil2006.getInstance();

	static void markIncoming(Message message) {
		message.setInbound(true);
		if (Log.isDebug()) {
			Log.debug("<I> " + message);
		}
	}

	static void markOutgoing(Message message) {
		message.setInbound(false);
		if (Log.isDebug()) {
			Log.debug("<O> " + message);
		}
	}

	/**
	 * This method simply returns straightaway as long as the eventing module is
	 * present within the current runtime. If the eventing module is
	 * <em>not</em> present, it throws a <code>SOAPException</code> with a
	 * corresponding fault message describing the problem.
	 * 
	 * @param msg the message received
	 * @throws MessageException if the eventing module is not present and
	 */
	private static void checkEventingPresence(Message msg) throws MessageException {
//		if (DPWSFramework.hasModule(DPWSFramework.DPWS_EVENTING_MODULE)) {
//			return;
//		}
		if (FrameworkModuleRegistry.getInstance().hasModule(EventingModule.class)){
			return;
		}
		
		throw new MessageException(FaultMessage.createActionNotSupportedFault(msg));
	}

	/**
	 * @param listener
	 */
	IncomingSOAPReceiver(IncomingMessageListener listener) {
		super();
		this.listener = listener;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.HelloMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(HelloMessage hello, DPWSProtocolData protocolData) {
		respondWithActionNotSupported(hello, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ByeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(ByeMessage bye, DPWSProtocolData protocolData) {
		respondWithActionNotSupported(bye, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ProbeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(ProbeMessage probe, DPWSProtocolData protocolData) {
		// this is for directed probes to a device
		probe.setDirected(true);
		markIncoming(probe);
		try {
			Message responseMessage = listener.handle(probe, protocolData);
			MESSAGE_INFORMER.forwardMessage(probe, protocolData);
			if (responseMessage == null) {
				// never send null but an empty probe matches response
				responseMessage = new ProbeMatchesMessage();
				responseMessage.setResponseTo(probe);
			}
			// Check for Messageversion, if Version = 2006 the Namespaces and
			// some attributs must be changed
			if (responseMessage.getVersion() == DPWSProtocolVersionInfo.DPWS2006) {
				util.changeOutgoingMessage(responseMessage);
				if (Log.isDebug()) {
					Log.debug("<I> Namespaces changed to DPWS 2006 for Directed ProbeMatchesMessage");
				}
			}

			respondWithMessage(responseMessage);
		} catch (MessageException e) {
			MESSAGE_INFORMER.forwardMessage(probe, protocolData);
			respondWithFault(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ProbeMatchesMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(ProbeMatchesMessage probeMatches, DPWSProtocolData protocolData) {
		respondWithActionNotSupported(probeMatches, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ResolveMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(ResolveMessage resolve, DPWSProtocolData protocolData) {
		respondWithActionNotSupported(resolve, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.discovery.ResolveMatchesMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(ResolveMatchesMessage resolveMatches, DPWSProtocolData protocolData) {
		respondWithActionNotSupported(resolveMatches, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.metadata.GetMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetMessage get, DPWSProtocolData protocolData) {
		markIncoming(get);
		try {
			Message responseMessage = listener.handle(get, protocolData);
			if (responseMessage.getVersion() == DPWSProtocolVersionInfo.DPWS2006) {
				DefaultDPWSUtil2006.getInstance().changeOutgoingMessage( responseMessage);
			}
			MESSAGE_INFORMER.forwardMessage(get, protocolData);
			respondWithMessage(responseMessage);
		} catch (MessageException e) {
			MESSAGE_INFORMER.forwardMessage(get, protocolData);
			respondWithFault(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.metadata.GetResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetResponseMessage getResponse, DPWSProtocolData protocolData) {
		respondWithActionNotSupported(getResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.metadata.GetMetadataMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetMetadataMessage getMetadata, DPWSProtocolData protocolData) {
		markIncoming(getMetadata);
		try {
			MESSAGE_INFORMER.forwardMessage(getMetadata, protocolData);
			Message responseMessage = listener.handle(getMetadata, protocolData);

			// Check for Messageversion, if Version = 2006 the Namespaces and
			// some attributs must be changed
			if (responseMessage.getVersion() == DPWSProtocolVersionInfo.DPWS2006) {
				util.changeOutgoingMessage(responseMessage);
				if (Log.isDebug()) {
					Log.debug("<I> Namespaces changed to DPWS 2006 for GetMetadataResponseMessage");
				}
			}
			respondWithMessage(responseMessage);
		} catch (MessageException e) {
			MESSAGE_INFORMER.forwardMessage(getMetadata, protocolData);
			respondWithFault(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.metadata. GetMetadataResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetMetadataResponseMessage getMetadataResponse, DPWSProtocolData protocolData) {
		respondWithActionNotSupported(getMetadataResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.SubscribeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(SubscribeMessage subscribe, DPWSProtocolData protocolData) {
		markIncoming(subscribe);
		try {
			checkEventingPresence(subscribe);
			Message responseMessage = listener.handle(subscribe, protocolData);
			MESSAGE_INFORMER.forwardMessage(subscribe, protocolData);
			respondWithMessage(responseMessage);
		} catch (MessageException e) {
			MESSAGE_INFORMER.forwardMessage(subscribe, protocolData);
			respondWithFault(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.SubscribeResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(SubscribeResponseMessage subscribeResponse, DPWSProtocolData protocolData) {
		respondWithActionNotSupported(subscribeResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.GetStatusMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetStatusMessage getStatus, DPWSProtocolData protocolData) {
		markIncoming(getStatus);
		try {
			checkEventingPresence(getStatus);
			Message responseMessage = listener.handle(getStatus, protocolData);
			MESSAGE_INFORMER.forwardMessage(getStatus, protocolData);
			respondWithMessage(responseMessage);
		} catch (MessageException e) {
			MESSAGE_INFORMER.forwardMessage(getStatus, protocolData);
			respondWithFault(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.GetStatusResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(GetStatusResponseMessage getStatusResponse, DPWSProtocolData protocolData) {
		respondWithActionNotSupported(getStatusResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.RenewMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(RenewMessage renew, DPWSProtocolData protocolData) {
		markIncoming(renew);
		try {
			checkEventingPresence(renew);
			Message responseMessage = listener.handle(renew, protocolData);
			MESSAGE_INFORMER.forwardMessage(renew, protocolData);
			respondWithMessage(responseMessage);
		} catch (MessageException e) {
			MESSAGE_INFORMER.forwardMessage(renew, protocolData);
			respondWithFault(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.RenewResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(RenewResponseMessage renewResponse, DPWSProtocolData protocolData) {
		respondWithActionNotSupported(renewResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.UnsubscribeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(UnsubscribeMessage unsubscribe, DPWSProtocolData protocolData) {
		markIncoming(unsubscribe);
		try {
			checkEventingPresence(unsubscribe);
			Message responseMessage = listener.handle(unsubscribe, protocolData);
			MESSAGE_INFORMER.forwardMessage(unsubscribe, protocolData);
			respondWithMessage(responseMessage);
		} catch (MessageException e) {
			MESSAGE_INFORMER.forwardMessage(unsubscribe, protocolData);
			respondWithFault(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.UnsubscribeResponseMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(UnsubscribeResponseMessage unsubscribeResponse, DPWSProtocolData protocolData) {
		respondWithActionNotSupported(unsubscribeResponse, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.eventing.SubscriptionEndMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(SubscriptionEndMessage subscriptionEnd, DPWSProtocolData protocolData) {
		markIncoming(subscriptionEnd);
		listener.handle(subscriptionEnd, protocolData);
		MESSAGE_INFORMER.forwardMessage(subscriptionEnd, protocolData);
		respond(202, null);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.invocation.InvokeMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(InvokeMessage invoke, DPWSProtocolData protocolData) {
		markIncoming(invoke);
		try {
			Message responseMessage = listener.handle(invoke, protocolData);
			MESSAGE_INFORMER.forwardMessage(invoke, protocolData);
			respondWithMessage(responseMessage);
		} catch (MessageException e) {
			MESSAGE_INFORMER.forwardMessage(invoke, protocolData);
			respondWithFault(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#
	 * getOperation(java.lang.String)
	 */
	@Override
	public IMessageEndpoint getOperation(String action) {
		return listener.getOperation(action);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.ws4d.java.message.FaultMessage,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receive(FaultMessage fault, DPWSProtocolData protocolData) {
		respondWithActionNotSupported(fault, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#
	 * receiveFailed(java.lang.Exception,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void receiveFailed(Exception e, DPWSProtocolData protocolData) {
		FaultMessage fault=null;
		if (e !=null)
		{
			if (e instanceof MessageException){ 
				respondWithFault((MessageException) e);
				return;
			}
		}
		
		respond(400, fault);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#
	 * sendFailed(java.lang.Exception,
	 * org.ws4d.java.communication.DPWSProtocolData)
	 */
	@Override
	public void sendFailed(Exception e, DPWSProtocolData protocolData) {
		if (Log.isWarn()) Log.warn("Send fail!"+e.getMessage());
		
	}

	/**
	 * @param responseMessage
	 */
	private void respondWithMessage(Message responseMessage) {
		if (responseMessage == null) {
			respond(202, null);
		} else {
			markOutgoing(responseMessage);
			respond(200, responseMessage);
		}
	}

	/**
	 * @param e
	 */
	private void respondWithFault(MessageException e) {
		FaultMessage fault=null;
		int errorCode=500;
		if (e!=null) {
			fault= e.getFault();
			markOutgoing(fault);
			errorCode=(SOAPConstants.SOAP_FAULT_SENDER.equals(fault.getCode()))?400:500;
		}
		
		respond(errorCode, fault);
	}

	/**
	 * @param message
	 */
	private void respondWithActionNotSupported(Message message, DPWSProtocolData protocolData) {
		markIncoming(message);
		String actionName = message.getAction().toString();
		if (Log.isWarn()) Log.warn("<I> Unexpected SOAP request message: " + actionName);
		if (Log.isDebug()) Log.debug(message.toString());
		MESSAGE_INFORMER.forwardMessage(message, protocolData);
		FaultMessage fault = FaultMessage.createActionNotSupportedFault(message);
		markOutgoing(fault);
		respond(400, fault);
	}
}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication;

import org.ws4d.java.communication.DPWS2006.DefaultConstantsHelper2006;
import org.ws4d.java.communication.DPWS2009.DefaultConstantsHelper2009;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.DPWSConstants2006;
import org.ws4d.java.message.Message;

public abstract class DPWSUtil {

	public static ConstantsHelper getHelper(ProtocolVersionInfo versionInfo) {
		ConstantsHelper helper = null;
		if (versionInfo instanceof DPWSProtocolVersionInfo)
		{
			DPWSProtocolVersionInfo info = (DPWSProtocolVersionInfo) versionInfo;
			switch (info.getDpwsVersion()) {
			case DPWSConstants.DPWS_VERSION2009:
				helper = new DefaultConstantsHelper2009();
				break;
			case DPWSConstants2006.DPWS_VERSION2006:
				helper = new DefaultConstantsHelper2006();
				break;
			}
		}else{
			helper = new DefaultConstantsHelper2009();
		}
		return helper;
	}



	public abstract void changeOutgoingMessage(Message message);

	public abstract Message copyOutgoingMessage(Message message);

}

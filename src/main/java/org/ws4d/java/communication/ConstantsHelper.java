/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.URI;

public interface ConstantsHelper {

	public int getDPWSVersion();

	/**
	 * DPWS Constants
	 */
	public String getDPWSNamespace();

	public String getDPWSNamespacePrefix();

	public String getDPWSFilterEventingAction();

	public URI getDPWSUriFilterEeventingAction();

	public QName getDPWSFaultFilterActionNotSupported();

	public String getMetadataDialectThisModel();

	public String getMetadataDialectThisDevice();

	public String getMetatdataDialectRelationship();

	public String getMetadataRelationshipHostingType();

	public String getDPWSActionFault();

	public QName getDPWSQnManufacturer();

	public QName getDPWSQnManufactuerURL();

	public QName getDPWSQnModelname();

	public QName getDPWSQnModelnumber();

	public QName getDPWSQnModelURL();

	public QName getDPWSQnPresentationURL();

	public QName getDPWSQnFriendlyName();

	public QName getDPWSQnFirmware();

	public QName getDPWSQnSerialnumber();

	public QName getDPWSQnServiceID();

	public QName getDPWSQnEndpointReference();

	public QName getDPWSQnTypes();

	public QName getDPWSQnDeviceType();

	/**
	 * WSA Constants
	 */
	public String getWSANamespace();

	public String getWSAElemReferenceProperties();

	public String getWSAElemPortType();

	public String getWSAElemServiceName();

	public String getWSAElemPolicy();

	public URI getWSAAnonymus();

	public String getWSAActionAddressingFault();

	public String getWSAActionSoapFault();

	/* faults */
	public QName getWSAFaultDestinationUnreachable();

	public QName getWSAFaultInvalidAddressingHeader();

	public QName getWSAFaultMessageAddressingHeaderRequired();

	public QName getWSAFaultActionNotSupported();

	public QName getWSAfaultEndpointUnavailable();

	public QName getWSAProblemHeaderQname();

	public QName getWSAProblemAction();

	/**
	 * WSD Constants
	 */
	public String getWSDNamespace();

	public String getWSDTo();

	public String getWSDActionHello();

	public String getWSDActionBye();

	public String getWSDActionProbe();

	public String getWSDActionProbeMatches();

	public String getWSDActionResolve();

	public String getWSDActionResolveMatches();

	public String getWSDActionFault();
}

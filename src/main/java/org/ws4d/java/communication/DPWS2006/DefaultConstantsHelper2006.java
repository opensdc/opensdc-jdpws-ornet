/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.DPWS2006;

import org.ws4d.java.communication.ConstantsHelper;
import org.ws4d.java.constants.DPWSConstants2006;
import org.ws4d.java.constants.WSAConstants2006;
import org.ws4d.java.constants.WSDConstants2006;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.URI;

public class DefaultConstantsHelper2006 implements ConstantsHelper {

	private static DefaultConstantsHelper2006	helper	= new DefaultConstantsHelper2006();

	public static DefaultConstantsHelper2006 getInstance() {
		if (helper != null)
			return helper;
		else
			return new DefaultConstantsHelper2006();
	}

	@Override
	public int getDPWSVersion() {
		return DPWSConstants2006.DPWS_VERSION2006;
	}

	/**
	 * DPWS Constants
	 */
	@Override
	public String getDPWSNamespace() {
		return DPWSConstants2006.DPWS_NAMESPACE_NAME;
	}

	@Override
	public String getDPWSNamespacePrefix() {
		return DPWSConstants2006.DPWS_NAMESPACE_PREFIX;
	}

	@Override
	public String getDPWSFilterEventingAction() {
		return DPWSConstants2006.DPWS_FILTER_EVENTING_ACTION;
	}

	@Override
	public URI getDPWSUriFilterEeventingAction() {
		return DPWSConstants2006.DPWS_URI_FILTER_EVENTING_ACTION;
	}

	@Override
	public QName getDPWSFaultFilterActionNotSupported() {
		return DPWSConstants2006.DPWS_FAULT_FILTER_ACTION_NOT_SUPPORTED;
	}

	/** METADATA. */
	@Override
	public String getMetadataDialectThisModel() {
		return DPWSConstants2006.METADATA_DIALECT_THISMODEL;
	}

	@Override
	public String getMetadataDialectThisDevice() {
		return DPWSConstants2006.METADATA_DIALECT_THISDEVICE;
	}

	@Override
	public String getMetatdataDialectRelationship() {
		return DPWSConstants2006.METADATA_DIALECT_RELATIONSHIP;
	}

	@Override
	public String getMetadataRelationshipHostingType() {
		return DPWSConstants2006.METADATA_RELATIONSHIP_HOSTING_TYPE;
	}

	/** The DPWS SOAP fault action. */
	@Override
	public String getDPWSActionFault() {
		return DPWSConstants2006.DPWS_ACTION_DPWS_FAULT;
	}

	/** QualifiedName of "Manufacturer". */
	@Override
	public QName getDPWSQnManufacturer() {
		return DPWSConstants2006.DPWS_QN_MANUFACTURER;
	}

	/** QualifiedName of "ManufacturerUrl". */
	@Override
	public QName getDPWSQnManufactuerURL() {
		return DPWSConstants2006.DPWS_QN_MANUFACTURERURL;
	}

	/** QualifiedName of "ModelName". */
	@Override
	public QName getDPWSQnModelname() {
		return DPWSConstants2006.DPWS_QN_MODELNAME;
	}

	/** QualifiedName of "ModelNumber". */
	@Override
	public QName getDPWSQnModelnumber() {
		return DPWSConstants2006.DPWS_QN_MODELNUMBER;
	}

	/** QualifiedName of "ModelUrl". */
	@Override
	public QName getDPWSQnModelURL() {
		return DPWSConstants2006.DPWS_QN_MODELURL;
	}

	/** QualifiedName of "PresentationUrl". */
	@Override
	public QName getDPWSQnPresentationURL() {
		return DPWSConstants2006.DPWS_QN_PRESENTATIONURL;
	}

	// QualifiedNames of ThisDevice

	/** QualifiedName of "FriendlyName". */
	@Override
	public QName getDPWSQnFriendlyName() {
		return DPWSConstants2006.DPWS_QN_FRIENDLYNAME;
	}

	/** QualifiedName of "FirmwareVersion". */
	@Override
	public QName getDPWSQnFirmware() {
		return DPWSConstants2006.DPWS_QN_FIRMWARE;
	}

	/** QualifiedName of "SerialNumber". */
	@Override
	public QName getDPWSQnSerialnumber() {
		return DPWSConstants2006.DPWS_QN_SERIALNUMBER;
	}

	// QualifiedNames of Host

	/** QualifiedName of "ServiceId". */
	@Override
	public QName getDPWSQnServiceID() {
		return DPWSConstants2006.DPWS_QN_SERVICEID;
	}

	/** QualifiedName of "EndpointReference". */
	@Override
	public QName getDPWSQnEndpointReference() {
		return DPWSConstants2006.DPWS_QN_ENDPOINTREFERENCE;
	}

	/** QualifiedName of "Types". */
	@Override
	public QName getDPWSQnTypes() {
		return DPWSConstants2006.DPWS_QN_TYPES;
	}

	/** DPWS dpws:Device type like described in R1020 */
	@Override
	public QName getDPWSQnDeviceType() {
		return DPWSConstants2006.DPWS_QN_DEVICETYPE;
	}

	/**
	 * WSA Constants
	 */
	@Override
	public String getWSANamespace() {
		return WSAConstants2006.WSA_NAMESPACE_NAME;
	}

	@Override
	public String getWSAElemReferenceProperties() {
		return WSAConstants2006.WSA_ELEM_REFERENCE_PROPERTIES;
	}

	@Override
	public String getWSAElemPortType() {
		return WSAConstants2006.WSA_ELEM_PORT_TYPE;
	}

	@Override
	public String getWSAElemServiceName() {
		return WSAConstants2006.WSA_ELEM_SERVICE_NAME;
	}

	@Override
	public String getWSAElemPolicy() {
		return WSAConstants2006.WSA_ELEM_POLICY;
	}

	@Override
	public URI getWSAAnonymus() {
		return WSAConstants2006.WSA_ANONYMOUS;
	}

	@Override
	public String getWSAActionAddressingFault() {
		return WSAConstants2006.WSA_ACTION_ADDRESSING_FAULT;
	}

	@Override
	public String getWSAActionSoapFault() {
		return WSAConstants2006.WSA_ACTION_SOAP_FAULT;
	}

	/* faults */
	@Override
	public QName getWSAFaultDestinationUnreachable() {
		return WSAConstants2006.WSA_FAULT_DESTINATION_UNREACHABLE;
	}

	@Override
	public QName getWSAFaultInvalidAddressingHeader() {
		return WSAConstants2006.WSA_FAULT_INVALID_ADDRESSING_HEADER;
	}

	@Override
	public QName getWSAFaultMessageAddressingHeaderRequired() {
		return WSAConstants2006.WSA_FAULT_MESSAGE_ADDRESSING_HEADER_REQUIRED;
	}

	@Override
	public QName getWSAFaultActionNotSupported() {
		return WSAConstants2006.WSA_FAULT_ACTION_NOT_SUPPORTED;
	}

	@Override
	public QName getWSAfaultEndpointUnavailable() {
		return WSAConstants2006.WSA_FAULT_ENDPOINT_UNAVAILABLE;
	}

	@Override
	public QName getWSAProblemHeaderQname() {
		return WSAConstants2006.WSA_PROBLEM_HEADER_QNAME;
	}

	@Override
	public QName getWSAProblemAction() {
		return WSAConstants2006.WSA_PROBLEM_ACTION;
	}

	/**
	 * WSD Constants
	 */
	@Override
	public String getWSDNamespace() {
		return WSDConstants2006.WSD_NAMESPACE_NAME;
	}

	@Override
	public String getWSDTo() {
		return WSDConstants2006.WSD_TO;
	}

	@Override
	public String getWSDActionHello() {
		return WSDConstants2006.WSD_ACTION_HELLO;
	}

	@Override
	public String getWSDActionBye() {
		return WSDConstants2006.WSD_ACTION_BYE;
	}

	@Override
	public String getWSDActionProbe() {
		return WSDConstants2006.WSD_ACTION_PROBE;
	}

	@Override
	public String getWSDActionProbeMatches() {
		return WSDConstants2006.WSD_ACTION_PROBEMATCHES;
	}

	@Override
	public String getWSDActionResolve() {
		return WSDConstants2006.WSD_ACTION_RESOLVE;
	}

	@Override
	public String getWSDActionResolveMatches() {
		return WSDConstants2006.WSD_ACTION_RESOLVEMATCHES;
	}

	@Override
	public String getWSDActionFault() {
		return WSDConstants2006.WSD_ACTION_WSD_FAULT;
	}
}

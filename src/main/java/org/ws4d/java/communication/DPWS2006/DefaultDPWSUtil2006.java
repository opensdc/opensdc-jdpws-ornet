/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.DPWS2006;

import org.ws4d.java.communication.DPWSUtil;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.DPWSConstants2006;
import org.ws4d.java.constants.DPWSMessageConstants;
import org.ws4d.java.constants.WSAConstants2006;
import org.ws4d.java.constants.WSDConstants2006;
import org.ws4d.java.constants.WXFConstants;
import org.ws4d.java.io.xml.ElementHandlerRegistry;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.message.discovery.ByeMessage;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.message.discovery.ProbeMatch;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMessage;
import org.ws4d.java.message.eventing.SubscribeMessage;
import org.ws4d.java.message.metadata.GetMessage;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.message.metadata.GetResponseMessage;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.ServiceId;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.IDGenerator;

public class DefaultDPWSUtil2006 extends DPWSUtil {

	// ##################################################
	// ############### Singelton Instance ###############
	private static DefaultDPWSUtil2006	instance	= null;

	public static DefaultDPWSUtil2006 getInstance() {
		if (instance == null) {
			//Register the Handler for DPWS 2006 ServiceID Element
			ElementHandlerRegistry.getRegistry().registerElementHandler(ServiceId.QNAME, new ServiceId());
			instance = new DefaultDPWSUtil2006();
		}
		return instance;
	}

	// ##################################################

	/**
	 * This method change the "Namespaces" and some "Attributs" to the DPWS 2006
	 * Specification.
	 */
	@Override
	public void changeOutgoingMessage(Message message) {
		switch (message.getType()) {
			case DPWSMessageConstants.HELLO_MESSAGE:
				changeOutgoingMessage((HelloMessage) message);
				break;
			case DPWSMessageConstants.BYE_MESSAGE:
				changeOutgoingMessage((ByeMessage) message);
				break;
			case DPWSMessageConstants.PROBE_MESSAGE:
				changeOutgoingMessage((ProbeMessage) message);
				break;
			case DPWSMessageConstants.PROBE_MATCHES_MESSAGE:
				changeOutgoingMessage((ProbeMatchesMessage) message);
				break;
			case DPWSMessageConstants.RESOLVE_MESSAGE:
				changeOutgoingMessage((ResolveMessage) message);
				break;
			case DPWSMessageConstants.RESOLVE_MATCHES_MESSAGE:
				changeOutgoingMessage((ResolveMatchesMessage) message);
				break;
			case DPWSMessageConstants.GET_MESSAGE:
				changeOutgoingMessage((GetMessage) message);
				break;
			case DPWSMessageConstants.GET_RESPONSE_MESSAGE:
				changeOutgoingMessage((GetResponseMessage) message);
				break;
			case DPWSMessageConstants.GET_METADATA_MESSAGE:
				changeOutgoingMessage((GetMetadataMessage) message);
				break;
			case DPWSMessageConstants.GET_METADATA_RESPONSE_MESSAGE:
				changeOutgoingMessage((GetMetadataResponseMessage) message);
				break;
			case DPWSMessageConstants.SUBSCRIBE_MESSAGE:
				changeOutgoingMessage((SubscribeMessage) message);
		}
	}

	private void changeOutgoingMessage(HelloMessage hello) {
		// Change wsa:Action
		hello.getHeader().setAction(new AttributedURI(WSDConstants2006.WSD_ACTION_HELLO));
		// Change wsa:To
		if (hello.getHeader().getTo() != null) {
			hello.getHeader().setTo(new AttributedURI(WSDConstants2006.WSD_TO));
		}
		
		changeTypesTo2006(hello.getTypes());
	}

	private void changeOutgoingMessage(ByeMessage bye) {
		// Change wsa:Action
		bye.getHeader().setAction(new AttributedURI(WSDConstants2006.WSD_ACTION_BYE));
		// Change wsa:To
		if (bye.getHeader().getTo() != null) {
			bye.getHeader().setTo(new AttributedURI(WSDConstants2006.WSD_TO));
		}
		
		changeTypesTo2006(bye.getTypes());
	}

	private void changeOutgoingMessage(ProbeMessage probe) {
		// Change wsa:Action
		probe.getHeader().setAction(new AttributedURI(WSDConstants2006.WSD_ACTION_PROBE));
		// Change wsa:To
		probe.getHeader().setTo(new AttributedURI(WSDConstants2006.WSD_TO));
	}

	private void changeOutgoingMessage(ProbeMatchesMessage probeMatches) {
		// Change wsa:Action
		probeMatches.getHeader().setAction(new AttributedURI(WSDConstants2006.WSD_ACTION_PROBEMATCHES));
		// Change wsa:To
		if (probeMatches.getHeader().getTo() != null) {
			probeMatches.getHeader().setTo(new AttributedURI(WSAConstants2006.WSA_ANONYMOUS));
		}
		// Change DPWS Devicetype QN
		Iterator it = probeMatches.getProbeMatches().iterator();
		while (it.hasNext()) {
			ProbeMatch probeMatch = (ProbeMatch) it.next();
			changeTypesTo2006(probeMatch.getTypes());
		}
	}

	private void changeOutgoingMessage(ResolveMessage resolve) {
		// Change wsa:Action
		resolve.getHeader().setAction(new AttributedURI(WSDConstants2006.WSD_ACTION_RESOLVE));
		// Change wsa:To
		resolve.getHeader().setTo(new AttributedURI(WSDConstants2006.WSD_TO));

	}

	private void changeOutgoingMessage(ResolveMatchesMessage resolveMatches) {
		// Change wsa:Action
		resolveMatches.getHeader().setAction(new AttributedURI(WSDConstants2006.WSD_ACTION_RESOLVEMATCHES));
		// Change wsa:To
		if (resolveMatches.getHeader().getTo() != null) {
			resolveMatches.getHeader().setTo(new AttributedURI(WSAConstants2006.WSA_ANONYMOUS));
		}
		// Change DPWS Devicetype QN
		changeTypesTo2006(resolveMatches.getResolveMatch().getTypes());

	}

	private void changeOutgoingMessage(GetMessage get) {
		// Set wsa:ReplyTo
		get.getHeader().setReplyTo(new EndpointReference(WSAConstants2006.WSA_ANONYMOUS));
		get.getHeader().setMessageId(new AttributedURI(IDGenerator.getUUIDasURI()));
	}

	private void changeOutgoingMessage(GetResponseMessage getResponse) {
		// Change wsa:To
		if (getResponse.getHeader().getTo() != null) {
			getResponse.getHeader().setTo(new AttributedURI(WSAConstants2006.WSA_ANONYMOUS));
		}

		getResponse.getHeader().setMessageId(new AttributedURI(IDGenerator.getUUIDasURI()));

		changeTypesTo2006(getResponse.getHost().getTypes());
		ServiceId id = new ServiceId(new URI("http://dpws.materna.de/"));
		getResponse.getHost().addUnknownElement(ServiceId.QNAME, id);

	}

	public GetMessage changeOutgoingMessage(GetMetadataMessage getMetadata) {
		GetMessage get = new GetMessage();
		copyOutgoingHeader(get.getHeader(), getMetadata);
		get.setVersion(getMetadata.getVersion());

		// Change wsa:Action
		get.getHeader().setAction(new AttributedURI(WXFConstants.WXF_ACTION_GET));
		// Set wsa:ReplyTo
		get.getHeader().setReplyTo(new EndpointReference(WSAConstants2006.WSA_ANONYMOUS));
		return get;
	}
	private void changeOutgoingMessage(GetMetadataResponseMessage getMetadataResponse) {
		// Change wsa:Action
		getMetadataResponse.getHeader().setAction(new AttributedURI(WXFConstants.WXF_ACTION_GETRESPONSE));
	}

	private void changeOutgoingMessage(SubscribeMessage subscribe) {
		// Change wse:filter-->dialect
		subscribe.getFilter().setDialect(DPWSConstants2006.DPWS_URI_FILTER_EVENTING_ACTION);
	}

	

	/**
	 * This method creates a copy of the given message an returns the copy.
	 * 
	 * @param message ,the given message
	 * @return ,the copy of the given message
	 */
	@Override
	public Message copyOutgoingMessage(Message message) {
		if (message instanceof HelloMessage) {
			return CopyOutgoingMessage((HelloMessage) message);
		} else if (message instanceof ByeMessage) {
			return CopyOutgoingMessage((ByeMessage) message);
		} else if (message instanceof ProbeMessage) {
			return CopyOutgoingMessage((ProbeMessage) message);
		} else if (message instanceof ResolveMessage) {
			return CopyOutgoingMessage((ResolveMessage) message);
		} else if (message instanceof GetMessage) {
			return CopyOutgoingMessage((GetMessage) message);
		}
		return message;
	}

	private Message CopyOutgoingMessage(HelloMessage original) {
		// generate new Hello Message within the DiscoveryData of the original
		HelloMessage hello = new HelloMessage(original.getDiscoveryData());

		// copying Header
		copyOutgoingHeader(hello.getHeader(), original);

		// copying inherited attributs
		copyOutgoingInheritAttributs(hello, original);

		return hello;
	}

	private Message CopyOutgoingMessage(ByeMessage original) {
		// generate new Hello Message within the DiscoveryData of the original
		ByeMessage bye = new ByeMessage(original.getDiscoveryData());

		// copying Header
		copyOutgoingHeader(bye.getHeader(), original);

		// copying inherited attributs
		copyOutgoingInheritAttributs(bye, original);

		return bye;
	}

	private Message CopyOutgoingMessage(ProbeMessage original) {
		// generate new Probe Message
		ProbeMessage probe = new ProbeMessage();

		// copying Header
		copyOutgoingHeader(probe.getHeader(), original);

		// copy Probespecific attributs
		probe.setTypes(original.getTypes());
		probe.setScopes(original.getScopes());
		probe.setDirected(original.isDirected());

		// copying inherited attributs
		copyOutgoingInheritAttributs(probe, original);

		return probe;
	}

	private Message CopyOutgoingMessage(ResolveMessage original) {
		// generate new Resolve Message
		ResolveMessage resolve = new ResolveMessage();

		// copying Header
		copyOutgoingHeader(resolve.getHeader(), original);

		// copy Resolvespecific attributs
		resolve.setEndpointReference(original.getEndpointReference());

		// copying inherited attributs
		copyOutgoingInheritAttributs(resolve, original);

		return resolve;
	}

	private Message CopyOutgoingMessage(GetMessage original) {
		// generate new Resolve Message
		GetMessage get = new GetMessage();

		// copying Header
		copyOutgoingHeader(get.getHeader(), original);
		// necessary for DPWS 2006
		get.getHeader().setReplyTo(new EndpointReference(WSAConstants2006.WSA_ANONYMOUS));

		// copying inherited attributs
		copyOutgoingInheritAttributs(get, original);

		return get;
	}

	/**
	 * This method copies all header attributs to the new header.
	 * 
	 * @param header
	 * @param original
	 */
	private void copyOutgoingHeader(MessageHeader header, Message original) {
		// copying Headerattributs
		header.setAction(new AttributedURI(original.getAction().toString()));
		header.setMessageId(original.getMessageId());

		AttributedURI tmp = original.getRelatesTo();
		if (tmp != null) {
			header.setRelatesTo(new AttributedURI(tmp.toString()));
		}
		EndpointReference epr = original.getReplyTo();
		if (epr != null) {
			header.setReplyTo(new EndpointReference(epr.getAddress()));
		}
		tmp = original.getTo();
		if (tmp != null) {
			header.setTo(new AttributedURI(original.getTo().toString()));
		}
		header.setAppSequence(original.getAppSequence());
		header.setReferenceParameters(original.getHeader().getReferenceParameters());
		header.setSignature(original.getHeader().getSignature());
		header.setValidated(original.getHeader().isValidated());
	}

	/**
	 * This method copies the inherit attributs of every Message (attributs
	 * extends Message.java)
	 * 
	 * @param copy
	 * @param original
	 */
	private void copyOutgoingInheritAttributs(Message copy, Message original) {
		// Change inherited Attributs (from class MESSAGE)
		copy.setInbound(original.isInbound());
		copy.setTargetAddress(original.getTargetAddress());
		copy.setSecure(original.isSecure());
		copy.setCertificate(original.getCertificate());
		copy.setPrivateKey(original.getPrivateKey());
	}

	public static void changeIncomingProbe(ProbeMessage probeMessage) {
		changeTypesTo2009(probeMessage.getTypes());
	}

	/**
	 * Change the QName:DeviceType of an incoming DPWS 2006 Message to DPWS 2009
	 * 
	 * @param types
	 */
	private static void changeTypesTo2009(QNameSet types) {
		// DPWS 2006 Device Type is included
		if (types.contains(DPWSConstants2006.DPWS_QN_DEVICETYPE) && !types.contains(DPWSConstants.DPWS_QN_DEVICETYPE)) {
			// Must be changed to DPWS 2009 Device Type
			//types.remove(DPWSConstants2006.DPWS_QN_DEVICETYPE);
			types.add(DPWSConstants.DPWS_QN_DEVICETYPE);
		}
	}

	/**
	 * Change the QName:DeviceType of an outgoing DPWS 2009 Message to DPWS 2006
	 * 
	 * @param types
	 */
	private void changeTypesTo2006(QNameSet types) {
		// DPWS 2009 Device Type is included
		if (types.contains(DPWSConstants.DPWS_QN_DEVICETYPE) && !types.contains(DPWSConstants2006.DPWS_QN_DEVICETYPE)) {
			// Must be changed to DPWS 2006 Device Type
			//types.remove(DPWSConstants.DPWS_QN_DEVICETYPE);
			types.add(DPWSConstants2006.DPWS_QN_DEVICETYPE);
		}
	}
}

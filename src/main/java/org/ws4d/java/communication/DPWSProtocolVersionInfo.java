/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication;

import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.DPWSConstants2006;

/**
 *
 */
public class DPWSProtocolVersionInfo implements ProtocolVersionInfo {

	/**
	 * The object for the OASIS DPWS 1.1 Specification
	 */
	public static final DPWSProtocolVersionInfo	DPWS1_1		= new DPWSProtocolVersionInfo(DPWSConstants.DPWS_VERSION2009);

	/**
	 * The object for the DPWS February 2006 Specification
	 */
	public static final DPWSProtocolVersionInfo	DPWS2006	= new DPWSProtocolVersionInfo(DPWSConstants2006.DPWS_VERSION2006);

	private final int							dpwsVersion;

	/**
	 * @param dpwsVersion
	 */
	private DPWSProtocolVersionInfo(int dpwsVersion) {
		super();
		this.dpwsVersion = dpwsVersion;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.ProtocolVersionInfo#getDisplayName()
	 */
	@Override
	public String getDisplayName() {
		switch (dpwsVersion) {
			case (DPWSConstants.DPWS_VERSION2009): {
				return DPWSConstants.DPWS_2009_NAME;
			}
			case (DPWSConstants2006.DPWS_VERSION2006): {
				return DPWSConstants2006.DPWS_2006_NAME;
			}
			default: {
				return "Unknown DPWS Version";
			}
		}
	}

	/**
	 * @return the dpwsVersion
	 */
	public int getDpwsVersion() {
		return dpwsVersion;
	}

	public static DPWSProtocolVersionInfo getDPWSProtocolVersionInfo(int dpwsVersion) {
		switch (dpwsVersion) {
			case DPWSConstants.DPWS_VERSION2009:
				return DPWS1_1;
			case DPWSConstants2006.DPWS_VERSION2006:
				return DPWS2006;
			default:
				return null;
		}
	}

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.NetworkDetection;
import org.ws4d.java.constants.HTTPConstants;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.IDGenerator;

/**
 * HTTP binding to allows access to DPWS devices and services.
 * <p>
 * This HTTP binding allows the creation of an HTTP address for a device or a
 * service.
 * </p>
 * 
 * <pre>
 * HTTPBinding	addr	= new HTTPBinding(&quot;192.168.0.1&quot;, 8080, &quot;/device&quot;);
 * </pre>
 * 
 * The HTTP binding above will create the address http://192.168.0.1:8080/device
 * and can be used for devices.
 */
public class HTTPBinding extends IPBinding {

	private final String originalPath;

	public HTTPBinding(IPAddress address, int port, String path) {
		super(address, port);
		if (path == null) {
			path = "/" + IDGenerator.getUUID();
		} else if (!path.startsWith("/")) {
			path = "/" + path;
		}
		
		originalPath=path;
		transportAddress = new URI(getURISchema() + "://" + getHostAddress() + ":" + port + path);
	}
	/**
	 * @deprecated <BR>
	 *             Use HTTPBinding(IPAddress ipAddress, int port, String path)
	 * @param address
	 * @param port
	 * @param path
	 */
	@Deprecated
	public HTTPBinding(String address, int port, String path) {
		this(PlatformSupport.getInstance().getToolkit().getNetworkDetection(NetworkDetection.IP_NETWORK).getIPAddress(address), port, path);
	}

	public HTTPBinding(URI address) {
		super(PlatformSupport.getInstance().getToolkit().getNetworkDetection(NetworkDetection.IP_NETWORK).getIPAddress(address.getHost()), address.getPort());
		this.transportAddress = address;
		originalPath=address.getPath();
	}

	public HTTPBinding(String address, int port) {
		super(new IPAddress(address), port);
		originalPath=new URI(address).getPath();
	}

	/**
	 * Returns the path of the HTTP address.
	 * 
	 * @return the path of the HTTP address.
	 */
	public String getPath() {
		return transportAddress.getPath();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + transportAddress.hashCode();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!super.equals(obj)) return false;
		if (getClass() != obj.getClass()) return false;
		final HTTPBinding other = (HTTPBinding) obj;
		if (!transportAddress.equals(other.transportAddress) || getType() != other.getType()) return false;
		return true;
	}

	@Override
	public int getType() {
		return HTTP_BINDING;
	}
	

	public String getURISchema() {
		return HTTPConstants.HTTP_SCHEMA;
	}
	@Override
	public void setDeviceEndpointReference(URI deviceEndpointRef) {
		if (deviceEndpointRef!=null)
		{
			transportAddress = new URI(getURISchema() + "://" + getHostAddress() + ":" + port + originalPath);
		}
	}
	
	
}

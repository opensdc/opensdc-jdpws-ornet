/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication;


/**
 * Instances of this interface encapsulate version information for a specific
 * communication protocol, such as DPWS. They are meant to be opaque for
 * everyone else but the {@link CommunicationManager communication manager}
 * instance dedicated to exactly this protocol.
 */
public interface ProtocolVersionInfo {

	/**
	 * Returns a short description of the version and protocol this instance
	 * refers to, e.g. <code>DPWS 1.1</code>.
	 * 
	 * @return a short description of this instance version and protocol
	 */
	public String getDisplayName();

}

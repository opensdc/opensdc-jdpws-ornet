/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication;

import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.HTTPConstants;
import org.ws4d.java.types.URI;

public class DiscoveryBindingIP extends IPBinding implements DiscoveryBinding{

	public static final int	IPV6				= 6;

	public static final int	IPV4				= 4;

	private String			iface;

	protected int			ipVersion			= -1;

	/**
	 * Discovery Binding is only for Input-Discovery. For Output see method
	 * addOutputDiscoveryDomain(ProtocolDomain domain).
	 * 
	 * @param mcAddress
	 * @param mcPort
	 * @param iface
	 */
	// public DiscoveryBinding(String mcAddress, int mcPort, String iface) {
	// super(mcAddress, mcPort);
	// this.iface = iface;
	// }
	//
	// public DiscoveryBinding(String mcAddress, String iface) {
	// super(mcAddress, DPWSConstants.DPWS_MCAST_PORT);
	// this.iface = iface;
	// }

	public DiscoveryBindingIP(int ipVersion, String iface) {
		super((ipVersion == 6 ? DPWSConstants.DPWS_MCAST_IPv6 : DPWSConstants.DPWS_MCAST_IPv4), DPWSConstants.DPWS_MCAST_PORT);
		this.iface = iface;
		this.ipVersion = ipVersion;
		// this.address = new URI(HTTPConstants.HTTP_SCHEMA + "://" +
		// getHostAddress() + ":" + port);
	}

	// public DiscoveryBinding(int ipVersion, String iface, String path) {
	// super((ipVersion==6?DPWSConstants.DPWS_MCAST_IPv6:DPWSConstants.DPWS_MCAST_IPv4),
	// DPWSConstants.DPWS_MCAST_PORT);
	// this.iface = iface;
	// if (path == null) {
	// path = "/" + IDGenerator.getUUID();
	// } else if (!path.startsWith("/")) {
	// path = "/" + path;
	// }
	// this.address = new URI(HTTPConstants.HTTP_SCHEMA + "://" +
	// getHostAddress() + ":" + port + path);
	// }

	// /**
	// * Returns the path of the HTTP address.
	// *
	// * @return the path of the HTTP address.
	// */
	// public String getPath() {
	// return address.getPath();
	// }

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.CommunicationBinding#getTransportAddress()
	 */
	@Override
	public URI getTransportAddress() {
		if (transportAddress == null) {
			transportAddress = new URI(HTTPConstants.HTTP_SCHEMA + "://" + getHostAddress() + ":" + port);
		}
		return transportAddress;
	}

	public String getIface() {
		return iface;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = prime + ((iface == null) ? 0 : iface.hashCode());
		result = prime * result + ipVersion;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DiscoveryBindingIP other = (DiscoveryBindingIP) obj;
		if (iface == null) {
			if (other.iface != null) {
				return false;
			}
		} else if (!iface.equals(other.iface)) {
			return false;
		}
		if (ipVersion != other.ipVersion) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.IPBinding#toString()
	 */
	@Override
	public String toString() {
		return (ipVersion == IPV6 ? "IPv6" : "IPv4") + " - " + iface+" "+super.toString();
	}

	@Override
	public int getType() {
		return DISCOVERY_BINDING;
	}
}

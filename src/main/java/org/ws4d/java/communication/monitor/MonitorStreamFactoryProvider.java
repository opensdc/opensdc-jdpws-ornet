/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.monitor;

public class MonitorStreamFactoryProvider 
{
	private final static MonitorStreamFactoryProvider instance=new MonitorStreamFactoryProvider();
	private MonitorStreamFactory monitorFactory;
	
	public static MonitorStreamFactoryProvider getInstance() {
		return instance;
	}
	private MonitorStreamFactoryProvider()
	{
		//void
	}
	
	/**
	 * Set the factory for stream monitoring.
	 * <p>
	 * This enables the monitoring of streams for debug purposes. A
	 * <code>MonitorStreamFactory</code> wraps streams to redistribute data. A
	 * communication manager can use the factory to redistribute data to the
	 * streams created by the factory.
	 * </p>
	 * 
	 * @param factory the factory which wraps streams and redistribute data.
	 */
	public void setMonitorStreamFactory(MonitorStreamFactory factory) {
		monitorFactory = factory;
	}

	/**
	 * Returns the <code>MonitorStreamFactory</code> which allows to wrap
	 * streams and redistribute data.
	 * 
	 * @return the factory to wrap streams and redistribute data.
	 * @see #setMonitorStreamFactory(MonitorStreamFactory)
	 */
	public  MonitorStreamFactory getMonitorStreamFactory() {
		return monitorFactory;
	}
}

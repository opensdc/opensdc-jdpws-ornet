/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.monitor;

import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.protocol.soap.generator.MessageReceiver;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.IMessageEndpoint;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.discovery.ByeMessage;
import org.ws4d.java.message.discovery.HelloMessage;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
import org.ws4d.java.message.discovery.ProbeMessage;
import org.ws4d.java.message.discovery.ResolveMatchesMessage;
import org.ws4d.java.message.discovery.ResolveMessage;
import org.ws4d.java.message.eventing.GetStatusMessage;
import org.ws4d.java.message.eventing.GetStatusResponseMessage;
import org.ws4d.java.message.eventing.RenewMessage;
import org.ws4d.java.message.eventing.RenewResponseMessage;
import org.ws4d.java.message.eventing.SubscribeMessage;
import org.ws4d.java.message.eventing.SubscribeResponseMessage;
import org.ws4d.java.message.eventing.SubscriptionEndMessage;
import org.ws4d.java.message.eventing.UnsubscribeMessage;
import org.ws4d.java.message.eventing.UnsubscribeResponseMessage;
import org.ws4d.java.message.metadata.GetMessage;
import org.ws4d.java.message.metadata.GetMetadataMessage;
import org.ws4d.java.message.metadata.GetMetadataResponseMessage;
import org.ws4d.java.message.metadata.GetResponseMessage;

public class MonitoredMessageReceiver implements MessageReceiver {

	private MessageReceiver			receiver	= null;

	private MonitoringContext		context		= null;

	private MonitorStreamFactory	monFac		= null;

	public MonitoredMessageReceiver(MessageReceiver receiver, MonitoringContext context) {
		this.receiver = receiver;
		this.context = context;
		this.monFac = MonitorStreamFactoryProvider.getInstance().getMonitorStreamFactory();
	}

	@Override
	public void receive(HelloMessage hello, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, hello);
		}
		receiver.receive(hello, protocolData);
	}

	@Override
	public void receive(ByeMessage bye, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, bye);
		}
		receiver.receive(bye, protocolData);
	}

	@Override
	public void receive(ProbeMessage probe, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, probe);
		}
		receiver.receive(probe, protocolData);
	}

	@Override
	public void receive(ProbeMatchesMessage probeMatches, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, probeMatches);
		}
		receiver.receive(probeMatches, protocolData);
	}

	@Override
	public void receive(ResolveMessage resolve, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, resolve);
		}
		receiver.receive(resolve, protocolData);
	}

	@Override
	public void receive(ResolveMatchesMessage resolveMatches, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, resolveMatches);
		}
		receiver.receive(resolveMatches, protocolData);
	}

	@Override
	public void receive(GetMessage get, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, get);
		}
		receiver.receive(get, protocolData);
	}

	@Override
	public void receive(GetResponseMessage getResponse, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, getResponse);
		}
		receiver.receive(getResponse, protocolData);
	}

	@Override
	public void receive(GetMetadataMessage getMetadata, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, getMetadata);
		}
		receiver.receive(getMetadata, protocolData);
	}

	@Override
	public void receive(GetMetadataResponseMessage getMetadataResponse, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, getMetadataResponse);
		}
		receiver.receive(getMetadataResponse, protocolData);
	}

	@Override
	public void receive(SubscribeMessage subscribe, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, subscribe);
		}
		receiver.receive(subscribe, protocolData);
	}

	@Override
	public void receive(SubscribeResponseMessage subscribeResponse, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, subscribeResponse);
		}
		receiver.receive(subscribeResponse, protocolData);
	}

	@Override
	public void receive(GetStatusMessage getStatus, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, getStatus);
		}
		receiver.receive(getStatus, protocolData);
	}

	@Override
	public void receive(GetStatusResponseMessage getStatusResponse, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, getStatusResponse);
		}
		receiver.receive(getStatusResponse, protocolData);
	}

	@Override
	public void receive(RenewMessage renew, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, renew);
		}
		receiver.receive(renew, protocolData);
	}

	@Override
	public void receive(RenewResponseMessage renewResponse, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, renewResponse);
		}
		receiver.receive(renewResponse, protocolData);
	}

	@Override
	public void receive(UnsubscribeMessage unsubscribe, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, unsubscribe);
		}
		receiver.receive(unsubscribe, protocolData);
	}

	@Override
	public void receive(UnsubscribeResponseMessage unsubscribeResponse, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, unsubscribeResponse);
		}
		receiver.receive(unsubscribeResponse, protocolData);
	}

	@Override
	public void receive(SubscriptionEndMessage subscriptionEnd, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, subscriptionEnd);
		}
		receiver.receive(subscriptionEnd, protocolData);
	}

	@Override
	public void receive(InvokeMessage invoke, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, invoke);
		}
		receiver.receive(invoke, protocolData);
	}

	@Override
	public void receive(FaultMessage fault, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.received(protocolData, context, fault);
		}
		receiver.receive(fault, protocolData);
	}

	@Override
	public void receiveFailed(Exception e, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.receivedFault(protocolData, context, e);
		}
		receiver.receiveFailed(e, protocolData);

	}

	@Override
	public void sendFailed(Exception e, DPWSProtocolData protocolData) {
		if (monFac != null) {
			monFac.receivedFault(protocolData, context, e);
		}
		receiver.sendFailed(e, protocolData);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.protocol.soap.generator.MessageReceiver#
	 * getOperation(java.lang.String)
	 */
	@Override
	public IMessageEndpoint getOperation(String action) {
		return receiver.getOperation(action);
	}

}

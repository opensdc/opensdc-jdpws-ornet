/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.monitor;

import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.structures.HashMap;

/**
 * Factory which allows to create an <code>OutputStream</code> to catch incoming
 * and outgoing data which allows the creation of <code>Message</code> objects.
 */
public abstract class MonitorStreamFactory {

	public static HashMap	inMon	= new HashMap();

	public static HashMap	outMon	= new HashMap();

	public abstract StreamMonitor createInputMonitor(ProtocolData pd);

	public abstract StreamMonitor createOutputMonitor(ProtocolData pd);

	/**
	 * Creates a <code>StreamMonitor</code> for incoming messages.
	 * 
	 * @return the <code>StreamMonitor</code> for incoming messages.
	 */
	public final synchronized StreamMonitor getInputMonitor(ProtocolData pd) {
		StreamMonitor mon = (StreamMonitor) inMon.get(pd);
		if (mon == null) {
			mon = createInputMonitor(pd);
			inMon.put(pd, mon);
		}
		return mon;
	}

	/**
	 * Creates a <code>StreamMonitor</code> for outgoing messages.
	 * 
	 * @return the <code>StreamMonitor</code> for outgoing messages.
	 */
	public final synchronized StreamMonitor getOutputMonitor(ProtocolData pd) {
		StreamMonitor mon = (StreamMonitor) outMon.get(pd);
		if (mon == null) {
			mon = createOutputMonitor(pd);
			outMon.put(pd, mon);
		}
		return mon;
	}

	public final synchronized MonitoringContext getNewMonitoringContextIn(ProtocolData pd) {
		StreamMonitor mon = (StreamMonitor) inMon.get(pd);
		MonitoringContext context = new MonitoringContext(pd);
		if (mon != null) {
			mon.setMonitoringContext(context);
		}
		return context;
	}

	public final synchronized MonitoringContext getNewMonitoringContextOut(ProtocolData pd) {
		StreamMonitor mon = (StreamMonitor) outMon.get(pd);
		MonitoringContext context = new MonitoringContext(pd);
		if (mon != null) {
			mon.setMonitoringContext(context);
		}
		return context;
	}

	public final synchronized MonitoringContext getMonitoringContextIn(ProtocolData pd) {
		StreamMonitor mon = (StreamMonitor) inMon.get(pd);
		if (mon != null) {
			return mon.getMonitoringContext();
		}
		return null;
	}

	public final synchronized MonitoringContext getMonitoringContextOut(ProtocolData pd) {
		StreamMonitor mon = (StreamMonitor) outMon.get(pd);
		if (mon != null) {
			return mon.getMonitoringContext();
		}
		return null;
	}

	/**
	 * Method which allows the link between the current <code>Thread</code> and
	 * a <code>StreamMonitor</code> for an <code>OutputStream</code>.
	 * 
	 * @param pd the protocol data which will be used to identify the monitor.
	 * @param mon the stream monitor
	 */
	public final synchronized void linkIn(ProtocolData pd, StreamMonitor mon) {
		inMon.put(pd, mon);
	}

	/**
	 * Method which allows the link between the current <code>Thread</code> and
	 * a <code>StreamMonitor</code> for an <code>InputStream</code>.
	 * 
	 * @param pd the protocol data which will be used to identify the monitor.
	 * @param mon the stream monitor
	 */
	public final synchronized void linkOut(ProtocolData pd, StreamMonitor mon) {
		outMon.put(pd, mon);
	}

	/**
	 * Allows the assignment of a incoming <code>Message</code> to a previously
	 * given <code>OutputStream</code>.
	 * 
	 * @param pd the protocol data which will be used to identify the monitor.
	 * @param message the message.
	 */
	public final synchronized void received(ProtocolData pd, MonitoringContext context, Message message) {
		StreamMonitor mon = (StreamMonitor) inMon.get(pd);
		if (mon != null) {
			mon.assign(pd, context, message);
		}
	}

	/**
	 * Allows the assignment of a incoming discarded <code>Message</code> to a
	 * previously given <code>OutputStream</code>.
	 * 
	 * @param pd the protocol data which will be used to identify the monitor.
	 * @param header the message header.
	 */
	public final synchronized void discard(ProtocolData pd, MonitoringContext context, MessageHeader header, int discardReason) {
		StreamMonitor mon = (StreamMonitor) inMon.get(pd);
		if (mon != null) {
			mon.discard(pd, context, header, discardReason);
		}
	}

	/**
	 * Allows the assignment of a outgoing <code>Message</code> to a previously
	 * given <code>OutputStream</code>.
	 * 
	 * @param pd the protocol data which will be used to identify the monitor.
	 * @param message the message.
	 */
	public final synchronized void send(ProtocolData pd, MonitoringContext context, Message message) {
		StreamMonitor mon = (StreamMonitor) outMon.get(pd);
		if (mon != null) {
			mon.assign(pd, context, message);
		}
	}

	/**
	 * Allows to inform the incoming monitor about a fault.
	 * 
	 * @param pd the protocol data which will be used to identify the monitor.
	 */
	public final synchronized void receivedFault(ProtocolData pd, MonitoringContext context, Exception e) {
		StreamMonitor mon = (StreamMonitor) inMon.get(pd);
		if (mon != null) {
			mon.fault(pd, context, e);
		}
	}

	/**
	 * Allows to inform the outgoing monitor about a fault.
	 * 
	 * @param pd the protocol data which will be used to identify the monitor.
	 */
	public final synchronized void sendFault(ProtocolData pd, MonitoringContext context, Exception e) {
		StreamMonitor mon = (StreamMonitor) outMon.get(pd);
		if (mon != null) {
			mon.fault(pd, context, e);
		}
	}

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.monitor;

import java.io.IOException;
import java.io.OutputStream;

import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.util.Log;

/**
 * This is a stream wrapper which allows to pass-through data to a given
 * <code>OutputStream</code> for monitoring.
 */
public class MonitoredOutputStream extends OutputStream {


	private OutputStream	out		= null;

	private StreamMonitor	stMon	= null;

	@SuppressWarnings("unused")
	private int				len		= 0;

	private boolean			monWarn	= true;

	public MonitoredOutputStream(OutputStream out, ProtocolData pd) {
		MonitorStreamFactory monFac = MonitorStreamFactoryProvider.getInstance().getMonitorStreamFactory();
		if (monFac != null) {
			stMon = monFac.getOutputMonitor(pd);
			monFac.linkOut(pd, stMon);
		}
		this.out = out;
	}

	@Override
	public void write(int arg0) throws IOException {
		len++;
		out.write(arg0);
		if (stMon != null && stMon.getOutputStream() != null) {
			OutputStream os = stMon.getOutputStream();
			if (os != null) {
				try {
					os.write(arg0);
				} catch (IOException e) {
					if (monWarn) {
						Log.error("Monitoring failed. Cannot write. " + e.getMessage());
						monWarn = false;
					}
				}
			}
		}
	}

	@Override
	public void write(byte[] b) throws IOException {
		out.write(b);
		len += b.length;
		if (stMon != null && stMon.getOutputStream() != null) {
			OutputStream os = stMon.getOutputStream();
			if (os != null) {
				try {
					os.write(b);
				} catch (IOException e) {
					if (monWarn) {
						Log.error("Monitoring failed. Cannot write. " + e.getMessage());
						monWarn = false;
					}
				}
			}
		}
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		out.write(b, off, len);
		this.len += len;
		if (stMon != null && stMon.getOutputStream() != null) {
			OutputStream os = stMon.getOutputStream();
			if (os != null) {
				try {
					os.write(b, off, len);
				} catch (IOException e) {
					if (monWarn) {
						Log.error("Monitoring failed. Cannot write. " + e.getMessage());
						monWarn = false;
					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.io.OutputStream#flush()
	 */
	@Override
	public void flush() throws IOException {
		out.flush();
		if (stMon != null && stMon.getOutputStream() != null) {
			OutputStream os = stMon.getOutputStream();
			if (os != null) {
				try {
					os.flush();
				} catch (IOException e) {
					if (monWarn) {
						Log.error("Monitoring failed. Cannot flush. " + e.getMessage());
						monWarn = false;
					}
				}
			}
		}
	}

	@Override
	public void close() throws IOException {
		flush();
		out.close();
		if (stMon != null && stMon.getOutputStream() != null) {
			OutputStream os = stMon.getOutputStream();
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					if (monWarn) {
						Log.error("Monitoring failed. Cannot close. " + e.getMessage());
						monWarn = false;
					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((out == null) ? 0 : out.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		MonitoredOutputStream other = (MonitoredOutputStream) obj;
		if (out == null) {
			if (other.out != null) return false;
		} else if (!out.equals(other.out)) return false;
		return true;
	}

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.monitor;

import java.io.IOException;
import java.io.OutputStream;

import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;

/**
 * Simple extension of the <code>MonitorStreamFactory</code> which allows to
 * write any incoming or outgoing <code>Message</code> to the default error
 * output stream.
 */
public class DefaultMonitoredStreamFactory extends MonitorStreamFactory {

	private static final OutputStream	FORWARDER	= new OutputStream() {

														@Override
														public void write(int b) throws IOException {
															System.err.write(b);
														}

													};

	@Override
	public StreamMonitor createInputMonitor(ProtocolData pd) {
		return new DefaultStreamMonitor();
	}

	@Override
	public StreamMonitor createOutputMonitor(ProtocolData pd) {
		return new DefaultStreamMonitor();
	}

	private class DefaultStreamMonitor implements StreamMonitor {

		@Override
		public OutputStream getOutputStream() {
			/*
			 * !!! don't return System.out or System.err directly, as they would
			 * get closed by the monitors!!!
			 */
			return FORWARDER;
		}

		@Override
		public void assign(ProtocolData protocolData, MonitoringContext context, Message message) {
			// TODO Auto-generated method stub

		}

		@Override
		public void fault(ProtocolData protocolData, MonitoringContext context, Exception e) {
			// TODO Auto-generated method stub
		}

		@Override
		public void setMonitoringContext(MonitoringContext context) {
			// TODO Auto-generated method stub

		}

		@Override
		public MonitoringContext getMonitoringContext() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void discard(ProtocolData protocolData, MonitoringContext context, MessageHeader header, int reason) {
			// TODO Auto-generated method stub

		}

	}

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.connection.tcp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;

import org.ws4d.java.communication.connection.ip.IPAddress;

/**
 * This class encapsulates an SE listening socket.
 */
public class SEServerSocket implements ServerSocket {
	
	private static final boolean performancePrefsEnabled	=	Boolean.parseBoolean(System.getProperty("MDPWS.Socket.PerformancePrefsEnabled", "true"));
	private static int			SOCKET_BACKLOG				= 	Integer.parseInt(System.getProperty("MDPWS.Socket.Backlog", "0"));

	private IPAddress			ipAddress					= 	null;

	private int					port						= 	-1;
	
	

	java.net.ServerSocket		server						= null;

	/**
	 * The number of attempts to find a random port before giving up
	 */
	protected static final int	PORT_RETRIES	= 3;

	public SEServerSocket(IPAddress ipAddress, int port) throws IOException {
		InetAddress adr = InetAddress.getByName(ipAddress.getAddress());
		try {
			server = new java.net.ServerSocket();
			if (performancePrefsEnabled) server.setPerformancePreferences(0, 1, 2);
			
			InetSocketAddress inetSocketAddress = new InetSocketAddress(adr, port);
			server.bind(inetSocketAddress, SOCKET_BACKLOG);
		} catch (Exception e) {
			throw new IOException(e.getMessage() + "For " + ipAddress + " at port " + port);
		}
		this.ipAddress = ipAddress;
		this.port = port;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#accept()
	 */
	@Override
	public Socket accept() throws IOException {
		Socket s= new SESocket(server.accept(), getIPAddress());
		return s;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#close()
	 */
	@Override
	public void close() throws IOException {
		server.close();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#getAddress()
	 */
	@Override
	public IPAddress getIPAddress() {
		return ipAddress;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#getPort()
	 */
	@Override
	public int getPort() {
		return port;
	}

}

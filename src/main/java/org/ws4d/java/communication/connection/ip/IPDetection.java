/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.connection.ip;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;

import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.LinkedList;
import org.ws4d.java.structures.List;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.StringUtil;

/**
 * IP address detection for SE.
 */
public class IPDetection extends NetworkDetection {

	public static final String				PROP_ADDRESS_GROUP_INET6			= "inet6";

	public static final String				PROP_ADDRESS_GROUP_INET4			= "inet4";

	public static final String				PROP_ADDRESS_GROUP_ALL				= "all";

	public static final String				PROP_ADDRESS_GROUP_LO				= "lo";

	public IPDetection() {
		super();
	}



	/*
	 * (non-Javadoc)
	 * @seeorg.ws4d.java.communication.connection.network.NetworkDetection#
	 * detectInterfaces()
	 */
	@Override
	void detectInterfaces() throws IOException {
		if (Log.isDebug()) {
			Log.debug("Start interface detection...");
		}
		if (networkinterfaces != null) {
			networkinterfaces.clear();
		} else {
			networkinterfaces = new LinkedList();
		}
		Enumeration<NetworkInterface> nis = NetworkInterface.getNetworkInterfaces();
		while (nis.hasMoreElements()) {
			NetworkInterface niSE =  nis.nextElement();

			org.ws4d.java.communication.connection.ip.NetworkInterface ni=getNetworkInterfaceWrapper(niSE);
			Enumeration<InetAddress> addrs = niSE.getInetAddresses();
			while (addrs.hasMoreElements()) {
				InetAddress addr = addrs.nextElement();
				ni.addAddress(new IPAddress(addr.getHostAddress(), addr.isLoopbackAddress(), (addr instanceof Inet6Address), addr.isLinkLocalAddress()));
			}
			if (ni.getAddresses() != null) {
				networkinterfaces.add(ni);
				if (Log.isDebug()) {
					Log.debug("Interface found: " + ni);
				}
			}
		}
		if (Log.isDebug()) {
			Log.debug("Interface detection done.");
		}
	}


	protected org.ws4d.java.communication.connection.ip.NetworkInterface getNetworkInterfaceWrapper(NetworkInterface niSE) throws IOException
	{
		NetworkInterfaceProps networkProps=getNetworkInterfaceProps(niSE);
		org.ws4d.java.communication.connection.ip.NetworkInterface ni = new org.ws4d.java.communication.connection.ip.NetworkInterface(niSE.getName(), networkProps.supportsMulticast, networkProps.isUP,networkProps.isLoopback);
		return ni;
	}

	protected NetworkInterfaceProps getNetworkInterfaceProps(NetworkInterface niSE ) throws IOException
	{
		boolean supportsMulticast=true;
		boolean isUP=true;
		boolean isLoopback=false;

		//		boolean useWorkaround=Boolean.parseBoolean(System.getProperty("MDPWS.NetworkInterfaceFallback", "false")) 
		//			|| (System.getProperty("java.version").length()>3 && Float.parseFloat(System.getProperty("java.version").substring(0,3))<1.6) ; //Bugfix: Index-out-of-Bounds exception
		//		Log.info("Use Workaround for NI: "+useWorkaround);

		Enumeration<InetAddress> addrEnum= niSE.getInetAddresses();
		while(addrEnum.hasMoreElements()){
			InetAddress addr=addrEnum.nextElement();
			isLoopback=isLoopback || addr.isLoopbackAddress();
		}
		//		}else{
		//			try {
		//				supportsMulticast=niSE.supportsMulticast();
		//				isUP=niSE.isUp();
		//				isLoopback=niSE.isLoopback();
		//			} catch (SocketException e) {
		//				throw e;
		//			}
		//
		//		}
		return new NetworkInterfaceProps(supportsMulticast,isUP,isLoopback);
	}


	/*
	 * (non-Javadoc)
	 * @seeorg.ws4d.java.communication.connection.network.NetworkDetection#
	 * detectAddresses()
	 */
	@Override
	public void detectAddresses() throws IOException {
		if (networkinterfaces == null) {
			detectInterfaces();
		}
		Iterator niIter = networkinterfaces.iterator();
		if (addresses != null) {
			addresses.clear();
		} else {
			addresses = new HashMap();
		}
		while (niIter.hasNext()) {
			org.ws4d.java.communication.connection.ip.NetworkInterface ni = (org.ws4d.java.communication.connection.ip.NetworkInterface) niIter.next();
			Iterator addrIter = ni.getAddresses();
			while (addrIter.hasNext()) {
				IPAddress addr = (IPAddress) addrIter.next();
				addresses.put(addr.getAddress(), addr);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.ws4d.java.communication.connection.network.NetworkDetection#
	 * getCanonicalAddress()
	 */
	@Override
	public String getCanonicalAddress(String address) {
		try {
			return InetAddress.getByName(address).getHostAddress();
		} catch (UnknownHostException e) {
			return null;
		}
	}


	@Override
	public List getGroupByName(String groupName) 
	{
		ArrayList ret = new ArrayList();
		Iterator iti = null;
		if (groupName.indexOf('@') == -1) {
			if (StringUtil.equalsIgnoreCase(PROP_ADDRESS_GROUP_ALL, groupName)) {
				// all
				iti = getAddresses();
			} else if (StringUtil.equalsIgnoreCase(PROP_ADDRESS_GROUP_LO, groupName)) {
				// lo
				iti = getAddresses(null, groupName);
			} else if (StringUtil.equalsIgnoreCase(PROP_ADDRESS_GROUP_INET4, groupName) || StringUtil.equalsIgnoreCase(PROP_ADDRESS_GROUP_INET6, groupName)) {
				// inet4, inet6
				iti = getAddresses(groupName, null);
			} else {
				// f.e. eth0, lo
				iti = getAddresses(null, groupName);
			}
		} else {
			String protocol = groupName.substring(0, groupName.indexOf('@'));
			String ifaceName = groupName.substring(groupName.indexOf('@') + 1);
			if (StringUtil.equalsIgnoreCase(PROP_ADDRESS_GROUP_INET4, protocol) || StringUtil.equalsIgnoreCase(PROP_ADDRESS_GROUP_ALL, protocol) || StringUtil.equalsIgnoreCase(PROP_ADDRESS_GROUP_LO, protocol) || StringUtil.equalsIgnoreCase(PROP_ADDRESS_GROUP_INET6, protocol)) {
				iti = getAddresses(protocol, ifaceName);
			} else {
				if (Log.isError()) Log.error("Can not parse address-group for protocol: " + protocol + ". Set default: all addresses.");
			}
		}
		if (iti != null && iti.hasNext()) {
			while (iti.hasNext()) {
				IPAddress ipAddr = (IPAddress) iti.next();
				ret.add(ipAddr);
			}
		} else {
			Log.warn("Can not parse address-group '" + groupName + "'. Set default: all addresses.");
		}
		return ret;
	}





	@Override
	protected String getAdapterName(
			org.ws4d.java.communication.connection.ip.NetworkInterface ni) {
		String adapterName=null;
		boolean foundAdapter = false;

		String osName=System.getProperty("os.name");

		if (osName!=null && osName.toLowerCase().startsWith("windows"))
		{
			try{
				// Run the Windows 'ipconfig' command and get its stdout
				ProcessBuilder cmdBuilder = new ProcessBuilder("ipconfig");
				Process process = cmdBuilder.start();
				BufferedReader stdout = new BufferedReader(new InputStreamReader(process.getInputStream()));

				// Find the section for the specified adapter
				String line=null;

				String tempApdapterName=null;
				while ((line = stdout.readLine()) != null) {
					line = line.trim();
					if (line.matches("^[Ee]thernet.[Aa]dapter\\s(.*):")){
						String[] splittedString= line.split("^[Ee]thernet.[Aa]dapter\\s");
						if (splittedString!=null && splittedString.length==2){
							tempApdapterName=splittedString[1].replace(":", "").trim();
						}
					} else if (line.matches("IP((v[46])?)([-\\s]?)[Aa]d(d?)ress(e?)(.*):(.*)")){
						String[] splittedString= line.split("IP((v[46])?)([-\\s]?)[Aa]d(d?)ress(e?)(.*):");
						if (splittedString!=null && splittedString.length==2){
							Iterator addrIter = ni.getAddresses();
							while (addrIter.hasNext()) {
								IPAddress addr = (IPAddress) addrIter.next();
								if (splittedString[1].contains(addr.getAddress())){
									foundAdapter = true;
								}
								break;
							}
						}
						if (foundAdapter)
							break;
					}
				}
				if (!foundAdapter) {
					process.waitFor();
				}else{
					adapterName=tempApdapterName;
				}
			}
			catch(Exception e){
				if (Log.isDebug()){
					Log.debug("GetAdapterName failed for "+ni+":"+e.getMessage());
					Log.debug(e);
				}
			}
		}else{
			if (Log.isInfo())
				Log.info("getAdapterName not implemented for non-windows os.");
		}


		return adapterName;
	}





	protected class NetworkInterfaceProps{

		final boolean supportsMulticast;
		final boolean isUP;
		final boolean isLoopback;
		/**
		 * @param supportsMulticast
		 * @param isUP
		 * @param isLoopback
		 */
		public NetworkInterfaceProps(boolean supportsMulticast, boolean isUP,
				boolean isLoopback) 
		{
			this.supportsMulticast=supportsMulticast;
			this.isUP=isUP;
			this.isLoopback=isLoopback;

		}

	}


}

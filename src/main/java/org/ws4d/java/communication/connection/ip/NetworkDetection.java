/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.connection.ip;

import java.io.IOException;

import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.LinkedList;
import org.ws4d.java.structures.List;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.StringUtil;

/**
 * This abstract class defines some methods for the platform specific network
 * detection.
 */
public abstract class NetworkDetection {

	public static final String IP_NETWORK="NetworkDetection.IP"; 




	protected static List	 networkinterfaces	= null;

	protected static HashMap addresses			= null;

	/**
	 * Returns all network interfaces found on this platform. Starts an
	 * interface detection phase if necessary.
	 * 
	 * @return all network interfaces.
	 */
	public final synchronized Iterator getNetworkInterfaces() {
		if (networkinterfaces == null) {
			try {
				detectInterfaces();
			} catch (IOException e) {
				// TODO SSch refactor
				Log.info(e);
			}
		}
		return networkinterfaces.iterator();
	}



	/**
	 * Returns an iterator with all available addresses.
	 * 
	 * @return
	 */
	public final synchronized Iterator getAddresses() {
		if (addresses == null) {
			try {
				detectAddresses();
			} catch (IOException e) {
				// TODO SSch refactor
				Log.info(e);
			}
		}
		return addresses.entrySet().iterator();
	}


	/**
	 * Extracted by SSch
	 * 
	 * @param protocol
	 * @param ifaceName
	 * @param noLoopbacks
	 * @return
	 */
	public Iterator getAddresses(String protocol, String ifaceName, boolean noLoopbacks) {
		Iterator niIter = getNetworkInterfaces();
		LinkedList result = new LinkedList();

		while (niIter.hasNext()) {
			org.ws4d.java.communication.connection.ip.NetworkInterface ni = (org.ws4d.java.communication.connection.ip.NetworkInterface) niIter.next();

			if (noLoopbacks && ni.isLoopback()) continue;

			if (ifaceName == null || StringUtil.equalsIgnoreCase(ni.getName(), ifaceName) || StringUtil.equalsIgnoreCase(getAdapterName(ni), ifaceName)) {
				Iterator addrIter = ni.getAddresses();
				while (addrIter.hasNext()) {
					IPAddress addr = (IPAddress) addrIter.next();
					if (protocol == null || (!addr.isIPv6() && StringUtil.equalsIgnoreCase(protocol, "inet4")) || (addr.isIPv6() && StringUtil.equalsIgnoreCase(protocol, "inet6"))) {
						result.add(addr);
					}
				}
			}
		}
		return result.iterator();
	}

	protected String getAdapterName(NetworkInterface ni) {
		return null;
	}



	/**
	 * Returns an iterator with all available addresses. Filtered by the type of
	 * protocol or by the interface name.
	 * 
	 * @param protocol inet4, inet6 or null for wildcard.
	 * @param ifaceName for example eth0 or null for wildcard.
	 * @return
	 */
	public Iterator getAddresses(String protocol, String ifaceName) {
		return getAddresses(protocol, ifaceName, false);
	}

	public String getInterfaceName(String address)
	{
		Iterator niIter = getNetworkInterfaces();

		while (niIter.hasNext()) {
			org.ws4d.java.communication.connection.ip.NetworkInterface ni = (org.ws4d.java.communication.connection.ip.NetworkInterface) niIter.next();
			String iFace=ni.getName(); 
			Iterator addrIter = ni.getAddresses();
			while (addrIter.hasNext()) {
				IPAddress addr = (IPAddress) addrIter.next();
				if (address == null || (address.equals(addr.getAddress()))) 
				{
					return iFace;
				}
			}
		}
		return null;
	}
	/**
	 * * Detect a group of address strings by name. For example: value =
	 * "inet6@eth0" returns all ipv6 addresses from the interface "eth0".
	 * 
	 * @param groupName
	 * @return A String List with the group addresses
	 */
	public abstract List getGroupByName(String groupName);

	public synchronized IPAddress getIPAddress(String address) {
		if (addresses == null) {
			try {
				detectAddresses();
			} catch (IOException e) {
				// TODO SSch refactor
				Log.info(e);
			}
		}

		IPAddress result = (IPAddress) addresses.get(address);

			
		if (result == null) {
			result = (IPAddress) addresses.get(getCanonicalAddress(address));

			if (result==null)
			{
				String addressWithBrackets = "["+address+"]";
				result = (IPAddress) addresses.get(addressWithBrackets);
			}

		}

		if (result == null && Log.isError()) {
			Log.error("IPAddress object not found for " + address);
		}

		return result;
	}

	/**
	 * This method returns the canonical form of the supplied
	 * <code>address</code>.
	 * 
	 * @param address either an IPv4, IPv6 address or a DNS name
	 * @return the canonical address corresponding to <code>address</code>,
	 *         usually an IP address
	 */
	abstract public String getCanonicalAddress(String address);

	/**
	 * Starts the interface detection.
	 * 
	 * @throws IOException
	 */
	abstract void detectInterfaces() throws IOException;

	/**
	 * Starts the address detection.
	 * 
	 * @throws IOException
	 */
	abstract void detectAddresses() throws IOException;

}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.connection.tcp;

import java.io.IOException;
import java.net.InetAddress;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509KeyManager;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.security.ForcedAliasKeyManager;
import org.ws4d.java.util.Log;

public class SESecureServerSocket implements ServerSocket {

	private IPAddress			ipAddress			= null;

	private int					port			= -1;

	java.net.ServerSocket		server			= null;

	private String				alias			= null;
	
	private static final boolean 	needClientAuthByDefault=Boolean.parseBoolean(System.getProperty("MDPWS.PlatformSE.SESecureServerSocket.needClientAuthByDefault", "false"));

	/**
	 * The number of attempts to find a random port to listen to before giving
	 * up.
	 */
	protected static final int	PORT_RETRIES	= 3;

	public SESecureServerSocket(IPAddress address, int port, String alias) throws IOException {
		this(address,port,alias,needClientAuthByDefault);
	}
	
	public SESecureServerSocket(IPAddress address, int port, String alias, boolean needClientAuth) throws IOException {
		this.alias = alias;
		this.ipAddress = address;
		this.port = port;
		try {
			javax.net.ServerSocketFactory ssf = this.getSSLServerSocketFactory();
			//Bugfix added by SSch use ipaddress for createServerSocket
			InetAddress adr = InetAddress.getByName(ipAddress.getAddress());
			//server = ssf.createServerSocket(port,0,adr);
			
			javax.net.ssl.SSLServerSocket sslSocket=(javax.net.ssl.SSLServerSocket) ssf.createServerSocket(port, 0, adr);
			sslSocket.setNeedClientAuth(needClientAuth);
			this.server=sslSocket;
		} catch (IOException e) {
			throw new IOException(e.getMessage() + " for " + address + " at port " + port+" with alias "+alias);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#accept()
	 */
	@Override
	public Socket accept() throws IOException {
		return new SESecureSocket(server.accept(), this.alias);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#close()
	 */
	@Override
	public void close() throws IOException {
		server.close();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#getAddress()
	 */
	@Override
	public IPAddress getIPAddress() {
		return ipAddress;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.ServerSocket#getPort()
	 */
	@Override
	public int getPort() {
		return port;
	}

	protected SSLServerSocketFactory getSSLServerSocketFactory() throws IOException, Exception {

		//TODO SSch QoS Framework
		SecurityManagerModule secMod=(SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);
		if (secMod!=null)
		{
			KeyManager[] kms = (KeyManager[]) secMod.getKeyManagers();
			TrustManager[] tms = (TrustManager[]) secMod.getTrustManagers();

			if (alias != null) {
				for (int i = 0; i < kms.length; i++) {
					if (kms[i] instanceof X509KeyManager) kms[i] = new ForcedAliasKeyManager((X509KeyManager) kms[i], alias);
				}
			}

			SSLContext context = SSLContext.getInstance("SSL");
			try{
				context.init(kms, tms, null);	
			}catch(Exception e){
				Log.error(e);
			}
			
			SSLServerSocketFactory ssf = context.getServerSocketFactory();
			return ssf;
		}

		return null;
	}

}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.connection.tcp;

import java.io.IOException;

import org.ws4d.java.communication.connection.ip.IPAddress;

public class SecureSocketFactorySE {

	/**
	 * Creates an SSL secured SE ServerSocket.
	 * 
	 * @param adr IP address.
	 * @param port port
	 * @return the ServerSocket.
	 * @throws IOException
	 */
	public static ServerSocket createServerSocket(IPAddress adr, int port, String alias) throws IOException {
		return new SESecureServerSocket(adr, port, alias);
	}

	/**
	 * Creates an SSL secured SE Socket.
	 * 
	 * @param adr IP address.
	 * @param port port
	 * @return the ServerSocket.
	 * @throws IOException
	 */
	public static Socket createSocket(IPAddress adr, int port, String alias) throws IOException {
		return new SESecureSocket(adr, port, alias);
	}
}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.connection.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.monitor.MonitorStreamFactoryProvider;
import org.ws4d.java.communication.monitor.MonitoredInputStream;
import org.ws4d.java.communication.monitor.MonitoredOutputStream;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.util.Log;

public class TCPClient {

	private Socket			socket		= null;

	private TCPConnection	connection	= null;

	public static TCPClient connect(IPAddress address, int port) throws IOException {
		return connect(address, port, false, null);
	}

	public static TCPClient connect(IPAddress address, int port, boolean secure, String alias) throws IOException {
		if (address == null) {
			throw new IOException("Cannot listen for incoming data. No IP address given.");
		}
		if (port < 1 || port > 65535) {
			throw new IOException("Cannot listen for incoming data. Port number invalid.");
		}
		TCPClient client = new TCPClient(address, port, secure, alias);
		return client;
	}

	private TCPClient(IPAddress address, int port, boolean secure, String alias) throws IOException {

		// Re-Modularization 2011-01-21 Implement SecMod
		//		if (DPWSFramework.hasModule(DPWSFramework.DPWS_SECURITY_MODULE)) {
		//			socket = secure ? DPWSFramework.getSecurityManager().getSecureSocket(address, port, alias) : SocketFactory.createSocket(address, port);
		//		} else {
		//			socket = SocketFactory.createSocket(address, port);
		//		}

		//TODO SSch QoS Framework
		SecurityManagerModule secMod=(SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);
		if (secMod!=null && secure)
		{
			socket = secMod.getSecureSocket(address, port, alias);
		} else {
			socket = PlatformSupport.getInstance().getToolkit().getSocketFactory().createSocket(address, port);
		}

		if (socket!=null && socket.getLocalAddress()!=null && socket.getRemoteAddress()!=null)
		{
			InputStream in;
			OutputStream out;

			DPWSProtocolData data = new DPWSProtocolData(null, socket.getLocalAddress().getAddress(), socket.getLocalPort(), socket.getRemoteAddress().getAddress(), socket.getRemotePort(), true);

			if (MonitorStreamFactoryProvider.getInstance().getMonitorStreamFactory() != null) {
				in = new MonitoredInputStream(socket.getInputStream(), data);
				out = new MonitoredOutputStream(socket.getOutputStream(), data);
			} else {
				in = socket.getInputStream();
				out = socket.getOutputStream();
			}

			connection = new TCPConnection(in, out, socket, data);

			if (Log.isDebug()) {
				Log.debug("<O-TCP> To " + socket.getLocalAddress() + "@" + socket.getLocalPort() + " from " + socket.getRemoteAddress() + "@" + socket.getRemotePort() + ", " + connection);
			}
		}else{
			throw new IOException("Could not retrieve a socket! Address:"+address+" Port:"+port+" Alias:"+alias+" LocalAddress:"+(socket!=null?socket.getLocalAddress():null)+" RemoteAddress:"+(socket!=null?socket.getRemoteAddress():null));
		}
	}

	public TCPConnection getConnection() {
		return connection;
	}

}

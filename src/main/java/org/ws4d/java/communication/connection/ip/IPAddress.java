/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.connection.ip;

/**
 * Internal IP-Address container. It comprised the Address as a string, the info
 * if it is a Loopback address, a IPv4 or IPv6 address and if IPv6 if it is a
 * LinkLocal address
 */
//TODO SSch Führe EndpointAddress-Interface ein und lass networkDetection dieses zurückliefern...
public class IPAddress {

	private String	address;

	private boolean	isLoopback;

	private boolean	isIPv6;

	private boolean	isIPv6LinkLocal;

	public IPAddress(String address, boolean isLoopback, boolean isIPv6, boolean isIPv6LinkLocal) {
		super();

		this.isIPv6 = isIPv6;
		this.address = address.trim().intern();
		addBrackets();
		this.isLoopback = isLoopback;
		this.isIPv6LinkLocal = isIPv6LinkLocal;
	}

	public IPAddress(String address) {
		super();
		/*
		 * If the address contains any ":", address is an ipv6 address. Correct
		 * ipv6 address has brackets.
		 */
		isIPv6 = address.indexOf(':') != -1;
		this.address = address.trim().intern();
		addBrackets();
		this.isLoopback = false;
		this.isIPv6LinkLocal = false;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isLoopback() {
		return isLoopback;
	}

	public boolean isIPv6() {
		return isIPv6;
	}

	public boolean isIPv6LinkLocal() {
		return isIPv6LinkLocal;
	}

	@Override
	public String toString() {
		return address;
	}

	@Override
	public int hashCode() {
		int result = 31 + ((address == null) ? 0 : address.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		IPAddress other = (IPAddress) obj;
		if (address == null) {
			if (other.address != null) return false;
		} else if (!address.equals(other.address)) return false;
		return true;
	}

	private void addBrackets() {
		/*
		 * Correct ipv6 address has brackets.
		 */
		if (isIPv6) {
			if (this.address.charAt(0) != '[') {
				this.address = "[" + this.address;
			}
			if (this.address.charAt(this.address.length() - 1) != ']') {
				this.address = this.address + "]";
			}
		}
	}

}

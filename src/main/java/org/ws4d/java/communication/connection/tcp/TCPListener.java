/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.connection.tcp;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.ws4d.java.communication.DPWSProtocolData;
import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.monitor.MonitorStreamFactoryProvider;
import org.ws4d.java.communication.monitor.MonitoredInputStream;
import org.ws4d.java.communication.monitor.MonitoredOutputStream;
import org.ws4d.java.constants.CommunicationConstants;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.LinkedList;
import org.ws4d.java.structures.List;
import org.ws4d.java.structures.ListIterator;
import org.ws4d.java.util.Log;

/**
 * TCP listener which allows listening for incoming TCP connections.
 * <p>
 * Each incoming connection will be handled in a separate thread.
 * </p>
 */
class TCPListener implements Runnable {

	/** Number of attempts to open a server connection before giving up. */
	private static final int		ACCEPT_RETRIES		= 3;

	/** Time in ms before we retry to accept a connection with errors. */
	private static final int		ACCEPT_RETRY_DELAY	= 1000;

	private IPAddress				ipAddress				= null;

	private int						port				= -1;

	private Object					lockObj				= new Object();

	private boolean					running				= false;

	private ServerSocket			serverSocket		= null;

	private TCPConnectionHandler	handler				= null;

	private List					connections			= new LinkedList();

	private boolean					secure				= false;

	/**
	 * Creates a TCP listener for the given address and port.
	 * <p>
	 * This will open a server socket for the given address and port and will
	 * pass a {@link TCPConnection} to the given {@link TCPConnectionHandler}
	 * </p>
	 * 
	 * @param address the address to which to listen.
	 * @param port the port.
	 * @param handler the handler which will handle the TCP connection.
	 * @throws IOException
	 */
	TCPListener(IPAddress ipAddress, int port, TCPConnectionHandler handler) throws IOException {
		this(ipAddress, port, handler, false, null);

	}

	TCPListener(IPAddress ipAddress, int port, TCPConnectionHandler handler, boolean secure, String alias) throws IOException {

		SecurityManagerModule secMod= (SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);
		if (secure && secMod==null){
			throw new IOException("Cannot create SSL Socket. DPWS security module missing. Alias: "+alias);
		}

		if (handler == null) {
			throw new IOException("Cannot listen for incoming data. No handler set for connection handling.");
		}
		if (ipAddress == null) {
			throw new IOException("Cannot listen for incoming data. No IP address given.");
		}
		if (port < 1 || port > 65535) {
			throw new IOException("Cannot listen for incoming data. Port number invalid.");
		}

		this.handler = handler;
		this.ipAddress = ipAddress;
		this.port = port;
		this.serverSocket = secure ? secMod.getSecureServerSocket(ipAddress, port, alias) : PlatformSupport.getInstance().getToolkit().getSocketFactory().createServerSocket(ipAddress, port);
		this.secure=secure;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		synchronized (lockObj) {
			lockObj.notifyAll();
		}
		if (Log.isDebug()) Log.debug("TCP listener up for " + ipAddress + " and port " + port + ".");
		int retryCount = 0;
		while (isRunning()) {
			try {
				/*
				 * Wait for incoming connection.
				 */
				Socket socket =null;
				if (serverSocket!=null)
				{
					socket=serverSocket.accept();

				}else{
					if (Log.isError()) Log.error("No Server socket available for TCPListener: "+this.toString());
					break;
				}


				if (!isRunning()) {
					break;
				}
				if (socket == null) {
					if (Log.isWarn())
						Log.warn("Incoming TCP connection has returned no socket. Re-listening for new connections.");
					continue;
				}

				if (Log.isDebug())
					Log.debug("Accepted TCP connection:"+socket);

				/*
				 * Get the streams.
				 */
				InputStream in = socket.getInputStream();
				OutputStream out = socket.getOutputStream();

				if (in == null) {
					if (Log.isWarn()) Log.warn("Incoming TCP connection has no input stream. Cannot handle connection. Re-listening for new connections.");
					continue;
				}

				if (out == null) {
					if (Log.isWarn()) Log.warn("Incoming TCP connection has no output stream. Cannot handle connection. Re-listening for new connections.");
					continue;
				}

				DPWSProtocolData data = null;

				if (socket.getRemoteAddress() == null) {
					/*
					 * TODO: CLDC quick fix! It's not possible to retrieve the
					 * remote address from the CLDC socket. :-(
					 */
					data = new DPWSProtocolData(null, null, socket.getRemotePort(), socket.getLocalAddress()!=null? socket.getLocalAddress().getAddress():"", socket.getLocalPort(), true);
				} else {
					data = new DPWSProtocolData(null, socket.getRemoteAddress().getAddress(), socket.getRemotePort(),socket.getLocalAddress()!=null? socket.getLocalAddress().getAddress():"", socket.getLocalPort(), true);
				}

				if (CommunicationConstants.BUFFERED_INPUT) {
					in = new BufferedInputStream(in);
				}

				if (MonitorStreamFactoryProvider.getInstance().getMonitorStreamFactory() != null) {
					in = new MonitoredInputStream(in, data);
					out = new MonitoredOutputStream(out, data);
				}

				/*
				 * Create incoming TCP connection.
				 */
				TCPConnection connection = new TCPConnection(in, out, socket, data);

				manageConnectionList(connection); //Changed by SSch for Bugfix purposes

				if (Log.isDebug()) {
					if (socket.getRemoteAddress() != null) {
						if (Log.isDebug()) Log.debug("<I-TCP> From " + socket.getRemoteAddress() + "@" + socket.getRemotePort() + " to " + socket.getLocalAddress() + "@" + socket.getLocalPort() + ", " + connection);
					} else {
						if (Log.isDebug()) Log.debug("<I-TCP> From unkown host to " + ipAddress + " and port " + port + ", " + connection);
					}
				}

				/*
				 * Handle incoming TCP connection in an own thread.
				 */
				if (Log.isDebug()) Log.debug(connection +" -> ThreadPool available: "+PlatformSupport.getInstance().getToolkit().getThreadPool().getAvailableFreeThreads());

				boolean connectionThreadExecuted=PlatformSupport.getInstance().getToolkit().getThreadPool().executeOrAbort(new TCPConnectionThread(connection, handler));

				if (Log.isDebug()) Log.debug(connection +" -> executed? "+connectionThreadExecuted);
				if (!connectionThreadExecuted)
					handler.sendServiceUnavailable(connection);
			} catch (IOException e) {
				if (isRunning()) {
					if (retryCount++ < ACCEPT_RETRIES) {
						try {
							Thread.sleep(ACCEPT_RETRY_DELAY);
						} catch (InterruptedException ie) {
							if (Log.isWarn()){
								Log.warn("TCP listener interrupted. TCP listener shutdown for " + ipAddress + " and port " + port + ".");
								Log.warn(ie);
							}

							break;
						}
						if (Log.isWarn())Log.warn("Cannot open port " + port + " for " + ipAddress + ". Try " + retryCount + "."+e.getMessage());
						continue;
					}
					if (Log.isError()) Log.error("Cannot open port " + port + " for " + ipAddress + ". TCP listener shutdown for " + ipAddress + " and port " + port + ".");
					break;
				} else {
					break;
				}
			}
		}


	}



	/**
	 * Added by SSch
	 * @param connection
	 */
	protected synchronized void manageConnectionList(TCPConnection connection) {

		cleanUpConnectionList(); //Added by SSch: Bugfix Closed connections are not removed from the management list. 

		/*
		 * Store connection for the KILL method! ;-)
		 */
		connections.add(connection);
	}

	/**
	 * Bugfix: Closed connections are not removed from the management list. 
	 * Checks whether connections have been closed and can be cleaned up.
	 * Added by SSch
	 */
	protected void cleanUpConnectionList() {
		int sizeBefore=connections.size();
		if (sizeBefore>20)
		{

			for(ListIterator connectionIt=connections.listIterator();connectionIt.hasNext();)
			{
				TCPConnection storedConnection=(TCPConnection)connectionIt.next();
				if (storedConnection.isClosed())
				{
					connectionIt.remove();
				}
			}
			////System.out.println(sizeBefore+" -> "+connections.size());
		}
	}

	/**
	 * Indicates whether this listener is running or not.
	 * 
	 * @return <code>true</code> if the listener is running and will handle
	 *         incoming TCP connections, <code>false</code> otherwise.
	 */
	public synchronized boolean isRunning() {
		return running;
	}

	/**
	 * Starts the TCP listener.
	 * 
	 * @return <code>true</code> if the listener is started or already running,
	 *         <code>false</code> otherwise.
	 */
	public synchronized boolean start() {
		if (running) return true;
		boolean started = false;

		/*
		 * Get lock, and wait until the TCP listener is ready. This is necessary
		 * because we do not know, whether the thread pool starts this thread
		 * instant or not.
		 */
		synchronized (lockObj) {
			try {
				started = PlatformSupport.getInstance().getToolkit().getThreadPool().executeOrAbort(this);
				lockObj.wait();
				running = true;
			} catch (InterruptedException e) {
				return false;
			}
		}
		return started;
	}

	/**
	 * Stops the TCP listener.
	 * <p>
	 * Existing TCP connection will remain active! To stop the TCP server and
	 * close all established connections.
	 * </p>
	 */
	public synchronized void stop() throws IOException {
		if (!running) return;
		running = false;
		serverSocket.close();
		if (Log.isDebug()) {
			Log.debug("TCP listener shutdown for " + ipAddress + " and port " + port + ".");
		}
	}

	/**
	 * Stops the TCP listener and kills all established connection.
	 * <p>
	 * This will also close all established connections.
	 * </p>
	 */
	public synchronized void kill() throws IOException {
		stop();
		TCPConnection connection = null;
		try {
			Iterator it = connections.iterator();
			while (it.hasNext()) {
				connection = (TCPConnection) it.next();
				connection.close();
				it.remove();
			}
		} catch (IOException e) {
			if (connection != null) {
				Log.error("Cannot close TCP connection (" + connection.getIdentifier() + ").");
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ipAddress == null) ? 0 : ipAddress.hashCode());
		result = prime * result + port;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		TCPListener other = (TCPListener) obj;
		if (ipAddress == null) {
			if (other.ipAddress != null) return false;
		} else if (!ipAddress.equals(other.ipAddress)) return false;
		if (port != other.port) return false;
		return true;
	}


	@Override
	public String toString() {
		return "TCPListener [ipAddress=" + ipAddress + ", port=" + port
				+ ", running=" + running +", secure=" + secure + "]";
	}

	public void setSecure(boolean secure) {
		this.secure = secure;
	}

	public boolean isSecure() {
		return secure;
	}

	/**
	 * This thread allows the handling of each incoming connection.
	 */
	private class TCPConnectionThread implements Runnable {

		private TCPConnection			connection		= null;

		private TCPConnectionHandler	handler			= null;

		private Object					connectionLock	= new Object();

		TCPConnectionThread(TCPConnection connection, TCPConnectionHandler handler) {
			this.connection = connection;
			this.handler = handler;
		}

		@Override
		public void run() {
			synchronized (connectionLock) {
				connectionLock.notifyAll();
			}
			try {
				handler.handle(connection);
				if (Log.isDebug()) {
					Log.debug("<I> Incoming TCP connection (" + connection.getIdentifier() + ") handling done.");
				}
				connection.close();
			} catch (IOException e) {
				if (!connection.isClosed()) {
					if (Log.isInfo())
					{
						Log.info("<I> Incoming TCP connection (" + connection.getIdentifier() + "). " + e.getMessage() + ". Closing connection.");
						Log.info(e);
					}
					try {
						connection.close();
					} catch (IOException e1) {
						if (Log.isInfo())
						{
							Log.info("<I> Incoming TCP connection (" + connection.getIdentifier() + "). " + e.getMessage() + ".");
							Log.info(e1);
						}
					}

				}
			}
		}

		/**
		 * Starts the TCP connection thread.
		 */
		@SuppressWarnings("unused")
		public void start() {
			synchronized (connectionLock) {
				PlatformSupport.getInstance().getToolkit().getThreadPool().executeOrAbort(this);
				try {
					connectionLock.wait();
				} catch (InterruptedException e) {
					Log.warn("TCP connection cannot be handled. Thread interrupted.");
					return;
				}
			}
		}

	}

}

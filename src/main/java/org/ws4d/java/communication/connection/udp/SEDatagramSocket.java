/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.connection.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.UnknownHostException;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.constants.FrameworkConstants;
import org.ws4d.java.util.Log;

/**
 * DatagramSocket wrapper for SE.
 */
public class SEDatagramSocket implements DatagramSocket {

	private final java.net.MulticastSocket	socket;

	private final IPAddress				socketAddress;

	private final int					port;
	
	private final NetworkInterface      iface;

	private final boolean				isMulticast;	
	

	public SEDatagramSocket(IPAddress socketAddress, int port, NetworkInterface iface) throws IOException {
		this.socketAddress = socketAddress;
		this.port = port;
		this.iface = iface;
		
		InetAddress inetAddress = InetAddress.getByName(socketAddress.getAddress());
		isMulticast = inetAddress.isMulticastAddress();
		
		if (isMulticast) {
			try {
				if (Log.isDebug())
					Log.debug("Trying to join multicast group: "+iface+":"+port+" @"+socketAddress);
				
				socket = new MulticastSocket(port);
				socket.setNetworkInterface(iface);
				socket.joinGroup(inetAddress);
			} catch (IOException e) {
				if (Log.isWarn())
					Log.warn("Can not join multicast group (" + socketAddress + "@" + port + ") at interface " + iface.getName()+ ". No receiving of UDP packets on this interface.");
				
				throw e;
			}
		} else {
			try {
				if (Log.isDebug())
					Log.debug("Trying to create multicast socket: "+iface.getName()+":"+port);
				if (port >= 0 && port <= 0xFFFF)
				{
					socket = new MulticastSocket(port);
				}else{
					socket = new MulticastSocket();	
				}
				socket.setNetworkInterface(iface);
			} catch (IOException e) {
				if (Log.isWarn())
					Log.warn("Can not set NetworkInterface(" + socketAddress + "@" + port + ") at interface " + iface.getName()+" Details: "+e.getMessage());
				
				throw e;
			}
		}
	}

	public SEDatagramSocket(IPAddress socketAddress, int port, String ifaceName) throws IOException {
		this(socketAddress, port, NetworkInterface.getByName(ifaceName));
	}

	public void test(String host, int port) throws UnknownHostException {
		socket.connect(InetAddress.getByName(host), port);
	}

	@Override
	public void close() throws IOException {
		if (isMulticast) {
			socket.close();
			if (Log.isDebug()) {
				Log.debug("UDP multicast listener shutdown for interface: " + iface.getName() + " - " + socketAddress + ". Port:"+socket.getLocalPort()+" LocalPort:"+socket.getLocalPort()+" Interface: "+socket.getInterface()+" Socket:" + socket);
			}
		} else {
			socket.close();
			if (Log.isDebug())
				Log.debug("UDP listener shutdown for interface: " + iface.getName() + " - " + socketAddress + ". Port:"+socket.getLocalPort()+" LocalPort:"+socket.getLocalPort()+" Interface: "+socket.getInterface()+" Socket:" + socket);
			
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.udp.DatagramSocket#receive()
	 */
	@Override
	public Datagram receive() throws IOException {
		// get pooled buffer
		//		byte[] buffer = (byte[]) BUFFERS.acquire();
		DatagramPacket packet=null;
		Datagram dgram=null;
		byte[] buffer = getBA();
		if (buffer!=null)
		{
			packet = new DatagramPacket(buffer, buffer.length);
			socket.receive(packet);
			
			dgram = new Datagram(this, buffer, packet.getLength());
			dgram.setSocketAddress(this.socketAddress);
			dgram.setSocketPort(this.port);
			InetAddress ina = packet.getAddress();
			dgram.setAddress(new IPAddress(ina.getHostAddress(), ina.isLoopbackAddress(), (ina instanceof Inet6Address), ina.isLinkLocalAddress()));
			dgram.setPort(packet.getPort());
		}

		return dgram;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.connection.udp.DatagramSocket#send(org.ws4d
	 * .java.communication.connection.udp.Datagram)
	 */
	@Override
	public void send(Datagram datagram) throws IOException {
		byte[] data = datagram.getData();

		InetAddress address = InetAddress.getByName(datagram.getIPAddress().getAddress());
		DatagramPacket packet = new DatagramPacket(data, datagram.getContentLength(), address, datagram.getPort());
		socket.send(packet);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.connection.udp.DatagramSocket#release(org
	 * .ws4d.java.communication.connection.udp.Datagram)
	 */
	@Override
	public void release(Datagram datagram) {
		// return pooled buffer
		//		BUFFERS.release(datagram.getData());
		releaseBA(datagram.getData());
//		try{
//			throw new RuntimeException();
//		}catch(RuntimeException e){
//			e.printStackTrace();
//		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "interface: " + iface.getName() + ", port: " + port;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.connection.udp.DatagramSocket#getSocketAddress
	 * ()
	 */
	@Override
	public IPAddress getSocketAddress() {
		return socketAddress;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.communication.connection.udp.DatagramSocket#getSocketPort()
	 */
	@Override
	public int getSocketPort() {
		return port;
	}

	private static final int 			maxByteArrays=Integer.parseInt(System.getProperty("MDPWS.SEDatagramSocket.MaxByteArrays","15"));
	private static final int			maxWait4ByteArray=Integer.parseInt(System.getProperty("MDPWS.SEDatagramSocket.MaxWait4ByteArray", "800"));
	private static final byte			exhaustedAction=Byte.parseByte(System.getProperty("MDPWS.SEDatagramSocket.ExhaustedAction",String.valueOf(GenericObjectPool.WHEN_EXHAUSTED_GROW)));
	private static final ObjectPool pool=new GenericObjectPool(new ByteArrayFactory(),maxByteArrays,exhaustedAction,maxWait4ByteArray, 4); //maxActive=8, maxWait=200
	/**
	 * 
	 */
	private synchronized void releaseBA(byte[] array) {
		if (Log.isDebug())
			Log.debug("Release Byte Array:"+pool.getNumActive()+" "+pool.getNumIdle()+" "+array);
		try {
			pool.returnObject(array);
		} catch (Exception e) {
			Log.error(e);
		}
		if (Log.isDebug())
		Log.debug("\t\tRelease Byte Array:"+pool.getNumActive()+" "+pool.getNumIdle()+" "+array);
	}

	/**
	 * @return
	 */
	private synchronized byte[] getBA() {
		byte[] retVal=null;
		if (Log.isDebug())
			Log.debug("\tBorrow Byte Array:"+pool.getNumActive()+" "+pool.getNumIdle());
		try {
			retVal= (byte[]) pool.borrowObject();
		} catch (Exception e) {
			Log.error(e);
		}
		if (Log.isDebug())
			Log.debug("\t\tBorrow Byte Array:"+pool.getNumActive()+" "+pool.getNumIdle()+" "+retVal);
		return retVal;
	}

	private static class ByteArrayFactory extends BasePoolableObjectFactory{

		/* (non-Javadoc)
		 * @see org.apache.commons.pool.BasePoolableObjectFactory#makeObject()
		 */
		@Override
		public Object makeObject() throws Exception {
			return new byte[FrameworkConstants.DGRAM_MAX_SIZE];
		}
	}

}

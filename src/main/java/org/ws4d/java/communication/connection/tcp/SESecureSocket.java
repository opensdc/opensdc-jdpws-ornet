/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.communication.connection.tcp;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;

import javax.net.ssl.HandshakeCompletedEvent;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509KeyManager;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.NetworkDetection;
import org.ws4d.java.dispatch.FrameworkModuleRegistry;
import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.security.ForcedAliasKeyManager;
import org.ws4d.java.util.Log;

public class SESecureSocket implements Socket {

	private static final int	DEFAULT_OUT_BUFFER_SIZE	= 8192;

	java.net.Socket				socket;

	private IPAddress			ipAddress					= null;

	private int					port					= -1;

	private InputStream			in						= null;

	private OutputStream		out						= null;

	private String				alias					= null;

	private final  HandshakeCompletedListener listener = new HandshakeCompletedListener() {
		
		@Override
		public void handshakeCompleted(HandshakeCompletedEvent arg0) {
			if (Log.isDebug())
				Log.debug(arg0.toString());
		}
	};

	/**
	 * Default constructor. Initializes the object.
	 * 
	 * @param host host name.
	 * @param port port number.
	 * @param alias of the certificate to use.
	 * @throws IOException
	 */
	public SESecureSocket(IPAddress host, int port, String alias) throws IOException {
		try {
			this.alias = alias;
			javax.net.SocketFactory socketFactory = this.getSSLSocketFactory();
			this.socket =  socketFactory.createSocket(host.getAddress(), port);
			prepareSSLSocket();
			
		} catch (Exception e) {
			Log.info(e);
			throw new IOException(e);
		}
	}

	private void prepareSSLSocket() {
		if (this.socket instanceof SSLSocket)
		{
			SSLSocket sslSocket=(SSLSocket) socket;
			if (Log.isDebug())
				sslSocket.addHandshakeCompletedListener(listener);
			
			try {
				sslSocket.startHandshake();
			} catch (IOException e) {
				Log.info(e);
			}
		}
	}

	public SESecureSocket(java.net.Socket socket, String alias) {
		this.alias=alias;
		this.socket = socket;
		prepareSSLSocket();
	}

	/**
	 * Closes the connection.
	 */
	@Override
	public void close() throws IOException {
		if (socket == null) {
			throw new IOException("No open connection. Can not close connection");
		}
		socket.close();
	}

	/**
	 * Opens an <code>InputStream</code> on the socket.
	 * 
	 * @return an InputStream.
	 */
	@Override
	public InputStream getInputStream() throws IOException {
		if (socket == null) {
			throw new IOException("No open connection. Can not open input stream");
		}
		if (in == null) {
			//TODO SSch BufferedInputStream???
			in = socket.getInputStream();
		}
		return in;
	}

	/**
	 * Opens an <code>OutputStream</code> on the socket.
	 * 
	 * @return an OutputStream.
	 */
	@Override
	public OutputStream getOutputStream() throws IOException {
		if (socket == null) {
			throw new IOException("No open connection. Can not open output stream");
		}
		if (out == null) {
			out = new BufferedOutputStream(socket.getOutputStream(), DEFAULT_OUT_BUFFER_SIZE);
		}
		return out;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getRemoteAddress()
	 */
	@Override
	public IPAddress getRemoteAddress() {
		if (socket == null) return null;
		InetAddress i = socket.getInetAddress();
		if (i != null) {
			return new IPAddress(i.getHostAddress());
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getRemotePort()
	 */
	@Override
	public int getRemotePort() {
		if (socket == null) return -1;
		return socket.getPort();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getLocalAddress()
	 */
	@Override
	public IPAddress getLocalAddress() {
		if (ipAddress == null && socket !=null && PlatformSupport.getInstance().getToolkit().getNetworkDetection(NetworkDetection.IP_NETWORK)!=null)
		{
			String hostAddress = socket.getLocalAddress().getHostAddress();
			ipAddress = PlatformSupport.getInstance().getToolkit().getNetworkDetection(NetworkDetection.IP_NETWORK).getIPAddress(hostAddress);
		}
		return ipAddress;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getLocalPort()
	 */
	@Override
	public int getLocalPort() {
		if (port == -1) port = socket.getLocalPort();

		return port;
	}

	protected SSLSocketFactory getSSLSocketFactory() throws IOException, Exception {
		// Call the super classes to get suitable trust and key managers
		SecurityManagerModule secMod=(SecurityManagerModule) FrameworkModuleRegistry.getInstance().getModule(SecurityManagerModule.class);
		if (secMod!=null)
		{
		KeyManager[] kms = (KeyManager[]) secMod.getKeyManagers();
		TrustManager[] tms = (TrustManager[])  secMod.getTrustManagers();

		if (alias != null) {
			for (int i = 0; i < kms.length; i++) {
				if (kms[i] instanceof X509KeyManager) kms[i] = new ForcedAliasKeyManager((X509KeyManager) kms[i], alias);
			}
		}

		SSLContext context = SSLContext.getInstance("SSL");
		try{
			context.init(kms, tms, null);	
		}catch(Exception e){
			Log.error(e);
		}

		SSLSocketFactory ssf = context.getSocketFactory();
		return ssf;
		}
		return null;
	}
	

	@Override
	public String toString() {
		return "SESecureSocket [socket=" + socket + ", ipAddress=" + ipAddress
				+ ", port=" + port + ", in=" + in + ", out=" + out + ", alias="
				+ alias + "]";
	}

}

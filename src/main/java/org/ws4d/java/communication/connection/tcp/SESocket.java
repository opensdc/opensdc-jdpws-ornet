/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.communication.connection.tcp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;

import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.NetworkDetection;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.util.Log;

/**
 * This class implements a connection for the SE Platform.
 */
public class SESocket implements Socket {

	private static final boolean ENABLE_NO_DELAY=Boolean.parseBoolean(System.getProperty("MDPWS.Socket.TCPNoDelayEnabled", "true"));
	private static final boolean ENABLE_KEEP_ALIVE=Boolean.parseBoolean(System.getProperty("MDPWS.Socket.TCPKeepAliveEnabled", "true"));
	private static final String	ANY_ADDRESS	= "0.0.0.0".intern();
	private static final String ANY_ADDRESS_V6="0:0:0:0:0:0:0:1".intern();
	private static final String ANY_ADDRESS_V6_SHORT="::1".intern();

	java.net.Socket			socket;

	private IPAddress		ipAddress	= null;

	private int				port	= -1;

	private InputStream		in		= null;

	private OutputStream	out		= null;

	/**
	 * Default constructor. Initializes the object.
	 * 
	 * @param host host name.
	 * @param port port number.
	 * @throws IOException
	 */
	public SESocket(IPAddress host, int port) throws IOException {
		String adr = host.getAddress();
		if (adr.length() > 0 && adr.charAt(0) == '[') {
			adr = (adr.contains("%")) ? adr.substring(0, adr.lastIndexOf('%')) + ']' : adr;
		} else {
			adr = (adr.contains("%")) ? adr.substring(0, adr.lastIndexOf('%')) : adr;
		}
		socket = new java.net.Socket(adr, port);
		
		//Information about TCP_MODELAY & Delayed Acknowledgements
		//for more info TCP_NODELAY http://developers.slashdot.org/comments.pl?sid=174457&threshold=1&commentsort=0&mode=thread&cid=14515105
		//Delayed Acknowledgements: http://www.nwlab.net/guide2na/netzwerkanalyse-probleme-2.html
		//HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\Tcpip\Parameters\Interfaces tcpackfrequency
		//http://support.microsoft.com/kb/328890
		//More Info: http://www.stuartcheshire.org/papers/NagleDelayedAck/
		//http://www.faqs.org/faqs/unix-faq/socket/
		socket.setTcpNoDelay(ENABLE_NO_DELAY);
		socket.setKeepAlive(ENABLE_KEEP_ALIVE);

		this.port = socket.getLocalPort();
		
		if (Log.isDebug())
			Log.debug("Adr:"+adr+" Port:"+port+" LocalPort:"+this.port+" Timeout:"+socket.getSoTimeout()+" TCPNoDely"+socket.getTcpNoDelay()+" SoLinger"+socket.getSoLinger()+" RcB:"+socket.getReceiveBufferSize()+" SB:"+socket.getSendBufferSize());
	}

	public SESocket(java.net.Socket socket, IPAddress address) {
		this.socket = socket;
		this.ipAddress = address;
		this.port = socket.getLocalPort();
	}

	/**
	 * Closes the connection.
	 */
	@Override
	public void close() throws IOException {
		if (socket == null) {
			throw new IOException("No open connection. Can not close connection");
		}

		if (in!=null)
			in.close();
		if (out!=null)
			out.close();

		socket.close();
		
	}

	/**
	 * Opens an <code>InputStream</code> on the socket.
	 * 
	 * @return an InputStream.
	 */
	@Override
	public InputStream getInputStream() throws IOException {
		if (socket == null) {
			throw new IOException("No open connection. Can not open input stream");
		}
		if (in == null) {
			in = new BufferedInputStream(socket.getInputStream(),8192);
			//			in = socket.getInputStream();
		}
		return in;
	}

	/**
	 * Opens an <code>OutputStream</code> on the socket.
	 * 
	 * @return an OutputStream.
	 */
	@Override
	public OutputStream getOutputStream() throws IOException {
		if (socket == null) {
			throw new IOException("No open connection. Can not open output stream");
		}
		if (out == null) {
			out = new BufferedOutputStream(socket.getOutputStream(),8192);
			//out = socket.getOutputStream();
		}
		return out;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getRemoteAddress()
	 */
	@Override
	public IPAddress getRemoteAddress() {
		if (socket == null) return null;
		InetAddress i = socket.getInetAddress();
		if (i != null) {
			return new IPAddress(i.getHostAddress());
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getRemotePort()
	 */
	@Override
	public int getRemotePort() {
		if (socket == null) return -1;
		return socket.getPort();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getLocalAddress()
	 */
	@Override
	public IPAddress getLocalAddress() {

		//Work around from Beta 5 for http://sourceforge.net/tracker/?func=detail&aid=3202934&group_id=189181&atid=928295
		//		Hallo Stefan,
		//
		//		dieser Bug wurde durch einen temporären Workaround entschärft (so dass keine NullPointerException mehr aufritt), 
		//		die eigentliche Ursache habe wir bisher aber noch nicht gefunden. 
		//		Unter Vista und Windows 7 gibt java.net.Socket.getLocalAddress() manchmal "0.0.0.0" zurück, 
		//		obwohl der Socket bereits verbunden sein müsste. Der Workaround ist in der 
		//		Methode org.ws4d.java.communication.connection.tcp.SESocket.getLocalAddress() enthalten.
		//
		//		Viele Grüße,
		//		    Ingo


		InetAddress localInetAdr = socket.getLocalAddress();
		String localAdr = localInetAdr.getHostAddress();

		/*
		 * local (any address wildcard) workaround. will use the remote address
		 * as local address in case of local address was 0.0.0.0. We assume the
		 * remote address to be the local one if the local address was a
		 * wildcard.
		 */
		if (localAdr.equals(ANY_ADDRESS) || localAdr.equals(ANY_ADDRESS_V6_SHORT) || localAdr.equals(ANY_ADDRESS_V6)) 
		{
			localAdr = socket.getInetAddress().getHostAddress();
			if (Log.isDebug()) {
				Log.debug("Local IP address workaround used. Local address was a (0.0.0.0) wildcard. [isBound: "+socket.isBound()+", isConnected:"+socket.isConnected()+"]");
			}
		}

		if (ipAddress == null){
			ipAddress = PlatformSupport.getInstance().getToolkit().getNetworkDetection(NetworkDetection.IP_NETWORK).getIPAddress(localAdr);
		}
		return ipAddress;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.connection.tcp.Socket#getLocalPort()
	 */
	@Override
	public int getLocalPort() {
		return port;
	}

}

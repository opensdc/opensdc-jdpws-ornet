/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.service;

import org.ws4d.java.communication.CommunicationManagerID;
import org.ws4d.java.communication.ProtocolVersionInfo;
import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.eventing.EventSink;
import org.ws4d.java.eventing.EventingException;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageException;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.structures.EmptyStructures;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.URI;
import org.ws4d.java.types.URISet;

/**
 * Instances of this class hold essential information about an event
 * subscription as seen from the server-side. These include the
 * {@link #getNotifyTo() endpoint reference} to which events are to be
 * delivered, the {@link #getFilterActions() actions} the subscription
 * addresses, its {@link #getExpirationTime() expiration time} and the optional
 * endpoint to which the server-initiated end of the subscription should be
 * announced.
 */
public class ServiceSubscription {

	private static final long	MAX_EXPIRATION_DURATION					= SchemaUtil.MILLIS_PER_YEAR;

	private static final String	FAULT_REASON_INVALID_EXIRATION_TIME		= "The expiration time requested is invalid.";

	private static final String	FAULT_REASON_UNSUPPORTED_EXIRATION_TYPE	= "Only expiration durations are supported.";

	// -------------- VAR ------------------

	// milliseconds from the epoch
	long						expirationTime;																		// in

	/** remote client subscription id */
	EndpointReference			notifyTo;

	EndpointReference			endTo;

	CommunicationManagerID				communicationId;

	URISet						filterActions;

	/** Local event sink needed for local client */
	EventSink					sink									= null;

	/** Local client subscription */
	String						clientSubscriptionId					= null;

	EndpointReference			subscriptionManager;

	ProtocolVersionInfo	versionInfo;

	public ServiceSubscription(ProtocolVersionInfo versionInfo) {
		this.versionInfo = versionInfo;
	}

	void setExpiration(String expires, Message msg) throws MessageException {
		if (expires == null || (expires = expires.trim()).length() == 0) {
			expirationTime = System.currentTimeMillis() + MAX_EXPIRATION_DURATION;
			return;
		}
		if (expires.charAt(0) == 'P') {
			// _positive_ duration
			long duration = SchemaUtil.parseDuration(expires);
			if (duration <= 0L) {
				// Fault wse:InvalidExpirationTime
				//TODO SSch Vielleicht eine Registry f�r Fehlercode conversion einf�hren Code Abstract <-> Protocol spezifisch
				throw DefaultSubscriptionManager.createFault(msg, WSEConstants.WSE_FAULT_INVALID_EXPIRATION_TIME, new LocalizedString(FAULT_REASON_INVALID_EXIRATION_TIME, LocalizedString.DEFAULT_LANG));
			}
			expirationTime = System.currentTimeMillis() + duration;
		} else {
			// we currently don't support dateTime
			// long expTime = parseDateTime(expires);
			// if (expTime <= System.currentTimeMillis()) {
			// return false;
			// }
			// expirationTime = expTime;
			// Fault wse:UnsupportedExpirationType
			// TODO add supported expiration types within fault detail
			throw DefaultSubscriptionManager.createFault(msg, WSEConstants.WSE_FAULT_UNSUPPORTED_EXPIRATION_TYPE, new LocalizedString(FAULT_REASON_UNSUPPORTED_EXIRATION_TYPE, LocalizedString.DEFAULT_LANG));
		}
	}

	/**
	 * Sets expiration for local client subscription.
	 * 
	 * @param duration
	 */
	void setExpiration(long duration) throws EventingException {
		if (duration == 0) {
			expirationTime = System.currentTimeMillis() + MAX_EXPIRATION_DURATION;
			return;
		} else if (duration > 0) {
			expirationTime = System.currentTimeMillis() + duration;
		} else {
			/*
			 * negative duration, throw exception
			 */
			throw new EventingException(WSEConstants.WSE_FAULT_INVALID_EXPIRATION_TIME, FAULT_REASON_INVALID_EXIRATION_TIME);
		}
	}

	/**
	 * Sets EPR of subscription manager for this service subscription instance.
	 * 
	 * @param subscriptionManager
	 */
	void setSubscriptionManager(EndpointReference subscriptionManager) {
		this.subscriptionManager = subscriptionManager;
	}

	/**
	 * Returns the expiration time of this subscription in milliseconds from the
	 * epoch.
	 * 
	 * @return the expiration time
	 */
	public long getExpirationTime() {
		return expirationTime;
	}

	/**
	 * Returns the endpoint reference to which notifications matching this
	 * subscription shall be sent.
	 * 
	 * @return the endpoint reference to which to send notifications
	 */
	public EndpointReference getNotifyTo() {
		return notifyTo;
	}

	/**
	 * Returns the (optional) endpoint reference to which a server-side
	 * cancellation of the subscription should be announced.
	 * 
	 * @return the endpoint reference to which to send a subscription-end
	 *         announcement
	 */
	public EndpointReference getEndTo() {
		return endTo;
	}

	/**
	 * Returns the ID of the protocol to communicate over with the client (aka.
	 * event sink) for this subscription. In other words, this is the same
	 * protocol to use when sending messages to either one of the
	 * {@link #getNotifyTo() notify-to} or {@link #getEndTo() end-to} addresses.
	 * 
	 * @return the ID of the protocol to use for communication with the
	 *         subscriber of this subscription
	 */
	public CommunicationManagerID getCommunicationProtocolId() {
		return communicationId;
	}

	/**
	 * Returns a read-only iterator over the set of {@link URI action URIs} to
	 * which this subscription refers. This method never returns
	 * <code>null</code>, it will instead return an empty iterator in the case
	 * where no filter actions are available.
	 * 
	 * @return an iterator over {@link URI} instances representing the actions
	 *         to which this subscription refers
	 */
	public Iterator getFilterActions() {
		return filterActions == null ? EmptyStructures.EMPTY_ITERATOR : new ReadOnlyIterator(filterActions.iterator());
	}

	/**
	 * Returns the EPR of the subscription manager governing the state of this
	 * service subscription. Usually, this EPR includes - in addition to the
	 * manager's address - a server-side identifier (e.g. wse:Identifier from
	 * WS-Eventing) for this subscription instance.
	 * 
	 * @return the endpoint reference of the subscription manager for this
	 *         subscription
	 */
	public EndpointReference getSubscriptionManager() {
		return subscriptionManager;
	}

	public ProtocolVersionInfo getProtocolVersion() {
		return versionInfo;
	}

	public void setProtocolVersionInfo(ProtocolVersionInfo versionInfo) {
		this.versionInfo = versionInfo;
	}
	
	

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.service.parameter;

import org.ws4d.java.structures.Iterator;

public abstract class ParameterDefinition extends ParameterValue {

	public abstract String serialize();

	@Override
	public int getValueType() {
		return TYPE_UNKOWN;
	}

	@Override
	public String toString() {
		StringBuffer sBuf = new StringBuffer();
		sBuf.append("PV [ name=");
		sBuf.append(name);
		String value = serialize();
		if (value != null) {
			sBuf.append(", value=");
			sBuf.append(value);
		}
		if (attributes.size() > 0) {
			sBuf.append(", attributes=");
			sBuf.append("(");
			for (Iterator it = attributes(); it.hasNext();) {
				IParameterAttribute pa = (IParameterAttribute) it.next();
				sBuf.append(pa.toString());
				if (it.hasNext()) {
					sBuf.append(", ");
				}
			}
			sBuf.append(")");

		}
		if (children.size() > 0) {
			sBuf.append(", children=");
			sBuf.append("(");
			for (Iterator it = children(); it.hasNext();) {
				IParameterValue pv = (IParameterValue) it.next();
				sBuf.append(pv.toString());
				if (it.hasNext()) {
					sBuf.append(", ");
				}
			}
			sBuf.append(")");
		}
		sBuf.append(", min=");
		sBuf.append(min);
		sBuf.append(", max=");
		sBuf.append(max);
		sBuf.append(" ]");
		return sBuf.toString();
	}

}

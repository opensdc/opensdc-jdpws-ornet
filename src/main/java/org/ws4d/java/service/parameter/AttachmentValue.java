/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.service.parameter;

import java.io.IOException;
import java.io.InputStream;

import org.ws4d.java.attachment.Attachment;
import org.ws4d.java.attachment.AttachmentException;
import org.ws4d.java.attachment.AttachmentStore;
import org.ws4d.java.communication.MIMEContextID;
import org.ws4d.java.constants.XOPConstants;
import org.ws4d.java.structures.List;
import org.ws4d.java.types.InternetMediaType;
import org.ws4d.java.types.QNameFactory;

public class AttachmentValue extends ParameterDefinition {

	protected Attachment	attachment		= null;

	private MIMEContextID	attachmentScope	= null;

	public AttachmentValue() {

	}

	public AttachmentValue(String href) {
		attachment = new AttachmentStub(href);
	}

	/**
	 * Returns an attachment for this parameter value.
	 * 
	 * @return the attachment for this parameter value.
	 */
	public Attachment getAttachment() {
		return attachment;
	}

	/**
	 * Sets the attachment for this parameter value.
	 * 
	 * @param attachment the attachment to set.
	 */
	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	public void setAttachmentScope(MIMEContextID context) {
		this.attachmentScope = context;
	}

	public void initialize(String href) {
		attachment = new AttachmentStub(href);
	}

	@Override
	public List getNamespaces() {
		List ns = super.getNamespaces();
		ns.add(QNameFactory.getInstance().getQName(XOPConstants.XOP_ELEM_INCLUDE, XOPConstants.XOP_NAMESPACE_NAME, XOPConstants.XOP_NAMESPACE_PREFIX));
		return ns;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.parameter.Value#getType()
	 */
	@Override
	public int getValueType() {
		return TYPE_ATTACHMENT;
	}

	@Override
	public String serialize() {
		// TODO Auto-generated method stub
		return null;
	}

//	public void parse(String content) {
//		// TODO Auto-generated method stub
//
//	}
//
//	public void parseContent(XmlPullParser parser) throws IOException, XmlPullParserException {
//		int tag = parser.getEventType();
//		boolean xop = false;
//		if (tag == XmlPullParser.START_TAG && XOPConstants.XOP_NAMESPACE_NAME.equals(parser.getNamespace()) && XOPConstants.XOP_ELEM_INCLUDE.equals(parser.getName())) {
//			xop = true;
//		}
//		// XOP:Include start tag
//		if (!xop) {
//			tag = parser.nextTag();
//		}
//		if (tag == XmlPullParser.START_TAG && XOPConstants.XOP_NAMESPACE_NAME.equals(parser.getNamespace()) && XOPConstants.XOP_ELEM_INCLUDE.equals(parser.getName())) {
//			String href = parser.getAttributeValue(null, XOPConstants.XOP_ATTRIB_HREF);
//			/*
//			 * Strip the cid prefix from this href ! :D
//			 */
//			if (href.startsWith(XOPConstants.XOP_CID_PREFIX)) {
//				href = href.substring(XOPConstants.XOP_CID_PREFIX.length(), href.length());
//			}
//			attachment = new AttachmentStub(href);
//		} else {
//			throw new IOException("Cannot create attachment. Element xop:include not found.");
//		}
//		// XOP:Include end tag
//		if (!xop) {
//			tag = parser.nextTag();
//		}
//	}

//	public void serializeContent(XmlSerializer serializer) throws IOException {
//		if (attachment != null) {
//			/*
//			 * Serialize the XOP include element with attachment cid
//			 */
//			String cid = attachment.getContentId();
//
//			serializer.startTag(XOPConstants.XOP_NAMESPACE_NAME, XOPConstants.XOP_ELEM_INCLUDE);
//			serializer.attribute(null, XOPConstants.XOP_ATTRIB_HREF, XOPConstants.XOP_CID_PREFIX + cid);
//			serializer.endTag(XOPConstants.XOP_NAMESPACE_NAME, XOPConstants.XOP_ELEM_INCLUDE);
//		}
//
//	}

	public class AttachmentStub implements Attachment {

		private final String	contentId;

		private Attachment		delegate;

		/**
		 * @param contentId
		 */
		public AttachmentStub(String contentId) {
			super();
			this.contentId = contentId;
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.data.Attachment#dispose()
		 */
		@Override
		public void dispose() {
			if (delegate != null) {
				delegate.dispose();
			}
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.data.Attachment#getBytes()
		 */
		@Override
		public byte[] getBytes() throws AttachmentException, IOException {
			if (delegate == null) {
				resolve();
			}
			return delegate.getBytes();
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.data.Attachment#getContentId()
		 */
		@Override
		public String getContentId() {
			return contentId;
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.data.Attachment#getContentType()
		 */
		@Override
		public InternetMediaType getContentType() throws AttachmentException {
			if (delegate == null) {
				resolve();
			}
			return delegate.getContentType();
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.data.Attachment#getInputStream()
		 */
		@Override
		public InputStream getInputStream() throws AttachmentException, IOException {
			if (delegate == null) {
				resolve();
			}
			return delegate.getInputStream();
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.data.Attachment#getTransferEncoding()
		 */
		@Override
		public String getTransferEncoding() throws AttachmentException {
			if (delegate == null) {
				resolve();
			}
			return delegate.getTransferEncoding();
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.attachment.Attachment#getType()
		 */
		@Override
		public int getType() throws AttachmentException {
			if (delegate == null) {
				resolve();
			}
			return delegate.getType();
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.attachment.Attachment#isAvailable()
		 */
		@Override
		public boolean isAvailable() {
			try {
				return AttachmentStore.getInstance().isAvailable(attachmentScope, contentId);
			} catch (AttachmentException e) {
				return false;
			}
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.attachment.Attachment#size()
		 */
		@Override
		public long size() throws AttachmentException {
			if (delegate == null) {
				resolve();
			}
			return delegate.size();
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.attachment.Attachment#isLocal()
		 */
		@Override
		public boolean isLocal() {
			return false;
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.attachment.Attachment#getFilePath()
		 */
		@Override
		public String getFilePath() throws AttachmentException {
			if (delegate == null) {
				resolve();
			}
			return delegate.getFilePath();
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.attachment.Attachment#save(java.lang.String)
		 */
		@Override
		public void save(String targetFilePath) throws AttachmentException, IOException {
			if (delegate == null) {
				resolve();
			}
			delegate.save(targetFilePath);
		}

		/*
		 * (non-Javadoc)
		 * @see org.ws4d.java.attachment.Attachment#move(java.lang.String)
		 */
		@Override
		public boolean move(String newFilePath) throws AttachmentException {
			if (delegate == null) {
				resolve();
			}
			return delegate.move(newFilePath);
		}

		private synchronized void resolve() throws AttachmentException {
			this.delegate = AttachmentStore.getInstance().resolve(attachmentScope, contentId);
		}

	}

}

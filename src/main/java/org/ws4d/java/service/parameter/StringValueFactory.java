/**
 * 
 */
package org.ws4d.java.service.parameter;



/**
 * @author Stefan Schlichting
 *
 */
public class StringValueFactory {
	private static final StringValueFactory instance=new StringValueFactory();
	
	
	
	public static StringValueFactory getInstance() {
		return instance;
	}

	private StringValueFactory()
	{
		
	}
	
	public StringValue getStringValue()
	{
		return new StringValue();
	}
	
	public void releaseStringValue(StringValue sv)
	{
		//void
	}
}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.service.parameter;

import org.ws4d.java.schema.Schema;
import org.ws4d.java.schema.Type;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.QName;

public interface ITypedParameterValue extends IParameterValue{
	public abstract Iterator childrenFromType();
	public abstract void setMinOccurs(int min);

	/**
	 * Returns the the minimum occurrence for this parameter value.
	 * <p>
	 * The "minOccurs" attribute in XML Schema describes the minimum occurrence
	 * of this element inside the created XML instance document.
	 * </p>
	 * 
	 * @return the minimum occurrence of this parameter value.
	 */
	public abstract int getMinOccurs();

	public abstract void setMaxOccurs(int max);

	/**
	 * Returns the the maximum occurrence for this parameter value.
	 * <p>
	 * The "maxOccurs" attribute in XML Schema describes the maximum occurrence
	 * of this element inside the created XML instance document.
	 * </p>
	 * 
	 * @return the maximum occurrence of this parameter value.
	 */
	public abstract int getMaxOccurs();
	
	public abstract Type getType();
	
	/**
	 * Set the type of this parameter value.
	 * 
	 * @param type the type.
	 */
	public void setType(Type type);
	
	
	/**
	 * Returns the element information item instance type.
	 * 
	 * @return element information item instance type.
	 */
	public QName getInstanceType();
	
	/**
	 * Set the element information item instance type.
	 * 
	 * @param element information item instance type.
	 */
	public void setInstanceType(QName instanceTypeQName);
	
	
	/**
	 * Returns the VALUE TYPE for this parameter.
	 * <p>
	 * A VALUE TYPE should be a unique representation of a
	 * {@link ParameterValue} implementation which allows to identify the
	 * implementation and cast correctly.
	 * 
	 * @return the VALUE TYPE.
	 */
	public abstract int getValueType();
	
	/**
	 * Returns <code>true</code> if this parameter value is based on a complex
	 * type, <code>false</code> otherwise.
	 * 
	 * @return <code>true</code> if this parameter value is based on a complex
	 *         type, <code>false</code> otherwise.
	 */
	public boolean hasChildrenFromType() ;
	
	
	/**
	 * Resolve the types based on the given XML schema.
	 * 
	 * @param s the XML schema which contains the types for this parameter
	 *            value.
	 */
	public void resolveTypes(Schema s);
	
	public ITypedParameterValue getChild(QName name, int index, boolean reset);
	
	public Iterator getChildren(ITypedParameterValue wVal, String path);
	
	public int countChildren(QName name, boolean reset);

}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.service.parameter;

import org.ws4d.java.types.QName;

public interface IParameterAttribute {

	@Override
	public abstract String toString();

	public abstract String getValue();

	public abstract void setValue(String value);

	public abstract QName getName();

}

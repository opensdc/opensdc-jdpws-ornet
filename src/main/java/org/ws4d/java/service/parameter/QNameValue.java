/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.service.parameter;

import org.ws4d.java.structures.List;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

public class QNameValue extends ParameterDefinition {

	protected QName	value	= null;

	public QNameValue() {

	}

	public QNameValue(QName value) {
		this.value = value;
	}

	/**
	 * Returns the value of this parameter value.
	 * 
	 * @return the value.
	 */
	public QName get() {
		return this.value;
	}

	/**
	 * Sets the value of this parameter value.
	 * 
	 * @param value the value to set.
	 */
	public void set(QName value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (value!=null)
			return value.toStringPlain();
		else
			return "";
	}

	@Override
	public String serialize() {
		if (value!=null)
			return value.toStringPlain();
		else
			return "";
	}

	public void parse(String content) {
		this.value = QNameFactory.getInstance().getQName(content);

	}

	@Override
	public List getNamespaces() {
		List l= super.getNamespaces();
		if (value!=null && value.getNamespace().trim().length()>0)
			l.add(value);
		return l;
	}

	
	
//
//	public void parseContent(XmlPullParser parser) throws IOException, XmlPullParserException {
//		int tag = parser.getEventType();
//		if (tag == XmlPullParser.START_TAG) {
//			tag = parser.next(); // move to the content
//		}
//		if (tag == XmlPullParser.TEXT) {
//			value = QName.construct(parser.getText());
//		}
//	}
//
//	public void serializeContent(XmlSerializer serializer) throws IOException {
//		serializer.text(value.toStringPlain());
//	}

}

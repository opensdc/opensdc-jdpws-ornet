/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.service.parameter;

import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.List;
import org.ws4d.java.structures.ListIterator;
import org.ws4d.java.types.QName;

public interface IParameterValue{

	/**
	 * Returns the namespaces used by this parameter value.
	 * <p>
	 * This method allows to collect all namespaces and use it if necessary.
	 * </p>
	 * 
	 * @return a {@link List} of {@link QName}.
	 */
	public abstract List getNamespaces();

	/**
	 * Allows to override the serialization of this parameter.
	 * <p>
	 * <h3>NOTICE:</h3> The given <code>String</code> can contain anything but
	 * SHOULD contain correct XML data. <strong>This method should be used for
	 * debug purposes.</strong> A nested parameter can be overriden too.
	 * </p>
	 * <p>
	 * Set to <code>null</code> to disable the override.
	 * </p>
	 * 
	 * @param value the value which should override the parameter serialization,
	 *            or <code>null</code> if the parameter should not be
	 *            overridden.
	 */
	public abstract void overrideSerialization(String value);

	/**
	 * Returns whether this parameter value is overridden or not.
	 * 
	 * @return <code>true</code> the parameter serialization is overridden,
	 *         <code>false</code> otherwise.
	 */
	public abstract boolean isOverriden();

	public abstract String getOverride();

	/**
	 * Sets the value of an attribute of this parameter value with given value.
	 * 
	 * @param attribute the name of the attribute.
	 * @param value the value of the attribute.
	 */
	public abstract void setAttributeValue(String attribute, String value);
	

	/**
	 * Returns the value of an attribute for this parameter value.
	 * 
	 * @param attribute the attribute to get the value of.
	 * @return the value of the attribute.
	 */
	public abstract String getAttributeValue(String attribute);

	public abstract void add(IParameterAttribute attribute);

	public abstract void setAttributes(HashMap attributes);

	public abstract void addAnyAttribute(QName name, String value);

	/**
	 * Returns <code>true</code> if this parameter value has attributes,
	 * <code>false</code> otherwise.
	 * 
	 * @return <code>true</code> if this parameter value has attributes,
	 *         <code>false</code> otherwise.
	 */
	public abstract boolean hasAttributes();

	/**
	 * Returns an iterator of attributes for this parameter value.
	 * 
	 * @return an iterator of attributes for this parameter value.
	 */
	public abstract Iterator attributes();

	/**
	 * Returns an iterator over the qualified names of all attributes within
	 * this parameter value.
	 * 
	 * @return an iterator over {@link QName} instances, which represent the
	 *         names of this parameter value's attributes
	 */
	public abstract Iterator attributeNames();

	/**
	 * Set the name of this parameter value.
	 * 
	 * @param name the name.
	 */
	public abstract void setName(QName name);

	/**
	 * Set whether this parameter should carry values or not.
	 * 
	 * @param nil <code>true</code> this parameter will not have any values and
	 *            the XML instance nil will be set.
	 *            <strong>xsi:nil="true"</strong>
	 */
	public abstract void setNil(boolean nil);

	/**
	 * Returns whether the XML instance <strong>nil</strong> value is set or
	 * not.
	 * 
	 * @return <code>true</code> if the XML instance <strong>nil</strong> value
	 *         is set, <code>false</code> otherwise.
	 */
	public abstract boolean isNil();

	/**
	 * Returns the name of the parameter value. The name of the parameter value
	 * is the name of the entry inside the XML document.
	 * 
	 * @return the parameter value name
	 */
	public abstract QName getName();

	/**
	 * Adds an inner-element to this parameter value. This method is necessary
	 * to create nested structures.
	 * 
	 * @param value the parameter value to add.
	 */
	public abstract void add(IParameterValue value);

	public abstract void remove(IParameterValue value);

	/**
	 * Returns <code>true</code> if this parameter value has inner-elements,
	 * <code>false</code> otherwise.
	 * 
	 * @return <code>true</code> if this parameter value has inner-elements,
	 *         <code>false</code> otherwise.
	 */
	public abstract boolean hasChildren();

	/**
	 * Returns the number of inner-elements for the parameter value given by the
	 * path.
	 * 
	 * @param path the path to access the inner-element.
	 * @return the amount of inner-elements.
	 */
	public abstract int getChildrenCount(String path);

	/**
	 * Returns the number of inner-elements for the parameter value.
	 * 
	 * @param path the path to access the inner-element.
	 * @return the amount of inner-elements.
	 */
	public abstract int getChildrenCount();

	/**
	 * Returns an iterator of inner-elements for the parameter value given by
	 * the path.
	 * 
	 * @param path the path to access the inner-element.
	 * @return iterator of inner-elements.
	 */
	public abstract Iterator getChildren(String path);

	/**
	 * Returns an iterator of inner-elements for this parameter value.
	 * 
	 * @return an iterator of inner-elements for this parameter value.
	 */
	public abstract Iterator children();

	/**
	 * Returns an listiterator of inner-elements for this parameter value.
	 * 
	 * @return an listiterator of inner-elements for this parameter value.
	 */
	public abstract ListIterator getChildrenList();

	public abstract IParameterValue removeChild(String path);

	public abstract IParameterValue createChild(String path);

	public abstract HashMap getNamespaceCache(int depth);

	public abstract void release();

	public abstract IParameterValue get(String path)
			throws IndexOutOfBoundsException, IllegalArgumentException;

	/**
	 * @param pv
	 */
	public abstract void setParent(IParameterValue pv);

	public abstract IParameterValue getParent();

	public abstract boolean hasAttachmentInTree();

	public abstract void setChildWithAttachment(
			IParameterValue childPVWithAttachment);

	public abstract Iterator getChildrenWithAttachment();
	

	/**
	 * @param value
	 */
	public abstract void removeChildWithAttachment(
			IParameterValue childPVWithAttachment);

	public abstract boolean isRootPV();

}

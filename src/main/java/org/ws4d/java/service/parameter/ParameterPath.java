/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.service.parameter;

import org.ws4d.java.util.StringUtil;

/**
 * This class allows to separate the path.
 */
class ParameterPath {

	private static final char	PATH_SEPERATOR	= '/';

	private static final char	INDEX_BEGIN		= '[';

	private static final char	INDEX_EMD		= ']';

	private String[]			nodes			= null;

	ParameterPath(String path) {
		nodes = StringUtil.split(path, PATH_SEPERATOR);
		if (nodes == null) {
			return;
		}
	}

	public int getDepth() {
		return (nodes == null) ? 0 : nodes.length;
	}

	public String getNode(int depth) {
		String node = nodes[depth];

		// check for index
		int sPos = node.indexOf(INDEX_BEGIN);
		if (sPos > -1) {
			node = node.substring(0, sPos);
		}
		return node;
	}

	public int getIndex(int depth) {
		String node = nodes[depth];
		int index = 0;

		// check for index
		int sPos = node.indexOf(INDEX_BEGIN);
		if (sPos > -1) {
			int ePos = node.indexOf(INDEX_EMD, sPos);
			index = Integer.valueOf(node.substring(sPos + 1, ePos)).intValue();
		}
		return index;
	}

	public String getPath(int depth) {
		String path = "";
		for (int i = depth; i < nodes.length; i++) {
			if (i == depth) {
				path += nodes[i];
			} else {
				path += PATH_SEPERATOR + nodes[i];
			}
		}
		return path;
	}
}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.service;

import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.dispatch.DeviceServiceRegistryProvider;
import org.ws4d.java.message.metadata.GetResponseMessage;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.EmptyStructures;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.structures.Set;
import org.ws4d.java.types.DiscoveryData;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.HostedMData;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

/**
 * Proxy class of a (remote) dpws device
 */
public class ProxyDevice extends DeviceCommons {

	/** Device reference of this device */
	private DeviceReference	deviceReference		= null;

	/** List of service references attached to this device */
	private Set				serviceReferences	= null;
	
	private boolean			valid				= true;

	/**
	 * Constructor. Constructs device proxy by get response message.
	 * 
	 * @param message Holds information about discovered device.
	 */
	public ProxyDevice(GetResponseMessage message, DeviceReference devRef) {
		super(message.getThisModel(), message.getThisDevice());
		deviceReference = devRef;

		this.setSecure(devRef.isSecureDevice());

		// host block updated in dev ref handler
		DataStructure hostedList = message.getHosted();
		if (hostedList == null) {
			return;
		}

		serviceReferences = new HashSet(hostedList.size());
		// HostMData host = message.getRelationship().getHost();

		for (Iterator it = hostedList.iterator(); it.hasNext();) {
			/*
			 * build up services, references
			 */
			HostedMData hosted = (HostedMData) it.next();
			ServiceReference servRef = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().getStaticServiceReference(hosted);
			serviceReferences.add(servRef);
		}
	}

	// --------------------------------------------------------

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.ServiceModifiableImpl#isRemote()
	 */
	@Override
	public boolean isRemote() {
		return true;
	}

	// --------------------- DISCOVERY DATA --------------------

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.Device#getEndpointReferences()
	 */
	@Override
	public EndpointReference getEndpointReference() {
		return deviceReference.getEndpointReference();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.Device#getPortTypes()
	 */
	@Override
	public Iterator getPortTypes() {
		try {
			return deviceReference.getDevicePortTypes(false);
		} catch (TimeoutException e) {
			Log.info(e);
		}
		return EmptyStructures.EMPTY_ITERATOR;

	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.Device#getMetadataVersion()
	 */
	@Override
	public long getMetadataVersion() {
		try {
			return deviceReference.getMetadataVersion(false);
		} catch (TimeoutException e) {
			Log.info(e);
		}
		return DiscoveryData.UNKNOWN_METADATA_VERSION;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.Device#getDeviceReference()
	 */
	@Override
	public DeviceReference getDeviceReference() {
		return deviceReference;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.Device#getXAddresses()
	 */
	@Override
	public Iterator getXAddresses() {
		try {
			return deviceReference.getXAddresses(false);
		} catch (TimeoutException e) {
			Log.info(e);
		}
		return EmptyStructures.EMPTY_ITERATOR;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.Device#getScopes()
	 */
	@Override
	public Iterator getScopes() {
		try {
			return deviceReference.getScopes(false);
		} catch (TimeoutException e) {
			Log.info(e);
		}
		return EmptyStructures.EMPTY_ITERATOR;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.Device#getServiceReferences()
	 */
	@Override
	public Iterator getServiceReferences() {
		return serviceReferences == null ? EmptyStructures.EMPTY_ITERATOR : new ReadOnlyIterator(serviceReferences);
	}

	@Override
	public Iterator getServiceReferences(QNameSet servicePortTypes) {
		if (serviceReferences == null || serviceReferences.size() == 0) {
			return EmptyStructures.EMPTY_ITERATOR;
		}

		Set matchingServRefs = new HashSet(serviceReferences.size());
		for (Iterator it = serviceReferences.iterator(); it.hasNext();) {
			ServiceReference servRef = (ServiceReference) it.next();
			if (servicePortTypes.isContainedBy(servRef.getPortTypes())) {
				matchingServRefs.add(servRef);
			}
		}
		return new ReadOnlyIterator(matchingServRefs);
	}

	@Override
	public ServiceReference getServiceReference(URI serviceId) {
		if (serviceId == null) {
			return null;
		}
		String searchedServiceId = serviceId.toString();

		for (Iterator it = serviceReferences.iterator(); it.hasNext();) {
			ServiceReference servRef = (ServiceReference) it.next();
			if (searchedServiceId.equals(servRef.getServiceId().toString())) {
				return servRef;
			}
		}

		return null;
	}

	@Override
	public String getDefaultNamespace() {
		return null;
	}
	
	public void setValid(boolean valid) {
		this.valid = valid;
	}

	@Override
	public boolean isValid() {
		return valid;
	}

	@Override
	public void invalidate() {
		this.valid = false;
	}

	public void update(GetResponseMessage message, DeviceReference devRef) {
		// update metadata
		modelMetadata = message.getThisModel();
		deviceMetadata = message.getThisDevice();

		setValid(true);

		this.setSecure(devRef.isSecureDevice());

		serviceReferences.clear();
		
		// host block updated in dev ref handler
		DataStructure hostedList = message.getHosted();
		if (hostedList == null) {
			return;
		}	

		for (Iterator it = hostedList.iterator(); it.hasNext();) {
			HostedMData hosted = (HostedMData) it.next();

			ServiceReference servRef = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().getStaticServiceReference(hosted);
			serviceReferences.add(servRef);
		}
	}

}

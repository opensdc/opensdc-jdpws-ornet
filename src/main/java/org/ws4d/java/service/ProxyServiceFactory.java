/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.service;

import org.ws4d.java.configuration.FrameworkProperties;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.util.Log;

public class ProxyServiceFactory {

	private final static ProxyServiceFactory	INSTANCE;

	static {
		final String factoryClassName = FrameworkProperties.getInstance().getProxyServiceFactroryClass();

		ProxyServiceFactory factory = null;
		if (factoryClassName == null) {
			factory = new ProxyServiceFactory();
		} else {
			try {
				Class<?> proxyServiceClass = Class.forName(factoryClassName);
				factory = (ProxyServiceFactory) proxyServiceClass.newInstance();
				Log.debug("ProxyServiceFactory: Using " + factoryClassName);
			} catch (ClassNotFoundException e) {
				Log.error("ProxyServiceFactory: Configured ProxyServiceFactory class [" + factoryClassName + "] not found, falling back to default implementation");
				factory = new ProxyServiceFactory();
			} catch (Exception e) {
				Log.error("ProxyServiceFactory: Unable to create instance of configured ProxyServiceFactory class [" + factoryClassName + "], falling back to default implementation");
				Log.error(e);
				factory = new ProxyServiceFactory();
			}

		}
		INSTANCE = factory;
	}

	public static ProxyServiceFactory getInstance() {
		return INSTANCE;
	}

	public ProxyService newProxyService(ServiceReference serviceReference, DeviceReference parentDeviceReference) {
		return new ProxyService(serviceReference, parentDeviceReference);
	}
}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.service;

import java.io.IOException;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DefaultResponseCallback;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.WSEConstants;
import org.ws4d.java.dispatch.OutDispatcher;
import org.ws4d.java.dispatch.ProtocolVersionInfoRegistry;
import org.ws4d.java.eventing.ClientSubscription;
import org.ws4d.java.eventing.EventSink;
import org.ws4d.java.eventing.EventSourceCommons;
import org.ws4d.java.eventing.EventingException;
import org.ws4d.java.message.FaultMessage;
import org.ws4d.java.message.IMessageEndpoint;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.MessageHeader;
import org.ws4d.java.message.eventing.GetStatusMessage;
import org.ws4d.java.message.eventing.GetStatusResponseMessage;
import org.ws4d.java.message.eventing.RenewMessage;
import org.ws4d.java.message.eventing.RenewResponseMessage;
import org.ws4d.java.message.eventing.SubscribeMessage;
import org.ws4d.java.message.eventing.SubscribeResponseMessage;
import org.ws4d.java.message.eventing.UnsubscribeMessage;
import org.ws4d.java.message.eventing.UnsubscribeResponseMessage;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.HashSet;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.Set;
import org.ws4d.java.types.Delivery;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.Filter;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.ReferenceParametersMData;
import org.ws4d.java.types.URI;
import org.ws4d.java.types.URISet;
import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.WSDL;
import org.ws4d.java.wsdl.WSDLOperation;
import org.ws4d.java.wsdl.WSDLPortType;
import org.ws4d.java.wsdl.WSDLRepository;

/**
 * Proxy class of a dpws service.
 * 
 * @author mspies
 */
public class ProxyService extends ServiceCommons {

	private ServiceReference	serviceReference;

	private DeviceReference		parentDeviceReference	= null;

	//TODO SSch avoid NPE and avaoid deadlock, maybe reentrent lock problem
//	private LockSupport		parentDeviceReferenceLocksupport	= new LockSupport();

	private int SUBSCRIPTION_MANAGER_NETWORK_COM_TIMEOUT=Timeouts.SUBSCRIPTION_MANAGER_NETWORK_COM_TIMEOUT;

	/**
	 * Constructor. Will create proxy service, which must be initialized by
	 * {@link #initialize(ServiceReference, DeviceReference)()} later on.
	 */
	ProxyService() {}

	/**
	 * @param serviceReference
	 */
	public ProxyService(ServiceReference serviceReference) {
		this(serviceReference, null);
	}

	/**
	 * @param serviceReference
	 * @param parentDeviceReference
	 */
	public ProxyService(ServiceReference serviceReference, DeviceReference parentDeviceReference) {
		super();

		try {
			initialize(serviceReference, parentDeviceReference);
		} catch (InstantiationException e) {
			// won't happen
		}
	}

	/**
	 * Must be called after construction of ProxyService without
	 * {@link ServiceReference} as parameter.
	 * 
	 * @param serviceReference
	 * @param parentDeviceReference - may be null.
	 */
	protected void initialize(ServiceReference serviceReference, DeviceReference parentDeviceReference) throws InstantiationException {
		if (this.serviceReference != null) {
			throw new InstantiationException("ProxyService already initialized!");
		}

		this.serviceReference = serviceReference;
		setParentDeviceReference(parentDeviceReference);

		this.setSecure(serviceReference.isSecureService());
		if (!WSDLRepository.DEMO_MODE) {
			if (loadFromEmbeddedWSDLs(getPortTypes())) {
				return;
			}
		}
		// not all found within embedded WSDLs, try building up from local repo
		if (loadFromRepository(getPortTypes())) {
			return;
		}
		/*
		 * not all found within service reference or local repo, build up from
		 * metadata locations
		 */
		loadFromMetadataLocations(getPortTypes());

	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.Service#getServiceReference()
	 */
	@Override
	public ServiceReference getServiceReference() {
		return serviceReference;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.Service#getParentDeviceReference()
	 */
	@Override
	public DeviceReference getParentDeviceReference() {
		return parentDeviceReference;
	}

	/**
	 * Sets device reference of parent device of this service.
	 * 
	 * @param devRef
	 */
	public void setParentDeviceReference(DeviceReference devRef) {
		try{
			//this.parentDeviceReferenceLocksupport.exclusiveLock();
			this.parentDeviceReference = devRef;
		}finally{
			//this.parentDeviceReferenceLocksupport.releaseExclusiveLock();
		}
	}

	// -------------------------------------------------------

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.Service#isRemote()
	 */
	@Override
	public boolean isRemote() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.Service#getServiceId()
	 */
	@Override
	public URI getServiceId() {
		return serviceReference.getServiceId();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.Service#getEndpointReferences()
	 */
	@Override
	public Iterator getEndpointReferences() {
		return serviceReference.getEndpointReferences();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.service.Service#getPortTypes()
	 */
	@Override
	public Iterator getPortTypes() {
		return serviceReference.getPortTypes();
	}

	/**
	 * @param portTypes
	 * @throws RuntimeException if no metadata (WSDL) is found for at least one
	 *             of the specified <code>portTypes</code>
	 */
	public void appendPortTypes(QNameSet portTypes) throws RuntimeException {
		if (loadFromRepository(portTypes.iterator())) {
			return;
		}
		loadFromMetadataLocations(portTypes.iterator());
	}

	/**
	 * Initializes event receiving from specified event sender.
	 * 
	 * @param sink event sink which will receive the notifications.
	 * @param clientSubscriptionId
	 * @param eventActionURIs set of action URIs to subscribe to.
	 * @param duration duration in milliseconds of subscription. If 0 no
	 *            expiration of subscription.
	 * @return subscription id (wse:identifier)
	 * @throws EventingException
	 * @throws TimeoutException
	 */
	@Override
	public ClientSubscription subscribe(EventSink sink, String clientSubscriptionId, URISet eventActionURIs, long duration) throws EventingException, TimeoutException {
		if (!sink.isOpen()) {
			Log.error("Cannot subscribe, event sink is not open");
			throw new EventingException("EventSink not open");
		}

		/*
		 * Create subscibe message
		 */
		SubscribeMessage request = new SubscribeMessage();
		ServiceReference serviceRef = getServiceReference();
		URI preferredXAddress = serviceRef.getPreferredXAddress();
		request.getHeader().setEndpointReference((EndpointReference) getEndpointReferences().next());
		request.setTargetAddress(preferredXAddress);
		
		setProtocolVersion(request);

		ReferenceParametersMData refParams = new ReferenceParametersMData();
		refParams.addUnknownElement(WSEConstants.WSE_QN_IDENTIFIER, clientSubscriptionId);
		/*
		 * TODO find out which of the sink's bindings corresponds to the
		 * communication interface, which from the address of one of the EPRs of
		 * the target service is reachable from
		 */

		EndpointReference notifyTarget = new EndpointReference(URI.EMPTY_URI, refParams);
		Delivery delivery = new Delivery(null, notifyTarget);
		request.setDelivery(delivery);
		request.setBindingCandidatesForNotification(sink.getBindings());

		if (duration != 0) {
			request.setExpires(SchemaUtil.createDuration(duration));
		}

		Filter filter = new Filter(DPWSConstants.DPWS_URI_FILTER_EVENTING_ACTION, eventActionURIs);
		request.setFilter(filter);

		CallbackHandler handler = createCallbackHandler(null); //Changed by SSch 
		OutDispatcher.getInstance().send(request, serviceRef.getPreferredCommunicationManagerID(), handler);

		synchronized (handler) {
			try {
				handler.wait(SUBSCRIPTION_MANAGER_NETWORK_COM_TIMEOUT);
			} catch (InterruptedException e) {
				// void
			}
		}

		ClientSubscription subscription = null;
		if (handler.msg != null) {
			/*
			 * CASE: Subscription Response received
			 */
			SubscribeResponseMessage subscribeRsp = (SubscribeResponseMessage) handler.msg;
			EndpointReference serviceSubscription;
			serviceSubscription = subscribeRsp.getSubscriptionManager();
			subscription = new DefaultClientSubscription(sink, clientSubscriptionId, serviceSubscription, duration, serviceReference);

			// SubscriptionManager manager = new SubscriptionManagerProxy(
			// serviceSubscriptionId, sink, duration );
			//
			// subscription = new
			// DefaultClientSubscription(clientSubscriptionId,
			// serviceSubscriptionId, serviceReference, manager);

			sink.addSubscription(clientSubscriptionId, subscription);
		} else if (handler.fault != null) {
			/*
			 * CASE: Fault received
			 */
			throw new EventingException(handler.fault);
		} else {
			throw new TimeoutException("Subscribe timeout");
		}

		return subscription;
	}

	/**
	 * Added by SSch in order to allow extension.
	 * 
	 * @param object
	 * @return
	 */
	protected CallbackHandler createCallbackHandler(Operation object) {
		return  new CallbackHandler(object);
	}

	/**
	 * Unsubscribes from specified subscription.
	 * 
	 * @param subscription subscription to terminate.
	 * @throws EventingException
	 * @throws TimeoutException
	 * @throws CommunicationException 
	 */
	@Override
	public void unsubscribe(ClientSubscription subscription) throws EventingException,  CommunicationException {
		if (subscription == null) {
			Log.error("Cannot unsubscribe, subscription is null");
			throw new EventingException("Subscription is null");
		}

		if (getServiceReference()!=null)
		{
			DeviceReference devRef = getServiceReference().getParentDeviceRef();
			if (devRef!=null && devRef.isDeviceObjectExisting())
			{
				/*
				 * Create unsubscribe message
				 */
				EndpointReference subRef = subscription.getServiceSubscriptionReference();
				UnsubscribeMessage request = new UnsubscribeMessage();
				EndpointReference eprSubscriptionManager = subscription.getServiceSubscriptionReference();
				request.setTargetAddress(eprSubscriptionManager.getAddress());
				MessageHeader header = request.getHeader();
				header.setEndpointReference(subRef);
				
				setProtocolVersion(request);

				CallbackHandler handler = createCallbackHandler(null); //Changed by SSch
				/*
				 * XXX this is based on the assumption that both the subscribed service
				 * as well as its possibly stand-alone subscription manager use the same
				 * communication protocol
				 */
				OutDispatcher.getInstance().send(request, getServiceReference().getPreferredCommunicationManagerID(), handler);

				synchronized (handler) {
					try {
						handler.wait(SUBSCRIPTION_MANAGER_NETWORK_COM_TIMEOUT);
					} catch (InterruptedException e) {
						// void
					}
				}

				if (handler.msg != null) {
					// CASE: Unsubscribe Response received, return
					return;
				} else if (handler.fault != null) {
					// CASE: Fault received
					throw new EventingException(handler.fault);
				} else {
					// CASE: Timeout of watchdog
					throw new TimeoutException("Unsubscribe timeout");
				}
			}
		}
		throw new CommunicationException("Unsubscribe failed due to missing service ref.");

	}

	/**
	 * Renews an existing subscription with new duration. If duration is "0"
	 * subscription never terminates.
	 * 
	 * @param subscription
	 * @param duration
	 * @throws EventingException
	 * @throws TimeoutException
	 */
	@Override
	public void renew(ClientSubscription subscription, long duration) throws EventingException, TimeoutException {
		if (subscription == null) {
			Log.error("Cannot renew, subscription is null");
			throw new EventingException("Subscription is null");
		}

		if (!subscription.getEventSink().isOpen()) {
			Log.error("Cannot renew, event sink is not open");
			throw new EventingException("EventSink not open");
		}

		/*
		 * Create renew message
		 */
		RenewMessage request = new RenewMessage();
		EndpointReference eprSubscriptionManager = subscription.getServiceSubscriptionReference();
		request.setTargetAddress(eprSubscriptionManager.getAddress());
		request.getHeader().setEndpointReference(subscription.getServiceSubscriptionReference());
		if (duration != 0) {
			request.setExpires(SchemaUtil.createDuration(duration));
		}

		setProtocolVersion(request);

		CallbackHandler handler = createCallbackHandler(null); //Changed by SSch
		/*
		 * XXX this is based on the assumption that both the subscribed service
		 * as well as its possibly stand-alone subscription manager use the same
		 * communication protocol
		 */
		OutDispatcher.getInstance().send(request, getServiceReference().getPreferredCommunicationManagerID(), handler);

		synchronized (handler) {
			try {
				handler.wait();
			} catch (InterruptedException e) {
				// void
			}
		}

		// URI subscriptionId = null;
		if (handler.msg != null) {
			// CASE: Subscription Response received
			// RenewResponseMessage renewRsp = (RenewResponseMessage)
			// context.msg;
		} else if (handler.fault != null) {
			// CASE: Fault received
			throw new EventingException(handler.fault);
		} else {
			throw new TimeoutException("Renew timeout");
		}

	}

	public long getStatus(ClientSubscription subscription) throws EventingException, TimeoutException {
		if (subscription == null) {
			Log.error("Cannot get status, subscription is null");
			throw new EventingException("Subscription is null");
		}

		if (!subscription.getEventSink().isOpen()) {
			Log.error("Cannot get status, event sink is not open");
			throw new EventingException("EventSink not open");
		}

		/*
		 * Create getStatus message
		 */
		GetStatusMessage request = new GetStatusMessage();
		EndpointReference eprSubscriptionManager = subscription.getServiceSubscriptionReference();
		request.setTargetAddress(eprSubscriptionManager.getAddress());

		request.getHeader().setEndpointReference(subscription.getServiceSubscriptionReference());
		setProtocolVersion(request);

		CallbackHandler handler = createCallbackHandler(null); //Changed by SSch
		/*
		 * XXX this is based on the assumption that both the subscribed service
		 * as well as its possibly stand-alone subscription manager use the same
		 * communication protocol
		 */
		OutDispatcher.getInstance().send(request, getServiceReference().getPreferredCommunicationManagerID(), handler);

		synchronized (handler) {
			try {
				handler.wait();
			} catch (InterruptedException e) {
				// void
			}
		}

		// URI subscriptionId = null;
		if (handler.msg != null) {
			// CASE: GetStatus response received
			GetStatusResponseMessage getStatusRsp = (GetStatusResponseMessage) handler.msg;
			String expires = getStatusRsp.getExpires();
			return SchemaUtil.parseDuration(expires);

		} else if (handler.fault != null) {
			// CASE: Fault received
			throw new EventingException(handler.fault);
		} else {
			throw new TimeoutException("GetStatus timeout");
		}
	}

	private boolean loadFromEmbeddedWSDLs(Iterator portTypes) {
		Iterator wsdls = serviceReference.getWSDLs();
		if (!wsdls.hasNext()) {
			return false;
		}
		// make a copy of required port types
		DataStructure portTypesToResolve = new HashSet();
		for (Iterator it = portTypes; it.hasNext();) {
			QName portTypeName = (QName) it.next();
			portTypesToResolve.add(portTypeName);
		}
		while (wsdls.hasNext()) {
			WSDL wsdl = (WSDL) wsdls.next();

			for (Iterator it = portTypesToResolve.iterator(); it.hasNext();) {
				QName portTypeName = (QName) it.next();
				WSDLPortType portType = wsdl.getPortType(portTypeName);
				/*
				 * we don't check whether this port type has an actual binding
				 * or service definition within the WSDL, as it is declared
				 * within the service reference (aka. within the service's
				 * hosted block)
				 */
				if (portType != null) {
					processWSDLPortType(portType);
					it.remove();
				}
			}
			//SSch 2011-03-20 added to allow modification of the wsdl
			prepareWSDLOnServiceLevel(wsdl);
		}
		return portTypesToResolve.isEmpty();
	}


	private boolean loadFromRepository(Iterator portTypes) {
		boolean allFound = true;
		WSDLRepository repo = WSDLRepository.getInstance();
		Set loadedWsdls = new HashSet();
		for (Iterator it = portTypes; it.hasNext();) {
			QName portTypeName = (QName) it.next();
			if (this.portTypes.containsKey(portTypeName)) {
				// port type already loaded
				continue;
			}

			WSDLPortType wsdlPortType = null;
			for (Iterator it2 = loadedWsdls.iterator(); it2.hasNext();) {
				WSDL wsdl = (WSDL) it2.next();
				wsdlPortType = wsdl.getPortType(portTypeName);
				if (wsdlPortType != null) {
					break;
				}
			}
			if (wsdlPortType == null) {
				WSDL wsdl = repo.getWSDL(portTypeName);
				if (wsdl == null) {
					allFound = false;
					if (Log.isDebug()) {
						Log.debug("Unable to find a WSDL within local repository for port type " + portTypeName);
					}
					continue;
				}
				//SSch 2011-03-20 added to allow modification of the wsdl
				prepareWSDLOnServiceLevel(wsdl);
				loadedWsdls.add(wsdl);
				wsdlPortType = wsdl.getPortType(portTypeName);
			}
			processWSDLPortType(wsdlPortType);
		}
		return allFound;
	}

	private void loadFromMetadataLocations(Iterator portTypes) {
		Iterator locations = serviceReference.getMetadataLocations();
		if (!locations.hasNext()) {
			throw new RuntimeException("No metadata locations found");
		}
		// make a copy of required port types
		DataStructure portTypesToResolve = new HashSet();
		for (Iterator it = portTypes; it.hasNext();) {
			QName portTypeName = (QName) it.next();
			// avoid already loaded
			if (!this.portTypes.containsKey(portTypeName)) {
				portTypesToResolve.add(portTypeName);
			}
		}
		while (locations.hasNext()) {
			if (portTypesToResolve.isEmpty()) {
				return;
			}
			URI address = (URI) locations.next();
			// Get WSDL from remote location
			try {
				WSDL wsdl = WSDLRepository.loadWsdl(address);

				for (Iterator it = portTypesToResolve.iterator(); it.hasNext();) {
					QName portTypeName = (QName) it.next();
					WSDLPortType portType = wsdl.getPortType(portTypeName);
					/*
					 * we don't check whether this port type has an actual
					 * binding or service definition within the WSDL, as it is
					 * declared within the service reference (aka. within the
					 * service's hosted block)
					 */
					if (portType != null) {
						processWSDLPortType(portType);
						it.remove();
					}
				}
				//SSch 2011-03-20 added to allow modification of the wsdl
				prepareWSDLOnServiceLevel(wsdl);
			} catch (IOException e) {
				Log.info("Could not load WSDL from "+address+". "+e.getMessage());
				Log.info(e);
				throw new RuntimeException(e.getMessage());
			}
		}
		if (!portTypesToResolve.isEmpty()) {
			throw new RuntimeException("Unable to resolve some port types of service: " + portTypesToResolve);
		}
	}


	/**
	 * @author schlichs
	 * @param wsdl
	 */
	protected void prepareWSDLOnServiceLevel(WSDL wsdl) {
		//	void
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.service.ServiceCommons#createOperation(org.ws4d.java.wsdl
	 * .WSDLOperation)
	 */
	@Override
	protected Operation createOperation(WSDLOperation wsdlOperation) {
		return new Operation(wsdlOperation) {

			@Override
			public IParameterValue invoke(IParameterValue IParameterValue) throws InvocationException, TimeoutException {
				/*
				 * client side invocation dispatcher
				 */
				return dispatchInvoke(this, IParameterValue);
			}

		};
	}

	protected IParameterValue dispatchInvoke(Operation op, IParameterValue parameterValue) throws InvocationException, TimeoutException {

		InvokeMessage msg = new InvokeMessage(op.getInputAction());
		return dispatchInvoke(msg, op, parameterValue);
	}


	/**
	 * Extracted content to dispatchInvoke(...,handler). by SSch
	 * 
	 * @param msg
	 * @param op
	 * @param parameterValue
	 * @return
	 * @throws InvocationException
	 * @throws TimeoutException
	 */
	protected IParameterValue dispatchInvoke(InvokeMessage msg, Operation op, IParameterValue parameterValue) throws InvocationException, TimeoutException {
		return dispatchInvoke(msg, op, parameterValue, createCallbackHandler(op));
	}

	protected IParameterValue dispatchInvoke(InvokeMessage msg, Operation op, IParameterValue parameterValue, CallbackHandler handler ) throws InvocationException, TimeoutException {//Added by SSch new Parameter Callbackhandler
		// create InvokeInputMessage from op; set correct action URI

		msg.getHeader().setMessageEndpoint(op);
		msg.getHeader().setEndpointReference((EndpointReference) getEndpointReferences().next());
		ServiceReference serviceRef = getServiceReference();
		URI preferredXAddress = serviceRef.getPreferredXAddress();
		msg.setTargetAddress(preferredXAddress);
		msg.setContent(parameterValue);
		setProtocolVersion(msg);

		if (handler==null)
			handler = createCallbackHandler(op); //Changed by SSch

		OutDispatcher.getInstance().send(msg, serviceRef.getPreferredCommunicationManagerID(), handler);

		if (op.isOneWay()) {
			// don't block forever
			return null;
		}

		synchronized (handler) {
			try {
				if (handler.msg == null && handler.fault == null) {
					//TODO SSch add max timeout ???
					handler.wait();
				}
			} catch (InterruptedException e) {
				// void
			}
		}

		if (handler.msg != null) {
			InvokeMessage rspMsg = (InvokeMessage) handler.msg;
			return rspMsg.getContent();
		} else if (handler.fault != null) {
			/*
			 * CASE: Fault received
			 */
			FaultMessage fault = handler.fault;
			throw new InvocationException(fault);
		} else {
			throw new TimeoutException("invocation time out");
		}
	}

	private void setProtocolVersion(Message msg) {
		try{
			//parentDeviceReferenceLocksupport.sharedLock();
			if (getParentDeviceReference()!=null) msg.setVersion(ProtocolVersionInfoRegistry.getInstance().get(getParentDeviceReference().getEndpointReference()));
		}finally{
			//parentDeviceReferenceLocksupport.releaseSharedLock();
		}
	}

	// ========================= INNER CLASS =========================

	protected class CallbackHandler extends DefaultResponseCallback {	//changed by ssch

		Message			msg		= null;

		FaultMessage	fault	= null;

		Operation		op		= null;

		protected CallbackHandler() {

		}

		protected CallbackHandler(Operation op) {
			this.op = op;
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultResponseCallback#handle(org.ws4d
		 * .java.communication.message.Message, org.ws4d.java.message
		 * .eventing.SubscribeResponseMessage,
		 * org.ws4d.java.communication.ProtocolData)
		 */
		@Override
		public void handle(Message request, SubscribeResponseMessage msg, ProtocolData protocolData) {
			releaseMessageSynchronization(msg);
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultResponseCallback#handle(org.ws4d
		 * .java.communication.message.Message,
		 * org.ws4d.java.message.invocation.InvokeMessage,
		 * org.ws4d.java.communication.ProtocolData)
		 */
		@Override
		public void handle(Message request, InvokeMessage msg, ProtocolData protocolData) {
			releaseMessageSynchronization(msg);
		}

		/*
		 * (non-Javadoc)
		 * @seeorg.ws4d.java.communication.DefaultResponseCallback#
		 * handleTransmissionException (org.ws4d.java.message.Message,
		 * java.lang.Exception)
		 */
		@Override
		public void handleTransmissionException(Message request, Exception exception) {
			// same as for timeouts
			handleTimeout(request);
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultResponseCallback#handle(org.ws4d
		 * .java.communication.message.Message,
		 * org.ws4d.java.message.FaultMessage,
		 * org.ws4d.java.communication.ProtocolData)
		 */
		@Override
		public void handle(Message request, FaultMessage msg, ProtocolData protocolData) {
			synchronized (this) {
				this.fault = msg;
				this.notifyAll();

			}
		}

		/*
		 * (non-Javadoc)
		 * @seeorg.ws4d.java.communication.DefaultResponseCallback#
		 * handleMalformedResponseException (org.ws4d.java.message.Message,
		 * java.lang.Exception)
		 */
		@Override
		public void handleMalformedResponseException(Message request, Exception exception) {
			// same as for timeouts
			handleTimeout(request);
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultResponseCallback#handleTimeout
		 * (org.ws4d.java.message.Message)
		 */
		@Override
		public void handleTimeout(Message request) {
			synchronized (this) {
				this.notifyAll();

			}
		}

		// ---------------------- MESSAGE HANDLING --------------------

		private void releaseMessageSynchronization(Message msg) {
			synchronized (this) {
				this.msg = msg;
				this.notifyAll();

			}
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultResponseCallback#handle(org.ws4d
		 * .java.communication.message.Message,
		 * org.ws4d.java.message.eventing.RenewResponseMessage,
		 * org.ws4d.java.communication.ProtocolData)
		 */
		@Override
		public void handle(Message request, RenewResponseMessage msg, ProtocolData protocolData) {
			releaseMessageSynchronization(msg);
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultResponseCallback#handle(org.ws4d
		 * .java.communication.message.Message, org.ws4d.java.message
		 * .eventing.UnsubscribeResponseMessage,
		 * org.ws4d.java.communication.ProtocolData)
		 */
		@Override
		public void handle(Message request, UnsubscribeResponseMessage msg, ProtocolData protocolData) {
			releaseMessageSynchronization(msg);
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultResponseCallback#handle(org.ws4d
		 * .java.communication.message.Message, org.ws4d.java.message
		 * .eventing.GetStatusResponseMessage,
		 * org.ws4d.java.communication.ProtocolData)
		 */
		@Override
		public void handle(Message request, GetStatusResponseMessage msg, ProtocolData protocolData) {
			releaseMessageSynchronization(msg);
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.ws4d.java.communication.DefaultResponseCallback#getOperation()
		 */
		@Override
		public IMessageEndpoint getOperation() {
			return op;
		}
	}

	@Override
	protected EventSourceCommons createEventSource(WSDLOperation wsdlOperation) {
		//TODO SSch Optionally implement ProxyEventSource
		return new DefaultEventSource(wsdlOperation);
	}

}

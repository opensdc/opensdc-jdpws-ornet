/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.service;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.eventing.ClientSubscription;
import org.ws4d.java.eventing.ClientSubscriptionManager;
import org.ws4d.java.eventing.EventSink;
import org.ws4d.java.eventing.EventingException;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.service.reference.Reference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.TimedEntry;
import org.ws4d.java.util.WatchDog;

/**
 * Subscription class, manages a client-side subscription.
 */
public final class DefaultClientSubscription extends TimedEntry implements ClientSubscription {

	private static final long	MAX_EXPIRATION_DURATION	= SchemaUtil.MILLIS_PER_YEAR;

	final EventSink				sink;

	final String				clientSubscriptionId;

	long						timeoutTime;

	final EndpointReference		serviceSubscriptionId;

	private ServiceReference	servRef;

	/**
	 * Constructor.
	 * 
	 * @param sink
	 * @param clientSubscriptionId
	 * @param serviceSubscriptionId
	 * @param duration
	 * @param servRef
	 */
	DefaultClientSubscription(EventSink sink, String clientSubscriptionId, EndpointReference serviceSubscriptionId, long duration, ServiceReference servRef) {
		this.sink = sink;
		this.clientSubscriptionId = clientSubscriptionId;
		this.serviceSubscriptionId = serviceSubscriptionId;
		if (duration != 0) {
			timeoutTime = System.currentTimeMillis() + duration;
			WatchDog.getInstance().register(this, duration);
		} else {
			timeoutTime = 0;
		}
		this.servRef = servRef;
	}



	@Override
	public String getClientSubscriptionId() {
		return clientSubscriptionId;
	}



	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.eventing.ClientSubscription#getTimeoutTime()
	 */
	@Override
	public long getTimeoutTime() {
		return timeoutTime;
	}

	// public URI getSubscriptionId() {
	// return serviceSubscriptionId.getReferenceParameters().getWseIdentifier();
	// }

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.eventing.ClientSubscription#getServiceSubscriptionReference
	 * ()
	 */
	@Override
	public EndpointReference getServiceSubscriptionReference() {
		return serviceSubscriptionId;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.eventing.ClientSubscription#getServiceSubscriptionId()
	 */
	@Override
	public URI getServiceSubscriptionId() {
		return serviceSubscriptionId.getReferenceParameters().getWseIdentifier();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.eventing.ClientSubscription#getEventSink()
	 */
	@Override
	public EventSink getEventSink() {
		return sink;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.eventing.ClientSubscription#getServiceReference()
	 */
	@Override
	public ServiceReference getServiceReference() {
		return servRef;
	}

	// ----------------------- SUBCRIPTION HANDLING -----------------

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.eventing.ClientSubscription#renew(long)
	 */
	@Override
	public void renew(long duration) throws EventingException, TimeoutException {
		if (servRef.getLocation() == Reference.LOCATION_REMOTE || servRef.getLocation() == Reference.LOCATION_UNKNOWN) {
			Service service = servRef.getService();
			service.renew(this, duration);
		} else {
			LocalService service = (LocalService) servRef.getService();
			service.renew(this, duration);
		}

		if (duration != 0) {
			timeoutTime = System.currentTimeMillis() + duration;
			WatchDog.getInstance().update(this, duration);
		} else {
			timeoutTime = System.currentTimeMillis() + MAX_EXPIRATION_DURATION;
			WatchDog.getInstance().update(this, MAX_EXPIRATION_DURATION);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.eventing.ClientSubscription#unsubscribe()
	 */
	@Override
	public void unsubscribe() throws EventingException, CommunicationException {
		Service service=null;
		
		try{
		if (servRef.getLocation() == Reference.LOCATION_REMOTE || servRef.getLocation() == Reference.LOCATION_UNKNOWN) {
			WatchDog.getInstance().unregister(this);
			if (servRef.isBuildUp())
			{
				service = servRef.getService();
			}
		} else {
			service = servRef.getService();
		}
		if (service!=null)
			service.unsubscribe(this);
		}finally{
			//remove subscription even if communication failed
			ClientSubscriptionManager.getInstance().removeClientSubscription(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.eventing.ClientSubscription#getStatus()
	 */
	@Override
	public long getStatus() throws EventingException, TimeoutException {
		if (servRef.getLocation() == Reference.LOCATION_REMOTE || servRef.getLocation() == Reference.LOCATION_UNKNOWN) {
			ProxyService service = (ProxyService) servRef.getService();
			return service.getStatus(this) + System.currentTimeMillis();
		} else {
			return timeoutTime;
		}
	}

	// -------------------- TimedEntry --------------------

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.management.TimedEntry#timedOut()
	 */
	@Override
	protected void timedOut() {
		sink.getEventListener().subscriptionTimeoutReceived(this);
	}

}

/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.wsdl.soap12;

import java.io.IOException;

import org.ws4d.java.communication.protocol.soap.generator.WSDLBindingBuilder;
import org.ws4d.java.constants.WSDLConstants;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.UnsupportedBindingException;
import org.ws4d.java.wsdl.WSDL;
import org.ws4d.java.wsdl.WSDLBinding;
import org.ws4d.java.wsdl.WSDLPort;
import org.xmlpull.v1.XmlPullParserException;

/**
 * 
 */
public class SOAP12DocumentLiteralHTTPBindingBuilder implements WSDLBindingBuilder {

	private SOAP12DocumentLiteralHTTPBinding	binding;

	/**
	 * 
	 */
	public SOAP12DocumentLiteralHTTPBindingBuilder() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.wsdl.WSDLBindingBuilder#getNamespace()
	 */
	@Override
	public String getNamespace() {
		return WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.wsdl.WSDLBindingBuilder#getBinding()
	 */
	@Override
	public WSDLBinding getBinding() {
		return getBindingInternal();
	}
	//SSch 2011-0-29 changed to allow extensibility
	protected SOAP12DocumentLiteralHTTPBinding getBindingInternal()
	{
		return binding;
	}
	
	//SSch 2011-0-29 Added to allow extensibility
	protected void setBindingInternal(SOAP12DocumentLiteralHTTPBinding newBinding)
	{
		this.binding=newBinding;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.wsdl.WSDLBindingBuilder#parseBindingExtension(org.ws4d
	 * .java.data.QName, org.ws4d.java.data.QName,
	 * org.ws4d.java.communication.protocol.soap.generator.ElementParser)
	 */
	@Override
	public void parseBindingExtension(QName bindingName, QName portType, ElementParser eParser) throws UnsupportedBindingException,IOException {
		//SSch create the child parser here
		eParser=new ElementParser(eParser);
		String style = SOAP12DocumentLiteralHTTPBinding.DOCUMENT_STYLE;
		String transport = null;
		int attributeCount = eParser.getAttributeCount();
		for (int i = 0; i < attributeCount; i++) {
			String attributeNamespace = eParser.getAttributeNamespace(i);
			String attributeName = eParser.getAttributeName(i);
			if ("".equals(attributeNamespace)) {
				attributeNamespace = eParser.getNamespace();
			}
			if (WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME.equals(attributeNamespace)) {
				if (WSDLConstants.WSDL_ATTRIB_STYLE.equals(attributeName)) {
					style = eParser.getAttributeValue(i);
				} else if (WSDLConstants.WSDL_ATTRIB_TRANSPORT.equals(attributeName)) {
					transport = eParser.getAttributeValue(i);
				}
			}
		}
		if (!SOAP12DocumentLiteralHTTPBinding.DOCUMENT_STYLE.equals(style)) {
			throw new UnsupportedBindingException("unsupported style: " + style);
		}
		if (!SOAP12DocumentLiteralHTTPBinding.HTTP_TRANSPORT.equals(transport)) {
			throw new UnsupportedBindingException("unsupported transport: " + transport);
		}
		
		//SSch Added to consume inner tags of the soap binding element
		try {
			eParser.consume();
		} catch (XmlPullParserException e) {
			Log.warn(e);
			throw new IOException(e.getMessage());
		}
		//SSch 2011-0-29 changed to allow extensibility
		setBindingInternal(new SOAP12DocumentLiteralHTTPBinding(bindingName, portType));
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.wsdl.WSDLBindingBuilder#parseOperationExtension(java
	 * .lang.String, org.ws4d.java.io.xml.ElementParser)
	 */
	@Override
	public void parseOperationExtension(String operationName, ElementParser parser) throws UnsupportedBindingException {
		//SSch 2011-0-29 changed to allow extensibility
		if (getBindingInternal() == null) {
			throw new UnsupportedBindingException("no acceptable binding extension processed");
		}
		String style = SOAP12DocumentLiteralHTTPBinding.DOCUMENT_STYLE;
		String soapAction = null;
		int attributeCount = parser.getAttributeCount();
		for (int i = 0; i < attributeCount; i++) {
			String attributeNamespace = parser.getAttributeNamespace(i);
			String attributeName = parser.getAttributeName(i);
			if ("".equals(attributeNamespace)) {
				attributeNamespace = parser.getNamespace();
			}
			if (WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME.equals(attributeNamespace)) {
				if (WSDLConstants.WSDL_ATTRIB_STYLE.equals(attributeName)) {
					style = parser.getAttributeValue(i);
				} else if (WSDLConstants.WSDL_ATTRIB_SOAP_ACTION.equals(attributeName)) {
					soapAction = parser.getAttributeValue(i);
				}
			}
		}
		if (!SOAP12DocumentLiteralHTTPBinding.DOCUMENT_STYLE.equals(style)) {
			//SSch 2011-0-29 changed to allow extensibility
			setBindingInternal(null);
			throw new UnsupportedBindingException("unsupported style: " + style);
		}
		if (soapAction != null) {
			getBindingInternal().setSoapAction(operationName, soapAction);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.wsdl.WSDLBindingBuilder#parseInputExtension(java.lang
	 * .String, org.ws4d.java.io.xml.ElementParser)
	 */
	@Override
	public void parseInputExtension(String inputName, ElementParser parser, String operationName) throws UnsupportedBindingException {
		parseIoTypeExtension(parser);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.wsdl.WSDLBindingBuilder#parseOutputExtension(java.
	 * lang.String, org.ws4d.java.io.xml.ElementParser)
	 */
	@Override
	public void parseOutputExtension(String outputName, ElementParser parser, String operationName) throws UnsupportedBindingException {
		parseIoTypeExtension(parser);

	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.wsdl.WSDLBindingBuilder#parseFaultExtension(java.lang
	 * .String, org.ws4d.java.io.xml.ElementParser)
	 */
	@Override
	public void parseFaultExtension(String faultName, ElementParser parser, String operationName) throws UnsupportedBindingException {
		parseIoTypeExtension(parser);

	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.wsdl.WSDLBindingBuilder#parsePortExtension(org.ws4d
	 * .java.data.QName, org.ws4d.java.data.QName,
	 * org.ws4d.java.io.xml.ElementParser)
	 */
	@Override
	public WSDLPort parsePortExtension(String portName, QName bindingName, ElementParser parser) {
		SOAP12DocumentLiteralHTTPPort port = new SOAP12DocumentLiteralHTTPPort(portName, bindingName);
		int attributeCount = parser.getAttributeCount();
		for (int i = 0; i < attributeCount; i++) {
			String attributeNamespace = parser.getAttributeNamespace(i);
			String attributeName = parser.getAttributeName(i);
			if ("".equals(attributeNamespace)) {
				attributeNamespace = parser.getNamespace();
			}
			if (WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME.equals(attributeNamespace)) {
				if (WSDLConstants.WSDL_ATTRIB_LOCATION.equals(attributeName)) {
					port.setLocation(new URI(parser.getAttributeValue(i)));
				}
			}
		}
		return port;
	}

	private void parseIoTypeExtension(ElementParser parser) throws UnsupportedBindingException {
		//SSch 2011-0-29 changed to allow extensibility
		if (getBindingInternal() == null) {
			throw new UnsupportedBindingException("no acceptable binding extension or operation extension processed");
		}
		String use = null;
		int attributeCount = parser.getAttributeCount();
		for (int i = 0; i < attributeCount; i++) {
			String attributeNamespace = parser.getAttributeNamespace(i);
			String attributeName = parser.getAttributeName(i);
			if ("".equals(attributeNamespace)) {
				attributeNamespace = parser.getNamespace();
			}
			if (WSDLConstants.SOAP12_BINDING_NAMESPACE_NAME.equals(attributeNamespace)) {
				if (WSDLConstants.WSDL_ATTRIB_USE.equals(attributeName)) {
					use = parser.getAttributeValue(i);
				}
			}
		}
		if (!SOAP12DocumentLiteralHTTPBinding.LITERAL_USE.equals(use)) {
			//SSch 2011-0-29 changed to allow extensibility
			setBindingInternal(null);
			throw new UnsupportedBindingException("unsupported use: " + use);
		}
	}

	/* (non-Javadoc)
	 * @see org.ws4d.java.communication.protocol.soap.generator.WSDLBindingBuilder#buildBinding(org.ws4d.java.types.QName, org.ws4d.java.types.QName)
	 */
	@Override
	public WSDLBinding buildBinding(QName name, QName type) {
		return buildBindingInternal(name, type);
	}

	/**
	 * @param name
	 * @param type
	 * @return
	 */
	protected WSDLBinding buildBindingInternal(QName name, QName type) {
		 return new SOAP12DocumentLiteralHTTPBinding(name,type);
	}

	/* 
	 * by SSch
	 * (non-Javadoc)
	 * @see org.ws4d.java.communication.protocol.soap.generator.WSDLBindingBuilder#setWSDL(org.ws4d.java.wsdl.WSDL)
	 */
	@Override
	public void setWSDL(WSDL wsdl) 
	{
		//void
	}

	
}

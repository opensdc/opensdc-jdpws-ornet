/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.wsdl;

import org.ws4d.java.configuration.FrameworkProperties;
import org.ws4d.java.util.Log;

/**
 *
 */
public class WSDLSupportFactoryProvider {

	private static final WSDLSupportFactoryProvider	INSTANCE=new WSDLSupportFactoryProvider();

	private WSDLSupportFactoryProvider() {
		WSDLSupportFactory factory = null;
		String factoryClassName = FrameworkProperties.getInstance().getWsdlSupportFactoryClass();
		if (factoryClassName == null) {
			// use default one silently
			factoryClassName = "org.ws4d.java.wsdl.DPWSWSDLSupportFactory";
		}
		try {
			Class<?> factoryClass = Class.forName(factoryClassName);
			factory = (WSDLSupportFactory) factoryClass.newInstance();
			if (Log.isDebug()) {
				Log.debug("Using WSDLSupportFactory [" + factoryClassName + "]");
			}
		} catch (ClassNotFoundException e) {
			Log.error("Configured WSDLSupportFactory class [" + factoryClassName + "] not found!");
			//				factory = new WSDLSupportFactoryProvider();
		} catch (Exception ex) {
			Log.error("Unable to create instance of configured WSDLSupportFactory class [" + factoryClassName + "]!");
			Log.error(ex);
			//				factory = new WSDLSupportFactoryProvider();
		}
		this.factory = factory;
	}

	private WSDLSupportFactory factory=null;

	/**
	 * @return
	 */
	public static WSDLSupportFactoryProvider getInstance() {
		return INSTANCE;
	}

	public WSDLSupportFactory getFactory() {
		return factory;
	}



}

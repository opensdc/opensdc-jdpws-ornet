/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.wsdl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.ws4d.java.io.fs.FileSystem;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.structures.ArrayList;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.ReadOnlyIterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlPullParserException;

/**
 * This class implements the WSDL Manager for handling predefined WSDLs in a so
 * called WSDL Repository.
 */
public class WSDLRepository {

	// TODO: Remove this after OSAMi Demonstation!
	public static final boolean			DEMO_MODE			= false;

	private static final String			DEMO_WSDL_COMMON	= "OSAmICommonSensorService.wsdl";

	private static final String			DEMO_WSDL_PULSE		= "OSAmIPulseService.wsdl";

	private static final String			DEMO_WSDL_O2		= "OSAmISpO2Service.wsdl";

	// TODO SSch extract as framework(?) property
	private static final String			REPO_PATH			= "wsdl_repo";

	private static final WSDLRepository	INSTANCE			= new WSDLRepository();

	private final FileSystem			fs;

	/*
	 * key = port type as QName, value = wsdl file location within repository as
	 * String
	 */
	private final HashMap				index				= new HashMap();

	public static String getRepoPath() {
		return REPO_PATH;
	}

	public static WSDLRepository getInstance() {
		return INSTANCE;
	}

	public static WSDL loadWsdl(URI wsdlUri) throws IOException {
		try {
			return WSDL.parse(wsdlUri);
		} catch (IOException e) {
			Log.error("Unable to obtain WSDL from " + wsdlUri + ": " + e.getMessage());
			Log.error(e);
			throw e;
		} catch (XmlPullParserException e) {
			Log.error("Ill formatted WSDL from " + wsdlUri + ": " + e.getMessage());
			Log.error(e);
			throw new IOException(e.getMessage());
		}
	}

	private WSDLRepository() {
		super();
		FileSystem fs = null;
		try {
			fs = PlatformSupport.getInstance().getToolkit().getLocalFileSystem();
		} catch (IOException e) {
			/*
			 * no file system available within current runtime or framework not
			 * started
			 */
			Log.error("No local file system available, WSDL repository will not work.");
		}
		this.fs = fs;
		if (fs != null) {
			initStore();
		}
	}

	public InputStream getWsdlInputStream(QName portType) {
		if (fs == null) {
			return null;
		}
		String wsdlFilePath = (String) index.get(portType);
		if (wsdlFilePath == null) {
			return null;
		}
		try {
			return fs.readFile(wsdlFilePath);
		} catch (IOException e) {
			Log.error("Unable to read WSDL file " + wsdlFilePath + ": " + e.getMessage());
		}
		return null;
	}

	public WSDL getWSDL(QName portType) {
		try {
			InputStream in = getWsdlInputStream(portType);
			if (in == null) return null;
			WSDL wsdl = WSDL.parse(in);
			if (DEMO_MODE) {
				Iterator it = index.keySet().iterator();
				while (it.hasNext()) {
					QName pt = (QName) it.next();
					Log.debug("PORTTYPE: " + pt);
				}
				String wsdlFilePath = (String) index.get(portType);
				String fileName = wsdlFilePath.substring(REPO_PATH.length() + fs.fileSeparator().length());
				if (Log.isDebug())
					Log.debug("OSAMi Demomode: LOAD " + fileName);
				
				if (fileName != null && fileName.equals(DEMO_WSDL_COMMON)) {
					/*
					 * get pulse WSDL
					 */
					WSDL pulse = getWSDLFromFile(DEMO_WSDL_PULSE);
					if (pulse != null) {
						pulse.addLinkedWsdl(wsdl);
					}
				} else if (fileName != null && fileName.equals(DEMO_WSDL_O2)) {
					/*
					 * get O2 WSDL
					 */
					WSDL common = getWSDLFromFile(DEMO_WSDL_COMMON);
					if (common != null) {
						wsdl.addLinkedWsdl(common);
					}
				} else if (fileName != null && fileName.equals(DEMO_WSDL_PULSE)) {
					/*
					 * get sensor WSDL
					 */
					WSDL common = getWSDLFromFile(DEMO_WSDL_COMMON);
					if (common != null) {
						wsdl.addLinkedWsdl(common);
					}
				}
			}
			in.close();
			if (Log.isDebug())
				Log.debug("WSDL: " + wsdl.toString());
			
			return wsdl;
		} catch (XmlPullParserException e) {
			Log.error("Ill formatted WSDL file " + index.get(portType) + ": " + e.getMessage());
		} catch (IOException e) {
			Log.error("Unable to read WSDL file " + index.get(portType) + ": " + e.getMessage());
		}
		return null;
	}

	public Iterator getPortTypes() {
		return new ReadOnlyIterator(index.keySet());
	}

	public WSDL loadAndStore(URI fromUri, String fileName) throws IOException {
		WSDL wsdl = loadWsdl(fromUri);
		if (wsdl != null) {
			// TODO make file name unique
			store(wsdl, fileName);
		}
		return wsdl;
	}

	public WSDL loadAndStore(InputStream in, String fileName) throws IOException {
		WSDL wsdl = null;
		try {
			wsdl = WSDL.parse(in);
			in.close();
			if (wsdl != null) {
				// TODO make file name unique
				store(wsdl, fileName);
			}
		} catch (XmlPullParserException e) {
			Log.error("Ill formatted WSDL file: " + e.getMessage());
		}
		return wsdl;
	}

	public WSDL loadAndStore(URI wsdlUri) throws IOException {
		return loadAndStore(wsdlUri, wsdlUri.toString());
	}

	public void store(WSDL wsdl, String fileName) {
		if (DEMO_MODE) {
			Log.debug("OSAMi Demomode: STORE " + fileName);
			if (fileName != null && fileName.equals(DEMO_WSDL_COMMON)) {
				/*
				 * get pulse WSDL
				 */
				WSDL pulse = getWSDLFromFile(DEMO_WSDL_PULSE);
				if (pulse != null) {
					pulse.addLinkedWsdl(wsdl);
				}
				WSDL o2 = getWSDLFromFile(DEMO_WSDL_O2);
				if (o2 != null) {
					o2.addLinkedWsdl(wsdl);
				}
			} else if (fileName != null && fileName.equals(DEMO_WSDL_O2)) {
				/*
				 * get O2 WSDL
				 */
				WSDL common = getWSDLFromFile(DEMO_WSDL_COMMON);
				if (common != null) {
					wsdl.addLinkedWsdl(common);
				}
			} else if (fileName != null && fileName.equals(DEMO_WSDL_PULSE)) {
				/*
				 * get sensor WSDL
				 */
				WSDL common = getWSDLFromFile(DEMO_WSDL_COMMON);
				if (common != null) {
					wsdl.addLinkedWsdl(common);
				}
			}
		}
		String filePath = REPO_PATH + fs.fileSeparator() + fs.escapeFileName(fileName);
		try {
			OutputStream out = fs.writeFile(filePath);
			wsdl.serialize(out);
			out.close();
			index(wsdl, filePath);
		} catch (IOException e) {
			Log.error("Unable to write to WSDL file " + filePath + ": " + e.getMessage());
		}
	}

	public WSDL getWSDLFromFile(String fileName) {
		String filePath = REPO_PATH + fs.fileSeparator() + fileName;
		try {
			InputStream in = fs.readFile(filePath);
			try {
				WSDL wsdl = WSDL.parse(in);
				return wsdl;
			} catch (XmlPullParserException e) {
				Log.error("Ill formatted WSDL file " + filePath + ": " + e.getMessage());
			} finally {
				in.close();
			}
		} catch (IOException e) {
			Log.error("Unable to read WSDL file " + filePath + ": " + e.getMessage());
		}
		return null;
	}

	public void delete(QName portType) {
		fs.deleteFile((String) index.get(portType));
		updateStore();
	}

	/**
	 * 
	 */
	public void updateStore() {
		String[] knownWsdlFiles = fs.listFiles(REPO_PATH);
		if (knownWsdlFiles == null) {
			index.clear();
			return;
		}
		for (int i = 0; i < knownWsdlFiles.length; i++) {
			String wsdlFile = knownWsdlFiles[i];
			String filePath = REPO_PATH + fs.fileSeparator() + wsdlFile;

			// add
			if (!index.containsKey(filePath)) {
				try {
					InputStream in = fs.readFile(filePath);
					try {
						WSDL wsdl = WSDL.parse(in);
						in.close();
						index(wsdl, filePath);
					} catch (XmlPullParserException e) {
						Log.error("Ill formatted WSDL file " + filePath + ": " + e.getMessage());
					}
				} catch (IOException e) {
					Log.error("Unable to read WSDL file " + filePath + ": " + e.getMessage());
				}
			}
		}
		ArrayList l = new ArrayList();
		// remove
		Iterator itPortType = index.keySet().iterator();
		while (itPortType.hasNext()) {
			QName portType = (QName) itPortType.next();
			int i;
			for (i = 0; i < knownWsdlFiles.length; i++) {
				if ((REPO_PATH + fs.fileSeparator() + knownWsdlFiles[i]).equals(index.get(portType))) break;
			}
			if (i == knownWsdlFiles.length) l.add(portType);
		}
		for (Iterator it = l.iterator(); it.hasNext();)
			index.remove(it.next());
	}

	/**
	 * @param wsdl
	 * @param filePath
	 */
	private void index(WSDL wsdl, String filePath) {
		for (Iterator it = wsdl.getSupportedPortTypes().iterator(); it.hasNext();) {
			WSDLPortType portType = (WSDLPortType) it.next();
			index.put(portType.getName(), filePath);
		}
	}

	private void initStore() {
		String[] knownWsdlFiles = fs.listFiles(REPO_PATH);
		if (knownWsdlFiles == null) {
			return;
		}
		for (int i = 0; i < knownWsdlFiles.length; i++) {
			String wsdlFile = knownWsdlFiles[i];
			String filePath = REPO_PATH + fs.fileSeparator() + wsdlFile;
			try {
				InputStream in = fs.readFile(filePath);
				try {
					WSDL wsdl = WSDL.parse(in);
					in.close();
					index(wsdl, filePath);
				} catch (XmlPullParserException e) {
					Log.error("Ill formatted WSDL file " + filePath + ": " + e.getMessage());
				}
			} catch (IOException e) {
				Log.error("Unable to read WSDL file " + filePath + ": " + e.getMessage());
			}
		}
	}

}

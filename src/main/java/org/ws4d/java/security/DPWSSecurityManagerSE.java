/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.security;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Enumeration;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.ws4d.java.communication.CommunicationBinding;
import org.ws4d.java.communication.HTTPSBinding;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.tcp.SecureSocketFactorySE;
import org.ws4d.java.communication.connection.tcp.ServerSocket;
import org.ws4d.java.communication.connection.tcp.Socket;
import org.ws4d.java.configuration.Properties;
import org.ws4d.java.configuration.SecurityProperties;
import org.ws4d.java.constants.HTTPConstants;
import org.ws4d.java.dispatch.DeviceServiceRegistryProvider;
import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.io.xml.XmlSerializer;
import org.ws4d.java.io.xml.canonicalization.CanonicalSerializer;
import org.ws4d.java.message.Message;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.StringUtil;

public class DPWSSecurityManagerSE implements SecurityManagerModule {

	private final String destinationAddressFilter="239.255.255.250@3702"; //Added by SSch

	private boolean isWrapInputStreamEnabled=Boolean.parseBoolean(System.getProperty("DPWS.DPWSSecurityManagerSE", "false"));
	
	private HashMap						protocolDataToInputStream	= new HashMap();

	private SecurityProperties	secProp;

	public DPWSSecurityManagerSE() {

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.security.DPWSSecurityManager#getBodySignature(org.ws4d.
	 * java.io.xml.XmlSerializer, org.ws4d.java.message.Message)
	 */
	@Override
	public String getBodySignature(XmlSerializer serial, Message msg) {
		CanonicalSerializer cs = (CanonicalSerializer) serial;
		byte[] arry = cs.bodyPart();
		byte[] signArray = getSignature(arry, (PrivateKey) msg.getPrivateKey());

		return Base64Util.encodeBytes(signArray);
	}

	private byte[] getSignature(byte[] rawxml, PrivateKey pk) {
		try {

			// Compute signature
			Signature instance = Signature.getInstance("SHA1withRSA");
			instance.initSign(pk);
			MessageDigest digest = MessageDigest.getInstance("sha1");

			// calculate digest of the one and only part
			rawxml = digest.digest(rawxml);

			// generate SignedInfo part and calculate its digest
			byte[][] digs = new byte[1][];
			digs[0] = rawxml;
			rawxml = generateSignedInfo(digs, new String[] { SecurityManagerModule.bodyPartID });

			rawxml = digest.digest(rawxml);

			// sign the SignedInfo parts digest
			instance.update(rawxml);

			byte[] signature = instance.sign();

			return signature;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void releaseMessage(ProtocolData pd)
	{

		protocolDataToInputStream.remove(pd);
		if (Log.isDebug())
		{
			Log.debug("Release IDawareInputStream for "+pd+". Size="+protocolDataToInputStream.size());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.security.DPWSSecurityManager#validateMessage(byte[],
	 * org.ws4d.java.communication.ProtocolData,
	 * org.ws4d.java.types.EndpointReference, java.lang.String[])
	 */
	@Override
	public boolean validateMessage(byte[] signature, ProtocolData pd, EndpointReference epr, String[] aliasCanditates) {
		try {
			//Bugfix SSch: remove from map even if signedparts ==null
			IDawareInputStream bbis = ((IDawareInputStream) (protocolDataToInputStream.get(pd)));
			releaseMessage(pd);

			byte[][] signedParts = bbis.getPartsByteArrays();
			if (signedParts == null) {
				Log.error("Message validation failed because the referred sections cound not be extracted!");
				return false;
			}

			Certificate cert = null;

			DeviceReference dRef = null;
			ServiceReference sRef = null;

			if ((dRef = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().getStaticDeviceReference(epr, false)) != null) {
				try {
					cert = (Certificate) dRef.getDevice().getCertificate();
				} catch (TimeoutException e) {
					e.printStackTrace();
				}
			} else if ((sRef = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().getStaticServiceReference(epr, false)) != null) {
				try {
					cert = (Certificate) sRef.getService().getCertificate();
				} catch (TimeoutException e) {
					e.printStackTrace();
				}
			}

			for (int i = 0; cert == null && aliasCanditates != null && i < aliasCanditates.length; i++) {
				cert = (Certificate) getCertificate(aliasCanditates[i]);
			}

			if (cert == null) {
				cert = (Certificate) getCertificate(epr.getAddress().toString());
				if (dRef != null) {
					dRef.getDevice().setCertificate(cert);
				} else if (sRef != null) {
					sRef.getService().setCertificate(cert);
				}
			}

			if (cert == null) {
				Log.error("Security: device/service uuid '" + epr.getAddress() + "' not found in the specified keystore!");
				return false;
			}

			// calculating digests over all parts
			MessageDigest digest = MessageDigest.getInstance("sha1");
			byte[][] digests = new byte[signedParts.length][];
			for (int i = 0; i < signedParts.length; i++) {
				digests[i] = digest.digest(signedParts[i]);
			}

			byte[] signedInfo = generateSignedInfo(digests, bbis.getIds());

			// the digest of the signedInfo element
			signedInfo = digest.digest(signedInfo);

			// sign that digest
			PublicKey pk = cert.getPublicKey();
			Signature s = Signature.getInstance("SHA1withRSA");
			s.initVerify(pk);
			s.update(signedInfo);
			if (s.verify(signature)) {
				Log.info("Discovery-Message validated!");
				return true;
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Log.warn("Discovery-Message could not be validated!");
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.security.DPWSSecurityManager#getMD5Hash(java.lang.String)
	 */
	@Override
	public long getMD5Hash(String str) {
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		m.update(str.getBytes(), 0, str.length());
		return new BigInteger(1, m.digest()).longValue();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.security.DPWSSecurityManager#getPrivateKey(java.lang.String
	 * , java.lang.String)
	 */
	@Override
	public Object getPrivateKey(String privKey, String pswd) {

		try {
			KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());

			FileInputStream fis = new FileInputStream(getSecProp().getKeyStoreFilePath());
			ks.load(fis, getSecProp().getKeyStorePswd().toCharArray());
			fis.close();

			PrivateKey pk = null;

			if (pswd == null) {
				pswd = SecurityProperties.getInstance().getKeyStorePswd();
			}
			if (pswd == null) throw new KeyStoreException("Could not fetch private key. Password not found.");

			try {
				pk = (PrivateKey) ks.getKey(privKey, pswd.toCharArray());
			} catch (UnrecoverableKeyException e) {
				// If there is no key the user should create one!
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return pk;
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.security.DPWSSecurityManager#getCertificate(java.lang.String
	 * )
	 */
	@Override
	@SuppressWarnings("resource")
	public Object getCertificate(String certAlias) {

		try {
			Certificate cert = null;
			KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
			if (SecurityProperties.getInstance().getTrustStorePath() == null && SecurityProperties.getInstance().getKeyStoreFilePath() == null) return null;

			FileInputStream fis = (getSecProp().getTrustStorePath() != null ? new FileInputStream(getSecProp().getTrustStorePath()) : new FileInputStream(getSecProp().getKeyStoreFilePath()));
			ks.load(fis, (getSecProp().getTrustStorePasswd() != null ? getSecProp().getTrustStorePasswd() : getSecProp().getKeyStorePswd()).toCharArray());
			fis.close();

			String nearestAlias = certAlias;
			int lastIndex = -1;

			while (nearestAlias.length() > 1) {
				if ((cert = ks.getCertificate(nearestAlias.toLowerCase())) != null) break;
				nearestAlias = (lastIndex = nearestAlias.indexOf('/')) < 0 ? "" : nearestAlias.substring(lastIndex + 1);
			}

			return cert;
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			Log.error("Security: Could not get keystore!");
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.security.DPWSSecurityManager#wrapInputStream(java.io.
	 * InputStream, org.ws4d.java.communication.ProtocolData)
	 */

	@Override
	public InputStream wrapInputStream(InputStream in, ProtocolData pd) {
		InputStream retVal=in;
		if (pd!=null && isWrapInputStreamEnabled())
		{
			if (destinationAddressFilter.equals(pd.getDestinationAddress()))
			{
				if (Log.isDebug())
					Log.debug("Create IDawareInputStream for "+pd);

				IDawareInputStream bbio = new IDawareInputStream(in, null);
				protocolDataToInputStream.put(pd, bbio);
				retVal=bbio;
			}
		}
		return retVal;
	}
	
	/* (non-Javadoc)
	 * @see org.ws4d.java.framework.module.SecurityManagerModule#isWrapInputStreamEnabled()
	 */
	@Override
	public boolean isWrapInputStreamEnabled() {
		return isWrapInputStreamEnabled;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.security.DPWSSecurityManager#getNewCanonicalSerializer()
	 */
	@Override
	public XmlSerializer getNewCanonicalSerializer(String id) {
		return new CanonicalSerializer(id);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.security.DPWSSecurityManager#getSecureServerSocket(java
	 * .lang.String, int, java.lang.String)
	 */
	@Override
	public ServerSocket getSecureServerSocket(IPAddress adr, int port, String alias){
		try {
			return SecureSocketFactorySE.createServerSocket(adr, port, alias);
		} catch (IOException e) {
			Log.info(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.security.DPWSSecurityManager#getSecureSocket(org.ws4d.java
	 * .types.URI)
	 */
	@Override
	public Socket getSecureSocket(URI location, String alias) {
		try {
			return SecureSocketFactorySE.createSocket(new IPAddress(location.getHost()), location.getPort(), alias);
		} catch (IOException e) {
			Log.info(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.security.DPWSSecurityManager#getSecureSocket(java.lang.
	 * String, int, java.lang.String)
	 */
	@Override
	public Socket getSecureSocket(IPAddress host, int port, String alias){
		try {
			return SecureSocketFactorySE.createSocket(host, port, alias);
		} catch (IOException e) {
			Log.info(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.security.DPWSSecurityManager#decode(java.lang.String)
	 */
	@Override
	public byte[] decode(String base64enc) {
		return Base64Util.decode(base64enc);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.security.DPWSSecurityManager#isHTTPS(org.ws4d.java.types
	 * .URI)
	 */
	@Override
	public boolean isHTTPS(URI uri) {
		// return uri.getSchema().equalsIgnoreCase(HTTPConstants.HTTPS_SCHEMA);
		return StringUtil.equalsIgnoreCase(uri.getSchema(), HTTPConstants.HTTPS_SCHEMA);
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.security.DPWSSecurityManager#getKeyManagers()
	 */
	@Override
	public Object[] getKeyManagers() throws IOException, GeneralSecurityException {
		if (SecurityProperties.getInstance().getKeyStoreFilePath() == null) return null;
		FileInputStream fis = new FileInputStream(SecurityProperties.getInstance().getKeyStoreFilePath());
		KeyStore ks = KeyStore.getInstance("jks");
		String keyStorePswd=SecurityProperties.getInstance().getKeyStorePswd();
		if (keyStorePswd==null)
			keyStorePswd="";

		ks.load(fis, keyStorePswd.toCharArray());
		fis.close();

		String alg = KeyManagerFactory.getDefaultAlgorithm();
		KeyManagerFactory kmFact = KeyManagerFactory.getInstance(alg);
		String keyPswd = SecurityProperties.getInstance().getKeyPswd();
		if (keyPswd==null)
			keyPswd=keyStorePswd;

		if (Log.isDebug())
		{
			Log.debug("KeyStore Content (KeyManagers):");
			Enumeration<String> aliases = ks.aliases();
			while(aliases.hasMoreElements())
				Log.debug(aliases.nextElement());
		}

		kmFact.init(ks, keyPswd.toCharArray());

		KeyManager[] kms = kmFact.getKeyManagers();
		return kms;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.security.DPWSSecurityManager#getTrustManagers()
	 */
	@Override
	public Object[] getTrustManagers() throws IOException, GeneralSecurityException {
		String alg = TrustManagerFactory.getDefaultAlgorithm();
		TrustManagerFactory tmFact = TrustManagerFactory.getInstance(alg);
		if (SecurityProperties.getInstance().getTrustStorePath() == null) return null;

		FileInputStream fis = new FileInputStream(SecurityProperties.getInstance().getTrustStorePath());
		KeyStore ks = KeyStore.getInstance("jks");
		String trustStorePasswd = SecurityProperties.getInstance().getTrustStorePasswd();
		if (trustStorePasswd==null)
			trustStorePasswd="";
		
		ks.load(fis, trustStorePasswd.toCharArray());
		fis.close();
		
		if (Log.isDebug())
		{
			Log.debug("KeyStore Content (TrustStore):");
			Enumeration<String> aliases = ks.aliases();
			while(aliases.hasMoreElements())
				Log.debug(aliases.nextElement());
		}


		tmFact.init(ks);

		TrustManager[] tms = tmFact.getTrustManagers();
		return tms;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.ws4d.java.security.DPWSSecurityManager#getAliasFromBinding(org.ws4d
	 * .java.communication.HTTPBinding)
	 */
	@Override
	public String getAliasFromBinding(CommunicationBinding binding) {
		String alias=null;
		if (binding instanceof HTTPSBinding)
		{
			alias= ((HTTPSBinding) binding).getAlias();
		}
		return alias;
	}

	/**
	 * This element will only be used internally. It will be gennerated digested
	 * and this digest will be signed
	 * 
	 * @param digs the signatures
	 * @param refs the reference ids of the digested parts of the xml message.
	 * @return the byte array of the signed info element. Ready to be signed.
	 */
	private static byte[] generateSignedInfo(byte[][] digs, String[] refs) {
		String signatureInfo = "<SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml_exc-c14n#\" /> " + "<SignatureMethod Algorithm=\"htt://www.w3.org/2000/09/xmldsig#rsa-sha1\" />";
		for (int i = 0; i < digs.length; i++) {
			signatureInfo += "<Reference URI=\"#" + refs[i] + "\" ><Transforms><Transform Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\" />" + "</Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\" />" + "<DigestValue>" + Base64Util.encodeBytes(digs[i]) + "</DigestValue></Reference>";
		}
		signatureInfo += "</SignedInfo>";
		return signatureInfo.getBytes();
	}


	/*
	 * (non-Javadoc)
	 * @see org.ws4d.java.security.DPWSSecurityManager#encode(byte[])
	 */
	@Override
	public String encode(byte[] raw) {
		return Base64Util.encodeBytes(raw);
	}

	@Override
	public void registerPropertiesHandler(Properties properties) 
	{
		//void
	}

	@Override
	public String getAliasForLocation(URI location) 
	{
		String aliasForLocation=null;
		if (getSecProp()!=null)
		{
			aliasForLocation=getSecProp().getClientAlias();
		}
		return aliasForLocation;
	}

	protected synchronized SecurityProperties getSecProp() {
		if (secProp==null)
			secProp=SecurityProperties.getInstance();

		return secProp;
	}

	public static void main(String[] args) {
		DPWSSecurityManagerSE secMan=new DPWSSecurityManagerSE();
		SecurityProperties.getInstance().setKeyStoreFileName("C:\\Documents and Settings\\schlichs\\My Documents\\Projekte\\DSC\\XCA_Keys\\Stores\\KeyStore.jks");
		SecurityProperties.getInstance().setKeyStorePaswd("sdummy");
		SecurityProperties.getInstance().setKeyPaswd("cdummy");
		try {
			secMan.getKeyManagers();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}




}
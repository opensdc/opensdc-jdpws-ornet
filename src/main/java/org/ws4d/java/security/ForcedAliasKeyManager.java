/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.security;

import java.net.Socket;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509KeyManager;

import org.ws4d.java.util.Log;

/**
 * The ForcedAliasKeyManager uses the next available alias from the keystore
 * with the most similarity to the given alias. E.g. if the given alias is
 * "https://example.device/test/beta" and there is no such alias in the store
 * the alias "example.device/test/beta" will be searched for instead.
 */
public class ForcedAliasKeyManager implements X509KeyManager {

	private X509KeyManager	baseKM;

	private String			alias;

	public ForcedAliasKeyManager(X509KeyManager baseKM, String alias) {
		this.baseKM = baseKM;
		this.alias = alias;
	}

	@Override
	public String chooseClientAlias(String[] keyType, Principal[] issuers, Socket socket) {
		// For each keyType, call getClientAliases on the base KeyManager
		// to find valid aliases. If our requested alias is found, select it
		// for return.
		boolean aliasFound = false;
		String nearestAlias = alias;

		while (nearestAlias.length() > 1 && !aliasFound) {
			for (int i = 0; i < keyType.length && !aliasFound; i++) {
				String[] validAliases = baseKM.getClientAliases(keyType[i], issuers);
				if (validAliases != null) {
					for (int j = 0; j < validAliases.length && !aliasFound; j++) {
						if (validAliases[j].toLowerCase().equals(nearestAlias.toLowerCase())) aliasFound = true;
					}
				}
			}

			int lastIndex = -1;
			if (aliasFound){
				nearestAlias = (lastIndex = nearestAlias.lastIndexOf('/')) < 0 ? nearestAlias : nearestAlias.substring(lastIndex + 1);
			}
		}

		if (Log.isInfo())
			Log.info("Nearest Client Alias:"+nearestAlias+" Alias:"+this.alias);
		
		if (aliasFound) {
			return nearestAlias;
		} else
			return null;
	}

	@Override
	public String chooseServerAlias(String keyType, Principal[] issuers, Socket socket) {
		//Changed by SSch, KSe - We want to use the alias if we have defined one
		//return baseKM.chooseServerAlias(keyType, issuers, socket);

		String nearestAlias=null;
		if (this.alias!=null)
			nearestAlias=this.alias.toLowerCase(); //CAVE: alias in the keystore is always lower case

		boolean aliasFound = false;

		if (nearestAlias!=null)
		{
			while (nearestAlias.length() > 1 && !aliasFound) 
			{
				String validAliases = baseKM.chooseServerAlias(keyType, issuers, socket);
				if (validAliases != null && validAliases.toLowerCase().equals(nearestAlias.toLowerCase())) aliasFound = true;
				
				int lastIndex = -1;
				if (!aliasFound){
					nearestAlias = (lastIndex = nearestAlias.lastIndexOf('/')) < 0 ? nearestAlias : nearestAlias.substring(lastIndex + 1);
				}
			}


		}
		if (Log.isInfo())
			Log.info("Nearest Server Alias:"+nearestAlias+" Alias:"+this.alias);
		
	return nearestAlias;


}

@Override
public X509Certificate[] getCertificateChain(String arg0) {
	return baseKM.getCertificateChain(arg0);
}

@Override
public String[] getClientAliases(String keyType, Principal[] issuers) {
	return baseKM.getClientAliases(keyType, issuers);
}

@Override
public PrivateKey getPrivateKey(String arg0) {
	return baseKM.getPrivateKey(arg0);
}

@Override
public String[] getServerAliases(String keyType, Principal[] issuers) {
	return baseKM.getServerAliases(keyType, issuers);
}

}

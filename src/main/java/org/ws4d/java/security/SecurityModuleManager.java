/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.security;

import org.ws4d.java.framework.module.SecurityManagerModule;
import org.ws4d.java.platform.PlatformSupport;

/**
 * 
 * TODO SSch Check
 * @author ssch
 */
public class SecurityModuleManager {
	public static final SecurityModuleManager instance=new SecurityModuleManager();
	
	private SecurityModuleManager()
	{
		//void
	}

	public static SecurityModuleManager getInstance() {
		return instance;
	}
	
	public SecurityManagerModule getSecurityManager(){
		return (SecurityManagerModule) PlatformSupport.getInstance().getToolkit().getSecurityManager();
	}
}

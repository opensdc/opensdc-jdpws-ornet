/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/

package org.ws4d.java.util;

import org.ws4d.java.structures.Iterator;
import org.ws4d.java.structures.LinkedSet;
import org.ws4d.java.structures.Set;

/**
 * Class to memorize time information. Override timedOut() method to do
 * something at time out time.
 * 
 * @author mspies
 */
public abstract class TimedEntry {

	protected Set relatedEntries=new LinkedSet();
	protected Object relationshipKey=null;
	
	protected long	timeToRemove;

	protected boolean	disabled	= false;

	// used for timed entries which are unregistered and reregistered
	protected boolean	registered	= false;

	/**
	 * Sets timer, when this timed entry should be timed out and removed.
	 */
	void setTimer(final long timeUntilTimeout) {
		this.timeToRemove = timeUntilTimeout + System.currentTimeMillis();
	}

	/**
	 * Compares this TimedEntry with the specified TimedEntry for order. Returns
	 * a negative long, zero, or a positive long as this TimedEntry is less
	 * than, equal to, or greater than the specified TimedEntry.
	 * 
	 * @param other
	 * @return
	 */
	long compareTo(TimedEntry other) {
		return timeToRemove - other.timeToRemove;
	}
	
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * Runs what should be done at time out. Will be called by the Watchdog at
	 * time out.
	 */
	protected abstract void timedOut();

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
//	public String toString() {
//		return "(TimeToRemove: " + timeToRemove + ")";
//	}
	
	
	
	public synchronized void addRelatedEntry(TimedEntry relatedEntry)
	{
		relatedEntries.add(relatedEntry);
	}
	
	@Override
	public String toString() {
		return "TimedEntry [relationshipKey=" + relationshipKey
				+ ", timeToRemove=" + timeToRemove + ", disabled=" + disabled
				+ ", registered=" + registered + "]";
	}

	public void clearRelatedEntries()
	{
		relatedEntries.clear();
	}
	
	public Iterator getRelatedEntries()
	{
		return relatedEntries.iterator();
	}
	
	public int getRelatedEntriesCount()
	{
		return relatedEntries.size();
	}
	
	public Object getRelationshipKey()
	{
		return relationshipKey;
	}
	
	public void setRelationshipKey(Object key)
	{
		relationshipKey=key;
	}
}

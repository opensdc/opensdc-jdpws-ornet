/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.util;

import java.lang.Math;

public class StatisticsHelper
{
    private long cnt = 0;
    private long last = 0;
    private int updatePeriodTest = 200;
    private int modulo = 30;
    private int resetAfter=500;
    private boolean resetDone=false;
    public static final double nanoToMilli = Math.pow(10, 6);
    private double updatePeriodN = updatePeriodTest * nanoToMilli;
    private final String id;
    private RunningStat rStatReal = new RunningStat();
    private RunningStat rStatRealM = new RunningStat();
    private RunningStat rStatRel = new RunningStat();
    private RunningStat rStatRelM = new RunningStat();
	private final boolean logToConsoleEnabled=Boolean.parseBoolean(System.getProperty("Platform.StatisticsHelper.LogToConsoleEnabled", "false"));
//	private final boolean logToCSVEnabled=Boolean.parseBoolean(System.getProperty("Platform.StatisticsHelper.LogToConsoleEnabled", "false"));

    public StatisticsHelper(String id)
    {
        this.id = id;

//        try
//        {
//        	if (logToCSVEnabled)
//        	{
//        		File file = new File(String.format("%s%s_STAT_%s.csv", System.getProperty("CSV.Directory", ""), id, System.currentTimeMillis()));
////        		Writer fileWriter = new FileWriter(file);
//        	}
//            //TODO SSch add CSVWriter
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }
    }

    public int getUpdatePeriodTest()
    {
        return updatePeriodTest;
    }

    public void setUpdatePeriodTest(int updatePeriodTest)
    {
        this.updatePeriodTest = updatePeriodTest;
        this.updatePeriodN = updatePeriodTest * nanoToMilli;
    }

    public void computeStatistics()
    {
        if (cnt < 0)
        {
            cnt++;
            return;
        }
        
        long current = System.nanoTime();

        if (last > 0)
        {
            cnt++;
            long d = (current - last); // time since last
            rStatReal.push(d);
            rStatRealM.push(d);
            double t = d - updatePeriodN; // normalized time since last invoke (deviation from expected time)
            rStatRel.push(t);
            rStatRelM.push(t);
        }
        last = current;
        if (cnt % modulo == 0 && cnt > 0)
        {
        	
            log(this.id + ";" + cnt + ";" + new java.util.Date() + ";Real;" + rStatReal.toStringNanoToMilli() + ";RealM;" + rStatRealM.toStringNanoToMilli() + ";Rel;" + rStatRel.toStringNanoToMilli() + ";RelM;" + rStatRelM.toStringNanoToMilli());

            @SuppressWarnings("unused")
			String[] fields = new String[] { String.valueOf(cnt), String.valueOf(System.currentTimeMillis()), String.valueOf((long) (rStatReal.mean() / nanoToMilli)), String.valueOf((long) (rStatReal.variance() / Math.pow(nanoToMilli, 2))), String
                            .valueOf((long) (rStatReal.standardDeviation() / nanoToMilli)), String.valueOf((long) (rStatReal.min() / nanoToMilli)), String.valueOf((long) (rStatReal.max() / nanoToMilli)), String.valueOf((long) (rStatRealM.mean() / nanoToMilli)), String.valueOf((long) (rStatRealM
                            .variance() / Math.pow(nanoToMilli, 2))), String.valueOf((long) (rStatRealM.standardDeviation() / nanoToMilli)), String.valueOf((long) (rStatRealM.min() / nanoToMilli)), String.valueOf((long) (rStatRealM.max() / nanoToMilli)), String
                            .valueOf((long) (rStatRel.mean() / nanoToMilli)), String.valueOf((long) (rStatRel.variance() / Math.pow(nanoToMilli, 2))), String.valueOf((long) (rStatRel.standardDeviation() / nanoToMilli)), String.valueOf((long) (rStatRel.min() / nanoToMilli)), String
                            .valueOf((long) (rStatRel.max() / nanoToMilli)), String.valueOf((long) (rStatRelM.mean() / nanoToMilli)), String.valueOf((long) (rStatRelM.variance() / Math.pow(nanoToMilli, 2))), String.valueOf((long) (rStatRelM.standardDeviation() / nanoToMilli)), String
                            .valueOf((long) (rStatRelM.min() / nanoToMilli)), String.valueOf((long) (rStatRelM.max() / nanoToMilli)) };

            rStatRelM.clear();
            rStatRealM.clear();
            // rStatAbsRelM.clear();
            
        }
        if (!resetDone &&  cnt > 0 && cnt>resetAfter)
        {
        	log("!!! Reseting statitics for "+id+" after "+cnt+".");
        	rStatReal.clear();
        	rStatRel.clear();
        	resetDone=true;
        }

    }

    private void log(String string) 
    {
    	if (logToConsoleEnabled){
    		System.out.println(string);
    	}else {
    		if (Log.isDebug())
    		{
    			Log.debug(string);
    		}
    	}
		
	}

	public static class RunningStat
    {
    	public static final double nanoToMilli = Math.pow(10, 6);
        private double min = Double.POSITIVE_INFINITY;
        private double max = Double.NEGATIVE_INFINITY;
        private double m_oldM = 0;
        private double m_newM = 0;
        private double m_oldS = 0;
        private double m_newS = 0;
        public long cnt = 0;

        public RunningStat()
        {
            clear();
        }

        public void push(double x)
        {
            cnt++;
            if (cnt == 1)
            {
                m_oldM = m_newM = x;
                m_oldS = 0;
            }
            else
            {
                m_newM = m_oldM + (x - m_oldM) / cnt;
                m_newS = m_oldS + (x - m_oldM) * (x - m_newM);

                // set up for next iteration
                m_oldM = m_newM;
                m_oldS = m_newS;
            }
            min = Math.min(min, x);
            max = Math.max(max, x);
        }

        public void clear()
        {
            cnt = 0;
            min = Double.POSITIVE_INFINITY;
            max = Double.NEGATIVE_INFINITY;
        }

        public double mean()
        {
            return (cnt > 0) ? m_newM : 0.0; // /nanoToMilli
        }

        public double variance()
        {
            return (((cnt > 1) ? m_newS / (cnt - 1) : 0.0)); // /Math.pow(nanoToMilli,2)
        }

        public double standardDeviation()
        {
            return Math.sqrt(variance());
        }

        public double min()
        {
            return min;
        }

        public double max()
        {
            return max;
        }

        @Override
        public String toString()
        {
            return (long) mean() + ";" + (long) variance() + ";" + (long) standardDeviation() + ";" + (long) min + ";" + (long) max;
        }

        public String toStringNanoToMilli()
        {
            return (float) (mean() / nanoToMilli) + ";" + (float) (variance() / Math.pow(nanoToMilli, 2)) + ";" + (float) (standardDeviation() / nanoToMilli) + ";" + (float) (min / nanoToMilli) + ";" + (float) (max / nanoToMilli) + ";" + (((float) (max / nanoToMilli)) - (float) (min / nanoToMilli)) + ";";
        }

    }

}


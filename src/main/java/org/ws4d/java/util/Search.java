/*******************************************************************************
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame. 
 * 
 * Contributors: 
 *  2011-2014  Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a. See changesets.zip for details.
 ******************************************************************************/
package org.ws4d.java.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * This class implements some search algorithms.
 */
public class Search {

	/**
	 * Creates a fault function for a given pattern.
	 * 
	 * @param pattern pattern to search as a byte array.
	 * @return array of offset corrections.
	 */
	private static int[] createFaultFunction(byte[] pattern) {
		int[] faultFunction;
		int lambda;

		faultFunction = new int[pattern.length];

		faultFunction[0] = 0;

		for (int l = 0; l < pattern.length - 1; l++) {
			lambda = faultFunction[l];
			while ((pattern[l + 1] != pattern[lambda]) && (lambda > 0)) {
				lambda = faultFunction[lambda - 1];
			}

			if (pattern[l + 1] == pattern[lambda])
				faultFunction[l + 1] = lambda + 1;
			else
				faultFunction[l + 1] = 0;
		}

		return faultFunction;
	}

	/**
	 * Searches for pattern in byte array. Using the <a
	 * href="http://en.wikipedia.org/wiki/Knuth-Morris-Pratt_algorithm"
	 * >Knuth-Morris-Pratt algorithm</a>.
	 * 
	 * @param text text to search in as a byte array.
	 * @param pattern pattern to search as a byte array.
	 * @return the index of the first matching pattern, -1 if no matching
	 *         pattern has been found.
	 */
	public static long searchPattern(byte[] text, byte[] pattern) {
		int l = 0;
		int[] faultFunction = createFaultFunction(pattern);

		for (int j = 0; j < text.length; j++) {

			while ((text[j] != pattern[l]) && (l > 0)) {
				l = faultFunction[l];
			}
			if (text[j] == pattern[l]) {
				l++;
				if (l == pattern.length) return (j + 1);
			}
		}

		return -1;
	}

	/**
	 * Searches for the pattern on stream. Using the <a
	 * href="http://en.wikipedia.org/wiki/Knuth-Morris-Pratt_algorithm"
	 * >Knuth-Morris-Pratt algorithm</a>.
	 * 
	 * @param in input stream.
	 * @param pattern pattern to search as a byte array.
	 * @return <code>byte</code> array containing the bytes before a match has
	 *         been found.
	 */
	public static byte[] searchPattern(InputStream in, byte[] pattern) {
		int l = 0;
		long readMaxLength = 1;
		@SuppressWarnings("unused")
		long readLength = 0;
		int readByte;

		int[] faultFunction = createFaultFunction(pattern);

		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		try {
			readByte = in.read();
			if (readByte != -1) {
				buffer.write((byte) readByte);
			}
			while (readByte != -1) {
				readByte = in.read();
				buffer.write((byte) readByte);
				if (readByte == -1) return null;

				while ((l > 0) && ((byte) readByte != pattern[l])) {
					l = faultFunction[l - 1];
				}
				if (readByte == pattern[l]) {
					l++;
				}
				if (l == pattern.length) {
					return buffer.toByteArray();
				}
				readLength += readMaxLength;
			}

		} catch (Exception e) {
			Log.debug(e);
		}

		return buffer.toByteArray();
	}

	/**
	 * Encapsulates the search for the pattern on stream in an other stream.
	 * Using the <a
	 * href="http://en.wikipedia.org/wiki/Knuth-Morris-Pratt_algorithm"
	 * >Knuth-Morris-Pratt algorithm</a>.
	 * 
	 * @param in input stream.
	 * @param pattern pattern to search as a byte array.
	 * @return the input stream which encapsulates the search.
	 */
	public static InputStream getSearchPatternWrapper(InputStream in, byte[] pattern) {
		return new KMPAlgoInputStream(in, pattern);
	}

	/**
	 * This stream encapsulates the search with the Knuth-Morris-Pratt algorithm
	 * for a pattern.
	 */
	private static class KMPAlgoInputStream extends InputStream {

		// stream
		private InputStream	in				= null;

		// algorithm stuff
		private byte[]		pattern			= null;

		private int[]		faultFunction	= null;

		private int			l				= 0;

		private long		readMaxLength	= 1;

		@SuppressWarnings("unused")
		private long		readLength		= 0;

		// boundary buffer stuff
		private boolean		beginMatch		= false;

		private int			p				= 0;

		private int			ip				= 0;

		private boolean		returnBuffer	= false;

		private byte[]		buffer			= null;

		/**
		 * Creates a nice Knuth-Morris-Pratt algorithm input stream.
		 * 
		 * @param in input stream.
		 * @param pattern pattern to search as a byte array.
		 */
		public KMPAlgoInputStream(InputStream in, byte[] pattern) {
			this.in = in;
			this.pattern = pattern;
			this.faultFunction = createFaultFunction(this.pattern);
			buffer = new byte[this.pattern.length];
		}

		/*
		 * (non-Javadoc)
		 * @see java.io.InputStream#available()
		 */
		@Override
		public int available() throws IOException {
			// FIXME analyze boundary match!
			return 0;
			// return in.available();
		}

		/*
		 * (non-Javadoc)
		 * @see java.io.InputStream#read()
		 */
		@Override
		public int read() throws IOException {
			if (returnBuffer) {
				if (p >= ip) {
					returnBuffer = false;
					buffer = new byte[this.pattern.length];
					p = 0;
					ip = 0;
				} else {
					int b = buffer[p] & 0xFF;
					p++;
					return b;
				}
			}
			/*
			 * The trick is, to read until the first match. After the first
			 * match we must try to read the pattern.
			 */
			int readByte;

			do {
				readByte = in.read();
				if (readByte == -1) {
					return -1;
				}
				while ((l > 0) && ((byte) readByte != pattern[l])) {
					l = faultFunction[l - 1];
					beginMatch = false;
					if (ip > 0) {
						returnBuffer = true;
					}
				}
				if (readByte == pattern[l]) {
					if (!beginMatch) {
						beginMatch = true;
					}
					buffer[ip] = (byte) readByte;
					ip++;
					l++;
				}
				if (l == pattern.length) {
					return -1;
				}
				readLength += readMaxLength;
			} while (beginMatch);

			if (returnBuffer) {
				buffer[ip] = (byte) readByte;
				ip++;
				if (p >= ip) {
					returnBuffer = false;
					buffer = new byte[this.pattern.length];
					p = 0;
					ip = 0;
				} else {
					int b = buffer[p] & 0xFF;
					p++;
					return b;
				}
			}
			return readByte;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((in == null) ? 0 : in.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			KMPAlgoInputStream other = (KMPAlgoInputStream) obj;
			if (in == null) {
				if (other.in != null) return false;
			} else if (!in.equals(other.in)) return false;
			return true;
		}
	}
}

/*******************************************************************************
 * Copyright (c) 2011 -2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - Several enhancements to JMEDS beta 4a
 ******************************************************************************/
package org.ws4d.java.util;

import org.ws4d.java.communication.connection.ip.IPDetection;
import org.ws4d.java.communication.connection.ip.NetworkDetection;
import org.ws4d.java.communication.connection.ip.NetworkInterface;
import org.ws4d.java.structures.Iterator;

public class ConfigurationUtil {

	public static String getAddress(String networkInterfaceName) 
	{
		NetworkDetection networkDetection= new IPDetection();
		//2011-01-10 SSch wird mit Beta4 nicht mehr benoetigt
		//networkDetection.detectInterfaces();
		for (Iterator it = networkDetection.getNetworkInterfaces(); it.hasNext();) {
			NetworkInterface ni = (NetworkInterface) it.next();
			String niName = ni.getName();
			if (networkInterfaceName==null || niName.equals(networkInterfaceName) )
			{
				Iterator it2=ni.getAddresses();
				if (it2.hasNext()) {
					return (String) it2.next();
				}
			}
		} 
		return networkInterfaceName;
	}

}
